package com.github.wxiaoqi.security.gate.Service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.RoutesRefreshedEvent;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.common.msg.ResponseCode;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by xujingfeng on 2017/4/1.
 */
@Slf4j
@Service
public class RefreshRouteService {

    @Autowired
    ApplicationEventPublisher publisher;

    @Autowired
    RouteLocator routeLocator;

    public Map<String, Object> refreshRoute() {
    	log.info("***刷新路由信息***");
    	Map<String, Object> resp = new HashMap<>();
        RoutesRefreshedEvent routesRefreshedEvent = new RoutesRefreshedEvent(routeLocator);
        publisher.publishEvent(routesRefreshedEvent);
        resp.put("code", ResponseCode.OK.getCode());
        resp.put("msg", ResponseCode.OK.getMessage());
        return resp;
    }

}
