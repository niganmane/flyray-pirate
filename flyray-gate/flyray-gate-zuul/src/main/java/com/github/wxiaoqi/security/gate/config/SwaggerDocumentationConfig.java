package com.github.wxiaoqi.security.gate.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

/** 
* @author: bolei
* @date：2018年8月28日 上午11:32:56 
* @description：类说明
*/

@Component
@Primary
public class SwaggerDocumentationConfig implements SwaggerResourcesProvider {

	@Value("${rest.api.names}")
    //private String[] apiNames;
	private String apiName;
	
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
    public List<SwaggerResource> get() {
        List resources = new ArrayList<>();
        String[] apiNames = apiName.split(",");
        if (apiNames != null) {
            Arrays.stream(apiNames).forEach(s ->
                    resources.add(swaggerResource(s, "/api/doc/" + s + "/v2/api-docs", "2.0"))
            );
        }
        return resources;
    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}
