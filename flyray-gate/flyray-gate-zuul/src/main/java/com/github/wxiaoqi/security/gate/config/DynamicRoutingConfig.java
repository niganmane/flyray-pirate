package com.github.wxiaoqi.security.gate.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.wxiaoqi.security.gate.zuul.DynamicRouteLocator;

/**
 * Created by xujingfeng on 2017/4/1.
 */
@Configuration
public class DynamicRoutingConfig {

    @Autowired
    ZuulProperties zuulProperties;
    @Autowired
    ServerProperties server;

    @Bean
    public DynamicRouteLocator routeLocator() {
    	DynamicRouteLocator routeLocator = new DynamicRouteLocator(this.server.getServlet().getServletPrefix(), this.zuulProperties);
        return routeLocator;
    }

}
