/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.6.16-log : Database - ag_auth_v1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ag_auth_v1` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `ag_auth_v1`;

/*Table structure for table `auction_goods_info` */

DROP TABLE IF EXISTS `auction_goods_info`;

CREATE TABLE `auction_goods_info` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `goods_id` varchar(30) DEFAULT NULL COMMENT '商品编号',
  `goods_name` varchar(20) DEFAULT NULL COMMENT '商品名称',
  `goods_description` varchar(50) DEFAULT NULL COMMENT '商品说明',
  `goods_img` text COMMENT '商品照片',
  `starting_price` decimal(18,2) DEFAULT NULL COMMENT '起拍价',
  `current_price` decimal(18,2) DEFAULT NULL COMMENT '现价',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `deadline_time` datetime DEFAULT NULL COMMENT '截止时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='竞拍商品信息表';

/*Data for the table `auction_goods_info` */

insert  into `auction_goods_info`(`id`,`platform_id`,`mer_id`,`goods_id`,`goods_name`,`goods_description`,`goods_img`,`starting_price`,`current_price`,`create_time`,`deadline_time`) values (1,'888888','123456','1','一号商品','雄州雾列，俊采星驰，台隍枕夷夏之交，宾主尽东南之美。','http://118.190.148.138:8088/vpaiApp/headPhoto/63536517549066813441516336956396.jpg','200.00','400.00','2018-07-16 11:12:49','2018-08-22 11:12:52'),(2,'888888','123456','2','二号商品',NULL,'http://118.190.148.138:8088/vpaiApp/headPhoto/63550997072655155201516707447018.jpg','360.00','366.00','2018-07-16 15:38:29','2018-07-04 15:38:31'),(3,'888888','123456','3','三号商品',NULL,'http://118.190.148.138:8088/vpaiApp/headPhoto/63753216449230929931521516207507.jpg','120.00','150.00','2018-07-16 15:40:15','2018-08-01 15:40:18');

/*Table structure for table `auction_recording_info` */

DROP TABLE IF EXISTS `auction_recording_info`;

CREATE TABLE `auction_recording_info` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `per_id` varchar(32) DEFAULT NULL COMMENT '用户账号',
  `goods_id` varchar(30) DEFAULT NULL COMMENT '商品编号',
  `amt` decimal(18,2) DEFAULT NULL COMMENT '竞拍金额',
  `create_time` datetime DEFAULT NULL COMMENT '竞拍时间',
  `name` varchar(20) DEFAULT NULL COMMENT '用户名',
  `user_head_portrait` text COMMENT '头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='竞拍记录表';

/*Data for the table `auction_recording_info` */

insert  into `auction_recording_info`(`id`,`platform_id`,`mer_id`,`per_id`,`goods_id`,`amt`,`create_time`,`name`,`user_head_portrait`) values (1,'888888','123456','66666','1','220.00','2018-07-17 16:12:12','江枫瑶影','http://118.190.148.138:8088/vpaiApp/headPhoto/63680814940023685121519777262483.jpg'),(2,'888888','123456','66666','1','230.00','2018-07-17 16:14:42','江枫瑶影','http://118.190.148.138:8088/vpaiApp/headPhoto/63680814940023685121519777262483.jpg'),(3,'888888','123456','66666','1','240.00','2018-07-17 17:36:19','江枫瑶影','http://118.190.148.138:8088/vpaiApp/headPhoto/63680814940023685121519777262483.jpg'),(4,'888888','123456','66666','1','250.00','2018-07-17 17:38:22','江枫瑶影','http://118.190.148.138:8088/vpaiApp/headPhoto/63680814940023685121519777262483.jpg'),(5,'888888','123456','66666','1','300.00','2018-07-17 19:18:40','江枫瑶影','http://118.190.148.138:8088/vpaiApp/headPhoto/63680814940023685121519777262483.jpg'),(7,'888888','123456','13563439881203712','1','400.00','2018-07-23 10:03:40','江枫瑶影','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJicd5aiaTgyOrxU2WmELKFwegMPTs6FfVcoYSSwBcibn2NZ2iaHVRSiaic5cTDnricOATfRr9q1XKTh4Smw/132');

/*Table structure for table `auth_client` */

DROP TABLE IF EXISTS `auth_client`;

CREATE TABLE `auth_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL COMMENT '服务编码',
  `secret` varchar(255) DEFAULT NULL COMMENT '服务密钥',
  `name` varchar(255) DEFAULT NULL COMMENT '服务名',
  `locked` char(1) DEFAULT NULL COMMENT '是否锁定',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `crt_time` datetime DEFAULT NULL COMMENT '创建时间',
  `crt_user` varchar(255) DEFAULT NULL COMMENT '创建人',
  `crt_name` varchar(255) DEFAULT NULL COMMENT '创建人姓名',
  `crt_host` varchar(255) DEFAULT NULL COMMENT '创建主机',
  `upd_time` datetime DEFAULT NULL COMMENT '更新时间',
  `upd_user` varchar(255) DEFAULT NULL COMMENT '更新人',
  `upd_name` varchar(255) DEFAULT NULL COMMENT '更新姓名',
  `upd_host` varchar(255) DEFAULT NULL COMMENT '更新主机',
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

/*Data for the table `auth_client` */

insert  into `auth_client`(`id`,`code`,`secret`,`name`,`locked`,`description`,`crt_time`,`crt_user`,`crt_name`,`crt_host`,`upd_time`,`upd_user`,`upd_name`,`upd_host`,`attr1`,`attr2`,`attr3`,`attr4`,`attr5`,`attr6`,`attr7`,`attr8`) values (1,'flyray-gate','123456','flyray-gate','0','',NULL,'','','','2017-07-07 21:51:32','1','管理员','0:0:0:0:0:0:0:1','','','','','','','',''),(3,'flyray-admin','123456','flyray-admin','0','',NULL,NULL,NULL,NULL,'2017-07-06 21:42:17','1','管理员','0:0:0:0:0:0:0:1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'flyray-auth','123456','flyray-auth','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'flyray-config','fXHsssa2','flyray-config','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,'flyray-demo-mybatis','bZf8yvj9','flyray-demo-mybatis','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,'flyray-template','bZf8yvj8','flyray-template','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,'flyray-biz','123456','flyray-biz','0','业务模块','2018-04-18 09:39:44','null','null','null','2018-04-18 09:39:44','null',NULL,'null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,'flyray-crm-core','123456','flyray-crm-core','0','用户框架','2018-05-17 10:03:16','null','null','null','2018-05-17 10:03:16','null',NULL,'null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,'flyray-pay-core','123456','flyray-pay-core','0','支付模块',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `auth_client_service` */

DROP TABLE IF EXISTS `auth_client_service`;

CREATE TABLE `auth_client_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL,
  `crt_user` varchar(255) DEFAULT NULL,
  `crt_name` varchar(255) DEFAULT NULL,
  `crt_host` varchar(255) DEFAULT NULL,
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;

/*Data for the table `auth_client_service` */

insert  into `auth_client_service`(`id`,`service_id`,`client_id`,`description`,`crt_time`,`crt_user`,`crt_name`,`crt_host`,`attr1`,`attr2`,`attr3`,`attr4`,`attr5`,`attr6`,`attr7`,`attr8`) values (21,'4','5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,'3','6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(41,'3','1',NULL,'2017-12-31 08:58:03','null','null','null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(42,'6','1',NULL,'2017-12-31 08:58:03','null','null','null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50,'16','14',NULL,'2018-05-17 10:06:19','null','null','null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(51,'14','16',NULL,'2018-05-17 10:06:19','null','null','null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(52,'17','16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(53,'17','14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(54,'14','17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(56,'3','16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `cms_activity_customer` */

DROP TABLE IF EXISTS `cms_activity_customer`;

CREATE TABLE `cms_activity_customer` (
  `id` varchar(255) NOT NULL COMMENT '主键',
  `activity_id` varchar(18) DEFAULT NULL COMMENT '活动序号',
  `customer_id` varchar(18) DEFAULT NULL COMMENT '用户序号',
  `joinStatus` varchar(4) DEFAULT NULL COMMENT '0未加入 1已加入 3本人发布',
  `real_name` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `contact_value` varchar(32) DEFAULT NULL COMMENT '联系号码',
  `contact_way` int(2) DEFAULT NULL COMMENT '联系方式     0微信号 1QQ号 2手机号',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cms_activity_customer` */

insert  into `cms_activity_customer`(`id`,`activity_id`,`customer_id`,`joinStatus`,`real_name`,`contact_value`,`contact_way`,`create_time`) values ('13692037959331840','8','13669185209577472','1','赵四','zs2011',0,'2018-07-24 11:17:40');

/*Table structure for table `cms_activity_info` */

DROP TABLE IF EXISTS `cms_activity_info`;

CREATE TABLE `cms_activity_info` (
  `id` varchar(18) NOT NULL COMMENT '序号',
  `activity_name` varchar(255) DEFAULT NULL COMMENT '活动名称',
  `activity_logo` varchar(255) DEFAULT NULL COMMENT '活动logo',
  `activity_des` varchar(255) DEFAULT NULL COMMENT '活动摘要',
  `activity_content` varchar(255) DEFAULT NULL COMMENT '活动内容',
  `activity_start_time` timestamp NULL DEFAULT NULL COMMENT '活动开始时间',
  `activity_end_time` timestamp NULL DEFAULT NULL COMMENT '活动结束时间',
  `activity_addr` varchar(100) DEFAULT NULL COMMENT '活动地点',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `flag` varchar(255) DEFAULT NULL COMMENT '状态标识 00：无效；10：推荐；20：置顶',
  `highlights` text COMMENT '活动花絮',
  `merchant_id` varchar(255) DEFAULT NULL COMMENT '商户号',
  `platform_id` varchar(255) DEFAULT NULL COMMENT '平台编号',
  `cover_img` varchar(255) DEFAULT NULL COMMENT '封面',
  `qr_img` varchar(255) DEFAULT NULL COMMENT '二维码图片',
  `publisher_name` varchar(32) DEFAULT NULL COMMENT '发布者姓名',
  `publisher_contactWay` int(2) DEFAULT NULL COMMENT '联系方式     0微信号 1QQ号 2手机号',
  `publisher_contactValue` varchar(32) DEFAULT NULL COMMENT '联系号码',
  `comment_count` int(10) DEFAULT NULL COMMENT '评论数量',
  `favort_count` int(10) DEFAULT NULL COMMENT '点赞数量',
  `acttype_name` varchar(32) DEFAULT NULL COMMENT '活动类型名称',
  `initiation_status` int(4) DEFAULT NULL COMMENT '发起状态 0准备中 1进行中 2结束',
  `people_num` int(4) DEFAULT NULL COMMENT '人数限制',
  `acttype` int(2) DEFAULT NULL COMMENT '活动类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cms_activity_info` */

insert  into `cms_activity_info`(`id`,`activity_name`,`activity_logo`,`activity_des`,`activity_content`,`activity_start_time`,`activity_end_time`,`activity_addr`,`create_time`,`flag`,`highlights`,`merchant_id`,`platform_id`,`cover_img`,`qr_img`,`publisher_name`,`publisher_contactWay`,`publisher_contactValue`,`comment_count`,`favort_count`,`acttype_name`,`initiation_status`,`people_num`,`acttype`) values ('1','缘来在当地','http://pic6.wed114.cn/20130421/20130421234458107.JPG','简介','2008年，钟扬第三次向上海市科委申请课题，终于成功。实验之后，又建立起10亩大的种植基地。\r\n经佐琴提供的钟扬2010年底向上海市科委报告的内容显示，他们的实验表明，可以通过不断保温抗寒驯化，增强红树在临港地区的适应性，而上海气温外的其余自然条件都符合红树的生长条件。','2017-11-15 11:00:00','2017-12-30 15:00:00','东方明珠','2017-10-06 17:34:50','10','<p>伺服电机浪费<br></p><p><strong>士大夫</strong></p><p><span><em><strong>撒地方看见实力打脸士大夫飞</strong></em></span></p><p>士大夫萨科技<strong><br></strong></p><p><img src=\"http://ueditor.baidu.com/server/ueditor/upload/image/demo.jpg\" title=\"\" alt=\"demo.jpg\" _src=\"/server/ueditor/upload/image/demo.jpg\"></p><p><img src=\"http://img.baidu.com/hi/jx2/j_0025.gif\" _src=\"http://img.baidu.com/hi/jx2/j_0025.gif\">​胜多少的几番</p>','123456','888888','http://pic6.wed114.cn/20130421/20130421234458107.JPG',NULL,'测试',0,'987654321',0,0,'运动',2,20,1),('10','咕嘟咕嘟','http://pic6.wed114.cn/20130421/20130421234458107.JPG','咕嘟','随着这位植物学家9月25日上午因车祸意外离世，这一宏愿也成为了他的身后事，只能待其他人来完成。','2017-11-16 00:00:00','2017-12-30 23:59:59','阿拉善','2018-06-25 11:08:41',NULL,NULL,'123456','888888','http://pic6.wed114.cn/20130421/20130421234458107.JPG',NULL,'测试',1,'987654321',0,0,'游戏',1,20,2),('11','破破破','http://pic6.wed114.cn/20130421/20130421234458107.JPG','时尚','将生长在更南方的红树，移植到上海，在海边增添一片绵延不断的森林，是钟扬生前特别重视的一个项目。他预计，需要几十年甚至两百年才能完成“南树北移”的心愿。','2017-11-17 00:00:00','2017-12-09 23:59:59','北京','2018-07-24 11:08:37',NULL,NULL,'123456','888888','http://pic6.wed114.cn/20130421/20130421234458107.JPG',NULL,'测试',2,'987654321',0,0,'交友',2,20,3),('2','使人成熟的是经历，而不是岁月','http://pic6.wed114.cn/20130421/20130421234458107.JPG','岁月催人老','但钟扬对此抱有信心，2000年调入上海他就思考谋划这个问题。在他看来，这不仅仅有科研意义，红树林促淤保滩，对空气和水质都有净化作用，如果成功，将是上海一张新的生态名片。','2017-11-18 17:37:13','2017-12-19 17:34:53','天安门','2017-10-06 17:34:55','10',NULL,'123456','888888','http://pic6.wed114.cn/20130421/20130421234458107.JPG',NULL,'测试',0,'987654321',0,0,'旅行',2,20,4),('3','绝口不提不是因为忘记，而是因为铭记','http://pic6.wed114.cn/20130421/20130421234458107.JPG','铭记','此前，红树林最北生长在温州，上海纬度高了3度，很多人怀疑南树北移的可能性。钟扬生前曾回忆，一位教授听到后，连忙劝说他，“不能瞎搞，这个搞不活的。”','2017-11-06 17:37:16','2017-12-16 17:34:55','故宫','2017-10-06 17:34:58','10',NULL,'123456','888888','http://pic6.wed114.cn/20130421/20130421234458107.JPG',NULL,'测试',1,'987654321',0,0,'读书',1,20,5),('4','嘻哈','http://pic6.wed114.cn/20130421/20130421234458107.JPG','嘻嘻哈哈','桐花树已繁衍三代，最近又结了种子。 能否忍受住寒冬，正是红树林落地上海的关键。','2017-11-14 17:37:19','2017-12-15 17:34:59','水立方','2017-10-06 17:35:01','20',NULL,'123456','888888','http://pic6.wed114.cn/20130421/20130421234458107.JPG',NULL,'测试',2,'987654321',0,0,'竞赛',2,20,6),('5','123木头人','http://pic6.wed114.cn/20130421/20130421234458107.JPG','游戏','“我们已经看到希望了，（桐花树）第三代种子已经开始适应上海气候了，冬天最冷的时候，零下八度，它能承受。”上海虹升农业公司董事长吉临娟说，她的公司是钟扬在当地的合作方。','2017-11-06 17:00:00','2017-12-17 23:59:59','鸟巢','2018-06-26 11:08:32',NULL,NULL,'123456','888888','http://pic6.wed114.cn/20130421/20130421234458107.JPG',NULL,'测试',0,'987654321',0,0,'竞赛',0,20,6),('6','瑞奇','http://pic6.wed114.cn/20130421/20130421234458107.JPG','狗狗','但好消息是，2011年从珠海接种的两年生桐花树幼苗，到2016年底已繁衍至第三代，耐寒性大大提高。另一种红树“秋茄”，2016年也第一次结了种子，繁衍出第二代。','2017-11-06 17:00:00','2017-12-08 23:59:59','富华游乐园','2018-06-30 11:08:28',NULL,NULL,'123456','888888','http://pic6.wed114.cn/20130421/20130421234458107.JPG',NULL,'测试',1,'987654321',0,0,'电影',1,20,7),('7','嘟嘟','http://pic6.wed114.cn/20130421/20130421234458107.JPG','嘟嘟侠','新场地有限，加之迁徙损害植物的根系，这次搬迁对项目推进影响不小。令经佐琴尤为可惜的是，几十棵长到两米多高的无瓣海棠，未能在搬迁中保留下来。','2017-11-07 00:00:00','2017-12-16 23:59:59','水上皇宫','2018-07-03 11:08:22',NULL,NULL,'123456','888888','http://pic6.wed114.cn/20130421/20130421234458107.JPG',NULL,'测试',2,'987654321',0,0,'音乐',2,20,8),('8','小黄人','http://pic6.wed114.cn/20130421/20130421234458107.JPG','俩眼','“别看你们看到东西不多，其实很不容易。”钟扬生前的助手经佐琴说，原来红树林种在南汇嘴公园，试验种植基地占地十亩，有19个大棚，但2016年底原址要做停车场，只能搬迁。','2017-10-07 00:00:00','2017-10-30 23:59:59','五角大楼','2018-07-12 11:08:19',NULL,'<p>伺服电机浪费<br></p><p><strong>士大夫</strong></p><p><span><em><strong>撒地方看见实力打脸士大夫飞</strong></em></span></p><p>士大夫萨科技<strong><br></strong></p><p><img src=\"http://ueditor.baidu.com/server/ueditor/upload/image/demo.jpg\" title=\"\" alt=\"demo.jpg\" _src=\"/server/ueditor/upload/image/demo.jpg\"></p><p><img src=\"http://img.baidu.com/hi/jx2/j_0025.gif\" _src=\"http://img.baidu.com/hi/jx2/j_0025.gif\">​胜多少的几番</p>','123456','888888','http://pic6.wed114.cn/20130421/20130421234458107.JPG',NULL,'测试',0,'987654321',1,0,'其他',1,20,9),('9','绝地武士','http://pic6.wed114.cn/20130421/20130421234458107.JPG','手拿荧光棒','去年底移植过来的两大棚红树。 本文图片均为 澎湃新闻记者 赖鑫林 图 9月29日，澎湃新闻（www.thepaper.cn）记者实地探访了这片红树林，从复旦大学驱车80公里，抵达位于上海东南角的南汇新城镇，穿过田垄、泥地，见到了眼前两个大棚，里面种着桐花树、秋茄、无瓣海棠等多种红树，大部分长势良好。','2017-11-18 00:00:00','2017-12-16 23:59:59','迪拜','2018-07-24 11:08:14',NULL,NULL,'123456','888888','http://pic6.wed114.cn/20130421/20130421234458107.JPG',NULL,'测试',1,'987654321',1,0,'运动',0,20,1);

/*Table structure for table `cms_community_comment` */

DROP TABLE IF EXISTS `cms_community_comment`;

CREATE TABLE `cms_community_comment` (
  `id` varchar(35) NOT NULL COMMENT '主键id',
  `comment_type` varchar(255) DEFAULT NULL COMMENT '评论类型(1评论2回复)',
  `comment_content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `comment_target_id` varchar(35) DEFAULT NULL COMMENT '评论的目标id',
  `comment_by_name` varchar(255) DEFAULT NULL COMMENT '发表评论的用户昵称',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '发表评论的用户id',
  `comment_target_user_name` varchar(255) DEFAULT NULL COMMENT '评论的目标用户昵称',
  `comment_target_user_id` varchar(32) DEFAULT NULL COMMENT '评论的目标用户id',
  `comment_likeCount` bigint(11) DEFAULT NULL COMMENT '该评论被点赞的数量',
  `comment_time` timestamp NULL DEFAULT NULL COMMENT '评论时间',
  `comment_module_no` varchar(4) DEFAULT NULL COMMENT '评论模块编号01社群 02卖朋友',
  `merchant_id` varchar(32) DEFAULT NULL COMMENT '商户号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '机构号',
  `parent_id` varchar(20) DEFAULT NULL COMMENT '回复评论的目标id',
  `avatarUrl` varchar(255) DEFAULT NULL COMMENT '头像地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cms_community_comment` */

insert  into `cms_community_comment`(`id`,`comment_type`,`comment_content`,`comment_target_id`,`comment_by_name`,`customer_id`,`comment_target_user_name`,`comment_target_user_id`,`comment_likeCount`,`comment_time`,`comment_module_no`,`merchant_id`,`platform_id`,`parent_id`,`avatarUrl`) values ('13671973244973056','1','enen','13671916003995648','saving','13669185209577472','','',NULL,'2018-07-23 12:41:45','02','123456','888888','','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLpOI28iaGx0KpOZBbEad3VypWVh3AOQb9ObvVjkMib9BE1ajWNnXicL71Hxkk6h7FJgF5THonXqxwbw/132'),('13672848261984256','1','123','13671916003995648','saving','13669185209577472','','',NULL,'2018-07-23 13:37:23','02','123456','888888','','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLpOI28iaGx0KpOZBbEad3VypWVh3AOQb9ObvVjkMib9BE1ajWNnXicL71Hxkk6h7FJgF5THonXqxwbw/132'),('13692919770853376','1','嗯嗯','9','saving','13669185209577472',NULL,NULL,NULL,'2018-07-24 10:53:29','03','123456','888888',NULL,'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLpOI28iaGx0KpOZBbEad3VypWVh3AOQb9ObvVjkMib9BE1ajWNnXicL71Hxkk6h7FJgF5THonXqxwbw/132'),('13789139959623680','1','2323','13671973244973056','博羸兄弟','13788719187046400','','',NULL,'2018-07-28 16:51:00','02','123456','888888','13671973244973056','https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83errTs38TbibfYiaI3zbJdKCle5QyNdG0ZQ5Ap680NLmliaYlNfBWZ9pv1vpbZ16Q8NjVJUPB2bQoh67Q/132'),('13789144416595968','1','2323','13671973244973056','博羸兄弟','13788719187046400','','',NULL,'2018-07-28 16:51:17','02','123456','888888','13671973244973056','https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83errTs38TbibfYiaI3zbJdKCle5QyNdG0ZQ5Ap680NLmliaYlNfBWZ9pv1vpbZ16Q8NjVJUPB2bQoh67Q/132');

/*Table structure for table `cms_community_society` */

DROP TABLE IF EXISTS `cms_community_society`;

CREATE TABLE `cms_community_society` (
  `id` varchar(35) NOT NULL COMMENT '主键id',
  `society_name` varchar(32) DEFAULT NULL COMMENT '名称',
  `society_qr` varchar(255) DEFAULT NULL COMMENT '二维码',
  `society_content` varchar(255) DEFAULT NULL COMMENT '介绍内容',
  `merchant_id` varchar(255) DEFAULT NULL COMMENT '商户号',
  `platform_id` varchar(255) DEFAULT NULL COMMENT '平台编号',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cms_community_society` */

insert  into `cms_community_society`(`id`,`society_name`,`society_qr`,`society_content`,`merchant_id`,`platform_id`,`create_time`) values ('346534611','社群1','/images/qrcodeSmall.png','那年冬天，父亲的差使1也交卸了，正是祸不单行的日子。我从北京到徐州，打算跟着父亲奔丧2回家。到徐州见着父亲，看见满院狼藉3的东西，又想起祖母，不禁簌簌地流下眼泪。父亲说：“事已如此，不必难过，好在天无绝人之路！”','123456','888888','2018-07-31 14:55:31'),('3465365','社群121','/images/qrcodeSmall.png','回家变卖典质4，父亲还了亏空；又借钱办了丧事。这些日子，家中光景5很是惨澹，一半为了丧事，一半为了父亲赋闲6。丧事完毕，父亲要到南京谋事，我也要回北京念书，我们便同行。','123456','888888','2018-07-30 14:55:36'),('364163133','社群13','/images/qrcodeSmall.png','到南京时，有朋友约去游逛，勾留7了一日；第二日上午便须渡江到浦口，下午上车北去。父亲因为事忙，本已说定不送我，叫旅馆里一个熟识的茶房8陪我同去。他再三嘱咐茶房，甚是仔细。但他终于不放心，怕茶房不妥帖9；颇踌躇10了一会。其实我那年已二十岁，北京已来往过两三次，是没有什么要紧的了。他踌躇了一会，终于决定还是自己送我去。我再三劝他不必去；他只说：“不要紧，他们去不好！”','123456','888888','2018-07-29 14:55:40'),('364631133','社群14','/images/qrcodeSmall.png','我们过了江，进了车站。我买票，他忙着照看行李。行李太多，得向脚夫11行些小费才可过去。他便又忙着和他们讲价钱。我那时真是聪明过分，总觉他说话不大漂亮，非自己插嘴不可，但他终于讲定了价钱；就送我上车。他给我拣定了靠车门的一张椅子；我将他给我做的紫毛大衣铺好座位。他嘱我路上小心，夜里要警醒些，不要受凉。又嘱托茶房好好照应我。我心里暗笑他的迂；他们只认得钱，托他们只是白托！而且我这样大年纪的人，难道还不能料理自己么？我现在想想，我那时真是太聪明了。','123456','888888','2018-07-01 14:55:48'),('36463133','社群16','/images/qrcodeSmall.png','布小帽，穿着黑布大马褂12，深青布棉袍，蹒跚13地走到铁道边，慢慢探身下去，尚不大难。可是他穿过铁道，要爬上那边月台，就不容易了。他用两手攀着上面，两脚再向上缩；他肥胖的身子向左微倾，显出努力的样子。这时我看见他的背影，我的泪很快地流下来了。我赶紧拭干了','123456','888888','2018-07-04 14:55:54'),('364631332','社群17','/images/qrcodeSmall.png','我赶紧拭干了泪。怕他看见，也怕别人看见。我再向外看时，他已抱了朱红的橘子往回走了。过铁道时，他先将橘子散放在地上，自己慢慢爬下，再抱起橘子走。到这边时，我赶紧去搀他。他和我走到车上，将橘子一股脑儿放在我的皮大衣上。于是扑扑衣上的泥土，心','123456','888888','2018-07-18 14:55:59'),('36463136','社群18','/images/qrcodeSmall.png','心里很轻松似的。过一会儿说：“我走了，到那边来信！”我望着他走出去。他走了几步，回过头看见我，说：“进去吧，里边没人。”等他的背影混入来来往往的人里，再找不着了，我便进来坐下，我的眼泪又来了。','123456','888888','2018-07-02 14:56:33'),('364633133','社群19','/images/qrcodeSmall.png','近几年来，父亲和我都是东奔西走，家中光景是一日不如一日。他少年出外谋生，独力支持，做了许多大事。哪知老境却如此颓唐！他触目伤怀，自然情不能自已。情','123456','888888','2018-07-02 14:56:30'),('36465444','社群10','/images/qrcodeSmall.png','光景是一日不如一日。他少年出外谋生，独力支持，做了许多大事。哪知老境却如此颓唐！他触目伤怀，自然情不能自已。情郁于中，自然要发之于','123456','888888','2018-07-16 14:56:25'),('366543421','社群11','/images/qrcodeSmall.png','一日不如一日。他少年出外谋生，独力支持，做了许多大事。哪知老境却如此颓唐！他触目伤怀，自然情不能自已。情郁于中，自然要发之于外；家庭琐屑便往往触他之怒。他待我渐渐不同往日。但最近两年不见，他终于忘却我的不好，只是惦记着我，惦记着他的儿子。我北来后','123456','888888','2018-06-22 14:56:19'),('41221663','社群12','/images/qrcodeSmall.png','之于外；家庭琐屑便往往触他之怒。他待我渐渐不同往日。但最近两年不见，他终于忘却我的不好，只是惦记着我，惦记着他的儿子。我北来后，他写了一信给我，信中说道：“我身体平安，惟膀子疼痛厉害，举箸14提笔，诸多不便，大约大去之期15不远矣。”我读到此处，在晶','123456','888888','2018-06-13 14:56:12'),('87654112','社群111','/images/qrcodeSmall.png','然要发之于外；家庭琐屑便往往触他之怒。他待我渐渐不同往日。但最近两年不见，他终于忘却我的不好，只是惦记着我，惦记着他的儿子。我北来后，他写了一信给我，信中说道：“我身体平安，惟膀子疼痛厉害，举箸14提笔，诸多不便，大约大去之期15不远矣。”我读到此处，在晶莹的泪光中，又看见那肥胖的、青布棉袍黑布马褂的背影。唉！我不知何时再能与他相见！ [2] ','123456','888888','2018-07-10 14:56:05');

/*Table structure for table `cms_community_view_favort` */

DROP TABLE IF EXISTS `cms_community_view_favort`;

CREATE TABLE `cms_community_view_favort` (
  `id` varchar(35) NOT NULL COMMENT '主键，js无法转化18位数字',
  `customer_id` varchar(35) DEFAULT NULL COMMENT '外键索引，用户编号',
  `point_id` varchar(35) DEFAULT NULL COMMENT '外键索引，观点编号',
  `favort_status` int(1) DEFAULT NULL COMMENT '1点赞2取消赞',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  `merchant_id` varchar(35) DEFAULT NULL COMMENT '商户号',
  `platform_id` varchar(35) DEFAULT NULL COMMENT '平台编号',
  `nickName` varchar(32) DEFAULT NULL,
  `avatarUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `point_id` (`point_id`),
  KEY `favort_status` (`favort_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cms_community_view_favort` */

insert  into `cms_community_view_favort`(`id`,`customer_id`,`point_id`,`favort_status`,`create_time`,`update_time`,`merchant_id`,`platform_id`,`nickName`,`avatarUrl`) values ('13671881312907264','13669185209577472','13671874510794752',1,'2018-07-23 12:35:54','2018-07-23 12:35:54','123456','888888','saving','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLpOI28iaGx0KpOZBbEad3VypWVh3AOQb9ObvVjkMib9BE1ajWNnXicL71Hxkk6h7FJgF5THonXqxwbw/132'),('13671931378216960','13669185209577472','13671916003995648',1,'2018-07-23 12:39:05','2018-07-24 11:12:50','123456','888888','saving','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLpOI28iaGx0KpOZBbEad3VypWVh3AOQb9ObvVjkMib9BE1ajWNnXicL71Hxkk6h7FJgF5THonXqxwbw/132'),('13742071414337536','13563439881203712','13671874510794752',1,'2018-07-26 14:58:28','2018-07-26 14:58:28','123456','888888','江枫瑶影','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJicd5aiaTgyOrxU2WmELKFwegMPTs6FfVcoYSSwBcibn2NZ2iaHVRSiaic5cTDnricOATfRr9q1XKTh4Smw/132'),('13788870120386560','13788719187046400','13671874510794752',1,'2018-07-28 16:33:51','2018-07-28 16:34:09','123456','888888','博羸兄弟','https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83errTs38TbibfYiaI3zbJdKCle5QyNdG0ZQ5Ap680NLmliaYlNfBWZ9pv1vpbZ16Q8NjVJUPB2bQoh67Q/132');

/*Table structure for table `cms_community_viewpoint` */

DROP TABLE IF EXISTS `cms_community_viewpoint`;

CREATE TABLE `cms_community_viewpoint` (
  `id` varchar(20) NOT NULL COMMENT '主键',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '索引，用户编号',
  `point_text` text COMMENT '发表观点，文字内容',
  `point_img` varchar(300) DEFAULT NULL COMMENT '发表观点，图片路径，json格式',
  `point_time` timestamp NULL DEFAULT NULL COMMENT '发表时间',
  `point_address` varchar(50) DEFAULT NULL COMMENT '发表地点',
  `favort_count` int(11) DEFAULT NULL COMMENT '点赞数量',
  `comment_count` int(11) DEFAULT NULL COMMENT '评论数量',
  `merchant_id` varchar(20) DEFAULT NULL COMMENT '商户号',
  `platform_id` varchar(20) DEFAULT NULL COMMENT '平台编号',
  `sale_name` varchar(20) DEFAULT NULL COMMENT '卖朋友姓名',
  `sale_sex` varchar(10) DEFAULT NULL COMMENT '卖朋友性别 0男 1女',
  `view_type` varchar(4) DEFAULT NULL COMMENT '观点类型 01社区 02卖朋友',
  `isPrivate` varchar(4) DEFAULT NULL COMMENT '是否匿名  默认不匿名 01匿名',
  `nick_name` varchar(32) DEFAULT NULL COMMENT '昵称',
  `avatar_url` varchar(255) DEFAULT NULL COMMENT '头像地址',
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `point_time` (`point_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cms_community_viewpoint` */

insert  into `cms_community_viewpoint`(`id`,`customer_id`,`point_text`,`point_img`,`point_time`,`point_address`,`favort_count`,`comment_count`,`merchant_id`,`platform_id`,`sale_name`,`sale_sex`,`view_type`,`isPrivate`,`nick_name`,`avatar_url`) values ('13671874510794752','13669185209577472','爱情属于勇敢的人~',NULL,'2018-07-23 12:35:28','',3,2,'123456','888888',NULL,NULL,'01','01','saving','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLpOI28iaGx0KpOZBbEad3VypWVh3AOQb9ObvVjkMib9BE1ajWNnXicL71Hxkk6h7FJgF5THonXqxwbw/132');

/*Table structure for table `customer_base` */

DROP TABLE IF EXISTS `customer_base`;

CREATE TABLE `customer_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(32) NOT NULL COMMENT '用户编号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_type` varchar(6) DEFAULT NULL COMMENT '客户类型    CUST00：平台  （ CUST01：商户  CUST02：用户）：普通客户默认为空',
  `pay_password` varchar(40) DEFAULT NULL COMMENT '支付密码',
  `password_error_count` int(11) DEFAULT NULL COMMENT '登录密码错误次数',
  `pay_password_error_count` int(11) DEFAULT NULL COMMENT '支付密码错误次数',
  `password_status` varchar(10) DEFAULT NULL COMMENT '登录密码状态  00：正常   01：未设置   02：锁定',
  `pay_password_status` varchar(10) DEFAULT NULL COMMENT '支付密码状态    00：正常   01：未设置   02：锁定',
  `personal_id` varchar(32) DEFAULT NULL COMMENT '个人客户编号',
  `merchant_id` varchar(32) DEFAULT NULL COMMENT '商户客户编号',
  `status` varchar(6) DEFAULT NULL COMMENT '账户状态 00：正常，01：客户冻结',
  `authentication_status` varchar(6) DEFAULT NULL COMMENT '认证状态（同步个人客户基础信息表中认证状态）  00：未认证  01：无需认证  02：认证成功  03：认证失败',
  `register_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `register_ip` varchar(46) DEFAULT NULL COMMENT '注册IP',
  `login_time` timestamp NULL DEFAULT NULL COMMENT '上次登录时间',
  `login_ip` varchar(46) DEFAULT NULL COMMENT '上次登录IP',
  `last_login_role` varchar(6) DEFAULT NULL COMMENT '最后登录角色（默认为空）  01：个人客户     02 ：商户客户',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='客户基础信息';

/*Data for the table `customer_base` */

insert  into `customer_base`(`id`,`customer_id`,`platform_id`,`customer_type`,`pay_password`,`password_error_count`,`pay_password_error_count`,`password_status`,`pay_password_status`,`personal_id`,`merchant_id`,`status`,`authentication_status`,`register_time`,`register_ip`,`login_time`,`login_ip`,`last_login_role`,`create_time`,`update_time`) values (1,'c1001','p1001',NULL,'123456',0,0,'00','00','p1001','m1001','00','01','2018-07-17 16:58:36',NULL,'2018-07-17 17:24:06',NULL,NULL,'2018-07-17 16:58:36','2018-07-19 11:08:43'),(2,'c1002','p1001',NULL,NULL,0,0,'00','00',NULL,NULL,'00',NULL,'2018-07-17 16:58:59',NULL,NULL,NULL,NULL,'2018-07-17 16:58:59','2018-07-17 17:04:02'),(3,'c1003','p1001',NULL,NULL,0,0,'00','00',NULL,NULL,'00',NULL,'2018-07-17 16:59:06',NULL,NULL,NULL,NULL,'2018-07-17 16:59:06','2018-07-17 17:04:03'),(4,'c1004','p1001',NULL,NULL,0,0,'00','00',NULL,NULL,'00',NULL,'2018-07-17 16:59:13',NULL,NULL,NULL,NULL,'2018-07-17 16:59:13','2018-07-17 17:04:04'),(5,'c1005','p1001',NULL,NULL,0,0,'00','00',NULL,NULL,'00',NULL,'2018-07-17 17:00:25',NULL,NULL,NULL,NULL,'2018-07-17 17:00:25','2018-07-17 17:04:04'),(6,'c1006','p1001',NULL,NULL,0,0,'00','00',NULL,NULL,'00',NULL,'2018-07-17 17:00:28',NULL,NULL,NULL,NULL,'2018-07-17 17:00:28','2018-07-17 17:04:05'),(7,'c1007','p1001',NULL,NULL,0,0,'00','00',NULL,NULL,'00',NULL,'2018-07-17 17:00:30',NULL,NULL,NULL,NULL,'2018-07-17 17:00:30','2018-07-17 17:04:06'),(8,'c1008','p1001',NULL,NULL,0,0,'00','00',NULL,NULL,'00',NULL,'2018-07-17 17:00:32',NULL,NULL,NULL,NULL,'2018-07-17 17:00:32','2018-07-17 17:04:06'),(9,'c1009','p1001',NULL,NULL,0,0,'00','00',NULL,NULL,'00',NULL,'2018-07-17 17:00:39',NULL,NULL,NULL,NULL,'2018-07-17 17:00:39','2018-07-17 17:04:07'),(10,'c1010','p1001',NULL,NULL,0,0,'00','00',NULL,NULL,'00',NULL,'2018-07-17 17:00:45',NULL,NULL,NULL,NULL,'2018-07-17 17:00:45','2018-07-17 17:04:08'),(11,'c1011','p1001',NULL,NULL,0,0,'00','00',NULL,NULL,'00',NULL,'2018-07-17 17:02:46',NULL,NULL,NULL,NULL,'2018-07-17 17:02:46','2018-07-17 17:04:10');

/*Table structure for table `customer_base_auths` */

DROP TABLE IF EXISTS `customer_base_auths`;

CREATE TABLE `customer_base_auths` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_id` varchar(32) NOT NULL COMMENT '用户编号',
  `customer_type` varchar(6) DEFAULT NULL COMMENT '客户类型    CUST00：平台   CUST01：商户  CUST02：用户',
  `auth_type` varchar(6) NOT NULL COMMENT '授权类型  00：登录授权  01：支付授权（预留）',
  `auth_method` varchar(20) DEFAULT NULL COMMENT '授权方式  phone：手机号  email：邮箱   username：用户名  weixin：微信  qq：QQ  weibo：微博',
  `isThird` varchar(6) DEFAULT NULL COMMENT '是否第三方  00：站内    01：第三方',
  `identifier` varchar(32) DEFAULT NULL COMMENT '识别码',
  `credential` varchar(60) NOT NULL COMMENT '凭据',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='客户授权信息';

/*Data for the table `customer_base_auths` */

insert  into `customer_base_auths`(`id`,`platform_id`,`customer_id`,`customer_type`,`auth_type`,`auth_method`,`isThird`,`identifier`,`credential`) values (1,'p1001\r\np1001\r\np1001','c1001',NULL,'00',NULL,'00',NULL,''),(3,'888888','13669185209577472','CUST02','00','weixin','01',NULL,'onbV_4v8knqWp27DIXij7glSn-mo'),(4,'888888','13788719187046400','CUST02','00','weixin','01',NULL,'onbV_4kn33Xz1YMoBc581NHCpc9Y'),(5,'888888','13827407778492416','CUST02','00','weixin','01',NULL,'onbV_4liYh60x-3vQgXDCgOD0bzc');

/*Table structure for table `fight_group_category_info` */

DROP TABLE IF EXISTS `fight_group_category_info`;

CREATE TABLE `fight_group_category_info` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `groups_id` int(10) DEFAULT NULL COMMENT '团类目id',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `group_name` varchar(10) DEFAULT NULL COMMENT '团名称',
  `tx_amt` decimal(18,2) DEFAULT NULL COMMENT '金额',
  `people_num` int(10) DEFAULT NULL COMMENT '限制人数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='团类目信息表';

/*Data for the table `fight_group_category_info` */

insert  into `fight_group_category_info`(`id`,`platform_id`,`mer_id`,`groups_id`,`goods_id`,`group_name`,`tx_amt`,`people_num`) values (1,'888888','123456',1,1,'不限','27.00',NULL),(2,'888888','123456',2,1,'2人团','23.00',2),(3,'888888','123456',3,1,'5人团','20.00',5),(4,'888888','123456',4,1,'10人团','15.00',10);

/*Table structure for table `fight_group_goods_info` */

DROP TABLE IF EXISTS `fight_group_goods_info`;

CREATE TABLE `fight_group_goods_info` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `goods_name` varchar(20) DEFAULT NULL COMMENT '商品名称',
  `goods_description` varchar(20) DEFAULT NULL COMMENT '说明',
  `goods_img` varchar(200) DEFAULT NULL COMMENT '商品图片',
  `original_price` decimal(10,0) DEFAULT NULL COMMENT '原价',
  `discount_price` decimal(10,0) DEFAULT NULL COMMENT '折扣价',
  `sold_num` int(10) DEFAULT NULL COMMENT '已售数量',
  `effective_time_description` varchar(100) DEFAULT NULL COMMENT '有效时间说明',
  `appointment_reminder` text COMMENT '预约提醒',
  `tips` text COMMENT '温馨提示',
  `group_description` text COMMENT '拼团说明',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='拼团商品信息';

/*Data for the table `fight_group_goods_info` */

insert  into `fight_group_goods_info`(`id`,`platform_id`,`mer_id`,`goods_id`,`goods_name`,`goods_description`,`goods_img`,`original_price`,`discount_price`,`sold_num`,`effective_time_description`,`appointment_reminder`,`tips`,`group_description`) values (1,'888888','123456',1,'海南贵妃芒8粒装','贡品贵妃 回味无穷','http://118.190.148.138:8088/vpaiApp/headPhoto/ffffffff-bbef-a433-4e73-0d3c66e5904e.jpg','29','0',12,'2016-07-06至2017-01-01（周末法定节日通用）','无需预约，消费高峰期可能需要等位','拼团用户不可享受商家其他优惠','1、商家出商品团，如1个人买该商品是29元，5人团价格变为19.9元。 \r\n2、消费者参加5人团，先付19.9元，则开团成功，然后把链接发到朋友圈，让有需要的朋友参团。 \r\n3、24小时内，满5人则拼团成功，未满5人则拼团失败，系统自动退款。\r\n\r\n拼团可以激发消费者低价消费的积极性，让消费者自发传播，它是一种快速的裂变营销。可以在很短时间内，积累出庞大的精准客户，而且这些客户都是基于同一个圈子。\r\n\r\n所以，拼团是比团购好几百倍的快速裂变营销方式。 美团和大众联姻，可以看到团购网站的潜力相当大，而拼团有极大可能性会颠覆团购模式，它的价值正在被业界所认同，是团购的2.0时代。'),(2,'888888','123456',2,'香楚长粒米5斤大米','鱼米之乡的臻品','http://118.190.148.138:8088/vpaiApp/headPhoto/ffffffff-adfe-dfee-85f0-43b77d89fee5.jpg','30','0',2,NULL,NULL,NULL,NULL),(3,'888888','123456',3,'龙泉湖散养咸鸭蛋','山水有灵万物生','http://118.190.148.138:8088/vpaiApp/headPhoto/ffffffff-dbec-e9f5-ffff-ffffcc847df6.jpg','29','0',14,NULL,NULL,NULL,NULL),(4,'888888','123456',4,'陕西特产新鲜时令水果阎良甜瓜4粒装','香甜','http://118.190.148.138:8088/vpaiApp/headPhoto/63735616810327736331521087443116.jpg','30','0',5,NULL,NULL,NULL,NULL);

/*Table structure for table `fight_group_goods_picture` */

DROP TABLE IF EXISTS `fight_group_goods_picture`;

CREATE TABLE `fight_group_goods_picture` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `picture_url` varchar(200) DEFAULT NULL COMMENT '图片url',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='商品图片表';

/*Data for the table `fight_group_goods_picture` */

insert  into `fight_group_goods_picture`(`id`,`platform_id`,`mer_id`,`goods_id`,`picture_url`) values (1,'888888','123456',1,'http://118.190.148.138:8088/vpaiApp/headPhoto/63753216449230929931521516207507.jpg'),(2,'888888','123456',1,'http://118.190.148.138:8088/vpaiApp/headPhoto/63536517549066813441516336956396.jpg'),(3,'888888','123456',1,'http://118.190.148.138:8088/vpaiApp/headPhoto/63550997072655155201516707447018.jpg');

/*Table structure for table `fight_group_info` */

DROP TABLE IF EXISTS `fight_group_info`;

CREATE TABLE `fight_group_info` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `group_id` int(10) DEFAULT NULL COMMENT '团id',
  `groups_id` int(10) DEFAULT NULL COMMENT '团类目id',
  `goods_id` int(10) DEFAULT NULL COMMENT '商品id',
  `joined_group_num` int(10) DEFAULT NULL COMMENT '已参团人数',
  `head_per_id` varchar(30) DEFAULT NULL COMMENT '团长用户账号',
  `end_date` date DEFAULT NULL COMMENT '截止日期',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='团信息表';

/*Data for the table `fight_group_info` */

insert  into `fight_group_info`(`id`,`platform_id`,`mer_id`,`group_id`,`groups_id`,`goods_id`,`joined_group_num`,`head_per_id`,`end_date`,`create_time`) values (1,'888888','123456',1,4,1,2,'66666','2018-07-19','2018-07-12 13:53:26'),(2,'888888','123456',2,3,1,3,'11111','2018-07-20','2018-07-12 15:27:53'),(3,'888888','123456',11111,2,1,1,'66666','2018-07-20','2018-07-13 19:43:14'),(5,'888888','123456',12515,2,1,2,'66666','2018-07-20','2018-07-13 20:00:32'),(6,'888888','123456',376254464,1,1,2,'13563439881203712','2018-07-30','2018-07-23 10:06:02'),(7,'888888','123456',900804608,3,1,1,'13827407778492416','2018-08-07','2018-07-31 10:01:17');

/*Table structure for table `fight_group_member_info` */

DROP TABLE IF EXISTS `fight_group_member_info`;

CREATE TABLE `fight_group_member_info` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `group_id` int(10) DEFAULT NULL COMMENT '团id',
  `per_id` varchar(30) DEFAULT NULL COMMENT '用户账号',
  `name` varchar(20) DEFAULT NULL COMMENT '用户名称',
  `user_head_portrait` text COMMENT '用户头像',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='团成员信息表';

/*Data for the table `fight_group_member_info` */

insert  into `fight_group_member_info`(`id`,`platform_id`,`mer_id`,`group_id`,`per_id`,`name`,`user_head_portrait`,`create_time`) values (1,'888888','123456',1,'66666','江枫瑶影','http://118.190.148.138:8088/vpaiApp/headPhoto/63680814940023685121519777262483.jpg','2018-07-12 17:16:03'),(2,'888888','123456',2,'11111','张三','http://118.190.148.138:8088/vpaiApp/headPhoto/63550899821886832651516680968788.jpg','2018-07-12 17:16:09'),(3,'888888','123456',1,'123654','李四','http://118.190.148.138:8088/vpaiApp/headPhoto/63677385314391818251519698779834.jpg','2018-07-13 17:15:53'),(23,'888888','123456',2,'66666','江枫瑶影','http://118.190.148.138:8088/vpaiApp/headPhoto/63680814940023685121519777262483.jpg','2018-07-13 18:12:47'),(30,'888888','123456',11111,'66666','江枫瑶影','http://118.190.148.138:8088/vpaiApp/headPhoto/63680814940023685121519777262483.jpg','2018-07-13 19:43:14'),(32,'888888','123456',12515,'66666','江枫瑶影','http://118.190.148.138:8088/vpaiApp/headPhoto/63680814940023685121519777262483.jpg','2018-07-13 20:00:32'),(33,'888888','123456',376254464,'13563439881203712','江枫瑶影','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJicd5aiaTgyOrxU2WmELKFwegMPTs6FfVcoYSSwBcibn2NZ2iaHVRSiaic5cTDnricOATfRr9q1XKTh4Smw/132','2018-07-23 10:06:03'),(34,'888888','123456',2,'13563439881203712','江枫瑶影','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJicd5aiaTgyOrxU2WmELKFwegMPTs6FfVcoYSSwBcibn2NZ2iaHVRSiaic5cTDnricOATfRr9q1XKTh4Smw/132','2018-07-23 10:06:27'),(35,'888888','123456',376254464,'13827407778492416','江枫瑶影','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJicd5aiaTgyOrxU2WmELKFwegMPTs6FfVcoYSSwBcibn2NZ2iaHVRSiaic5cTDnricOATfRr9q1XKTh4Smw/132','2018-07-31 09:55:56'),(36,'888888','123456',900804608,'13827407778492416','江枫瑶影','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJicd5aiaTgyOrxU2WmELKFwegMPTs6FfVcoYSSwBcibn2NZ2iaHVRSiaic5cTDnricOATfRr9q1XKTh4Smw/132','2018-07-31 10:01:17'),(37,'888888','123456',12515,'25245151','江枫瑶影','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJicd5aiaTgyOrxU2WmELKFwegMPTs6FfVcoYSSwBcibn2NZ2iaHVRSiaic5cTDnricOATfRr9q1XKTh4Smw/132','2018-07-31 10:25:13');

/*Table structure for table `freeze_journal` */

DROP TABLE IF EXISTS `freeze_journal`;

CREATE TABLE `freeze_journal` (
  `journal_id` varchar(32) NOT NULL COMMENT '流水号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_type` varchar(6) NOT NULL COMMENT '客户类型    CUST00：平台   CUST01：商户  CUST02：用户',
  `customer_no` varchar(32) DEFAULT NULL COMMENT '个人或商户编号',
  `trade_type` varchar(2) NOT NULL COMMENT '交易类型  01：充值，02：提现，',
  `order_no` varchar(32) NOT NULL COMMENT '订单号',
  `freeze_balance` decimal(18,2) NOT NULL COMMENT '冻结金额',
  `status` varchar(6) NOT NULL COMMENT '冻结状态 1：已冻结  2：部分解冻  3：已解冻',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`journal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='冻结流水表';

/*Data for the table `freeze_journal` */

/*Table structure for table `gateway_api_define` */

DROP TABLE IF EXISTS `gateway_api_define`;

CREATE TABLE `gateway_api_define` (
  `id` varchar(50) NOT NULL,
  `path` varchar(255) NOT NULL,
  `service_id` varchar(50) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `retryable` tinyint(1) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `strip_prefix` int(11) DEFAULT NULL,
  `api_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `gateway_api_define` */

insert  into `gateway_api_define`(`id`,`path`,`service_id`,`url`,`retryable`,`enabled`,`strip_prefix`,`api_name`) values ('1','/admin/**','flyray-admin','',0,1,1,'back'),('2','/auth/**','flyray-auth','',0,1,1,'auth'),('3','/crm/**','flyray-crm-core',NULL,0,1,1,'crmapi'),('4','/biz/**','flyray-biz',NULL,0,1,1,'bizapi'),('5','/adminCrm/**','flyray-crm-core',NULL,0,1,1,'admincrmapi');

/*Table structure for table `job_execution_log` */

DROP TABLE IF EXISTS `job_execution_log`;

CREATE TABLE `job_execution_log` (
  `id` varchar(40) NOT NULL,
  `job_name` varchar(100) NOT NULL,
  `task_id` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `sharding_item` int(11) NOT NULL,
  `execution_source` varchar(20) NOT NULL,
  `failure_cause` varchar(4000) DEFAULT NULL,
  `is_success` int(11) NOT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `complete_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `job_execution_log` */

/*Table structure for table `job_status_trace_log` */

DROP TABLE IF EXISTS `job_status_trace_log`;

CREATE TABLE `job_status_trace_log` (
  `id` varchar(40) NOT NULL,
  `job_name` varchar(100) NOT NULL,
  `original_task_id` varchar(255) NOT NULL,
  `task_id` varchar(255) NOT NULL,
  `slave_id` varchar(50) NOT NULL,
  `source` varchar(50) NOT NULL,
  `execution_type` varchar(20) NOT NULL,
  `sharding_item` varchar(100) NOT NULL,
  `state` varchar(20) NOT NULL,
  `message` text,
  `creation_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `TASK_ID_STATE_INDEX` (`task_id`(191),`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `job_status_trace_log` */

/*Table structure for table `merchant_account` */

DROP TABLE IF EXISTS `merchant_account`;

CREATE TABLE `merchant_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(32) NOT NULL COMMENT '账户编号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `merchant_id` varchar(32) DEFAULT NULL COMMENT '商户编号',
  `customer_type` varchar(6) NOT NULL COMMENT '客户类型    CUST00：平台   CUST01：商户  CUST02：用户',
  `account_type` varchar(10) NOT NULL COMMENT '账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：冻结金额账户，ACC008：平台管理费账户，ACC009：平台服务费账户等......',
  `ccy` varchar(6) DEFAULT NULL COMMENT '币种  CNY：人民币',
  `account_balance` decimal(18,2) DEFAULT NULL COMMENT '账户余额',
  `freeze_balance` decimal(18,2) DEFAULT NULL COMMENT '冻结金额',
  `check_sum` varchar(40) DEFAULT NULL COMMENT '校验码（余额加密值）',
  `status` varchar(2) DEFAULT NULL COMMENT '账户状态 00：正常，01：冻结',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商户账户信息';

/*Data for the table `merchant_account` */

/*Table structure for table `merchant_account_journal` */

DROP TABLE IF EXISTS `merchant_account_journal`;

CREATE TABLE `merchant_account_journal` (
  `journal_id` varchar(32) NOT NULL COMMENT '流水号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `merchant_id` varchar(32) DEFAULT NULL COMMENT '商户编号',
  `account_id` varchar(32) DEFAULT NULL COMMENT '账户编号',
  `account_type` varchar(10) DEFAULT NULL COMMENT '账户类型    ACC001：余额账户',
  `order_no` varchar(32) DEFAULT NULL COMMENT '订单号',
  `in_out_flag` varchar(2) DEFAULT NULL COMMENT '来往标志  1：来账   2：往账 ',
  `trade_amt` decimal(18,2) DEFAULT NULL COMMENT '交易金额',
  `trade_type` varchar(2) DEFAULT NULL COMMENT '交易类型  01：充值，02：提现，',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`journal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商户账户流水（充、转、提、退、冻结流水）';

/*Data for the table `merchant_account_journal` */

/*Table structure for table `merchant_base` */

DROP TABLE IF EXISTS `merchant_base`;

CREATE TABLE `merchant_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` varchar(32) NOT NULL COMMENT '商户编号',
  `merchant_name` varchar(60) DEFAULT NULL COMMENT '商户名称',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '用户编号',
  `third_no` varchar(32) DEFAULT NULL COMMENT '第三方商户编号',
  `parent_mer_id` varchar(32) DEFAULT NULL COMMENT '父级商户编号',
  `mer_type` varchar(4) DEFAULT NULL COMMENT '商户类型  00：普通商户  01：子商户 ',
  `company_name` varchar(100) DEFAULT NULL COMMENT '企业名称',
  `business_scope` text COMMENT '经营范围',
  `business_no` varchar(60) DEFAULT NULL COMMENT '工商注册号',
  `legal_person_name` varchar(32) DEFAULT NULL COMMENT '法人姓名',
  `legal_person_cred_type` varchar(6) DEFAULT NULL COMMENT '法人证件类型  0-身份证；1-户口本；2-护照；3-军官证；4-士兵证；5-港澳居民往来内地通行证；6-台湾通报往来内地通行证；7-临时身份证；8-外国人居留证；9-警官证；x-其他证件 ',
  `legal_person_cred_no` varchar(32) DEFAULT NULL COMMENT '法人证件号码',
  `business_licence` text COMMENT '营业执照',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `mobile` varchar(32) DEFAULT NULL COMMENT '企业座机',
  `fax` varchar(32) DEFAULT NULL COMMENT '企业传真',
  `http_address` text COMMENT '企业网址',
  `registered_capital` decimal(18,2) DEFAULT NULL COMMENT '注册资金',
  `company_address` text COMMENT '企业地址',
  `authentication_status` varchar(6) DEFAULT NULL COMMENT '认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败',
  `status` varchar(2) DEFAULT NULL COMMENT '账户状态 00：正常，01：冻结',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `owner` bigint(20) DEFAULT NULL COMMENT '归属人（存后台管理系统登录人员id）指谁发展的客户',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Key_merchant_name` (`merchant_name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='商户客户基础信息';

/*Data for the table `merchant_base` */

insert  into `merchant_base`(`id`,`merchant_id`,`merchant_name`,`platform_id`,`customer_id`,`third_no`,`parent_mer_id`,`mer_type`,`company_name`,`business_scope`,`business_no`,`legal_person_name`,`legal_person_cred_type`,`legal_person_cred_no`,`business_licence`,`phone`,`mobile`,`fax`,`http_address`,`registered_capital`,`company_address`,`authentication_status`,`status`,`create_time`,`update_time`,`owner`) values (2,'13583565112291328','123','p1001',NULL,NULL,NULL,NULL,'说法阿萨德','撒地方撒阿萨德','54656453641321','玩儿去安抚','0','852741963456789',NULL,'阿斯顿发生','恶趣味若','未确认','未确认发','500000.00','撒地方','00','00','2018-07-19 15:00:54','2018-07-19 18:10:56',0),(3,'13609744399413248','mer001','ALL',NULL,NULL,NULL,NULL,'sadf','sadf','sdafsdafsadfas','sdf','0','sadfsdafasd',NULL,'sdfsadfdas','sadfsadfsad','sadfdsafsa','asdfsad','1000.00','sadfsadf','00','00','2018-07-20 18:45:20','2018-07-20 18:45:20',0),(5,'13609750871486464','mer002','ALL',NULL,NULL,NULL,NULL,'sadf','sadf','sdafsdafsadfas','sdf','0','sadfsdafasd',NULL,'sdfsadfdas','sadfsadfsad','sadfdsafsa','asdfsad','1000.00','sadfsadf','00','00','2018-07-20 18:45:44','2018-07-20 18:45:44',0),(6,'13609813053353984','mer1001','1001',NULL,NULL,NULL,NULL,'sdafs','sdafs','sadfsdafsdaf','asdf','0','sdfsdafsadf',NULL,'sadf','sadf','asdf','asdf','1000.00','asdf','00','00','2018-07-20 18:49:42','2018-07-20 18:49:42',0),(7,'13672755642576896','飞雷开源','13672505153499136',NULL,NULL,NULL,NULL,'飞雷开源','2323232','2323232232323','2323','1','23232',NULL,'2323223','2323','2323','23','23.00','232323','00','00','2018-07-23 13:31:30','2018-07-23 13:31:30',0);

/*Table structure for table `pay_channel` */

DROP TABLE IF EXISTS `pay_channel`;

CREATE TABLE `pay_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `pay_company_no` varchar(32) NOT NULL COMMENT '支付公司编号',
  `pay_company_name` varchar(60) DEFAULT NULL COMMENT '支付公司名称',
  `pay_channel_no` varchar(32) NOT NULL COMMENT '支付通道编号',
  `pay_channel_name` varchar(60) DEFAULT NULL COMMENT '支付通道名称',
  `trande_type` varchar(6) DEFAULT NULL COMMENT '交易类型（仅做展示）  00：支付  01：退款  02：代收  03：代付',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付通道配置';

/*Data for the table `pay_channel` */

/*Table structure for table `pay_channel_config` */

DROP TABLE IF EXISTS `pay_channel_config`;

CREATE TABLE `pay_channel_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户号',
  `pay_channel_no` varchar(32) NOT NULL COMMENT '支付通道编号',
  `customer_type` varchar(6) DEFAULT NULL COMMENT '客户类型    CUST00：平台   CUST01：商户  CUST02：用户',
  `fee_code` varchar(32) DEFAULT NULL COMMENT '费率代码',
  `out_mer_no` varchar(32) DEFAULT NULL COMMENT '第三方商户号 APPID',
  `out_sub_mer_no` varchar(32) DEFAULT NULL COMMENT '第三方子商户号',
  `out_mer_account` varchar(32) DEFAULT NULL COMMENT '第三方账号',
  `out_mer_public_key` text COMMENT '第三方公钥',
  `out_mer_private_key` text COMMENT '第三方私钥',
  `encryption_method` varchar(6) DEFAULT NULL COMMENT '加密方式 MD5 RSA',
  `key_path` text COMMENT '证书地址',
  `key_pwd` varchar(32) DEFAULT NULL COMMENT '证书密码',
  `status` varchar(6) DEFAULT NULL COMMENT '状态  0：开启   1：关闭',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台（商户）支付通道配置';

/*Data for the table `pay_channel_config` */

/*Table structure for table `pay_channel_fee_config` */

DROP TABLE IF EXISTS `pay_channel_fee_config`;

CREATE TABLE `pay_channel_fee_config` (
  `fee_code` varchar(32) NOT NULL COMMENT '费率代码',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `fee_name` varchar(60) DEFAULT NULL COMMENT '费率名称',
  `ccy` varchar(10) DEFAULT NULL COMMENT '币种',
  `billing_method` varchar(6) NOT NULL COMMENT '计费方式',
  `fee_amt` decimal(18,2) DEFAULT NULL COMMENT '收费金额',
  `fee_ratio` decimal(9,3) DEFAULT NULL COMMENT '收费比例',
  `min_fee` decimal(18,2) DEFAULT NULL COMMENT '最低收费',
  `max_fee` decimal(18,2) DEFAULT NULL COMMENT '最高收费',
  PRIMARY KEY (`fee_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付通道费率配置表';

/*Data for the table `pay_channel_fee_config` */

/*Table structure for table `pay_channel_interface` */

DROP TABLE IF EXISTS `pay_channel_interface`;

CREATE TABLE `pay_channel_interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `pay_channel_no` varchar(32) NOT NULL COMMENT '支付通道编号',
  `trande_type` varchar(6) DEFAULT NULL COMMENT '交易类型（仅做展示）  00：支付  01：退款  02：代收  03：代付',
  `service_name` varchar(100) NOT NULL COMMENT '接口名称',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付通道接口配置';

/*Data for the table `pay_channel_interface` */

/*Table structure for table `pay_for_another_order` */

DROP TABLE IF EXISTS `pay_for_another_order`;

CREATE TABLE `pay_for_another_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `order_id` varchar(40) DEFAULT NULL COMMENT '代付订单号',
  `out_order_no` varchar(40) DEFAULT NULL COMMENT '外部订单号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `per_id` varchar(60) DEFAULT NULL COMMENT '客户账号',
  `mer_id` varchar(60) DEFAULT NULL COMMENT '商户账号',
  `amount` decimal(18,2) DEFAULT NULL COMMENT '代付金额',
  `fee` decimal(18,2) DEFAULT NULL COMMENT '代付手续费',
  `bank_code` varchar(10) DEFAULT NULL COMMENT '银行编码',
  `bank_account_no` varchar(255) DEFAULT NULL COMMENT '银行卡号密文',
  `bank_account_name` varchar(50) DEFAULT NULL COMMENT '银行账户名',
  `summary` varchar(30) DEFAULT NULL COMMENT '备注',
  `bank_union_code` varchar(50) DEFAULT NULL COMMENT '银行联行号',
  `create_time` datetime DEFAULT NULL COMMENT '代付时间',
  `tx_status` varchar(2) DEFAULT NULL COMMENT '代付状态 00成功 01失败 02代付处理中 03代付申请成功',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='代付订单表';

/*Data for the table `pay_for_another_order` */

/*Table structure for table `pay_order` */

DROP TABLE IF EXISTS `pay_order`;

CREATE TABLE `pay_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pay_order_no` varchar(60) NOT NULL COMMENT ' 订单号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `per_id` varchar(60) DEFAULT NULL COMMENT '客户账号',
  `mer_id` varchar(60) DEFAULT NULL COMMENT '商户账号',
  `pay_time` datetime DEFAULT NULL COMMENT '交易时间',
  `order_amt` decimal(18,2) DEFAULT NULL COMMENT '订单金额',
  `pay_amt` decimal(18,2) DEFAULT NULL COMMENT '实际交易金额',
  `pay_fee` decimal(18,2) DEFAULT NULL COMMENT '交易手续费',
  `body` text COMMENT '订单描述',
  `pay_code` text COMMENT '交易类型 1支付 2充值',
  `pay_method` varchar(32) DEFAULT NULL COMMENT '支付方式（01支付宝 02微信）',
  `order_status` varchar(10) DEFAULT NULL COMMENT '交易状态（00支付成功 01支付失败 02待支付 03支付中 04作废）',
  `refunded_amt` decimal(18,2) DEFAULT NULL COMMENT ' 已退款金额',
  `ext_value` text COMMENT '扩展map',
  PRIMARY KEY (`id`,`pay_order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8 COMMENT='支付订单表';

/*Data for the table `pay_order` */

/*Table structure for table `pay_serial` */

DROP TABLE IF EXISTS `pay_serial`;

CREATE TABLE `pay_serial` (
  `serial_no` varchar(32) NOT NULL COMMENT '支付流水号',
  `pay_order_no` varchar(32) DEFAULT '' COMMENT '订单号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(60) DEFAULT NULL COMMENT '商户账号',
  `pay_channel_no` varchar(32) DEFAULT NULL COMMENT '支付渠道号',
  `pay_company_no` varchar(32) DEFAULT NULL COMMENT '支付公司编号',
  `pay_amt` decimal(18,2) DEFAULT NULL COMMENT '交易金额',
  `pay_fee` decimal(18,0) DEFAULT NULL COMMENT '交易手续费',
  `pay_status` varchar(10) DEFAULT NULL COMMENT '支付状态（00支付成功 01支付失败 02待支付 03支付中 04作废）',
  PRIMARY KEY (`serial_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付流水表';

/*Data for the table `pay_serial` */

/*Table structure for table `personal_account` */

DROP TABLE IF EXISTS `personal_account`;

CREATE TABLE `personal_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(32) NOT NULL COMMENT '账户编号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `personal_id` varchar(32) NOT NULL COMMENT '个人信息编号',
  `account_type` varchar(10) NOT NULL COMMENT '账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：冻结金额账户，ACC008：平台管理费账户，ACC009：平台服务费账户等......',
  `ccy` varchar(6) DEFAULT NULL COMMENT '币种  CNY：人民币',
  `account_balance` decimal(18,2) DEFAULT NULL COMMENT '账户余额',
  `freeze_balance` decimal(18,2) DEFAULT NULL COMMENT '冻结金额',
  `check_sum` varchar(40) DEFAULT NULL COMMENT '校验码（余额加密值）',
  `status` varchar(2) DEFAULT NULL COMMENT '账户状态 00：正常，01：冻结',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人账户信息';

/*Data for the table `personal_account` */

/*Table structure for table `personal_account_journal` */

DROP TABLE IF EXISTS `personal_account_journal`;

CREATE TABLE `personal_account_journal` (
  `journal_id` varchar(32) NOT NULL COMMENT '流水号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `personal_id` varchar(32) NOT NULL COMMENT '个人信息编号',
  `account_id` varchar(32) NOT NULL COMMENT '账户编号',
  `account_type` varchar(10) NOT NULL COMMENT '账户类型    ACC001：余额账户',
  `order_no` varchar(32) DEFAULT NULL COMMENT '订单号',
  `in_out_flag` varchar(2) DEFAULT NULL COMMENT '来往标志  1：来账   2：往账',
  `trade_amt` decimal(18,2) DEFAULT NULL COMMENT '交易金额',
  `trade_type` varchar(2) DEFAULT NULL COMMENT '交易类型  01：充值，02：提现，',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`journal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人账户流水（充、转、提、退、冻结流水）';

/*Data for the table `personal_account_journal` */

/*Table structure for table `personal_base` */

DROP TABLE IF EXISTS `personal_base`;

CREATE TABLE `personal_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personal_id` varchar(32) NOT NULL COMMENT '个人客户编号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '用户编号',
  `third_no` varchar(32) DEFAULT NULL COMMENT '第三方会员编号',
  `real_name` varchar(20) DEFAULT NULL COMMENT '用户名称',
  `id_number` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `nick_name` varchar(32) DEFAULT NULL COMMENT '用户昵称',
  `sex` varchar(1) DEFAULT NULL COMMENT '性别 1：男 2：女',
  `birthday` varchar(10) DEFAULT NULL COMMENT '生日',
  `address` varchar(256) DEFAULT NULL COMMENT '居住地',
  `hometown` varchar(256) DEFAULT NULL COMMENT '家乡',
  `id_positive` text COMMENT '身份证正面',
  `id_negative` text COMMENT '身份证反面',
  `authentication_status` varchar(6) DEFAULT NULL COMMENT '认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败',
  `status` varchar(2) DEFAULT NULL COMMENT '账户状态 00：正常，01：冻结',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `owner` bigint(20) DEFAULT NULL COMMENT '归属人（存后台管理系统登录人员id）指谁发展的客户',
  `avatar` text COMMENT '头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COMMENT='个人客户基础信息';

/*Data for the table `personal_base` */

insert  into `personal_base`(`id`,`personal_id`,`platform_id`,`customer_id`,`third_no`,`real_name`,`id_number`,`nick_name`,`sex`,`birthday`,`address`,`hometown`,`id_positive`,`id_negative`,`authentication_status`,`status`,`create_time`,`update_time`,`owner`,`avatar`) values (1,'13580266776637440','p1001','',NULL,'ceshi001','963852741963852','昵称001','1','2018-07-02','dsaf','sdaf',NULL,NULL,'00','00','2018-07-19 11:31:12','2018-07-19 11:33:17',0,NULL),(6,'13669455777050624','888888','13669185209577472',NULL,NULL,NULL,'saving',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-23 10:01:45','2018-07-23 10:01:45',0,'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLpOI28iaGx0KpOZBbEad3VypWVh3AOQb9ObvVjkMib9BE1ajWNnXicL71Hxkk6h7FJgF5THonXqxwbw/132'),(7,'13672553024663552','13672505153499136',NULL,NULL,'累计问','121213213232323','232323','1','2018-07-04','232323','23232',NULL,NULL,'00','00','2018-07-23 13:18:37','2018-07-23 13:18:37',0,NULL),(8,'13672609002106880','13672505153499136',NULL,NULL,'累计问','121213213232323','232323','1','2018-07-04','232323','23232',NULL,NULL,'00','00','2018-07-23 13:22:10','2018-07-23 13:22:10',0,NULL),(9,'13672675480252416','13672505153499136',NULL,NULL,'雷继文','23232323232322323','232323','1','2018-07-11','2323','232323',NULL,NULL,'00','00','2018-07-23 13:26:24','2018-07-23 13:26:24',0,NULL),(10,'13672691156725760','13672505153499136',NULL,NULL,'雷继文','23232424223323232','22323','1','2018-07-19','2323','23232',NULL,NULL,'00','00','2018-07-23 13:27:24','2018-07-23 13:27:24',0,NULL),(11,'13672716341161984','13672505153499136',NULL,NULL,'雷继文','2323232323232323','2322323','1','2018-07-04','23232323','232323233',NULL,NULL,'00','00','2018-07-23 13:29:00','2018-07-23 13:29:00',0,NULL),(12,'13743175853486080','13672505153499136',NULL,NULL,'23232','23232323223232323','23232','1','2018-07-05','23232','23232',NULL,NULL,'00','00','2018-07-26 16:08:41','2018-07-26 16:08:41',0,NULL),(13,'13759007728152576','13672505153499136',NULL,NULL,'34343434','343434343434344','34343434','1','2018-07-09','3434','343434',NULL,NULL,'00','00','2018-07-27 08:55:15','2018-07-27 08:55:15',NULL,NULL),(14,'13763816686956544','13672505153499136',NULL,NULL,'yonghu001','1234567890123456','sadfs','1','2018-07-12','sdaf','sadf',NULL,NULL,'00','00','2018-07-27 14:00:59','2018-07-27 14:00:59',0,NULL),(15,'13763899138584576','13672505153499136',NULL,NULL,'ceshi','1234567891234567','sadfs','1','2018-07-02','awetr','wer',NULL,NULL,'00','00','2018-07-27 14:06:14','2018-07-27 14:06:14',13672505364787200,NULL),(16,'13763899194683392','13672505153499136',NULL,NULL,'ceshi','1234567891234567','sadfs','1','2018-07-02','awetr','wer',NULL,NULL,'00','00','2018-07-27 14:06:14','2018-07-27 14:06:14',13672505364787200,NULL),(17,'13763899248947200','13672505153499136',NULL,NULL,'ceshi','1234567891234567','sadfs','1','2018-07-02','awetr','wer',NULL,NULL,'00','00','2018-07-27 14:06:14','2018-07-27 14:06:14',13672505364787200,NULL),(18,'13763899289579520','13672505153499136',NULL,NULL,'ceshi','1234567891234567','sadfs','1','2018-07-02','awetr','wer',NULL,NULL,'00','00','2018-07-27 14:06:15','2018-07-27 14:06:15',13672505364787200,NULL),(19,'13763899333619712','13672505153499136',NULL,NULL,'ceshi','1234567891234567','sadfs','1','2018-07-02','awetr','wer',NULL,NULL,'00','00','2018-07-27 14:06:15','2018-07-27 14:06:15',13672505364787200,NULL),(20,'13763899373727744','13672505153499136',NULL,NULL,'ceshi','1234567891234567','sadfs','1','2018-07-02','awetr','wer',NULL,NULL,'00','00','2018-07-27 14:06:15','2018-07-27 14:06:15',13672505364787200,NULL),(21,'13763899432972288','13672505153499136',NULL,NULL,'ceshi','1234567891234567','sadfs','1','2018-07-02','awetr','wer',NULL,NULL,'00','00','2018-07-27 14:06:15','2018-07-27 14:06:15',13672505364787200,NULL),(22,'13763899522363392','13672505153499136',NULL,NULL,'ceshi','1234567891234567','sadfs','1','2018-07-02','awetr','wer',NULL,NULL,'00','00','2018-07-27 14:06:15','2018-07-27 14:06:15',13672505364787200,NULL),(23,'13763899554869248','13672505153499136',NULL,NULL,'ceshi','1234567891234567','sadfs','1','2018-07-02','awetr','wer',NULL,NULL,'00','00','2018-07-27 14:06:15','2018-07-27 14:06:15',13672505364787200,NULL),(24,'13827407778492416','888888','13827407778492416',NULL,NULL,NULL,'江枫瑶影',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 09:25:30','2018-07-30 09:25:30',0,'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJicd5aiaTgyOrxU2WmELKFwegMPTs6FfVcoYSSwBcibn2NZ2iaHVRSiaic5cTDnricOATfRr9q1XKTh4Smw/132'),(25,'13827536479137792','888888','13827536479137792',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 09:33:41','2018-07-30 09:33:41',0,NULL),(26,'13827573737140224','888888','13827573737140224',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 09:36:02','2018-07-30 09:36:02',0,NULL),(27,'13827589197082624','888888','13827589197082624',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 09:37:01','2018-07-30 09:37:01',0,NULL),(28,'13827661895380992','888888','13827661895380992',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 09:41:38','2018-07-30 09:41:38',0,NULL),(29,'13827717201473536','888888','13827717201473536',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 09:45:10','2018-07-30 09:45:10',0,NULL),(30,'13827785178034176','888888','13827785178034176',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 09:49:28','2018-07-30 09:49:28',0,NULL),(31,'13827830022221824','888888','13827830022221824',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 09:52:19','2018-07-30 09:52:19',0,NULL),(32,'13827909724745728','888888','13827909724745728',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 09:57:23','2018-07-30 09:57:23',0,NULL),(33,'13828061253152768','888888','13828061253152768',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 10:07:01','2018-07-30 10:07:01',0,NULL),(34,'13828069419462656','888888','13828069419462656',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 10:07:33','2018-07-30 10:07:33',0,NULL),(35,'13828148586164224','888888','13828148586164224',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 10:12:35','2018-07-30 10:12:35',0,NULL),(36,'13828459282116608','888888','13828459282116608',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 10:32:21','2018-07-30 10:32:21',0,NULL),(37,'13828585308368896','888888','13828585308368896',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 10:40:21','2018-07-30 10:40:21',0,NULL),(38,'13828622816194560','888888','13828622816194560',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 10:42:44','2018-07-30 10:42:44',0,NULL),(39,'13828664573898752','888888','13828664573898752',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 10:45:23','2018-07-30 10:45:23',0,NULL),(40,'13828755331559424','888888','13828755331559424',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 10:51:10','2018-07-30 10:51:10',0,NULL),(41,'13828913203589120','888888','13828913203589120',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 11:01:12','2018-07-30 11:01:12',0,NULL),(42,'13832457315233792','888888','13832457315233792',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 14:46:31','2018-07-30 14:46:31',0,NULL),(43,'13832519415050240','888888','13832519415050240',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 14:50:28','2018-07-30 14:50:28',0,NULL),(44,'13832622973464576','888888','13832622973464576',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 14:57:03','2018-07-30 14:57:03',0,NULL),(45,'13832653049245696','888888','13832653049245696',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 14:58:57','2018-07-30 14:58:57',0,NULL),(46,'13832689322897408','888888','13832689322897408',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 15:01:16','2018-07-30 15:01:16',0,NULL),(47,'13833305011335168','888888','13833305011335168',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 15:40:25','2018-07-30 15:40:25',0,NULL),(48,'13833365900832768','888888','13833365900832768',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 15:44:17','2018-07-30 15:44:17',0,NULL),(49,'13833387660881920','888888','13833387660881920',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 15:45:40','2018-07-30 15:45:40',0,NULL),(50,'13833581990588416','888888','13833581990588416',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 15:58:01','2018-07-30 15:58:01',0,NULL),(51,'13833932550778880','888888','13833932550778880',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 16:20:19','2018-07-30 16:20:19',0,NULL),(52,'13833941894377472','888888','13833941894377472',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 16:20:54','2018-07-30 16:20:54',0,NULL),(53,'13833943740395520','888888','13833943740395520',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 16:21:01','2018-07-30 16:21:01',0,NULL),(54,'13833946810626048','888888','13833946810626048',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 16:21:13','2018-07-30 16:21:13',0,NULL),(55,'13834698040356864','888888','13834698040356864',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 17:08:59','2018-07-30 17:08:59',0,NULL),(56,'13834721594519552','888888','13834721594519552',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 17:10:29','2018-07-30 17:10:29',0,NULL),(57,'13835093835591680','888888','13835093835591680',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 17:34:08','2018-07-30 17:34:08',0,NULL),(58,'13835256869761024','888888','13835256869761024',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 17:44:30','2018-07-30 17:44:30',0,NULL),(59,'13835355240607744','888888','13835355240607744',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 17:50:45','2018-07-30 17:50:45',0,NULL),(60,'13835365539196928','888888','13835365539196928',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 17:51:25','2018-07-30 17:51:25',0,NULL),(61,'13835412854091776','888888','13835412854091776',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 17:54:25','2018-07-30 17:54:25',0,NULL),(62,'13835546533113856','888888','13835546533113856',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 18:02:55','2018-07-30 18:02:55',0,NULL),(63,'13835652914556928','888888','13835652914556928',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 18:09:41','2018-07-30 18:09:41',0,NULL),(64,'13835694847897600','888888','13835694847897600',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 18:12:21','2018-07-30 18:12:21',0,NULL),(65,'13835705899102208','888888','13835705899102208',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 18:13:03','2018-07-30 18:13:03',0,NULL),(66,'13835707841851392','888888','13835707841851392',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 18:13:10','2018-07-30 18:13:10',0,NULL),(67,'13835985038422016','888888','13835985038422016',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-30 18:30:48','2018-07-30 18:30:48',0,NULL),(68,'13849715700215808','888888','13849715700215808',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-31 09:03:51','2018-07-31 09:03:51',0,NULL),(69,'13849793400745984','888888','13849793400745984',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-31 09:08:44','2018-07-31 09:08:44',0,NULL),(70,'13849799080620032','888888','13849799080620032',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-31 09:09:06','2018-07-31 09:09:06',0,NULL),(71,'13850427017998336','888888','13850427017998336',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-31 09:49:01','2018-07-31 09:49:01',0,NULL),(72,'13850441230135296','888888','13850441230135296',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-31 09:49:55','2018-07-31 09:49:55',0,NULL),(73,'13850470925283328','888888','13850470925283328',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-31 09:51:49','2018-07-31 09:51:49',0,NULL),(74,'13854481233817600','888888','13854481233817600',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-31 14:06:48','2018-07-31 14:06:48',0,NULL),(75,'13854552487702528','888888','13854552487702528',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-31 14:11:19','2018-07-31 14:11:19',0,NULL),(76,'13854570706972672','888888','13854570706972672',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-31 14:12:28','2018-07-31 14:12:28',0,NULL),(77,'13854580891791360','888888','13854580891791360',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-31 14:13:07','2018-07-31 14:13:07',0,NULL),(78,'13854601822416896','888888','13854601822416896',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-31 14:14:27','2018-07-31 14:14:27',0,NULL),(79,'13854776954793984','888888','13854776954793984',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-31 14:25:35','2018-07-31 14:25:35',0,NULL);

/*Table structure for table `personal_billing` */

DROP TABLE IF EXISTS `personal_billing`;

CREATE TABLE `personal_billing` (
  `bill_id` varchar(32) NOT NULL COMMENT '账单编号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `personal_id` varchar(32) DEFAULT NULL COMMENT '个人信息编号',
  `account_journal_id` varchar(32) DEFAULT NULL COMMENT '账务流水号',
  `order_no` varchar(32) DEFAULT NULL COMMENT '订单号',
  `bill_type` varchar(6) DEFAULT NULL COMMENT '账单类型    饮食，服装，美容，生活用品，保险，理财',
  `in_out_flag` varchar(4) DEFAULT NULL COMMENT '来往标志  1：来账   2：往账',
  `trade_amt` decimal(18,2) DEFAULT NULL COMMENT '交易金额',
  `seller_id` varchar(32) DEFAULT NULL COMMENT '资金来源编号',
  `seller_type` varchar(4) DEFAULT NULL COMMENT '客户类型    CUST00：平台   CUST01：商户  CUST02：用户   CUST03：第三方',
  `produce_info` varchar(256) DEFAULT NULL COMMENT '商品说明',
  `pay_way` varchar(6) DEFAULT NULL COMMENT '付款方式     账户类型+银行卡',
  `remark` text COMMENT '备注',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人账单';

/*Data for the table `personal_billing` */

/*Table structure for table `personal_distribution_relation` */

DROP TABLE IF EXISTS `personal_distribution_relation`;

CREATE TABLE `personal_distribution_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `personal_id` varchar(32) NOT NULL COMMENT '用户编号',
  `parent_per_id` varchar(32) DEFAULT NULL COMMENT '父级编号',
  `parent_level` int(11) DEFAULT NULL COMMENT '父级级别',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人分销关系';

/*Data for the table `personal_distribution_relation` */

/*Table structure for table `platform_accout_config` */

DROP TABLE IF EXISTS `platform_accout_config`;

CREATE TABLE `platform_accout_config` (
  `id` int(11) NOT NULL COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `customer_type` varchar(6) NOT NULL COMMENT '客户类型    CUST00：平台   CUST01：商户  CUST02：用户',
  `account_type` varchar(6) NOT NULL COMMENT '账户类型  ACC001：余额账户，',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台所支持账户配置';

/*Data for the table `platform_accout_config` */

/*Table structure for table `platform_base` */

DROP TABLE IF EXISTS `platform_base`;

CREATE TABLE `platform_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `platform_name` varchar(100) DEFAULT NULL COMMENT '平台名称',
  `platform_introduction` text COMMENT '平台简介',
  `authentication_status` varchar(6) DEFAULT NULL COMMENT '认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败',
  `operator_id` int(11) DEFAULT NULL COMMENT '最后操作人编号',
  `operator_name` varchar(256) DEFAULT NULL COMMENT '最后操作人名称',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Key_platform_name` (`platform_name`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='平台基础信息';

/*Data for the table `platform_base` */

insert  into `platform_base`(`id`,`platform_id`,`platform_name`,`platform_introduction`,`authentication_status`,`operator_id`,`operator_name`,`create_time`,`update_time`) values (21,'13582420100853760','pk7','1122范德萨发绿绿绿绿绿绿绿绿绿绿绿绿','00',NULL,NULL,'2018-07-19 13:48:05','2018-07-19 13:48:05'),(22,'13582718642237440','pk8','法发顺丰大声道法法师','00',NULL,NULL,'2018-07-19 14:07:03','2018-07-19 14:07:03'),(25,'13586391164858368','李发文','李发文李发文李发文李发文李发文李发文李发文李发文','00',NULL,NULL,'2018-07-19 18:00:33','2018-07-19 18:00:33'),(26,'13587269034258432','pk3','pk3pk3pk3pk3pk3pk3','00',NULL,NULL,'2018-07-19 18:56:25','2018-07-19 18:56:25'),(27,'13587300181422080','pk5','12333333333333333333333333333333333','00',NULL,NULL,'2018-07-19 18:58:22','2018-07-19 18:58:22'),(28,'13605875080310784','pk1','pk1pk1pk1pk1pk1pk1pk1pk1pk1','00',NULL,NULL,'2018-07-20 14:39:21','2018-07-20 14:39:21'),(29,'13605940777267200','pk2','pk2','00',NULL,NULL,'2018-07-20 14:43:42','2018-07-20 14:43:42'),(30,'13605966203138048','pk4','pk4pk4pk4pk4','00',NULL,NULL,'2018-07-20 14:45:06','2018-07-20 14:45:06'),(31,'13605970952138752','pk6','pk6pk6pk6pk6pk6pk6','00',NULL,NULL,'2018-07-20 14:45:24','2018-07-20 14:45:24'),(32,'13606010669576192','pk11','pk11pk11pk11pk11pk11pk11','00',NULL,NULL,'2018-07-20 14:47:56','2018-07-20 14:47:56'),(33,'13606172350033920','pk9','pk9pk9pk9pk9pk9pk9pk9pk9pk9pk9pk9','00',NULL,NULL,'2018-07-20 14:58:12','2018-07-20 14:58:12'),(34,'13606244572278784','pk12','pk12pk12pk12pk12pk12pk12','00',NULL,NULL,'2018-07-20 15:02:48','2018-07-20 15:02:48'),(35,'13606353385369600','pk13','pk13pk13pk13pk13pk13pk13','00',NULL,NULL,'2018-07-20 15:09:43','2018-07-20 15:09:43'),(36,'13608825924956160','flyray','开源平台','00',NULL,NULL,'2018-07-20 17:46:56','2018-07-20 17:46:56'),(37,'13672505153499136','flyray001','开源平台','00',NULL,NULL,'2018-07-23 13:15:34','2018-07-23 13:15:34'),(38,'13879033951498240','刘楠男','客户管理','00',NULL,NULL,'2018-08-01 16:06:18','2018-08-01 16:06:18');

/*Table structure for table `platform_base_extend` */

DROP TABLE IF EXISTS `platform_base_extend`;

CREATE TABLE `platform_base_extend` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `company_name` varchar(100) DEFAULT NULL COMMENT '企业名称',
  `business_scope` text COMMENT '经营范围',
  `business_no` varchar(60) DEFAULT NULL COMMENT '工商注册号',
  `legal_person_name` varchar(32) DEFAULT NULL COMMENT '法人姓名',
  `legal_person_cred_type` varchar(6) DEFAULT NULL COMMENT '法人证件类型  0-身份证；1-户口本；2-护照；3-军官证；4-士兵证；5-港澳居民往来内地通行证；6-台湾通报往来内地通行证；7-临时身份证；8-外国人居留证；9-警官证；x-其他证件 ',
  `legal_person_cred_no` varchar(32) DEFAULT NULL COMMENT '法人证件号码',
  `business_licence` text COMMENT '营业执照',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `mobile` varchar(32) DEFAULT NULL COMMENT '企业座机',
  `fax` varchar(32) DEFAULT NULL COMMENT '企业传真',
  `http_address` text COMMENT '企业网址',
  `registered_capital` decimal(18,2) DEFAULT NULL COMMENT '注册资金',
  `company_address` text COMMENT '企业地址',
  `operator_id` int(11) DEFAULT NULL COMMENT '最后操作人编号',
  `operator_name` varchar(256) DEFAULT NULL COMMENT '最后操作人名称',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台扩展信息';

/*Data for the table `platform_base_extend` */

/*Table structure for table `platform_callback_url` */

DROP TABLE IF EXISTS `platform_callback_url`;

CREATE TABLE `platform_callback_url` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `callback_type` varchar(6) DEFAULT NULL COMMENT '回调类型   00：支付回调  01：退款回调  02：消息通知',
  `callback_url` text COMMENT '回调地址',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台回调地址配置';

/*Data for the table `platform_callback_url` */

/*Table structure for table `platform_safety_config` */

DROP TABLE IF EXISTS `platform_safety_config`;

CREATE TABLE `platform_safety_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `platform_id` varchar(32) NOT NULL COMMENT '平台编号',
  `encryption_type` varchar(6) NOT NULL COMMENT '加密类型  00：报文加密  01：身份证加密  02：银行卡加密  03：登录密码加密  04：支付密码加密',
  `encryption_method` varchar(6) NOT NULL COMMENT '加密方式   MD5 RSA SHA256 ...',
  `salt_key` text COMMENT '密钥（盐值）',
  `public_key` text COMMENT '公钥',
  `private_key` text COMMENT '私钥',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台安全配置信息';

/*Data for the table `platform_safety_config` */

/*Table structure for table `refund_order` */

DROP TABLE IF EXISTS `refund_order`;

CREATE TABLE `refund_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pay_order_no` varchar(32) NOT NULL COMMENT '支付订单号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(60) DEFAULT NULL COMMENT '商户账号',
  `refund_order_no` varchar(32) DEFAULT NULL COMMENT '退款订单号',
  `refund_amt` decimal(18,2) DEFAULT NULL COMMENT '退款金额',
  `refund_fee` decimal(18,2) DEFAULT NULL COMMENT '退款手续费',
  `refund_time` datetime DEFAULT NULL COMMENT '退款时间',
  `refund_reason` text COMMENT '退款原因',
  `refund_method` varchar(10) DEFAULT NULL COMMENT '退款方式（跟支付方式对应）',
  `refund_status` varchar(10) DEFAULT NULL COMMENT '退款状态（00退款成功 01退款失败 02退款处理中）',
  PRIMARY KEY (`id`,`pay_order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='退款订单表';

/*Data for the table `refund_order` */

/*Table structure for table `refund_serial` */

DROP TABLE IF EXISTS `refund_serial`;

CREATE TABLE `refund_serial` (
  `serial_no` varchar(32) NOT NULL COMMENT '退款流水号',
  `refund_order_no` varchar(32) NOT NULL COMMENT '退款订单号',
  `platform_id` varchar(32) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(60) DEFAULT NULL COMMENT '商户账号',
  `pay_channel_no` varchar(32) DEFAULT NULL COMMENT '支付渠道号',
  `pay_company_no` varchar(32) DEFAULT NULL COMMENT '支付公司编号',
  `redund_amt` decimal(18,2) DEFAULT NULL COMMENT '退款金额',
  `refund_fee` decimal(18,2) DEFAULT NULL COMMENT '退款手续费',
  `refund_status` varchar(10) DEFAULT NULL COMMENT '退款状态（00退款成功 01退款失败 02退款处理中）',
  PRIMARY KEY (`serial_no`,`refund_order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退款流水表';

/*Data for the table `refund_serial` */

/*Table structure for table `restaurant_appraisal_detail_info` */

DROP TABLE IF EXISTS `restaurant_appraisal_detail_info`;

CREATE TABLE `restaurant_appraisal_detail_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `per_id` varchar(32) DEFAULT NULL COMMENT '用户账号',
  `dishes_id` int(20) NOT NULL COMMENT '菜品id',
  `user_name` varchar(20) DEFAULT NULL COMMENT '用户名',
  `user_head_portrait` varchar(200) DEFAULT NULL COMMENT '用户头像',
  `appraisal_grade` varchar(1) DEFAULT NULL COMMENT '评价等级 1很满意 2满意 3一般 4不满意',
  `appraisal_info` varchar(30) DEFAULT NULL COMMENT '评价内容',
  `appraisal_time` datetime DEFAULT NULL COMMENT '评论时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='菜品评价明细表';

/*Data for the table `restaurant_appraisal_detail_info` */

insert  into `restaurant_appraisal_detail_info`(`id`,`platform_id`,`mer_id`,`per_id`,`dishes_id`,`user_name`,`user_head_portrait`,`appraisal_grade`,`appraisal_info`,`appraisal_time`) values (1,'888888','123456',NULL,3,'江枫瑶影','http://118.190.148.138:8088/vpaiApp/headPhoto/63793441559774494721522462647517.jpg','1','味道好，量足','2018-07-05 15:30:44'),(2,'888888','123456',NULL,3,'江枫瑶影','http://118.190.148.138:8088/vpaiApp/headPhoto/63793441559774494721522462647517.jpg','1','不错','2018-07-05 15:36:58'),(3,'888888','123456',NULL,3,'江枫瑶影','http://118.190.148.138:8088/vpaiApp/headPhoto/63793441559774494721522462647517.jpg','2','一如既往地好吃','2018-07-05 15:37:42');

/*Table structure for table `restaurant_appraisal_info` */

DROP TABLE IF EXISTS `restaurant_appraisal_info`;

CREATE TABLE `restaurant_appraisal_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `dishes_id` int(20) NOT NULL COMMENT '菜品id',
  `very_satisfied_num` int(20) NOT NULL COMMENT '很满意数量',
  `satisfied_num` int(20) NOT NULL COMMENT '满意数量',
  `general_num` int(20) NOT NULL COMMENT '一般数量',
  `not_satisfied_num` int(20) NOT NULL COMMENT '不满意数量',
  `total_num` int(20) NOT NULL COMMENT '总评价数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='菜品评价信息表';

/*Data for the table `restaurant_appraisal_info` */

insert  into `restaurant_appraisal_info`(`id`,`platform_id`,`mer_id`,`dishes_id`,`very_satisfied_num`,`satisfied_num`,`general_num`,`not_satisfied_num`,`total_num`) values (1,'888888','123456',3,2,1,0,0,3);

/*Table structure for table `restaurant_dishes_category_info` */

DROP TABLE IF EXISTS `restaurant_dishes_category_info`;

CREATE TABLE `restaurant_dishes_category_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_id` int(20) DEFAULT NULL COMMENT '菜品类目id',
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `name` varchar(5) DEFAULT NULL COMMENT '类目名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='菜品类目表';

/*Data for the table `restaurant_dishes_category_info` */

insert  into `restaurant_dishes_category_info`(`id`,`category_id`,`platform_id`,`mer_id`,`name`) values (11,1,'888888','123456','热销'),(12,2,'888888','123456','折扣'),(13,3,'888888','123456','炒菜'),(14,4,'888888','123456','主食'),(15,5,'888888','123456','汤');

/*Table structure for table `restaurant_dishes_info` */

DROP TABLE IF EXISTS `restaurant_dishes_info`;

CREATE TABLE `restaurant_dishes_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dishes_id` bigint(20) DEFAULT NULL COMMENT '菜品id',
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `category_id` varchar(20) DEFAULT NULL COMMENT '类目id',
  `dishes_name` varchar(20) DEFAULT NULL COMMENT '菜品名称',
  `instructions` varchar(500) DEFAULT NULL COMMENT '说明',
  `image_url` varchar(100) DEFAULT NULL COMMENT '图片地址',
  `order_num` int(10) NOT NULL COMMENT '预定数量',
  `price` varchar(10) DEFAULT NULL COMMENT '价格',
  `is_specification` varchar(2) DEFAULT NULL COMMENT '规格开关',
  `type` varchar(2) DEFAULT NULL COMMENT '业务类型 1点餐 2外卖 3公共',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='菜品信息表';

/*Data for the table `restaurant_dishes_info` */

insert  into `restaurant_dishes_info`(`id`,`dishes_id`,`platform_id`,`mer_id`,`category_id`,`dishes_name`,`instructions`,`image_url`,`order_num`,`price`,`is_specification`,`type`) values (11,1,'888888','123456','1','水煮肉片','肉片300克，加油麦菜，蒜苗，山芹炒制而成，口味麻辣，本店招牌菜品','http://118.190.148.138:8088/vpaiApp/headPhoto/63731986331011645441520997328662.jpg',0,'39','','3'),(12,2,'888888','123456','1','酸辣土豆丝','土豆丝450克，可以做酸辣，不辣','http://118.190.148.138:8088/vpaiApp/headPhoto/63677385314391818251519698779834.jpg',0,'16',NULL,'1'),(13,3,'888888','123456','1','麻婆豆腐','嫩豆腐350克，加嫩酥肉末，豆瓣酱炒制而成，口味麻辣鲜香','http://118.190.148.138:8088/vpaiApp/headPhoto/63558127166737612801517381198838.jpg',1,'16',NULL,'3'),(14,4,'888888','123456','2','毛血旺','本店招牌菜，口味麻辣','http://118.190.148.138:8088/vpaiApp/headPhoto/63554604437455503361516768186081.jpg',0,'46',NULL,'3'),(15,5,'888888','123456','2','手撕包菜','包菜350克，口味清淡，可以做微辣','http://118.190.148.138:8088/vpaiApp/headPhoto/63550899821886832651516680968788.jpg',0,'12',NULL,'3'),(16,6,'888888','123456','3','松仁玉米','口味香甜，适合小孩和女士食用','http://118.190.148.138:8088/vpaiApp/headPhoto/20180125082121.png',0,'18',NULL,'1'),(17,7,'888888','123456','3','糖醋里脊','口味酸甜咸香','http://118.190.148.138:8088/vpaiApp/headPhoto/20180126124247.png',0,'24',NULL,'3'),(18,8,'888888','123456','4','腊肉炒饭','主食的不二选择','http://118.190.148.138:8088/vpaiApp/headPhoto/63525931508836597761516086468978.jpg',6,'3',NULL,'3'),(19,9,'888888','123456','4','黄金大饼','豆沙馅，外酥里嫩，口味香甜','http://118.190.148.138:8088/vpaiApp/headPhoto/63550947241429565441519723430103.jpg',0,'5',NULL,'3'),(20,10,'888888','123456','5','酸辣汤','主原料有黑木耳丝，千叶豆腐丝，细粉条，火腿丝，香菜末加胡椒粉，鸡蛋熬制而成，口味酸辣','http://118.190.148.138:8088/vpaiApp/headPhoto/63550997072655155201516707447018.jpg',0,'2',NULL,'3');

/*Table structure for table `restaurant_distribution_info` */

DROP TABLE IF EXISTS `restaurant_distribution_info`;

CREATE TABLE `restaurant_distribution_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `per_id` varchar(32) DEFAULT NULL COMMENT '用户账号',
  `distribution_name` varchar(5) DEFAULT NULL COMMENT '配送姓名',
  `distribution_sex` varchar(1) DEFAULT NULL COMMENT '配送性别',
  `distribution_phone` varchar(20) DEFAULT NULL COMMENT '配送手机号',
  `distribution_province` varchar(20) DEFAULT NULL COMMENT '省',
  `distribution_city` varchar(30) DEFAULT NULL COMMENT '市',
  `distribution_area` varchar(20) DEFAULT NULL COMMENT '区',
  `distribution_address` varchar(50) DEFAULT NULL COMMENT '配送地址',
  `default_type` varchar(1) DEFAULT NULL COMMENT '默认标识 1默认',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='配送信息表';

/*Data for the table `restaurant_distribution_info` */

insert  into `restaurant_distribution_info`(`id`,`platform_id`,`mer_id`,`per_id`,`distribution_name`,`distribution_sex`,`distribution_phone`,`distribution_province`,`distribution_city`,`distribution_area`,`distribution_address`,`default_type`) values (1,'888888','123456','13827407778492416','贺旭峰',NULL,'18888888888','山西省','太原市','杏花岭区','滨河城',NULL),(2,'888888','123456','13827407778492416','贺旭峰',NULL,'18888888888','山东省','潍坊市','奎文区','恒易宝莲','1'),(3,'888888','123456','13827407778492416','贺旭峰',NULL,'18888888888','山东省','潍坊市','奎文区','世纪泰华城',NULL),(6,'888888','123456','13827407778492416','贺旭峰',NULL,'18366666666','北京','北京市','昌平区','沙河高教园',NULL),(7,'888888','123456','13827407778492416','苏三',NULL,'15131525525','山西省','太原市','迎泽区','省政府',NULL),(8,'888888','123456','13827407778492416','马厩',NULL,'10215142563','浙江省','杭州市','西湖区','断桥',NULL),(12,'888888','123456','13827407778492416','路飞',NULL,'12036545852','山西省','太原市','尖草坪区','太钢东门',NULL);

/*Table structure for table `restaurant_info` */

DROP TABLE IF EXISTS `restaurant_info`;

CREATE TABLE `restaurant_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `name` varchar(20) DEFAULT NULL COMMENT '店铺名称',
  `logo_img` varchar(200) DEFAULT NULL COMMENT '餐厅封面',
  `area` varchar(20) DEFAULT NULL COMMENT '区域',
  `latitude` varchar(20) DEFAULT NULL COMMENT '纬度',
  `longitude` varchar(20) DEFAULT NULL COMMENT '经度',
  `con_service` varchar(50) DEFAULT NULL COMMENT '附加信息',
  `per_capita_spending` varchar(10) DEFAULT NULL COMMENT '人均消费',
  `bulletin` varchar(50) DEFAULT NULL COMMENT '公告',
  `con_mobile` varchar(20) DEFAULT NULL COMMENT '电话',
  `open_btime` time DEFAULT NULL COMMENT '营业开始时间',
  `open_etime` time DEFAULT NULL COMMENT '营业结束时间',
  `introduce` varchar(50) DEFAULT NULL COMMENT '门店介绍',
  `qualification` varchar(200) DEFAULT NULL COMMENT '商家资质',
  `is_order` varchar(1) DEFAULT NULL COMMENT '点餐开关 1开',
  `is_reservation` varchar(1) DEFAULT NULL COMMENT '预订开关 1开',
  `is_takeaway` varchar(1) DEFAULT NULL COMMENT '外卖开关 1开',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='餐厅信息表';

/*Data for the table `restaurant_info` */

insert  into `restaurant_info`(`id`,`platform_id`,`mer_id`,`name`,`logo_img`,`area`,`latitude`,`longitude`,`con_service`,`per_capita_spending`,`bulletin`,`con_mobile`,`open_btime`,`open_etime`,`introduce`,`qualification`,`is_order`,`is_reservation`,`is_takeaway`) values (1,'888888','123456','恒易宝莲','http://118.190.148.138:8088/vpaiApp/headPhoto/63680814940023685121519777262483.jpg','奎文区','36.701963','119.115415','wifi，停车','12','其大无外','1234567','08:00:00','22:00:00','这是门店介绍','http://118.190.148.138:8088/vpaiApp/20171031135347.png','1','1','1');

/*Table structure for table `restaurant_order_info` */

DROP TABLE IF EXISTS `restaurant_order_info`;

CREATE TABLE `restaurant_order_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `per_id` varchar(32) DEFAULT NULL COMMENT '用户账号',
  `price` decimal(18,2) DEFAULT NULL COMMENT '金额',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `pay_status` varchar(2) DEFAULT NULL COMMENT '状态',
  `table_id` int(20) DEFAULT NULL COMMENT '餐桌名称',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `type` varchar(1) DEFAULT NULL COMMENT '类型 1点餐 2预订 3外卖',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='订单表';

/*Data for the table `restaurant_order_info` */

insert  into `restaurant_order_info`(`id`,`platform_id`,`mer_id`,`per_id`,`price`,`pay_time`,`pay_status`,`table_id`,`remark`,`type`) values (1,'888888','123456','66666','32.00','2018-07-08 18:04:01','02',NULL,'','3');

/*Table structure for table `restaurant_picture_info` */

DROP TABLE IF EXISTS `restaurant_picture_info`;

CREATE TABLE `restaurant_picture_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `picture` varchar(100) DEFAULT NULL COMMENT '商家照片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='餐厅照片信息表';

/*Data for the table `restaurant_picture_info` */

insert  into `restaurant_picture_info`(`id`,`platform_id`,`mer_id`,`picture`) values (6,'888888','123456','http://118.190.148.138:8088/vpaiApp/headPhoto/63533838622103961601516273250003.jpg'),(7,'888888','123456','http://118.190.148.138:8088/vpaiApp/headPhoto/63536517549066813441516336956396.jpg'),(8,'888888','123456','http://118.190.148.138:8088/vpaiApp/headPhoto/63550348973918126081517147374566.jpg');

/*Table structure for table `restaurant_reservation_info` */

DROP TABLE IF EXISTS `restaurant_reservation_info`;

CREATE TABLE `restaurant_reservation_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `per_id` varchar(32) DEFAULT NULL COMMENT '用户账号',
  `reservation_date` date DEFAULT NULL COMMENT '预定日期',
  `reservation_time` time DEFAULT NULL COMMENT '用餐时间',
  `reservation_people` varchar(2) DEFAULT NULL COMMENT '用餐人数',
  `reservation_name` varchar(5) DEFAULT NULL COMMENT '姓名',
  `reservation_phone` varchar(20) DEFAULT NULL COMMENT '手机号',
  `reservation_remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `status` varchar(2) DEFAULT NULL COMMENT '状态 1已预订 2已消费 3已取消',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='预订信息表';

/*Data for the table `restaurant_reservation_info` */

insert  into `restaurant_reservation_info`(`id`,`platform_id`,`mer_id`,`per_id`,`reservation_date`,`reservation_time`,`reservation_people`,`reservation_name`,`reservation_phone`,`reservation_remark`,`status`) values (3,'888888','123456',NULL,'2018-07-05','18:35:00','9','贺旭峰','18888888888','最晚八点钟到，麻烦给留一个桌位。',NULL);

/*Table structure for table `restaurant_shopping_cart_info` */

DROP TABLE IF EXISTS `restaurant_shopping_cart_info`;

CREATE TABLE `restaurant_shopping_cart_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `per_id` varchar(32) DEFAULT NULL COMMENT '用户账号',
  `table_id` int(20) DEFAULT NULL COMMENT '餐桌id',
  `dishes_id` int(10) NOT NULL COMMENT '菜品id',
  `dishes_name` varchar(20) DEFAULT NULL COMMENT '菜品名称',
  `dishes_num` int(10) NOT NULL COMMENT '数量',
  `dishes_price` varchar(10) DEFAULT NULL COMMENT '总价',
  `status` varchar(10) DEFAULT NULL COMMENT '状态 1已选择 2已提交 3已支付',
  `type` varchar(2) DEFAULT NULL COMMENT '类型 1点餐 2外卖',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8 COMMENT='购物车信息表';

/*Data for the table `restaurant_shopping_cart_info` */

insert  into `restaurant_shopping_cart_info`(`id`,`platform_id`,`mer_id`,`per_id`,`table_id`,`dishes_id`,`dishes_name`,`dishes_num`,`dishes_price`,`status`,`type`) values (24,'888888','123456',NULL,1,2,'酸辣土豆丝',1,'16','2','1'),(25,'888888','123456',NULL,1,1,'水煮肉片',2,'78','2','1'),(26,'888888','123456',NULL,1,3,'麻婆豆腐',1,'16','2','1'),(27,'888888','123456',NULL,1,8,'腊肉炒饭',3,'9','2','1'),(28,'888888','123456',NULL,1,10,'酸辣汤',3,'6','2','1'),(32,'888888','123456',NULL,1,4,'毛血旺',1,'46','2','1'),(33,'888888','123456',NULL,1,6,'松仁玉米',1,'18','2','1'),(36,'888888','123456',NULL,1,2,'酸辣土豆丝',2,'32','2','1'),(37,'888888','123456',NULL,1,1,'水煮肉片',1,'39','2','1'),(47,'888888','123456','66666',NULL,1,'水煮肉片',3,'117','1','1'),(48,'888888','123456','66666',NULL,2,'酸辣土豆丝',2,'32','1','1'),(55,'888888','123456','66666',NULL,3,'麻婆豆腐',2,'32','1','2'),(56,'888888','123456',NULL,1,1,'水煮肉片',1,'39','2','1'),(57,'888888','123456','13669185209577472',NULL,8,'腊肉炒饭',1,'3','1','1'),(67,'888888','123456','13563439881203712',NULL,3,'麻婆豆腐',1,'16','1','2'),(71,'888888','123456','13563439881203712',NULL,2,'酸辣土豆丝',1,'16','1','1'),(72,'888888','123456','13563439881203712',NULL,1,'水煮肉片',2,'78','1','1'),(73,'888888','123456',NULL,1,2,'酸辣土豆丝',1,'16','2','1'),(74,'888888','123456',NULL,1,2,'酸辣土豆丝',1,'16','2','1'),(75,'888888','123456',NULL,1,2,'酸辣土豆丝',1,'16','2','1'),(76,'888888','123456','',NULL,1,'水煮肉片',1,'39','1','2'),(77,'888888','123456',NULL,1,1,'水煮肉片',1,'39','2','1'),(78,'888888','123456','13788719187046400',NULL,1,'水煮肉片',1,'39','1','1'),(79,'888888','123456',NULL,1,1,'水煮肉片',1,'39','1','1'),(80,'888888','123456',NULL,1,3,'麻婆豆腐',1,'16','1','1'),(81,'888888','123456','13827407778492416',NULL,1,'水煮肉片',1,'39','1','2');

/*Table structure for table `restaurant_table_info` */

DROP TABLE IF EXISTS `restaurant_table_info`;

CREATE TABLE `restaurant_table_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `table_id` bigint(20) DEFAULT NULL COMMENT '餐桌ID',
  `platform_id` varchar(40) DEFAULT NULL COMMENT '平台编号',
  `mer_id` varchar(32) DEFAULT NULL COMMENT '商户账号',
  `table_name` varchar(10) DEFAULT NULL COMMENT '餐桌名称',
  `table_people` varchar(2) DEFAULT NULL COMMENT '人数',
  `status` varchar(2) DEFAULT NULL COMMENT '状态 1去点餐 2桌台已支付',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='餐桌信息表';

/*Data for the table `restaurant_table_info` */

insert  into `restaurant_table_info`(`id`,`table_id`,`platform_id`,`mer_id`,`table_name`,`table_people`,`status`) values (11,1,'888888','123456','一号餐桌','8','1'),(12,2,'888888','123456','二号餐桌','12','2'),(13,3,'888888','123456','三号餐桌','2','1'),(14,4,'888888','123456','四号餐桌','2','1'),(15,5,'888888','123456','五号餐桌','2','2');

/*Table structure for table `system_dictionary` */

DROP TABLE IF EXISTS `system_dictionary`;

CREATE TABLE `system_dictionary` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `dict_type` varchar(20) DEFAULT NULL COMMENT '类型   accountType：账户类型',
  `dict_type_name` varchar(60) DEFAULT NULL,
  `dict_code` varchar(32) DEFAULT NULL COMMENT '码值',
  `dict_codet_name` varchar(60) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

/*Data for the table `system_dictionary` */

/*Table structure for table `unfreeze_journal` */

DROP TABLE IF EXISTS `unfreeze_journal`;

CREATE TABLE `unfreeze_journal` (
  `journal_id` varchar(32) NOT NULL COMMENT '流水号',
  `freeze_id` varchar(32) NOT NULL COMMENT '冻结流水号',
  `order_no` varchar(32) NOT NULL COMMENT '订单号',
  `trade_type` varchar(2) NOT NULL COMMENT '交易类型  01：充值，02：提现，',
  `unfreeze_balance` decimal(18,2) NOT NULL COMMENT '解冻金额',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`journal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='解冻流水表';

/*Data for the table `unfreeze_journal` */

/*Table structure for table `zipkin_annotations` */

DROP TABLE IF EXISTS `zipkin_annotations`;

CREATE TABLE `zipkin_annotations` (
  `trace_id` bigint(20) NOT NULL COMMENT 'coincides with zipkin_spans.trace_id',
  `span_id` bigint(20) NOT NULL COMMENT 'coincides with zipkin_spans.id',
  `a_key` varchar(255) NOT NULL COMMENT 'BinaryAnnotation.key or Annotation.value if type == -1',
  `a_value` blob COMMENT 'BinaryAnnotation.value(), which must be smaller than 64KB',
  `a_type` int(11) NOT NULL COMMENT 'BinaryAnnotation.type() or -1 if Annotation',
  `a_timestamp` bigint(20) DEFAULT NULL COMMENT 'Used to implement TTL; Annotation.timestamp or zipkin_spans.timestamp',
  `endpoint_ipv4` int(11) DEFAULT NULL COMMENT 'Null when Binary/Annotation.endpoint is null',
  `endpoint_ipv6` binary(16) DEFAULT NULL COMMENT 'Null when Binary/Annotation.endpoint is null, or no IPv6 address',
  `endpoint_port` smallint(6) DEFAULT NULL COMMENT 'Null when Binary/Annotation.endpoint is null',
  `endpoint_service_name` varchar(255) DEFAULT NULL COMMENT 'Null when Binary/Annotation.endpoint is null',
  UNIQUE KEY `trace_id` (`trace_id`,`span_id`,`a_key`,`a_timestamp`) COMMENT 'Ignore insert on duplicate',
  KEY `trace_id_2` (`trace_id`,`span_id`) COMMENT 'for joining with zipkin_spans',
  KEY `trace_id_3` (`trace_id`) COMMENT 'for getTraces/ByIds',
  KEY `endpoint_service_name` (`endpoint_service_name`) COMMENT 'for getTraces and getServiceNames',
  KEY `a_type` (`a_type`) COMMENT 'for getTraces',
  KEY `a_key` (`a_key`) COMMENT 'for getTraces'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;

/*Data for the table `zipkin_annotations` */

/*Table structure for table `zipkin_dependencies` */

DROP TABLE IF EXISTS `zipkin_dependencies`;

CREATE TABLE `zipkin_dependencies` (
  `day` date NOT NULL,
  `parent` varchar(255) NOT NULL,
  `child` varchar(255) NOT NULL,
  `call_count` bigint(20) DEFAULT NULL,
  UNIQUE KEY `day` (`day`,`parent`,`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;

/*Data for the table `zipkin_dependencies` */

/*Table structure for table `zipkin_spans` */

DROP TABLE IF EXISTS `zipkin_spans`;

CREATE TABLE `zipkin_spans` (
  `trace_id` bigint(20) NOT NULL,
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `debug` bit(1) DEFAULT NULL,
  `start_ts` bigint(20) DEFAULT NULL COMMENT 'Span.timestamp(): epoch micros used for endTs query and to implement TTL',
  `duration` bigint(20) DEFAULT NULL COMMENT 'Span.duration(): micros used for minDuration and maxDuration query',
  UNIQUE KEY `trace_id` (`trace_id`,`id`) COMMENT 'ignore insert on duplicate',
  KEY `trace_id_2` (`trace_id`,`id`) COMMENT 'for joining with zipkin_annotations',
  KEY `trace_id_3` (`trace_id`) COMMENT 'for getTracesByIds',
  KEY `name` (`name`) COMMENT 'for getTraces and getSpanNames',
  KEY `start_ts` (`start_ts`) COMMENT 'for getTraces ordering and range'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;

/*Data for the table `zipkin_spans` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
