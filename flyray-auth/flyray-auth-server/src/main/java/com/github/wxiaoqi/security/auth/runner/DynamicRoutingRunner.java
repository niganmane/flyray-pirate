package com.github.wxiaoqi.security.auth.runner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

import com.github.wxiaoqi.security.auth.biz.GatewayApiDefineBiz;
import com.github.wxiaoqi.security.auth.entity.GatewayApiDefine;
import com.github.wxiaoqi.security.common.entity.GatewayApiDefineEntity;

/**
 */
@Configuration
public class DynamicRoutingRunner implements CommandLineRunner {
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private GatewayApiDefineBiz gatewayApiDefineBiz;

    @Override
    public void run(String... args) throws Exception {
    	Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("enabled", 1);
        List<GatewayApiDefine> gatewayApiDefineTemp = gatewayApiDefineBiz.queryList(reqMap);
    	List<GatewayApiDefineEntity> gatewayApiDefines = new ArrayList<GatewayApiDefineEntity>();
    	for (int i = 0; i < gatewayApiDefineTemp.size(); i++) {
    		GatewayApiDefine gatewayApiDefine = gatewayApiDefineTemp.get(i);
    		GatewayApiDefineEntity gatewayApiDefineEntity = new GatewayApiDefineEntity();
    		BeanUtils.copyProperties(gatewayApiDefine,gatewayApiDefineEntity);
    		gatewayApiDefines.add(gatewayApiDefineEntity);
		}
    	
    	redisTemplate.opsForValue().set("dynamicRoutings", gatewayApiDefines);
    }
}
