package com.github.wxiaoqi.security.auth.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.wxiaoqi.security.api.vo.user.UserInfo;
import com.github.wxiaoqi.security.auth.common.bean.JWTInfo;
import com.github.wxiaoqi.security.auth.feign.IUserService;
import com.github.wxiaoqi.security.auth.service.AuthService;
import com.github.wxiaoqi.security.auth.service.Kaptcha;
import com.github.wxiaoqi.security.auth.util.user.JwtAuthenticationRequest;
import com.github.wxiaoqi.security.auth.util.user.JwtTokenUtil;
import com.github.wxiaoqi.security.common.entity.PlatformSafetyConfigCommon;
import com.github.wxiaoqi.security.common.exception.auth.ImageCodeInvalidException;
import com.github.wxiaoqi.security.common.exception.auth.UserInvalidException;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

/**
 * 后台登陆用户token授权实现
 * 
 * @author Administrator
 *
 */
@Service
public class AuthServiceImpl implements AuthService {

	private JwtTokenUtil jwtTokenUtil;
	private IUserService userService;
	@Autowired
	private Kaptcha kaptcha;

	@Autowired
	public AuthServiceImpl(JwtTokenUtil jwtTokenUtil, IUserService userService) {
		this.jwtTokenUtil = jwtTokenUtil;
		this.userService = userService;
	}

	@Override
	public Map<String, Object> login(JwtAuthenticationRequest authenticationRequest) throws Exception {
		Map<String, Object> respMap = new HashMap<>();
		if (!kaptcha.validate(authenticationRequest.getImagecode())) {
			throw new ImageCodeInvalidException("图形验证码错误或已失效，请手动刷新！");
		}
		UserInfo info = userService.validate(authenticationRequest);
		if (!StringUtils.isEmpty(info.getUserId())) {
			String token = jwtTokenUtil
					.generateToken(new JWTInfo(info.getPlatformId(), info.getUsername(), info.getUserId() + ""));

			respMap.put("skinStyle", info.getSkinStyle());
			respMap.put("token", token);
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		} else {
			throw new UserInvalidException("用户不存在或账户密码错误!");
		}
		return respMap;
	}

	@Override
	public void validate(String token) throws Exception {
		jwtTokenUtil.getInfoFromToken(token);
	}

	@Override
	public String refresh(String oldToken) throws Exception {
		return jwtTokenUtil.generateToken(jwtTokenUtil.getInfoFromToken(oldToken));
	}

	@Override
	public Map<String, Object> operatorLogin(JwtAuthenticationRequest authenticationRequest) throws Exception {
		Map<String, Object> respMap = new HashMap<>();
		UserInfo info = userService.validate(authenticationRequest);
		System.out.println("获取用户信息。。。。。。。。。。。。。。。。{}" + info);
		if (!StringUtils.isEmpty(info.getUserId())) {
			String token = jwtTokenUtil
					.generateToken(new JWTInfo(info.getPlatformId(), info.getUsername(), info.getUserId() + ""));

			respMap.put("skinStyle", info.getSkinStyle());
			respMap.put("token", token);
			respMap.put("userId", info.getUserId());
			respMap.put("userType", info.getUserType());
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		} else {
			throw new UserInvalidException("用户不存在或账户密码错误!");
		}
		return respMap;
	}

	@Override
	public String createCrmapiToken(PlatformSafetyConfigCommon platformSafetyConfig) throws Exception {
		String token = jwtTokenUtil.generateToken(new JWTInfo(platformSafetyConfig.getAppId(), platformSafetyConfig.getAppKey()));
		return token;
	}
	
}