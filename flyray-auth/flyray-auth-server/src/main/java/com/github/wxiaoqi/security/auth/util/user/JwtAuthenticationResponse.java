package com.github.wxiaoqi.security.auth.util.user;

import java.io.Serializable;

import com.github.wxiaoqi.security.common.admin.pay.response.BaseResponse;

public class JwtAuthenticationResponse extends BaseResponse implements Serializable {
    private static final long serialVersionUID = 1250166508152483573L;

    public String token;

	public JwtAuthenticationResponse(String token) {
        this.token = token;
    }

    public JwtAuthenticationResponse() {
	}
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	

}
