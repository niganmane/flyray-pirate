package com.github.wxiaoqi.security.auth.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.auth.biz.GatewayApiDefineBiz;
import com.github.wxiaoqi.security.auth.entity.GatewayApiDefine;
import com.github.wxiaoqi.security.common.crm.request.GatewayApiDefineRequest;
import com.github.wxiaoqi.security.common.rest.BaseController;

@Controller
@RequestMapping("gatewayApiDefine")
public class GatewayApiDefineController extends BaseController<GatewayApiDefineBiz,GatewayApiDefine> {
	/**
	 * 查询动态路由表
	 * @author centerroot
	 * @time 创建时间:2018年8月29日上午9:54:13
	 * @param gatewayApiDefineRequest
	 * @return
	 */
	@RequestMapping(value = "/queryList", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryList(@RequestBody @Valid GatewayApiDefineRequest gatewayApiDefineRequest){
		return baseBiz.queryList(gatewayApiDefineRequest);
	}
	
	/**
	 * 添加动态路由表
	 * @author centerroot
	 * @time 创建时间:2018年8月29日上午9:54:25
	 * @param gatewayApiDefineRequest
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> add(@RequestBody @Valid GatewayApiDefineRequest gatewayApiDefineRequest){
		return baseBiz.add(gatewayApiDefineRequest);
	}
	
	/**
	 * 查询单个动态路由
	 * @author centerroot
	 * @time 创建时间:2018年8月29日上午9:54:39
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/queryOne/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOneObj(@PathVariable String id){
		Map<String, Object> respMap = baseBiz.getOneObj(id);
        return respMap;
    }
	
	/**
	 * 删除动态路由
	 * @author centerroot
	 * @time 创建时间:2018年8月29日上午9:54:50
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/deleteObj/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public Map<String, Object> removeObj(@PathVariable String id){
		Map<String, Object> respMap = baseBiz.removeObj(id);
		return respMap;
    }
	
	/**
	 * 更新动态路由
	 * @author centerroot
	 * @time 创建时间:2018年8月29日上午9:55:03
	 * @param gatewayApiDefineRequest
	 * @return
	 */
	@RequestMapping(value = "/updateObj",method = RequestMethod.PUT)
    @ResponseBody
    public Map<String, Object> updateObj(@RequestBody @Valid GatewayApiDefineRequest gatewayApiDefineRequest){
		Map<String, Object> respMap = baseBiz.updateObj(gatewayApiDefineRequest);
        return respMap;
    }
}