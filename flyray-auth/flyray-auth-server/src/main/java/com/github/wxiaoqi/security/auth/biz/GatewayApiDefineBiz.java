package com.github.wxiaoqi.security.auth.biz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.entity.GatewayApiDefine;
import com.github.wxiaoqi.security.auth.feign.GatewayFeign;
import com.github.wxiaoqi.security.auth.mapper.GatewayApiDefineMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.GatewayApiDefineRequest;
import com.github.wxiaoqi.security.common.entity.GatewayApiDefineEntity;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.common.util.SnowFlake;

import lombok.extern.slf4j.Slf4j;

/**
 * 动态路由
 * @author centerroot
 * @email ${email}
 * @date 2018-08-28 15:36:21
 */
@Slf4j
@Service
public class GatewayApiDefineBiz extends BaseBiz<GatewayApiDefineMapper,GatewayApiDefine> {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private GatewayFeign gatewayFeign;
    
    public List<GatewayApiDefine> queryList(Map<String, Object> map){
		return mapper.queryList(map);
	}
    
	/**
	 * 查询动态路由表
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:38
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	public Map<String, Object> queryList(GatewayApiDefineRequest gatewayApiDefineRequest){
		log.info("【查询动态路由表】   请求参数：{}",EntityUtils.beanToMap(gatewayApiDefineRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String path = gatewayApiDefineRequest.getPath();
		String serviceId = gatewayApiDefineRequest.getServiceId();
		String url = gatewayApiDefineRequest.getUrl();
		String apiName = gatewayApiDefineRequest.getApiName();
		int page = gatewayApiDefineRequest.getPage();
		int limit = gatewayApiDefineRequest.getLimit();
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("page", page);
	    params.put("limit", limit);
	    Query query = new Query(params);
	    Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		

		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("path", path);
		reqMap.put("serviceId", serviceId);
		reqMap.put("url", url);
		reqMap.put("apiName", apiName);
		
        List<GatewayApiDefine> list = mapper.queryList(reqMap);
        TableResultResponse<GatewayApiDefine> table = new TableResultResponse<GatewayApiDefine>(result.getTotal(), list);
        respMap.put("body", table);
        respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【查询动态路由表】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 添加动态路由表
	 * @author centerroot
	 * @time 创建时间:2018年8月29日上午9:46:59
	 * @param gatewayApiDefineRequest
	 * @return
	 */
	public Map<String, Object> add(GatewayApiDefineRequest gatewayApiDefineRequest){
		log.info("【添加动态路由表】   请求参数：{}",EntityUtils.beanToMap(gatewayApiDefineRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		
		GatewayApiDefine gatewayApiDefine = new GatewayApiDefine();
		BeanUtils.copyProperties(gatewayApiDefineRequest,gatewayApiDefine);
		String id = String.valueOf(SnowFlake.getId());
		gatewayApiDefine.setId(id);
		mapper.insert(gatewayApiDefine);
		
		setRedisRouteInfo();
		Map<String, Object> refreshMap = gatewayFeign.refreshRoute();
		log.info("【添加动态路由表】   路由配置刷新结果：{}",refreshMap);
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【添加动态路由表】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 查询单个动态路由
	 * @author centerroot
	 * @time 创建时间:2018年8月29日上午9:47:07
	 * @param id
	 * @return
	 */
	public Map<String, Object> getOneObj(String id){
		log.info("【查询单个动态路由】   请求参数：id:{}",id);
		Map<String, Object> respMap = new HashMap<String, Object>();
		
		GatewayApiDefine gatewayApiDefine = mapper.selectByPrimaryKey(id);
		respMap.put("gatewayApiDefine", gatewayApiDefine);
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		
		log.info("【查询单个动态路由】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 删除动态路由
	 * @author centerroot
	 * @time 创建时间:2018年8月29日上午9:50:30
	 * @param id
	 * @return
	 */
	public Map<String, Object> removeObj(String id){
		log.info("【删除动态路由】   请求参数：id:{}",id);
		Map<String, Object> respMap = new HashMap<String, Object>();
		mapper.deleteByPrimaryKey(id);
		setRedisRouteInfo();
		Map<String, Object> refreshMap = gatewayFeign.refreshRoute();
		log.info("【删除动态路由】   路由配置刷新结果：{}",refreshMap);
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【删除动态路由】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 更新动态路由
	 * @author centerroot
	 * @time 创建时间:2018年8月29日上午9:51:42
	 * @param gatewayApiDefineRequest
	 * @return
	 */
	public Map<String, Object> updateObj(GatewayApiDefineRequest gatewayApiDefineRequest){
		log.info("【更新动态路由】   请求参数：{}",EntityUtils.beanToMap(gatewayApiDefineRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		
		GatewayApiDefine gatewayApiDefine = new GatewayApiDefine();
		BeanUtils.copyProperties(gatewayApiDefineRequest,gatewayApiDefine);
		mapper.updateByPrimaryKeySelective(gatewayApiDefine);
		
		setRedisRouteInfo();
		Map<String, Object> refreshMap = gatewayFeign.refreshRoute();
		log.info("【更新动态路由】   路由配置刷新结果：{}",refreshMap);
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【更新动态路由】   响应参数：{}",respMap);
		return respMap;
	}
	
	
	public void setRedisRouteInfo(){
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("enabled", 1);
        List<GatewayApiDefine> gatewayApiDefineTemp = mapper.queryList(reqMap);
    	List<GatewayApiDefineEntity> gatewayApiDefines = new ArrayList<GatewayApiDefineEntity>();
    	for (int i = 0; i < gatewayApiDefineTemp.size(); i++) {
    		GatewayApiDefine gatewayApiDefine = gatewayApiDefineTemp.get(i);
    		GatewayApiDefineEntity gatewayApiDefineEntity = new GatewayApiDefineEntity();
    		BeanUtils.copyProperties(gatewayApiDefine,gatewayApiDefineEntity);
    		gatewayApiDefines.add(gatewayApiDefineEntity);
		}
    	redisTemplate.opsForValue().set("dynamicRoutings", gatewayApiDefines);
	}
}