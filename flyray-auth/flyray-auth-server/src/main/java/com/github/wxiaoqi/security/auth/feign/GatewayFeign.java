package com.github.wxiaoqi.security.auth.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 网关接口
 * @author centerroot
 * @time 创建时间:2018年8月29日下午5:09:50
 * @description
 */
@FeignClient(value = "flyray-gate")
public interface GatewayFeign {
	
	/**
	 * 刷新路由配置
	 * @author centerroot
	 * @time 创建时间:2018年8月29日下午5:13:56
	 * @return
	 */
	@RequestMapping(value = "feign/route/refresh",method = RequestMethod.GET)
	public Map<String, Object> refreshRoute();
	
}
