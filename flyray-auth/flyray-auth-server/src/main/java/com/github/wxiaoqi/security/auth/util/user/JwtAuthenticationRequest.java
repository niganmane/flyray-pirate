package com.github.wxiaoqi.security.auth.util.user;

import java.io.Serializable;

public class JwtAuthenticationRequest implements Serializable {

	private static final long serialVersionUID = -8445943548965154778L;

	private String username;
	private String password;
	private String platformId;
	private String saltKey;
	//图形验证码
	private String imagecode;
	
	private String appId;// 加密后的平台编号
	private String key;//   加密后的签名用的盐值
	public JwtAuthenticationRequest(String username, String password, String platformId, String saltKey,String imagecode) {
		this.username = username;
		this.password = password;
		this.platformId = platformId;
		this.saltKey = saltKey;
		this.imagecode = imagecode;
	}
	public JwtAuthenticationRequest(String username, String password, String platformId, String saltKey,String imagecode,String appId,String key) {
		this.username = username;
		this.password = password;
		this.platformId = platformId;
		this.saltKey = saltKey;
		this.imagecode = imagecode;
		this.appId = appId;
		this.key = key;
	}
	
	public JwtAuthenticationRequest() {
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getSaltKey() {
		return saltKey;
	}

	public void setSaltKey(String saltKey) {
		this.saltKey = saltKey;
	}

	public String getImagecode() {
		return imagecode;
	}

	public void setImagecode(String imagecode) {
		this.imagecode = imagecode;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
}
