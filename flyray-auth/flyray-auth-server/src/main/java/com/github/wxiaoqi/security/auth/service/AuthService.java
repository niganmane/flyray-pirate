package com.github.wxiaoqi.security.auth.service;


import java.util.Map;

import com.github.wxiaoqi.security.auth.util.user.JwtAuthenticationRequest;
import com.github.wxiaoqi.security.common.entity.PlatformSafetyConfigCommon;

public interface AuthService {
    Map<String, Object> login(JwtAuthenticationRequest authenticationRequest) throws Exception;
    Map<String, Object> operatorLogin(JwtAuthenticationRequest authenticationRequest) throws Exception;
    String refresh(String oldToken) throws Exception;
    void validate(String token) throws Exception;
	String createCrmapiToken(PlatformSafetyConfigCommon platformSafetyConfig) throws Exception;
    
}
