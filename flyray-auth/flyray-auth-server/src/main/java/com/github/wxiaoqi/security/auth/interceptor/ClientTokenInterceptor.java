package com.github.wxiaoqi.security.auth.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.wxiaoqi.security.auth.common.config.ServiceAuthConfig;
import com.github.wxiaoqi.security.auth.service.AuthClientService;

import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * Created by ace on 2017/9/12.
 */
public class ClientTokenInterceptor implements RequestInterceptor {
    private Logger logger = LoggerFactory.getLogger(ClientTokenInterceptor.class);
    @Autowired
    private ServiceAuthConfig serviceAuthConfig;
    @Autowired
    private AuthClientService authClientService;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        try {
            requestTemplate.header(serviceAuthConfig.getTokenHeader(), authClientService.apply(serviceAuthConfig.getClientId(), serviceAuthConfig.getClientSecret()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
