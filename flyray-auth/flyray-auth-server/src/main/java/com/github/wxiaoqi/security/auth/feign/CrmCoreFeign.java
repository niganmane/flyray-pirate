package com.github.wxiaoqi.security.auth.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * CRM Feign请求
 * @author centerroot
 * @time 创建时间:2018年9月20日下午5:09:24
 * @description
 */
@FeignClient(value = "flyray-crm-core")
public interface CrmCoreFeign {
	
	/**
	 * 刷新路由配置
	 * @author centerroot
	 * @time 创建时间:2018年8月29日下午5:13:56
	 * @return
	 */
	@RequestMapping(value = "feign/platform/queryPlatformInfo",method = RequestMethod.POST)
	public Map<String, Object> queryPlatformInfo(Map<String, Object> param);
	
}
