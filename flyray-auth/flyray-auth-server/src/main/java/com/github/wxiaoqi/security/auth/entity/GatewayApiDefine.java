package com.github.wxiaoqi.security.auth.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-28 15:36:21
 */
@Table(name = "gateway_api_define")
public class GatewayApiDefine implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private String id;
	
	    //
    @Column(name = "path")
    private String path;
	
	    //
    @Column(name = "service_id")
    private String serviceId;
	
	    //
    @Column(name = "url")
    private String url;
	
	    //
    @Column(name = "retryable")
    private Integer retryable;
	
	    //
    @Column(name = "enabled")
    private Integer enabled;
	
	    //
    @Column(name = "strip_prefix")
    private Integer stripPrefix;
	
	    //
    @Column(name = "api_name")
    private String apiName;
	

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * 获取：
	 */
	public String getPath() {
		return path;
	}
	/**
	 * 设置：
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	/**
	 * 获取：
	 */
	public String getServiceId() {
		return serviceId;
	}
	/**
	 * 设置：
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * 获取：
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * 设置：
	 */
	public void setRetryable(Integer retryable) {
		this.retryable = retryable;
	}
	/**
	 * 获取：
	 */
	public Integer getRetryable() {
		return retryable;
	}
	/**
	 * 设置：
	 */
	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}
	/**
	 * 获取：
	 */
	public Integer getEnabled() {
		return enabled;
	}
	/**
	 * 设置：
	 */
	public void setStripPrefix(Integer stripPrefix) {
		this.stripPrefix = stripPrefix;
	}
	/**
	 * 获取：
	 */
	public Integer getStripPrefix() {
		return stripPrefix;
	}
	/**
	 * 设置：
	 */
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}
	/**
	 * 获取：
	 */
	public String getApiName() {
		return apiName;
	}
}
