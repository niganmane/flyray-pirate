package com.github.wxiaoqi.security.auth.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.auth.entity.GatewayApiDefine;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-28 15:36:21
 */
@org.apache.ibatis.annotations.Mapper
public interface GatewayApiDefineMapper extends Mapper<GatewayApiDefine> {
	
	List<GatewayApiDefine> queryList(Map<String, Object> map);
	
}
