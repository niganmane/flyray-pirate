package com.github.wxiaoqi.security.auth.common.bean;


import com.github.wxiaoqi.security.auth.common.util.jwt.IJWTInfo;

/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 微服务客户端授权
**/

public class ClientInfo implements IJWTInfo {
	
	/**
	 * 微服务ID
	 */
	String XId;
	/**
	 * 微服务code
	 */
    String clientCode;
    /**
	 * 微服务名称
	 */
    String name;

    public ClientInfo(String clientCode, String name, String XId) {
    	this.clientCode = clientCode;
        this.name = name;
        this.XId = XId;
    }

    public void setXId(String XId) {
        this.XId = XId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    
    public String getName() {
		return name;
	}

	public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getUniqueName() {
        return clientCode;
    }

    @Override
    public String getXId() {
        return XId;
    }

	@Override
	public String getPlatformId() {
		return null;
	}
	
	@Override
	public String getSaltKey() {
		return null;
	}

}
