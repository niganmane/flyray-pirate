package com.github.wxiaoqi.security.auth.common.bean;

import java.io.Serializable;

import com.github.wxiaoqi.security.auth.common.util.jwt.IJWTInfo;


/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 登陆用户授权
**/

public class JWTInfo implements Serializable,IJWTInfo {
	
	/**
	 * 登陆用户名称
	 */
    private String name;
    /**
     * 用户id userId
     */
    private String XId;
    /**
     * 平台编号
     * */
    private String platformId;
    /**
     * 签名盐值
     * */
    private String saltKey;
    
    public JWTInfo(String platformId, String name, String XId) {
    	this.platformId = platformId;
        this.name = name;
        this.XId = XId;
    }

    public JWTInfo(String platformId, String saltKey) {
		this.platformId = platformId;
		this.saltKey = saltKey;
	}

	@Override
    public String getUniqueName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getXId() {
        return XId;
    }

    public void setXId(String XId) {
        this.XId = XId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JWTInfo jwtInfo = (JWTInfo) o;

        if (name != null ? !name.equals(jwtInfo.name) : jwtInfo.name != null) {
            return false;
        }
        return XId != null ? XId.equals(jwtInfo.XId) : jwtInfo.XId == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (XId != null ? XId.hashCode() : 0);
        return result;
    }

	@Override
	public String getPlatformId() {
		return platformId;
	}

	@Override
	public String getSaltKey() {
		return saltKey;
	}

	@Override
	public String getClientCode() {
		return null;
	}
}
