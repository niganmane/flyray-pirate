package me.flyray.netty.proxy.jdk;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

import me.flyray.netty.proxy.UserService;
import me.flyray.netty.proxy.UserServiceImpl;

public class JDKProxyTest {

    public static void main(String[] args) {
        UserService userService = new UserServiceImpl();
        InvocationHandler invocationHandler = new MyInvocationHandler(userService);
        UserService userServiceProxy = (UserService) Proxy.newProxyInstance(userService.getClass().getClassLoader(),
                userService.getClass().getInterfaces(), invocationHandler);
        System.out.println(userServiceProxy.getName(1));
        System.out.println(userServiceProxy.getAge(1));
    }

}
