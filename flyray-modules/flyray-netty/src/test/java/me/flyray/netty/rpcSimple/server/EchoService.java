package me.flyray.netty.rpcSimple.server;

public interface EchoService {

    String echo(String ping);

}
