package me.flyray.netty.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.netty.channel.Channel;
import io.netty.channel.socket.SocketChannel;
import me.flyray.netty.handlers.NettyChannelMap;
import me.flyray.netty.service.TestService;

/** 
* @author: bolei
* @date：2018年9月11日 下午2:18:59 
* @description：类说明
*/

@RestController
public class TestController {

	@Autowired
	   private TestService testService;

	   @RequestMapping(value = "test", method = RequestMethod.GET)
	   public String testMono(String s) {
		   SocketChannel channel = NettyChannelMap.get("w-lock");
		   channel.writeAndFlush(s);
	       return testService.test();
	   }
}
