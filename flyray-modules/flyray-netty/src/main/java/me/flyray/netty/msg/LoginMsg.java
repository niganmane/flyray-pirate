package me.flyray.netty.msg;

/** 
* @author: bolei
* @date：2018年9月11日 下午1:19:49 
* @description：类说明
*/

public class LoginMsg extends BaseMsg{
	String username;
	String password;
	
	public LoginMsg() {
		super();
		setMsgType(MsgType.LOGIN);
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


}

