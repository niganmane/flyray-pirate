package me.flyray.netty.msg;

/** 
* @author: bolei
* @date：2018年9月11日 下午1:21:43 
* @description：类说明
*/

public class ReplyMsg extends BaseMsg {
    public ReplyMsg() {
        super();
        setMsgType(MsgType.REPLY);
    }
    private ReplyBody body;
 
    public ReplyBody getBody() {
        return body;
    }
 
    public void setBody(ReplyBody body) {
        this.body = body;
    }
}

