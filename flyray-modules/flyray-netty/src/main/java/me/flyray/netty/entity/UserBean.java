package me.flyray.netty.entity;

/** 
* @author: bolei
* @date：2018年9月11日 下午7:14:38 
* @description：类说明
*/

public class UserBean {
    private int id;
    private String name;

    public UserBean(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public UserBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

