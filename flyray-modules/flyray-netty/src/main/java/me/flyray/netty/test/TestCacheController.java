package me.flyray.netty.test;

import com.github.wxiaoqi.security.cache.CacheProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *　@author bolei
 *　@date Oct 5, 2018
　*　@description 
**/

@Controller
@RequestMapping("/test")
public class TestCacheController {

    private static final Logger log = LoggerFactory.getLogger(TestCacheController.class);

    @ResponseBody
    @RequestMapping("index")
    public String index(){

        String str =  "";

        CacheProvider.set("tyh", "aaaaaaaaaaaaaaaaaa");
        str += "|";
        str += CacheProvider.get("tyh");
        str += "|";
        str += CacheProvider.del("tyh");

        str += "|||";


        str += "|";
        str += CacheProvider.get("users", User.class);
        str += "|";
        str += CacheProvider.del("users");

        return str.toString();
    }
}
