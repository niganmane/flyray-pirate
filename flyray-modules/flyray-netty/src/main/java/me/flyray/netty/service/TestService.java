package me.flyray.netty.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import me.flyray.netty.entity.UserBean;

/** 
* @author: bolei
* @date：2018年9月11日 下午7:13:56 
* @description：类说明
*/

@Service
public class TestService {

    private Map<Integer, UserBean> UserBeanMap = new ConcurrentHashMap<>();

    @PostConstruct
    private void init() {
        UserBean UserBean1 = new UserBean(1, "tom1");
        UserBean UserBean2 = new UserBean(2, "tom2");
        UserBean UserBean3 = new UserBean(3, "tom3");
        UserBean UserBean4 = new UserBean(4, "tom4");
        UserBeanMap.put(1, UserBean1);
        UserBeanMap.put(2, UserBean2);
        UserBeanMap.put(3, UserBean3);
        UserBeanMap.put(4, UserBean4);
    }

    public String test() {
        System.out.println("[TestService].....");
        return "hello word";
    }

}

