package me.flyray.netty.msg;

import java.io.Serializable;

/** 
* @author: bolei
* @date：2018年9月11日 下午1:19:03 
* @description：类说明
*/

public class AskParams implements Serializable {
	 
    private static final long serialVersionUID = 1L;
 
    private String auth;
 
 
 
    public String getAuth() {
 
        return auth;
 
    }
 
 
 
    public void setAuth(String auth) {
 
        this.auth = auth;
 
    }
 
}

