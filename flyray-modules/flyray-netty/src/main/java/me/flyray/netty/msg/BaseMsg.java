package me.flyray.netty.msg;

import java.io.Serializable;

import me.flyray.netty.constant.Constants;

/** 
* @author: bolei
* @date：2018年9月11日 下午1:18:39 
* @description：类说明
*/

public abstract class BaseMsg implements Serializable{
	  private static final long serialVersionUID = 1L;
	  private MsgType msgType;
	  private String clientID;
	  
	  public BaseMsg() {
		this.clientID = Constants.getClientID();
	}
	  
	public MsgType getMsgType() {
		return msgType;
	}
	public void setMsgType(MsgType msgType) {
		this.msgType = msgType;
	}
	public String getClientID() {
		return clientID;
	}
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	
	
}
