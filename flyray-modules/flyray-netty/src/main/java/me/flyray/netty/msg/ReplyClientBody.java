package me.flyray.netty.msg;

/** 
* @author: bolei
* @date：2018年9月11日 下午1:22:27 
* @description：类说明
*/

public class ReplyClientBody extends ReplyBody {
	 
    private String clientInfo;
 
 
 
    public ReplyClientBody(String clientInfo) {
 
        this.clientInfo = clientInfo;
 
    }
 
 
 
    public String getClientInfo() {
 
        return clientInfo;
 
    }
 
 
 
    public void setClientInfo(String clientInfo) {
 
        this.clientInfo = clientInfo;
 
    }
 
}

