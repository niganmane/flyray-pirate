package me.flyray.netty.msg;

/** 
* @author: bolei
* @date：2018年9月11日 下午1:22:44 
* @description：类说明
*/

public class ReplyServerBody extends ReplyBody{
	  private String serverInfo;

	    public ReplyServerBody(String serverInfo) {

	        this.serverInfo = serverInfo;

	    }

	    public String getServerInfo() {

	        return serverInfo;

	    }

	    public void setServerInfo(String serverInfo) {

	        this.serverInfo = serverInfo;

	    }

}

