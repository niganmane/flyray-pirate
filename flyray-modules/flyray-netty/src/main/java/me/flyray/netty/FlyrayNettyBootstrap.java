package me.flyray.netty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"me.flyray.netty","com.github.wxiaoqi.security.cache"})
public class FlyrayNettyBootstrap {
	public static void main(String[] args) {
			SpringApplication.run(FlyrayNettyBootstrap.class, args);
		}
}
