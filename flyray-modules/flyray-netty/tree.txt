[INFO] Scanning for projects...
[INFO] 
[INFO] -----------------------< me.flyray:flyray-netty >-----------------------
[INFO] Building flyray-netty 0.0.1-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from releases: http://maven.ibuscloud.net:33888/nexus/content/groups/public/com/github/wxiaoqi/flyray-cache/2.0-SNAPSHOT/maven-metadata.xml
Downloading from thirdparty: http://maven.ibuscloud.net:33888/nexus/content/groups/public/com/github/wxiaoqi/flyray-cache/2.0-SNAPSHOT/maven-metadata.xml
Downloading from aliyun: http://maven.aliyun.com/nexus/content/groups/public/com/github/wxiaoqi/flyray-cache/2.0-SNAPSHOT/maven-metadata.xml
Downloading from snapshots: http://maven.ibuscloud.net:33888/nexus/content/groups/public/com/github/wxiaoqi/flyray-cache/2.0-SNAPSHOT/maven-metadata.xml
Downloading from hadoop: https://mvnrepository.com/artifact/org.apache.hadoop/com/github/wxiaoqi/flyray-cache/2.0-SNAPSHOT/maven-metadata.xml
Downloading from spring-snapshot: https://repo.spring.io/snapshot/com/github/wxiaoqi/flyray-cache/2.0-SNAPSHOT/maven-metadata.xml
Downloading from nexus: http://www.stormvalley.cn:8083/nexus/content/groups/public/com/github/wxiaoqi/flyray-cache/2.0-SNAPSHOT/maven-metadata.xml
[WARNING] Could not transfer metadata com.github.wxiaoqi:flyray-cache:2.0-SNAPSHOT/maven-metadata.xml from/to nexus (http://www.stormvalley.cn:8083/nexus/content/groups/public): Connect to www.stormvalley.cn:8083 [www.stormvalley.cn/120.77.59.175] failed: Connection timed out: connect
[WARNING] Failure to transfer com.github.wxiaoqi:flyray-cache:2.0-SNAPSHOT/maven-metadata.xml from http://www.stormvalley.cn:8083/nexus/content/groups/public was cached in the local repository, resolution will not be reattempted until the update interval of nexus has elapsed or updates are forced. Original error: Could not transfer metadata com.github.wxiaoqi:flyray-cache:2.0-SNAPSHOT/maven-metadata.xml from/to nexus (http://www.stormvalley.cn:8083/nexus/content/groups/public): Connect to www.stormvalley.cn:8083 [www.stormvalley.cn/120.77.59.175] failed: Connection timed out: connect
Downloading from snapshots: http://maven.ibuscloud.net:33888/nexus/content/groups/public/com/github/wxiaoqi/flyray-pirate/2.0-SNAPSHOT/maven-metadata.xml
Downloading from releases: http://maven.ibuscloud.net:33888/nexus/content/groups/public/com/github/wxiaoqi/flyray-pirate/2.0-SNAPSHOT/maven-metadata.xml
Downloading from thirdparty: http://maven.ibuscloud.net:33888/nexus/content/groups/public/com/github/wxiaoqi/flyray-pirate/2.0-SNAPSHOT/maven-metadata.xml
Downloading from aliyun: http://maven.aliyun.com/nexus/content/groups/public/com/github/wxiaoqi/flyray-pirate/2.0-SNAPSHOT/maven-metadata.xml
Downloading from hadoop: https://mvnrepository.com/artifact/org.apache.hadoop/com/github/wxiaoqi/flyray-pirate/2.0-SNAPSHOT/maven-metadata.xml
Downloading from nexus: http://www.stormvalley.cn:8083/nexus/content/groups/public/com/github/wxiaoqi/flyray-pirate/2.0-SNAPSHOT/maven-metadata.xml
Downloading from spring-snapshot: https://repo.spring.io/snapshot/com/github/wxiaoqi/flyray-pirate/2.0-SNAPSHOT/maven-metadata.xml
[WARNING] Could not transfer metadata com.github.wxiaoqi:flyray-pirate:2.0-SNAPSHOT/maven-metadata.xml from/to nexus (http://www.stormvalley.cn:8083/nexus/content/groups/public): Connect to www.stormvalley.cn:8083 [www.stormvalley.cn/120.77.59.175] failed: Connection timed out: connect
[INFO] 
[INFO] --- maven-dependency-plugin:3.0.1:tree (default-cli) @ flyray-netty ---
[INFO] me.flyray:flyray-netty:jar:0.0.1-SNAPSHOT
[INFO] +- com.github.wxiaoqi:flyray-cache:jar:2.0-SNAPSHOT:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-data-redis:jar:2.0.0.RELEASE:compile
[INFO] |  |  +- org.springframework.data:spring-data-redis:jar:2.0.5.RELEASE:compile
[INFO] |  |  |  +- org.springframework.data:spring-data-keyvalue:jar:2.0.5.RELEASE:compile
[INFO] |  |  |  |  \- org.springframework.data:spring-data-commons:jar:2.0.5.RELEASE:compile
[INFO] |  |  |  +- org.springframework:spring-tx:jar:5.0.4.RELEASE:compile
[INFO] |  |  |  \- org.springframework:spring-oxm:jar:5.0.4.RELEASE:compile
[INFO] |  |  \- io.lettuce:lettuce-core:jar:5.0.2.RELEASE:compile
[INFO] |  |     +- io.projectreactor:reactor-core:jar:3.1.5.RELEASE:compile
[INFO] |  |     |  \- org.reactivestreams:reactive-streams:jar:1.0.2:compile
[INFO] |  |     +- io.netty:netty-common:jar:4.1.22.Final:compile
[INFO] |  |     +- io.netty:netty-transport:jar:4.1.22.Final:compile
[INFO] |  |     |  +- io.netty:netty-buffer:jar:4.1.22.Final:compile
[INFO] |  |     |  \- io.netty:netty-resolver:jar:4.1.22.Final:compile
[INFO] |  |     \- io.netty:netty-handler:jar:4.1.22.Final:compile
[INFO] |  |        \- io.netty:netty-codec:jar:4.1.22.Final:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-cache:jar:2.0.0.RELEASE:compile
[INFO] |  |  +- org.springframework:spring-context:jar:5.0.4.RELEASE:compile
[INFO] |  |  \- org.springframework:spring-context-support:jar:5.0.4.RELEASE:compile
[INFO] |  +- org.apache.commons:commons-pool2:jar:2.5.0:compile
[INFO] |  +- com.fasterxml.jackson.core:jackson-databind:jar:2.9.4:compile
[INFO] |  |  +- com.fasterxml.jackson.core:jackson-annotations:jar:2.9.0:compile
[INFO] |  |  \- com.fasterxml.jackson.core:jackson-core:jar:2.9.4:compile
[INFO] |  \- com.alibaba:fastjson:jar:1.2.15:compile
[INFO] +- org.springframework.boot:spring-boot-starter:jar:2.0.0.RELEASE:compile
[INFO] |  +- org.springframework.boot:spring-boot:jar:2.0.0.RELEASE:compile
[INFO] |  +- org.springframework.boot:spring-boot-autoconfigure:jar:2.0.0.RELEASE:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-logging:jar:2.0.0.RELEASE:compile
[INFO] |  |  +- ch.qos.logback:logback-classic:jar:1.2.3:compile
[INFO] |  |  |  \- ch.qos.logback:logback-core:jar:1.2.3:compile
[INFO] |  |  +- org.apache.logging.log4j:log4j-to-slf4j:jar:2.10.0:compile
[INFO] |  |  |  \- org.apache.logging.log4j:log4j-api:jar:2.10.0:compile
[INFO] |  |  \- org.slf4j:jul-to-slf4j:jar:1.7.25:compile
[INFO] |  +- javax.annotation:javax.annotation-api:jar:1.3.2:compile
[INFO] |  +- org.springframework:spring-core:jar:5.0.4.RELEASE:compile
[INFO] |  |  \- org.springframework:spring-jcl:jar:5.0.4.RELEASE:compile
[INFO] |  \- org.yaml:snakeyaml:jar:1.19:runtime
[INFO] +- org.springframework.boot:spring-boot-starter-web:jar:2.0.0.RELEASE:compile
[INFO] |  +- org.springframework.boot:spring-boot-starter-json:jar:2.0.0.RELEASE:compile
[INFO] |  |  +- com.fasterxml.jackson.datatype:jackson-datatype-jdk8:jar:2.9.4:compile
[INFO] |  |  +- com.fasterxml.jackson.datatype:jackson-datatype-jsr310:jar:2.9.4:compile
[INFO] |  |  \- com.fasterxml.jackson.module:jackson-module-parameter-names:jar:2.9.4:compile
[INFO] |  +- org.hibernate.validator:hibernate-validator:jar:6.0.7.Final:compile
[INFO] |  |  +- javax.validation:validation-api:jar:2.0.1.Final:compile
[INFO] |  |  +- org.jboss.logging:jboss-logging:jar:3.3.2.Final:compile
[INFO] |  |  \- com.fasterxml:classmate:jar:1.3.4:compile
[INFO] |  +- org.springframework:spring-web:jar:5.0.4.RELEASE:compile
[INFO] |  |  \- org.springframework:spring-beans:jar:5.0.4.RELEASE:compile
[INFO] |  \- org.springframework:spring-webmvc:jar:5.0.4.RELEASE:compile
[INFO] |     +- org.springframework:spring-aop:jar:5.0.4.RELEASE:compile
[INFO] |     \- org.springframework:spring-expression:jar:5.0.4.RELEASE:compile
[INFO] +- org.springframework.boot:spring-boot-starter-undertow:jar:2.0.0.RELEASE:compile
[INFO] |  +- io.undertow:undertow-core:jar:1.4.22.Final:compile
[INFO] |  |  +- org.jboss.xnio:xnio-api:jar:3.3.8.Final:compile
[INFO] |  |  \- org.jboss.xnio:xnio-nio:jar:3.3.8.Final:runtime
[INFO] |  +- io.undertow:undertow-servlet:jar:1.4.22.Final:compile
[INFO] |  |  \- org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec:jar:1.0.0.Final:compile
[INFO] |  +- io.undertow:undertow-websockets-jsr:jar:1.4.22.Final:compile
[INFO] |  |  \- org.jboss.spec.javax.websocket:jboss-websocket-api_1.1_spec:jar:1.1.0.Final:compile
[INFO] |  +- javax.servlet:javax.servlet-api:jar:3.1.0:compile
[INFO] |  \- org.glassfish:javax.el:jar:3.0.0:compile
[INFO] +- org.springframework.boot:spring-boot-starter-test:jar:2.0.0.RELEASE:test
[INFO] |  +- org.springframework.boot:spring-boot-test:jar:2.0.0.RELEASE:test
[INFO] |  +- org.springframework.boot:spring-boot-test-autoconfigure:jar:2.0.0.RELEASE:test
[INFO] |  +- com.jayway.jsonpath:json-path:jar:2.4.0:test
[INFO] |  |  \- net.minidev:json-smart:jar:2.3:compile
[INFO] |  |     \- net.minidev:accessors-smart:jar:1.2:compile
[INFO] |  +- junit:junit:jar:4.12:test
[INFO] |  +- org.assertj:assertj-core:jar:3.9.1:test
[INFO] |  +- org.mockito:mockito-core:jar:2.15.0:test
[INFO] |  |  +- net.bytebuddy:byte-buddy:jar:1.7.10:test
[INFO] |  |  +- net.bytebuddy:byte-buddy-agent:jar:1.7.10:test
[INFO] |  |  \- org.objenesis:objenesis:jar:2.6:test
[INFO] |  +- org.hamcrest:hamcrest-core:jar:1.3:test
[INFO] |  +- org.hamcrest:hamcrest-library:jar:1.3:test
[INFO] |  +- org.skyscreamer:jsonassert:jar:1.5.0:test
[INFO] |  |  \- com.vaadin.external.google:android-json:jar:0.0.20131108.vaadin1:test
[INFO] |  +- org.springframework:spring-test:jar:5.0.4.RELEASE:test
[INFO] |  \- org.xmlunit:xmlunit-core:jar:2.5.1:test
[INFO] +- org.springframework.boot:spring-boot-devtools:jar:2.0.0.RELEASE:compile
[INFO] +- io.netty:netty-all:jar:5.0.0.Alpha1:compile
[INFO] +- cglib:cglib:jar:3.2.7:compile
[INFO] |  +- org.ow2.asm:asm:jar:6.2:compile
[INFO] |  \- org.apache.ant:ant:jar:1.10.3:compile
[INFO] |     \- org.apache.ant:ant-launcher:jar:1.10.3:compile
[INFO] \- org.apache.hadoop:hadoop-common:jar:3.1.1:compile
[INFO]    +- org.apache.hadoop:hadoop-annotations:jar:3.1.1:compile
[INFO]    |  \- jdk.tools:jdk.tools:jar:1.8:system
[INFO]    +- com.google.guava:guava:jar:11.0.2:compile
[INFO]    +- commons-cli:commons-cli:jar:1.2:compile
[INFO]    +- org.apache.commons:commons-math3:jar:3.1.1:compile
[INFO]    +- org.apache.httpcomponents:httpclient:jar:4.5.5:compile
[INFO]    |  \- org.apache.httpcomponents:httpcore:jar:4.4.9:compile
[INFO]    +- commons-codec:commons-codec:jar:1.11:compile
[INFO]    +- commons-io:commons-io:jar:2.5:compile
[INFO]    +- commons-net:commons-net:jar:3.6:compile
[INFO]    +- commons-collections:commons-collections:jar:3.2.2:compile
[INFO]    +- org.eclipse.jetty:jetty-server:jar:9.4.8.v20171121:compile
[INFO]    |  +- org.eclipse.jetty:jetty-http:jar:9.4.8.v20171121:compile
[INFO]    |  \- org.eclipse.jetty:jetty-io:jar:9.4.8.v20171121:compile
[INFO]    +- org.eclipse.jetty:jetty-util:jar:9.4.8.v20171121:compile
[INFO]    +- org.eclipse.jetty:jetty-servlet:jar:9.4.8.v20171121:compile
[INFO]    |  \- org.eclipse.jetty:jetty-security:jar:9.4.8.v20171121:compile
[INFO]    +- org.eclipse.jetty:jetty-webapp:jar:9.4.8.v20171121:compile
[INFO]    |  \- org.eclipse.jetty:jetty-xml:jar:9.4.8.v20171121:compile
[INFO]    +- javax.servlet.jsp:jsp-api:jar:2.1:runtime
[INFO]    +- com.sun.jersey:jersey-core:jar:1.19:compile
[INFO]    |  \- javax.ws.rs:jsr311-api:jar:1.1.1:compile
[INFO]    +- com.sun.jersey:jersey-servlet:jar:1.19:compile
[INFO]    +- com.sun.jersey:jersey-json:jar:1.19:compile
[INFO]    |  +- org.codehaus.jettison:jettison:jar:1.1:compile
[INFO]    |  +- com.sun.xml.bind:jaxb-impl:jar:2.2.3-1:compile
[INFO]    |  |  \- javax.xml.bind:jaxb-api:jar:2.3.0:compile
[INFO]    |  +- org.codehaus.jackson:jackson-core-asl:jar:1.9.2:compile
[INFO]    |  +- org.codehaus.jackson:jackson-mapper-asl:jar:1.9.2:compile
[INFO]    |  +- org.codehaus.jackson:jackson-jaxrs:jar:1.9.2:compile
[INFO]    |  \- org.codehaus.jackson:jackson-xc:jar:1.9.2:compile
[INFO]    +- com.sun.jersey:jersey-server:jar:1.19:compile
[INFO]    +- commons-logging:commons-logging:jar:1.1.3:compile
[INFO]    +- log4j:log4j:jar:1.2.17:compile
[INFO]    +- commons-lang:commons-lang:jar:2.6:compile
[INFO]    +- commons-beanutils:commons-beanutils:jar:1.9.3:compile
[INFO]    +- org.apache.commons:commons-configuration2:jar:2.1.1:compile
[INFO]    +- org.apache.commons:commons-lang3:jar:3.7:compile
[INFO]    +- org.slf4j:slf4j-api:jar:1.7.25:compile
[INFO]    +- org.slf4j:slf4j-log4j12:jar:1.7.25:compile
[INFO]    +- org.apache.avro:avro:jar:1.7.7:compile
[INFO]    |  +- com.thoughtworks.paranamer:paranamer:jar:2.3:compile
[INFO]    |  \- org.xerial.snappy:snappy-java:jar:1.0.5:compile
[INFO]    +- com.google.re2j:re2j:jar:1.1:compile
[INFO]    +- com.google.protobuf:protobuf-java:jar:2.5.0:compile
[INFO]    +- com.google.code.gson:gson:jar:2.8.2:compile
[INFO]    +- org.apache.hadoop:hadoop-auth:jar:3.1.1:compile
[INFO]    |  +- com.nimbusds:nimbus-jose-jwt:jar:4.41.1:compile
[INFO]    |  |  \- com.github.stephenc.jcip:jcip-annotations:jar:1.0-1:compile
[INFO]    |  \- org.apache.curator:curator-framework:jar:2.12.0:compile
[INFO]    +- com.jcraft:jsch:jar:0.1.54:compile
[INFO]    +- org.apache.curator:curator-client:jar:2.12.0:compile
[INFO]    +- org.apache.curator:curator-recipes:jar:2.12.0:compile
[INFO]    +- com.google.code.findbugs:jsr305:jar:3.0.0:compile
[INFO]    +- org.apache.htrace:htrace-core4:jar:4.1.0-incubating:compile
[INFO]    +- org.apache.zookeeper:zookeeper:jar:3.4.9:compile
[INFO]    |  +- jline:jline:jar:0.9.94:compile
[INFO]    |  \- io.netty:netty:jar:3.10.5.Final:compile
[INFO]    +- org.apache.commons:commons-compress:jar:1.4.1:compile
[INFO]    |  \- org.tukaani:xz:jar:1.0:compile
[INFO]    +- org.apache.kerby:kerb-simplekdc:jar:1.0.1:compile
[INFO]    |  +- org.apache.kerby:kerb-client:jar:1.0.1:compile
[INFO]    |  |  +- org.apache.kerby:kerby-config:jar:1.0.1:compile
[INFO]    |  |  +- org.apache.kerby:kerb-core:jar:1.0.1:compile
[INFO]    |  |  |  \- org.apache.kerby:kerby-pkix:jar:1.0.1:compile
[INFO]    |  |  |     +- org.apache.kerby:kerby-asn1:jar:1.0.1:compile
[INFO]    |  |  |     \- org.apache.kerby:kerby-util:jar:1.0.1:compile
[INFO]    |  |  +- org.apache.kerby:kerb-common:jar:1.0.1:compile
[INFO]    |  |  |  \- org.apache.kerby:kerb-crypto:jar:1.0.1:compile
[INFO]    |  |  +- org.apache.kerby:kerb-util:jar:1.0.1:compile
[INFO]    |  |  \- org.apache.kerby:token-provider:jar:1.0.1:compile
[INFO]    |  \- org.apache.kerby:kerb-admin:jar:1.0.1:compile
[INFO]    |     +- org.apache.kerby:kerb-server:jar:1.0.1:compile
[INFO]    |     |  \- org.apache.kerby:kerb-identity:jar:1.0.1:compile
[INFO]    |     \- org.apache.kerby:kerby-xdr:jar:1.0.1:compile
[INFO]    +- org.codehaus.woodstox:stax2-api:jar:3.1.4:compile
[INFO]    \- com.fasterxml.woodstox:woodstox-core:jar:5.0.3:compile
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 44.947 s
[INFO] Finished at: 2018-10-29T18:52:02+08:00
[INFO] ------------------------------------------------------------------------
