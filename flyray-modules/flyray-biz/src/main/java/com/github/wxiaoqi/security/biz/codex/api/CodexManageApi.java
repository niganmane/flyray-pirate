package com.github.wxiaoqi.security.biz.codex.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.codex.biz.CodexEngineBiz;
import com.github.wxiaoqi.security.common.admin.pay.response.BaseResponse;
import com.github.wxiaoqi.security.common.biz.request.BaseCouponIntegralQueryReq;
import com.github.wxiaoqi.security.common.biz.request.BestCouponIntegralQueryReq;
import com.github.wxiaoqi.security.common.biz.request.CouponInfoQueryReq;
import com.github.wxiaoqi.security.common.biz.request.CouponIntegralRefundReq;
import com.github.wxiaoqi.security.common.biz.request.CouponIntegralUseReq;
import com.github.wxiaoqi.security.common.biz.request.CouponReceiveReq;
import com.github.wxiaoqi.security.common.biz.request.CouponStatusReq;
import com.github.wxiaoqi.security.common.biz.request.IntegralReceiveReq;
import com.github.wxiaoqi.security.common.biz.request.WXCouponRevReq;
import com.github.wxiaoqi.security.common.biz.response.BaseCouponIntegralQueryRsp;

/**
 * 规则引擎管理-外部
 * 
 * @author muyongquan 2017年12月26日 下午5:21:58
 */
@Controller
@RequestMapping("codexEngineApi")
public class CodexManageApi extends BaseAction {

	private final static Logger logger = LoggerFactory.getLogger(CodexManageApi.class);

	@Autowired
	private CodexEngineBiz codexEngineBiz;

	/**
	 * 事件触发的红包或者积分基础查询服务
	 * @param feeCalculateReq
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/codex/baseCouponIntegralQuery", method = RequestMethod.POST)
	@ResponseBody
	public BaseCouponIntegralQueryRsp baseCouponIntegralQuery(HttpServletRequest request) throws Exception {
		BaseCouponIntegralQueryReq couponIntegralQueryReq=ParamStr2Bean(request,BaseCouponIntegralQueryReq.class);
		BaseCouponIntegralQueryRsp rsp = codexEngineBiz.baseCouponIntegralQuery(couponIntegralQueryReq);
		return rsp;
	}
                                                                                     
	/***
	 * 红包、积分 最优查询
	 * @throws Exception 
	 */
	@RequestMapping(value = "/codex/optimumCouponIntegralQuery", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> couponReceive(HttpServletRequest request) throws Exception {
		BestCouponIntegralQueryReq bestCouponIntegralQueryReq=ParamStr2Bean(request,BestCouponIntegralQueryReq.class);
		Map<String, Object> rsp = codexEngineBiz.queryBestCoupon(bestCouponIntegralQueryReq);
		return rsp;
	}

	/***
	 * 红包 手动领取
	 * @throws Exception 
	 */
	@RequestMapping(value = "/codex/couponIntegralReceive", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> couponIntegralReceive(HttpServletRequest request) throws Exception {
		CouponReceiveReq couponReceiveReq=ParamStr2Bean(request,CouponReceiveReq.class);
		Map<String, Object> rsp =  codexEngineBiz.couponReceive(couponReceiveReq);
		return rsp;
	}
	/***
	 * 红包优惠信息查询
	 * @throws Exception 
	 */
	@RequestMapping(value = "/codex/couponPreferential", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> couponPreferential(HttpServletRequest request) throws Exception {
		CouponInfoQueryReq couponInfoQueryReq=ParamStr2Bean(request,CouponInfoQueryReq.class);
		Map<String, Object> rsp = codexEngineBiz.selectCouponPreferential(couponInfoQueryReq);
		return rsp;
	}
	/**
	 * 查询红包使用情况，已使用、待使用、已过期
	 * @throws Exception 
	 */
	 @RequestMapping(value = "/codex/couponAll", method = RequestMethod.POST)
	 @ResponseBody
	 public Map<String, Object> couponAll(HttpServletRequest request) throws Exception {
		 CouponStatusReq couponStatusReq=ParamStr2Bean(request,CouponStatusReq.class);
		 Map<String, Object> map = codexEngineBiz.couponStatus(couponStatusReq);
		 return map;
	 }
	/***
	 * 红包、积分消费服务
	 * @throws Exception 
	 */
	@RequestMapping(value = "/codex/couponIntegralConsume", method = RequestMethod.POST)
	@ResponseBody
	public BaseResponse couponIntegralConsume(HttpServletRequest request) throws Exception {
		CouponIntegralUseReq couponIntegralUseReq=ParamStr2Bean(request,CouponIntegralUseReq.class);
		BaseResponse baseResponse = codexEngineBiz.consume(couponIntegralUseReq);
		return baseResponse;
	}

	/***
	 * 红包、积分退款服务
	 * @throws Exception 
	 */
	@RequestMapping(value = "/codex/couponIntegralRefund", method = RequestMethod.POST)
	@ResponseBody
	public BaseResponse couponIntegralRefund(HttpServletRequest request) throws Exception {
		CouponIntegralRefundReq accountingBalanceRechargeReq=ParamStr2Bean(request,CouponIntegralRefundReq.class);
		BaseResponse baseResponse = codexEngineBiz.refund(accountingBalanceRechargeReq);
		return baseResponse;
	}

	
	/***
	 * 小程序-获取可领取红包列表
	 * 
	 * param:场景ID
	 * @throws Exception 
	 */
	@RequestMapping(value = "/codex/getCouponBySceneId", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getCouponBySceneId(HttpServletRequest request) throws Exception {
		WXCouponRevReq wxCouponRevReq=ParamStr2Bean(request,WXCouponRevReq.class);
		Map<String, Object> resp = codexEngineBiz.getCouponBySceneId(wxCouponRevReq);
		return resp;
	}
	
	
	/***
	 * 小程序-积分手动领取
	 * @throws Exception 
	 */
	@RequestMapping(value = "/codex/integralReceive", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> integralReceive(HttpServletRequest request) throws Exception {
		IntegralReceiveReq integralReceiveReq=ParamStr2Bean(request,IntegralReceiveReq.class);
		Map<String, Object> rsp =  codexEngineBiz.integralReceive(integralReceiveReq);
		return rsp;
	}

}
