package com.github.wxiaoqi.security.biz.modules.restaurant.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantReservationInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantReservationInfo;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序点餐预订信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/reservation")
public class RestaurantReservationInfoRestController extends BaseController<RestaurantReservationInfoBiz, RestaurantReservationInfo> {
	
	@Autowired
	private RestaurantReservationInfoBiz restaurantReservationInfoBiz;
	
	/**
	 * 查询点餐预订信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<RestaurantReservationInfo> query(@RequestBody RestaurantQueryParam param) {
		log.info("查询点餐预订信息列表------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return restaurantReservationInfoBiz.queryRestaurantReservationInfoPage(param);
	}

}
