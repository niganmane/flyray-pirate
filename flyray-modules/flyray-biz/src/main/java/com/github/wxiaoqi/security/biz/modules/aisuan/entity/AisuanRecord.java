package com.github.wxiaoqi.security.biz.modules.aisuan.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-10 10:58:58
 */
@Table(name = "aisuan_record")
public class AisuanRecord implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //主键
    @Id
    private Long id;
	
	    //平台编号
    @Column(name = "platform_id")
    private Long platformId;
	
	    //商户编号
    @Column(name = "merchant_id")
    private Long merchantId;
	
	    //咨询人编号
    @Column(name = "customer_id")
    private Long customerId;
	
	    //1、一事一测2、一年运势预测3、八字简批4、个人起名改名5、单位企业起名6、商标命名7、开业择日8、大型户外活动择日，策划9、结婚择日10、八字合婚11、免费风水咨询
    @Column(name = "type")
    private Integer type;
	
	    //提交时间
    @Column(name = "create_time")
    private Date createTime;
	
	
	    //咨询内容
    @Column(name = "content")
    private String content;
	
	
	    //1、已付款2、未付款3、付款失败4、已退款5、退款失败
    @Column(name = "pay_status")
    private Integer payStatus;
	
	    //1、待回复2、已回复3、已完成
    @Column(name = "order_status")
    private Integer orderStatus;
	
	    //回复人编号
    @Column(name = "apply_user_id")
    private Long applyUserId;
    
    @Column(name = "parent_id")
    private Long parentId;
    
    //最顶级咨询的id为0，方便区分哪些回复是一个咨询的
    @Column(name = "top_id")
    private Long topId;
	
    @Column(name = "pay_order_no")
    private String payOrderNo;
    
    @Column(name = "refund_order_no")
    private String refundOrderNo;
    
    @Column(name = "target_tea")
    private Long targetTea;
    
    @Column(name = "is_target")
    private Integer isTarget;
    
    @Column(name = "tea_name")
    private String teaName;
    	//实际支付金额
    @Column(name = "amt")
    private String amt;
    //1预测(1、一事一测2、一年运势预测3、八字简批)2起名(4、个人起名改名5、单位企业起名6、商标命名)3择日(7、开业择日8、大型户外活动择日，策划9、结婚择日)4八字合婚(10、八字合婚),5免费咨询(11、免费风水咨询)6八字排盘(12八字排盘)7六爻起卦(13六爻起卦)
    
    @Column(name = "first_level")
    private Integer firstLevel;
   
    
    
    
    
    
	public Integer getFirstLevel() {
		return firstLevel;
	}
	public void setFirstLevel(Integer firstLevel) {
		this.firstLevel = firstLevel;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getTeaName() {
		return teaName;
	}
	public void setTeaName(String teaName) {
		this.teaName = teaName;
	}
	public Long getTargetTea() {
		return targetTea;
	}
	public void setTargetTea(Long targetTea) {
		this.targetTea = targetTea;
	}
	public Integer getIsTarget() {
		return isTarget;
	}
	public void setIsTarget(Integer isTarget) {
		this.isTarget = isTarget;
	}
	public String getPayOrderNo() {
		return payOrderNo;
	}
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	public String getRefundOrderNo() {
		return refundOrderNo;
	}
	public void setRefundOrderNo(String refundOrderNo) {
		this.refundOrderNo = refundOrderNo;
	}
	public Long getTopId() {
		return topId;
	}
	public void setTopId(Long topId) {
		this.topId = topId;
	}
	/**
	 * 设置：主键
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public Long getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户编号
	 */
	public Long getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：客户编号
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：客户编号
	 */
	public Long getCustomerId() {
		return customerId;
	}
	/**
	 * 设置：1、一事一测2、一年运势预测3、八字简批4、个人起名改名5、单位企业起名6、商标命名7、开业择日8、大型户外活动择日，策划9、结婚择日10、八字合婚11、免费风水咨询
	 */
	public void setType(Integer type) {
		this.type = type;
	}
	/**
	 * 获取：1、一事一测2、一年运势预测3、八字简批4、个人起名改名5、单位企业起名6、商标命名7、开业择日8、大型户外活动择日，策划9、结婚择日10、八字合婚11、免费风水咨询
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 设置：提交时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：提交时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：咨询内容
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：咨询内容
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：1、已付款2、未付款3、付款失败4、已退款5、退款失败
	 */
	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}
	/**
	 * 获取：1、已付款2、未付款3、付款失败4、已退款5、退款失败
	 */
	public Integer getPayStatus() {
		return payStatus;
	}
	/**
	 * 设置：1、待回复2、已回复3、已完成
	 */
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}
	/**
	 * 获取：1、待回复2、已回复3、已完成
	 */
	public Integer getOrderStatus() {
		return orderStatus;
	}
	/**
	 * 设置：回复人编号
	 */
	public void setApplyUserId(Long applyUserId) {
		this.applyUserId = applyUserId;
	}
	/**
	 * 获取：回复人编号
	 */
	public Long getApplyUserId() {
		return applyUserId;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
}
