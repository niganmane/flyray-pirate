package com.github.wxiaoqi.security.biz.codex.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.wxiaoqi.security.biz.codex.entity.MarIntegral;

import tk.mybatis.mapper.common.Mapper;

/**
 * 积分基本信息表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@org.apache.ibatis.annotations.Mapper
public interface MarIntegralMapper extends Mapper<MarIntegral> {
	/**
	 * 查询积分总数量
	 * @author centerroot
	 * @time 创建时间:2018年2月27日上午9:52:58
	 * @param actId
	 * @return
	 */
	Integer getIntegralNum();
	
	/**
	 * 查询活动中积分总数量
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:35:20
	 * @param actId
	 * @return
	 */
	Integer getActivityIntegralNum(String actId);

	
	/**
	 * 根据活动编号，开始时间，结束时间查询积分列表
	 * @param createTime
	 * @param shutTime
	 * @param actId
	 * @return
	 */
	List<MarIntegral> getIntegralList(@Param("createTime") String createTime,@Param("shutTime") String shutTime,@Param("actId") String actId);

	/**
	 * 状态置为可领取
	 */
	void changStatusToOpen();
	/**
	 * 状态置为不可领取
	 */
	void changStatusToClose();
	
	/**
	 * 查询单个红包
	 */
	MarIntegral selectByIntegralId(@Param("integralId") String integralId);
	
	/**
	 * 根据事件查询积分列表
	 */
	List<MarIntegral> selectIntegralsByIncCode(@Param("incCode") String incCode);
}
