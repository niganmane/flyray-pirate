package com.github.wxiaoqi.security.biz.modules.fightGroup.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.fightGroup.biz.FightGroupInfoBiz;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupInfo;
import com.github.wxiaoqi.security.common.cms.request.FightGroupQueryParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序拼团团信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("fightGroup/groupInfo")
public class FightGroupInfoRestController extends BaseController<FightGroupInfoBiz, FightGroupInfo> {
	
	@Autowired
	private FightGroupInfoBiz fightGroupInfoBiz;
	
	/**
	 * 查询拼团团信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<FightGroupInfo> query(@RequestBody FightGroupQueryParam param) {
		log.info("查询拼团团信息列表------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return fightGroupInfoBiz.queryFightGroupInfoPage(param);
	}
	
}
