package com.github.wxiaoqi.security.biz.modules.pay.biz;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;

import com.alibaba.druid.support.json.JSONUtils;
import com.alibaba.fastjson.JSONObject;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.feign.CrmFeign;
import com.github.wxiaoqi.security.biz.feign.MallFeign;
import com.github.wxiaoqi.security.biz.feign.PayFeign;
import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanRecommend;
import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanRecord;
import com.github.wxiaoqi.security.biz.modules.aisuan.mapper.AisuanRecommendMapper;
import com.github.wxiaoqi.security.biz.modules.aisuan.mapper.AisuanRecordMapper;
import com.github.wxiaoqi.security.biz.modules.auction.entity.AuctionOrder;
import com.github.wxiaoqi.security.biz.modules.auction.mapper.AuctionOrderMapper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupOrder;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupOrderMapper;
import com.github.wxiaoqi.security.biz.modules.pay.entity.CmsPayOrder;
import com.github.wxiaoqi.security.biz.modules.pay.mapper.CmsPayOrderMapper;
import com.github.wxiaoqi.security.biz.modules.refund.biz.RefundBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantOrderInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantOrderInfoMapper;
import com.github.wxiaoqi.security.common.enums.AccountTransferTypeEnums;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

/**
 * 支付
 * @author he
 *
 */
@Service
public class PayBiz {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(PayBiz.class);

	@Autowired
	private PayFeign payFeign;
	@Autowired
	private RefundBiz refundBiz;
	@Autowired
	private CrmFeign crmFeign;
	@Autowired
	private MallFeign mallFeign;
	@Autowired
	private CmsPayOrderMapper cmsPayOrderMapper;
	@Autowired
	private AuctionOrderMapper auctionOrderMapper;
	@Autowired
	private FightGroupOrderMapper fightGroupOrderMapper;
	@Autowired
	private RestaurantOrderInfoMapper restaurantOrderInfoMapper;
	@Autowired
	private AisuanRecordMapper aisuanRecordMapper;
	@Autowired
	private AisuanRecommendMapper aisuanRecommendMapper;

	/**
	 * 创建订单并支付
	 * @param request
	 * @return
	 */
	public Map<String, Object> createAndPay(@RequestBody Map<String, Object> params){

		logger.info("创建订单并支付请求.......{}",params);
		Map<String, Object> respMap = new HashMap<String, Object>();

		//创建订单
		String payOrderNo = String.valueOf(SnowFlake.getId());
		params.put("payOrderNo", payOrderNo);
		Map<String, Object> createPayOrderRespMap = payCoreCreatePayOrder(params);
		if(!ResponseCode.OK.getCode().equals(createPayOrderRespMap.get("code"))){
			logger.info("创建订单并支付失败.......{}",createPayOrderRespMap.get("msg"));
			respMap.put("success", false);
			respMap.put("code", createPayOrderRespMap.get("code"));
			respMap.put("msg", createPayOrderRespMap.get("msg"));
			return respMap;
		}

		BigDecimal fee = BigDecimal.ZERO;
		//计算手续费
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("platformId", (String)params.get("platformId"));
		param.put("merchantId", (String)params.get("merchantId"));
		param.put("payChannelNo", (String)params.get("payChannelNo"));
		param.put("payAmt", (String)params.get("payAmt"));
		param.put("outCustomerId", (String)params.get("outCustomerId"));
		Map<String, Object> queryMerChannelFee = crmFeign.queryMerChannelFee(param);
		logger.info("创建订单并支付手续费.......{}",queryMerChannelFee);
		if(ResponseCode.OK.getCode().equals(queryMerChannelFee.get("code"))){
			String feeStr = (String)queryMerChannelFee.get("fee");
			fee = new BigDecimal(feeStr);
		}
		
		//发起支付
		Map<String, Object> doPayMap = payCoreDoPay(params);
		CmsPayOrder cmsPayOrder = new CmsPayOrder();
		if(!ResponseCode.OK.getCode().equals(doPayMap.get("code"))){
			//创建支付订单
			cmsPayOrder.setTxStatus("01");//支付失败
			logger.info("创建订单并支付失败.......{}",doPayMap.get("msg"));
			respMap.put("success", false);
			respMap.put("code", doPayMap.get("code"));
			respMap.put("msg", doPayMap.get("msg"));
		}else{
			//创建支付订单
			cmsPayOrder.setTxStatus("03");//支付中
			//创建场景订单
			this.createScenesOrder(params);
			respMap.put("payObject", doPayMap.get("payObject"));
			respMap.put("payOrderNo", payOrderNo);
			respMap.put("success", true);
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
			//新增场景订单
			cmsPayOrder.setPlatformId((String)params.get("platformId"));
			cmsPayOrder.setMerchantId((String)params.get("merchantId"));
			cmsPayOrder.setCustomerId((String)params.get("customerId"));
			cmsPayOrder.setPayOrderNo((String)params.get("payOrderNo"));
			cmsPayOrder.setPayCode((String)params.get("payCode"));
			cmsPayOrder.setPersonalId((String)params.get("personalId"));
			String scenesCode = (String) params.get("scenesCode");
			cmsPayOrder.setScenesCode(scenesCode);
			cmsPayOrder.setTxAmt(new BigDecimal((String)params.get("payAmt")));
			cmsPayOrder.setCreateTime(new Date());
			cmsPayOrder.setExtMap((String)params.get("extMap"));
			cmsPayOrder.setTxFee(fee);
			cmsPayOrderMapper.insert(cmsPayOrder);
		}
		logger.info("创建订单并支付响应.......{}",respMap);
		return respMap;
	}

	/**
	 * 创建订单
	 * @param request
	 * @return
	 */
	public Map<String, Object> createPayOrder(@RequestBody Map<String, Object> params){

		logger.info("创建订单请求.......{}",params);
		Map<String, Object> respMap = new HashMap<String, Object>();
		//创建订单
		String payOrderNo = String.valueOf(SnowFlake.getId());
		params.put("payOrderNo", payOrderNo);
		if("1".equals((String)params.get("payType"))){//第三方
			Map<String, Object> createPayOrderRespMap = payCoreCreatePayOrder(params);
			if(!ResponseCode.OK.getCode().equals(createPayOrderRespMap.get("code"))){
				logger.info("创建订单失败.......{}",createPayOrderRespMap.get("msg"));
				respMap.put("success", false);
				respMap.put("code", createPayOrderRespMap.get("code"));
				respMap.put("msg", createPayOrderRespMap.get("msg"));
				return respMap;
			}
		}

		//创建支付订单
		CmsPayOrder cmsPayOrder = new CmsPayOrder();
		cmsPayOrder.setPlatformId((String)params.get("platformId"));
		cmsPayOrder.setMerchantId((String)params.get("merchantId"));
		cmsPayOrder.setCustomerId((String)params.get("customerId"));
		cmsPayOrder.setPersonalId((String)params.get("personalId"));
		cmsPayOrder.setPayOrderNo((String)params.get("payOrderNo"));
		String scenesCode = (String) params.get("scenesCode");
		cmsPayOrder.setScenesCode(scenesCode);
		cmsPayOrder.setTxAmt(new BigDecimal((String)params.get("payAmt")));
		cmsPayOrder.setTxStatus("02");//待支付
		cmsPayOrder.setCreateTime(new Date());
		cmsPayOrder.setPayType((String)params.get("payType"));
		cmsPayOrder.setPayCode((String)params.get("payCode"));
		cmsPayOrder.setExtMap((String)params.get("extMap"));
		cmsPayOrderMapper.insert(cmsPayOrder);

		//创建场景订单
		this.createScenesOrder(params);

		respMap.put("checkPassword", "0");//需要校验交易密码
		respMap.put("payOrderNo", payOrderNo);
		respMap.put("success", true);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		logger.info("创建订单响应.......{}",respMap);
		return respMap;
	}

	/**
	 * 支付
	 * @param request
	 * @return
	 */
	public Map<String, Object> doPay(@RequestBody Map<String, Object> params){
		logger.info("支付请求.......{}",params);
		Map<String, Object> respMap = new HashMap<String, Object>();

		//校验订单是否存在
		CmsPayOrder cmsPayOrder = new CmsPayOrder();
		cmsPayOrder.setPlatformId((String)params.get("platformId"));
		cmsPayOrder.setPayOrderNo((String)params.get("payOrderNo"));
		CmsPayOrder selectCmsPayOrder = cmsPayOrderMapper.selectOne(cmsPayOrder);
		if(null == selectCmsPayOrder){
			logger.info("支付失败.......{}",ResponseCode.ORDER_NO_EXIST.getMessage());
			respMap.put("success", false);
			respMap.put("code", ResponseCode.ORDER_NO_EXIST.getCode());
			respMap.put("msg", ResponseCode.ORDER_NO_EXIST.getMessage());
			return respMap;
		}

		//发起支付   微信支付宝支付调用pay_core，余额支付调用crm_core
		Map<String, Object> doPayMap = new HashMap<String, Object>();
		if("1".equals((String)params.get("payType"))){//第三方
			doPayMap = payCoreDoPay(params);
		}else if("2".equals((String)params.get("payType"))){//火源，火钻，原力值支付
			params.put("personalId", selectCmsPayOrder.getPersonalId());
			params.put("payAmt", String.valueOf(selectCmsPayOrder.getTxAmt()));
			params.put("merchantId", selectCmsPayOrder.getMerchantId());
			doPayMap = crmCoreDoPay(params);
		}

		if(!ResponseCode.OK.getCode().equals(doPayMap.get("code"))){
			//修改支付订单
			selectCmsPayOrder.setTxStatus("01");//支付失败
			logger.info("支付失败.......{}",doPayMap.get("msg"));
			respMap.put("success", false);
			respMap.put("code", doPayMap.get("code"));
			respMap.put("msg", doPayMap.get("msg"));
		}else{
			//修改支付订单
			if("1".equals((String)params.get("payType"))){//第三方
				selectCmsPayOrder.setTxStatus("03");//支付中
			}else if("2".equals((String)params.get("payType"))){//余额支付
				selectCmsPayOrder.setTxStatus("00");//支付成功
			}
			respMap.put("payObject", doPayMap.get("payObject"));
			respMap.put("success", true);
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		}
		cmsPayOrderMapper.updateByPrimaryKey(selectCmsPayOrder);

		//更新场景订单
		String scenesCode = selectCmsPayOrder.getScenesCode();
		if(ResponseCode.OK.getCode().equals(doPayMap.get("code"))){
			params.put("txStatus", "00");
		}
		if("4".equals(scenesCode)){
			//竞拍，只有火源支付
			this.updateAuctionOrder(params);
		}else if("5".equals(scenesCode) || "6".equals(scenesCode)){
			//拼团参团，支持微信和火钻支付//拼团创建团
			this.updateJoinGroupOrder(params);
		}else if("3".equals(scenesCode) || "2".equals(scenesCode) || "1".equals(scenesCode)){
			//店内点餐//预订点餐//外卖
			params.put("scenesCode", scenesCode);
			this.updateOrderDishesOrder(params);
		}else if ("8".equals(scenesCode)) {
			//商城
			if("1".equals((String)params.get("payType"))){//第三方
				params.put("txStatus", "01");
			}else if("2".equals((String)params.get("payType"))){//火源，火钻，原力值支付
				params.put("txStatus", "00");
			}
			this.updateMallOrder(params);
		}

		logger.info("支付响应.......{}",respMap);
		return respMap;
	}

	/**
	 * 调用pay_core创建订单
	 * @param params
	 * @return
	 */
	private Map<String, Object> payCoreCreatePayOrder(Map<String, Object> params){
		Map<String, Object> createPayOrderMap = new HashMap<String, Object>();
		createPayOrderMap.put("platformId", (String)params.get("platformId"));
		createPayOrderMap.put("merchantId", (String)params.get("merchantId"));
		createPayOrderMap.put("customerId", (String)params.get("customerId"));
		createPayOrderMap.put("payOrderNo", (String)params.get("payOrderNo"));
		createPayOrderMap.put("orderAmt", (String)params.get("orderAmt"));
		createPayOrderMap.put("payAmt", (String)params.get("payAmt"));
		createPayOrderMap.put("body", (String)params.get("body"));
		createPayOrderMap.put("payCode", (String)params.get("payCode"));//1支付，2充值
		logger.info("调用pay_core创建订单请求.......{}",createPayOrderMap);
		Map<String, Object> createRespMap = payFeign.createPayOrder(createPayOrderMap);
		logger.info("调用pay_core创建订单响应.......{}",createRespMap);
		return createRespMap;
	}

	/**
	 * 调用pay_core支付
	 * @param params
	 * @return
	 */
	private Map<String, Object> payCoreDoPay(Map<String, Object> params){
		Map<String, Object> doPayMap = new HashMap<String, Object>();
		doPayMap.put("platformId", (String)params.get("platformId"));
		doPayMap.put("payChannelNo", (String)params.get("payChannelNo"));//微信公众号/小程序支付
		doPayMap.put("payCompanyNo", (String)params.get("payCompanyNo"));//微信
		doPayMap.put("payOrderNo", (String)params.get("payOrderNo"));
		Map<String, Object> extValue = new HashMap<String, Object>();
		extValue.put("openId", (String)params.get("openId"));
		String extString = JSONUtils.toJSONString(extValue);
		doPayMap.put("extValue", extString);
		doPayMap.put("payFee", (String)params.get("payFee"));
		logger.info("调用pay_core支付请求.......{}",doPayMap);
		Map<String, Object> doPayRespMap = payFeign.doPay(doPayMap);
		logger.info("调用pay_core支付响应.......{}",doPayRespMap);
		return doPayRespMap;
	}

	/**
	 * 调用crm_core支付并校验交易密码
	 * @param params
	 * @return
	 */
	private Map<String, Object> crmCoreDoPay(Map<String, Object> params){
		Map<String, Object> doPayMap = new HashMap<String, Object>();
		doPayMap.put("platformId", (String)params.get("platformId"));
		doPayMap.put("transferAmt", (String)params.get("payAmt"));
		doPayMap.put("transferType", AccountTransferTypeEnums.p2m.getCode());
		doPayMap.put("intoAccountId", (String)params.get("merchantId"));
		doPayMap.put("intoAccountType", (String)params.get("accountType"));
		doPayMap.put("intoCustomerType", "CUST01");//商户
		doPayMap.put("outAccountId", (String)params.get("personalId"));
		doPayMap.put("outAccountType", (String)params.get("accountType"));
		doPayMap.put("outCustomerType", "CUST02");//用户
		doPayMap.put("tradeType", "01");//支付
		doPayMap.put("orderNo", (String)params.get("payOrderNo"));
		Map<String, Object> respMap = crmFeign.balancePay(doPayMap);
		return respMap;
	}

	/**
	 * 创建场景订单
	 * @param params
	 * @return
	 */
	private void createScenesOrder(Map<String, Object> params){
		String scenesCode = (String) params.get("scenesCode");
		//添加各场景相关订单表
		if("4".equals(scenesCode)){
			//竞拍
			this.createAuctionOrder(params);
		}else if("5".equals(scenesCode) || "6".equals(scenesCode)){
			//拼团参团//拼团创建团
			this.createFightGroupOrder(params);
		}else if("3".equals(scenesCode) || "2".equals(scenesCode) || "1".equals(scenesCode)){
			//店内点餐//预订点餐//外卖
			this.createOrderDishesOrder(params);
		}else if ("8".equals(scenesCode)) {
			//商城
			logger.info("创建商城订单。。。。{}"+params);
			this.createMallOrder(params);
		}else if("10".equals(scenesCode)){
			logger.info("创建创建爱算订单。。。。{}"+params);
			this.createFortuneOrder(params);
		}
	}
	private  void createFortuneOrder(Map<String, Object> params){
		String extMap = (String) params.get("extMap");
		JSONObject jsonExtMap = (JSONObject) JSONObject.parse(extMap);
		AisuanRecord aisuanRecord = new AisuanRecord();
		aisuanRecord.setId(SnowFlake.getId());
		aisuanRecord.setPlatformId(Long.valueOf((String) params.get("platformId")));
		if(!StringUtils.isEmpty(params.get("merchantId"))){
			aisuanRecord.setMerchantId(Long.valueOf((String) params.get("merchantId")));
		}
		aisuanRecord.setCustomerId(Long.valueOf((String) params.get("customerId")));
		aisuanRecord.setCreateTime(new Date());
		aisuanRecord.setContent((String) jsonExtMap.get("content"));
		aisuanRecord.setOrderStatus(1);
		aisuanRecord.setPayStatus(2);
		aisuanRecord.setPayOrderNo((String) params.get("payOrderNo"));
		Integer type = Integer.valueOf((String) jsonExtMap.get("type"));
		aisuanRecord.setType(type);//
		//1预测(1、一事一测2、一年运势预测3、八字简批)
		if(type == 1 || type == 2 || type == 3){
			aisuanRecord.setFirstLevel(1);
		}
		//2起名(4、个人起名改名5、单位企业起名6、商标命名)
		if(type == 4 || type == 5 || type == 6){
			aisuanRecord.setFirstLevel(2);
		}
		//3择日(7、开业择日8、大型户外活动择日，策划9、结婚择日)
		if(type == 7 || type == 8 || type == 9){
			aisuanRecord.setFirstLevel(3);
		}
		//4八字合婚(10、八字合婚)
		if(type == 10){
			aisuanRecord.setFirstLevel(4);
		}
		//5免费咨询(11、免费风水咨询)
		if(type == 11){
			aisuanRecord.setFirstLevel(5);
		}
		//6八字排盘(12八字排盘)
		if(type == 12){
			aisuanRecord.setFirstLevel(6);
		}
		//7六爻起卦(13六爻起卦)
		if(type == 13){
			aisuanRecord.setFirstLevel(7);
		}
		Integer isTarget = (Integer) jsonExtMap.get("isTarget");
		aisuanRecord.setIsTarget(isTarget);
		if(isTarget == 2){
			String id = (String) jsonExtMap.get("targetTea");
			aisuanRecord.setTargetTea(Long.valueOf(id));
			//查出姓名
			AisuanRecommend recommend = new  AisuanRecommend();
			recommend.setUserId(Long.valueOf(id));
			List<AisuanRecommend> recommendList =  aisuanRecommendMapper.select(recommend);
			if(recommendList.size() > 0){
				aisuanRecord.setTeaName(recommendList.get(0).getUserName());
			}
		}
		aisuanRecord.setParentId(Long.valueOf("0"));
		aisuanRecord.setTopId(Long.valueOf("0"));
		aisuanRecordMapper.insertSelective(aisuanRecord);
	}
	/**
	 * 创建竞拍订单
	 * @param params
	 */
	private void createAuctionOrder(Map<String, Object> params){
		String extMap = (String) params.get("extMap");
		JSONObject jsonExtMap = (JSONObject) JSONObject.parse(extMap);
		AuctionOrder auctionOrder = new AuctionOrder();
		auctionOrder.setPlatformId((String) params.get("platformId"));
		auctionOrder.setMerchantId((String) params.get("merchantId"));
		auctionOrder.setGoodsId((String)jsonExtMap.get("goodsId"));
		auctionOrder.setPayOrderNo((String) params.get("payOrderNo"));
		auctionOrder.setCreateTime(new Date());
		auctionOrder.setPerId((String) params.get("customerId"));
		auctionOrder.setTxAmt(new BigDecimal((String) params.get("payAmt")));
		auctionOrderMapper.insert(auctionOrder);
	}

	/**
	 * 创建拼团订单
	 * @param params
	 */
	private void createFightGroupOrder(Map<String, Object> params){
		FightGroupOrder fightGroupOrder = new FightGroupOrder();
		fightGroupOrder.setPlatformId((String) params.get("platformId"));
		fightGroupOrder.setMerchantId((String) params.get("merchantId"));
		String scenesCode = (String) params.get("scenesCode");
		if("5".equals(scenesCode)){
			String extMap = (String) params.get("extMap");
			JSONObject jsonExtMap = (JSONObject) JSONObject.parse(extMap);
			fightGroupOrder.setGroupId((String)jsonExtMap.get("groupId"));
		}
		fightGroupOrder.setPayOrderNo((String) params.get("payOrderNo"));
		fightGroupOrder.setCreateTime(new Date());
		fightGroupOrder.setCustomerId((String) params.get("customerId"));
		fightGroupOrder.setTxAmt(new BigDecimal((String) params.get("payAmt")));
		fightGroupOrderMapper.insert(fightGroupOrder);
	}

	/**
	 * 创建商城订单
	 * @param params
	 */
	private Map<String, Object> createMallOrder(Map<String, Object> params){
		Map<String, Object> respMap = mallFeign.createPayOrder(params);

		return respMap;
	}


	/**
	 * 修改商城订单状态
	 * @param params
	 */
	private void updateMallOrder(Map<String, Object> params){
		Map<String, Object> respMap = mallFeign.updatePayOrder(params);
		logger.info("修改商城订单状态。。。{}"+respMap);
	}


	/**
	 * 创建店内点餐订单
	 * @param params
	 */
	private void createOrderDishesOrder(Map<String, Object> params){
		RestaurantOrderInfo restaurantOrderInfo = new RestaurantOrderInfo();
		restaurantOrderInfo.setPlatformId((String) params.get("platformId"));
		restaurantOrderInfo.setMerchantId((String) params.get("merchantId"));
		restaurantOrderInfo.setCustomerId((String) params.get("customerId"));
		restaurantOrderInfo.setPayOrderNo((String) params.get("payOrderNo"));
		restaurantOrderInfo.setPayTime(new Date());
		restaurantOrderInfo.setPrice(new BigDecimal((String) params.get("payAmt")));
		restaurantOrderInfo.setRemark((String) params.get("body"));
		String extMap = (String) params.get("extMap");
		JSONObject jsonExtMap = (JSONObject) JSONObject.parse(extMap);
		if("3".equals((String) params.get("scenesCode"))){
			restaurantOrderInfo.setTableId((String)jsonExtMap.get("tableId"));
		}else if("1".equals((String) params.get("scenesCode"))){
			restaurantOrderInfo.setDistributionId((String)jsonExtMap.get("distributionId"));
		}else if("2".equals((String) params.get("scenesCode"))){
			restaurantOrderInfo.setReservationId((String)jsonExtMap.get("reservationId"));
		}
		if("3".equals((String) params.get("scenesCode"))){
			restaurantOrderInfo.setType("1");//店内点餐
		}else if("2".equals((String) params.get("scenesCode"))){
			restaurantOrderInfo.setType("2");//预订
		}else if("1".equals((String) params.get("scenesCode"))){
			restaurantOrderInfo.setType("3");//外卖
		}
		restaurantOrderInfoMapper.insert(restaurantOrderInfo);
	}

	/**
	 * 竞拍订单处理
	 * @param params
	 */
	private void updateAuctionOrder(Map<String, Object> params){
		//根据订单号查询竞拍订单
		AuctionOrder queryAuctionOrder = new AuctionOrder();
		queryAuctionOrder.setPlatformId((String) params.get("platformId"));
		queryAuctionOrder.setPayOrderNo((String) params.get("payOrderNo"));
		AuctionOrder queryOrder = auctionOrderMapper.queryLastOne(queryAuctionOrder);
		if("00".equals((String) params.get("txStatus"))){

			//查询该商品最后一条付款成功的记录
			AuctionOrder auctionOrder = new AuctionOrder();
			auctionOrder.setPlatformId((String) params.get("platformId"));
			auctionOrder.setGoodsId(queryOrder.getGoodsId());
			auctionOrder.setStatus("00");//支付成功
			AuctionOrder lastAuctionOrder = auctionOrderMapper.queryLastOne(auctionOrder);
			queryOrder.setStatus("00");//支付成功
			if(null == lastAuctionOrder){
				//记录为空，则更新为序列为1的记录
				queryOrder.setNumber(1);
			}else{
				//如果不为空，则更新为序列加1的记录，并将原记录发起退款
				queryOrder.setNumber(lastAuctionOrder.getNumber() + 1);
				//发起退款
				Map<String, Object> refundMap = new HashMap<String, Object>();
				refundMap.put("platformId", lastAuctionOrder.getPlatformId());
				refundMap.put("payOrderNo", lastAuctionOrder.getPayOrderNo());
				refundMap.put("refundAmt", String.valueOf(lastAuctionOrder.getTxAmt()));
				Map<String, Object> refundRespMap = this.refund(refundMap);
				if(!ResponseCode.OK.getCode().equals(refundRespMap.get("code"))){
					lastAuctionOrder.setStatus("03");//退款失败
				}else{
					lastAuctionOrder.setStatus("02");//退款成功
				}
				auctionOrderMapper.updateStatus(lastAuctionOrder);
			}
		}else{
			queryOrder.setStatus("01");//支付失败
		}
		auctionOrderMapper.updateByPrimaryKey(queryOrder);
	}

	/**
	 * 参团订单处理
	 * @param params
	 */
	private void updateJoinGroupOrder(Map<String, Object> params){
		FightGroupOrder fightGroupOrder = new FightGroupOrder();
		fightGroupOrder.setPlatformId((String) params.get("platformId"));
		fightGroupOrder.setMerchantId((String) params.get("merchantId"));
		fightGroupOrder.setPayOrderNo((String) params.get("payOrderNo"));
		FightGroupOrder selectFightGroupOrder = fightGroupOrderMapper.selectOne(fightGroupOrder);
		if(null != selectFightGroupOrder){
			if("1".equals((String)params.get("payType"))){//第三方
				if(!"00".equals((String) params.get("txStatus"))){
					selectFightGroupOrder.setStatus("01");//支付失败
				}
			}else{
				if("00".equals((String) params.get("txStatus"))){
					selectFightGroupOrder.setStatus("00");//支付成功
				}else{
					selectFightGroupOrder.setStatus("01");//支付失败
				}
			}
			fightGroupOrderMapper.updateByPrimaryKey(selectFightGroupOrder);
		}
	}

	/**
	 * 店内点餐订单处理
	 * @param params
	 */
	private void updateOrderDishesOrder(Map<String, Object> params){
		RestaurantOrderInfo restaurantOrderInfo = new RestaurantOrderInfo();
		restaurantOrderInfo.setPlatformId((String) params.get("platformId"));
		restaurantOrderInfo.setMerchantId((String) params.get("merchantId"));
		restaurantOrderInfo.setPayOrderNo((String) params.get("payOrderNo"));
		RestaurantOrderInfo selectRestaurantOrderInfo = restaurantOrderInfoMapper.selectOne(restaurantOrderInfo);
		if(null != selectRestaurantOrderInfo){
			if("1".equals((String)params.get("payType"))){//第三方
				if(!"00".equals((String) params.get("txStatus"))){
					selectRestaurantOrderInfo.setStatus("01");//支付失败
				}
			}else{
				if("00".equals((String) params.get("txStatus"))){
					selectRestaurantOrderInfo.setStatus("00");//支付成功
				}else{
					selectRestaurantOrderInfo.setStatus("01");//支付失败
				}
			}
			if("1".equals((String) params.get("scenesCode"))){
				selectRestaurantOrderInfo.setIsConfirm("1");
			}
			restaurantOrderInfoMapper.updateByPrimaryKey(selectRestaurantOrderInfo);
		}
	}

	/**
	 * 发起退款
	 */
	private Map<String, Object> refund(Map<String, Object> params){
		Map<String, Object> refundMap = new HashMap<String, Object>();
		refundMap.put("platformId", (String) params.get("platformId"));
		refundMap.put("payOrderNo", (String) params.get("payOrderNo"));
		refundMap.put("refundAmt", (String) params.get("refundAmt"));
		Map<String, Object> refundRespMap = refundBiz.applyAndRefund(refundMap);
		return refundRespMap;
	}
}
