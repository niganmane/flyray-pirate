package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 随机立减使用记录表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Table(name = "mar_random_use")
public class MarRandomUse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Long seqNo;
    
    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
    
    //商户号
    @Column(name = "MERCHANT_ID")
    private String merchantId;
	
	    //随机立减id
    @Column(name = "SUB_ID")
    private String subId;
	
	    //随机立减流水号
    @Column(name = "SUB_SERIAL")
    private String subSerial;
	
	    //活动id
    @Column(name = "ACT_ID")
    private String actId;
	
	    //场景id
    @Column(name = "SCENE_ID")
    private String sceneId;
	
	    //事件编号
    @Column(name = "INC_CODE")
    private String incCode;
	
	    //商户会员号
    @Column(name = "personal_id")
    private String personalId;
	
	    //支付定单号
    @Column(name = "MERORDERNO")
    private String merorderno;
	
	
	    //订单状态 00已支付
    @Column(name = "TXSTATUS")
    private String txstatus;
	
	    //创建时间
    @Column(name = "USE_TIME")
    private Date useTime;
	
	    //随机立减金额
    @Column(name = "SUB_AMT")
    private BigDecimal subAmt;
	

	/**
	 * 设置：
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：
	 */
	public Long getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：随机立减id
	 */
	public void setSubId(String subId) {
		this.subId = subId;
	}
	/**
	 * 获取：随机立减id
	 */
	public String getSubId() {
		return subId;
	}
	/**
	 * 设置：随机立减流水号
	 */
	public void setSubSerial(String subSerial) {
		this.subSerial = subSerial;
	}
	/**
	 * 获取：随机立减流水号
	 */
	public String getSubSerial() {
		return subSerial;
	}
	/**
	 * 设置：活动id
	 */
	public void setActId(String actId) {
		this.actId = actId;
	}
	/**
	 * 获取：活动id
	 */
	public String getActId() {
		return actId;
	}
	/**
	 * 设置：场景id
	 */
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	/**
	 * 获取：场景id
	 */
	public String getSceneId() {
		return sceneId;
	}
	/**
	 * 设置：事件编号
	 */
	public void setIncCode(String incCode) {
		this.incCode = incCode;
	}
	/**
	 * 获取：事件编号
	 */
	public String getIncCode() {
		return incCode;
	}
	
	/**
	 * 设置：支付定单号
	 */
	public void setMerorderno(String merorderno) {
		this.merorderno = merorderno;
	}
	/**
	 * 获取：支付定单号
	 */
	public String getMerorderno() {
		return merorderno;
	}

	/**
	 * 设置：订单状态 00已支付
	 */
	public void setTxstatus(String txstatus) {
		this.txstatus = txstatus;
	}
	/**
	 * 获取：订单状态 00已支付
	 */
	public String getTxstatus() {
		return txstatus;
	}
	/**
	 * 设置：创建时间
	 */
	public void setUseTime(Date useTime) {
		this.useTime = useTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getUseTime() {
		return useTime;
	}
	/**
	 * 设置：随机立减金额
	 */
	public void setSubAmt(BigDecimal subAmt) {
		this.subAmt = subAmt;
	}
	/**
	 * 获取：随机立减金额
	 */
	public BigDecimal getSubAmt() {
		return subAmt;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getPersonalId() {
		return personalId;
	}
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	
}
