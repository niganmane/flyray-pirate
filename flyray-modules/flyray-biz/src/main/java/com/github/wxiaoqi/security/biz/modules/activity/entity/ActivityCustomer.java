package com.github.wxiaoqi.security.biz.modules.activity.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-19 10:54:46
 */
@Table(name = "cms_activity_customer")
public class ActivityCustomer implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
    //序号
	@Id
	private String id;
	
	//活动序号
    @Column(name = "activity_id")
    private String activityId;
	
	//用户序号
    @Column(name = "customer_id")
    private String customerId;
	
	//0未加入 1已加入
    @Column(name = "joinStatus")
    private String join;
	
	//真实姓名
    @Column(name = "real_name")
    private String realName;
	
	//联系号码
    @Column(name = "contact_value")
    private String contactValue;
	
	//联系方式     0微信号 1QQ号 2手机号
    @Column(name = "contact_way")
    private Integer contactWay;
    
    @Column(name = "create_time")
    private Timestamp createTime;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 设置：活动序号
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	/**
	 * 获取：活动序号
	 */
	public String getActivityId() {
		return activityId;
	}
	/**
	 * 设置：用户序号
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：用户序号
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * 设置：0未加入 1已加入
	 */
	public void setJoin(String join) {
		this.join = join;
	}
	/**
	 * 获取：0未加入 1已加入
	 */
	public String getJoin() {
		return join;
	}
	/**
	 * 设置：真实姓名
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}
	/**
	 * 获取：真实姓名
	 */
	public String getRealName() {
		return realName;
	}
	/**
	 * 设置：联系号码
	 */
	public void setContactValue(String contactValue) {
		this.contactValue = contactValue;
	}
	/**
	 * 获取：联系号码
	 */
	public String getContactValue() {
		return contactValue;
	}
	/**
	 * 设置：联系方式     0微信号 1QQ号 2手机号
	 */
	public void setContactWay(Integer contactWay) {
		this.contactWay = contactWay;
	}
	/**
	 * 获取：联系方式     0微信号 1QQ号 2手机号
	 */
	public Integer getContactWay() {
		return contactWay;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	@Override
	public String toString() {
		return "ActivityCustomer [id=" + id + ", activityId=" + activityId + ", customerId=" + customerId + ", join="
				+ join + ", realName=" + realName + ", contactValue=" + contactValue + ", contactWay=" + contactWay
				+ ", createTime=" + createTime + "]";
	}
	
}
