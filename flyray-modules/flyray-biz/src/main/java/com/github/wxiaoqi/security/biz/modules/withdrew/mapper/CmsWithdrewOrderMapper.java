package com.github.wxiaoqi.security.biz.modules.withdrew.mapper;

import com.github.wxiaoqi.security.biz.modules.withdrew.entity.CmsWithdrewOrder;

import tk.mybatis.mapper.common.Mapper;

/**
 * 提现表
 * @author he
 * @date 2018-09-09 13:37:13
 */
@org.apache.ibatis.annotations.Mapper
public interface CmsWithdrewOrderMapper extends Mapper<CmsWithdrewOrder> {
	
}
