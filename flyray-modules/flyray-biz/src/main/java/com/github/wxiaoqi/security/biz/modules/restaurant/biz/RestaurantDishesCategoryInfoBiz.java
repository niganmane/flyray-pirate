package com.github.wxiaoqi.security.biz.modules.restaurant.biz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantDishesCategoryInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantDishesInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantShoppingCartInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantDishesCategoryInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantDishesInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantShoppingCartInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 菜品类目表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Slf4j
@Service
public class RestaurantDishesCategoryInfoBiz extends BaseBiz<RestaurantDishesCategoryInfoMapper,RestaurantDishesCategoryInfo> {
	

	@Autowired
	private RestaurantDishesCategoryInfoMapper restaurantDishesCategoryInfoMapper;
	@Autowired
	private RestaurantDishesInfoMapper restaurantDishesInfoMapper;
	@Autowired
	private RestaurantInfoMapper restaurantInfoMapper;
	@Autowired
	private RestaurantShoppingCartInfoMapper restaurantShoppingCartInfoMapper;
	
	/**
	 * 点餐餐品信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryOrderDishesInfo(Map<String, Object> request){
		log.info("点餐餐品信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String tableId = (String) request.get("tableId");
		RestaurantDishesCategoryInfo restaurantDishesCategoryInfo = new RestaurantDishesCategoryInfo();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		restaurantDishesCategoryInfo.setPlatformId(platFormId);
		restaurantDishesCategoryInfo.setMerchantId(merId);
		List<RestaurantDishesCategoryInfo> select = restaurantDishesCategoryInfoMapper.select(restaurantDishesCategoryInfo);
		for(RestaurantDishesCategoryInfo infos:select){
			String id = infos.getCategoryId();
			RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
			restaurantDishesInfo.setPlatformId(platFormId);
			restaurantDishesInfo.setMerchantId(merId);
			restaurantDishesInfo.setCategoryId(Integer.valueOf(id));
			List<RestaurantDishesInfo> dishesList = restaurantDishesInfoMapper.queryOrderDishes(restaurantDishesInfo);
			List<Map<String, Object>> dishesMapList = new ArrayList<Map<String, Object>>();
			//查询每一种菜当前用户购物车数量
			for(RestaurantDishesInfo dishesInfo:dishesList){
				RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
				restaurantShoppingCartInfo.setPlatformId(platFormId);
				restaurantShoppingCartInfo.setMerchantId(merId);
				restaurantShoppingCartInfo.setDishesId(dishesInfo.getDishesId());
				restaurantShoppingCartInfo.setTableId(tableId);
				restaurantShoppingCartInfo.setStatus("1");//已选择
				restaurantShoppingCartInfo.setType("1");//点餐
				RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
				Map<String, Object> map = EntityUtils.beanToMap(dishesInfo);
				if(null != selectOne){
					map.put("dishesNum", selectOne.getDishesNum());
				}else{
					map.put("dishesNum", 0);
				}
				dishesMapList.add(map);
			}
			Map<String, Object> dishesMap = EntityUtils.beanToMap(infos);
			dishesMap.put("dishList", dishesMapList);
			list.add(dishesMap);
		}
		//查询餐厅信息
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerchantId(merId);
		List<RestaurantInfo> restaurantInfoList = restaurantInfoMapper.select(restaurantInfo);
		response.put("dishesList", list);
		response.put("restaurantInfoList", restaurantInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("点餐餐品信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 预约餐品信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryReservationDishesInfo(Map<String, Object> request){
		log.info("预约餐品信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String perId = (String) request.get("perId");
		RestaurantDishesCategoryInfo restaurantDishesCategoryInfo = new RestaurantDishesCategoryInfo();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		restaurantDishesCategoryInfo.setPlatformId(platFormId);
		restaurantDishesCategoryInfo.setMerchantId(merId);
		List<RestaurantDishesCategoryInfo> select = restaurantDishesCategoryInfoMapper.select(restaurantDishesCategoryInfo);
		for(RestaurantDishesCategoryInfo infos:select){
			String id = infos.getCategoryId();
			RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
			restaurantDishesInfo.setPlatformId(platFormId);
			restaurantDishesInfo.setMerchantId(merId);
			restaurantDishesInfo.setCategoryId(Integer.valueOf(id));
			List<RestaurantDishesInfo> dishesList = restaurantDishesInfoMapper.queryOrderDishes(restaurantDishesInfo);
			List<Map<String, Object>> dishesMapList = new ArrayList<Map<String, Object>>();
			//查询每一种菜当前用户购物车数量
			for(RestaurantDishesInfo dishesInfo:dishesList){
				RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
				restaurantShoppingCartInfo.setPlatformId(platFormId);
				restaurantShoppingCartInfo.setMerchantId(merId);
				restaurantShoppingCartInfo.setDishesId(dishesInfo.getDishesId());
				restaurantShoppingCartInfo.setPerId(perId);
				restaurantShoppingCartInfo.setStatus("1");//已选择
				restaurantShoppingCartInfo.setType("3");//点餐
				RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
				Map<String, Object> map = EntityUtils.beanToMap(dishesInfo);
				if(null != selectOne){
					map.put("dishesNum", selectOne.getDishesNum());
				}else{
					map.put("dishesNum", 0);
				}
				dishesMapList.add(map);
			}
			Map<String, Object> dishesMap = EntityUtils.beanToMap(infos);
			dishesMap.put("dishList", dishesMapList);
			list.add(dishesMap);
		}
		//查询餐厅信息
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerchantId(merId);
		List<RestaurantInfo> restaurantInfoList = restaurantInfoMapper.select(restaurantInfo);
		response.put("dishesList", list);
		response.put("restaurantInfoList", restaurantInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("预约餐品信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 外卖餐品信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryTakeawayDishesInfo(Map<String, Object> request){
		log.info("外卖餐品信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String perId = (String) request.get("perId");
		RestaurantDishesCategoryInfo restaurantDishesCategoryInfo = new RestaurantDishesCategoryInfo();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		restaurantDishesCategoryInfo.setPlatformId(platFormId);
		restaurantDishesCategoryInfo.setMerchantId(merId);
		List<RestaurantDishesCategoryInfo> select = restaurantDishesCategoryInfoMapper.select(restaurantDishesCategoryInfo);
		for(RestaurantDishesCategoryInfo infos:select){
			String id = infos.getCategoryId();
			RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
			restaurantDishesInfo.setPlatformId(platFormId);
			restaurantDishesInfo.setMerchantId(merId);
			restaurantDishesInfo.setCategoryId(Integer.valueOf(id));
			List<RestaurantDishesInfo> dishesList = restaurantDishesInfoMapper.queryTakeawayDishes(restaurantDishesInfo);
			List<Map<String, Object>> dishesMapList = new ArrayList<Map<String, Object>>();
			//查询每一种菜当前用户购物车数量
			for(RestaurantDishesInfo dishesInfo:dishesList){
				RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
				restaurantShoppingCartInfo.setPlatformId(platFormId);
				restaurantShoppingCartInfo.setMerchantId(merId);
				restaurantShoppingCartInfo.setDishesId(dishesInfo.getDishesId());
				restaurantShoppingCartInfo.setPerId(perId);
				restaurantShoppingCartInfo.setStatus("1");//已选择
				restaurantShoppingCartInfo.setType("2");//外卖
				RestaurantShoppingCartInfo selectOne = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
				Map<String, Object> map = EntityUtils.beanToMap(dishesInfo);
				if(null != selectOne){
					map.put("dishesNum", selectOne.getDishesNum());
				}else{
					map.put("dishesNum", 0);
				}
				dishesMapList.add(map);
			}
			Map<String, Object> dishesMap = EntityUtils.beanToMap(infos);
			dishesMap.put("dishList", dishesMapList);
			list.add(dishesMap);
		}
		//查询餐厅信息
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerchantId(merId);
		List<RestaurantInfo> restaurantInfoList = restaurantInfoMapper.select(restaurantInfo);
		response.put("dishesList", list);
		response.put("restaurantInfoList", restaurantInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("外卖餐品信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询菜品类目列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<RestaurantDishesCategoryInfo> queryRestaurantDishesCategoryInfoPage(RestaurantQueryParam param) {
		log.info("查询菜品类目列表。。。。{}"+param);
		Example example = new Example(RestaurantDishesCategoryInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
//		if (param.getGroupsId() != null && param.getGroupsId().length() > 0) {
//			criteria.andEqualTo("groupsId",param.getGroupsId());
//		}
		if (param.getPage() == null) {
			param.setPage(1);
		}
		if (param.getLimit() == null) {
			param.setLimit(9999);
		}
		Page<RestaurantDishesCategoryInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<RestaurantDishesCategoryInfo> list = mapper.selectByExample(example);
		TableResultResponse<RestaurantDishesCategoryInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteDishesCategory(Map<String, Object> param) {
		log.info("删除菜品类目。。。start。。。。。。。。。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Integer id = (Integer) param.get("id");
			RestaurantDishesCategoryInfo categoryInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (categoryInfo != null) {
				mapper.delete(categoryInfo);
				respMap.put("code", "00");
				respMap.put("msg", "请求成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "记录不存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		log.info("删除菜品类目。。。end。。。。。。。。。。。。{}"+respMap);
		return respMap;
	}
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	public Map<String, Object> addDishesCategory(Map<String, Object> param) {
		log.info("添加菜品类目。。。start。。。。。。。。。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			RestaurantDishesCategoryInfo categoryInfo = new RestaurantDishesCategoryInfo();
			String platformId = (String) param.get("platformId");
			String merId = (String) param.get("merchantId");
			String name = (String) param.get("name");
			categoryInfo.setCategoryId(String.valueOf(SnowFlake.getId()));
			categoryInfo.setMerchantId(merId);
			categoryInfo.setPlatformId(platformId);
			categoryInfo.setName(name);
			mapper.insert(categoryInfo);
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		log.info("添加菜品类目。。。end。。。。。。。。。。。。{}"+respMap);
		return respMap;
	}
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateDishesCategory(Map<String, Object> param) {
		log.info("修改菜品类目。。。start。。。。。。。。。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Integer id = (Integer) param.get("id");
			String name = (String) param.get("name");
			RestaurantDishesCategoryInfo categoryInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (categoryInfo != null) {
				categoryInfo.setName(name);
				mapper.updateByPrimaryKeySelective(categoryInfo);
				respMap.put("code", "00");
				respMap.put("msg", "请求成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "记录不存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		log.info("修改菜品类目。。。end。。。。。。。。。。。。{}"+respMap);
		return respMap;
	}
	
	
}