package com.github.wxiaoqi.security.biz.modules.fightGroup.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 团类目信息表
 * 
 * @author he
 * @date 2018-07-12 10:17:33
 */
@Table(name = "fight_group_category_info")
public class FightGroupCategoryInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//团类目id
	@Column(name = "groups_id")
	private Integer groupsId;

	//商品id
	@Column(name = "goods_id")
	private Integer goodsId;

	//团名称
	@Column(name = "group_name")
	private String groupName;

	//金额
	@Column(name = "tx_amt")
	private BigDecimal txAmt;

	//限制人数
	@Column(name = "people_num")
	private Integer peopleNum;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：团类目id
	 */
	public void setGroupsId(Integer groupsId) {
		this.groupsId = groupsId;
	}
	/**
	 * 获取：团类目id
	 */
	public Integer getGroupsId() {
		return groupsId;
	}
	/**
	 * 设置：商品id
	 */
	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}
	/**
	 * 获取：商品id
	 */
	public Integer getGoodsId() {
		return goodsId;
	}
	/**
	 * 设置：团名称
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	/**
	 * 获取：团名称
	 */
	public String getGroupName() {
		return groupName;
	}
	/**
	 * 设置：金额
	 */
	public void setTxAmt(BigDecimal txAmt) {
		this.txAmt = txAmt;
	}
	/**
	 * 获取：金额
	 */
	public BigDecimal getTxAmt() {
		return txAmt;
	}
	/**
	 * 设置：限制人数
	 */
	public void setPeopleNum(Integer peopleNum) {
		this.peopleNum = peopleNum;
	}
	/**
	 * 获取：限制人数
	 */
	public Integer getPeopleNum() {
		return peopleNum;
	}
	@Override
	public String toString() {
		return "FightGroupCategoryInfo [id=" + id + ", platformId=" + platformId + ", merchantId=" + merchantId + ", groupsId="
				+ groupsId + ", goodsId=" + goodsId + ", groupName=" + groupName + ", txAmt=" + txAmt + ", peopleNum="
				+ peopleNum + "]";
	}
	
}
