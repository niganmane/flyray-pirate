package com.github.wxiaoqi.security.biz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.github.wxiaoqi.security.auth.client.EnableAceAuthClient;
import com.spring4all.swagger.EnableSwagger2Doc;

/**
 * ${DESCRIPTION}
 *
 * @author bolei
 * @create 2018-4-2 20:30
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"com.github.wxiaoqi.security.auth.client.feign","com.github.wxiaoqi.security.biz.client","com.github.wxiaoqi.security.biz.feign"})
@EnableScheduling
@ComponentScan({"com.github.wxiaoqi.security.biz","com.github.wxiaoqi.security.cache"})
@EnableAceAuthClient
@MapperScan("com.github.wxiaoqi.security.biz")
@EnableSwagger2Doc
public class FlyrayBizBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(FlyrayBizBootstrap.class, args);
    }
}
