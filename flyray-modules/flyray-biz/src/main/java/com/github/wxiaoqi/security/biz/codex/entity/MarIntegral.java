package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 积分基本信息表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Table(name = "mar_integral")
public class MarIntegral implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Long seqNo;
    
    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
    
    //商户号
    @Column(name = "MERCHANT_ID")
    private String merchantId;
	
	    //积分编号
    @Column(name = "INTEGRAL_ID")
    private String integralId;
	
	    //使用积分最低金额
    @Column(name = "TOTAL_PAY")
    private BigDecimal totalPay;
	
	    //活动编号
    @Column(name = "ACT_ID")
    private String actId;
	
	    //事件编号 多个事件用逗号分隔
    @Column(name = "INC_CODE")
    private String incCode;
	
	    //事件发送积分总量
    @Column(name = "INTEGRAL_AMT_INC")
    private Integer integralAmtInc;
	
	    //活动创建时间
    @Column(name = "CREATE_TIME")
    private Date createTime;
	
	    //活动结束时间
    @Column(name = "SHUT_TIME")
    private Date shutTime;
	
	    //每次领取积分量
    @Column(name = "INTEGRAL_AMT")
    private Integer integralAmt;
	
	    //积分状态，00可领，01不可领
    @Column(name = "INTEGRAL_STATUS")
    private String integralStatus;
	
	    //积分说明
    @Column(name = "INTEGRALI_INFO")
    private String integraliInfo;
	
	    //积分的使用场景；多个场景用逗号分隔
    @Column(name = "SCENE_ID")
    private String sceneId;
	
	    //每天限领次数
    @Column(name = "EVERY_DAY")
    private Integer everyDay;
	
	    //每周限领次数
    @Column(name = "EVERY_WEEK")
    private Integer everyWeek;
	
	    //领取方式 01自动领取 02手动领取
    @Column(name = "PAYMENT")
    private String payment;
	
	    //每月限领次数
    @Column(name = "EVERY_MONTH")
    private Integer everyMonth;
	
	    //创建时间
    @Column(name = "ADD_TIME")
    private Date addTime;
	
	    //积分发行多少个
    @Column(name = "INC_NUM")
    private Integer incNum;
    /**
     * 活动是否开始
     */
    private String isOpen;

    public String getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(String isOpen) {
		this.isOpen = isOpen;
	}

	/**
	 * 设置：
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：
	 */
	public Long getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：积分编号
	 */
	public void setIntegralId(String integralId) {
		this.integralId = integralId;
	}
	/**
	 * 获取：积分编号
	 */
	public String getIntegralId() {
		return integralId;
	}
	/**
	 * 设置：使用积分最低金额
	 */
	public void setTotalPay(BigDecimal totalPay) {
		this.totalPay = totalPay;
	}
	/**
	 * 获取：使用积分最低金额
	 */
	public BigDecimal getTotalPay() {
		return totalPay;
	}
	/**
	 * 设置：活动编号
	 */
	public void setActId(String actId) {
		this.actId = actId;
	}
	/**
	 * 获取：活动编号
	 */
	public String getActId() {
		return actId;
	}
	/**
	 * 设置：事件编号 多个事件用逗号分隔
	 */
	public void setIncCode(String incCode) {
		this.incCode = incCode;
	}
	/**
	 * 获取：事件编号 多个事件用逗号分隔
	 */
	public String getIncCode() {
		return incCode;
	}
	/**
	 * 设置：事件发送积分总量
	 */
	public void setIntegralAmtInc(Integer integralAmtInc) {
		this.integralAmtInc = integralAmtInc;
	}
	/**
	 * 获取：事件发送积分总量
	 */
	public Integer getIntegralAmtInc() {
		return integralAmtInc;
	}
	/**
	 * 设置：活动创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：活动创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：活动结束时间
	 */
	public void setShutTime(Date shutTime) {
		this.shutTime = shutTime;
	}
	/**
	 * 获取：活动结束时间
	 */
	public Date getShutTime() {
		return shutTime;
	}
	/**
	 * 设置：每次领取积分量
	 */
	public void setIntegralAmt(Integer integralAmt) {
		this.integralAmt = integralAmt;
	}
	/**
	 * 获取：每次领取积分量
	 */
	public Integer getIntegralAmt() {
		return integralAmt;
	}
	/**
	 * 设置：积分状态，00可领，01不可领
	 */
	public void setIntegralStatus(String integralStatus) {
		this.integralStatus = integralStatus;
	}
	/**
	 * 获取：积分状态，00可领，01不可领
	 */
	public String getIntegralStatus() {
		return integralStatus;
	}
	/**
	 * 设置：积分说明
	 */
	public void setIntegraliInfo(String integraliInfo) {
		this.integraliInfo = integraliInfo;
	}
	/**
	 * 获取：积分说明
	 */
	public String getIntegraliInfo() {
		return integraliInfo;
	}
	/**
	 * 设置：积分的使用场景；多个场景用逗号分隔
	 */
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	/**
	 * 获取：积分的使用场景；多个场景用逗号分隔
	 */
	public String getSceneId() {
		return sceneId;
	}
	/**
	 * 设置：每天限领次数
	 */
	public void setEveryDay(Integer everyDay) {
		this.everyDay = everyDay;
	}
	/**
	 * 获取：每天限领次数
	 */
	public Integer getEveryDay() {
		return everyDay;
	}
	/**
	 * 设置：每周限领次数
	 */
	public void setEveryWeek(Integer everyWeek) {
		this.everyWeek = everyWeek;
	}
	/**
	 * 获取：每周限领次数
	 */
	public Integer getEveryWeek() {
		return everyWeek;
	}
	/**
	 * 设置：领取方式 01自动领取 02手动领取
	 */
	public void setPayment(String payment) {
		this.payment = payment;
	}
	/**
	 * 获取：领取方式 01自动领取 02手动领取
	 */
	public String getPayment() {
		return payment;
	}
	/**
	 * 设置：每月限领次数
	 */
	public void setEveryMonth(Integer everyMonth) {
		this.everyMonth = everyMonth;
	}
	/**
	 * 获取：每月限领次数
	 */
	public Integer getEveryMonth() {
		return everyMonth;
	}
	/**
	 * 设置：创建时间
	 */
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getAddTime() {
		return addTime;
	}
	/**
	 * 设置：积分发行多少个
	 */
	public void setIncNum(Integer incNum) {
		this.incNum = incNum;
	}
	/**
	 * 获取：积分发行多少个
	 */
	public Integer getIncNum() {
		return incNum;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
}
