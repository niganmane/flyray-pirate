package com.github.wxiaoqi.security.biz.modules.fightGroup.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupGoodsInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 拼团商品信息
 * 
 * @author he
 * @date 2018-07-10 09:35:10
 */
@org.apache.ibatis.annotations.Mapper
public interface FightGroupGoodsInfoMapper extends Mapper<FightGroupGoodsInfo> {
	
	List<FightGroupGoodsInfo> queryFightGroupGoodsInfo(Map<String, Object> map);
	
}
