package com.github.wxiaoqi.security.biz.codex.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.mapper.MarCouponMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarIntegralMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarRandomCouponMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.PersonalCouponRelationMapper;
import com.github.wxiaoqi.security.biz.codex.service.CouponAndIntegralStatusService;

@Service("statusService1")
public class CouponAndIntegralStatusServiceImpl implements CouponAndIntegralStatusService {
	
	private static final Logger logger = LoggerFactory.getLogger(CouponAndIntegralStatusServiceImpl.class);
	
	@Autowired
	private MarCouponMapper couponMapper;
	@Autowired
	private MarIntegralMapper integralMapper;
	@Autowired
	private PersonalCouponRelationMapper custCouponRelationMapper;
	@Autowired
	private MarRandomCouponMapper randomMapper;
	
	
	
	@Override
	public void changeCouponAndIntegralStatus() {
		logger.info("定时状态修改开始。。。。。。");
		Long s = System.currentTimeMillis();
		//红包置为可领取
		couponMapper.changStatusToOpen();
		//红包置为不可领取
		couponMapper.changStatusToClose();
		//积分置为可领取
		integralMapper.changStatusToOpen();
		//积分置为不可领取
		integralMapper.changStatusToClose();
		//红包置为有效
		custCouponRelationMapper.changStatusToOpen();
		//红包置为失效
		custCouponRelationMapper.changStatusToClose();
		//随机红包领取状态
		randomMapper.changeStatusToClose();
		randomMapper.changeStatusToOpen();
		Long e = System.currentTimeMillis();
		logger.info("定时状态修改结束。。。。。。耗时："+(e-s)+"毫秒");
	}

}
