package com.github.wxiaoqi.security.biz.modules.refund.biz;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.feign.CrmFeign;
import com.github.wxiaoqi.security.biz.feign.MallFeign;
import com.github.wxiaoqi.security.biz.feign.PayFeign;
import com.github.wxiaoqi.security.biz.modules.auction.entity.AuctionOrder;
import com.github.wxiaoqi.security.biz.modules.auction.mapper.AuctionOrderMapper;
import com.github.wxiaoqi.security.biz.modules.pay.entity.CmsPayOrder;
import com.github.wxiaoqi.security.biz.modules.pay.mapper.CmsPayOrderMapper;
import com.github.wxiaoqi.security.biz.modules.refund.entity.CmsRefundOrder;
import com.github.wxiaoqi.security.biz.modules.refund.mapper.CmsRefundOrderMapper;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

/**
 * 退款
 * @author he
 *
 */
@Service
public class RefundBiz {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(RefundBiz.class);

	@Autowired
	private PayFeign payFeign;
	@Autowired
	private CrmFeign crmFeign;
	@Autowired
	private MallFeign mallFeign;
	@Autowired
	private CmsPayOrderMapper cmsPayOrderMapper;
	@Autowired
	private CmsRefundOrderMapper cmsRefundOrderMapper;
	@Autowired
	private AuctionOrderMapper auctionOrderMapper;

	/**
	 * 申请并发起退款
	 * @return
	 */
	public Map<String, Object> applyAndRefund(Map<String, Object> params){

		logger.info("申请并发起退款请求.......{}",params);
		Map<String, Object> respMap = new HashMap<String, Object>();
		
		//校验订单是否存在
		CmsPayOrder cmsPayOrder = new CmsPayOrder();
		cmsPayOrder.setPlatformId((String)params.get("platformId"));
		cmsPayOrder.setPayOrderNo((String)params.get("payOrderNo"));
		CmsPayOrder selectOrder = cmsPayOrderMapper.selectOne(cmsPayOrder);
		if(null == selectOrder){
			logger.info("申请并发起退款-订单不存在.......");
			respMap.put("success", false);
			respMap.put("code", ResponseCode.ORDER_NO_EXIST.getCode());
			respMap.put("msg", ResponseCode.ORDER_NO_EXIST.getMessage());
			return respMap;
		}
		
		CmsRefundOrder cmsRefundOrder = new CmsRefundOrder();
		String refundOrderNo = String.valueOf(SnowFlake.getId());
		params.put("refundOrderNo", refundOrderNo);
		String payType = selectOrder.getPayType();
		Map<String, Object> refundMap = new HashMap<String, Object>();
		if("1".equals(payType)){//第三方支付
			//退款申请
			Map<String, Object> refundApplyMap = refundApply(params);
			if(!ResponseCode.OK.getCode().equals(refundApplyMap.get("code"))){
				logger.info("申请并发起退款失败.......{}",refundApplyMap.get("msg"));
				respMap.put("success", false);
				respMap.put("code", refundApplyMap.get("code"));
				respMap.put("msg", refundApplyMap.get("msg"));
				return respMap;
			}
			
			//发起退款审核
			refundMap = refund(params);
		}else{//非第三方支付
			//调用crm账户出账接口
//			refundMap = crmFeign.balanceRefund(params);
			refundMap.put("code", "0000");
		}
		if(!ResponseCode.OK.getCode().equals(refundMap.get("code"))){
			cmsRefundOrder.setTxStatus("01");//退款失败
			logger.info("申请并发起退款失败.......{}",refundMap.get("msg"));
			respMap.put("success", false);
			respMap.put("code", refundMap.get("code"));
			respMap.put("msg", refundMap.get("msg"));
		}else{
			if("1".equals(payType)){//第三方支付
				cmsRefundOrder.setTxStatus("03");//退款中
			}else{
				cmsRefundOrder.setTxStatus("00");//退款成功
			}
			respMap.put("refundOrderNo", refundOrderNo);
			respMap.put("success", true);
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		}

		
		//创建退款订单
		cmsRefundOrder.setPlatformId((String)params.get("platformId"));
		cmsRefundOrder.setMerchantId(selectOrder.getMerchantId());
		cmsRefundOrder.setCustomerId(selectOrder.getCustomerId());
		cmsRefundOrder.setPayOrderNo((String)params.get("payOrderNo"));
		cmsRefundOrder.setRefundOrderNo(refundOrderNo);
		cmsRefundOrder.setTxAmt(new BigDecimal((String)params.get("refundAmt")));
		cmsRefundOrder.setCreateTime(new Date());
		cmsRefundOrderMapper.insert(cmsRefundOrder);

		//处理场景订单
		params.put("scenesCode", selectOrder.getScenesCode());
		params.put("payType", payType);
		this.scenesOrderDealWith(params);

		logger.info("申请并发起退款响应.......{}",respMap);
		return respMap;
	}

	/**
	 * 调用pay_core退款申请
	 * @param params
	 * @return
	 */
	private Map<String, Object> refundApply(Map<String, Object> params){
		Map<String, Object> refundApplyMap = new HashMap<String, Object>();
		refundApplyMap.put("platformId", (String)params.get("platformId"));
		refundApplyMap.put("payOrderNo", (String)params.get("payOrderNo"));
		refundApplyMap.put("refundOrderNo", (String)params.get("refundOrderNo"));
		refundApplyMap.put("refundAmt", (String)params.get("refundAmt"));
		logger.info("调用pay_core退款申请请求.......{}",refundApplyMap);
		Map<String, Object> applyRespMap = payFeign.refundApply(refundApplyMap);
		logger.info("调用pay_core退款申请响应.......{}",applyRespMap);
		return applyRespMap;
	}

	/**
	 * 调用pay_core退款审核
	 * @param params
	 * @return
	 */
	private Map<String, Object> refund(Map<String, Object> params){
		Map<String, Object> refundMap = new HashMap<String, Object>();
		refundMap.put("platformId", (String)params.get("platformId"));
		refundMap.put("refundOrderNo", (String)params.get("refundOrderNo"));
		logger.info("调用pay_core退款审核请求.......{}",refundMap);
		Map<String, Object> refundRespMap = payFeign.refund(refundMap);
		logger.info("调用pay_core退款审核响应.......{}",refundRespMap);
		return refundRespMap;
	}

	/**
	 * 场景订单处理
	 * @param params
	 */
	private void scenesOrderDealWith(Map<String, Object> params){
		String scenesCode = (String)params.get("scenesCode");
		String payType = (String) params.get("payType");
		//处理各场景相关订单表
		if("4".equals(scenesCode)){
			//竞拍
			this.updateAuctionOrder(params);
		}else if("5".equals(scenesCode)){
			//拼团参团

		}else if("3".equals(scenesCode)){
			//店内点餐

		}else if("2".equals(scenesCode)){
			//预订点餐

		}else if("1".equals(scenesCode)){
			//外卖

		}else if("6".equals(scenesCode)){
			//拼团创建团
			
		} else if ("8".equals(scenesCode)) {
			//商城
			if (payType.equals("1")) {
				//第三方支付，申请退款，待审核
				params.put("txStatus", "04");
			}else {
				//火钻支付，退款成功
				params.put("txStatus", "03");
			}
			this.updateMallOrder(params);
		}
	}
	
	
	
	/**
	 * 修改商城订单状态
	 * @param params
	 */
	private void updateMallOrder(Map<String, Object> params){
		Map<String, Object> respMap = mallFeign.updatePayOrder(params);
		logger.info("修改商城订单状态。。。{}"+respMap);
	}
	
	
	
	/**
	 * 竞拍订单处理
	 * @param params
	 */
	private void updateAuctionOrder(Map<String, Object> params){
		AuctionOrder auctionOrder = new AuctionOrder();
		auctionOrder.setPlatformId((String) params.get("platformId"));
		auctionOrder.setMerchantId((String) params.get("merchantId"));
		auctionOrder.setPayOrderNo((String)params.get("payOrderNo"));
		auctionOrder.setStatus("00");//支付成功
		AuctionOrder queryAuctionOrder = auctionOrderMapper.selectOne(auctionOrder);
		if(null != queryAuctionOrder){
			queryAuctionOrder.setRefundOrderNo((String)params.get("refundOrderNo"));
			auctionOrderMapper.updateByPrimaryKey(queryAuctionOrder);
		}
	}

}
