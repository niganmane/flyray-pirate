package com.github.wxiaoqi.security.biz.modules.aisuan.rest;

import com.github.wxiaoqi.security.biz.modules.aisuan.biz.AisuanAdviceBiz;
import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanAdvice;
import com.github.wxiaoqi.security.common.cms.request.AisuanAdviceQueryAdminParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordQueryAdminParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordQueryOneParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordReplyParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping("aisuanAdviceAdmin")
public class AisuanAdviceController extends BaseController<AisuanAdviceBiz,AisuanAdvice> {
	@Autowired
	private AisuanAdviceBiz aisuanAdviceBiz;
	/**
	 * 查询列表
	 */
	@RequestMapping(value = "/queryListPage",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryListPage(@RequestBody AisuanAdviceQueryAdminParam param){
		log.info("查询列表传入参数{}", EntityUtils.beanToMap(param));
		TableResultResponse<AisuanAdvice> table = aisuanAdviceBiz.queryListPage(param);
		if(table == null){
			return ResponseHelper.success(param, null, ResponseCode.ABNORMAL_FIELDS.getCode(), ResponseCode.ABNORMAL_FIELDS.getMessage());
		}else {
			return ResponseHelper.success(table, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}
		
	}
	/**
	 * 查询单个详情
	 */
	@RequestMapping(value = "/queryInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryInfo(@RequestBody AisuanRecordQueryOneParam param){
		log.info("查询单个详情传入参数{}", EntityUtils.beanToMap(param));
		Long id = param.getId();
		AisuanAdvice advice =  aisuanAdviceBiz.selectById(id);
		return ResponseHelper.success(advice, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
	/**
	 * 回复
	 */
    @RequestMapping(value = "/reply",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> reply(@RequestBody AisuanRecordReplyParam param) {
    	log.info("回复传入参数{}", EntityUtils.beanToMap(param));
    	try {
    		aisuanAdviceBiz.reply(param);
    		return ResponseHelper.success(param, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseHelper.success(param, null, ResponseCode.ABNORMAL_FIELDS.getCode(), ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
    	
    }
	
}