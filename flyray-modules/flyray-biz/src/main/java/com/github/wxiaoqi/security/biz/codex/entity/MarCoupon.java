package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * 红包基本信息表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Table(name = "mar_coupon")
public class MarCoupon implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Long seqNo;
    
    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
    
    //商户号
    @Column(name = "MERCHANT_ID")
    private String merchantId;
	
	    //红包ID
    @Column(name = "COU_ID")
    private String couId;
	
	    //红包名称
    @Column(name = "COU_NAME")
    private String couName;
	
	    //使用红包最低金额
    @Column(name = "TOTAL_PAY")
    private BigDecimal totalPay;
	
	    //优惠券抵扣金额
    @Column(name = "SUBTRACTION")
    private BigDecimal subtraction;
	
	    //活动编号
    @Column(name = "ACT_ID")
    private String actId;
	
	    //创建时间
    @Column(name = "CREATE_TIME")
    private Date createTime;
	
	    //关闭时间
    @Column(name = "SHUT_TIME")
    private Date shutTime;
	
	    //优惠券开始时间
    @Column(name = "START_TIME")
    private Date startTime;
	
	    //优惠券到期时间
    @Column(name = "END_TIME")
    private Date endTime;
	
	    //红包状态，00可领，01不可领
    @Column(name = "COU_STATUS")
    private String couStatus;
	
	    //优惠券说明
    @Column(name = "COU_INFO")
    private String couInfo;
	
	    //红包领取开始时间
    @Column(name = "REV_START")
    private Date revStart;
	
	    //领取结束时间
    @Column(name = "REV_END")
    private Date revEnd;
	
	    //红包的使用场景；多个场景用逗号分隔
    @Column(name = "SCENE_ID")
    private String sceneId;
	
	    //红包数量
    @Column(name = "COU_AMT")
    private Integer couAmt;
	
	    //每天限领次数
    @Column(name = "EVERY_DAY")
    private Integer everyDay;
	
	    //每周限领次数
    @Column(name = "EVERY_WEEK")
    private Integer everyWeek;
	
	    //领取方式 01自动领取 02手动领取
    @Column(name = "PAYMENT")
    private String payment;
	
	    //每月限领次数
    @Column(name = "EVERY_MONTH")
    private Integer everyMonth;
	
	    //事件编码 多个事件用逗号分隔
    @Column(name = "INC_CODE")
    private String incCode;
	
	    //创建人
    @Column(name = "CREAT_USER")
    private String creatUser;
	
	    //红包类型 '0' 表示普通红包，'1' 表示随机红包
    @Column(name = "COU_TYPE")
    private String couType;
	
	    //随机红包总金额
    @Column(name = "COU_SUM")
    private BigDecimal couSum;
    /**
     * 随机红包金额
     */
    @Transient
    private BigDecimal couponAmt;
    
    @Transient
    private String couSerial;
    
    /**
     * 活动是否开始
     */
    private String isOpen;

    public BigDecimal getCouponAmt() {
		return couponAmt;
	}

	public void setCouponAmt(BigDecimal couponAmt) {
		this.couponAmt = couponAmt;
	}

	public String getCouSerial() {
		return couSerial;
	}

	public void setCouSerial(String couSerial) {
		this.couSerial = couSerial;
	}

	public String getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(String isOpen) {
		this.isOpen = isOpen;
	}

	/**
	 * 设置：
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：
	 */
	public Long getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：红包ID
	 */
	public void setCouId(String couId) {
		this.couId = couId;
	}
	/**
	 * 获取：红包ID
	 */
	public String getCouId() {
		return couId;
	}
	/**
	 * 设置：红包名称
	 */
	public void setCouName(String couName) {
		this.couName = couName;
	}
	/**
	 * 获取：红包名称
	 */
	public String getCouName() {
		return couName;
	}
	/**
	 * 设置：使用红包最低金额
	 */
	public void setTotalPay(BigDecimal totalPay) {
		this.totalPay = totalPay;
	}
	/**
	 * 获取：使用红包最低金额
	 */
	public BigDecimal getTotalPay() {
		return totalPay;
	}
	/**
	 * 设置：优惠券抵扣金额
	 */
	public void setSubtraction(BigDecimal subtraction) {
		this.subtraction = subtraction;
	}
	/**
	 * 获取：优惠券抵扣金额
	 */
	public BigDecimal getSubtraction() {
		return subtraction;
	}
	/**
	 * 设置：活动编号
	 */
	public void setActId(String actId) {
		this.actId = actId;
	}
	/**
	 * 获取：活动编号
	 */
	public String getActId() {
		return actId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：关闭时间
	 */
	public void setShutTime(Date shutTime) {
		this.shutTime = shutTime;
	}
	/**
	 * 获取：关闭时间
	 */
	public Date getShutTime() {
		return shutTime;
	}
	/**
	 * 设置：优惠券开始时间
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	/**
	 * 获取：优惠券开始时间
	 */
	public Date getStartTime() {
		return startTime;
	}
	/**
	 * 设置：优惠券到期时间
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	/**
	 * 获取：优惠券到期时间
	 */
	public Date getEndTime() {
		return endTime;
	}
	/**
	 * 设置：红包状态，00可领，01不可领
	 */
	public void setCouStatus(String couStatus) {
		this.couStatus = couStatus;
	}
	/**
	 * 获取：红包状态，00可领，01不可领
	 */
	public String getCouStatus() {
		return couStatus;
	}
	/**
	 * 设置：优惠券说明
	 */
	public void setCouInfo(String couInfo) {
		this.couInfo = couInfo;
	}
	/**
	 * 获取：优惠券说明
	 */
	public String getCouInfo() {
		return couInfo;
	}
	/**
	 * 设置：红包领取开始时间
	 */
	public void setRevStart(Date revStart) {
		this.revStart = revStart;
	}
	/**
	 * 获取：红包领取开始时间
	 */
	public Date getRevStart() {
		return revStart;
	}
	/**
	 * 设置：领取结束时间
	 */
	public void setRevEnd(Date revEnd) {
		this.revEnd = revEnd;
	}
	/**
	 * 获取：领取结束时间
	 */
	public Date getRevEnd() {
		return revEnd;
	}
	/**
	 * 设置：红包的使用场景；多个场景用逗号分隔
	 */
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	/**
	 * 获取：红包的使用场景；多个场景用逗号分隔
	 */
	public String getSceneId() {
		return sceneId;
	}
	/**
	 * 设置：红包数量
	 */
	public void setCouAmt(Integer couAmt) {
		this.couAmt = couAmt;
	}
	/**
	 * 获取：红包数量
	 */
	public Integer getCouAmt() {
		return couAmt;
	}
	/**
	 * 设置：每天限领次数
	 */
	public void setEveryDay(Integer everyDay) {
		this.everyDay = everyDay;
	}
	/**
	 * 获取：每天限领次数
	 */
	public Integer getEveryDay() {
		return everyDay;
	}
	/**
	 * 设置：每周限领次数
	 */
	public void setEveryWeek(Integer everyWeek) {
		this.everyWeek = everyWeek;
	}
	/**
	 * 获取：每周限领次数
	 */
	public Integer getEveryWeek() {
		return everyWeek;
	}
	/**
	 * 设置：领取方式 01自动领取 02手动领取
	 */
	public void setPayment(String payment) {
		this.payment = payment;
	}
	/**
	 * 获取：领取方式 01自动领取 02手动领取
	 */
	public String getPayment() {
		return payment;
	}
	/**
	 * 设置：每月限领次数
	 */
	public void setEveryMonth(Integer everyMonth) {
		this.everyMonth = everyMonth;
	}
	/**
	 * 获取：每月限领次数
	 */
	public Integer getEveryMonth() {
		return everyMonth;
	}
	/**
	 * 设置：事件编码 多个事件用逗号分隔
	 */
	public void setIncCode(String incCode) {
		this.incCode = incCode;
	}
	/**
	 * 获取：事件编码 多个事件用逗号分隔
	 */
	public String getIncCode() {
		return incCode;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreatUser(String creatUser) {
		this.creatUser = creatUser;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreatUser() {
		return creatUser;
	}
	/**
	 * 设置：红包类型 '0' 表示普通红包，'1' 表示随机红包
	 */
	public void setCouType(String couType) {
		this.couType = couType;
	}
	/**
	 * 获取：红包类型 '0' 表示普通红包，'1' 表示随机红包
	 */
	public String getCouType() {
		return couType;
	}
	/**
	 * 设置：随机红包总金额
	 */
	public void setCouSum(BigDecimal couSum) {
		this.couSum = couSum;
	}
	/**
	 * 获取：随机红包总金额
	 */
	public BigDecimal getCouSum() {
		return couSum;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	
}
