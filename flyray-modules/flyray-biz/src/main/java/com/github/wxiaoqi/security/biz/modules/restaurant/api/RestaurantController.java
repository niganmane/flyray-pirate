package com.github.wxiaoqi.security.biz.modules.restaurant.api;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.api.BaseController;
import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantAppraisalDetailInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantAppraisalInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantDishesCategoryInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantDishesInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantDistributionInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantOrderInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantReservationInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantShoppingCartInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantTableInfoBiz;
import com.github.wxiaoqi.security.common.cms.request.BaseParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantAddDistributionParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantAppraisalDetailParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantConfirmOrderParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantDistributionParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantOrderDishesParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantOrderShoppingParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantOrderShoppingSubmitParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantPayOrderParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantReservationPayParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantReservationShoppingParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantShoppingOperatingParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantSubmitReservationParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantTakeawayAppraisalParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantToAppraisalParam;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 餐饮相关接口
 * @author he
 *
 */
@Api(tags="餐饮管理")
@Controller
@RequestMapping("restaurants")
public class RestaurantController extends BaseController{
	
	@Autowired
	private RestaurantInfoBiz restaurantInfoBiz;
	@Autowired
	private RestaurantTableInfoBiz restaurantTableInfoBiz;
	@Autowired
	private RestaurantDishesCategoryInfoBiz restaurantDishesCategoryInfoBiz;
	@Autowired
	private RestaurantShoppingCartInfoBiz restaurantShoppingCartInfoBiz;
	@Autowired
	private RestaurantDishesInfoBiz restaurantDishesInfoBiz;
	@Autowired
	private RestaurantAppraisalInfoBiz restaurantAppraisalInfoBiz;
	@Autowired
	private RestaurantAppraisalDetailInfoBiz restaurantAppraisalDetailInfoBiz;
	@Autowired
	private RestaurantReservationInfoBiz restaurantReservationInfoBiz;
	@Autowired
	private RestaurantDistributionInfoBiz restaurantDistributionInfoBiz;
	@Autowired
	private RestaurantOrderInfoBiz restaurantOrderInfoBiz;
	
	/**
	 * 餐厅信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("餐厅信息查询")
	@RequestMapping(value = "/query",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryRestaurantInfo(@RequestBody @Valid BaseParam baseParam) throws Exception {
		Map<String, Object> response = restaurantInfoBiz.queryRestaurantInfo(EntityUtils.beanToMap(baseParam));
		return response;
    }
	
	/**
	 * 餐桌信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("餐桌信息查询")
	@RequestMapping(value = "/queryTableInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryTableInfo(@RequestBody @Valid BaseParam baseParam) throws Exception {
		Map<String, Object> response = restaurantTableInfoBiz.queryTableInfo(EntityUtils.beanToMap(baseParam));
		return response;
	}
	
	/**
	 * 点餐餐品信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("点餐餐品信息查询")
	@RequestMapping(value = "/queryOrderDishesInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryOrderDishesInfo(@RequestBody @Valid RestaurantOrderDishesParam orderDishesParam) throws Exception {
		Map<String, Object> response = restaurantDishesCategoryInfoBiz.queryOrderDishesInfo(EntityUtils.beanToMap(orderDishesParam));
		return response;
	}
	
	/**
	 * 菜品详情查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("菜品详情查询")
	@RequestMapping(value = "/queryDishesDetailInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryDishesDetailInfo(@RequestBody @Valid RestaurantOrderShoppingParam orderShoppingParam) throws Exception {
		Map<String, Object> response = restaurantDishesInfoBiz.queryDishesDetailInfo(EntityUtils.beanToMap(orderShoppingParam));
		return response;
	}
	
	/**
	 * 点餐购物车/已选菜单信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("点餐购物车/已选菜单信息查询")
	@RequestMapping(value = "/queryOrderShoppingCartInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryOrderShoppingCartInfo(@RequestBody @Valid RestaurantOrderDishesParam orderDishesParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.queryOrderShoppingCartInfo(EntityUtils.beanToMap(orderDishesParam));
		return response;
	}
	
	/**
	 * 点餐购物车增加
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("点餐购物车增加")
	@RequestMapping(value = "/orderShoppingCartAdd",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> orderShoppingCartAdd(@RequestBody @Valid RestaurantOrderShoppingParam orderShoppingParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.orderShoppingCartAdd(EntityUtils.beanToMap(orderShoppingParam));
		return response;
	}
	
	/**
	 * 点餐购物车减少
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("点餐购物车减少")
	@RequestMapping(value = "/orderShoppingCartCut",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> orderShoppingCartCut(@RequestBody @Valid RestaurantOrderShoppingParam orderShoppingParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.orderShoppingCartCut(EntityUtils.beanToMap(orderShoppingParam));
		return response;
	}
	
	/**
	 * 点餐购物车删除菜系
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("点餐购物车删除菜系")
	@RequestMapping(value = "/orderShoppingCartDel",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> orderShoppingCartDel(@RequestBody @Valid RestaurantOrderShoppingParam orderShoppingParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.orderShoppingCartDel(EntityUtils.beanToMap(orderShoppingParam));
		return response;
	}
	
	/**
	 * 点餐购物车清空
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("点餐购物车清空")
	@RequestMapping(value = "/orderShoppingCartClear",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> orderShoppingCartClear(@RequestBody @Valid RestaurantOrderDishesParam orderDishesParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.orderShoppingCartClear(EntityUtils.beanToMap(orderDishesParam));
		return response;
	}
	
	/**
	 * 点餐购物车提交
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("点餐购物车提交")
	@RequestMapping(value = "/orderShoppingCartSubmit",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> orderShoppingCartSubmit(@RequestBody @Valid RestaurantOrderShoppingSubmitParam orderShoppingSubmitParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.orderShoppingCartSubmit(EntityUtils.beanToMap(orderShoppingSubmitParam));
		return response;
	}
	
	/**
	 * 菜品评价信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("菜品评价信息查询")
	@RequestMapping(value = "/queryAppraisalInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryAppraisalInfo(@RequestBody @Valid RestaurantOrderShoppingParam orderShoppingParam) throws Exception {
		Map<String, Object> response = restaurantAppraisalInfoBiz.queryAppraisalInfo(EntityUtils.beanToMap(orderShoppingParam));
		return response;
	}
	
	/**
	 * 菜品评价信息详情查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("菜品评价信息详情查询")
	@RequestMapping(value = "/queryAppraisalDetailInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryAppraisalDetailInfo(@RequestBody @Valid RestaurantAppraisalDetailParam apraisalDetailParam) throws Exception {
		Map<String, Object> response = restaurantAppraisalDetailInfoBiz.queryAppraisalDetailInfo(EntityUtils.beanToMap(apraisalDetailParam));
		return response;
	}
	
	/**
	 * 提交预订信息
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("提交预订信息")
	@RequestMapping(value = "/submitReservationInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> submitReservationInfo(@RequestBody @Valid RestaurantSubmitReservationParam submitReservationParam) throws Exception {
		Map<String, Object> response = restaurantReservationInfoBiz.submitReservationInfo(EntityUtils.beanToMap(submitReservationParam));
		return response;
	}
	
	/**
	 * 预约餐品信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("预约餐品信息查询")
	@RequestMapping(value = "/queryReservationDishesInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryReservationDishesInfo(@RequestBody @Valid RestaurantParam restaurantParam) throws Exception {
		Map<String, Object> response = restaurantDishesCategoryInfoBiz.queryReservationDishesInfo(EntityUtils.beanToMap(restaurantParam));
		return response;
	}
	
	/**
	 * 预订/外卖购物车信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("预订/外卖购物车信息查询")
	@RequestMapping(value = "/queryReservationShoppingCartInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryReservationShoppingCartInfo(@RequestBody @Valid RestaurantReservationShoppingParam reservationShoppingParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.queryReservationShoppingCartInfo(EntityUtils.beanToMap(reservationShoppingParam));
		return response;
	}
	
	/**
	 * 预订/外卖购物车增加
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("预订/外卖购物车增加")
	@RequestMapping(value = "/reservationShoppingCartAdd",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> reservationShoppingCartAdd(@RequestBody @Valid RestaurantShoppingOperatingParam shoppingOperatingParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.reservationShoppingCartAdd(EntityUtils.beanToMap(shoppingOperatingParam));
		return response;
	}
	
	/**
	 * 预订/外卖购物车减少
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("预订/外卖购物车减少")
	@RequestMapping(value = "/reservationShoppingCartCut",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> reservationShoppingCartCut(@RequestBody @Valid RestaurantShoppingOperatingParam shoppingOperatingParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.reservationShoppingCartCut(EntityUtils.beanToMap(shoppingOperatingParam));
		return response;
	}
	
	/**
	 * 预订/外卖购物车删除菜系
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("预订/外卖购物车删除菜系")
	@RequestMapping(value = "/reservationShoppingCartDel",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> reservationShoppingCartDel(@RequestBody @Valid RestaurantShoppingOperatingParam shoppingOperatingParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.reservationShoppingCartDel(EntityUtils.beanToMap(shoppingOperatingParam));
		return response;
	}
	
	/**
	 * 预订/外卖购物车清空
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("预订/外卖购物车清空")
	@RequestMapping(value = "/reservationShoppingCartClear",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> reservationShoppingCartClear(@RequestBody @Valid RestaurantReservationShoppingParam reservationShoppingParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.reservationShoppingCartClear(EntityUtils.beanToMap(reservationShoppingParam));
		return response;
	}
	
	/**
	 * 外卖餐品信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("外卖餐品信息查询")
	@RequestMapping(value = "/queryTakeawayDishesInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryTakeawayDishesInfo(@RequestBody @Valid RestaurantParam restaurantParam) throws Exception {
		Map<String, Object> response = restaurantDishesCategoryInfoBiz.queryTakeawayDishesInfo(EntityUtils.beanToMap(restaurantParam));
		return response;
	}
	
	/**
	 * 默认配送地址查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("默认配送地址查询")
	@RequestMapping(value = "/queryDefaultDistributionInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryDefaultDistributionInfo(@RequestBody @Valid RestaurantParam restaurantParam) throws Exception {
		Map<String, Object> response = restaurantDistributionInfoBiz.queryDefaultDistributionInfo(EntityUtils.beanToMap(restaurantParam));
		return response;
	}
	
	/**
	 * 配送地址列表查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("配送地址列表查询")
	@RequestMapping(value = "/queryDistributionListInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryDistributionListInfo(@RequestBody @Valid RestaurantParam restaurantParam) throws Exception {
		Map<String, Object> response = restaurantDistributionInfoBiz.queryDistributionListInfo(EntityUtils.beanToMap(restaurantParam));
		return response;
	}
	
	/**
	 * 设置默认配送地址
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("设置默认配送地址")
	@RequestMapping(value = "/settingDefaultDistribution",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> settingDefaultDistribution(@RequestBody @Valid RestaurantDistributionParam distributionParam) throws Exception {
		Map<String, Object> response = restaurantDistributionInfoBiz.settingDefaultDistribution(EntityUtils.beanToMap(distributionParam));
		return response;
	}
	
	/**
	 * 删除配送地址
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("删除配送地址")
	@RequestMapping(value = "/deleteDistributionInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> deleteDistributionInfo(@RequestBody @Valid RestaurantDistributionParam distributionParam) throws Exception {
		Map<String, Object> response = restaurantDistributionInfoBiz.deleteDistributionInfo(EntityUtils.beanToMap(distributionParam));
		return response;
	}
	
	/**
	 * 添加/修改配送地址
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("添加/修改配送地址")
	@RequestMapping(value = "/addDistributionInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> addDistributionInfo(@RequestBody @Valid RestaurantAddDistributionParam addDistributionParam) throws Exception {
		Map<String, Object> response = restaurantDistributionInfoBiz.addDistributionInfo(EntityUtils.beanToMap(addDistributionParam));
		return response;
	}
	
	/**
	 * 查询配送地址明细
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("查询配送地址明细")
	@RequestMapping(value = "/queryDistributionDeatilInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryDistributionDeatilInfo(@RequestBody @Valid RestaurantDistributionParam distributionParam) throws Exception {
		Map<String, Object> response = restaurantDistributionInfoBiz.queryDistributionDeatilInfo(EntityUtils.beanToMap(distributionParam));
		return response;
	}
	
	/**
	 * 店内点餐支付完成修改状态
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("店内点餐支付完成修改状态")
	@RequestMapping(value = "/orderPayUpdate",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> orderPayUpdate(@RequestBody @Valid RestaurantOrderDishesParam orderDishesParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.orderPayUpdate(EntityUtils.beanToMap(orderDishesParam));
		return response;
	}
	
	/**
	 * 预约外卖点餐支付完成修改状态
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("预约外卖点餐支付完成修改状态")
	@RequestMapping(value = "/reservationPayUpdate",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> reservationPayUpdate(@RequestBody @Valid RestaurantReservationPayParam restaurantReservationPayParam) throws Exception {
		Map<String, Object> response = restaurantShoppingCartInfoBiz.reservationPayUpdate(EntityUtils.beanToMap(restaurantReservationPayParam));
		return response;
	}
	
	/**
	 * 查询点餐订单信息
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("查询点餐订单信息")
	@RequestMapping(value = "/queryOrderPayOrderInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryOrderPayOrderInfo(@RequestBody @Valid RestaurantPayOrderParam restaurantPayOrderParam) throws Exception {
		Map<String, Object> response = restaurantOrderInfoBiz.queryOrderPayOrderInfo(EntityUtils.beanToMap(restaurantPayOrderParam));
		return response;
	}
	
	/**
	 * 发起评论
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("发起评论")
	@RequestMapping(value = "/toAppraisal",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> toAppraisal(@RequestBody @Valid RestaurantToAppraisalParam restaurantToAppraisalParam) throws Exception {
		Map<String, Object> response = restaurantAppraisalInfoBiz.toAppraisal(EntityUtils.beanToMap(restaurantToAppraisalParam));
		return response;
	}
	
	/**
	 * 查询外卖订单信息
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("查询外卖订单信息")
	@RequestMapping(value = "/queryTakeawayPayOrderInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryTakeawayPayOrderInfo(@RequestBody @Valid RestaurantPayOrderParam restaurantPayOrderParam) throws Exception {
		Map<String, Object> response = restaurantOrderInfoBiz.queryTakeawayPayOrderInfo(EntityUtils.beanToMap(restaurantPayOrderParam));
		return response;
	}
	
	/**
	 * 订单确认收货
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("订单确认收货")
	@RequestMapping(value = "/confirmOrder",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> confirmOrder(@RequestBody @Valid RestaurantConfirmOrderParam restaurantConfirmOrderParam) throws Exception {
		Map<String, Object> response = restaurantOrderInfoBiz.confirmOrder(EntityUtils.beanToMap(restaurantConfirmOrderParam));
		return response;
	}
	
	/**
	 * 外卖评论
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("外卖评论")
	@RequestMapping(value = "/takeawayAppraisal",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> takeawayAppraisal(@RequestBody @Valid RestaurantTakeawayAppraisalParam restaurantTakeawayAppraisalParam) throws Exception {
		Map<String, Object> response = restaurantAppraisalInfoBiz.takeawayAppraisal(EntityUtils.beanToMap(restaurantTakeawayAppraisalParam));
		return response;
	}
	
	/**
	 * 外卖订单详情查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("外卖订单详情查询")
	@RequestMapping(value = "/queryTakeawayOrderDetailInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryTakeawayOrderDetailInfo(@RequestBody @Valid RestaurantConfirmOrderParam restaurantConfirmOrderParam) throws Exception {
		Map<String, Object> response = restaurantOrderInfoBiz.queryTakeawayOrderDetailInfo(EntityUtils.beanToMap(restaurantConfirmOrderParam));
		return response;
	}
	
	/**
	 * 预订订单查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("预订订单查询")
	@RequestMapping(value = "/queryReservationOrderInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryReservationOrderInfo(@RequestBody @Valid RestaurantPayOrderParam restaurantPayOrderParam) throws Exception {
		Map<String, Object> response = restaurantOrderInfoBiz.queryReservationOrderInfo(EntityUtils.beanToMap(restaurantPayOrderParam));
		return response;
	}

}
