package com.github.wxiaoqi.security.biz.modules.activity.api;

import com.github.wxiaoqi.security.biz.api.BaseController;
import com.github.wxiaoqi.security.biz.modules.activity.biz.ActivityInfoBiz;
import com.github.wxiaoqi.security.biz.modules.activity.entity.ActivityInfo;
import com.github.wxiaoqi.security.common.cms.request.CmsJoinActivityParam;
import com.github.wxiaoqi.security.common.cms.request.CmsQueryActivityParam;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.ResponseHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;


/**
 * 小程序活动相关接口
 * @author Administrator
 *
 */
@Api(tags="活动管理")
@Slf4j
@Controller
@RequestMapping("activities")
public class ActivityController extends BaseController {
	
	@Autowired
	private ActivityInfoBiz activityBiz;
	
	@ApiOperation("查询热门活动列表")
	@ResponseBody
	@RequestMapping(value = "/hotList", method = RequestMethod.POST)
	public Map<String, Object> queryHotActivies(@RequestBody CmsQueryActivityParam param) {
		log.info("查询热门活动 请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> respMap = activityBiz.queryHotActivity(param);
		log.info("查询热门活动 响应参数：{}"+param);
		return respMap;
	}
	
	
	/**
	 * 查询所有活动列表
	 * @param param
	 * @return
	 */
	@ApiOperation("查询所有活动列表")
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody CmsQueryActivityParam param) {
		log.info("查询所有活动------start------{}", param);
		Map<String, Object> respMap = activityBiz.queryActivity(param);
		log.info("查询活动首页信息------查询推荐活动记录------{}", respMap);
		return respMap;
	}
	
	
	@ApiOperation("查询单个活动详情")
	@ResponseBody
	@RequestMapping(value = "/info", method = RequestMethod.POST)
	public Map<String, Object> queryInfo(@RequestBody CmsQueryActivityParam param) {
		log.info("查询单个活动详情------start------{}", param);
		ActivityInfo activityInfo = activityBiz.queryActivityInfo(param);
		if (activityInfo == null) {
			return ResponseHelper.success(null, null, "403", "活动不存在");
		}else {
			return ResponseHelper.success(activityInfo, null, "200", "查询成功");
			
		}
	}
	
	
	@ApiOperation("参加活动")
	@ResponseBody
	@RequestMapping(value = "/join", method = RequestMethod.POST)
	public Map<String, Object> joinActivity(@RequestBody CmsJoinActivityParam param) {
		log.info("参加活动------start------{}", param);
		activityBiz.joinActivity(param);
		return ResponseHelper.success(null, null, "200", "插入成功");
	}
	
	
	@ApiOperation("取消参加活动")
	@ResponseBody
	@RequestMapping(value = "/cancel", method = RequestMethod.POST)
	public Map<String, Object> cancelJoinActivity(@RequestBody CmsJoinActivityParam param) {
		log.info("取消参加活动------start------{}", param);
		Map<String, Object> respMap = activityBiz.cancelJoinActivity(param);
		log.info("取消参加活动------{}", respMap);
		return respMap;
	}
	
}
