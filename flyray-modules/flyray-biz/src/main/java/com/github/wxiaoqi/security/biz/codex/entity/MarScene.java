package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Table(name = "mar_scene")
public class MarScene implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //主键，自增
    @Id
    private Long seqNo;
	
	    //场景名
    @Column(name = "SCENE_NAME")
    private String sceneName;
	
	    //场景编号
    @Column(name = "SCENE_CODE")
    private String sceneCode;
	
	    //场景简介
    @Column(name = "SCENE_INFO")
    private String sceneInfo;
	
	    //营销开关  0：开 1：关
    @Column(name = "MARKETING_SWITCH")
    private String marketingSwitch;
	

	/**
	 * 设置：主键，自增
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：主键，自增
	 */
	public Long getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：场景名
	 */
	public void setSceneName(String sceneName) {
		this.sceneName = sceneName;
	}
	/**
	 * 获取：场景名
	 */
	public String getSceneName() {
		return sceneName;
	}
	/**
	 * 设置：场景编号
	 */
	public void setSceneCode(String sceneCode) {
		this.sceneCode = sceneCode;
	}
	/**
	 * 获取：场景编号
	 */
	public String getSceneCode() {
		return sceneCode;
	}
	/**
	 * 设置：场景简介
	 */
	public void setSceneInfo(String sceneInfo) {
		this.sceneInfo = sceneInfo;
	}
	/**
	 * 获取：场景简介
	 */
	public String getSceneInfo() {
		return sceneInfo;
	}
	/**
	 * 设置：营销开关  0：开 1：关
	 */
	public void setMarketingSwitch(String marketingSwitch) {
		this.marketingSwitch = marketingSwitch;
	}
	/**
	 * 获取：营销开关  0：开 1：关
	 */
	public String getMarketingSwitch() {
		return marketingSwitch;
	}
}
