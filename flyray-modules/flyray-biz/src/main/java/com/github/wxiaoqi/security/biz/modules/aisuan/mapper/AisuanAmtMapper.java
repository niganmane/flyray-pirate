package com.github.wxiaoqi.security.biz.modules.aisuan.mapper;

import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanAmt;

import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface AisuanAmtMapper extends Mapper<AisuanAmt> {

}
