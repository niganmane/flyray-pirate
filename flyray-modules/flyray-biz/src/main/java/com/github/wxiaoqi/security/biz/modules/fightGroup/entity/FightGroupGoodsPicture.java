package com.github.wxiaoqi.security.biz.modules.fightGroup.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 商品图片表
 * 
 * @author he
 * @date 2018-07-11 15:15:06
 */
@Table(name = "fight_group_goods_picture")
public class FightGroupGoodsPicture implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//商品id
	@Column(name = "goods_id")
	private String goodsId;

	//图片url
	@Column(name = "picture_url")
	private String pictureUrl;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：商品id
	 */
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	/**
	 * 获取：商品id
	 */
	public String getGoodsId() {
		return goodsId;
	}
	/**
	 * 设置：图片url
	 */
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	/**
	 * 获取：图片url
	 */
	public String getPictureUrl() {
		return pictureUrl;
	}
}
