package com.github.wxiaoqi.security.biz.modules.restaurant.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantShoppingCartInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantShoppingDishesInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 购物车信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantShoppingCartInfoMapper extends Mapper<RestaurantShoppingCartInfo> {
	
	/**
	 * 查询金额总和
	 * @param restaurantShoppingCartInfo
	 * @return
	 */
	public String queryCountByPrice(RestaurantShoppingCartInfo restaurantShoppingCartInfo);
	
	/**
	 * 查询数量总和
	 * @param restaurantShoppingCartInfo
	 * @return
	 */
	public Integer queryCountByCount(RestaurantShoppingCartInfo restaurantShoppingCartInfo);
	
	/**
	 * 分页查询购物车信息
	 * @param map
	 * @return
	 */
	List<RestaurantShoppingCartInfo> queryRestaurantShoppingCartInfo(Map<String, Object> map);
	/**
	 * 查询购物车菜品详情
	 * @param restaurantShoppingCartInfo
	 * @return
	 */
	public List<RestaurantShoppingDishesInfo> queryRestaurantDishesInfo(RestaurantShoppingCartInfo restaurantShoppingCartInfo);
	
}
