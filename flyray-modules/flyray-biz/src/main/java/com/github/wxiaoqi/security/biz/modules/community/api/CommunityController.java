package com.github.wxiaoqi.security.biz.modules.community.api;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.api.BaseController;
import com.github.wxiaoqi.security.biz.modules.community.biz.CommunitySocietyBiz;
import com.github.wxiaoqi.security.biz.modules.community.biz.CommunityViewpointBiz;
import com.github.wxiaoqi.security.common.cms.request.CmsQuerySocietyParam;
import com.github.wxiaoqi.security.common.cms.request.CmsQueryViewPointParam;
import com.github.wxiaoqi.security.common.cms.request.CmsSaveViewPointParam;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 小程序社区相关接口
 * @author Administrator
 *
 */
@Slf4j
@Api(tags="社区")
@Controller
@RequestMapping("community")
public class CommunityController extends BaseController {
	
	@Autowired
	private CommunityViewpointBiz viewpointBiz;
	@Autowired
	private CommunitySocietyBiz societyBiz;

	
	@ApiOperation("查询观点列表")
	@ResponseBody
	@RequestMapping(value = "/viewpoints/query", method = RequestMethod.POST)
	public Map<String, Object> queryViewPoint(@RequestBody CmsQueryViewPointParam param) throws Exception {
		log.info("查询观点列表 请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> response = viewpointBiz.queryViewPoint(param);
		log.info("观点列表。。。{}"+response);
		return response;
	}
	
	
	@ApiOperation("添加观点")
	@ResponseBody
	@RequestMapping(value = "/viewpoint/add", method = RequestMethod.POST)
	public Map<String, Object> addViewPoint(@RequestBody CmsSaveViewPointParam param) throws Exception {
		log.info("添加观点 请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> response = viewpointBiz.addViewPoint(param);
		log.info("添加观点---end---{}",response);
		return response;
	}
	
	
	@ApiOperation("查询社群")
	@ResponseBody
	@RequestMapping(value="/societies/query", method = RequestMethod.POST)
	public Map<String, Object> querySocietyInfo(@RequestBody CmsQuerySocietyParam param) throws Exception {
		log.info("查询社群 请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> response = societyBiz.querySocietyInfo(param);
		log.info("查询社群 。。。{}"+response);
		return response;
	}
	
	@ApiOperation("观点详情")
	@ResponseBody
	@RequestMapping(value = "/viewpoint/info", method = RequestMethod.POST)
	public Map<String, Object> viewpointInfo(@RequestBody CmsQueryViewPointParam param) throws Exception {
		log.info("观点详情 请求参数：{}",EntityUtils.beanToMap(param));
		Map<String, Object> response = viewpointBiz.queryViewpointInfo(param);
		log.info("观点详情---end---{}",response);
		return response;
	}
	
	
}
