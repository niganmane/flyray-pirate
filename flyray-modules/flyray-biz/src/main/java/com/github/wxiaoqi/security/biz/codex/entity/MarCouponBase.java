package com.github.wxiaoqi.security.biz.codex.entity;

import java.math.BigDecimal;

public class MarCouponBase{
    /**
     * 红包id
     */
    private String couponId;
    /**
     * 红包流水号
     */
    private String couponSerialNo;
    /**
     * 红包有效时间
     */
    private String valdateTime;
    /**
     * 领取方式 
     */
    private String payment;
    /**
     * 是否最优   默认:00  最优:01
     */
    private String isOptimal;
    /**
     * 是否可用   00:可用  01:不可用
     */
    private String isUsable;
    /**
     * 使用红包最低金额
     */
    private String totalPay;
    /**
     * 优惠券抵扣金额
     */
    private String subtraction;
    /**
     * 红包场景
     * @return
     */
    private String scene;
    /**
     * 红包场景id
     * @return
     */
    private String sceneIds;
    
    /**
     * 红包类型 0，普通红包；1，随机红包
     */
    private String couType;
    
   
    /**
     * 随机红包 金额
     * */
    private BigDecimal couponAmt;
    
    
	public BigDecimal getCouponAmt() {
		return couponAmt;
	}

	public void setCouponAmt(BigDecimal couponAmt) {
		this.couponAmt = couponAmt;
	}

	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getSceneIds() {
		return sceneIds;
	}

	public void setSceneIds(String sceneIds) {
		this.sceneIds = sceneIds;
	}

	public String getCouponSerialNo() {
		return couponSerialNo;
	}

	public void setCouponSerialNo(String couponSerialNo) {
		this.couponSerialNo = couponSerialNo;
	}

	public String getValdateTime() {
		return valdateTime;
	}

	public void setValdateTime(String valdateTime) {
		this.valdateTime = valdateTime;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getIsOptimal() {
		return isOptimal;
	}

	public void setIsOptimal(String isOptimal) {
		this.isOptimal = isOptimal;
	}

	public String getIsUsable() {
		return isUsable;
	}

	public void setIsUsable(String isUsable) {
		this.isUsable = isUsable;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public String getTotalPay() {
		return totalPay;
	}

	public void setTotalPay(String totalPay) {
		this.totalPay = totalPay;
	}

	public String getSubtraction() {
		return subtraction;
	}

	public void setSubtraction(String subtraction) {
		this.subtraction = subtraction;
	}

	public String getCouType() {
		return couType;
	}

	public void setCouType(String couType) {
		this.couType = couType;
	}
	
	
	
}
