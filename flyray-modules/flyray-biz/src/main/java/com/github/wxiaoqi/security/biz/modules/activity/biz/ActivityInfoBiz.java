package com.github.wxiaoqi.security.biz.modules.activity.biz;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.modules.activity.entity.ActivityCustomer;
import com.github.wxiaoqi.security.biz.modules.activity.entity.ActivityInfo;
import com.github.wxiaoqi.security.biz.modules.activity.mapper.ActivityCustomerMapper;
import com.github.wxiaoqi.security.biz.modules.activity.mapper.ActivityInfoMapper;
import com.github.wxiaoqi.security.biz.modules.comment.entity.Comment;
import com.github.wxiaoqi.security.biz.modules.comment.mapper.CommentMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.CmsJoinActivityParam;
import com.github.wxiaoqi.security.common.cms.request.CmsQueryActivityParam;
import com.github.wxiaoqi.security.common.cms.request.CmsSaveActivityParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 
 *
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:43
 */
@Slf4j
@Service
public class ActivityInfoBiz extends BaseBiz<ActivityInfoMapper,ActivityInfo> {
	
	@Autowired
	private ActivityCustomerMapper activityCustomerMapper;
	@Autowired
	private CommentMapper commentMapper;
	
	/**
	 * 查询活动列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryActivity(CmsQueryActivityParam param) {
		log.info("查询活动列表，请求参数。。。{}"+param);
		 Example example =new Example(ActivityInfo.class);
//		 Criteria criteria = example.createCriteria();
//		 criteria.andEqualTo("merchantId",param.getMerchantId());
//		 criteria.andEqualTo("platformId",param.getPlatformId());
		 example.setOrderByClause("create_time desc");
		 List<ActivityInfo> list = mapper.selectByExample(example);
		 log.info("活动列表。。。{}"+list);
		 
		 Map<String, Object> respMap = new HashMap<>();
		 respMap.put("body", list);
		 respMap.put("code", ResponseCode.OK.getCode());
		 respMap.put("msg", ResponseCode.OK.getMessage());
		 respMap.put("success", true);
		 respMap.put("total", list.size());
		 return respMap;
	}
	
	/**
	 * 查询热门活动列表
	 * @param param
	 * @return
	 */
	public Map<String, Object> queryHotActivity(CmsQueryActivityParam param) {
		log.info("查询热门活动列表，请求参数。。。{}"+param);
		 Example example =new Example(ActivityInfo.class);
		 example.setOrderByClause("create_time desc");
		 List<ActivityInfo> list = mapper.selectByExample(example);
		 log.info("热门活动列表。。。{}"+list);
		 
		 Map<String, Object> respMap = new HashMap<>();
		 if (list != null && list.size() > 0) {
			if (list.size() > 3) {
				List<ActivityInfo> list1 = new ArrayList<>();
				for (int i = 0; i < 3; i++) {
					list1.add(list.get(i));
				}
				respMap.put("body", list1);
			}else {
				respMap.put("body", list);
			}
			 respMap.put("code", ResponseCode.OK.getCode());
			 respMap.put("msg", ResponseCode.OK.getMessage());
			 respMap.put("success", true);
		}else {
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
			respMap.put("success", false);
		}
		 return respMap;
	}
	
	
	/**
	 * 查询活动详情
	 * @param param
	 * @return
	 */
	public ActivityInfo queryActivityInfo(CmsQueryActivityParam param) {
		String id = param.getId();
		String customerId = param.getCustomerId();
		ActivityInfo activityInfo = mapper.selectByPrimaryKey(id);
		if (activityInfo != null) {
			ActivityCustomer activityCustomer = new ActivityCustomer();
			activityCustomer.setCustomerId(customerId);
			activityCustomer.setActivityId(id);
			ActivityCustomer res = activityCustomerMapper.selectOne(activityCustomer);
			if (res != null) {
				activityInfo.setJoin(res.getJoin());
			}else {
				activityInfo.setJoin("0");
			}
			ActivityCustomer activityCustomer1 = new ActivityCustomer();
			activityCustomer1.setActivityId(id);
			activityCustomer1.setJoin("1");
			Integer count = activityCustomerMapper.selectCount(activityCustomer1);
			activityInfo.setJoinnumber(count);
			List<ActivityCustomer> joins = activityCustomerMapper.select(activityCustomer1);
			activityInfo.setJoinList(joins);
			Comment comment = new Comment();
			comment.setCommentTargetId(id);
			comment.setCommentModuleNo("03");
			Integer count1 = commentMapper.selectCount(comment);
			List<Comment> comments = commentMapper.select(comment);
			activityInfo.setCommentCount(count1);
			activityInfo.setComments(comments);
		}
		return activityInfo;
	}
	 
	
	/**
	 * 参加活动
	 * @param param
	 * @return
	 */
	public void joinActivity(CmsJoinActivityParam param) {
		String activityId = param.getActivityId();
		ActivityInfo activityInfo = mapper.selectByPrimaryKey(activityId);
		if (activityInfo != null) {
			String customerId = param.getCustomerId();
			Integer contactWay = param.getContactWay();
			String contactValue = param.getContactValue();
			String realName = param.getRealName();
			String id = String.valueOf(SnowFlake.getId());
			ActivityCustomer activityCustomer = new ActivityCustomer();
			activityCustomer.setId(id);
			activityCustomer.setActivityId(activityId);
			activityCustomer.setCustomerId(customerId);
			activityCustomer.setContactWay(contactWay);
			activityCustomer.setRealName(realName);
			activityCustomer.setContactValue(contactValue);
			activityCustomer.setJoin("1");
			activityCustomerMapper.insert(activityCustomer);
		}
	}
	
	
	/**
	 * 取消参加活动
	 * @param param
	 * @return
	 */
	public Map<String, Object> cancelJoinActivity(CmsJoinActivityParam param) {
		Map<String, Object> resMap = new HashMap<>();
		String activityId = param.getActivityId();
		ActivityInfo activityInfo = mapper.selectByPrimaryKey(activityId);
		if (activityInfo != null) {
			String customerId = param.getCustomerId();
			ActivityCustomer activityCustomer = new ActivityCustomer();
			activityCustomer.setActivityId(activityId);
			activityCustomer.setJoin("1");
			activityCustomer.setCustomerId(customerId);
			activityCustomerMapper.delete(activityCustomer);
			resMap.put("code", ResponseCode.OK.getCode());
			resMap.put("msg", ResponseCode.OK.getMessage());
			resMap.put("success", true);
		}else {
			resMap.put("success", false);
			return ResponseHelper.success(resMap, null, "01", "活动不存在");
		}
		return resMap;
	}
	
	/**
	 * 添加活动
	 * @param param
	 * @return
	 */
	public Map<String, Object> addActivity(CmsSaveActivityParam param) {
		log.info("【添加活动】。。。。请求参数{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		ActivityInfo activityInfo = new ActivityInfo();
		try {
			BeanUtils.copyProperties(activityInfo, param);
			String id = String.valueOf(SnowFlake.getId());
			activityInfo.setId(id);
			activityInfo.setCommentCount(0);
			activityInfo.setCreateTime(new Date());
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			activityInfo.setActivityStartTime(format.parse(param.getActivityStartTimeStr()));
			activityInfo.setActivityEndTime(format.parse(param.getActivityEndTimeStr()));
			if (mapper.insert(activityInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加活动】  响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【修改活动】 报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加活动】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【添加活动】   响应参数：{}", respMap);
		return respMap;
	}
	
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateActivity(CmsSaveActivityParam param) {
		log.info("【修改活动】。。。。请求参数{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		ActivityInfo activityInfo = new ActivityInfo();
		try {
			BeanUtils.copyProperties(activityInfo, param);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			activityInfo.setActivityStartTime(format.parse(param.getActivityStartTimeStr()));
			activityInfo.setActivityEndTime(format.parse(param.getActivityEndTimeStr()));
			if (mapper.updateByPrimaryKeySelective(activityInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【修改活动】  响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【修改活动】 报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【修改活动】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【修改活动】   响应参数：{}", respMap);
		return respMap;
	}
	
	
	/**
	 * 分页查询活动
	 * @param param
	 * @return
	 */
	public TableResultResponse<ActivityInfo> queryActivitiesWithPage(CmsQueryActivityParam param) {
		log.info("查询活动列表。。。。{}"+param);
		Example example = new Example(ActivityInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if (param.getActivityName() != null && param.getActivityName().length() > 0) {
			criteria.andEqualTo("activityName",param.getActivityName());
		}
		example.setOrderByClause("create_time desc");
		Page<ActivityInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<ActivityInfo> list = mapper.selectByExample(example);
		TableResultResponse<ActivityInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	
	/**
	 * 关闭活动
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteActivity(CmsSaveActivityParam param) {
		log.info("【删除活动】。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		ActivityInfo activityInfo = new ActivityInfo();
		try {
			BeanUtils.copyProperties(activityInfo, param);
			if (mapper.delete(activityInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除活动】  响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【删除活动】 报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除活动】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【删除活动】   响应参数：{}", respMap);
		return respMap;
	}
}