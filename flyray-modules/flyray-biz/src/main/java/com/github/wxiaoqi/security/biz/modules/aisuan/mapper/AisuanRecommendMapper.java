package com.github.wxiaoqi.security.biz.modules.aisuan.mapper;


import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanRecommend;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-10 11:45:46
 */
@org.apache.ibatis.annotations.Mapper
public interface AisuanRecommendMapper extends Mapper<AisuanRecommend> {
	
}
