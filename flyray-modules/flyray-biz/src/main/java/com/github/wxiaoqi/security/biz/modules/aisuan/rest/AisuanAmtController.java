package com.github.wxiaoqi.security.biz.modules.aisuan.rest;

import com.github.wxiaoqi.security.biz.feign.AdminFeign;
import com.github.wxiaoqi.security.biz.modules.aisuan.biz.AisuanAmtBiz;
import com.github.wxiaoqi.security.biz.modules.aisuan.biz.AisuanRecommendBiz;
import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanAmt;
import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanRecommend;
import com.github.wxiaoqi.security.common.cms.request.AisuanAddRecommendParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanQueryAmtParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanUpdateAmtParam;
import com.github.wxiaoqi.security.common.cms.request.BaseParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping("aisuanAmtAdmin")
public class AisuanAmtController extends BaseController<AisuanAmtBiz,AisuanAmt> {
	@Autowired
	private AisuanAmtBiz aisuanAmtBiz;
	/**
	 * 查询平台下的用户
	 */
	@RequestMapping(value = "/queryAmt",method = RequestMethod.POST)
	@ResponseBody
	public TableResultResponse<AisuanAmt> queryUserUnderPlatform(@RequestBody AisuanQueryAmtParam params) {
		log.info("查询平台下的用户传入参数{}", EntityUtils.beanToMap(params));
		Map<String, Object> map = new HashMap<>();
		Query query = new Query(EntityUtils.beanToMap(params));
		TableResultResponse<AisuanAmt> response = aisuanAmtBiz.selectByQuery(query);
		log.info("查询平台下的用户返回{}", EntityUtils.beanToMap(response));
		return response;
	}
	/**
	 * 修改金額
	 */
	 @RequestMapping(value = "/updateAmt",method = RequestMethod.POST)
	 @ResponseBody
	 public Map<String, Object> updateAmt(@RequestBody AisuanUpdateAmtParam param) {
	  log.info("修改金額传入参数{}", EntityUtils.beanToMap(param));
	  Map<String, Object> result = aisuanAmtBiz.update(param);
	  return result;
	 }
	 
	
	
}