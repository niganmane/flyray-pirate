package com.github.wxiaoqi.security.biz.modules.restaurant.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantTableInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantTableInfo;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantTableRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序点餐餐桌信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/tableInfo")
public class RestaurantTableInfoRestController extends BaseController<RestaurantTableInfoBiz, RestaurantTableInfo> {
	
	@Autowired
	private RestaurantTableInfoBiz restaurantTableInfoBiz;
	
	/**
	 * 查询点餐餐桌信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<RestaurantTableInfo> query(@RequestBody RestaurantQueryParam param) {
		log.info("查询点餐餐桌信息列表------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return restaurantTableInfoBiz.queryRestaurantTableInfoPage(param);
	}
	
	/**
	 * 添加餐桌
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody RestaurantTableRequestParam param) {
		log.info("添加餐桌------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		Map<String, Object> respMap = restaurantTableInfoBiz.addRestaurantTableInfo(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("添加餐桌-----end-------------{}", respMap);
		return respMap;
	}
	
	/**
	 * 删除餐桌
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody RestaurantTableRequestParam param) {
		log.info("删除餐桌------start------{}", param);
		Map<String, Object> respMap = restaurantTableInfoBiz.deleteRestaurantTableInfo(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除餐桌------end------{}", respMap);
		return respMap;
	}

	/**
	 * 修改餐桌
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody RestaurantTableRequestParam param) {
		log.info("修改餐桌------start------{}", param);
		Map<String, Object> respMap = restaurantTableInfoBiz.updateRestaurantTableInfo(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("修改餐桌------end------{}", respMap);
		return respMap;
	}

}
