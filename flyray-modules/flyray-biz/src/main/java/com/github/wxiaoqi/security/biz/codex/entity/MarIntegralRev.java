package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 积分领取表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Table(name = "mar_integral_rev")
public class MarIntegralRev implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Long seqNo;
    
    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
    
    //商户号
    @Column(name = "MERCHANT_ID")
    private String merchantId;
	
	    //商户会员号
    @Column(name = "personal_id")
    private String personalId;
	
	    //积分领取数量
    @Column(name = "INTEGRAL_AMT")
    private Integer integralAmt;
	
	    //领取时间
    @Column(name = "RECEIVER_TIME")
    private Date receiverTime;
	
	    //场景编号
    @Column(name = "SCENE_ID")
    private String sceneId;
	
	    //活动编号
    @Column(name = "ACT_ID")
    private String actId;
	
	    //支付订单号
    @Column(name = "MERORDERNO")
    private String merorderno;
	
	    //事件编号
    @Column(name = "INC_CODE")
    private String incCode;
	
	    //积分id
    @Column(name = "INTEGRAL_ID")
    private String integralId;
	

	/**
	 * 设置：
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：
	 */
	public Long getSeqNo() {
		return seqNo;
	}
	
	/**
	 * 设置：积分领取数量
	 */
	public void setIntegralAmt(Integer integralAmt) {
		this.integralAmt = integralAmt;
	}
	/**
	 * 获取：积分领取数量
	 */
	public Integer getIntegralAmt() {
		return integralAmt;
	}
	/**
	 * 设置：领取时间
	 */
	public void setReceiverTime(Date receiverTime) {
		this.receiverTime = receiverTime;
	}
	/**
	 * 获取：领取时间
	 */
	public Date getReceiverTime() {
		return receiverTime;
	}
	/**
	 * 设置：场景编号
	 */
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	/**
	 * 获取：场景编号
	 */
	public String getSceneId() {
		return sceneId;
	}
	/**
	 * 设置：活动编号
	 */
	public void setActId(String actId) {
		this.actId = actId;
	}
	/**
	 * 获取：活动编号
	 */
	public String getActId() {
		return actId;
	}
	/**
	 * 设置：支付订单号
	 */
	public void setMerorderno(String merorderno) {
		this.merorderno = merorderno;
	}
	/**
	 * 获取：支付订单号
	 */
	public String getMerorderno() {
		return merorderno;
	}
	/**
	 * 设置：事件编号
	 */
	public void setIncCode(String incCode) {
		this.incCode = incCode;
	}
	/**
	 * 获取：事件编号
	 */
	public String getIncCode() {
		return incCode;
	}
	/**
	 * 设置：积分id
	 */
	public void setIntegralId(String integralId) {
		this.integralId = integralId;
	}
	/**
	 * 获取：积分id
	 */
	public String getIntegralId() {
		return integralId;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getPersonalId() {
		return personalId;
	}
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	
}
