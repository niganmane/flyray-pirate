package com.github.wxiaoqi.security.biz.codex.api;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.wxiaoqi.security.biz.codex.biz.MarRandomCouponBiz;
import com.github.wxiaoqi.security.biz.codex.biz.MarRandomCouponLevelBiz;
import com.github.wxiaoqi.security.biz.codex.entity.MarRandomCoupon;
import com.github.wxiaoqi.security.biz.codex.entity.MarRandomCouponLevel;
import com.github.wxiaoqi.security.biz.util.NumberUtils;
import com.github.wxiaoqi.security.biz.util.RedEnvelopUtil;
import com.github.wxiaoqi.security.common.rest.BaseController;

/**
 * 随机红包-内部
 * @author Administrator
 *
 */
@Controller
@RequestMapping("marRandomCouponLevel")
public class MarRandomCouponLevelController extends BaseController<MarRandomCouponLevelBiz,MarRandomCouponLevel> {

	private static final Logger logger = LoggerFactory.getLogger(MarRandomCouponLevelController.class);
	
	
	@Autowired
	private MarRandomCouponBiz randomCouponBiz;
	/**
	 * 添加红包档
	 * @param couId
	 * @param firstAmt
	 * @param firstCount
	 * @param secondAmt
	 * @param secondCount
	 * @param thirdAmt
	 * @param thirdCount
	 */
	public void addRandomCouponLevel(String couId, BigDecimal firstAmt, Integer firstCount, BigDecimal secondAmt, Integer secondCount, BigDecimal thirdAmt, Integer thirdCount) {
		logger.info("添加红包档。。。");
		MarRandomCouponLevel couponLevel = new MarRandomCouponLevel();
		couponLevel.setCouId(couId);
		couponLevel.setFirstAmt(firstAmt);
		couponLevel.setFirstCount(firstCount);
		couponLevel.setSecondAmt(secondAmt);
		couponLevel.setSecondCount(secondCount);
		couponLevel.setThirdAmt(thirdAmt);
		couponLevel.setThirdCount(thirdCount);
		baseBiz.insertSelective(couponLevel);
		logger.info("添加红包档...结束");
	}
	
	/**
	 * 产生随机红包
	 * @param couId
	 * @param couStatus
	 * @param level
	 * @param amt
	 * @param count
	 * @param max
	 * @param min
	 */
	public void addRandomCoupon(String couId, String couStatus, double firstAmt, Integer firstCount,double oneMax, double oneMin, double secondAmt, Integer secondCount,double twoMax, double twoMin, double thirdAmt, Integer thirdCount,double threeMax, double threeMin) {
		logger.info("添加随机红包。。。{}");
		List<MarRandomCoupon> list = new ArrayList<>();
		long startTime1=System.currentTimeMillis();
		double[] first = RedEnvelopUtil.generate(firstAmt, firstCount, oneMax, oneMin);
		if (first != null && first.length > 0) {
			for (double d : first) {
				MarRandomCoupon randomCoupon = new MarRandomCoupon();
				String coupSerial = NumberUtils.generateSerialNumber();
				randomCoupon.setCouId(couId);
				randomCoupon.setCouSerial(coupSerial);
				randomCoupon.setCouAmt(new BigDecimal(d));
				randomCoupon.setCouLevel("01");
				randomCoupon.setRevStatus(couStatus);
				randomCoupon.setIsReceived("00");
				DateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				try {
					randomCoupon.setCreteTime(sdf1.parse(sdf1.format(new Date())));
				} catch (java.text.ParseException e) {
					e.printStackTrace();
				}
				list.add(randomCoupon);
			}
		}
		long endTime1=System.currentTimeMillis();
		System.out.println("1级耗时："+(endTime1-startTime1)+"ms");
		long startTime2=System.currentTimeMillis();
		double[] second = RedEnvelopUtil.generate(secondAmt, secondCount, twoMax, twoMin);
		if (second != null && second.length > 0) {
			for (double d : second) {
				MarRandomCoupon randomCoupon = new MarRandomCoupon();
				String coupSerial = NumberUtils.generateSerialNumber();
				randomCoupon.setCouId(couId);
				randomCoupon.setCouSerial(coupSerial);
				randomCoupon.setCouAmt(new BigDecimal(d));
				randomCoupon.setCouLevel("02");
				randomCoupon.setRevStatus(couStatus);
				randomCoupon.setIsReceived("00");
				DateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				try {
					randomCoupon.setCreteTime(sdf1.parse(sdf1.format(new Date())));
				} catch (java.text.ParseException e) {
					e.printStackTrace();
				}
				list.add(randomCoupon);
			}
		}
		long endTime2=System.currentTimeMillis();
		System.out.println("2级耗时："+(endTime2-startTime2)+"ms");
		long startTime3=System.currentTimeMillis();
		double[] third = RedEnvelopUtil.generate(thirdAmt, thirdCount, threeMax, threeMin);
		if (third != null && third.length > 0) {
			for (double d : third) {
				MarRandomCoupon randomCoupon = new MarRandomCoupon();
				String coupSerial = NumberUtils.generateSerialNumber();
				randomCoupon.setCouId(couId);
				randomCoupon.setCouSerial(coupSerial);
				randomCoupon.setCouAmt(new BigDecimal(d));
				randomCoupon.setCouLevel("03");
				randomCoupon.setRevStatus(couStatus);
				randomCoupon.setIsReceived("00");
				DateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				try {
					randomCoupon.setCreteTime(sdf1.parse(sdf1.format(new Date())));
				} catch (java.text.ParseException e) {
					e.printStackTrace();
				}
				list.add(randomCoupon);
			}
		} 
		long endTime3=System.currentTimeMillis();
		System.out.println("3级耗时："+(endTime3-startTime3)+"ms");
		long startTime4=System.currentTimeMillis();
		if (list != null && list.size() > 0) {
			int pointsDataLimit = 1000;//限制条数
			Integer size = list.size();
			if (size > pointsDataLimit) {
				int part = size/pointsDataLimit;
				System.out.println("共有 ： "+size+"条，！"+" 分为 ："+part+"批");
				for(int i = 0;i < part;i++) {
					List<MarRandomCoupon> listPage = list.subList(0, pointsDataLimit);
					randomCouponBiz.addRandomCouponList(listPage);
					list.subList(0, pointsDataLimit).clear();
				}
				if (!list.isEmpty()) {
					randomCouponBiz.addRandomCouponList(list);
				}
			}else {
				randomCouponBiz.addRandomCouponList(list);
			}
		}
		long endTime4=System.currentTimeMillis();
		System.out.println("插入耗时："+(endTime4-startTime4)+"ms");
		logger.info("添加随机红包结束。。。{}");
	}
}