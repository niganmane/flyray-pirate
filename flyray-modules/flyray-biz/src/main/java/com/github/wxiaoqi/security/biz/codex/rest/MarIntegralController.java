package com.github.wxiaoqi.security.biz.codex.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.codex.biz.MarIntegralBiz;
import com.github.wxiaoqi.security.biz.codex.entity.MarIntegral;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.common.util.SerialNoUtil;

/**
 * 积分配置 - 后台管理
 * 
 * @author zgk
 * 
 */
@Controller
@RequestMapping("marIntegrals")
public class MarIntegralController extends BaseController<MarIntegralBiz, MarIntegral> {

	private final static Logger logger = LoggerFactory.getLogger(MarIntegralController.class);

	@Autowired
	private MarIntegralBiz integralInfoBiz;

	/**
	 * 积分活动添加
	 * 
	 * @param
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public ObjectRestResponse<MarIntegral> addIntegralInfo(@RequestBody Map<String, Object> params) throws ParseException {

		logger.info("添加积分。。。{}"+params);
		MarIntegral marIntegral = new MarIntegral();
		String faceValue = (String) params.get("integralAmt");
		String incNumStr = (String) params.get("incNum");
		String createTimeStr = (String) params.get("createTime");
		String shutTimeStr = (String) params.get("shutTime");
		String integraliInfo = (String) params.get("incRemark");
		String actId = (String) params.get("actId");
		String everyDayStr = (String) params.get("everyDay");
		String everyMonthStr = (String) params.get("everyMonth");
		String everyWeekStr = (String) params.get("everyWeek");
		String incCode = (String) params.get("incCode");
		String integralStatus = (String) params.get("integralStatus");
		integralStatus = integralInfoBiz.getIntegralStatus(actId,integralStatus);
		String sceneId = (String) params.get("sceneId");
		int everyDay = Integer.valueOf(everyDayStr).intValue();
		int everyMonth = Integer.valueOf(everyMonthStr).intValue();
		int everyWeek = Integer.valueOf(everyWeekStr).intValue();
		int incNum = Integer.valueOf(incNumStr).intValue();
		int integralAmt = Integer.valueOf(faceValue).intValue();
		int integralAmtInc = integralAmt * incNum;
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
		if (!StringUtils.isEmpty(createTimeStr)) {
			createTimeStr = createTimeStr.replace("Z", " UTC");
			System.out.println("开始时间："+format.parse(createTimeStr));
			marIntegral.setCreateTime(format.parse(createTimeStr));
		}
		if (!StringUtils.isEmpty(shutTimeStr)) {
			shutTimeStr = shutTimeStr.replace("Z", " UTC");
			System.out.println("结束时间："+format.parse(shutTimeStr));
			marIntegral.setShutTime(format.parse(shutTimeStr));
		}
		try {
			marIntegral.setAddTime(sdf2.parse(sdf2.format(new Date())));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		marIntegral.setActId(actId);
		marIntegral.setEveryDay(everyDay);
		marIntegral.setEveryMonth(everyMonth);
		marIntegral.setEveryWeek(everyWeek);
		marIntegral.setIncCode(incCode);
		marIntegral.setIncNum(incNum);
		marIntegral.setSceneId(sceneId);
		marIntegral.setIntegralAmt(integralAmt);
		marIntegral.setIntegralAmtInc(integralAmtInc);
		marIntegral.setIntegralId(SerialNoUtil.createID());
		marIntegral.setIntegraliInfo(integraliInfo);
		marIntegral.setIntegralStatus(integralStatus);
		marIntegral.setPayment("01");
		integralInfoBiz.insertSelective(marIntegral);
		logger.info("添加积分结束。。。");
		return new ObjectRestResponse<MarIntegral>();
	}

	/**
	 * 积分活动详情查看
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/info", method = RequestMethod.POST)
	@ResponseBody
	public ObjectRestResponse<MarIntegral> integralManage(@RequestBody Map<String, Object> params) {
		logger.info("查看积分详情。。。{}"+params);
		ObjectRestResponse<MarIntegral> entityObjectRestResponse = new ObjectRestResponse<>();
		String integralId = (String) params.get("integralId");
		MarIntegral marIntegral = integralInfoBiz.selectByIntegralId(integralId);
        entityObjectRestResponse.data(marIntegral);
        return entityObjectRestResponse;
	}

	/**
	 * 积分活动内容修改
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ObjectRestResponse<MarIntegral> updateIntegralInfo(@RequestBody Map<String, Object> params) {
		
		logger.info("积分修改。。。{}"+params);
		String integralId = (String) params.get("integralId");
		MarIntegral marIntegral = integralInfoBiz.selectByIntegralId(integralId);
		String faceValue = params.get("integralAmt").toString();
		String incNumStr = params.get("incNum").toString();
		String createTimeStr = (String) params.get("createTime");
		String shutTimeStr = (String) params.get("shutTime");
		String integraliInfo = (String) params.get("incRemark");
		String actId = (String) params.get("actId");
		String everyDayStr = params.get("everyDay").toString();
		String everyMonthStr = params.get("everyMonth").toString();
		String everyWeekStr = params.get("everyWeek").toString();
		String incCode = (String) params.get("incCode");
		String integralStatus = (String) params.get("integralStatus");
		integralStatus = integralInfoBiz.getIntegralStatus(actId,integralStatus);
		String sceneId = (String) params.get("sceneId");
		int everyDay = Integer.valueOf(everyDayStr).intValue();
		int everyMonth = Integer.valueOf(everyMonthStr).intValue();
		int everyWeek = Integer.valueOf(everyWeekStr).intValue();
		int incNum = Integer.valueOf(incNumStr).intValue();
		int integralAmt = Integer.valueOf(faceValue).intValue();
		int integralAmtInc = integralAmt * incNum;
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date createTime = null;
		Date shutTime = null;
		try {
			createTime = formatter.parse(createTimeStr);
			shutTime = formatter.parse(shutTimeStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		marIntegral.setActId(actId);
		marIntegral.setCreateTime(createTime);
		marIntegral.setEveryDay(everyDay);
		marIntegral.setEveryMonth(everyMonth);
		marIntegral.setEveryWeek(everyWeek);
		marIntegral.setIncCode(incCode);
		marIntegral.setIncNum(incNum);
		marIntegral.setSceneId(sceneId);
		marIntegral.setIntegralAmt(integralAmt);
		marIntegral.setIntegralAmtInc(integralAmtInc);
		marIntegral.setIntegraliInfo(integraliInfo);
		marIntegral.setIntegralStatus(integralStatus);
		marIntegral.setPayment("01");
		marIntegral.setShutTime(shutTime);
		integralInfoBiz.updateSelectiveById(marIntegral);
		logger.info("积分修改结束。。。");
		return new ObjectRestResponse<MarIntegral>();
	}

	
	/**
	 * 删除积分活动详情
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ObjectRestResponse<MarIntegral> deleteIntegralInfo(@RequestBody Map<String, Object> params) {
		Integer seqNo = (Integer) params.get("seqNo");
		Long seqNoLong = Long.valueOf(String.valueOf(seqNo));
		integralInfoBiz.deleteById(seqNoLong);
		return new ObjectRestResponse<MarIntegral>();
	}
	
	/**
	 * 停用积分活动详情
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/stop", method = RequestMethod.POST)
	@ResponseBody
	public ObjectRestResponse<MarIntegral> stopIntegralInfo(@RequestBody Map<String, Object> params) {
		logger.info("积分停用。。。{}"+params);
		String integralId = (String) params.get("integralId");
		MarIntegral marIntegral = integralInfoBiz.selectByIntegralId(integralId);
		marIntegral.setIntegralStatus("01");
		integralInfoBiz.updateSelectiveById(marIntegral);
		logger.info("积分停用结束。。。");
		return new ObjectRestResponse<MarIntegral>();
	}

	/**
	 * 积分管理
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/integralManage", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> integralManage(@RequestBody MarIntegral marIntegral) {
//		Map<String, Object> map = integralInfoBiz.getIntegralInfo(marIntegral);
		return null;
	}
	
	/**
	 * 条件查询积分列表
	 * @param params
	 * @return
	 * @throws ParseException 
	 */
	@ResponseBody
	@RequestMapping(value = "/page", method = RequestMethod.POST)
	public TableResultResponse<MarIntegral> getCouponList(@RequestParam("page") String page, @RequestParam("limit") String limit, String createTime, String shutTime, String actId) throws ParseException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("page", page);
		params.put("limit", limit);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		if(!StringUtils.isEmpty(createTime)){
			createTime  = createTime.replace("Z", " UTC");
			Date ct = format.parse(createTime);
			params.put("createTime", formatter.format(ct));
		}
		if(!StringUtils.isEmpty(shutTime)){
			shutTime = shutTime.replace("Z", " UTC");
			Date rs = format.parse(shutTime);
			Calendar c = Calendar.getInstance();
		    c.setTime(rs);
		    c.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天
		    Date endD = c.getTime();
			params.put("shutTime", formatter.format(endD));
		}
		if(StringUtils.isNotBlank(actId)){   
			params.put("actId", actId);
		}
		logger.info("积分列表。。。"+params);
		Query query = new Query(params);
		Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		List<MarIntegral> list = integralInfoBiz.getIntegralList(params);
		return new TableResultResponse<MarIntegral>(result.getTotal(), list);
	}
	
	
	
	
	
	
	
	
	
}
