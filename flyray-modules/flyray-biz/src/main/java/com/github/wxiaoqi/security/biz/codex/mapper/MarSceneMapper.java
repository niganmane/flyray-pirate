package com.github.wxiaoqi.security.biz.codex.mapper;

import com.github.wxiaoqi.security.biz.codex.entity.MarScene;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@org.apache.ibatis.annotations.Mapper
public interface MarSceneMapper extends Mapper<MarScene> {
	
}
