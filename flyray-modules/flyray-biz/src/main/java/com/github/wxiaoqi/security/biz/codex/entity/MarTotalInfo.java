package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 红包积分统计
 * @author centerroot
 * @time 创建时间:2018年3月19日下午5:43:25
 * @description
 */
public class MarTotalInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7045175714661027147L;
	/**
	 * 统计订单数量
	 */
	private int totalNum;
	/**
	 * 统计金额
	 */
	private BigDecimal totalAmt;
	public int getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}
	public BigDecimal getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	@Override
	public String toString() {
		return "MarTotalInfo [totalNum=" + totalNum + ", totalAmt=" + totalAmt + "]";
	}
	
}
