package com.github.wxiaoqi.security.biz.modules.restaurant.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantAppraisalDetailInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 菜品评价明细表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantAppraisalDetailInfoMapper extends Mapper<RestaurantAppraisalDetailInfo> {
	
	/**
	 * 按评论时间降序查询评论明细列表
	 * @param restaurantAppraisalDetailInfo
	 * @return
	 */
	public List<RestaurantAppraisalDetailInfo> queryDetailInfoList(RestaurantAppraisalDetailInfo restaurantAppraisalDetailInfo);
	
	/**
	 * 分页查询评论详情列表
	 * @param map
	 * @return
	 */
	List<RestaurantAppraisalDetailInfo> queryRestaurantAppraisalDetailInfo(Map<String, Object> map);
	
}
