package com.github.wxiaoqi.security.biz.mqlistener;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.github.wxiaoqi.security.biz.modules.pay.biz.PayCallBackBiz;
import com.github.wxiaoqi.security.biz.modules.refund.biz.RefundCallBackBiz;

import lombok.extern.slf4j.Slf4j;

/** 
* @author: bolei
* @date：2018年7月28日 下午5:13:48 
* @description：支付相关业务回调通过MQ处理
*/

@Component
@RabbitListener(queues = "topic.payment")
@Slf4j
public class paymentMqHandler {
	
	@Autowired
	private PayCallBackBiz payCallBackBiz;
	@Autowired
	private RefundCallBackBiz refundCallBackBiz;

	@RabbitHandler
    public void process(String request) {
		if(!StringUtils.isEmpty(request)){
			//出入账处理
			try {
				Map<String, Object> reqMap = JSON.parseObject(request);
				String tradeType = (String) reqMap.get("tradeType");
				if("1".equals(tradeType)){//支付回调
					payCallBackBiz.payCallBack(reqMap);
				}else if("2".equals(tradeType)){//退款回调
					refundCallBackBiz.refundCallBack(reqMap);;
				}
			} catch (Exception e) {
				e.printStackTrace();
				log.info("监听消息队列--topic.payment- 报错"+e.getMessage());
			}
		}
		
    }
}
