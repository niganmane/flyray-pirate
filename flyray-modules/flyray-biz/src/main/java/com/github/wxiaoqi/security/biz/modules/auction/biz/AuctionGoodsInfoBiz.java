package com.github.wxiaoqi.security.biz.modules.auction.biz;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.modules.auction.entity.AuctionGoodsInfo;
import com.github.wxiaoqi.security.biz.modules.auction.mapper.AuctionGoodsInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.AuctionQueryParam;
import com.github.wxiaoqi.security.common.cms.request.AuctionRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 竞拍商品信息表
 *
 * @author he
 * @date 2018-07-16 11:38:52
 */
@Slf4j
@Service
public class AuctionGoodsInfoBiz extends BaseBiz<AuctionGoodsInfoMapper,AuctionGoodsInfo> {
	

	@Autowired
	private AuctionGoodsInfoMapper auctionGoodsInfoMapper;
	
	/**
	 * 竞拍商品信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> query(Map<String, Object> request){
		log.info("竞拍商品信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		Example example = new Example(AuctionGoodsInfo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("platformId", platFormId);
		criteria.andEqualTo("merchantId", merId);
		example.setOrderByClause("create_time desc");
		List<AuctionGoodsInfo> goodsInfoList = auctionGoodsInfoMapper.selectByExample(example);
		response.put("goodsInfoList", goodsInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("竞拍商品信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 竞拍商品信息详情查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryDetail(Map<String, Object> request){
		log.info("竞拍商品信息详情查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String goodsId = (String) request.get("goodsId");
		AuctionGoodsInfo auctionGoodsInfo = new AuctionGoodsInfo();
		auctionGoodsInfo.setPlatformId(platFormId);
		auctionGoodsInfo.setMerchantId(merId);
		auctionGoodsInfo.setGoodsId(goodsId);
		AuctionGoodsInfo selectGoodsInfo = auctionGoodsInfoMapper.selectOne(auctionGoodsInfo);
		Date deadlineTime = selectGoodsInfo.getDeadlineTime();
		if(deadlineTime.before(new Date())){
			response.put("auctionSwitch", "1");//竞拍关闭
		}else{
			response.put("auctionSwitch", "0");//竞拍开
		}
		response.put("auctionGoodsInfo", selectGoodsInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("竞拍商品信息详情查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 分页查询竞拍商品
	 * @param param
	 * @return
	 */
	public TableResultResponse<AuctionGoodsInfo> queryAuctionGoodsPage(AuctionQueryParam param) {
		log.info("分页查询竞拍商品。。。。{}"+param);
		Example example = new Example(AuctionGoodsInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if (param.getGoodsId() != null && param.getGoodsId().length() > 0) {
			criteria.andEqualTo("goodsId",param.getGoodsId());
		}
		example.setOrderByClause("create_time desc");
		Page<AuctionGoodsInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<AuctionGoodsInfo> list = mapper.selectByExample(example);
		TableResultResponse<AuctionGoodsInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	/**
	 * 添加商品
	 * @param param
	 * @return
	 */
	public Map<String, Object> addGoodsInfo(AuctionRequestParam param) {
		log.info("【添加竞拍商品】。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		AuctionGoodsInfo goodsInfo = new AuctionGoodsInfo();
		try {
			BeanUtils.copyProperties(goodsInfo, param);
			goodsInfo.setGoodsId(String.valueOf(SnowFlake.getId()));
			goodsInfo.setCreateTime(new Date());
			goodsInfo.setCurrentPrice(new BigDecimal(param.getStartingPrice()));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			goodsInfo.setDeadlineTime(sdf.parse(param.getDeadlineTimeStr()));
			goodsInfo.setStartingPrice(new BigDecimal(param.getStartingPrice()));
			if (mapper.insert(goodsInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加竞拍商品】   响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【添加竞拍商品】  报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加竞拍商品】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【添加竞拍商品】   响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 删除商品
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteGoodsInfo(AuctionRequestParam param) {
		log.info("【删除商品】。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		AuctionGoodsInfo goodsInfo = new AuctionGoodsInfo();
		try {
			BeanUtils.copyProperties(goodsInfo, param);
			goodsInfo.setId(Long.valueOf(param.getId()));
			 if (mapper.delete(goodsInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除商品】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【删除商品】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除商品】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【删除商品】   响应参数：{}", respMap);
		 return respMap;
	}
	
	/**
	 * 修改商品
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateGoodsInfo(AuctionRequestParam param) {
		log.info("【修改商品】 。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		AuctionGoodsInfo goodsInfo = new AuctionGoodsInfo();
		try {
			BeanUtils.copyProperties(goodsInfo, param);
			goodsInfo.setId(Long.valueOf(param.getId()));
			goodsInfo.setCurrentPrice(new BigDecimal(param.getStartingPrice()));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			goodsInfo.setDeadlineTime(sdf.parse(param.getDeadlineTimeStr()));
			goodsInfo.setStartingPrice(new BigDecimal(param.getStartingPrice()));
			if (mapper.updateByPrimaryKeySelective(goodsInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【修改商品】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【修改商品】  报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【修改商品】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【修改商品】   响应参数：{}", respMap);
		 return respMap;
	}
}