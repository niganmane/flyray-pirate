package com.github.wxiaoqi.security.biz.modules.restaurant.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantPictureInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantPictureInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 餐厅照片信息表
 *
 * @author Mr.AG
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantPictureInfoBiz extends BaseBiz<RestaurantPictureInfoMapper,RestaurantPictureInfo> {
}