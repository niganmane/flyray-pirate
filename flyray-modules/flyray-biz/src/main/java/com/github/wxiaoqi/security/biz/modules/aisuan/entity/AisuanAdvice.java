package com.github.wxiaoqi.security.biz.modules.aisuan.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-10 11:45:46
 */
@Table(name = "aisuan_advice")
public class AisuanAdvice implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //主键
    @Id
    private Long id;
	
	    //提交的建议内容
    @Column(name = "advice")
    private String advice;
	
	    //回复内容
    @Column(name = "content")
    private String content;
	
	    //提交时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //回复时间
    @Column(name = "update_time")
    private Date updateTime;
	
	    //平台编号
    @Column(name = "platform_id")
    private Long platformId;
	
	    //商户编号
    @Column(name = "merchant_id")
    private Long merchantId;
	
	    //会员编号
    @Column(name = "customer_id")
    private Long customerId;
	
    @Column(name = "apply_user_id")
    private Long applyUserId;
	/**
	 * 设置：主键
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：提交的建议内容
	 */
	public void setAdvice(String advice) {
		this.advice = advice;
	}
	/**
	 * 获取：提交的建议内容
	 */
	public String getAdvice() {
		return advice;
	}
	/**
	 * 设置：回复内容
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：回复内容
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：提交时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：提交时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：回复时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：回复时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public Long getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户编号
	 */
	public Long getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：会员编号
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：会员编号
	 */
	public Long getCustomerId() {
		return customerId;
	}
	public Long getApplyUserId() {
		return applyUserId;
	}
	public void setApplyUserId(Long applyUserId) {
		this.applyUserId = applyUserId;
	}
	
}
