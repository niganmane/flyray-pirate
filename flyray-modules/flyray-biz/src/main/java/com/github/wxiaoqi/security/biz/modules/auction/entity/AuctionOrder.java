package com.github.wxiaoqi.security.biz.modules.auction.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 竞拍订单表
 * @author he
 * @date 2018-08-10 13:36:20
 */
@Table(name = "auction_order")
public class AuctionOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//用户账号
	@Column(name = "per_id")
	private String perId;
	
	//支付订单号
	@Column(name = "pay_order_no")
	private String payOrderNo;
	
	//退款订单号
	@Column(name = "refund_order_no")
	private String refundOrderNo;

	//商品编号
	@Column(name = "goods_id")
	private String goodsId;

	//金额
	@Column(name = "tx_amt")
	private BigDecimal txAmt;

	//序号
	@Column(name = "number")
	private Integer number;

	//状态 00支付成功 01支付失败 02退款成功 03退款失败
	@Column(name = "status")
	private String status;

	//创建时间
	@Column(name = "create_time")
	private Date createTime;


	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：用户账号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：用户账号
	 */
	public String getPerId() {
		return perId;
	}
	public String getPayOrderNo() {
		return payOrderNo;
	}
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	public String getRefundOrderNo() {
		return refundOrderNo;
	}
	public void setRefundOrderNo(String refundOrderNo) {
		this.refundOrderNo = refundOrderNo;
	}
	/**
	 * 设置：商品编号
	 */
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	/**
	 * 获取：商品编号
	 */
	public String getGoodsId() {
		return goodsId;
	}
	/**
	 * 设置：金额
	 */
	public void setTxAmt(BigDecimal txAmt) {
		this.txAmt = txAmt;
	}
	/**
	 * 获取：金额
	 */
	public BigDecimal getTxAmt() {
		return txAmt;
	}
	/**
	 * 设置：序号
	 */
	public void setNumber(Integer number) {
		this.number = number;
	}
	/**
	 * 获取：序号
	 */
	public Integer getNumber() {
		return number;
	}
	/**
	 * 设置：状态  00支付成功 01支付失败 02退款成功 03退款失败
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：状态  00支付成功 01支付失败 02退款成功 03退款失败
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
