	package com.github.wxiaoqi.security.biz.codex.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.codex.biz.CodexEngineBiz;
import com.github.wxiaoqi.security.biz.codex.entity.MarActivity;
import com.github.wxiaoqi.security.biz.codex.entity.MarCoupon;
import com.github.wxiaoqi.security.biz.codex.entity.MarIncident;
import com.github.wxiaoqi.security.biz.codex.mapper.MarActivityMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarCouponMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarIncidentMapper;
import com.github.wxiaoqi.security.biz.feign.CrmFeign;
import com.github.wxiaoqi.security.common.biz.request.CouponReceiveReq;
import com.github.wxiaoqi.security.common.crm.request.PersonalBaseRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

/**
 * 定向红包领取
 * @author Administrator
 *
 */
@Controller
@RequestMapping("assign")
public class MarAssignCouponRevController{
	
	private final static Logger logger = LoggerFactory.getLogger(MarAssignCouponRevController.class);
	
	@Autowired
	private CrmFeign crmFeign;
	@Autowired
	private CodexEngineBiz codexEngineBiz;
	@Autowired
	private MarActivityMapper marActivityMapper;
	@Autowired
	private MarCouponMapper marCouponMapper;
	@Autowired
	private MarIncidentMapper marIncidentMapper;
	@Value("${citizencard.platformId}")
	private String platformId;
	@Value("${citizencard.merchantId}")
	private String merchantId;
	
	/**
	 * 定向红包领取
	 * @param params
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value = "/couponRev", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> assignCouponRev(String mobile,String couponId,String incCode,String merOrderNo,String orderAmt) throws ParseException {
		logger.info("定向红包领取mobile........{}", mobile);
		logger.info("定向红包领取couponId........{}", couponId);
		logger.info("定向红包领取incCode........{}", incCode);
		logger.info("定向红包领取merOrderNo........{}", merOrderNo);
		logger.info("定向红包领取orderAmt........{}", orderAmt);
		Map<String, Object> response = new HashMap<String, Object>();
		//根据手机号查询注册信息是否存在
		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("platformId", platformId);
		reqMap.put("phone", mobile);
		Map<String, Object> respMap = crmFeign.queryPersonalInfo(reqMap);

		if (null == respMap || !ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			response.put("code", ResponseCode.PER_NOTEXIST.getCode());
			response.put("msg", ResponseCode.PER_NOTEXIST.getMessage());
			return response;
		}
		PersonalBaseRequest personalBaseEntity = (PersonalBaseRequest) respMap.get("personalBase");
		
		//调用红包领取
		CouponReceiveReq couponReceiveReq = new CouponReceiveReq();
		couponReceiveReq.setPlatformId(platformId);
		couponReceiveReq.setPersonalId(personalBaseEntity.getPersonalId());
		couponReceiveReq.setMerchantId(merchantId);
		
		couponReceiveReq.setCouponId(couponId);
		couponReceiveReq.setIncidentId(incCode);
		couponReceiveReq.setMerOrderNo(merOrderNo);
		couponReceiveReq.setOrderAmt(orderAmt);
		couponReceiveReq.setRevType("2");//定向领取
		Map<String, Object> couponReceive = codexEngineBiz.couponReceive(couponReceiveReq);
		if(ResponseCode.OK.getCode().equals(couponReceive.get("code"))){
			response.put("code",ResponseCode.OK.getCode());
			response.put("msg",ResponseCode.OK.getCode());
		}else{
			//领取失败，返回错误信息
			response.put("code", couponReceive.get("code"));
			response.put("msg", couponReceive.get("msg"));
		}
		return response;
	}
	
	/**
	 * 查询有效活动列表
	 * @return
	 */
	@RequestMapping(value = "/activity",method = RequestMethod.GET)
    @ResponseBody
    public List<MarActivity> activity(){
		MarActivity marActivity = new MarActivity();
		marActivity.setActStauts("01");//活动进行中
        return marActivityMapper.selectActivityList(marActivity);
    }
	
	/**
	 * 根据活动ID查询活动下的红包
	 * @return
	 */
	@RequestMapping(value = "/coupon", method = RequestMethod.POST)
    @ResponseBody
	public List<MarCoupon> coupon(String actId){
		logger.info("根据活动ID查询活动下的红包。。。{}",actId);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("actId", actId);
		return marCouponMapper.queryMarCouponList(param);
	}
	
	/**
	 * 根据活动ID查询可触发的事件编号
	 * @return
	 */
	@RequestMapping(value = "/incidents",method = RequestMethod.POST)
	@ResponseBody
	public List<MarIncident> incidents(String couponId){
		MarCoupon marCoupon = marCouponMapper.selectCoup(couponId);
		String incCodeSet = marCoupon.getIncCode();
		String[] incCodes = incCodeSet.split(",");
		List<String> idsList = new ArrayList<String>();
        // 存储id  
        for(String id:incCodes){
        	idsList.add(id);
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("idsList", idsList);
		return marIncidentMapper.getIncidentList(map);
	}

}
