package com.github.wxiaoqi.security.biz.modules.refund.mapper;

import com.github.wxiaoqi.security.biz.modules.refund.entity.CmsRefundOrder;

import tk.mybatis.mapper.common.Mapper;

/**
 * 场景退款表
 * @author he
 * @date 2018-08-13 16:19:30
 */
@org.apache.ibatis.annotations.Mapper
public interface CmsRefundOrderMapper extends Mapper<CmsRefundOrder> {
	
}
