package com.github.wxiaoqi.security.biz.codex.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONObject;
import com.github.wxiaoqi.security.common.util.EntityUtils;


/**
 * 
 * */
public class BaseAction {

	/**
	 * 读取字节流参数转换成bean
	 * @throws IllegalAccessException 
	 * @throws Exception 
	 *
	 * */
	public <T> T ParamStr2Bean(HttpServletRequest request,Class<T> class1) throws Exception{
		String params=String.valueOf(request.getAttribute("param"));
		Map<String, Object>map=(Map<String, Object>) JSONObject.parse(params);
		return EntityUtils.map2Bean(map, class1);
	}
	
}
