package com.github.wxiaoqi.security.biz.modules.community.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.github.wxiaoqi.security.biz.modules.comment.entity.Comment;
import com.github.wxiaoqi.security.biz.modules.comment.entity.Favort;


/**
 * 
 * 
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:44
 */
@Table(name = "cms_community_viewpoint")
public class CommunityViewpoint implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //主键
    @Id
    private String id;
	
	    //索引，用户编号
    @Column(name = "customer_id")
    private String customerId;
	
	    //发表观点，文字内容
    @Column(name = "point_text")
    private String pointText;
	
	    //发表观点，图片路径，json格式
    @Column(name = "point_img")
    private String pointImg;
	
	    //发表时间
    @Column(name = "point_time")
    private Date pointTime;
	
	    //发表地点
    @Column(name = "point_address")
    private String pointAddress;
	
	    //点赞数量
    @Column(name = "favort_count")
    private Integer favortCount;
	
	    //评论数量
    @Column(name = "comment_count")
    private Integer commentCount;
	
	    //商户号
    @Column(name = "merchant_id")
    private String merchantId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //卖朋友姓名
    @Column(name = "sale_name")
    private String saleName;
	
	    //卖朋友性别 0男 1女
    @Column(name = "sale_sex")
    private String saleSex;
	
	    //观点类型 01社区 02卖朋友
    @Column(name = "view_type")
    private String viewType;
	
    //是否匿名 01匿名
    @Column(name = "isPrivate")
    private String isPrivate;
    
    //昵称
    @Column(name = "nick_name")
    private String nickName;
	
    //头像
    @Column(name = "avatar_url")
    private String avatarUrl;
    
    //点赞列表
    @Transient
    private List<Favort> favorts;
    
    
    //评论列表
    @Transient
    private List<Comment> comments;
    
    @Transient
    private String favort;
    
    
    
	//发表状态距现在时间
    @Transient
	private String diffTime;
    
	/**
	 * 设置：主键
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public String getId() {
		return id;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 设置：发表观点，文字内容
	 */
	public void setPointText(String pointText) {
		this.pointText = pointText;
	}
	/**
	 * 获取：发表观点，文字内容
	 */
	public String getPointText() {
		return pointText;
	}
	/**
	 * 设置：发表观点，图片路径，json格式
	 */
	public void setPointImg(String pointImg) {
		this.pointImg = pointImg;
	}
	/**
	 * 获取：发表观点，图片路径，json格式
	 */
	public String getPointImg() {
		return pointImg;
	}
	/**
	 * 设置：发表时间
	 */
	public void setPointTime(Date pointTime) {
		this.pointTime = pointTime;
	}
	/**
	 * 获取：发表时间
	 */
	public Date getPointTime() {
		return pointTime;
	}
	/**
	 * 设置：发表地点
	 */
	public void setPointAddress(String pointAddress) {
		this.pointAddress = pointAddress;
	}
	/**
	 * 获取：发表地点
	 */
	public String getPointAddress() {
		return pointAddress;
	}
	/**
	 * 设置：点赞数量
	 */
	public void setFavortCount(Integer favortCount) {
		this.favortCount = favortCount;
	}
	/**
	 * 获取：点赞数量
	 */
	public Integer getFavortCount() {
		return favortCount;
	}
	/**
	 * 设置：评论数量
	 */
	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}
	/**
	 * 获取：评论数量
	 */
	public Integer getCommentCount() {
		return commentCount;
	}
	
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：卖朋友姓名
	 */
	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}
	/**
	 * 获取：卖朋友姓名
	 */
	public String getSaleName() {
		return saleName;
	}
	/**
	 * 设置：卖朋友性别 0男 1女
	 */
	public void setSaleSex(String saleSex) {
		this.saleSex = saleSex;
	}
	/**
	 * 获取：卖朋友性别 0男 1女
	 */
	public String getSaleSex() {
		return saleSex;
	}
	/**
	 * 设置：观点类型 01社区 02卖朋友
	 */
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}
	/**
	 * 获取：观点类型 01社区 02卖朋友
	 */
	public String getViewType() {
		return viewType;
	}
	public String getIsPrivate() {
		return isPrivate;
	}
	public void setIsPrivate(String isPrivate) {
		this.isPrivate = isPrivate;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	public List<Favort> getFavorts() {
		return favorts;
	}
	public void setFavorts(List<Favort> favorts) {
		this.favorts = favorts;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	public String getFavort() {
		return favort;
	}
	public void setFavort(String favort) {
		this.favort = favort;
	}
	public String getDiffTime() {
		return diffTime;
	}
	public void setDiffTime(String diffTime) {
		this.diffTime = diffTime;
	}
	@Override
	public String toString() {
		return "CommunityViewpoint [id=" + id + ", customerId=" + customerId + ", pointText=" + pointText
				+ ", pointImg=" + pointImg + ", pointTime=" + pointTime + ", pointAddress=" + pointAddress
				+ ", favortCount=" + favortCount + ", commentCount=" + commentCount + ", merchantId=" + merchantId
				+ ", platformId=" + platformId + ", saleName=" + saleName + ", saleSex=" + saleSex + ", viewType="
				+ viewType + ", isPrivate=" + isPrivate + ", nickName=" + nickName + ", avatarUrl=" + avatarUrl
				+ ", favorts=" + favorts + ", comments=" + comments + ", favort=" + favort + ", diffTime=" + diffTime
				+ "]";
	}
}
