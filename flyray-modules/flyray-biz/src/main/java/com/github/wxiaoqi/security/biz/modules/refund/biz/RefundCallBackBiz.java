package com.github.wxiaoqi.security.biz.modules.refund.biz;

import java.math.BigDecimal;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.feign.CrmFeign;
import com.github.wxiaoqi.security.biz.feign.MallFeign;
import com.github.wxiaoqi.security.biz.modules.auction.entity.AuctionOrder;
import com.github.wxiaoqi.security.biz.modules.auction.mapper.AuctionOrderMapper;
import com.github.wxiaoqi.security.biz.modules.pay.entity.CmsPayOrder;
import com.github.wxiaoqi.security.biz.modules.pay.mapper.CmsPayOrderMapper;
import com.github.wxiaoqi.security.biz.modules.refund.entity.CmsRefundOrder;
import com.github.wxiaoqi.security.biz.modules.refund.mapper.CmsRefundOrderMapper;

/**
 * 退款回调处理
 * @author he
 *
 */
@Service
public class RefundCallBackBiz {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(RefundCallBackBiz.class);

	@Autowired
	private CrmFeign crmFeign;
	@Autowired
	private MallFeign mallFeign;
	@Autowired
	private CmsPayOrderMapper cmsScenesOrderMapper;
	@Autowired
	private AuctionOrderMapper auctionOrderMapper;
	@Autowired
	private CmsRefundOrderMapper cmsRefundOrderMapper;

	/**
	 * 退款完成回调
	 * @param request
	 * @return
	 */
	public void refundCallBack(Map<String, Object> params){
		logger.info("退款完成回调开始.......{}",params);
		String platformId = (String) params.get("platformId");
		String orderNo = (String) params.get("orderNo");//退款订单号
		String payOrderNo = (String) params.get("payOrderNo");//支付订单号
		String txStatus = (String) params.get("txStatus");
		String amount = (String) params.get("amount");
		BigDecimal amountDecimal = new BigDecimal(amount);
		
		//校验订单是否存在
		CmsRefundOrder cmsRefundOrder = new CmsRefundOrder();
		cmsRefundOrder.setPlatformId(platformId);
		cmsRefundOrder.setPayOrderNo(payOrderNo);
		cmsRefundOrder.setRefundOrderNo(orderNo);
		CmsRefundOrder selectCmsRefundOrder = cmsRefundOrderMapper.selectOne(cmsRefundOrder);
		
		CmsPayOrder cmsScenesOrder = new CmsPayOrder();
		cmsScenesOrder.setPlatformId(platformId);
		cmsScenesOrder.setPayOrderNo(payOrderNo);
		CmsPayOrder selectOrder = cmsScenesOrderMapper.selectOne(cmsScenesOrder);

		if(null == selectCmsRefundOrder || null == selectOrder){
			logger.info("支付完成回调-订单不存在.......");
			return;
		}
		if(0 != selectCmsRefundOrder.getTxAmt().compareTo(amountDecimal)){
			logger.info("支付完成回调-金额不一致.......");
			return;
		}

		//退款成功出账，更新订单状态
		if("00".equals(txStatus)){
			crmFeign.outAccounting(params);
			selectCmsRefundOrder.setTxStatus("00");//退款成功
		}else{
			selectCmsRefundOrder.setTxStatus("01");//退款失败
		}
		cmsRefundOrderMapper.updateByPrimaryKey(selectCmsRefundOrder);

		//更新场景订单
		String scenesCode = selectOrder.getScenesCode();
		if("4".equals(scenesCode)){
			//竞拍
			this.updateAuctionOrder(params);
		}else if("5".equals(scenesCode)){
			//拼团参团

		}else if("3".equals(scenesCode)){
			//店内点餐

		}else if("2".equals(scenesCode)){
			//预订点餐

		}else if("1".equals(scenesCode)){
			//外卖

		}else if("6".equals(scenesCode)){
			//拼团创建团
			
		}else if ("8".equals(scenesCode)) {
			//商城
			this.updateMallOrder(params);
		}
	}
	
	/**
	 * 商城订单处理
	 * @param params
	 */
	private void updateMallOrder(Map<String, Object> params){
		String txStatus = (String) params.get("txStatus");
		if(null != txStatus){
			if("00".equals(txStatus)){
				params.put("txStatus", "03");//退款成功
			}else{
				params.put("txStatus", "05");//退款失败
			}
		}
		Map<String, Object> respMap = mallFeign.updatePayOrder(params);
		logger.info("修改商城订单状态。。。{}"+respMap);
	}
	
	/**
	 * 竞拍订单处理
	 * @param params
	 */
	private void updateAuctionOrder(Map<String, Object> params){
		String txStatus = (String) params.get("txStatus");
		AuctionOrder auctionOrder = new AuctionOrder();
		auctionOrder.setPlatformId((String) params.get("platformId"));
		auctionOrder.setMerchantId((String) params.get("merId"));
		auctionOrder.setPayOrderNo((String)params.get("payOrderNo"));
		auctionOrder.setRefundOrderNo((String)params.get("refundOrderNo"));
		auctionOrder.setStatus("00");//支付成功
		AuctionOrder queryAuctionOrder = auctionOrderMapper.selectOne(auctionOrder);
		if(null != queryAuctionOrder){
			if("00".equals(txStatus)){
				queryAuctionOrder.setStatus("02");//退款成功
			}else{
				queryAuctionOrder.setStatus("03");//退款失败
			}
		}
		auctionOrderMapper.updateByPrimaryKey(queryAuctionOrder);
	}

}
