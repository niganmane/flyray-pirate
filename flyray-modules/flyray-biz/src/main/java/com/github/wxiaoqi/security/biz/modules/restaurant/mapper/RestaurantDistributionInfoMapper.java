package com.github.wxiaoqi.security.biz.modules.restaurant.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantDistributionInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 配送信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantDistributionInfoMapper extends Mapper<RestaurantDistributionInfo> {
	
	/**
	 * 查询非默认列表
	 * @param restaurantDistributionInfo
	 * @return
	 */
	public List<RestaurantDistributionInfo> selectNotDefault(RestaurantDistributionInfo restaurantDistributionInfo);
	
	/**
	 * 分页查询配送信息列表
	 * @param map
	 * @return
	 */
	List<RestaurantDistributionInfo> queryRestaurantDistributionInfo(Map<String, Object> map);
	
}
