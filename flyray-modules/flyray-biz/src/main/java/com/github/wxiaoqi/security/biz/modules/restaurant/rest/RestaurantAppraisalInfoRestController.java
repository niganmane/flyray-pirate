package com.github.wxiaoqi.security.biz.modules.restaurant.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantAppraisalInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantAppraisalInfo;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序点餐评论信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/appraisal")
public class RestaurantAppraisalInfoRestController extends BaseController<RestaurantAppraisalInfoBiz, RestaurantAppraisalInfo> {
	
	@Autowired
	private RestaurantAppraisalInfoBiz restaurantAppraisalInfoBiz;
	
	/**
	 * 查询点餐评论信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<RestaurantAppraisalInfo> query(@RequestBody RestaurantQueryParam param) {
		log.info("查询点餐评论信息列表------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return restaurantAppraisalInfoBiz.queryRestaurantAppraisalInfoPage(param);
	}

}
