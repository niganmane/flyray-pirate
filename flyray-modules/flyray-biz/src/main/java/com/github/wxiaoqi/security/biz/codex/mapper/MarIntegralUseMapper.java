package com.github.wxiaoqi.security.biz.codex.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.codex.entity.MarActivitySceneTemp;
import com.github.wxiaoqi.security.biz.codex.entity.MarIntegralUse;
import com.github.wxiaoqi.security.biz.codex.entity.MarTotalInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 积分使用表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@org.apache.ibatis.annotations.Mapper
public interface MarIntegralUseMapper extends Mapper<MarIntegralUse> {
	/**
	 * 查询积分使用数量
	 * @author centerroot
	 * @time 创建时间:2018年2月27日上午9:53:59
	 * @return
	 */
	Integer getIntegralUseNum();
	
	/**
	 * 查询活动中积分使用数量
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:35:20
	 * @param actId
	 * @return
	 */
	Integer getActivityIntegralUseNum(String actId);
	
	/**
	 * 查询积分让利金额
	 * @author centerroot
	 * @time 创建时间:2018年2月27日上午9:54:30
	 * @return
	 */
	Double getIntegralAmount();
	
	/**
	 * 查询活动中积分让利金额
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:35:20
	 * @param actId
	 * @return
	 */
	Double getActivityIntegralAmount(String actId);
	
	/**
	 * 查询不同场景积分使用占比
	 * @author centerroot
	 * @time 创建时间:2018年2月27日上午9:55:06
	 * @return
	 */
	List<MarActivitySceneTemp> getSceneIntegralProportion();
	
	/**
	 * 查询活动中不同场景积分使用占比
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:35:20
	 * @param actId
	 * @return
	 */
	List<MarActivitySceneTemp> getActivitySceneIntegralProportion(String actId);
	

	/**
	 * 根据条件查询积分使用列表
	 * @author centerroot
	 * @time 创建时间:2018年3月19日下午5:45:42
	 * @param param
	 * @return
	 */
	List<MarIntegralUse> queryMarIntegralUseList(Map<String, Object> param);

	/**
	 * 根据条件统计积分使用记录和统计金额
	 * @author centerroot
	 * @time 创建时间:2018年3月19日下午5:46:17
	 * @param param
	 * @return
	 */
	List<MarTotalInfo> queryMarIntegralUseTotalAmt(Map<String, Object> param);
}
