package com.github.wxiaoqi.security.biz.modules.community.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.community.entity.CommunityViewpoint;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:44
 */
@org.apache.ibatis.annotations.Mapper
public interface CommunityViewpointMapper extends Mapper<CommunityViewpoint> {
	
	
	List<CommunityViewpoint> queryViewpoints(Map<String, Object> param);
}
