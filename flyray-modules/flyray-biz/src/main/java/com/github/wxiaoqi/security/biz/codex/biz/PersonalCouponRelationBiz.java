package com.github.wxiaoqi.security.biz.codex.biz;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.wxiaoqi.security.biz.codex.entity.PersonalCouponRelation;
import com.github.wxiaoqi.security.biz.codex.mapper.MarCouponMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.PersonalCouponRelationMapper;
import com.github.wxiaoqi.security.biz.feign.CrmFeign;
import com.github.wxiaoqi.security.common.admin.pay.response.BaseResponse;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.biz.request.CouponIntegralRefundReq;
import com.github.wxiaoqi.security.common.biz.request.CouponIntegralUseReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingCouponFreezeReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingCouponRefundReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingCouponUnFreezeAndPayReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingCouponUnFreezeReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingFreezeReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingIntegralDetailListRsp;
import com.github.wxiaoqi.security.common.biz.request.MarketingIntegralFreezeReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingIntegralRefundReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingIntegralUnFreezeAndPayReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingIntegralUnFreezeReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingReceiveReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingRefundReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingUnFreezeAndPayReq;
import com.github.wxiaoqi.security.common.biz.request.MarketingUnFreezeReq;
import com.github.wxiaoqi.security.common.crm.request.IntoAccountRequest;
import com.github.wxiaoqi.security.common.crm.request.PersonalBaseRequest;
import com.github.wxiaoqi.security.common.entity.PersonalAccountEntity;
import com.github.wxiaoqi.security.common.enums.CustomerTypeEnums;
import com.github.wxiaoqi.security.common.msg.AccountType;
import com.github.wxiaoqi.security.common.msg.CouponUseStatus;
import com.github.wxiaoqi.security.common.msg.EffectiveStatus;
import com.github.wxiaoqi.security.common.msg.MerchantType;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TradeType;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.SerialNoUtil;

/**
 * 
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Service
public class PersonalCouponRelationBiz extends BaseBiz<PersonalCouponRelationMapper, PersonalCouponRelation> {

	private final static Logger logger = LoggerFactory.getLogger(PersonalCouponRelationBiz.class);

	@Autowired
	private PersonalCouponRelationMapper personalCouponRelationMapper;
	@Autowired
	private MarCouponMapper couponMapper;
	@Autowired
	private CommonBiz commonBiz;
	@Autowired
	private CodexEngineBiz codexEngineBiz;
	@Autowired
	private CrmFeign crmFeign;

	/**
	 * 红包、积分领取整合方法 1、判断业务类型 2、红包领取，判断红包流水号平台编号是否存在，存在直接返成功，不存在存入一条用户红包关联信息
	 * 3、积分领取，查询用户积分账户是否存在，不存在创建账户并入账，存在直接入账，记录账务明细
	 * 
	 * @param marketingCouponReceiveReq
	 * @return
	 */
	public BaseResponse marketingReceive(MarketingReceiveReq marketingReceiveReq) {
		Map<String, Object> beanToMap = EntityUtils.beanToMap(marketingReceiveReq);
		logger.info("红包、积分领取请求参数：{}" + beanToMap);
		BaseResponse baseResponse = new BaseResponse();
		// 校验用户信息
		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("platformId", marketingReceiveReq.getPlatformId());
		reqMap.put("personalId", marketingReceiveReq.getPersonalId());
		Map<String, Object> respMap = crmFeign.queryPersonalInfo(reqMap);

		if (null == respMap || !ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			baseResponse.setCode(ResponseCode.PER_NOTEXIST.getCode());
			baseResponse.setMsg(ResponseCode.PER_NOTEXIST.getMessage());
			return baseResponse;
		}
		if ("10".equals(marketingReceiveReq.getBusinessType())) {
			// 红包
			baseResponse = couponReceive(marketingReceiveReq);
		} else if ("20".equals(marketingReceiveReq.getBusinessType())) {
			// 积分
			baseResponse = integralReceive(marketingReceiveReq);
		} else if ("30".equals(marketingReceiveReq.getBusinessType())) {
			// 红包和积分
			// 红包
			BaseResponse response = couponReceive(marketingReceiveReq);
			if (ResponseCode.OK.getCode().equals(response.getCode())) {
				// 积分
				baseResponse = integralReceive(marketingReceiveReq);
			} else {
				return response;
			}
		}
		logger.info("红包、积分领取响应参数：{}" + EntityUtils.beanToMap(baseResponse));
		return baseResponse;
	}

	/**
	 * 红包领取 红包领取，判断红包流水号平台编号是否存在，存在直接返成功，不存在存入一条用户红包关联信息
	 * 
	 * @param marketingCouponReceiveReq
	 * @return
	 */
	public BaseResponse couponReceive(MarketingReceiveReq marketingReceiveReq) {
		BaseResponse baseResponse = new BaseResponse();
		Map<String, Object> beanToMap = EntityUtils.beanToMap(marketingReceiveReq);
		// List<CouponInfo> couponList = marketingReceiveReq.getCouponList();
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> couponList = (List<Map<String, Object>>) beanToMap.get("couponList");
		// 遍历一张或多张红包
		if (couponList != null && couponList.size() > 0) {
			for (int i = 0; i < couponList.size(); i++) {
				PersonalCouponRelation personalCouponRelation = new PersonalCouponRelation();
				personalCouponRelation.setPlatformId(marketingReceiveReq.getPlatformId());
				personalCouponRelation.setPersonalId(marketingReceiveReq.getPersonalId());
				Map<String, Object> couponInfo = couponList.get(i);
				personalCouponRelation.setCouponSerialNo((String) couponInfo.get("couponSerialNo"));
				PersonalCouponRelation selectOne = personalCouponRelationMapper.selectOne(personalCouponRelation);
				if (selectOne == null) {
					personalCouponRelation.setPlatformId(marketingReceiveReq.getPlatformId());
					personalCouponRelation.setPersonalId(marketingReceiveReq.getPersonalId());
					personalCouponRelation.setMerchantId(marketingReceiveReq.getMerchantId());
					personalCouponRelation.setCouponId((String) couponInfo.get("couponId"));
					personalCouponRelation.setEffectiveStatus((String) couponInfo.get("effectiveStatus"));// 有效状态
					personalCouponRelation.setRecieveTime(new Date());
					personalCouponRelation.setUseStatus(CouponUseStatus.UNUSED.getCode());// 未使用
					personalCouponRelation.setSceneId((String) couponInfo.get("sceneId"));
					personalCouponRelationMapper.insertSelective(personalCouponRelation);
				}
			}
		}
		baseResponse.setCode(ResponseCode.OK.getCode());
		baseResponse.setMsg(ResponseCode.OK.getMessage());
		return baseResponse;
	}

	/**
	 * 积分领取 积分领取，查询用户积分账户是否存在，不存在创建账户并入账，存在直接入账，记录账务明细
	 * 
	 * @param marketingCouponReceiveReq
	 * @return
	 */
	public BaseResponse integralReceive(MarketingReceiveReq marketingReceiveReq) {
		BaseResponse baseResponse = new BaseResponse();
		BigDecimal integralAmount = new BigDecimal("0");
		if (!"".equals(marketingReceiveReq.getIntegralAmount()) && marketingReceiveReq.getIntegralAmount() != null) {
			integralAmount = new BigDecimal(marketingReceiveReq.getIntegralAmount());
		}

		// String orderNo = SerialNoUtil.createID();
		// IntoAccountRequest intoAccountRequest = new IntoAccountRequest();
		// intoAccountRequest.setAccountType(AccountType.ACC_INTEGRAL.getCode());
		// intoAccountRequest.setCustomerType(CustomerTypeEnums.per_user.getCode());
		// intoAccountRequest.setIntoAccAmt(marketingReceiveReq.getIntegralAmount());
		// intoAccountRequest.setMerchantId(marketingReceiveReq.getMerchantId());
		// intoAccountRequest.setMerchantType(MerchantType.MER_PERSONAL.getCode());
		// intoAccountRequest.setOrderNo(orderNo);
		// intoAccountRequest.setPersonalId(marketingReceiveReq.getPersonalId());
		// intoAccountRequest.setPlatformId(marketingReceiveReq.getPlatformId());
		// intoAccountRequest.setTradeType(TradeType.RECEIVE.getCode());

		String orderNo = SerialNoUtil.createID();
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("platformId", marketingReceiveReq.getPlatformId());
		reqMap.put("orderNo", orderNo);
		reqMap.put("intoAccAmt", integralAmount);
		reqMap.put("merchantId", marketingReceiveReq.getMerchantId());
		reqMap.put("merchantType", MerchantType.MER_PERSONAL.getCode());
		reqMap.put("customerType", CustomerTypeEnums.per_user.getCode());
		reqMap.put("accountType", AccountType.ACC_INTEGRAL.getCode());
		reqMap.put("personalId", marketingReceiveReq.getPersonalId());
		reqMap.put("tradeType", TradeType.RECEIVE.getCode());
		Map<String, Object> reqspMap = crmFeign.inAccounting(reqMap);
		if (null != reqspMap && ResponseCode.OK.getCode().equals(reqspMap.get("code"))) {
			baseResponse.setCode(ResponseCode.OK.getCode());
			baseResponse.setMsg(ResponseCode.OK.getMessage());
		} else {
			baseResponse.setCode(ResponseCode.INTEGRAL_RECEIVE_FAILED.getCode());
			baseResponse.setMsg(ResponseCode.INTEGRAL_RECEIVE_FAILED.getMessage());
		}
		return baseResponse;
	}

	/**
	 * 红包积分冻结整合方法 红包修改状态 积分冻结余额，记录明细和冻结流水
	 * 
	 * @param marketingFreezeReq
	 * @return
	 */
	public BaseResponse marketingFreeze(MarketingFreezeReq marketingFreezeReq) {
		BaseResponse baseResponse = new BaseResponse();
		Map<String, Object> beanToMap = EntityUtils.beanToMap(marketingFreezeReq);
		logger.info("红包积分冻结请求参数：{}" + beanToMap);
		// 校验用户信息
		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("platformId", marketingFreezeReq.getPlatformId());
		reqMap.put("personalId", marketingFreezeReq.getPersonalId());
		Map<String, Object> respMap = crmFeign.queryPersonalInfo(reqMap);

		if (null == respMap || !ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			baseResponse.setCode(ResponseCode.PER_NOTEXIST.getCode());
			baseResponse.setMsg(ResponseCode.PER_NOTEXIST.getMessage());
			return baseResponse;
		}
		if ("10".equals(marketingFreezeReq.getBusinessType())) {
			// 红包
			MarketingCouponFreezeReq marketingCouponFreezeReq = EntityUtils.map2Bean(beanToMap,
					MarketingCouponFreezeReq.class);
			baseResponse = couponFreeze(marketingCouponFreezeReq);
		} else if ("20".equals(marketingFreezeReq.getBusinessType())) {
			// 积分
			MarketingIntegralFreezeReq marketingIntegralFreezeReq = EntityUtils.map2Bean(beanToMap,
					MarketingIntegralFreezeReq.class);
			baseResponse = integralFreeze(marketingIntegralFreezeReq);
		} else if ("30".equals(marketingFreezeReq.getBusinessType())) {
			// 红包和积分
			// 红包
			MarketingCouponFreezeReq marketingCouponFreezeReq = EntityUtils.map2Bean(beanToMap,
					MarketingCouponFreezeReq.class);
			BaseResponse response = couponFreeze(marketingCouponFreezeReq);
			if (ResponseCode.OK.getCode().equals(response.getCode())) {
				// 积分
				MarketingIntegralFreezeReq marketingIntegralFreezeReq = EntityUtils.map2Bean(beanToMap,
						MarketingIntegralFreezeReq.class);
				BaseResponse integralResponse = integralFreeze(marketingIntegralFreezeReq);
				if (ResponseCode.OK.getCode().equals(integralResponse.getCode())) {
					baseResponse.setCode(ResponseCode.OK.getCode());
					baseResponse.setMsg(ResponseCode.OK.getMessage());
				} else {
					// 失败时解冻红包，返回失败
					PersonalCouponRelation PersonalCouponRelation = new PersonalCouponRelation();
					PersonalCouponRelation.setPlatformId(marketingCouponFreezeReq.getPlatformId());
					PersonalCouponRelation.setPersonalId(marketingCouponFreezeReq.getPersonalId());
					PersonalCouponRelation.setCouponSerialNo(marketingCouponFreezeReq.getCouponSerialNo());
					PersonalCouponRelation.setCouponId(marketingCouponFreezeReq.getCouponId());
					PersonalCouponRelation.setEffectiveStatus(EffectiveStatus.EFFECTIVE.getCode());// 有效
					PersonalCouponRelation selectOne = personalCouponRelationMapper.selectOne(PersonalCouponRelation);
					selectOne.setUseStatus(CouponUseStatus.UNUSED.getCode());// 未使用
					personalCouponRelationMapper.updateByPrimaryKey(selectOne);
					baseResponse.setCode(ResponseCode.FREEZE_FAILED.getCode());
					baseResponse.setMsg(ResponseCode.FREEZE_FAILED.getMessage());
				}
			} else {
				return response;
			}
		}
		logger.info("红包积分冻结响应参数：{}" + EntityUtils.beanToMap(baseResponse));
		return baseResponse;
	}

	/**
	 * 红包冻结 1、根据条件查询出红包 2、修改红包使用状态
	 * 
	 * @param marketingCouponFreezeReq
	 * @return
	 */
	public BaseResponse couponFreeze(MarketingCouponFreezeReq marketingCouponFreezeReq) {
		BaseResponse baseResponse = new BaseResponse();
		// 查询可用红包是否存在
		PersonalCouponRelation PersonalCouponRelation = new PersonalCouponRelation();
		PersonalCouponRelation.setPlatformId(marketingCouponFreezeReq.getPlatformId());
		PersonalCouponRelation.setPersonalId(marketingCouponFreezeReq.getPersonalId());
		PersonalCouponRelation.setCouponSerialNo(marketingCouponFreezeReq.getCouponSerialNo());
		PersonalCouponRelation.setCouponId(marketingCouponFreezeReq.getCouponId());
		PersonalCouponRelation.setUseStatus(CouponUseStatus.UNUSED.getCode());// 未使用
		PersonalCouponRelation.setEffectiveStatus(EffectiveStatus.EFFECTIVE.getCode());// 有效
		PersonalCouponRelation selectOne = personalCouponRelationMapper.selectOne(PersonalCouponRelation);
		if (selectOne == null) {
			baseResponse.setCode(ResponseCode.COUPON_IS_NULL.getCode());
			baseResponse.setMsg(ResponseCode.COUPON_IS_NULL.getMessage());
		} else {
			// 修改红包使用状态
			selectOne.setUseStatus(CouponUseStatus.USING.getCode());// 使用中
			personalCouponRelationMapper.updateByPrimaryKey(selectOne);
			baseResponse.setCode(ResponseCode.OK.getCode());
			baseResponse.setMsg(ResponseCode.OK.getMessage());
		}
		return baseResponse;
	}

	/**
	 * 积分冻结 1、查询用户积分账户 2、积分账户余额出账，冻结余额增加 3、写入一条账务明细 4、写入一条冻结记录
	 * 
	 * @param marketingIntegralFreezeReq
	 * @return
	 */
	public BaseResponse integralFreeze(MarketingIntegralFreezeReq marketingIntegralFreezeReq) {
		BaseResponse baseResponse = new BaseResponse();
		BigDecimal integralAmount = new BigDecimal(marketingIntegralFreezeReq.getIntegralAmount());

		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("platformId", marketingIntegralFreezeReq.getPlatformId());
		reqMap.put("personalId", marketingIntegralFreezeReq.getPersonalId());
		reqMap.put("orderNo", marketingIntegralFreezeReq.getMerOrderNo());
		reqMap.put("freezeAmt", marketingIntegralFreezeReq.getIntegralAmount());
		reqMap.put("merchantId", marketingIntegralFreezeReq.getMerchantId());
		reqMap.put("merchantType", MerchantType.MER_MERCHANT.getCode());
		reqMap.put("tradeType", marketingIntegralFreezeReq.getMerOrderNo());
		reqMap.put("customerType", CustomerTypeEnums.per_user.getCode());
		reqMap.put("accountType", AccountType.ACC_INTEGRAL.getCode());
		Map<String, Object> respMap = crmFeign.freeze(reqMap);
		if (null != respMap && ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			baseResponse.setCode(ResponseCode.OK.getCode());
			baseResponse.setMsg(ResponseCode.OK.getMessage());
		}else{
			baseResponse.setCode(ResponseCode.FREEZE_FAILED.getCode());
			baseResponse.setMsg(ResponseCode.FREEZE_FAILED.getMessage());
		}
		
		

		return baseResponse;
	}

	/**
	 * 红包积分解冻整合方法 红包修改状态 积分解冻冻结余额，记录明细和冻结流水
	 * 
	 * @param marketingUnFreezeReq
	 * @return
	 */
	public BaseResponse marketingUnFreeze(MarketingUnFreezeReq marketingUnFreezeReq) {
		BaseResponse baseResponse = new BaseResponse();
		Map<String, Object> beanToMap = EntityUtils.beanToMap(marketingUnFreezeReq);
		logger.info("红包积分解冻请求参数：{}" + beanToMap);
		// 校验用户信息
		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("platformId", marketingUnFreezeReq.getPlatformId());
		reqMap.put("personalId", marketingUnFreezeReq.getPersonalId());
		Map<String, Object> respMap = crmFeign.queryPersonalInfo(reqMap);

		if (null == respMap || !ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			baseResponse.setCode(ResponseCode.PER_NOTEXIST.getCode());
			baseResponse.setMsg(ResponseCode.PER_NOTEXIST.getMessage());
			return baseResponse;
		}
		if ("10".equals(marketingUnFreezeReq.getBusinessType())) {
			// 红包
			MarketingCouponUnFreezeReq marketingCouponUnFreezeReq = EntityUtils.map2Bean(beanToMap,MarketingCouponUnFreezeReq.class);
			baseResponse = couponUnFreeze(marketingCouponUnFreezeReq);
		} else if ("20".equals(marketingUnFreezeReq.getBusinessType())) {
			// 积分
			MarketingIntegralUnFreezeReq marketingIntegralUnFreezeReq = EntityUtils.map2Bean(beanToMap,
					MarketingIntegralUnFreezeReq.class);
			baseResponse = integralUnFreeze(marketingIntegralUnFreezeReq);
		} else if ("30".equals(marketingUnFreezeReq.getBusinessType())) {
			// 红包和积分
			// 红包
			MarketingCouponUnFreezeReq marketingCouponUnFreezeReq = EntityUtils.map2Bean(beanToMap,
					MarketingCouponUnFreezeReq.class);
			BaseResponse response = couponUnFreeze(marketingCouponUnFreezeReq);
			if (ResponseCode.OK.getCode().equals(response.getCode())) {
				// 积分
				MarketingIntegralUnFreezeReq marketingIntegralUnFreezeReq = EntityUtils.map2Bean(beanToMap,
						MarketingIntegralUnFreezeReq.class);
				BaseResponse integralResponse = integralUnFreeze(marketingIntegralUnFreezeReq);
				if (ResponseCode.OK.getCode().equals(integralResponse.getCode())) {
					baseResponse.setCode(ResponseCode.OK.getCode());
					baseResponse.setMsg(ResponseCode.OK.getMessage());
				} else {
					// 失败时冻结红包，返回失败
					PersonalCouponRelation PersonalCouponRelation = new PersonalCouponRelation();
					PersonalCouponRelation.setPlatformId(marketingCouponUnFreezeReq.getPlatformId());
					PersonalCouponRelation.setPersonalId(marketingCouponUnFreezeReq.getPersonalId());
					PersonalCouponRelation.setCouponSerialNo(marketingCouponUnFreezeReq.getCouponSerialNo());
					PersonalCouponRelation.setCouponId(marketingCouponUnFreezeReq.getCouponId());
					PersonalCouponRelation.setEffectiveStatus(EffectiveStatus.EFFECTIVE.getCode());// 有效
					PersonalCouponRelation selectOne = personalCouponRelationMapper.selectOne(PersonalCouponRelation);
					selectOne.setUseStatus(CouponUseStatus.USING.getCode());// 使用中
					personalCouponRelationMapper.updateByPrimaryKey(selectOne);
					baseResponse.setCode(ResponseCode.UNFREEZE_FAILED.getCode());
					baseResponse.setMsg(ResponseCode.UNFREEZE_FAILED.getMessage());
				}
			} else {
				return response;
			}
		}
		logger.info("红包积分解冻响应参数：{}" + EntityUtils.beanToMap(baseResponse));
		return baseResponse;
	}

	/**
	 * 红包解冻 1、根据条件查询出红包 2、修改红包使用状态
	 * 
	 * @param marketingCouponUnFreezeReq
	 * @return
	 */
	public BaseResponse couponUnFreeze(MarketingCouponUnFreezeReq marketingCouponUnFreezeReq) {
		BaseResponse baseResponse = new BaseResponse();
		// 查询可用红包是否存在
		PersonalCouponRelation PersonalCouponRelation = new PersonalCouponRelation();
		PersonalCouponRelation.setPlatformId(marketingCouponUnFreezeReq.getPlatformId());
		PersonalCouponRelation.setPersonalId(marketingCouponUnFreezeReq.getPersonalId());
		PersonalCouponRelation.setCouponSerialNo(marketingCouponUnFreezeReq.getCouponSerialNo());
		PersonalCouponRelation.setCouponId(marketingCouponUnFreezeReq.getCouponId());
		PersonalCouponRelation.setUseStatus(CouponUseStatus.USING.getCode());// 使用中
		PersonalCouponRelation.setEffectiveStatus(EffectiveStatus.EFFECTIVE.getCode());// 有效
		PersonalCouponRelation selectOne = personalCouponRelationMapper.selectOne(PersonalCouponRelation);
		if (selectOne == null) {
			baseResponse.setCode(ResponseCode.COUPON_IS_NULL.getCode());
			baseResponse.setMsg(ResponseCode.COUPON_IS_NULL.getMessage());
		} else {
			// 修改红包使用状态
			selectOne.setUseStatus(CouponUseStatus.UNUSED.getCode());// 未使用
			personalCouponRelationMapper.updateByPrimaryKey(selectOne);
			baseResponse.setCode(ResponseCode.OK.getCode());
			baseResponse.setMsg(ResponseCode.OK.getMessage());
		}
		return baseResponse;
	}

	/**
	 * 积分解冻 1、查询用户积分账户冻结金额 2、积分账户余额入账，冻结余额减少 3、写入一条账务明细 4、写入一条冻结记录
	 * 
	 * @param marketingIntegralUnFreezeReq
	 * @return
	 */
	public BaseResponse integralUnFreeze(MarketingIntegralUnFreezeReq marketingIntegralUnFreezeReq) {
		BaseResponse baseResponse = new BaseResponse();
		BigDecimal integralAmount = new BigDecimal(marketingIntegralUnFreezeReq.getIntegralAmount());
		
		
		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("platformId", marketingIntegralUnFreezeReq.getPlatformId());
		reqMap.put("personalId", marketingIntegralUnFreezeReq.getPersonalId());
		reqMap.put("orderNo", marketingIntegralUnFreezeReq.getMerOrderNo());
		reqMap.put("unfreezeAmt", marketingIntegralUnFreezeReq.getIntegralAmount());
		reqMap.put("merchantId", marketingIntegralUnFreezeReq.getMerchantId());
		reqMap.put("merchantType", MerchantType.MER_MERCHANT.getCode());
		reqMap.put("tradeType", marketingIntegralUnFreezeReq.getMerOrderNo());
		reqMap.put("customerType", CustomerTypeEnums.per_user.getCode());
		reqMap.put("accountType", AccountType.ACC_INTEGRAL.getCode());
		reqMap.put("freezeId", marketingIntegralUnFreezeReq.getFreezeNoRel());
		Map<String, Object> respMap = crmFeign.unfreeze(reqMap);
		if (null != respMap && ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			baseResponse.setCode(ResponseCode.OK.getCode());
			baseResponse.setMsg(ResponseCode.OK.getMessage());
		}else{
			baseResponse.setCode(ResponseCode.UNFREEZE_FAILED.getCode());
			baseResponse.setMsg(ResponseCode.UNFREEZE_FAILED.getMessage());
		}
		
		return baseResponse;
	}

	/**
	 * 红包积分解冻并出账整合方法 红包修改状态 积分解冻冻结余额，记录明细和冻结流水
	 * 
	 * @param marketingUnFreezeAndPayReq
	 * @return
	 */
	public BaseResponse marketingUnFreezeAndPay(MarketingUnFreezeAndPayReq marketingUnFreezeAndPayReq) {
		BaseResponse baseResponse = new BaseResponse();
		Map<String, Object> beanToMap = EntityUtils.beanToMap(marketingUnFreezeAndPayReq);
		logger.info("红包积分解冻请求参数：{}" + beanToMap);
		// 校验用户信息
		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("platformId", marketingUnFreezeAndPayReq.getPlatformId());
		reqMap.put("personalId", marketingUnFreezeAndPayReq.getPersonalId());
		Map<String, Object> respMap = crmFeign.queryPersonalInfo(reqMap);

		if (null == respMap || !ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			baseResponse.setCode(ResponseCode.PER_NOTEXIST.getCode());
			baseResponse.setMsg(ResponseCode.PER_NOTEXIST.getMessage());
			return baseResponse;
		}
		if ("10".equals(marketingUnFreezeAndPayReq.getBusinessType())) {
			// 红包
			MarketingCouponUnFreezeAndPayReq marketingCouponUnFreezeAndPayReq = EntityUtils.map2Bean(beanToMap,
					MarketingCouponUnFreezeAndPayReq.class);
			baseResponse = couponUnFreezeAndPay(marketingCouponUnFreezeAndPayReq);
		} else if ("20".equals(marketingUnFreezeAndPayReq.getBusinessType())) {
			// 积分
			MarketingIntegralUnFreezeAndPayReq marketingIntegralUnFreezeAndPayReq = EntityUtils.map2Bean(beanToMap,
					MarketingIntegralUnFreezeAndPayReq.class);
			baseResponse = integralUnFreezeAndPay(marketingIntegralUnFreezeAndPayReq);
		} else if ("30".equals(marketingUnFreezeAndPayReq.getBusinessType())) {
			// 红包和积分
			// 红包
			MarketingCouponUnFreezeAndPayReq marketingCouponUnFreezeAndPayReq = EntityUtils.map2Bean(beanToMap,
					MarketingCouponUnFreezeAndPayReq.class);
			BaseResponse response = couponUnFreezeAndPay(marketingCouponUnFreezeAndPayReq);
			if (ResponseCode.OK.getCode().equals(response.getCode())) {
				// 积分
				MarketingIntegralUnFreezeAndPayReq marketingIntegralUnFreezeAndPayReq = EntityUtils.map2Bean(beanToMap,
						MarketingIntegralUnFreezeAndPayReq.class);
				BaseResponse integralResponse = integralUnFreezeAndPay(marketingIntegralUnFreezeAndPayReq);
				if (ResponseCode.OK.getCode().equals(integralResponse.getCode())) {
					baseResponse.setCode(ResponseCode.OK.getCode());
					baseResponse.setMsg(ResponseCode.OK.getMessage());
				} else {
					// 失败时冻结红包，返回失败
					PersonalCouponRelation personalCouponRelation = new PersonalCouponRelation();
					personalCouponRelation.setPlatformId(marketingCouponUnFreezeAndPayReq.getPlatformId());
					personalCouponRelation.setPersonalId(marketingCouponUnFreezeAndPayReq.getPersonalId());
					personalCouponRelation.setCouponSerialNo(marketingCouponUnFreezeAndPayReq.getCouponSerialNo());
					personalCouponRelation.setCouponId(marketingCouponUnFreezeAndPayReq.getCouponId());
					personalCouponRelation.setEffectiveStatus(EffectiveStatus.EFFECTIVE.getCode());// 有效
					PersonalCouponRelation selectOne = personalCouponRelationMapper.selectOne(personalCouponRelation);
					selectOne.setUseStatus(CouponUseStatus.USING.getCode());// 使用中
					personalCouponRelationMapper.updateByPrimaryKey(selectOne);
					baseResponse.setCode(ResponseCode.UNFREEZE_FAILED.getCode());
					baseResponse.setMsg(ResponseCode.UNFREEZE_FAILED.getMessage());
				}
			} else {
				return response;
			}
		}
		logger.info("红包积分解冻响应参数：{}" + EntityUtils.beanToMap(baseResponse));
		if (ResponseCode.OK.getCode().equals(baseResponse.getCode())) {
			// 成功后调用营销引擎记录流水
			CouponIntegralUseReq couponIntegralUseReq = new CouponIntegralUseReq();
			couponIntegralUseReq.setPlatformId(marketingUnFreezeAndPayReq.getPlatformId());
			couponIntegralUseReq.setPersonalId(marketingUnFreezeAndPayReq.getPersonalId());
			couponIntegralUseReq.setMerchantId(marketingUnFreezeAndPayReq.getMerchantId());
			couponIntegralUseReq.setCouponId(marketingUnFreezeAndPayReq.getCouponId());
			couponIntegralUseReq.setCouponSerialNo(marketingUnFreezeAndPayReq.getCouponSerialNo());
			couponIntegralUseReq.setSceneId(marketingUnFreezeAndPayReq.getSceneId());
			couponIntegralUseReq.setIntegralAmount(marketingUnFreezeAndPayReq.getIntegralAmount());
			couponIntegralUseReq.setMerOrderNo(marketingUnFreezeAndPayReq.getMerOrderNo());
			couponIntegralUseReq.setCouponAmt(marketingUnFreezeAndPayReq.getCouponAmt());
			couponIntegralUseReq.setIntegralAmt(marketingUnFreezeAndPayReq.getIntegralAmount());
			logger.info("调用营销引擎消费请求参数：{}" + couponIntegralUseReq);
			codexEngineBiz.consume(couponIntegralUseReq);
		}
		return baseResponse;
	}

	/**
	 * 红包解冻并出账 1、根据条件查询出红包 2、修改红包使用状态
	 * 
	 * @param marketingCouponFreezeReq
	 * @return
	 */
	public BaseResponse couponUnFreezeAndPay(MarketingCouponUnFreezeAndPayReq marketingCouponUnFreezeAndPayReq) {
		BaseResponse baseResponse = new BaseResponse();
		// 查询可用红包是否存在
		PersonalCouponRelation PersonalCouponRelation = new PersonalCouponRelation();
		PersonalCouponRelation.setPlatformId(marketingCouponUnFreezeAndPayReq.getPlatformId());
		PersonalCouponRelation.setPersonalId(marketingCouponUnFreezeAndPayReq.getPersonalId());
		PersonalCouponRelation.setCouponSerialNo(marketingCouponUnFreezeAndPayReq.getCouponSerialNo());
		PersonalCouponRelation.setCouponId(marketingCouponUnFreezeAndPayReq.getCouponId());
		PersonalCouponRelation.setUseStatus(CouponUseStatus.USING.getCode());// 使用中
		PersonalCouponRelation.setEffectiveStatus(EffectiveStatus.EFFECTIVE.getCode());// 有效
		PersonalCouponRelation selectOne = personalCouponRelationMapper.selectOne(PersonalCouponRelation);
		if (selectOne == null) {
			baseResponse.setCode(ResponseCode.COUPON_IS_NULL.getCode());
			baseResponse.setMsg(ResponseCode.COUPON_IS_NULL.getMessage());
		} else {
			// 修改红包使用状态
			selectOne.setUseStatus(CouponUseStatus.USED.getCode());// 已使用
			personalCouponRelationMapper.updateByPrimaryKey(selectOne);
			baseResponse.setCode(ResponseCode.OK.getCode());
			baseResponse.setMsg(ResponseCode.OK.getMessage());
		}
		return baseResponse;
	}

	/**
	 * 积分解冻并出账 1、查询用户积分账户冻结金额 2、积分账户冻结余额减少 3、写入一条账务明细 4、写入一条冻结记录
	 * 
	 * @param marketingIntegralUnFreezeReq
	 * @return
	 */
	public BaseResponse integralUnFreezeAndPay(MarketingIntegralUnFreezeAndPayReq marketingIntegralUnFreezeAndPayReq) {
		BaseResponse baseResponse = new BaseResponse();
		BigDecimal integralAmount = new BigDecimal(marketingIntegralUnFreezeAndPayReq.getIntegralAmount());
		
		
		Map<String, Object> unFreAndOutReqMap = new HashMap<>();
		unFreAndOutReqMap.put("platformId", marketingIntegralUnFreezeAndPayReq.getPlatformId());
		unFreAndOutReqMap.put("unFreAmt", marketingIntegralUnFreezeAndPayReq.getIntegralAmount());
		unFreAndOutReqMap.put("merchantId", marketingIntegralUnFreezeAndPayReq.getMerchantId());
		unFreAndOutReqMap.put("merchantType", MerchantType.MER_MERCHANT.getCode());
		unFreAndOutReqMap.put("customerType", CustomerTypeEnums.per_user.getCode());
		unFreAndOutReqMap.put("accountType", AccountType.ACC_INTEGRAL.getCode());
		unFreAndOutReqMap.put("freezeId", marketingIntegralUnFreezeAndPayReq.getFreezeNoRel());
		unFreAndOutReqMap.put("orderNo", marketingIntegralUnFreezeAndPayReq.getMerOrderNo());
		unFreAndOutReqMap.put("personalId", marketingIntegralUnFreezeAndPayReq.getPersonalId());
		unFreAndOutReqMap.put("tradeType", TradeType.PAY.getCode());
		Map<String, Object> respMap = crmFeign.unFreAndOutAccount(unFreAndOutReqMap);
		if (null != respMap && ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			baseResponse.setCode(ResponseCode.OK.getCode());
			baseResponse.setMsg(ResponseCode.OK.getMessage());
		}else{
			baseResponse.setCode(ResponseCode.UNFREEZE_FAILED.getCode());
			baseResponse.setMsg(ResponseCode.UNFREEZE_FAILED.getMessage());
		}

		return baseResponse;
	}

	/**
	 * 红包、积分退款整合方法
	 * 
	 * @param marketingRefundReq
	 * @return
	 */
	public BaseResponse marketingRefund(MarketingRefundReq marketingRefundReq) {
		BaseResponse baseResponse = new BaseResponse();
		Map<String, Object> beanToMap = EntityUtils.beanToMap(marketingRefundReq);
		logger.info("红包、积分退款整合方法请求参数：{}" + beanToMap);
		// 校验用户信息
		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("platformId", marketingRefundReq.getPlatformId());
		reqMap.put("personalId", marketingRefundReq.getPersonalId());
		Map<String, Object> respMap = crmFeign.queryPersonalInfo(reqMap);

		if (null == respMap || !ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			baseResponse.setCode(ResponseCode.PER_NOTEXIST.getCode());
			baseResponse.setMsg(ResponseCode.PER_NOTEXIST.getMessage());
			return baseResponse;
		}
		if ("20".equals(marketingRefundReq.getRefundType())) {
			// 只退积分
			MarketingIntegralRefundReq marketingIntegralRefundReq = EntityUtils.map2Bean(beanToMap,
					MarketingIntegralRefundReq.class);
			baseResponse = integralRefund(marketingIntegralRefundReq);
		} else if ("10".equals(marketingRefundReq.getRefundType()) || "30".equals(marketingRefundReq.getRefundType())) {
			// 退红包和积分
			// 红包
			MarketingCouponRefundReq marketingCouponRefundReq = EntityUtils.map2Bean(beanToMap,
					MarketingCouponRefundReq.class);
			BaseResponse response = couponRefund(marketingCouponRefundReq);
			if (ResponseCode.OK.getCode().equals(response.getCode())) {
				// 积分
				MarketingIntegralRefundReq marketingIntegralRefundReq = EntityUtils.map2Bean(beanToMap,
						MarketingIntegralRefundReq.class);
				BaseResponse integralResponse = integralRefund(marketingIntegralRefundReq);
				if (ResponseCode.OK.getCode().equals(integralResponse.getCode())) {
					baseResponse.setCode(ResponseCode.OK.getCode());
					baseResponse.setMsg(ResponseCode.OK.getMessage());
				} else {
					// 失败时红包状态还原为已使用，返回失败
					PersonalCouponRelation PersonalCouponRelation = new PersonalCouponRelation();
					PersonalCouponRelation.setPlatformId(marketingCouponRefundReq.getPlatformId());
					PersonalCouponRelation.setPersonalId(marketingCouponRefundReq.getPersonalId());
					PersonalCouponRelation.setCouponSerialNo(marketingCouponRefundReq.getCouponSerialNo());
					PersonalCouponRelation.setCouponId(marketingCouponRefundReq.getCouponId());
					PersonalCouponRelation.setEffectiveStatus(EffectiveStatus.EFFECTIVE.getCode());// 有效
					PersonalCouponRelation selectOne = personalCouponRelationMapper.selectOne(PersonalCouponRelation);
					selectOne.setUseStatus(CouponUseStatus.USED.getCode());// 已使用
					personalCouponRelationMapper.updateByPrimaryKey(selectOne);
					baseResponse.setCode(ResponseCode.UNFREEZE_FAILED.getCode());
					baseResponse.setMsg(ResponseCode.UNFREEZE_FAILED.getMessage());
				}
			} else {
				return response;
			}
		}
		logger.info("红包、积分退款整合方法响应参数：{}" + EntityUtils.beanToMap(baseResponse));
		if (ResponseCode.OK.getCode().equals(baseResponse.getCode())) {
			// 成功后调用营销引擎记录流水
			CouponIntegralRefundReq couponIntegralRefundReq = new CouponIntegralRefundReq();
			couponIntegralRefundReq.setPlatformId(marketingRefundReq.getPlatformId());
			couponIntegralRefundReq.setMerOrderNo(marketingRefundReq.getMerOrderNo());
			codexEngineBiz.refund(couponIntegralRefundReq);
		}
		return baseResponse;
	}

	/**
	 * 红包退款 1、根据条件查询出红包 2、修改红包使用状态
	 * 
	 * @param marketingCouponRefundReq
	 * @return
	 */
	public BaseResponse couponRefund(MarketingCouponRefundReq marketingCouponRefundReq) {
		BaseResponse baseResponse = new BaseResponse();
		// 查询可用红包是否存在
		PersonalCouponRelation PersonalCouponRelation = new PersonalCouponRelation();
		PersonalCouponRelation.setPlatformId(marketingCouponRefundReq.getPlatformId());
		PersonalCouponRelation.setPersonalId(marketingCouponRefundReq.getPersonalId());
		PersonalCouponRelation.setCouponSerialNo(marketingCouponRefundReq.getCouponSerialNo());
		PersonalCouponRelation.setCouponId(marketingCouponRefundReq.getCouponId());
		// 存在冻结状态直接退款，暂时注释
		// PersonalCouponRelation.setUseStatus(CouponUseStatus.USED.getCode());//已使用
		PersonalCouponRelation.setEffectiveStatus(EffectiveStatus.EFFECTIVE.getCode());// 有效
		PersonalCouponRelation selectOne = personalCouponRelationMapper.selectOne(PersonalCouponRelation);
		if (selectOne == null) {
			baseResponse.setCode(ResponseCode.COUPON_IS_NULL.getCode());
			baseResponse.setMsg(ResponseCode.COUPON_IS_NULL.getMessage());
		} else {
			// 修改红包使用状态
			selectOne.setUseStatus(CouponUseStatus.UNUSED.getCode());// 未使用
			personalCouponRelationMapper.updateByPrimaryKey(selectOne);
			baseResponse.setCode(ResponseCode.OK.getCode());
			baseResponse.setMsg(ResponseCode.OK.getMessage());
		}
		return baseResponse;
	}

	/**
	 * 积分退款 1、查询用户积分余额 2、积分账户余额增加 3、写入一条账务明细
	 * 
	 * @param marketingIntegralRefundReq
	 * @return
	 */
	public BaseResponse integralRefund(MarketingIntegralRefundReq marketingIntegralRefundReq) {
		BaseResponse baseResponse = new BaseResponse();
		String amount = marketingIntegralRefundReq.getIntegralAmount();
		
		
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("platformId", marketingIntegralRefundReq.getPlatformId());
		reqMap.put("orderNo", marketingIntegralRefundReq.getMerOrderNo());
		reqMap.put("intoAccAmt", marketingIntegralRefundReq.getIntegralAmount());
		reqMap.put("merchantId", marketingIntegralRefundReq.getMerchantId());
		reqMap.put("merchantType", MerchantType.MER_PERSONAL.getCode());
		reqMap.put("customerType", CustomerTypeEnums.per_user.getCode());
		reqMap.put("accountType", AccountType.ACC_INTEGRAL.getCode());
		reqMap.put("personalId", marketingIntegralRefundReq.getPersonalId());
		reqMap.put("tradeType", TradeType.RECEIVE.getCode());
		Map<String, Object> reqspMap = crmFeign.inAccounting(reqMap);
		if (null != reqspMap && ResponseCode.OK.getCode().equals(reqspMap.get("code"))) {
			baseResponse.setCode(ResponseCode.OK.getCode());
			baseResponse.setMsg(ResponseCode.OK.getMessage());
		} else {
			baseResponse.setCode(ResponseCode.INTEGRAL_REFUND_SAVE_FAILED.getCode());
			baseResponse.setMsg(ResponseCode.INTEGRAL_REFUND_SAVE_FAILED.getMessage());
		}
		
		return baseResponse;
	}

	/**
	 * 积分明细列表查询
	 * 
	 * @param params
	 * @return
	 */
	public MarketingIntegralDetailListRsp integralDetail(Map<String, Object> params) {
		MarketingIntegralDetailListRsp marketingIntegralDetailListRsp = new MarketingIntegralDetailListRsp();
		Map<String, Object> reqMap = new HashMap<String, Object>();
		String platformId = (String) params.get("platformId");
		String personalId = (String) params.get("personalId");
		reqMap.put("platformId", platformId);
//		reqMap.put("merchantId", marketingIntegralRefundReq.getMerchantId());
//		reqMap.put("merchantType", MerchantType.MER_PERSONAL.getCode());
		reqMap.put("customerType", CustomerTypeEnums.per_user.getCode());
		reqMap.put("accountType", AccountType.ACC_INTEGRAL.getCode());
		reqMap.put("personalId", personalId);
		Map<String, Object> respMap = crmFeign.accountQuery(reqMap);
		if (null != respMap && ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			PersonalAccountEntity personalAccountEntity = (PersonalAccountEntity) respMap.get("accountInfo");
			if (null != personalAccountEntity) {
				marketingIntegralDetailListRsp.setBalance(personalAccountEntity.getAccountBalance().setScale(0).toString());
			} else {
				marketingIntegralDetailListRsp.setBalance("0");
			}
			Map<String, Object> reqJournalMap = new HashMap<String, Object>();
			reqMap.put("platformId", platformId);
			reqMap.put("personalId", personalId);
			reqMap.put("accountType", AccountType.ACC_INTEGRAL.getCode());
			Map<String, Object> respJournalMap = crmFeign.accountJournalQuery(reqMap);
			
			if (null != respJournalMap && ResponseCode.OK.getCode().equals(respJournalMap.get("code"))) {
				marketingIntegralDetailListRsp.setIntegralDetailList(respJournalMap.get("accountJournalInfo"));
			}
			
			marketingIntegralDetailListRsp.setCode(ResponseCode.OK.getCode());
			marketingIntegralDetailListRsp.setMsg(ResponseCode.OK.getMessage());
		} else {
			marketingIntegralDetailListRsp.setCode(ResponseCode.PER_ACC_NOTEXIST.getCode());
			marketingIntegralDetailListRsp.setMsg(ResponseCode.PER_ACC_NOTEXIST.getMessage());
		}
		return marketingIntegralDetailListRsp;
	}
}