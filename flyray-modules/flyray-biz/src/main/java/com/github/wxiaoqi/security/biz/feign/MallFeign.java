package com.github.wxiaoqi.security.biz.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 小程序商城订单调用
 * @author he
 *
 */
@FeignClient(value = "flyray-mall-api")
public interface MallFeign {
	
	/**
	 * 创建订单
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/order/create",method = RequestMethod.POST)
	public Map<String, Object> createPayOrder(@RequestBody Map<String, Object> params);
	
	/**
	 * 修改订单状态
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "feign/order/update",method = RequestMethod.POST)
	public Map<String, Object> updatePayOrder(@RequestBody Map<String, Object> params);
	
	/**
	 * 支付完成
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "feign/order/finish",method = RequestMethod.POST)
	public Map<String, Object> finishPayOrder(@RequestBody Map<String, Object> params);
	
	
	@RequestMapping(value = "feign/order/refund",method = RequestMethod.POST)
	public Map<String, Object> orderRefund(@RequestBody Map<String, Object> params);
	
}
