package com.github.wxiaoqi.security.biz.codex.biz;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.client.jwt.UserAuthUtil;
import com.github.wxiaoqi.security.auth.common.util.jwt.IJWTInfo;
import com.github.wxiaoqi.security.biz.codex.entity.MarActivity;
import com.github.wxiaoqi.security.biz.codex.entity.MarActivitySceneTemp;
import com.github.wxiaoqi.security.biz.codex.entity.MarCoupon;
import com.github.wxiaoqi.security.biz.codex.entity.MarIntegral;
import com.github.wxiaoqi.security.biz.codex.mapper.MarActivityMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarCouponMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarCouponRevMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarCouponUseMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarIntegralMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarIntegralRevMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarIntegralUseMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.codex.request.MarActivityQueryRequest;
import com.github.wxiaoqi.security.common.codex.request.MarActivityRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.SerialNoUtil;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;


/**
 * 活动基本信息表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MarActivityBiz extends BaseBiz<MarActivityMapper,MarActivity> {
	private final static Logger logger = LoggerFactory.getLogger(MarActivityBiz.class);
	@Value("${proportion.integral}")
	private double proportionIntegral = 5;
	@Autowired
	private MarCouponMapper marCouponMapper;
	@Autowired
	private MarCouponRevMapper marCouponRevMapper;
	@Autowired
	private MarCouponUseMapper marCouponUseMapper;
	@Autowired
	private MarIntegralMapper marIntegralMapper;
	@Autowired
	private MarIntegralRevMapper marIntegralRevMapper;
	@Autowired
	private MarIntegralUseMapper marIntegralUseMapper;
	@Autowired
    private UserAuthUtil userAuthUtil;
	
	/**
	 * 获取活动列表  按时间降序
	 */
	public TableResultResponse<MarActivity> queryActivityList(MarActivityQueryRequest param) {
		logger.info("获取活动列表。。。{}"+param);
		Example example = new Example(MarActivity.class);
		Criteria criteria = example.createCriteria();
		if(!StringUtils.isEmpty(param.getPlatformId())) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if(!StringUtils.isEmpty(param.getMerchantId())) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		example.setOrderByClause("CREATE_TIME desc");
		Page<MarActivity> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<MarActivity> list = mapper.selectByExample(example);
		logger.info("获取活动列表，响应参数.。。。{}"+list);
		TableResultResponse<MarActivity> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	/**
	 * 获取红包领取率
	 * @author centerroot
	 * @time 创建时间:2018年2月27日上午9:56:49
	 * @param params
	 * @return
	 */
	public Map<String, Object> getCouponRevRateData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取红包领取率数据请求参数：{}", params);
		Integer totalNum;
		Integer revNum;
		totalNum = marCouponMapper.getCouponNum();
		revNum = marCouponRevMapper.getCouponRevNum();
		if (null == totalNum) {
			totalNum = 0;
		}
		if (null == revNum) {
			revNum = 0;
		}
		respMap.put("totalNum", totalNum);
		respMap.put("revNum", revNum);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取红包领取率数据响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 获取活动中红包领取率
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:16
	 * @param params
	 * @return
	 */
	public Map<String, Object> getActivityCouponRevRateData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取活动中红包领取率数据请求参数：{}", params);
		String actId = (String) params.get("actId");
		Integer totalNum;
		Integer revNum;
		totalNum = marCouponMapper.getActivityCouponNum(actId);
		revNum = marCouponRevMapper.getActivityCouponRevNum(actId);
		if (null == totalNum) {
			totalNum = 0;
		}
		if (null == revNum) {
			revNum = 0;
		}
		respMap.put("totalNum", totalNum);
		respMap.put("revNum", revNum);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取活动中红包领取率数据响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 获取红包使用率
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:17
	 * @param params
	 * @return
	 */
	public Map<String, Object> getCouponUseRateData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取红包使用率数据请求参数：{}", params);
		Integer useNum;
		Integer revNum;
		useNum = marCouponUseMapper.getCouponUseNum();
		revNum = marCouponRevMapper.getCouponRevNum();
		if (null == useNum) {
			useNum = 0;
		}
		if (null == revNum) {
			revNum = 0;
		}
		respMap.put("useNum", useNum);
		respMap.put("revNum", revNum);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取红包使用率数据响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 获取活动中红包使用率
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:17
	 * @param params
	 * @return
	 */
	public Map<String, Object> getActivityCouponUseRateData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取活动中红包使用率数据请求参数：{}", params);
		String actId = (String) params.get("actId");
		Integer useNum;
		Integer revNum;
		useNum = marCouponUseMapper.getActivityCouponUseNum(actId);
		revNum = marCouponRevMapper.getActivityCouponRevNum(actId);
		if (null == useNum) {
			useNum = 0;
		}
		if (null == revNum) {
			revNum = 0;
		}
		respMap.put("useNum", useNum);
		respMap.put("revNum", revNum);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取活动中红包使用率数据响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 获取红包场景占比
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:19
	 * @param params
	 * @return
	 */
	public Map<String, Object> getCouponSceneRateData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取红包场景占比数据请求参数：{}", params);
		List<MarActivitySceneTemp> sceneTemp = marCouponUseMapper.getSceneCouponProportion();
		if (null != sceneTemp) {
			respMap.put("sceneCouponTemp", sceneTemp);
		}
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取红包场景占比数据响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 获取活动中红包场景占比
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:19
	 * @param params
	 * @return
	 */
	public Map<String, Object> getActivityCouponSceneRateData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取活动中红包场景占比数据请求参数：{}", params);
		String actId = (String) params.get("actId");
		List<MarActivitySceneTemp> sceneTemp = marCouponUseMapper.getActivitySceneCouponProportion(actId);
		if (null != sceneTemp) {
			respMap.put("sceneCouponTemp", sceneTemp);
		}
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取活动中红包场景占比数据响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 获取积分领取率
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:21
	 * @param params
	 * @return
	 */
	public Map<String, Object> getIntegralRevRateData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取积分领取率数据请求参数：{}", params);
		Integer totalNum;
		Integer revNum;
		totalNum = marIntegralMapper.getIntegralNum();
		revNum = marIntegralRevMapper.getIntegralRevNum();
		if (null == totalNum) {
			totalNum = 0;
		}
		if (null == revNum) {
			revNum = 0;
		}
		respMap.put("totalNum", totalNum);
		respMap.put("revNum", revNum);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取积分领取率数据响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 获取活动中积分领取率
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:21
	 * @param params
	 * @return
	 */
	public Map<String, Object> getActivityIntegralRevRateData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取活动中积分领取率数据请求参数：{}", params);
		String actId = (String) params.get("actId");
		Integer totalNum;
		Integer revNum;
		totalNum = marIntegralMapper.getActivityIntegralNum(actId);
		revNum = marIntegralRevMapper.getActivityIntegralRevNum(actId);
		if (null == totalNum) {
			totalNum = 0;
		}
		if (null == revNum) {
			revNum = 0;
		}
		respMap.put("totalNum", totalNum);
		respMap.put("revNum", revNum);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取活动中积分领取率数据响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 获取积分使用率
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:23
	 * @param params
	 * @return
	 */
	public Map<String, Object> getIntegralUseRateData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取积分使用率数据请求参数：{}", params);
		Integer useNum;
		Integer revNum;
		useNum = marIntegralUseMapper.getIntegralUseNum();
		revNum = marIntegralRevMapper.getIntegralRevNum();
		if (null == useNum) {
			useNum = 0;
		}
		if (null == revNum) {
			revNum = 0;
		}
		respMap.put("useNum", useNum);
		respMap.put("revNum", revNum);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取积分使用率数据响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 获取活动中积分使用率
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:23
	 * @param params
	 * @return
	 */
	public Map<String, Object> getActivityIntegralUseRateData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取活动中积分使用率数据请求参数：{}", params);
		String actId = (String) params.get("actId");
		Integer useNum;
		Integer revNum;
		useNum = marIntegralUseMapper.getActivityIntegralUseNum(actId);
		revNum = marIntegralRevMapper.getActivityIntegralRevNum(actId);
		if (null == useNum) {
			useNum = 0;
		}
		if (null == revNum) {
			revNum = 0;
		}
		respMap.put("useNum", useNum);
		respMap.put("revNum", revNum);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取活动中积分使用率数据响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 获取积分场景占比
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:25
	 * @param params
	 * @return
	 */
	public Map<String, Object> getIntegralSceneRateData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取积分场景占比数据请求参数：{}", params);
		List<MarActivitySceneTemp> sceneTemp = marIntegralUseMapper.getSceneIntegralProportion();
		if (null != sceneTemp) {
			respMap.put("sceneIntegralTemp", sceneTemp);
			respMap.put("total", sceneTemp.size());
		}else{
			respMap.put("total", 0);
		}
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取积分场景占比数据响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 获取活动中积分场景占比
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:25
	 * @param params
	 * @return
	 */
	public Map<String, Object> getActivityIntegralSceneRateData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取活动中积分场景占比数据请求参数：{}", params);
		String actId = (String) params.get("actId");
		List<MarActivitySceneTemp> sceneTemp = marIntegralUseMapper.getActivitySceneIntegralProportion(actId);
		if (null != sceneTemp) {
			respMap.put("sceneIntegralTemp", sceneTemp);
			respMap.put("total", sceneTemp.size());
		}else{
			respMap.put("total", 0);
		}
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取活动中积分场景占比数据响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 获取让利占比
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:26
	 * @param params
	 * @return
	 */
	public Map<String, Object> getAmountProportionData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取让利占比数据请求参数：{}", params);
		Double couponAmount;
		Double integralAmount;
		couponAmount = marCouponUseMapper.getCouponAmount();
		integralAmount = marIntegralUseMapper.getIntegralAmount();
		
		if (null == couponAmount) {
			couponAmount = 0d;
		}
		if (null == integralAmount) {
			integralAmount = 0d;
		}else{
			integralAmount = integralAmount/proportionIntegral;
		}
		
		respMap.put("couponAmount", couponAmount);
		respMap.put("integralAmount", integralAmount);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取让利占比数据响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 获取活动中让利占比
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:28:26
	 * @param params
	 * @return
	 */
	public Map<String, Object> getActivityAmountProportionData(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		logger.info("获取活动中让利占比数据请求参数：{}", params);
		String actId = (String) params.get("actId");
		Double couponAmount;
		Double integralAmount;
		couponAmount = marCouponUseMapper.getActivityCouponAmount(actId);
		integralAmount = marIntegralUseMapper.getActivityIntegralAmount(actId);
		
		if (null == couponAmount) {
			couponAmount = 0d;
		}
		if (null == integralAmount) {
			integralAmount = 0d;
		}else{
			integralAmount = integralAmount/proportionIntegral;
		}
		
		respMap.put("couponAmount", couponAmount);
		respMap.put("integralAmount", integralAmount);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getCode());
		logger.info("获取活动中让利占比数据响应参数：{}", respMap);
		return respMap;
	}

	
	/**
	 * 活动状态修改为未开始或者是活动结束时   该活动下的所有红包积分设置为不可领
	 * 
	 * @param actId
	 */
	public void setStatus(String actId) {
		logger.info("修改活动状态。。。{}"+actId);
		List<MarCoupon> couponList = marCouponMapper.queryCouponList(null, null, actId, null);
		if (couponList.size() > 0 && couponList != null) {
			for (MarCoupon record : couponList) {
				record.setCouStatus("01");
				marCouponMapper.updateByPrimaryKeySelective(record);
			}
		}
		List<MarIntegral> integralList = marIntegralMapper.getIntegralList(null, null, actId);
		if (integralList != null && integralList.size() > 0) {
			for (MarIntegral record : integralList) {
				record.setIntegralStatus("01");
				marIntegralMapper.updateByPrimaryKeySelective(record);
			}
		}
	}
	
	
	/** 
	 * 添加
	 */
	public Map<String, Object> addActivity(MarActivityRequestParam param){
		Map<String, Object> respMap = new HashMap<String, Object>();
		try {
			MarActivity config = new MarActivity();
			BeanUtils.copyProperties(param, config);
			MarActivity activity = new MarActivity();
			activity.setPlatformId(config.getPlatformId());
			activity.setMerchantId(config.getMerchantId());
			activity.setActInfo(config.getActInfo());
			config.setActId(SerialNoUtil.createID());
			try {
				IJWTInfo info =	userAuthUtil.getInfoFromToken(param.getActToken());
				config.setCreateUser(info.getXId());
			} catch (Exception e) {
				logger.info("未获取到当前用户");
				e.printStackTrace();
			}
			config.setCreateTime(new Timestamp(new Date().getTime()));
			MarActivity selectOne = mapper.selectOne(activity);
			if(null != selectOne){
				respMap.put("code", ResponseCode.FUNCTION_EXIST.getCode());
				respMap.put("msg", ResponseCode.FUNCTION_EXIST.getMessage());
				return respMap;
			}
			if (mapper.insertSelective(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		return respMap;
	}
	
	/**
	 * 删除
	 */
	public MarActivity deleteActivity(MarActivityRequestParam param){
		MarActivity config1 = null;
		try {
			MarActivity config = new MarActivity();
			BeanUtils.copyProperties(param, config);
			config.setSeqNo(Long.valueOf(param.getSeqNo()));
			config1 = mapper.selectOne(config);
			mapper.delete(config);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config1;
	}

	
	/**
	 * 修改
	 */
	public MarActivity updateActivity(MarActivityRequestParam param){
		MarActivity config1 = null;
		try {
			MarActivity config = new MarActivity();
			BeanUtils.copyProperties(param, config);
			config.setSeqNo(Long.valueOf(param.getSeqNo()));
			config1 = mapper.selectOne(config);
			mapper.updateByPrimaryKey(config);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config1;
	}

}