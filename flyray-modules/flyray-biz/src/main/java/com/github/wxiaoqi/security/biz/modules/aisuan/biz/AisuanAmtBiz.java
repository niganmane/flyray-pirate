package com.github.wxiaoqi.security.biz.modules.aisuan.biz;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanAmt;
import com.github.wxiaoqi.security.biz.modules.aisuan.mapper.AisuanAmtMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.AisuanAmtQueryOneParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanUpdateAmtParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.ResponseHelper;


/**
 * 
 *
 * @author 
 * @email ${email}
 * @date 2018-09-10 11:45:46
 */
@Service
public class AisuanAmtBiz extends BaseBiz<AisuanAmtMapper,AisuanAmt> {
	public Map<String, Object> queryByType(AisuanAmtQueryOneParam param){
		AisuanAmt amt = new AisuanAmt();
		amt.setType(param.getType());
		amt = mapper.selectOne(amt);
		return ResponseHelper.success(amt, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	public Map<String, Object> update(AisuanUpdateAmtParam param){
		AisuanAmt amt = new AisuanAmt();
		amt.setAmt(param.getAmt());
		amt.setId(param.getId());
		mapper.updateByPrimaryKeySelective(amt);
		return ResponseHelper.success(amt, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
}