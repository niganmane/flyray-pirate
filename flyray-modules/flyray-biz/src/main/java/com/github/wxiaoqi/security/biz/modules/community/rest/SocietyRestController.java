package com.github.wxiaoqi.security.biz.modules.community.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.community.biz.CommunitySocietyBiz;
import com.github.wxiaoqi.security.biz.modules.community.entity.CommunitySociety;
import com.github.wxiaoqi.security.common.cms.request.CmsQuerySocietyParam;
import com.github.wxiaoqi.security.common.cms.request.CmsSocietyRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序后台社群相关接口
 * @author Administrator
 *
 */
@Slf4j
@Controller
@RequestMapping("societies")
public class SocietyRestController extends BaseController<CommunitySocietyBiz, CommunitySociety> {

	@Autowired
	private CommunitySocietyBiz biz;
	
	/**
	 * 列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<CommunitySociety> query(@RequestBody CmsQuerySocietyParam param) {
		log.info("查询所有社群------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return biz.querySocietyInfos(param);
	}
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody CmsSocietyRequestParam param) {
		log.info("添加社群------start------{}", param);
		Map<String, Object> respMap = biz.addSociety(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("添加社群-----end-------------{}");
		return respMap;
	}
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody CmsSocietyRequestParam param) {
		log.info("修改社群------start------{}", param);
		Map<String, Object> respMap = biz.updateSociety(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("修改社群-----end-------------{}");
		return respMap;
	}
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody CmsSocietyRequestParam param) {
		log.info("删除社群------start------{}", param);
		Map<String, Object> respMap = biz.deleteSociety(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除社群------end------{}", respMap);
		return respMap;
	}
}
