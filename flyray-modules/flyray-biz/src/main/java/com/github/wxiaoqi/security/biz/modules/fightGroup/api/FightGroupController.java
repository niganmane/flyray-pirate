package com.github.wxiaoqi.security.biz.modules.fightGroup.api;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.api.BaseController;
import com.github.wxiaoqi.security.biz.modules.fightGroup.biz.FightGroupCategoryInfoBiz;
import com.github.wxiaoqi.security.biz.modules.fightGroup.biz.FightGroupGoodsInfoBiz;
import com.github.wxiaoqi.security.biz.modules.fightGroup.biz.FightGroupInfoBiz;
import com.github.wxiaoqi.security.common.cms.request.BaseParam;
import com.github.wxiaoqi.security.common.cms.request.FightGroupCreatParam;
import com.github.wxiaoqi.security.common.cms.request.FightGroupDetailParam;
import com.github.wxiaoqi.security.common.cms.request.FightGroupGoodsDetailParam;
import com.github.wxiaoqi.security.common.cms.request.FightGroupJoinParam;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 拼团相关接口
 * @author he
 *
 */
@Api(tags="拼团管理")
@Controller
@RequestMapping("fightGroup")
public class FightGroupController extends BaseController{
	
	@Autowired
	private FightGroupGoodsInfoBiz fightGroupGoodsInfoBiz;
	@Autowired
	private FightGroupCategoryInfoBiz fightGroupCategoryInfoBiz;
	@Autowired
	private FightGroupInfoBiz fightGroupInfoBiz;
	
	/**
	 * 查询拼团商品信息列表
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("查询拼团商品信息列表")
	@RequestMapping(value = "/query",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryFightGroupGoodsInfo(@RequestBody @Valid BaseParam baseParam) throws Exception {
		Map<String, Object> response = fightGroupGoodsInfoBiz.queryFightGroupGoodsInfo(EntityUtils.beanToMap(baseParam));
		return response;
    }
	
	/**
	 * 查询拼团商品信息明细
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("查询拼团商品信息明细")
	@RequestMapping(value = "/queryGoodsDetailInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryFightGroupGoodsDetailInfo(@RequestBody @Valid FightGroupGoodsDetailParam fightGroupDetailParam) throws Exception {
		Map<String, Object> response = fightGroupGoodsInfoBiz.queryFightGroupGoodsDetailInfo(EntityUtils.beanToMap(fightGroupDetailParam));
		return response;
	}
	
	/**
	 * 查询拼团详情
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("查询拼团详情")
	@RequestMapping(value = "/queryDetailInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryFightGroupDetailInfo(@RequestBody @Valid FightGroupDetailParam fightGroupDetailParam) throws Exception {
		Map<String, Object> response = fightGroupCategoryInfoBiz.queryFightGroupDetailInfo(EntityUtils.beanToMap(fightGroupDetailParam));
		return response;
	}
	
	/**
	 * 参团
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("参团")
	@RequestMapping(value = "/joinGroup",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> joinGroup(@RequestBody @Valid FightGroupJoinParam fightGroupJoinParam) throws Exception {
		Map<String, Object> response = fightGroupInfoBiz.joinGroup(EntityUtils.beanToMap(fightGroupJoinParam));
		return response;
	}
	
	/**
	 * 创建团
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("创建团")
	@RequestMapping(value = "/createGroup",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> createGroup(@RequestBody @Valid FightGroupCreatParam fightGroupCreatParam) throws Exception {
		Map<String, Object> response = fightGroupInfoBiz.createGroup(EntityUtils.beanToMap(fightGroupCreatParam));
		return response;
	}

}
