package com.github.wxiaoqi.security.biz.modules.community.biz;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.modules.community.entity.CommunitySociety;
import com.github.wxiaoqi.security.biz.modules.community.mapper.CommunitySocietyMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.CmsQuerySocietyParam;
import com.github.wxiaoqi.security.common.cms.request.CmsSocietyRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 
 *
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:43
 */
@Slf4j
@Service
public class CommunitySocietyBiz extends BaseBiz<CommunitySocietyMapper,CommunitySociety> {
	
	
	/**
	 * 查询社群
	 * @param map
	 * @return
	 */
	public Map<String, Object> querySocietyInfo(CmsQuerySocietyParam param) {
		Example example =new Example(CommunitySociety.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("merchantId", param.getMerchantId());
		criteria.andEqualTo("platformId",param.getPlatformId());
		Integer page = param.getPage();
		Integer limit = param.getLimit();
		Map<String, Object> map = new HashMap<>();
		map.put("page", page);
		map.put("limit", limit);
		Query query = new Query(map);
		Page<CommunitySociety> result = PageHelper.startPage(query.getPage(), query.getLimit());
		List<CommunitySociety> list = mapper.selectByExample(example);
		TableResultResponse<CommunitySociety> table = new TableResultResponse<>(result.getTotal(), list);
		log.info("社群列表。。。{}"+list);
		Map<String, Object> respMap = new java.util.HashMap<>();
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		respMap.put("success", true);
		respMap.put("body", table);
		return respMap;
	}
	
	
	/**
	 * 后台查询社群列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<CommunitySociety> querySocietyInfos(CmsQuerySocietyParam param) {
		log.info("查询社群列表。。。{}"+param);
		Example example =new Example(CommunitySociety.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		example.setOrderByClause("create_time desc");
		Page<CommunitySociety> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<CommunitySociety> list = mapper.selectByExample(example);
		TableResultResponse<CommunitySociety> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	
	/**
	 * 添加
	 * @param bean
	 * @return
	 */
	public Map<String, Object> addSociety(CmsSocietyRequestParam bean){
		log.info("【添加社群】。。。请求参数{}"+bean);
		CommunitySociety config = new CommunitySociety();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 config.setId(String.valueOf(SnowFlake.getId()));
			 config.setCreateTime(new Timestamp(new Date().getTime()));
			 if (mapper.insert(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加社群】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【添加社群】  报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加社群】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【添加社群】   响应参数：{}", respMap);
		 return respMap;
	}
	
	
	/**
	 * 修改
	 * @param bean
	 * @return
	 */
	public Map<String, Object> updateSociety(CmsSocietyRequestParam bean){
		log.info("【修改社群】。。。请求参数{}"+bean);
		CommunitySociety config = new CommunitySociety();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 if (mapper.updateByPrimaryKeySelective(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【修改社群】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【修改社群】  报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【修改社群】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【修改社群】   响应参数：{}", respMap);
		 return respMap;
	}
	
	
	/**
	 * 删除
	 * @param bean
	 * @return
	 */
	public Map<String, Object> deleteSociety(CmsSocietyRequestParam bean){
		log.info("【删除社群】。。。请求参数{}"+bean);
		CommunitySociety config = new CommunitySociety();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 if (mapper.delete(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除社群】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【删除社群】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除社群】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【删除社群】   响应参数：{}", respMap);
		 return respMap;
	}
	
}