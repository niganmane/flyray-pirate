package com.github.wxiaoqi.security.biz.modules.fightGroup.mapper;

import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupGoodsPicture;

import tk.mybatis.mapper.common.Mapper;

/**
 * 商品图片表
 * 
 * @author he
 * @date 2018-07-11 15:15:06
 */
@org.apache.ibatis.annotations.Mapper
public interface FightGroupGoodsPictureMapper extends Mapper<FightGroupGoodsPicture> {
	
}
