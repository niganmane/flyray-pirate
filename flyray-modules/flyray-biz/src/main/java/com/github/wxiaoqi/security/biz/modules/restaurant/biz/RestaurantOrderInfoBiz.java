package com.github.wxiaoqi.security.biz.modules.restaurant.biz;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantDistributionInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantNoConfirmOrderInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantOrderInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantReservationInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantShoppingCartInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantShoppingDishesInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantTableInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantDistributionInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantOrderInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantReservationInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantShoppingCartInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantTableInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 订单表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantOrderInfoBiz extends BaseBiz<RestaurantOrderInfoMapper,RestaurantOrderInfo> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantOrderInfoBiz.class);
	
	@Autowired
	private RestaurantOrderInfoMapper restaurantOrderInfoMapper;
	@Autowired
	private RestaurantShoppingCartInfoMapper restaurantShoppingCartInfoMapper;
	@Autowired
	private RestaurantTableInfoMapper restaurantTableInfoMapper;
	@Autowired
	private RestaurantDistributionInfoMapper restaurantDistributionInfoMapper;
	@Autowired
	private RestaurantReservationInfoMapper restaurantReservationInfoMapper;
	
	/**
	 * 查询点餐订单信息
	 * 默认查询当天最后一条订单信息及该用户购物车全部已支付的商品
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryOrderPayOrderInfo(Map<String, Object> request){
		logger.info("查询点餐订单信息请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String merchantId = (String) request.get("merchantId");
		String customerId = (String) request.get("customerId");
		RestaurantOrderInfo restaurantOrderInfo = new RestaurantOrderInfo();
		restaurantOrderInfo.setPlatformId(platformId);
		restaurantOrderInfo.setMerchantId(merchantId);
		restaurantOrderInfo.setCustomerId(customerId);
		restaurantOrderInfo.setStatus("00");//支付成功
		restaurantOrderInfo.setType("1");//店内点餐
		RestaurantOrderInfo todayLastOrderInfo = restaurantOrderInfoMapper.queryTodayLastOrderInfo(restaurantOrderInfo);
		if(null != todayLastOrderInfo){
			Map<String, Object> map = EntityUtils.beanToMap(todayLastOrderInfo);
			String tableId = todayLastOrderInfo.getTableId();
			RestaurantTableInfo restaurantTableInfo = new RestaurantTableInfo();
			restaurantTableInfo.setPlatformId(platformId);
			restaurantTableInfo.setMerchantId(merchantId);
			restaurantTableInfo.setTableId(tableId);
			RestaurantTableInfo selectRestaurantTableInfo = restaurantTableInfoMapper.selectOne(restaurantTableInfo);
			if(null != selectRestaurantTableInfo){
				String tableName = selectRestaurantTableInfo.getTableName();
				map.put("tableName", tableName);
			}
			
			//查询已评价购物车
			RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
			restaurantShoppingCartInfo.setPlatformId(platformId);
			restaurantShoppingCartInfo.setMerchantId(merchantId);
			restaurantShoppingCartInfo.setPerId(customerId);
			restaurantShoppingCartInfo.setType("1");//店内点餐
			restaurantShoppingCartInfo.setStatus("3");//已支付
			restaurantShoppingCartInfo.setIsAppraisal("0");//已评价
			restaurantShoppingCartInfo.setPayOrderNo(todayLastOrderInfo.getPayOrderNo());
			List<RestaurantShoppingDishesInfo> appraisaledInfo = restaurantShoppingCartInfoMapper.queryRestaurantDishesInfo(restaurantShoppingCartInfo);
			response.put("appraisaledInfo", appraisaledInfo);
			
			//查询未评价购物车
			restaurantShoppingCartInfo.setIsAppraisal("1");//未评价
			List<RestaurantShoppingDishesInfo> unAppraisalInfo = restaurantShoppingCartInfoMapper.queryRestaurantDishesInfo(restaurantShoppingCartInfo);
			response.put("unAppraisalInfo", unAppraisalInfo);
			response.put("restaurantOrderInfo", map);
		}else{
			response.put("restaurantOrderInfo", null);
			response.put("appraisaledInfo", null);
			response.put("unAppraisalInfo", null);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("查询点餐订单信息响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询外卖订单信息
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryTakeawayPayOrderInfo(Map<String, Object> request){
		logger.info("查询外卖订单信息请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String merchantId = (String) request.get("merchantId");
		String customerId = (String) request.get("customerId");
		
		//查询未确认订单
		RestaurantOrderInfo restaurantOrderInfo = new RestaurantOrderInfo();
		restaurantOrderInfo.setPlatformId(platformId);
		restaurantOrderInfo.setMerchantId(merchantId);
		restaurantOrderInfo.setCustomerId(customerId);
		restaurantOrderInfo.setStatus("00");//支付成功
		restaurantOrderInfo.setIsConfirm("1");//未确认
		List<RestaurantNoConfirmOrderInfo> noConfirmList = restaurantOrderInfoMapper.queryNoConfirmList(restaurantOrderInfo);
		response.put("noConfirmList", noConfirmList);
		
		//查询未评论订单
		restaurantOrderInfo.setIsConfirm("0");//已确认
		restaurantOrderInfo.setIsAppraisal("1");//未评论
		List<RestaurantNoConfirmOrderInfo> confirmedList = restaurantOrderInfoMapper.queryNoConfirmList(restaurantOrderInfo);
		response.put("confirmedList", confirmedList);
		
		//查询已完成订单
		restaurantOrderInfo.setIsAppraisal("0");//已评论
		List<RestaurantNoConfirmOrderInfo> completedList = restaurantOrderInfoMapper.queryNoConfirmList(restaurantOrderInfo);
		response.put("completedList", completedList);
		
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("查询外卖订单信息响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 订单确认收货
	 * @param request
	 * @return
	 */
	public Map<String, Object> confirmOrder(Map<String, Object> request){
		logger.info("订单确认收货请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String merchantId = (String) request.get("merchantId");
		String payOrderNo = (String) request.get("payOrderNo");
		RestaurantOrderInfo restaurantOrderInfo = new RestaurantOrderInfo();
		restaurantOrderInfo.setPlatformId(platformId);
		restaurantOrderInfo.setMerchantId(merchantId);
		restaurantOrderInfo.setPayOrderNo(payOrderNo);
		RestaurantOrderInfo selectOrderInfo = restaurantOrderInfoMapper.selectOne(restaurantOrderInfo);
		if(null != selectOrderInfo){
			selectOrderInfo.setIsConfirm("0");//已确认
			selectOrderInfo.setIsAppraisal("1");//未评论
			restaurantOrderInfoMapper.updateByPrimaryKey(selectOrderInfo);
			response.put("code", ResponseCode.OK.getCode());
			response.put("msg", ResponseCode.OK.getMessage());
		}else{
			response.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			response.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		logger.info("订单确认收货响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 外卖订单详情查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryTakeawayOrderDetailInfo(Map<String, Object> request){
		logger.info("外卖订单详情查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String merchantId = (String) request.get("merchantId");
		String payOrderNo = (String) request.get("payOrderNo");
		RestaurantOrderInfo restaurantOrderInfo = new RestaurantOrderInfo();
		restaurantOrderInfo.setPlatformId(platformId);
		restaurantOrderInfo.setMerchantId(merchantId);
		restaurantOrderInfo.setPayOrderNo(payOrderNo);
		restaurantOrderInfo.setType("3");//外卖
		RestaurantOrderInfo selectOrderInfo = restaurantOrderInfoMapper.selectOne(restaurantOrderInfo);
		if(null != selectOrderInfo){
			//配送信息查询
			String distributionId = selectOrderInfo.getDistributionId();
			RestaurantDistributionInfo restaurantDistributionInfo = new RestaurantDistributionInfo();
			restaurantDistributionInfo.setDistributionId(distributionId);
			RestaurantDistributionInfo selectButionInfo = restaurantDistributionInfoMapper.selectOne(restaurantDistributionInfo);
			if(null != selectButionInfo){
				response.put("distributionInfo", selectButionInfo);
			}
			//订单购物车菜品查询
			RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
			restaurantShoppingCartInfo.setPayOrderNo(payOrderNo);
			List<RestaurantShoppingCartInfo> shoppingCartList = restaurantShoppingCartInfoMapper.select(restaurantShoppingCartInfo);
			if(null != shoppingCartList && shoppingCartList.size() > 0){
				response.put("shoppingCartList", shoppingCartList);
			}
			response.put("selectOrderInfo", selectOrderInfo);
			response.put("code", ResponseCode.OK.getCode());
			response.put("msg", ResponseCode.OK.getMessage());
		}else{
			response.put("code", ResponseCode.ORDER_NO_EXIST.getCode());
			response.put("msg", ResponseCode.ORDER_NO_EXIST.getMessage());
		}
		logger.info("外卖订单详情查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 预订订单查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryReservationOrderInfo(Map<String, Object> request){
		logger.info("预订订单查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String merchantId = (String) request.get("merchantId");
		String customerId = (String) request.get("customerId");
		Example example = new Example(RestaurantOrderInfo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("platformId", platformId);
		criteria.andEqualTo("merchantId", merchantId);
		criteria.andEqualTo("customerId", customerId);
		criteria.andEqualTo("status", "00");//支付成功
		criteria.andEqualTo("type", "2");//预订
		example.setOrderByClause("pay_time desc");
		List<RestaurantOrderInfo> recordingList = restaurantOrderInfoMapper.selectByExample(example);
		if(recordingList != null && recordingList.size() > 0){
			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			for(RestaurantOrderInfo restaurantOrderInfo:recordingList){
				Map<String, Object> map = new HashMap<String, Object>();
				String reservationId = restaurantOrderInfo.getReservationId();
				if(!StringUtils.isEmpty(reservationId)){
					RestaurantReservationInfo restaurantReservationInfo = new RestaurantReservationInfo();
					restaurantReservationInfo.setReservationId(reservationId);
					RestaurantReservationInfo selectReservationInfo = restaurantReservationInfoMapper.selectOne(restaurantReservationInfo);
					if(null != selectReservationInfo){
						Date reservationDate = selectReservationInfo.getReservationDate();
						Date reservationTime = selectReservationInfo.getReservationTime();
						SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
						SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
						String formatDate = sdfDate.format(reservationDate);
						String formatTime = sdfTime.format(reservationTime);
						Map<String, Object> reservationMap = EntityUtils.beanToMap(selectReservationInfo);
						reservationMap.put("reservationDate", formatDate);
						reservationMap.put("reservationTime", formatTime);
						map.put("reservationInfo", reservationMap);
					}else{
						map.put("reservationInfo", null);
					}
				}else{
					map.put("reservationInfo", null);
				}
				String payOrderNo = restaurantOrderInfo.getPayOrderNo();
				RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
				restaurantShoppingCartInfo.setPayOrderNo(payOrderNo);
				List<RestaurantShoppingCartInfo> shoppingCartList = restaurantShoppingCartInfoMapper.select(restaurantShoppingCartInfo);
				if(null != shoppingCartList && shoppingCartList.size() > 0){
					map.put("shoppingCartList", shoppingCartList);
				}else{
					map.put("shoppingCartList", null);
				}
				map.put("restaurantOrderInfo", restaurantOrderInfo);
				list.add(map);
			}
			response.put("orderDetailList", list);
		}else{
			response.put("orderDetailList", null);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("预订订单查询响应。。。。。。{}", response);
		return response;
	}
	
}