package com.github.wxiaoqi.security.biz.modules.aisuan.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-10 11:45:46
 */
@Table(name = "aisuan_recommend")
public class AisuanRecommend implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //主键自增
    @Id
    private Integer id;
	
	    //外键，用户编号
    @Column(name = "user_id")
    private Long userId;
	
	    //头像
    @Column(name = "avert")
    private String avert;
	
	    //姓名
    @Column(name = "user_name")
    private String userName;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //平台编号
    @Column(name = "platform_id")
    private Long platformId;
	
	    //商户编号
    @Column(name = "merchant_id")
    private Long merchantId;
	
	    //会员编号
    @Column(name = "customer_id")
    private Long customerId;
	
    @Column(name = "sort")
    private Integer sort;
    
    //预测价格
    @Column(name = "amt")
    private String amt;
    
    @Column(name = "wx")
    private String wx;
    
    @Column(name = "phone")
    private String phone;
    
    @Column(name = "intro")
    private String intro;
    
    
    
    
    
    
	public String getWx() {
		return wx;
	}
	public void setWx(String wx) {
		this.wx = wx;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	/**
	 * 设置：主键自增
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键自增
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：外键，用户编号
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：外键，用户编号
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：头像
	 */
	public void setAvert(String avert) {
		this.avert = avert;
	}
	/**
	 * 获取：头像
	 */
	public String getAvert() {
		return avert;
	}
	/**
	 * 设置：姓名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * 获取：姓名
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public Long getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户编号
	 */
	public Long getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：会员编号
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：会员编号
	 */
	public Long getCustomerId() {
		return customerId;
	}
	
	
}
