package com.github.wxiaoqi.security.biz.modules.withdrew.biz;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.feign.CrmFeign;
import com.github.wxiaoqi.security.biz.modules.withdrew.entity.CmsWithdrewOrder;
import com.github.wxiaoqi.security.biz.modules.withdrew.mapper.CmsWithdrewOrderMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.enums.CustomerTypeEnums;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

import lombok.extern.slf4j.Slf4j;

/**
 * 提现表
 * @author he
 * @date 2018-09-09 13:37:13
 */
@Slf4j
@Service
public class CmsWithdrewOrderBiz extends BaseBiz<CmsWithdrewOrderMapper,CmsWithdrewOrder> {
	
	@Autowired
	private CmsWithdrewOrderMapper cmsWithdrewOrderMapper;
	@Autowired
	private CrmFeign crmFeign;
	
	/**
	 * 提现申请
	 * @param request
	 * @return
	 */
	public Map<String, Object> withdrewApply(@RequestBody Map<String, Object> params){
		log.info("提现申请请求.......{}",params);
		String withdrewOrderNo = String.valueOf(SnowFlake.getId());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("platformId", (String)params.get("platformId"));
		map.put("merchantId", (String)params.get("merchantId"));
		map.put("personalId", (String)params.get("personalId"));
		map.put("orderNo", withdrewOrderNo);
		map.put("freezeAmt", (String)params.get("txAmt"));
		map.put("merchantType", "CUST02");//用户
		map.put("customerType", CustomerTypeEnums.per_user.getCode());
		map.put("accountType", (String)params.get("accountType"));
		map.put("tradeType", "02");//提现
		Map<String, Object> freezeMap = crmFeign.freeze(map);
		if(ResponseCode.OK.getCode().equals(freezeMap.get("code"))){
			CmsWithdrewOrder cmsWithdrewOrder = new CmsWithdrewOrder();
			cmsWithdrewOrder.setPlatformId((String)params.get("platformId"));
			cmsWithdrewOrder.setMerchantId((String)params.get("merchantId"));
			cmsWithdrewOrder.setCustomerId((String)params.get("customerId"));
			cmsWithdrewOrder.setPersonalId((String)params.get("personalId"));
			cmsWithdrewOrder.setTxAmt(new BigDecimal((String)params.get("txAmt")));
			cmsWithdrewOrder.setWithdrewOrderNo(withdrewOrderNo);
			cmsWithdrewOrder.setCreateTime(new Date());
			cmsWithdrewOrder.setAlipayNo((String)params.get("alipayNo"));
			cmsWithdrewOrder.setTxStatus("02");//申请成功
			cmsWithdrewOrderMapper.insert(cmsWithdrewOrder);
		}
		log.info("提现申请响应.......{}",freezeMap);
		return freezeMap;
	}
}