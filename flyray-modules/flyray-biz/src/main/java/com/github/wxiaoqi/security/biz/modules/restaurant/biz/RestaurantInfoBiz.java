package com.github.wxiaoqi.security.biz.modules.restaurant.biz;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantPictureInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantPictureInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.RestaurantPicRequestParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 餐厅信息表
 *
 * @author he
 * @date 2018-06-29 10:32:56
 */
@Slf4j
@Service
public class RestaurantInfoBiz extends BaseBiz<RestaurantInfoMapper,RestaurantInfo> {
	
	
	@Autowired
	private RestaurantInfoMapper restaurantInfoMapper;
	@Autowired
	private RestaurantPictureInfoBiz restaurantPictureInfoBiz;
	@Autowired
	private RestaurantPictureInfoMapper pictureInfoMapper;
	
	
	/**
	 * 餐厅信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryRestaurantInfo(Map<String, Object> request){
		log.info("餐厅信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merchantId = (String) request.get("merchantId");
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerchantId(merchantId);
		List<RestaurantInfo> restaurantInfoList = restaurantInfoMapper.select(restaurantInfo);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for(RestaurantInfo restaurantInfos:restaurantInfoList){
			RestaurantPictureInfo restaurantPictureInfo = new RestaurantPictureInfo();
			restaurantPictureInfo.setMerchantId(restaurantInfos.getMerchantId());
			restaurantPictureInfo.setPlatformId(restaurantInfos.getPlatformId());
			List<RestaurantPictureInfo> selectList = restaurantPictureInfoBiz.selectList(restaurantPictureInfo);
			Map<String, Object> map = EntityUtils.beanToMap(restaurantInfos);
			map.put("pictureList", selectList);
			list.add(map);
		}
		response.put("info", list);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("餐厅信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询餐厅信息列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<RestaurantInfo> queryRestaurantInfoPage(RestaurantQueryParam param) {
		log.info("查询餐厅信息列表。。。。{}"+param);
		Example example = new Example(RestaurantInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if (param.getName() != null && param.getName().length() > 0) {
			criteria.andEqualTo("name",param.getName());
		}
		if (param.getPage() == null) {
			param.setPage(1);
		}
		if (param.getLimit() == null) {
			param.setLimit(9999);
		}
		Page<RestaurantInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<RestaurantInfo> list = mapper.selectByExample(example);
		
		//图片
		if (list != null && list.size() > 0) {
			for (RestaurantInfo restaurantInfo : list) {
				RestaurantPictureInfo restaurantPicture = new RestaurantPictureInfo();
				restaurantPicture.setPlatformId(restaurantInfo.getPlatformId());
				restaurantPicture.setMerchantId(restaurantInfo.getMerchantId());
				restaurantPicture.setRestaurantId(restaurantInfo.getRestaurantId());
				List<RestaurantPictureInfo> pictures = pictureInfoMapper.select(restaurantPicture);
				restaurantInfo.setPictureList(pictures);
			}
		}
		TableResultResponse<RestaurantInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	/**
	 * 删除餐厅
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteRestaurantInfo(RestaurantRequestParam param) {
		log.info("删除餐厅。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		RestaurantInfo info = new RestaurantInfo();
		try {
			BeanUtils.copyProperties(info, param);
			info.setId(Long.valueOf(param.getId()));
			if (mapper.delete(info) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除餐厅】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【删除餐厅】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除餐厅】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【删除餐厅】   响应参数：{}", respMap);
		 return respMap;
	}
	
	/**
	 * 添加餐厅
	 * @param param
	 * @return
	 */
	public Map<String, Object> addRestaurantInfo(RestaurantRequestParam param) {
		log.info("添加餐厅。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		RestaurantInfo info = new RestaurantInfo();
		try {
			BeanUtils.copyProperties(info, param);
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			info.setOpenBtime(new Time(sdf.parse(param.getOpenBtime()).getTime()));
			info.setOpenEtime(new Time(sdf.parse(param.getOpenBtime()).getTime()));
			if (mapper.insert(info) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加餐厅】   响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【添加餐厅】  报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加餐厅】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【添加餐厅】   响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 修改餐厅
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateRestaurantInfo(RestaurantRequestParam param) {
		log.info("修改餐厅。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		RestaurantInfo info = new RestaurantInfo();
		try {
			BeanUtils.copyProperties(info, param);
			info.setId(Long.valueOf(param.getId()));
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			info.setOpenBtime(new Time(sdf.parse(param.getOpenBtime()).getTime()));
			info.setOpenEtime(new Time(sdf.parse(param.getOpenBtime()).getTime()));
			if (mapper.updateByPrimaryKeySelective(info) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【修改餐厅】   响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【修改餐厅】  报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【修改餐厅】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【修改餐厅】   响应参数：{}", respMap);
		return respMap;
	}
	
	
	/**
	 * 添加餐厅图片
	 * @param param
	 * @return
	 */
	public Map<String, Object> addPic(RestaurantPicRequestParam param) {
		log.info("添加餐厅图片。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		RestaurantPictureInfo picInfo = new RestaurantPictureInfo();
		try {
			BeanUtils.copyProperties(picInfo, param);
			if (pictureInfoMapper.insert(picInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加餐厅图片】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【添加餐厅图片】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加商品图片】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【添加餐厅图片   响应参数：{}", respMap);
		 return respMap;
	}
	
	/**
	 * 删除商品图片
	 * @param param
	 * @return
	 */
	public Map<String, Object> deletePic(RestaurantPicRequestParam param) {
		log.info("删除餐厅图片。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		RestaurantPictureInfo picInfo = new RestaurantPictureInfo();
		try {
			BeanUtils.copyProperties(picInfo, param);
			picInfo.setId(Long.valueOf(param.getId()));
			if (pictureInfoMapper.delete(picInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除餐厅图片】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【删除餐厅图片】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除餐厅图片】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【删除餐厅图片   响应参数：{}", respMap);
		 return respMap;
	}

	
}