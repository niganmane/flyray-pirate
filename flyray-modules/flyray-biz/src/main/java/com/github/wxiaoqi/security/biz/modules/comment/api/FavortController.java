package com.github.wxiaoqi.security.biz.modules.comment.api;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.comment.biz.FavortBiz;
import com.github.wxiaoqi.security.common.cms.request.CmsSaveFavortParam;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags="点赞")
@Controller
@RequestMapping("favorts")
public class FavortController {

	@Autowired
	private FavortBiz favortBiz;
	
	
	/**
	 * 点赞或者是取消赞
	 */
	@ResponseBody
	@RequestMapping(value="/doFavort", method = RequestMethod.POST)
	public Map<String, Object> doFavort(@RequestBody CmsSaveFavortParam param) {
		log.info("请求观点---start---{}",param);
		Map<String, Object> respMap = favortBiz.doFavort(param);
		return respMap;
	}
	
}
