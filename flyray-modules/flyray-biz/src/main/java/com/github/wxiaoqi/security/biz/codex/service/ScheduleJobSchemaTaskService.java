
package com.github.wxiaoqi.security.biz.codex.service;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.codex.entity.ScheduleJob;

/**
 * 定时任务
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.2.0 2016-11-28
 */
public interface ScheduleJobSchemaTaskService {

	/**
	 * 获取列表
	 * @param params
	 * @return
	 */
	List<ScheduleJob> queryJobList(Map<String, Object> params);

	/**
	 * 添加定时任务
	 */
	void addJob(ScheduleJob scheduleJob);
	
	/**
	 * 更新定时任务
	 */
	void updateJob(ScheduleJob scheduleJob);
	
	/**
	 * 删除定时任务
	 */
	void deleteBatch(Long jobId);
	
	/**
	 * 立即执行
	 */
	void runJob(Long jobId);
	
	/**
	 * 暂停运行
	 */
	void pauseJob(Long jobId);
	
	/**
	 * 恢复运行
	 */
	void resumeJob(Long jobId);
}
