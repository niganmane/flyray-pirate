package com.github.wxiaoqi.security.biz.codex.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.MarRandomCouponLevel;
import com.github.wxiaoqi.security.biz.codex.mapper.MarRandomCouponLevelMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 随机红包等级
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Service
public class MarRandomCouponLevelBiz extends BaseBiz<MarRandomCouponLevelMapper,MarRandomCouponLevel> {
}