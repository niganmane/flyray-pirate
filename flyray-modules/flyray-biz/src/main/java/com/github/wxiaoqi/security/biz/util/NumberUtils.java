package com.github.wxiaoqi.security.biz.util;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 数字工具类
 * 
 * @author ZXW
 * @time 2017/06/01
 */
public class NumberUtils {
	private static final Long digit = 10000000000000L;
	private static final String date_format = "yyyyMMddHHmmssSSS";

	private static ThreadLocal<DateFormat> threadLocal = new ThreadLocal<DateFormat>();

	public static DateFormat getDateFormat() {
		DateFormat df = threadLocal.get();
		if (df == null) {
			df = new SimpleDateFormat(date_format);
			threadLocal.set(df);
		}
		return df;
	}

	/**
	 * 生成请求流水号
	 * 
	 * @return 请求流失号
	 */
	public static String generateSerialNumber() {
		String dataStr = getDateFormat().format(new Date());
		Long str = Math.round(Math.random() * digit);
		String result = String.format("%1$013d", str);
		return dataStr + result;
	}
	/**
	 * 生成红包积分流水号
	 * 
	 * */
	public static String coupiInegSerialNumber(){
		return getDateFormat().format(new Date());
	}

//	public static void main(String[] args) {
//		
//		System.out.println(generateSerialNumber());
//		System.out.println(generateSerialNumber().length());
//	}
}
