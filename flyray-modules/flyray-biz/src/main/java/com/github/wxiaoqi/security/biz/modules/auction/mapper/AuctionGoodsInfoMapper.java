package com.github.wxiaoqi.security.biz.modules.auction.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.auction.entity.AuctionGoodsInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 竞拍商品信息表
 * 
 * @author he
 * @date 2018-07-16 11:38:52
 */
@org.apache.ibatis.annotations.Mapper
public interface AuctionGoodsInfoMapper extends Mapper<AuctionGoodsInfo> {
	
	List<AuctionGoodsInfo> queryAuctionGoods(Map<String, Object> map);
	
}
