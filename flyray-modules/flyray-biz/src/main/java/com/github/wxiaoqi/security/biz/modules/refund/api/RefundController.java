package com.github.wxiaoqi.security.biz.modules.refund.api;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.api.BaseController;
import com.github.wxiaoqi.security.biz.modules.refund.biz.RefundBiz;
import com.github.wxiaoqi.security.common.pay.request.RefundParam;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 退款相关接口
 * @author he
 *
 */
@Api(tags="退款管理")
@Controller
@RequestMapping("refund")
public class RefundController extends BaseController {
	
	@Autowired
	private RefundBiz refundBiz;
	
	/**
	 * 创建订单并支付
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("申请退款")
	@RequestMapping(value = "/refundApply",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> createAndPay(@RequestBody @Valid RefundParam refundParam) throws Exception {
		String platformId = refundParam.getPlatformId();
		String customerId = refundParam.getCustomerId();
		String merchantId = refundParam.getMerchantId();
		String payOrderNo = refundParam.getPayOrderNo();
		String refundAmt = refundParam.getRefundAmt();
		String scenesCode = refundParam.getScenesCode();
		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("platformId", platformId);
		reqMap.put("customerId", customerId);
		reqMap.put("merchantId", merchantId);
		reqMap.put("payOrderNo", payOrderNo);
		reqMap.put("refundAmt", refundAmt);
		reqMap.put("scenesCode", scenesCode);
		Map<String, Object> response = refundBiz.applyAndRefund(reqMap);
		return response;
    }
	
	

}
