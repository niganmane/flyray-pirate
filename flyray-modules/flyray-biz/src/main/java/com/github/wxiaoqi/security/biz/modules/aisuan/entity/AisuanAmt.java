package com.github.wxiaoqi.security.biz.modules.aisuan.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "aisuan_amt")
public class AisuanAmt implements Serializable {
	
	@Id
	private Integer id;
	
	@Column(name = "type")
    private Integer type;
	
	@Column(name = "amt")
	private String amt;
	
	@Column(name = "name")
	private String name;

	@Column(name = "intro")
	private String intro;
	
	@Column(name = "remark")
	private String remark;
	
	
	
	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}
	
	
}
