package com.github.wxiaoqi.security.biz.modules.aisuan.biz;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanAdvice;
import com.github.wxiaoqi.security.biz.modules.aisuan.mapper.AisuanAdviceMapper;
import com.github.wxiaoqi.security.biz.modules.auction.entity.AuctionGoodsInfo;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.AisuanAdviceAddparam;
import com.github.wxiaoqi.security.common.cms.request.AisuanAdviceQueryAdminParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordReplyParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.FlyrayBeanUtils;
import com.github.wxiaoqi.security.common.util.SnowFlake;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 
 *
 * @author 
 * @email ${email}
 * @date 2018-09-10 11:45:46
 */
@Service
@Slf4j
public class AisuanAdviceBiz extends BaseBiz<AisuanAdviceMapper,AisuanAdvice> {
	
	public void save(AisuanAdviceAddparam param) throws Exception {
		AisuanAdvice advice = new AisuanAdvice();
		BeanUtils.copyProperties(advice, param);
		advice.setId(SnowFlake.getId());
		advice.setCreateTime(new Date());
		mapper.insert(advice);
	}
	public List<AisuanAdvice> query(AisuanAdviceQueryAdminParam param) {

		log.info("分页查询爱算建议。。。。{}",param);
		Example example = new Example(AuctionGoodsInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if (param.getAdviceId() != null) {
			criteria.andEqualTo("id",param.getAdviceId());
		}
		if(param.getCustomerId() != null){
			criteria.andEqualTo("customerId",param.getCustomerId());
		}
		example.setOrderByClause("create_time desc");
		//Page<AisuanAdvice> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<AisuanAdvice> list = mapper.selectByExample(example);
		//TableResultResponse<AisuanAdvice> table = new TableResultResponse<>(result.getTotal(), list);
		return list;
	
	}
	public TableResultResponse<AisuanAdvice> queryListPage(AisuanAdviceQueryAdminParam param) {
		try {
			if(param.getUserType() == 1){
			//系统管理员 --- 查询全部的去掉platformId
			param.setPlatformId(null);
			param.setMerchantId(null);
			}
			Page<AisuanAdvice> result = PageHelper.startPage(param.getPage(), param.getLimit());
			List<AisuanAdvice> list = mapper.queryList(FlyrayBeanUtils.objectToMap(param));
			TableResultResponse<AisuanAdvice> table = new TableResultResponse<>(result.getTotal(), list);
			return table;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public void reply(AisuanRecordReplyParam param) throws Exception {
		AisuanAdvice advice = new AisuanAdvice();
		advice.setUpdateTime(new Date());
		advice.setApplyUserId(param.getApplyUserId());
		advice.setId(param.getId());
		mapper.updateByPrimaryKeySelective(advice);
	}
}