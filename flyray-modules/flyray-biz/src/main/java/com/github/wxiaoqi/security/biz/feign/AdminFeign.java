package com.github.wxiaoqi.security.biz.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "flyray-admin")
public interface AdminFeign {
	@RequestMapping(value = "feign/user/query",method = RequestMethod.POST)
    public Map<String, Object> query(@RequestBody Map<String, Object> param);
}
