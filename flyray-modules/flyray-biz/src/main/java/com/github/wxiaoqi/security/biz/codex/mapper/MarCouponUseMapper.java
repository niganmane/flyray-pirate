package com.github.wxiaoqi.security.biz.codex.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.codex.entity.MarActivitySceneTemp;
import com.github.wxiaoqi.security.biz.codex.entity.MarCouponUse;
import com.github.wxiaoqi.security.biz.codex.entity.MarTotalInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 红包使用表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@org.apache.ibatis.annotations.Mapper
public interface MarCouponUseMapper extends Mapper<MarCouponUse> {
	/**
	 * 查询红包使用个数
	 * @author centerroot
	 * @time 创建时间:2018年2月27日上午9:51:27
	 * @param actId
	 * @return
	 */
	Integer getCouponUseNum();
	
	/**
	 * 查询活动中红包使用个数
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:35:20
	 * @param actId
	 * @return
	 */
	Integer getActivityCouponUseNum(String actId);
	
	/**
	 * 查询红包让利金额
	 * @author centerroot
	 * @time 创建时间:2018年2月27日上午9:51:56
	 * @param actId
	 * @return
	 */
	Double getCouponAmount();
	
	/**
	 * 查询活动中红包让利金额
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:35:20
	 * @param actId
	 * @return
	 */
	Double getActivityCouponAmount(String actId);
	
	/**
	 * 查询不同场景红包使用占比
	 * @author centerroot
	 * @time 创建时间:2018年2月27日上午9:52:25
	 * @param actId
	 * @return
	 */
	List<MarActivitySceneTemp> getSceneCouponProportion();
	
	/**
	 * 查询活动中不同场景红包使用占比
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:35:20
	 * @param actId
	 * @return
	 */
	List<MarActivitySceneTemp> getActivitySceneCouponProportion(String actId);
	
	
	/**
	 * 根据条件查询红包使用列表
	 * @author centerroot
	 * @time 创建时间:2018年3月19日下午5:45:42
	 * @param param
	 * @return
	 */
	List<MarCouponUse> queryMarCouponUseList(Map<String, Object> param);

	/**
	 * 根据条件统计红包使用记录数和统计金额
	 * @author centerroot
	 * @time 创建时间:2018年3月19日下午5:46:17
	 * @param param
	 * @return
	 */
	List<MarTotalInfo> queryMarCouponUseTotalAmt(Map<String, Object> param);
}
