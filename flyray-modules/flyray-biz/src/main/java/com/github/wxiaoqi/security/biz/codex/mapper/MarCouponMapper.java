package com.github.wxiaoqi.security.biz.codex.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.github.wxiaoqi.security.biz.codex.entity.MarCoupon;

import tk.mybatis.mapper.common.Mapper;

/**
 * 红包基本信息表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
public interface MarCouponMapper extends Mapper<MarCoupon> {
	/**
	 * 查询红包总数量
	 * @author centerroot
	 * @time 创建时间:2018年2月27日上午9:50:01
	 * @param actId
	 * @return
	 */
	Integer getCouponNum();
	/**
	 * 查询活动中红包总数量
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:35:20
	 * @param actId
	 * @return
	 */
	Integer getActivityCouponNum(String actId);
	
	/**
	 *	已使用 
	 */
	List<MarCoupon> queryCouponUsed(@Param(value="merCustNo") String merCustNo, @Param(value="acceptBizNo") String acceptBizNo);
	/**
	 * 待使用
	 */
	List<MarCoupon> queryCouponNoUsed(@Param(value="merCustNo") String merCustNo, @Param(value="acceptBizNo") String acceptBizNo);
	/**
	 * 已过期
	 */
	List<MarCoupon> queryCouponOverDate(@Param(value="merCustNo") String merCustNo, @Param(value="acceptBizNo") String acceptBizNo);
	
	/**
	 * 使用中
	 */
	
	List<MarCoupon> queryCouponOnUse(@Param(value="merCustNo") String merCustNo, @Param(value="acceptBizNo") String acceptBizNo);
	/**
	 * 
	 * @param revStart
	 * @param revEnd
	 * @param actId   活动编号
	 * @param couType 红包类型
	 * @return
	 */
	List<MarCoupon> queryCouponList(@Param("revStart") String revStart,@Param("revEnd") String revEnd,@Param("actId") String actId,@Param("couType") String couType);

	/**
	 * 状态置为可领取
	 */
	void changStatusToOpen();
	/**
	 * 状态置为不可领取
	 */
	void changStatusToClose();
	

	List<MarCoupon> queryMarCouponList(Map<String, Object> param);
	
	/***
	 * 查询普通红包和随机红包的优惠信息
	 * @param couId 红包id couponSerialNo 红包流水号
	 * */
	MarCoupon getCouponPreferential(Map<String, Object> param);
	
	/**
	 * 根据事件id查询可领取的红包 
	 * */
	List<MarCoupon>getCouponsByincCode(@Param("incCode") String incCode);
	/**
	 * 获取单个红包
	 * */
	MarCoupon selectCoup(@Param("couId") String couId);
	/**
	 * 在该场景下可使用的可领取红包
	 */
	List<MarCoupon> selectCouponRev(@Param(value="actId")String actId, @Param(value="sceneID") String sceneID);

	
	/**
	 * 根据订单号查询订单是否存在 
	 * 
	 * */
	List<String> checkMerCustNo(@Param(value="merOrderNo")String merOrderNo); 
	
	/***
	 * 根据订单号 获取订单实际支付金额 
	 * */
	Map<String,Object> getAmountByOrderNo(@Param(value="merOrderNo")String merOrderNo);
}
