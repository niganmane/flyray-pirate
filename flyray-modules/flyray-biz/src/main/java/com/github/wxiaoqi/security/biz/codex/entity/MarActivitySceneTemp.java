package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;

/**
 * 各活动的不同场景红包积分使用占比
 * 
 * @author centerroot
 * @time 创建时间:2018年2月24日下午3:37:06
 * @description
 */
public class MarActivitySceneTemp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8643278490272459160L;

	/**
	 * 场景编号
	 */
	private String scene;

	/**
	 * 数量
	 */
	private String total;

	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

}