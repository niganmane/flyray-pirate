package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 活动基本信息表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Table(name = "mar_activity")
public class MarActivity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Long seqNo;
    
    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
    
    //商户号
    @Column(name = "MERCHANT_ID")
    private String merchantId;
	
	    //活动编号
    @Column(name = "ACT_ID")
    private String actId;
	
	    //活动详情
    @Column(name = "ACT_INFO")
    private String actInfo;
	
	    //创建时间
    @Column(name = "CREATE_TIME")
    private Date createTime;
	
	    //创建人
    @Column(name = "CREATE_USER")
    private String createUser;
	
	    //活动状态 00活动未开始01活动进行中02活动结束
    @Column(name = "ACT_STAUTS")
    private String actStauts;
	
	    //备注
    @Column(name = "ACT_REMARK")
    private String actRemark;
	
    
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：
	 */
	public Long getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：活动编号
	 */
	public void setActId(String actId) {
		this.actId = actId;
	}
	/**
	 * 获取：活动编号
	 */
	public String getActId() {
		return actId;
	}
	/**
	 * 设置：活动详情
	 */
	public void setActInfo(String actInfo) {
		this.actInfo = actInfo;
	}
	/**
	 * 获取：活动详情
	 */
	public String getActInfo() {
		return actInfo;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreateUser() {
		return createUser;
	}
	/**
	 * 设置：活动状态 00活动未开始01活动进行中02活动结束
	 */
	public void setActStauts(String actStauts) {
		this.actStauts = actStauts;
	}
	/**
	 * 获取：活动状态 00活动未开始01活动进行中02活动结束
	 */
	public String getActStauts() {
		return actStauts;
	}
	/**
	 * 设置：备注
	 */
	public void setActRemark(String actRemark) {
		this.actRemark = actRemark;
	}
	/**
	 * 获取：备注
	 */
	public String getActRemark() {
		return actRemark;
	}
}
