package com.github.wxiaoqi.security.biz.modules.restaurant.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 餐厅信息表
 * 
 * @author he
 * @date 2018-06-29 10:32:56
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantInfoMapper extends Mapper<RestaurantInfo> {
	
	List<RestaurantInfo> queryRestaurantInfo(Map<String, Object> map);
	
}
