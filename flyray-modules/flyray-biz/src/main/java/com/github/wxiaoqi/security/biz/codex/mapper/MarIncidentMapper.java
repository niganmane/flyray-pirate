package com.github.wxiaoqi.security.biz.codex.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.github.wxiaoqi.security.biz.codex.entity.MarIncident;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@org.apache.ibatis.annotations.Mapper
public interface MarIncidentMapper extends Mapper<MarIncident> {
	/**
	 * 根据事件编号查询事件详情
	 * */
	public MarIncident getMarIncidentByincCode(@Param(value="incCode") String incCode);
	
	/**
	 * 根据多个事件编号查询列表
	 * */
	public List<MarIncident> getIncidentList(Map<String, Object> map);
}
