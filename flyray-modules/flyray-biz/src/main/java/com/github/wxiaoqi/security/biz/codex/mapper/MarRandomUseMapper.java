package com.github.wxiaoqi.security.biz.codex.mapper;

import com.github.wxiaoqi.security.biz.codex.entity.MarRandomUse;

import tk.mybatis.mapper.common.Mapper;

/**
 * 随机立减使用记录表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@org.apache.ibatis.annotations.Mapper
public interface MarRandomUseMapper extends Mapper<MarRandomUse> {
	
}
