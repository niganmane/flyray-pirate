package com.github.wxiaoqi.security.biz.modules.seckill.api;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.api.BaseController;
import com.github.wxiaoqi.security.biz.modules.seckill.biz.SeckillGoodsInfoBiz;
import com.github.wxiaoqi.security.biz.modules.seckill.entity.SeckillGoodsInfo;
import com.github.wxiaoqi.security.common.cms.request.AuctionGoodsDetailParam;
import com.github.wxiaoqi.security.common.cms.request.BaseParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="秒杀管理")
@Controller
@RequestMapping("seckill")
public class SeckillController extends BaseController{

	@Autowired
	private SeckillGoodsInfoBiz seckillGoodsInfoBiz;

	/**
	 * 秒杀商品信息查询
	 * @returns
	 * @throws Exception 
	 */ 
	@ApiOperation("秒杀商品信息查询")
	@RequestMapping(value = "/queryGoodsInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryGoodsInfo(@RequestBody @Valid BaseParam baseParam) throws Exception {
		List<SeckillGoodsInfo> goodsInfo = seckillGoodsInfoBiz.queryGoodsInfo(EntityUtils.beanToMap(baseParam));
		return ResponseHelper.success(goodsInfo, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
	/**
	 * 秒杀商品详情查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("秒杀商品详情查询")
	@RequestMapping(value = "/queryGoodsdetailInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryGoodsdetailInfo(@RequestBody @Valid AuctionGoodsDetailParam auctionGoodsDetailParam) throws Exception {
		SeckillGoodsInfo goodsInfo = seckillGoodsInfoBiz.queryGoodsdetailInfo(EntityUtils.beanToMap(auctionGoodsDetailParam));
		return ResponseHelper.success(goodsInfo, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}

}
