package com.github.wxiaoqi.security.biz.codex.mapper;

import org.apache.ibatis.annotations.Param;

import com.github.wxiaoqi.security.biz.codex.entity.MarIntegralRev;

import tk.mybatis.mapper.common.Mapper;

/**
 * 积分领取表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@org.apache.ibatis.annotations.Mapper
public interface MarIntegralRevMapper extends Mapper<MarIntegralRev> {
	/**
	 * 根据积分id统计积分领取数量
	 * */
	int queryCountIntegRev(@Param("integralId") String integralId);
	/***
	 * 统计指定用户指定积分当天的领取数量
	 * @param beginTime 指定日期 ;merCusrNo 商户会员号;integralId 积分id
	 * */
	int countIntegRevByday(@Param("dateTime") String dateTime,@Param("merCusrNo") String merCusrNo,@Param("integralId") String integralId);
	
	/**
	 * 统计指定用户指定积分领取数量 每周或者每月
	 * @param beginTime 开始时间; endTime 结束时间;merCusrNo 商户会员号;integralId 积分id
	 * */
	int countIntegRevBydate(@Param("beginTime") String beginTime,@Param("endTime") String endTime,@Param("merCusrNo") String merCusrNo,@Param("integralId") String integralId);
	
	/**
	 * 查询积分领取数量
	 * @author centerroot
	 * @time 创建时间:2018年2月27日上午9:53:33
	 * @return
	 */
	Integer getIntegralRevNum();
	
	/**
	 * 查询活动中积分领取数量
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:35:20
	 * @param actId
	 * @return
	 */
	Integer getActivityIntegralRevNum(String actId);
	
	/**
	 * 根据支付订单号查询是否领取过积分
	 * @param merCustNo
	 * @param sceneId
	 * @return
	 */
	Integer getIntegralRevByOrderNoAndIncCode(@Param(value="merOrderNo") String merOrderNo, @Param(value="incCode") String incCode);
}
