package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-27 10:47:34
 */
@Table(name = "personal_coupon_relation")
public class PersonalCouponRelation implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //个人客户编号
    @Column(name = "personal_id")
    private String personalId;
	
	    //商户客户编号
    @Column(name = "merchant_id")
    private String merchantId;
	
	    //红包流水号
    @Column(name = "coupon_serial_no")
    private String couponSerialNo;
	
	    //红包ID
    @Column(name = "coupon_id")
    private String couponId;
	
	    //使用状态 00：未使用，01：使用中，02：已使用
    @Column(name = "use_status")
    private String useStatus;
	
	    //领取时间
    @Column(name = "recieve_time")
    private Date recieveTime;
	
	    //有效状态 00：有效 01：失效
    @Column(name = "effective_status")
    private String effectiveStatus;
	
	    //红包使用场景ID
    @Column(name = "scene_id")
    private String sceneId;
	

	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：个人客户编号
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 获取：个人客户编号
	 */
	public String getPersonalId() {
		return personalId;
	}
	/**
	 * 设置：商户客户编号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户客户编号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：红包流水号
	 */
	public void setCouponSerialNo(String couponSerialNo) {
		this.couponSerialNo = couponSerialNo;
	}
	/**
	 * 获取：红包流水号
	 */
	public String getCouponSerialNo() {
		return couponSerialNo;
	}
	/**
	 * 设置：红包ID
	 */
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}
	/**
	 * 获取：红包ID
	 */
	public String getCouponId() {
		return couponId;
	}
	/**
	 * 设置：使用状态 00：未使用，01：使用中，02：已使用
	 */
	public void setUseStatus(String useStatus) {
		this.useStatus = useStatus;
	}
	/**
	 * 获取：使用状态 00：未使用，01：使用中，02：已使用
	 */
	public String getUseStatus() {
		return useStatus;
	}
	/**
	 * 设置：领取时间
	 */
	public void setRecieveTime(Date recieveTime) {
		this.recieveTime = recieveTime;
	}
	/**
	 * 获取：领取时间
	 */
	public Date getRecieveTime() {
		return recieveTime;
	}
	/**
	 * 设置：有效状态 00：有效 01：失效
	 */
	public void setEffectiveStatus(String effectiveStatus) {
		this.effectiveStatus = effectiveStatus;
	}
	/**
	 * 获取：有效状态 00：有效 01：失效
	 */
	public String getEffectiveStatus() {
		return effectiveStatus;
	}
	/**
	 * 设置：红包使用场景ID
	 */
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	/**
	 * 获取：红包使用场景ID
	 */
	public String getSceneId() {
		return sceneId;
	}
}
