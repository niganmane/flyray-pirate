package com.github.wxiaoqi.security.biz.codex.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.wxiaoqi.security.biz.codex.entity.MarRandomCoupon;

import tk.mybatis.mapper.common.Mapper;

/**
 * 随机红包池表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@org.apache.ibatis.annotations.Mapper
public interface MarRandomCouponMapper extends Mapper<MarRandomCoupon> {
	void addRandomCouponList(List<MarRandomCoupon> list);

	Integer checkRandomCoupon(@Param(value = "couId") String couId);

	/**
	 * 获取用户指定的各个档的红包
	 */
	List<MarRandomCoupon> getRandomBylevel(@Param(value = "couId") String couId,
			@Param(value = "couLevel") String couLevel);

	void updateByID(MarRandomCoupon randomCoupon);

	void changeStatusToOpen();

	void changeStatusToClose();
}
