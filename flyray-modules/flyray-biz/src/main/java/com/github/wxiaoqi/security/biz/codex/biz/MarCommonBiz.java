package com.github.wxiaoqi.security.biz.codex.biz;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.github.wxiaoqi.security.biz.codex.entity.MarCouponUse;
import com.github.wxiaoqi.security.biz.codex.entity.MarDiscountBalanceFile;
import com.github.wxiaoqi.security.biz.codex.entity.MarIntegralUse;
import com.github.wxiaoqi.security.biz.codex.entity.MarTotalInfo;
import com.github.wxiaoqi.security.biz.codex.mapper.MarCommonMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarCouponUseMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarIntegralUseMapper;

/**
 * 营销引擎混合查询
 * @author centerroot
 * @time 创建时间:2018年3月20日下午5:31:00
 * @description
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MarCommonBiz {
	private final static Logger logger = LoggerFactory.getLogger(MarCommonBiz.class);

	@Autowired
	private MarCommonMapper marCommonMapper;
	@Autowired
	private MarCouponUseMapper marCouponUseMapper;
	@Autowired
	private MarIntegralUseMapper marIntegralUseMapper;
	
	/**
	 * 获取折扣优惠列表
	 * @author centerroot
	 * @time 创建时间:2018年3月20日下午5:32:41
	 * @param params
	 * @return
	 */
	public Map<String, Object> getDiscountList(Map<String, Object> params) {
		Map<String, Object> resp = new HashMap<String, Object>();
		logger.info("获取折扣优惠列表===请求参数:{}", params);
		List<MarDiscountBalanceFile> discountList = new ArrayList<MarDiscountBalanceFile>();
		int discountCountNum = 0;
		BigDecimal totalAmt = BigDecimal.ZERO;
		String page = (String) params.get("page");
		String limit = (String) params.get("limit");
		String actId = (String) params.get("actId");
		String merNo = (String) params.get("merNo");
		String sceneId = (String) params.get("sceneId");
		String discountType = (String) params.get("discountType");
		String beginDate = (String) params.get("beginDate");
		String endDate = (String) params.get("endDate");
		Map<String, Object> totalNumMap = new HashMap<String, Object>();
		totalNumMap.put("actId", actId);
		totalNumMap.put("merNo", merNo);
		totalNumMap.put("sceneId", sceneId);
		totalNumMap.put("beginDate", beginDate);
		totalNumMap.put("endDate", endDate);
		totalNumMap.put("txstatus", "00");
		Map<String, Object> reqLimitMap = new HashMap<String, Object>();
		reqLimitMap.put("actId", actId);
		reqLimitMap.put("merNo", merNo);
		reqLimitMap.put("sceneId", sceneId);
		reqLimitMap.put("beginDate", beginDate);
		reqLimitMap.put("endDate", endDate);
		if (!StringUtils.isEmpty(page) && !StringUtils.isEmpty(limit)) {
			Integer pageNo = Integer.valueOf(page);
			Integer pageSize = Integer.valueOf(limit);
			
			reqLimitMap.put("pageNo", (pageNo-1)*pageSize);
			reqLimitMap.put("pageSize", pageSize);
		}
		
		reqLimitMap.put("txstatus", "00");
		// 查询全部
		if ("00".equals(discountType)) {
			logger.info("查询全部的优惠折扣信息");
			if (!StringUtils.isEmpty(actId)) {
				// 活动参数为空，只查询红包的记录
				List<MarCouponUse> marCoupons = marCouponUseMapper.queryMarCouponUseList(reqLimitMap);
				List<MarTotalInfo> marCouponAmtList = marCouponUseMapper.queryMarCouponUseTotalAmt(totalNumMap);

				logger.info("获取折扣优惠列表===优惠记录结果:{}", marCoupons.toString());
				logger.info("获取折扣优惠列表===优惠记录金额:{}", marCouponAmtList.toString());
				if (null != marCoupons && marCoupons.size() > 0) {
					for (int i = 0; i < marCoupons.size(); i++) {
						MarDiscountBalanceFile discount = new MarDiscountBalanceFile();
						MarCouponUse marCouponUse = marCoupons.get(i);
						discount.setActId(marCouponUse.getActId());
						discount.setDiscountAmt(marCouponUse.getCouponAmt());
						discount.setMerchantId(marCouponUse.getMerchantId());
						discount.setMerorderno(marCouponUse.getMerorderno());
						discount.setSceneId(marCouponUse.getSceneId());
						discount.setUseTime(marCouponUse.getUseTime());
						discount.setDiscountType("01");
						discountList.add(discount);
					}
				}
				if (null != marCouponAmtList && marCouponAmtList.size()>0) {
					MarTotalInfo marCouponAmt = marCouponAmtList.get(0);
					discountCountNum = discountCountNum + marCouponAmt.getTotalNum();
					if (null != marCouponAmt.getTotalAmt()) {
						totalAmt = totalAmt.add(marCouponAmt.getTotalAmt());
					}
				}
			}else{
				discountList = marCommonMapper.queryMarDiscountList(reqLimitMap);
				List<MarTotalInfo> marCommonAmtList = marCommonMapper.queryMarDiscountTotalAmt(totalNumMap);
				logger.info("获取折扣优惠列表===优惠记录结果:{}", discountList.toString());
				logger.info("获取折扣优惠列表===优惠记录金额:{}", marCommonAmtList.toString());
				if (null != marCommonAmtList && marCommonAmtList.size()>0) {
					MarTotalInfo marCommonAmt = marCommonAmtList.get(0);
					discountCountNum = discountCountNum + marCommonAmt.getTotalNum();
					if (null != marCommonAmt.getTotalAmt()) {
						totalAmt = totalAmt.add(marCommonAmt.getTotalAmt());
					}
				}
			}
			
			
			
		// 查询红包
		} else if ("01".equals(discountType)) {
			List<MarCouponUse> marCoupons = marCouponUseMapper.queryMarCouponUseList(reqLimitMap);
			List<MarTotalInfo> marCouponAmtList = marCouponUseMapper.queryMarCouponUseTotalAmt(totalNumMap);
			logger.info("获取折扣优惠列表===优惠记录结果:{}", marCouponUseMapper.toString());
			logger.info("获取折扣优惠列表===优惠记录金额:{}", marCouponUseMapper.toString());
			if (null != marCoupons && marCoupons.size() > 0) {
				for (int i = 0; i < marCoupons.size(); i++) {
					MarDiscountBalanceFile discount = new MarDiscountBalanceFile();
					MarCouponUse marCouponUse = marCoupons.get(i);
					discount.setActId(marCouponUse.getActId());
					discount.setDiscountAmt(marCouponUse.getCouponAmt());
					discount.setMerchantId(marCouponUse.getMerchantId());
					discount.setMerorderno(marCouponUse.getMerorderno());
					discount.setSceneId(marCouponUse.getSceneId());
					discount.setUseTime(marCouponUse.getUseTime());
					discount.setDiscountType("01");
					discountList.add(discount);
				}
			}
			if (null != marCouponAmtList && marCouponAmtList.size()>0) {
				MarTotalInfo marCouponAmt = marCouponAmtList.get(0);
				discountCountNum = discountCountNum + marCouponAmt.getTotalNum();
				if (null != marCouponAmt.getTotalAmt()) {
					totalAmt = totalAmt.add(marCouponAmt.getTotalAmt());
				}
			}
		// 查询积分
		} else if ("02".equals(discountType)) {
			List<MarIntegralUse> marIntegrals = marIntegralUseMapper.queryMarIntegralUseList(reqLimitMap);
			List<MarTotalInfo> marIntegralsAmtList = marIntegralUseMapper.queryMarIntegralUseTotalAmt(totalNumMap);
			logger.info("获取折扣优惠列表===优惠记录结果:{}", marIntegrals.toString());
			logger.info("获取折扣优惠列表===优惠记录金额:{}", marIntegralsAmtList.toString());
			if (null != marIntegrals && marIntegrals.size() > 0) {
				for (int i = 0; i < marIntegrals.size(); i++) {
					MarDiscountBalanceFile discount = new MarDiscountBalanceFile();
					MarIntegralUse marIntegralUse = marIntegrals.get(i);
					discount.setDiscountAmt(marIntegralUse.getIntegralDeductionAmt());
					discount.setMerchantId(marIntegralUse.getMerchantId());
					discount.setMerorderno(marIntegralUse.getMerorderno());
					discount.setSceneId(marIntegralUse.getSceneId());
					discount.setUseTime(marIntegralUse.getUseTime());
					discount.setDiscountType("02");
					discountList.add(discount);
				}
			}
			if (null != marIntegralsAmtList && marIntegralsAmtList.size()>0) {
				MarTotalInfo marIntegralsAmt = marIntegralsAmtList.get(0);
				discountCountNum = discountCountNum + marIntegralsAmt.getTotalNum();
				if (null != marIntegralsAmt.getTotalAmt()) {
					totalAmt = totalAmt.add(marIntegralsAmt.getTotalAmt());
				}
			}
		}
		resp.put("discountList", discountList);
		resp.put("discountCountNum", discountCountNum);
		resp.put("totalAmt", totalAmt);
		logger.info("获取折扣优惠列表===响应参数参数:{}", resp);
		return resp;
	}
}
