package com.github.wxiaoqi.security.biz.codex.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.wxiaoqi.security.biz.codex.biz.MarRandomUseBiz;
import com.github.wxiaoqi.security.biz.codex.entity.MarRandomUse;
import com.github.wxiaoqi.security.common.rest.BaseController;

@Controller
@RequestMapping("marRandomUse")
public class MarRandomUseController extends BaseController<MarRandomUseBiz,MarRandomUse> {

}