package com.github.wxiaoqi.security.biz.modules.fightGroup.mapper;

import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupOrder;

import tk.mybatis.mapper.common.Mapper;

/**
 * 拼团订单表
 * @author he
 * @date 2018-08-13 18:22:43
 */
@org.apache.ibatis.annotations.Mapper
public interface FightGroupOrderMapper extends Mapper<FightGroupOrder> {
	
}
