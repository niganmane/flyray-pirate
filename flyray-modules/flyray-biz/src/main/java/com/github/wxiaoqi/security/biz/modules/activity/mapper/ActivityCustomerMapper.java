package com.github.wxiaoqi.security.biz.modules.activity.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.activity.entity.ActivityCustomer;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-19 10:54:46
 */
 @org.apache.ibatis.annotations.Mapper
public interface ActivityCustomerMapper extends Mapper<ActivityCustomer> {
	
	 List<ActivityCustomer> queryActivityJoins(Map<String, Object> param);
}
