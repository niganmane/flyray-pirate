package com.github.wxiaoqi.security.biz.modules.aisuan.rest;

import com.github.wxiaoqi.security.biz.modules.aisuan.biz.AisuanRecordBiz;
import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanRecord;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordQueryAdminParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordQueryOneParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordQueryParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordReplyParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("aisuanRecordAdmin")
@Slf4j
public class AisuanRecordController extends BaseController<AisuanRecordBiz,AisuanRecord> {
	@Autowired
	private AisuanRecordBiz aisuanRecordBiz;
	/**
	 * 查询记录列表
	 */
	@RequestMapping(value = "/queryListPage",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryListPage(@RequestBody AisuanRecordQueryAdminParam param){
		log.info("查询记录列表,传入参数{}", EntityUtils.beanToMap(param));
		TableResultResponse<AisuanRecord> table = aisuanRecordBiz.queryListPage(param);
		log.info("查询记录列表,返回{}", EntityUtils.beanToMap(table));
		if(table == null){
			return ResponseHelper.success(param, null, ResponseCode.ABNORMAL_FIELDS.getCode(), ResponseCode.ABNORMAL_FIELDS.getMessage());
		}else {
			return ResponseHelper.success(table, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}
	}
	/**
	 * 询单详情
	 */
    @RequestMapping(value = "/queryInfo",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryInfo(@RequestBody AisuanRecordQueryOneParam param) {
    	log.info("询单详情,传入参数{}", EntityUtils.beanToMap(param));
    	Long id = param.getId();
    	AisuanRecord record = new AisuanRecord();
    	record.setId(id); 
    	record = aisuanRecordBiz.selectOne(record);
    	log.info("询单详情,返回{}", EntityUtils.beanToMap(record));
    	if(record == null){
    		return ResponseHelper.success(record, null, ResponseCode.ABNORMAL_FIELDS.getCode(), ResponseCode.ABNORMAL_FIELDS.getMessage());
    	}else {
    		return ResponseHelper.success(record, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
    	}
	}
    /**
     * 回复
     */
    @RequestMapping(value = "/reply",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> reply(@RequestBody AisuanRecordReplyParam param) {
    	log.info("回复,传入参数{}", EntityUtils.beanToMap(param));
    	try {
			aisuanRecordBiz.reply(param);
			return ResponseHelper.success(param, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseHelper.success(param, null, ResponseCode.ABNORMAL_FIELDS.getCode(), ResponseCode.ABNORMAL_FIELDS.getMessage());
			
		}
    }
    /**
     * 查询回复历史
     */
    @RequestMapping(value = "/queryReplyRecord",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryReplyRecord(@RequestBody AisuanRecordQueryOneParam param){
    	log.info("查询回复历史,传入参数{}", EntityUtils.beanToMap(param));
    	List<AisuanRecord> list = aisuanRecordBiz.queryReplyRecord(param);
    	return ResponseHelper.success(list, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
    }
	/**
	 * 咨询完成
	 */
	@RequestMapping(value = "/complete",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> complete(@RequestBody AisuanRecordQueryOneParam param){
		log.info("咨询完成传入参数{}", param);
		return aisuanRecordBiz.complete(param);
    }
}