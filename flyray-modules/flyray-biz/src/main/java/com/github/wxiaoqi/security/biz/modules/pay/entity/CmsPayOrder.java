package com.github.wxiaoqi.security.biz.modules.pay.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 场景支付订单
 * @author he
 * @date 2018-08-09 16:34:06
 */
@Table(name = "cms_pay_order")
public class CmsPayOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户编号
	@Column(name = "merchant_id")
	private String merchantId;

	//用户编号
	@Column(name = "customer_id")
	private String customerId;

	//个人客户编号
	@Column(name = "personal_id")
	private String personalId;

	//商户订单号
	@Column(name = "pay_order_no")
	private String payOrderNo;

	//场景编号
	@Column(name = "scenes_code")
	private String scenesCode;

	//支付金额
	@Column(name = "tx_amt")
	private BigDecimal txAmt;

	//支付方式  1第三方支付 2非第三方支付
	@Column(name = "pay_type")
	private String payType;

	//状态 00：支付成功 01：支付失败 02：待支付 03：支付中
	@Column(name = "tx_status")
	private String txStatus;

	//创建时间
	@Column(name = "create_time")
	private Date createTime;

	//交易方式 1支付，2充值
	@Column(name = "pay_code")
	private String payCode;

	//扩展map
	@Column(name = "ext_map")
	private String extMap;

	//支付手续费
	@Column(name = "tx_fee")
	private BigDecimal txFee;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getPersonalId() {
		return personalId;
	}
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	public String getPayOrderNo() {
		return payOrderNo;
	}
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	/**
	 * 设置：场景编号
	 */
	public void setScenesCode(String scenesCode) {
		this.scenesCode = scenesCode;
	}
	/**
	 * 获取：场景编号
	 */
	public String getScenesCode() {
		return scenesCode;
	}
	/**
	 * 设置：支付金额
	 */
	public void setTxAmt(BigDecimal txAmt) {
		this.txAmt = txAmt;
	}
	/**
	 * 获取：支付金额
	 */
	public BigDecimal getTxAmt() {
		return txAmt;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getTxStatus() {
		return txStatus;
	}
	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getPayCode() {
		return payCode;
	}
	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}
	public String getExtMap() {
		return extMap;
	}
	public void setExtMap(String extMap) {
		this.extMap = extMap;
	}
	public BigDecimal getTxFee() {
		return txFee;
	}
	public void setTxFee(BigDecimal txFee) {
		this.txFee = txFee;
	}
}
