package com.github.wxiaoqi.security.biz.codex.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.MarScene;
import com.github.wxiaoqi.security.biz.codex.mapper.MarSceneMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Service
public class MarSceneBiz extends BaseBiz<MarSceneMapper,MarScene> {
}