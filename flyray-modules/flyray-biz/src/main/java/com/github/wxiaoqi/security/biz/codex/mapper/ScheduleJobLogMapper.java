package com.github.wxiaoqi.security.biz.codex.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.codex.entity.ScheduleJobLog;

import tk.mybatis.mapper.common.Mapper;

/**
 * 定时任务日志
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-26 13:56:02
 */
@org.apache.ibatis.annotations.Mapper
public interface ScheduleJobLogMapper extends Mapper<ScheduleJobLog> {
	List<ScheduleJobLog> queryLogList(Map<String, Object> map);
	void addLog(ScheduleJobLog entity);
}
