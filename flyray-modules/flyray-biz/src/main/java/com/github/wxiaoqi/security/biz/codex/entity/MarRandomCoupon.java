package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 随机红包池表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Table(name = "mar_random_coupon")
public class MarRandomCoupon implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //主键
    @Id
    private Long seqNo;
    
  //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
    
    //商户号
    @Column(name = "MERCHANT_ID")
    private String merchantId;
	
	    //流水号
    @Column(name = "COU_SERIAL")
    private String couSerial;
	
	    //红包编号01一档；02二挡；03三挡
    @Column(name = "COU_ID")
    private String couId;
	
	    //红包等级:01一档 02 二档 03三档
    @Column(name = "COU_LEVEL")
    private String couLevel;
	
	    //随机红包金额
    @Column(name = "COU_AMT")
    private BigDecimal couAmt;
	
	    //是否领取00 未领取；01已领取
    @Column(name = "IS_RECEIVED")
    private String isReceived;
	
	    //是否可领取00可领取；01不可领取
    @Column(name = "REV_STATUS")
    private String revStatus;
	
	    //创建时间
    @Column(name = "CRETE_TIME")
    private Date creteTime;
	

	/**
	 * 设置：主键
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：主键
	 */
	public Long getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：流水号
	 */
	public void setCouSerial(String couSerial) {
		this.couSerial = couSerial;
	}
	/**
	 * 获取：流水号
	 */
	public String getCouSerial() {
		return couSerial;
	}
	/**
	 * 设置：红包编号01一档；02二挡；03三挡
	 */
	public void setCouId(String couId) {
		this.couId = couId;
	}
	/**
	 * 获取：红包编号01一档；02二挡；03三挡
	 */
	public String getCouId() {
		return couId;
	}
	/**
	 * 设置：红包等级:01一档 02 二档 03三档
	 */
	public void setCouLevel(String couLevel) {
		this.couLevel = couLevel;
	}
	/**
	 * 获取：红包等级:01一档 02 二档 03三档
	 */
	public String getCouLevel() {
		return couLevel;
	}
	/**
	 * 设置：随机红包金额
	 */
	public void setCouAmt(BigDecimal couAmt) {
		this.couAmt = couAmt;
	}
	/**
	 * 获取：随机红包金额
	 */
	public BigDecimal getCouAmt() {
		return couAmt;
	}
	/**
	 * 设置：是否领取00 未领取；01已领取
	 */
	public void setIsReceived(String isReceived) {
		this.isReceived = isReceived;
	}
	/**
	 * 获取：是否领取00 未领取；01已领取
	 */
	public String getIsReceived() {
		return isReceived;
	}
	/**
	 * 设置：是否可领取00可领取；01不可领取
	 */
	public void setRevStatus(String revStatus) {
		this.revStatus = revStatus;
	}
	/**
	 * 获取：是否可领取00可领取；01不可领取
	 */
	public String getRevStatus() {
		return revStatus;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreteTime(Date creteTime) {
		this.creteTime = creteTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreteTime() {
		return creteTime;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	
}
