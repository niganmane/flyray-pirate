package com.github.wxiaoqi.security.biz.modules.refund.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 场景退款表
 * @author he
 * @date 2018-08-13 16:19:30
 */
@Table(name = "cms_refund_order")
public class CmsRefundOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户编号
	@Column(name = "merchant_id")
	private String merchantId;

	//用户编号
	@Column(name = "customer_id")
	private String customerId;

	//支付订单号
	@Column(name = "pay_order_no")
	private String payOrderNo;

	//
	@Column(name = "refund_order_no")
	private String refundOrderNo;

	//退款金额
	@Column(name = "tx_amt")
	private BigDecimal txAmt;

	//状态 00：退款成功 01：退款失败 02：退款申请成功 03：退款中
	@Column(name = "tx_status")
	private String txStatus;

	//创建时间
	@Column(name = "create_time")
	private Date createTime;


	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户编号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：用户编号
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：用户编号
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * 设置：支付订单号
	 */
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	/**
	 * 获取：支付订单号
	 */
	public String getPayOrderNo() {
		return payOrderNo;
	}
	/**
	 * 设置：
	 */
	public void setRefundOrderNo(String refundOrderNo) {
		this.refundOrderNo = refundOrderNo;
	}
	/**
	 * 获取：
	 */
	public String getRefundOrderNo() {
		return refundOrderNo;
	}
	/**
	 * 设置：退款金额
	 */
	public void setTxAmt(BigDecimal txAmt) {
		this.txAmt = txAmt;
	}
	/**
	 * 获取：退款金额
	 */
	public BigDecimal getTxAmt() {
		return txAmt;
	}
	/**
	 * 设置：状态 00：退款成功 01：退款失败 02：退款申请成功 03：退款中
	 */
	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}
	/**
	 * 获取：状态 00：退款成功 01：退款失败 02：退款申请成功 03：退款中
	 */
	public String getTxStatus() {
		return txStatus;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
