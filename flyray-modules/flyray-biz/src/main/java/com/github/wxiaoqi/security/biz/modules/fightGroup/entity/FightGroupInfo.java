package com.github.wxiaoqi.security.biz.modules.fightGroup.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 团信息表
 * 
 * @author he
 * @date 2018-07-12 10:17:33
 */
@Table(name = "fight_group_info")
public class FightGroupInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//团id
	@Column(name = "group_id")
	private Integer groupId;

	//团类目id
	@Column(name = "groups_id")
	private Integer groupsId;

	//商品id
	@Column(name = "goods_id")
	private Integer goodsId;

	//已参团人数
	@Column(name = "joined_group_num")
	private Integer joinedGroupNum;

	//团长用户账号
	@Column(name = "head_per_id")
	private String headPerId;

	//截止日期
	@Column(name = "end_date")
	private Timestamp endDate;
	
	//创建时间
	@Column(name = "create_time")
	private Date createTime;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：团id
	 */
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	/**
	 * 获取：团id
	 */
	public Integer getGroupId() {
		return groupId;
	}
	/**
	 * 设置：团类目id
	 */
	public void setGroupsId(Integer groupsId) {
		this.groupsId = groupsId;
	}
	/**
	 * 获取：团类目id
	 */
	public Integer getGroupsId() {
		return groupsId;
	}
	public Integer getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(Integer goodsId) {
		this.goodsId = goodsId;
	}
	/**
	 * 设置：已参团人数
	 */
	public void setJoinedGroupNum(Integer joinedGroupNum) {
		this.joinedGroupNum = joinedGroupNum;
	}
	/**
	 * 获取：已参团人数
	 */
	public Integer getJoinedGroupNum() {
		return joinedGroupNum;
	}
	/**
	 * 设置：团长用户账号
	 */
	public void setHeadPerId(String headPerId) {
		this.headPerId = headPerId;
	}
	/**
	 * 获取：团长用户账号
	 */
	public String getHeadPerId() {
		return headPerId;
	}
	/**
	 * 设置：截止日期
	 */
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	/**
	 * 获取：截止日期
	 */
	public Timestamp getEndDate() {
		return endDate;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
