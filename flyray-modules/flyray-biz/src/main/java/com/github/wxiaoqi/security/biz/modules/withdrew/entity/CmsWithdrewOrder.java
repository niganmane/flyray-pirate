package com.github.wxiaoqi.security.biz.modules.withdrew.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 提现表
 * @author he
 * @date 2018-09-09 13:37:13
 */
@Table(name = "cms_withdrew_order")
public class CmsWithdrewOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户编号
	@Column(name = "merchant_id")
	private String merchantId;

	//用户编号
	@Column(name = "customer_id")
	private String customerId;

	//个人客户编号
	@Column(name = "personal_id")
	private String personalId;

	//支付订单号
	@Column(name = "withdrew_order_no")
	private String withdrewOrderNo;

	//提现金额
	@Column(name = "tx_amt")
	private BigDecimal txAmt;

	//提现状态 00成功 01失败 02申请成功
	@Column(name = "tx_status")
	private String txStatus;

	//创建时间
	@Column(name = "create_time")
	private Date createTime;

	//支付宝账号
	@Column(name = "alipay_no")
	private String alipayNo;


	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户编号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：用户编号
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：用户编号
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * 设置：个人客户编号
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 获取：个人客户编号
	 */
	public String getPersonalId() {
		return personalId;
	}
	/**
	 * 设置：支付订单号
	 */
	public void setWithdrewOrderNo(String withdrewOrderNo) {
		this.withdrewOrderNo = withdrewOrderNo;
	}
	/**
	 * 获取：支付订单号
	 */
	public String getWithdrewOrderNo() {
		return withdrewOrderNo;
	}
	/**
	 * 设置：提现金额
	 */
	public void setTxAmt(BigDecimal txAmt) {
		this.txAmt = txAmt;
	}
	/**
	 * 获取：提现金额
	 */
	public BigDecimal getTxAmt() {
		return txAmt;
	}
	/**
	 * 设置：提现状态 00成功 01失败 02申请成功
	 */
	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}
	/**
	 * 获取：提现状态 00成功 01失败 02申请成功
	 */
	public String getTxStatus() {
		return txStatus;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：支付宝账号
	 */
	public void setAlipayNo(String alipayNo) {
		this.alipayNo = alipayNo;
	}
	/**
	 * 获取：支付宝账号
	 */
	public String getAlipayNo() {
		return alipayNo;
	}
}
