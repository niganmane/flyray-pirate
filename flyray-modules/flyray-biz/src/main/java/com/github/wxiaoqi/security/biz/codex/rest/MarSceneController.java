package com.github.wxiaoqi.security.biz.codex.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.wxiaoqi.security.biz.codex.biz.MarSceneBiz;
import com.github.wxiaoqi.security.biz.codex.entity.MarScene;
import com.github.wxiaoqi.security.common.rest.BaseController;

/**
 * 场景管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("marScenes")
public class MarSceneController extends BaseController<MarSceneBiz, MarScene> {

}
