package com.github.wxiaoqi.security.biz.modules.comment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * 
 * 
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:44
 */
@Table(name = "cms_community_comment")
public class Comment implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //主键id
    @Id
    private String id;
	
	    //评论类型(1评论2回复)
    @Column(name = "comment_type")
    private String commentType;
	
	    //评论内容
    @Column(name = "comment_content")
    private String commentContent;
	
	    //评论的目标id
    @Column(name = "comment_target_id")
    private String commentTargetId;
	
	    //发表评论的用户昵称
    @Column(name = "comment_by_name")
    private String commentByName;
	
	    //发表评论的用户id
    @Column(name = "customer_id")
    private String customerId;
	
	    //评论的目标用户昵称
    @Column(name = "comment_target_user_name")
    private String commentTargetUserName;
	
	    //评论的目标用户id
    @Column(name = "comment_target_user_id")
    private String commentTargetUserId;
	
	    //该评论被点赞的数量
    @Column(name = "comment_likeCount")
    private Long commentLikecount;
    
    //发表评论的用户头像
    @Column(name = "avatarUrl")
    private String avatarUrl;
	
	    //评论时间
    @Column(name = "comment_time")
    private Date commentTime;
	
	    //评论模块编号01社群 02卖朋友
    @Column(name = "comment_module_no")
    private String commentModuleNo;
	
	    //商户号
    @Column(name = "merchant_id")
    private String merchantId;
	
	    //机构号
    @Column(name = "platform_id")
    private String platformId;
	
	    //回复评论的目标id
    @Column(name = "parent_id")
    private String parentId;
	
    @Transient
    private List<Comment> comments; 

	/**
	 * 设置：主键id
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：主键id
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：评论类型(1评论2回复)
	 */
	public void setCommentType(String commentType) {
		this.commentType = commentType;
	}
	/**
	 * 获取：评论类型(1评论2回复)
	 */
	public String getCommentType() {
		return commentType;
	}
	/**
	 * 设置：评论内容
	 */
	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}
	/**
	 * 获取：评论内容
	 */
	public String getCommentContent() {
		return commentContent;
	}
	/**
	 * 设置：评论的目标id
	 */
	public void setCommentTargetId(String commentTargetId) {
		this.commentTargetId = commentTargetId;
	}
	/**
	 * 获取：评论的目标id
	 */
	public String getCommentTargetId() {
		return commentTargetId;
	}
	/**
	 * 设置：发表评论的用户昵称
	 */
	public void setCommentByName(String commentByName) {
		this.commentByName = commentByName;
	}
	/**
	 * 获取：发表评论的用户昵称
	 */
	public String getCommentByName() {
		return commentByName;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 设置：评论的目标用户昵称
	 */
	public void setCommentTargetUserName(String commentTargetUserName) {
		this.commentTargetUserName = commentTargetUserName;
	}
	/**
	 * 获取：评论的目标用户昵称
	 */
	public String getCommentTargetUserName() {
		return commentTargetUserName;
	}
	/**
	 * 设置：评论的目标用户id
	 */
	public void setCommentTargetUserId(String commentTargetUserId) {
		this.commentTargetUserId = commentTargetUserId;
	}
	/**
	 * 获取：评论的目标用户id
	 */
	public String getCommentTargetUserId() {
		return commentTargetUserId;
	}
	/**
	 * 设置：该评论被点赞的数量
	 */
	public void setCommentLikecount(Long commentLikecount) {
		this.commentLikecount = commentLikecount;
	}
	/**
	 * 获取：该评论被点赞的数量
	 */
	public Long getCommentLikecount() {
		return commentLikecount;
	}
	/**
	 * 设置：评论时间
	 */
	public void setCommentTime(Date commentTime) {
		this.commentTime = commentTime;
	}
	/**
	 * 获取：评论时间
	 */
	public Date getCommentTime() {
		return commentTime;
	}
	/**
	 * 设置：评论模块编号01社群 02卖朋友
	 */
	public void setCommentModuleNo(String commentModuleNo) {
		this.commentModuleNo = commentModuleNo;
	}
	/**
	 * 获取：评论模块编号01社群 02卖朋友
	 */
	public String getCommentModuleNo() {
		return commentModuleNo;
	}
	/**
	 * 设置：商户号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：机构号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：机构号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：回复评论的目标id
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	/**
	 * 获取：回复评论的目标id
	 */
	public String getParentId() {
		return parentId;
	}
	
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	@Override
	public String toString() {
		return "Comment [id=" + id + ", commentType=" + commentType + ", commentContent=" + commentContent
				+ ", commentTargetId=" + commentTargetId + ", commentByName=" + commentByName + ", customerId="
				+ customerId + ", commentTargetUserName=" + commentTargetUserName + ", commentTargetUserId="
				+ commentTargetUserId + ", commentLikecount=" + commentLikecount + ", avatarUrl=" + avatarUrl
				+ ", commentTime=" + commentTime + ", commentModuleNo=" + commentModuleNo + ", merchantId=" + merchantId
				+ ", platformId=" + platformId + ", parentId=" + parentId + ", comments=" + comments + "]";
	}
	
}
