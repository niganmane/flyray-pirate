package com.github.wxiaoqi.security.biz.modules.restaurant.biz;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantOrderInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantReservationInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantOrderInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantReservationInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 预订信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantReservationInfoBiz extends BaseBiz<RestaurantReservationInfoMapper,RestaurantReservationInfo> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantReservationInfoBiz.class);
	
	@Autowired
	private	RestaurantReservationInfoMapper restaurantReservationInfoMapper;
	@Autowired
	private	RestaurantOrderInfoMapper restaurantOrderInfoMapper;
	
	/**
	 * 提交预订信息
	 * @param request
	 * @return
	 */
	public Map<String, Object> submitReservationInfo(Map<String, Object> request){
		logger.info("提交预订信息请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String reservation = (String) request.get("reservation");
		JSONObject reservationObject = (JSONObject) JSONObject.parse(reservation);
		String platformId = (String) reservationObject.get("platformId");
		String merchantId = (String) reservationObject.get("merchantId");
		String perId = (String) reservationObject.get("perId");
		String reservationName = (String) reservationObject.get("reservationName");
		String reservationPeople = (String) reservationObject.get("reservationPeople");
		String reservationPhone = (String) reservationObject.get("reservationPhone");
		String reservationRemark = (String) reservationObject.get("reservationRemark");
		String reservationDate = (String) reservationObject.get("reservationDate");
		String reservationTime = (String) reservationObject.get("reservationTime");
		String payOrderNo = (String) reservationObject.get("payOrderNo");
		long id = SnowFlake.getId();
		RestaurantReservationInfo restaurantReservationInfo = new RestaurantReservationInfo();
		restaurantReservationInfo.setReservationId(String.valueOf(id));
		restaurantReservationInfo.setPlatformId(platformId);
		restaurantReservationInfo.setMerchantId(merchantId);
		restaurantReservationInfo.setPerId(perId);
		restaurantReservationInfo.setReservationName(reservationName);
		restaurantReservationInfo.setReservationPeople(reservationPeople);
		restaurantReservationInfo.setReservationPhone(reservationPhone);
		restaurantReservationInfo.setReservationRemark(reservationRemark);
		restaurantReservationInfo.setStatus("1");//已预订
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
		try {
			restaurantReservationInfo.setReservationDate(sdfDate.parse(reservationDate));
			restaurantReservationInfo.setReservationTime(sdfTime.parse(reservationTime));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		restaurantReservationInfoMapper.insert(restaurantReservationInfo);
		
		//如果订单号不为空，则更新订单表中配送id
		if(!StringUtils.isEmpty(payOrderNo)){
			RestaurantOrderInfo restaurantOrderInfo = new RestaurantOrderInfo();
			restaurantOrderInfo.setPlatformId(platformId);
			restaurantOrderInfo.setMerchantId(merchantId);
			restaurantOrderInfo.setPayOrderNo(payOrderNo);
			restaurantOrderInfo.setType("2");//预订
			RestaurantOrderInfo selectOrderInfo = restaurantOrderInfoMapper.selectOne(restaurantOrderInfo);
			if(null != selectOrderInfo){
				selectOrderInfo.setReservationId(String.valueOf(id));
				restaurantOrderInfoMapper.updateByPrimaryKey(selectOrderInfo);
			}
		}
		response.put("reservationId", String.valueOf(id));
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("提交预订信息响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询预订信息列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<RestaurantReservationInfo> queryRestaurantReservationInfoPage(RestaurantQueryParam param) {
		logger.info("查询预订信息列表。。。。{}"+param);
		Example example = new Example(RestaurantReservationInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if (param.getTableName() != null && param.getTableName().length() > 0) {
			criteria.andEqualTo("tableName",param.getTableName());
		}
		example.setOrderByClause("reservation_time desc");
		Page<RestaurantReservationInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<RestaurantReservationInfo> list = mapper.selectByExample(example);
		TableResultResponse<RestaurantReservationInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
}