package com.github.wxiaoqi.security.biz.modules.activity.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.activity.entity.ActivityInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-19 10:54:45
 */
@org.apache.ibatis.annotations.Mapper
public interface ActivityInfoMapper extends Mapper<ActivityInfo> {
	
	List<ActivityInfo> queryActivities(Map<String, Object> map);
}
