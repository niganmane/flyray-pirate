package com.github.wxiaoqi.security.biz.modules.community.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:43
 */
@Table(name = "cms_community_society")
public class CommunitySociety implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //主键id
    @Id
    private String id;
    
    
    //名称
    @Column(name = "society_name")
    private String societyName;
    
	    //二维码
    @Column(name = "society_qr")
    private String societyQr;
	
	    //介绍内容
    @Column(name = "society_content")
    private String societyContent;
	
    //商户号
	@Column(name = "merchant_id")
	private String merchantId;
	
	
	//平台编号
	@Column(name = "platform_id")
	private String platformId;
	
	//创建时间
	@Column(name = "create_time")
	private Timestamp createTime;

	/**
	 * 设置：主键id
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：主键id
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：二维码
	 */
	public void setSocietyQr(String societyQr) {
		this.societyQr = societyQr;
	}
	/**
	 * 获取：二维码
	 */
	public String getSocietyQr() {
		return societyQr;
	}
	/**
	 * 设置：介绍内容
	 */
	public void setSocietyContent(String societyContent) {
		this.societyContent = societyContent;
	}
	/**
	 * 获取：介绍内容
	 */
	public String getSocietyContent() {
		return societyContent;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getSocietyName() {
		return societyName;
	}
	public void setSocietyName(String societyName) {
		this.societyName = societyName;
	}
	@Override
	public String toString() {
		return "CommunitySociety [id=" + id + ", societyName=" + societyName + ", societyQr=" + societyQr
				+ ", societyContent=" + societyContent + ", merchantId=" + merchantId + ", platformId=" + platformId
				+ ", createTime=" + createTime + "]";
	}

}
