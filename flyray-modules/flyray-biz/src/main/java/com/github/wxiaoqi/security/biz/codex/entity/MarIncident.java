package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Table(name = "mar_incident")
public class MarIncident implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Long seqNo;
	
	    //事件名称
    @Column(name = "INC_NAME")
    private String incName;
	
	    //事件代码 
    @Column(name = "INC_CODE")
    private String incCode;
	
	    //事件说明
    @Column(name = "INC_INFO")
    private String incInfo;
	
	    //订单号是否必传 0表示不 1表示必传
    @Column(name = "IS_NEED")
    private String isNeed;
	

	/**
	 * 设置：
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：
	 */
	public Long getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：事件名称
	 */
	public void setIncName(String incName) {
		this.incName = incName;
	}
	/**
	 * 获取：事件名称
	 */
	public String getIncName() {
		return incName;
	}
	/**
	 * 设置：事件代码 
	 */
	public void setIncCode(String incCode) {
		this.incCode = incCode;
	}
	/**
	 * 获取：事件代码 
	 */
	public String getIncCode() {
		return incCode;
	}
	/**
	 * 设置：事件说明
	 */
	public void setIncInfo(String incInfo) {
		this.incInfo = incInfo;
	}
	/**
	 * 获取：事件说明
	 */
	public String getIncInfo() {
		return incInfo;
	}
	/**
	 * 设置：订单号是否必传 0表示不 1表示必传
	 */
	public void setIsNeed(String isNeed) {
		this.isNeed = isNeed;
	}
	/**
	 * 获取：订单号是否必传 0表示不 1表示必传
	 */
	public String getIsNeed() {
		return isNeed;
	}
}
