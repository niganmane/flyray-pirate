package com.github.wxiaoqi.security.biz.modules.pay.api;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.api.BaseController;
import com.github.wxiaoqi.security.biz.modules.pay.biz.PayBiz;
import com.github.wxiaoqi.security.common.pay.request.CreateAndPayParam;
import com.github.wxiaoqi.security.common.pay.request.CreatePayOrderParam;
import com.github.wxiaoqi.security.common.pay.request.DoPayParam;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 支付相关接口
 * @author he
 *
 */
@Api(tags="支付管理")
@Controller
@RequestMapping("pay")
public class PayController extends BaseController {
	
	@Autowired
	private PayBiz payBiz;
	
	/**
	 * 创建订单并支付
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("创建订单并支付")
	@RequestMapping(value = "/createAndPay",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> createAndPay(@RequestBody @Valid CreateAndPayParam createAndPayParam) throws Exception {
		Map<String, Object> response = payBiz.createAndPay(EntityUtils.beanToMap(createAndPayParam));
		return response;
    }
	
	/**
	 * 创建订单
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("创建订单")
	@RequestMapping(value = "/createPayOrder",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> createPayOrder(@RequestBody @Valid CreatePayOrderParam createPayOrderParam) throws Exception {
		Map<String, Object> response = payBiz.createPayOrder(EntityUtils.beanToMap(createPayOrderParam));
		return response;
	}
	
	/**
	 * 支付
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("支付")
	@RequestMapping(value = "/doPay",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> doPay(@RequestBody @Valid DoPayParam doPayParam) throws Exception {
		Map<String, Object> response = payBiz.doPay(EntityUtils.beanToMap(doPayParam));
		return response;
	}

}
