package com.github.wxiaoqi.security.biz.modules.fightGroup.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 团成员信息表
 * 
 * @author he
 * @date 2018-07-12 10:17:33
 */
@Table(name = "fight_group_member_info")
public class FightGroupMemberInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//团id
	@Column(name = "group_id")
	private Integer groupId;

	//用户账号
	@Column(name = "per_id")
	private String perId;

	//用户名称
	@Column(name = "name")
	private String name;

	//用户头像
	@Column(name = "user_head_portrait")
	private String userHeadPortrait;
	
	//创建时间
	@Column(name = "create_time")
	private Date createTime;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：团id
	 */
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	/**
	 * 获取：团id
	 */
	public Integer getGroupId() {
		return groupId;
	}
	/**
	 * 设置：用户账号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：用户账号
	 */
	public String getPerId() {
		return perId;
	}
	/**
	 * 设置：用户名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：用户名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：用户头像
	 */
	public void setUserHeadPortrait(String userHeadPortrait) {
		this.userHeadPortrait = userHeadPortrait;
	}
	/**
	 * 获取：用户头像
	 */
	public String getUserHeadPortrait() {
		return userHeadPortrait;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
