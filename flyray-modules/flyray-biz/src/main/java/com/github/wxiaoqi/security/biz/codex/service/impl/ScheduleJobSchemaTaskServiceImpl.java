
package com.github.wxiaoqi.security.biz.codex.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.quartz.CronTrigger;
import org.quartz.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.ScheduleJob;
import com.github.wxiaoqi.security.biz.codex.mapper.ScheduleJobMapper;
import com.github.wxiaoqi.security.biz.codex.service.ScheduleJobSchemaTaskService;
import com.github.wxiaoqi.security.biz.util.Constant;
import com.github.wxiaoqi.security.biz.util.ScheduleUtils;

@Service("scheduleJobService")
public class ScheduleJobSchemaTaskServiceImpl implements ScheduleJobSchemaTaskService {
	
	private static Logger logger = LoggerFactory.getLogger(ScheduleJobSchemaTaskServiceImpl.class);
	
	@Autowired
    private Scheduler scheduler;
	
	@Autowired
	private ScheduleJobMapper scheduleJobMapper;
	/**
	 * 项目启动时，初始化定时器
	 */
	@PostConstruct
	public void init(){
		List<ScheduleJob> scheduleJobList = scheduleJobMapper.queryList(null);
		if (scheduleJobList != null && scheduleJobList.size() > 0) {
			for(ScheduleJob scheduleJob : scheduleJobList){
				CronTrigger cronTrigger = ScheduleUtils.getCronTrigger(scheduler, scheduleJob.getJobId());
				//如果不存在，则创建
				if(cronTrigger == null) {
					ScheduleUtils.createScheduleJob(scheduler, scheduleJob);
				}else {
					ScheduleUtils.updateScheduleJob(scheduler, scheduleJob);
				}
			}
		}
	}

    
	@Override
	public List<ScheduleJob> queryJobList(Map<String, Object> params) {
		return scheduleJobMapper.queryList(params);
	}


	@Override
	public void addJob(ScheduleJob scheduleJob) {
		logger.info("新增。。。。。。。。。。{}"+scheduleJob);
		scheduleJob.setCreateTime(new Date());
		scheduleJob.setStatus(Constant.ScheduleStatus.NORMAL.getValue());
        scheduleJobMapper.addJob(scheduleJob);
        Map<String, Object> map = new HashMap<>();
		List<ScheduleJob> list = scheduleJobMapper.queryList(map);
		ScheduleJob entity = list.get(0);
		ScheduleUtils.createScheduleJob(scheduler, entity);
	}


	@Override
	public void updateJob(ScheduleJob scheduleJob) {
        ScheduleUtils.updateScheduleJob(scheduler, scheduleJob);
        scheduleJobMapper.updateJob(scheduleJob);
	}


	@Override
	public void deleteBatch(Long jobId) {
		
		ScheduleUtils.deleteScheduleJob(scheduler, jobId);
		ScheduleJob entity  = new ScheduleJob();
		entity.setJobId(jobId);
		scheduleJobMapper.deleteJob(entity);
	}


	@Override
	public void runJob(Long jobId) {
		Map<String, Object> map = new HashMap<>();
		map.put("jobId", jobId);
		List<ScheduleJob> list = scheduleJobMapper.queryList(map);
		if (list != null && list.size() > 0) {
			ScheduleJob entity = list.get(0);
			ScheduleUtils.run(scheduler, entity);
		}
	}


	@Override
	public void pauseJob(Long jobId) {
		ScheduleUtils.pauseJob(scheduler, jobId);
		Map<String, Object> map = new HashMap<>();
		map.put("jobId", jobId);
		List<ScheduleJob> list = scheduleJobMapper.queryList(map);
		if (list != null && list.size() > 0) {
			ScheduleJob entity = list.get(0);
			entity.setStatus(Constant.ScheduleStatus.PAUSE.getValue());
			scheduleJobMapper.updateStatus(entity);
		}
	}


	@Override
	public void resumeJob(Long jobId) {
		ScheduleUtils.resumeJob(scheduler, jobId);
		Map<String, Object> map = new HashMap<>();
		map.put("jobId", jobId);
		List<ScheduleJob> list = scheduleJobMapper.queryList(map);
		if (list != null && list.size() > 0) {
			ScheduleJob entity = list.get(0);
			entity.setStatus(Constant.ScheduleStatus.NORMAL.getValue());
			scheduleJobMapper.updateStatus(entity);
		}
	}

    
}
