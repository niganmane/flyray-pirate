package com.github.wxiaoqi.security.biz.modules.restaurant.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 购物车信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Table(name = "restaurant_shopping_cart_info")
public class RestaurantShoppingCartInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//用户账号
	@Column(name = "per_id")
	private String perId;
	
	//支付订单号
	@Column(name = "pay_order_no")
	private String payOrderNo;
	
	//餐桌id
	@Column(name = "table_id")
	private String tableId;

	//菜品id
	@Column(name = "dishes_id")
	private String dishesId;

	//菜品名称
	@Column(name = "dishes_name")
	private String dishesName;

	//数量
	@Column(name = "dishes_num")
	private Integer dishesNum;

	//总价
	@Column(name = "dishes_price")
	private String dishesPrice;

	//状态 1已选择 2已提交 3已支付
	@Column(name = "status")
	private String status;
	
	//类型 1点餐 2外卖 3预约
	@Column(name = "type")
	private String type;
	
	//是否评价 0：已评价 1：未评价
	@Column(name = "is_appraisal")
	private String isAppraisal;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerchantId(String merId) {
		this.merchantId = merId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：用户账号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：用户账号
	 */
	public String getPerId() {
		return perId;
	}
	public String getPayOrderNo() {
		return payOrderNo;
	}
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	/**
	 * 设置：菜品id
	 */
	public void setDishesId(String dishesId) {
		this.dishesId = dishesId;
	}
	/**
	 * 获取：菜品id
	 */
	public String getDishesId() {
		return dishesId;
	}
	/**
	 * 设置：菜品名称
	 */
	public void setDishesName(String dishesName) {
		this.dishesName = dishesName;
	}
	/**
	 * 获取：菜品名称
	 */
	public String getDishesName() {
		return dishesName;
	}
	/**
	 * 设置：数量
	 */
	public void setDishesNum(Integer dishesNum) {
		this.dishesNum = dishesNum;
	}
	/**
	 * 获取：数量
	 */
	public Integer getDishesNum() {
		return dishesNum;
	}
	/**
	 * 设置：总价
	 */
	public void setDishesPrice(String dishesPrice) {
		this.dishesPrice = dishesPrice;
	}
	/**
	 * 获取：总价
	 */
	public String getDishesPrice() {
		return dishesPrice;
	}
	/**
	 * 设置：状态
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：状态
	 */
	public String getStatus() {
		return status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIsAppraisal() {
		return isAppraisal;
	}
	public void setIsAppraisal(String isAppraisal) {
		this.isAppraisal = isAppraisal;
	}
}
