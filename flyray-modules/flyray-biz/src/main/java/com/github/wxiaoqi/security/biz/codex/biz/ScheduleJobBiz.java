package com.github.wxiaoqi.security.biz.codex.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.ScheduleJob;
import com.github.wxiaoqi.security.biz.codex.mapper.ScheduleJobMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 定时任务
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-26 13:56:02
 */
@Service
public class ScheduleJobBiz extends BaseBiz<ScheduleJobMapper,ScheduleJob> {
}