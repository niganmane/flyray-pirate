package com.github.wxiaoqi.security.biz.modules.aisuan.biz;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanRecommend;
import com.github.wxiaoqi.security.biz.modules.aisuan.mapper.AisuanRecommendMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.AisuanAddRecommendParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;


/**
 * 
 *
 * @author 
 * @email ${email}
 * @date 2018-09-10 11:45:46
 */
@Service
public class AisuanRecommendBiz extends BaseBiz<AisuanRecommendMapper,AisuanRecommend> {
	
	public List<AisuanRecommend> query(){
		List<AisuanRecommend> list = mapper.selectAll();
		return list;
	}
	public Map<String, Object> add(AisuanAddRecommendParam param) {
		Example example = new Example(AisuanRecommend.class);
		Criteria criteria = example.createCriteria();
		//判断现在有多少条
		List<AisuanRecommend> list = mapper.selectByExample(example);
		if(list.size() >= 6){
			return ResponseHelper.success(param, null, ResponseCode.USER_NUMBER_OVER.getCode(), ResponseCode.USER_NUMBER_OVER.getMessage());
		}
		//判断这个用户存在吗
		criteria.andEqualTo("userId", param.getUserId());
		List<AisuanRecommend> list2 = mapper.selectByExample(example);
		if(list2.size() > 0){
			return ResponseHelper.success(param, null, ResponseCode.USER_NUMBER_OVER.getCode(), ResponseCode.USER_NUMBER_OVER.getMessage());
		}
		AisuanRecommend recommend = new AisuanRecommend();
		try {
			BeanUtils.copyProperties(param, recommend);
			recommend.setCreateTime(new Date());
			recommend.setPlatformId(Long.valueOf(param.getPlatformId()));
			if(param.getMerchantId() != null && param.getMerchantId() != ""){
				recommend.setMerchantId(Long.valueOf(param.getMerchantId()));
			}
			
			mapper.insertSelective(recommend);
			return ResponseHelper.success(param, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseHelper.success(param, null, ResponseCode.ABNORMAL_FIELDS.getCode(), ResponseCode.ABNORMAL_FIELDS.getMessage());
	}
}