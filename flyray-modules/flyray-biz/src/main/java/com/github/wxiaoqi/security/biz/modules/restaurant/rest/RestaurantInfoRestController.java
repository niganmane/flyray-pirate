package com.github.wxiaoqi.security.biz.modules.restaurant.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantInfo;
import com.github.wxiaoqi.security.common.cms.request.RestaurantPicRequestParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序点餐餐厅信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/restaurant")
public class RestaurantInfoRestController extends BaseController<RestaurantInfoBiz, RestaurantInfo>{
	
	@Autowired
	private RestaurantInfoBiz restaurantInfoBiz;
	
	/**
	 * 查询点餐餐厅信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<RestaurantInfo> query(@RequestBody RestaurantQueryParam param) {
		log.info("查询点餐餐厅信息列表------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return restaurantInfoBiz.queryRestaurantInfoPage(param);
	}
	
	/**
	 * 添加餐厅
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody RestaurantRequestParam param) {
		log.info("添加餐厅------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		Map<String, Object> respMap = restaurantInfoBiz.addRestaurantInfo(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("添加餐厅-----end-------------{}", respMap);
		return respMap;
	}
	
	/**
	 * 删除餐厅
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody RestaurantRequestParam param) {
		log.info("删除餐厅------start------{}", param);
		Map<String, Object> respMap = restaurantInfoBiz.deleteRestaurantInfo(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除餐厅------end------{}", respMap);
		return respMap;
	}

	/**
	 * 修改餐厅
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody RestaurantRequestParam param) {
		log.info("修改餐厅------start------{}", param);
		Map<String, Object> respMap = restaurantInfoBiz.updateRestaurantInfo(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("修改餐厅------end------{}", respMap);
		return respMap;
	}
	
	
	/**
	 * 添加商品图片
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addPic", method = RequestMethod.POST)
	public Map<String, Object> addPic(@RequestBody RestaurantPicRequestParam param) {
		log.info("添加商品图片------start------{}", param);
		Map<String, Object> respMap = restaurantInfoBiz.addPic(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("添加商品图片------end------{}", respMap);
		return respMap;
	}
	
	
	/**
	 * 删除商品图片
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deletePic", method = RequestMethod.POST)
	public Map<String, Object> deletePic(@RequestBody RestaurantPicRequestParam param) {
		log.info("删除商品图片------start------{}", param);
		Map<String, Object> respMap = restaurantInfoBiz.deletePic(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除商品图片------end------{}", respMap);
		return respMap;
	}

}
