package com.github.wxiaoqi.security.biz.modules.restaurant.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 配送信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Table(name = "restaurant_distribution_info")
public class RestaurantDistributionInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	
	//配送地址id
	@Column(name = "distribution_id")
	private String distributionId;
	
	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//用户账号
	@Column(name = "per_id")
	private String perId;

	//配送姓名
	@Column(name = "distribution_name")
	private String distributionName;

	//配送手机号
	@Column(name = "distribution_phone")
	private String distributionPhone;

	//省
	@Column(name = "distribution_province")
	private String distributionProvince;

	//市
	@Column(name = "distribution_city")
	private String distributionCity;

	//区
	@Column(name = "distribution_area")
	private String distributionArea;

	//配送地址
	@Column(name = "distribution_address")
	private String distributionAddress;

	//默认标识
	@Column(name = "default_type")
	private String defaultType;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	public String getDistributionId() {
		return distributionId;
	}
	public void setDistributionId(String distributionId) {
		this.distributionId = distributionId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：用户账号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：用户账号
	 */
	public String getPerId() {
		return perId;
	}
	/**
	 * 设置：配送姓名
	 */
	public void setDistributionName(String distributionName) {
		this.distributionName = distributionName;
	}
	/**
	 * 获取：配送姓名
	 */
	public String getDistributionName() {
		return distributionName;
	}
	/**
	 * 设置：配送手机号
	 */
	public void setDistributionPhone(String distributionPhone) {
		this.distributionPhone = distributionPhone;
	}
	/**
	 * 获取：配送手机号
	 */
	public String getDistributionPhone() {
		return distributionPhone;
	}
	public String getDistributionProvince() {
		return distributionProvince;
	}
	public void setDistributionProvince(String distributionProvince) {
		this.distributionProvince = distributionProvince;
	}
	public String getDistributionCity() {
		return distributionCity;
	}
	public void setDistributionCity(String distributionCity) {
		this.distributionCity = distributionCity;
	}
	public String getDistributionArea() {
		return distributionArea;
	}
	public void setDistributionArea(String distributionArea) {
		this.distributionArea = distributionArea;
	}
	public String getDistributionAddress() {
		return distributionAddress;
	}
	public void setDistributionAddress(String distributionAddress) {
		this.distributionAddress = distributionAddress;
	}
	public String getDefaultType() {
		return defaultType;
	}
	public void setDefaultType(String defaultType) {
		this.defaultType = defaultType;
	}
}
