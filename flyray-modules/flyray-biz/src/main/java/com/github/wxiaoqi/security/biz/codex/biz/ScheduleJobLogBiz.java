package com.github.wxiaoqi.security.biz.codex.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.ScheduleJobLog;
import com.github.wxiaoqi.security.biz.codex.mapper.ScheduleJobLogMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 定时任务日志
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-26 13:56:02
 */
@Service
public class ScheduleJobLogBiz extends BaseBiz<ScheduleJobLogMapper,ScheduleJobLog> {
}