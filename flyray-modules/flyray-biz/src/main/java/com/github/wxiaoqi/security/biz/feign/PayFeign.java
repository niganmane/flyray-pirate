package com.github.wxiaoqi.security.biz.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 支付相关接口调用
 * @author he
 *
 */
@FeignClient(value = "flyray-pay-core")
public interface PayFeign {
	
	/**
	 * 创建订单
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/payment/creatPayOrder",method = RequestMethod.POST)
	public Map<String, Object> createPayOrder(@RequestBody Map<String, Object> params);
	
	/**
	 * 支付
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/payment/doPay",method = RequestMethod.POST)
	public Map<String, Object> doPay(@RequestBody Map<String, Object> params);
	
	/**
	 * 退款申请
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/refund/refundApply",method = RequestMethod.POST)
	public Map<String, Object> refundApply(@RequestBody Map<String, Object> params);
	
	/**
	 * 退款审核
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/refund/refund",method = RequestMethod.POST)
	public Map<String, Object> refund(@RequestBody Map<String, Object> params);

}
