package com.github.wxiaoqi.security.biz.modules.aisuan.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanAdvice;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-10 11:45:46
 */
@org.apache.ibatis.annotations.Mapper
public interface AisuanAdviceMapper extends Mapper<AisuanAdvice> {
	List<AisuanAdvice> queryList(Map<String, Object> map);
	
}
