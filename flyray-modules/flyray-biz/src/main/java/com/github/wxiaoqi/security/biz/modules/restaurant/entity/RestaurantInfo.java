package com.github.wxiaoqi.security.biz.modules.restaurant.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * 餐厅信息表
 * 
 * @author he
 * @date 2018-06-29 10:32:56
 */
@Table(name = "restaurant_info")
public class RestaurantInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	
	//餐馆编号
	@Column(name = "restaurant_id")
	private String restaurantId;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//店铺名称
	@Column(name = "name")
	private String name;
	
	//店铺封面照片
	@Column(name = "logo_img")
	private String logoImg;

	//区域
	@Column(name = "area")
	private String area;
	
	//纬度
	@Column(name = "latitude")
	private String latitude;
	
	//经度
	@Column(name = "longitude")
	private String longitude;

	//附加信息
	@Column(name = "con_service")
	private String conService;

	//人均消费
	@Column(name = "per_capita_spending")
	private String perCapitaSpending;

	//公告
	@Column(name = "bulletin")
	private String bulletin;

	//电话
	@Column(name = "con_mobile")
	private String conMobile;

	//营业开始时间
	@Column(name = "open_btime")
	private Time openBtime;

	//营业结束时间
	@Column(name = "open_etime")
	private Time openEtime;

	//门店介绍
	@Column(name = "introduce")
	private String introduce;

	//商家资质
	@Column(name = "qualification")
	private String qualification;

	//点餐开关 1开
	@Column(name = "is_order")
	private String isOrder;

	//预订开关 1开
	@Column(name = "is_reservation")
	private String isReservation;

	//外卖开关 1开
	@Column(name = "is_takeaway")
	private String isTakeaway;
	
	//餐厅照片列表
	@Transient
	private List<RestaurantPictureInfo> pictureList;

	public String getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}
	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerchantId(String merId) {
		this.merchantId = merId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：店铺名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：店铺名称
	 */
	public String getName() {
		return name;
	}
	public String getLogoImg() {
		return logoImg;
	}
	public void setLogoImg(String logoImg) {
		this.logoImg = logoImg;
	}
	/**
	 * 设置：区域
	 */
	public void setArea(String area) {
		this.area = area;
	}
	/**
	 * 获取：区域
	 */
	public String getArea() {
		return area;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	/**
	 * 设置：附加信息
	 */
	public void setConService(String conService) {
		this.conService = conService;
	}
	/**
	 * 获取：附加信息
	 */
	public String getConService() {
		return conService;
	}
	/**
	 * 设置：人均消费
	 */
	public void setPerCapitaSpending(String perCapitaSpending) {
		this.perCapitaSpending = perCapitaSpending;
	}
	/**
	 * 获取：人均消费
	 */
	public String getPerCapitaSpending() {
		return perCapitaSpending;
	}
	/**
	 * 设置：公告
	 */
	public void setBulletin(String bulletin) {
		this.bulletin = bulletin;
	}
	/**
	 * 获取：公告
	 */
	public String getBulletin() {
		return bulletin;
	}
	/**
	 * 设置：电话
	 */
	public void setConMobile(String conMobile) {
		this.conMobile = conMobile;
	}
	/**
	 * 获取：电话
	 */
	public String getConMobile() {
		return conMobile;
	}
	/**
	 * 设置：营业开始时间
	 */
	public void setOpenBtime(Time openBtime) {
		this.openBtime = openBtime;
	}
	/**
	 * 获取：营业开始时间
	 */
	public Time getOpenBtime() {
		return openBtime;
	}
	/**
	 * 设置：营业结束时间
	 */
	public void setOpenEtime(Time openEtime) {
		this.openEtime = openEtime;
	}
	/**
	 * 获取：营业结束时间
	 */
	public Time getOpenEtime() {
		return openEtime;
	}
	/**
	 * 设置：门店介绍
	 */
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	/**
	 * 获取：门店介绍
	 */
	public String getIntroduce() {
		return introduce;
	}
	/**
	 * 设置：商家资质
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	/**
	 * 获取：商家资质
	 */
	public String getQualification() {
		return qualification;
	}
	/**
	 * 设置：点餐开关 1开
	 */
	public void setIsOrder(String isOrder) {
		this.isOrder = isOrder;
	}
	/**
	 * 获取：点餐开关 1开
	 */
	public String getIsOrder() {
		return isOrder;
	}
	/**
	 * 设置：预订开关 1开
	 */
	public void setIsReservation(String isReservation) {
		this.isReservation = isReservation;
	}
	/**
	 * 获取：预订开关 1开
	 */
	public String getIsReservation() {
		return isReservation;
	}
	/**
	 * 设置：外卖开关 1开
	 */
	public void setIsTakeaway(String isTakeaway) {
		this.isTakeaway = isTakeaway;
	}
	/**
	 * 获取：外卖开关 1开
	 */
	public String getIsTakeaway() {
		return isTakeaway;
	}
	public List<RestaurantPictureInfo> getPictureList() {
		return pictureList;
	}
	public void setPictureList(List<RestaurantPictureInfo> pictureList) {
		this.pictureList = pictureList;
	}
}
