package com.github.wxiaoqi.security.biz.modules.restaurant.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 菜品评价明细表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Table(name = "restaurant_appraisal_detail_info")
public class RestaurantAppraisalDetailInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//用户账号
	@Column(name = "per_id")
	private String perId;
	
	//支付订单号
	@Column(name = "pay_order_no")
	private String payOrderNo;

	//菜品id
	@Column(name = "dishes_id")
	private String dishesId;

	//用户名
	@Column(name = "user_name")
	private String userName;

	//用户头像
	@Column(name = "user_head_portrait")
	private String userHeadPortrait;

	//评价等级
	@Column(name = "appraisal_grade")
	private String appraisalGrade;

	//评价内容
	@Column(name = "appraisal_info")
	private String appraisalInfo;

	//评论时间
	@Column(name = "appraisal_time")
	private Date appraisalTime;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：用户账号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：用户账号
	 */
	public String getPerId() {
		return perId;
	}
	public String getPayOrderNo() {
		return payOrderNo;
	}
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	/**
	 * 设置：菜品id
	 */
	public void setDishesId(String dishesId) {
		this.dishesId = dishesId;
	}
	/**
	 * 获取：菜品id
	 */
	public String getDishesId() {
		return dishesId;
	}
	/**
	 * 设置：用户名
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * 获取：用户名
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * 设置：用户头像
	 */
	public void setUserHeadPortrait(String userHeadPortrait) {
		this.userHeadPortrait = userHeadPortrait;
	}
	/**
	 * 获取：用户头像
	 */
	public String getUserHeadPortrait() {
		return userHeadPortrait;
	}
	/**
	 * 设置：评价等级
	 */
	public void setAppraisalGrade(String appraisalGrade) {
		this.appraisalGrade = appraisalGrade;
	}
	/**
	 * 获取：评价等级
	 */
	public String getAppraisalGrade() {
		return appraisalGrade;
	}
	/**
	 * 设置：评价内容
	 */
	public void setAppraisalInfo(String appraisalInfo) {
		this.appraisalInfo = appraisalInfo;
	}
	/**
	 * 获取：评价内容
	 */
	public String getAppraisalInfo() {
		return appraisalInfo;
	}
	/**
	 * 设置：评论时间
	 */
	public void setAppraisalTime(Date appraisalTime) {
		this.appraisalTime = appraisalTime;
	}
	/**
	 * 获取：评论时间
	 */
	public Date getAppraisalTime() {
		return appraisalTime;
	}
}
