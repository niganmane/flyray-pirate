package com.github.wxiaoqi.security.biz.modules.auction.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.auction.biz.AuctionGoodsInfoBiz;
import com.github.wxiaoqi.security.biz.modules.auction.entity.AuctionGoodsInfo;
import com.github.wxiaoqi.security.common.cms.request.AuctionQueryParam;
import com.github.wxiaoqi.security.common.cms.request.AuctionRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序竞拍商品相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("auction/goods")
public class AuctionGoodsRestController extends BaseController<AuctionGoodsInfoBiz, AuctionGoodsInfo> {
	
	@Autowired
	private AuctionGoodsInfoBiz auctionGoodsInfoBiz;
	
	/**
	 * 查询竞拍商品列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<AuctionGoodsInfo> query(@RequestBody AuctionQueryParam param) {
		log.info("查询竞拍商品列表------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return auctionGoodsInfoBiz.queryAuctionGoodsPage(param);
	}
	
	/**
	 * 添加竞拍商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody AuctionRequestParam param) {
		log.info("添加竞拍商品------start------{}", param);
		Map<String, Object> respMap = auctionGoodsInfoBiz.addGoodsInfo(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("添加竞拍商品-----end-------------{}");
		return respMap;
	}
	
	/**
	 * 删除商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody AuctionRequestParam param) {
		log.info("删除商品------start------{}", param);
		Map<String, Object> respMap = auctionGoodsInfoBiz.deleteGoodsInfo(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除商品------end------{}", respMap);
		return respMap;
	}
	
	/**
	 * 修改商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody AuctionRequestParam param) {
		log.info("修改商品------start------{}", param);
		Map<String, Object> respMap = auctionGoodsInfoBiz.updateGoodsInfo(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("修改商品------end------{}", respMap);
		return respMap;
	}

}
