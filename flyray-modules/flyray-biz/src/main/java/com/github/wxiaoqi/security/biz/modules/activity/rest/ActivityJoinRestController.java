package com.github.wxiaoqi.security.biz.modules.activity.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.activity.biz.ActivityCustomerBiz;
import com.github.wxiaoqi.security.biz.modules.activity.entity.ActivityCustomer;
import com.github.wxiaoqi.security.common.cms.request.CmsQueryActivityJoinParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序活动相关接口
 * @author Administrator
 *
 */
@Slf4j
@Controller
@RequestMapping("activity/join")
public class ActivityJoinRestController extends BaseController<ActivityCustomerBiz, ActivityCustomer> {

	/**
	 * 列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<ActivityCustomer> query(@RequestBody CmsQueryActivityJoinParam  param) {
		log.info("查询活动参加记录------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return baseBiz.queryActivityJoin(param);
	}
	
}
