/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.github.wxiaoqi.security.biz.codex.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.ScheduleJobLog;
import com.github.wxiaoqi.security.biz.codex.mapper.ScheduleJobLogMapper;
import com.github.wxiaoqi.security.biz.codex.service.ScheduleJobLogSchemaTaskService;

@Service("scheduleJobLogService")
public class ScheduleJobLogSchemaTaskServiceImpl implements ScheduleJobLogSchemaTaskService {
	
	@Autowired
	private ScheduleJobLogMapper scheduleJobLogMapper;


	@Override
	public List<ScheduleJobLog> queryLogList(Map<String, Object> map) {
		return scheduleJobLogMapper.queryLogList(map);
	}

	@Override
	public void addLog(ScheduleJobLog entity) {
		scheduleJobLogMapper.addLog(entity);
	}

}
