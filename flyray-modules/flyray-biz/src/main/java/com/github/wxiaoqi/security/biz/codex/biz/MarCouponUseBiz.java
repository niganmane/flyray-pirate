package com.github.wxiaoqi.security.biz.codex.biz;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.MarCouponUse;
import com.github.wxiaoqi.security.biz.codex.entity.MarTotalInfo;
import com.github.wxiaoqi.security.biz.codex.mapper.MarCouponUseMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 红包使用表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Service
public class MarCouponUseBiz extends BaseBiz<MarCouponUseMapper,MarCouponUse> {
	
	private static final Logger logger = LoggerFactory.getLogger(MarCouponUseBiz.class);

	@Autowired
	private MarCouponUseMapper marCouponUseMapper;

	
	/**
	 * 根据条件查询红包使用列表
	 * @author centerroot
	 * @time 创建时间:2018年3月19日下午5:45:42
	 * @param param
	 * @return
	 */
	List<MarCouponUse> queryMarCouponUseList(Map<String, Object> param){
		return marCouponUseMapper.queryMarCouponUseList(param);
	}

	/**
	 * 根据条件统计红包使用记录数和统计金额
	 * @author centerroot
	 * @time 创建时间:2018年3月19日下午5:46:17
	 * @param param
	 * @return
	 */
	List<MarTotalInfo> queryMarCouponUseTotalAmt(Map<String, Object> param){
		return marCouponUseMapper.queryMarCouponUseTotalAmt(param);
	}
	
	

}