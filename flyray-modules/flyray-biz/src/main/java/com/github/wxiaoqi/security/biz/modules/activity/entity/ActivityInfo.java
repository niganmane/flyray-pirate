package com.github.wxiaoqi.security.biz.modules.activity.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.github.wxiaoqi.security.biz.modules.comment.entity.Comment;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-19 10:54:45
 */
@Table(name = "cms_activity_info")
public class ActivityInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//序号
    @Id
    private String id;
	
	//活动名称
    @Column(name = "activity_name")
    private String activityName;
	
	//活动logo
    @Column(name = "activity_logo")
    private String activityLogo;
	
	//活动摘要
    @Column(name = "activity_des")
    private String activityDes;
	
	//活动内容
    @Column(name = "activity_content")
    private String activityContent;
	
	//活动开始时间
    @Column(name = "activity_start_time")
    private Date activityStartTime;
	
	//活动结束时间
    @Column(name = "activity_end_time")
    private Date activityEndTime;
	
	//活动地点
    @Column(name = "activity_addr")
    private String activityAddr;
	
	//创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	//商户号
    @Column(name = "merchant_id")
    private String merchantId;
	
	//平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	//封面
    @Column(name = "cover_img")
    private String coverImg;
	
	//二维码图片
    @Column(name = "qr_img")
    private String qrImg;
	
	//发布者姓名
    @Column(name = "publisher_name")
    private String publisherName;
	
	//联系方式     0微信号 1QQ号 2手机号
    @Column(name = "publisher_contactWay")
    private Integer publisherContactway;
	
	//联系号码
    @Column(name = "publisher_contactValue")
    private String publisherContactvalue;
	
	//评论数量
    @Column(name = "comment_count")
    private Integer commentCount;

    //活动类型名称
    @Column(name = "acttype_name")
    private String acttypeName;
	
	//发起状态 0准备中 1进行中 2结束
    @Column(name = "initiation_status")
    private Integer initiationStatus;
	
	//人数限制
    @Column(name = "people_num")
    private Integer peopleNum;
    
    //活动类型
    @Column(name = "acttype")
    private Integer acttype;
    
    //参加状态 0未加入 1已加入 3本人发起
    @Transient
    private String join;
    
    //已参加人数
    @Transient
    private Integer joinnumber;
    
    
    @Transient
    private List<ActivityCustomer> joinList;
    
    @Transient
    private List<Comment> comments;
    
	public String getJoin() {
		return join;
	}
	public void setJoin(String join) {
		this.join = join;
	}
	/**
	 * 设置：序号
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：活动名称
	 */
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	/**
	 * 获取：活动名称
	 */
	public String getActivityName() {
		return activityName;
	}
	/**
	 * 设置：活动logo
	 */
	public void setActivityLogo(String activityLogo) {
		this.activityLogo = activityLogo;
	}
	/**
	 * 获取：活动logo
	 */
	public String getActivityLogo() {
		return activityLogo;
	}
	/**
	 * 设置：活动摘要
	 */
	public void setActivityDes(String activityDes) {
		this.activityDes = activityDes;
	}
	/**
	 * 获取：活动摘要
	 */
	public String getActivityDes() {
		return activityDes;
	}
	/**
	 * 设置：活动内容
	 */
	public void setActivityContent(String activityContent) {
		this.activityContent = activityContent;
	}
	/**
	 * 获取：活动内容
	 */
	public String getActivityContent() {
		return activityContent;
	}
	/**
	 * 设置：活动开始时间
	 */
	public void setActivityStartTime(Date activityStartTime) {
		this.activityStartTime = activityStartTime;
	}
	/**
	 * 获取：活动开始时间
	 */
	public Date getActivityStartTime() {
		return activityStartTime;
	}
	/**
	 * 设置：活动结束时间
	 */
	public void setActivityEndTime(Date activityEndTime) {
		this.activityEndTime = activityEndTime;
	}
	/**
	 * 获取：活动结束时间
	 */
	public Date getActivityEndTime() {
		return activityEndTime;
	}
	/**
	 * 设置：活动地点
	 */
	public void setActivityAddr(String activityAddr) {
		this.activityAddr = activityAddr;
	}
	/**
	 * 获取：活动地点
	 */
	public String getActivityAddr() {
		return activityAddr;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：商户号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：封面
	 */
	public void setCoverImg(String coverImg) {
		this.coverImg = coverImg;
	}
	/**
	 * 获取：封面
	 */
	public String getCoverImg() {
		return coverImg;
	}
	/**
	 * 设置：二维码图片
	 */
	public void setQrImg(String qrImg) {
		this.qrImg = qrImg;
	}
	/**
	 * 获取：二维码图片
	 */
	public String getQrImg() {
		return qrImg;
	}
	/**
	 * 设置：发布者姓名
	 */
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}
	/**
	 * 获取：发布者姓名
	 */
	public String getPublisherName() {
		return publisherName;
	}
	/**
	 * 设置：联系方式     0微信号 1QQ号 2手机号
	 */
	public void setPublisherContactway(Integer publisherContactway) {
		this.publisherContactway = publisherContactway;
	}
	/**
	 * 获取：联系方式     0微信号 1QQ号 2手机号
	 */
	public Integer getPublisherContactway() {
		return publisherContactway;
	}
	/**
	 * 设置：联系号码
	 */
	public void setPublisherContactvalue(String publisherContactvalue) {
		this.publisherContactvalue = publisherContactvalue;
	}
	/**
	 * 获取：联系号码
	 */
	public String getPublisherContactvalue() {
		return publisherContactvalue;
	}
	/**
	 * 设置：评论数量
	 */
	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}
	/**
	 * 获取：评论数量
	 */
	public Integer getCommentCount() {
		return commentCount;
	}
	/**
	 * 设置：活动类型名称
	 */
	public void setActtypeName(String acttypeName) {
		this.acttypeName = acttypeName;
	}
	/**
	 * 获取：活动类型名称
	 */
	public String getActtypeName() {
		return acttypeName;
	}
	/**
	 * 设置：发起状态 0准备中 1进行中 2结束
	 */
	public void setInitiationStatus(Integer initiationStatus) {
		this.initiationStatus = initiationStatus;
	}
	/**
	 * 获取：发起状态 0准备中 1进行中 2结束
	 */
	public Integer getInitiationStatus() {
		return initiationStatus;
	}
	/**
	 * 设置：人数限制
	 */
	public void setPeopleNum(Integer peopleNum) {
		this.peopleNum = peopleNum;
	}
	/**
	 * 获取：人数限制
	 */
	public Integer getPeopleNum() {
		return peopleNum;
	}
	public Integer getActtype() {
		return acttype;
	}
	public void setActtype(Integer acttype) {
		this.acttype = acttype;
	}
	public Integer getJoinnumber() {
		return joinnumber;
	}
	public void setJoinnumber(Integer joinnumber) {
		this.joinnumber = joinnumber;
	}
	public List<ActivityCustomer> getJoinList() {
		return joinList;
	}
	public void setJoinList(List<ActivityCustomer> joinList) {
		this.joinList = joinList;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	@Override
	public String toString() {
		return "ActivityInfo [id=" + id + ", activityName=" + activityName + ", activityLogo=" + activityLogo
				+ ", activityDes=" + activityDes + ", activityContent=" + activityContent + ", activityStartTime="
				+ activityStartTime + ", activityEndTime=" + activityEndTime + ", activityAddr=" + activityAddr
				+ ", createTime=" + createTime + ", merchantId="
				+ merchantId + ", platformId=" + platformId + ", coverImg=" + coverImg + ", qrImg=" + qrImg
				+ ", publisherName=" + publisherName + ", publisherContactway=" + publisherContactway
				+ ", publisherContactvalue=" + publisherContactvalue + ", commentCount=" + commentCount
				+ ", acttypeName=" + acttypeName + ", initiationStatus="
				+ initiationStatus + ", peopleNum=" + peopleNum + ", acttype=" + acttype + ", join=" + join
				+ ", joinnumber=" + joinnumber + ", joinList=" + joinList + ", comments=" + comments + "]";
	}
}
