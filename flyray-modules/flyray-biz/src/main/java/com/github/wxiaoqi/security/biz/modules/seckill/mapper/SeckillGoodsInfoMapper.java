package com.github.wxiaoqi.security.biz.modules.seckill.mapper;

import com.github.wxiaoqi.security.biz.modules.seckill.entity.SeckillGoodsInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 秒杀商品信息表
 * @author he
 * @date 2018-09-03 17:42:34
 */
@org.apache.ibatis.annotations.Mapper
public interface SeckillGoodsInfoMapper extends Mapper<SeckillGoodsInfo> {
	
}
