package com.github.wxiaoqi.security.biz.modules.community.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.community.entity.CommunitySociety;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:43
 */
@org.apache.ibatis.annotations.Mapper
public interface CommunitySocietyMapper extends Mapper<CommunitySociety> {
	
	List<CommunitySociety> querySocieties(Map<String, Object> map);
}
