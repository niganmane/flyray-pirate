package com.github.wxiaoqi.security.biz.modules.auction.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.auction.biz.AuctionRecordingInfoBiz;
import com.github.wxiaoqi.security.biz.modules.auction.entity.AuctionRecordingInfo;
import com.github.wxiaoqi.security.common.cms.request.AuctionRecordingQueryParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序竞拍记录相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("auction/recording")
public class AuctionRecordingRestController extends BaseController<AuctionRecordingInfoBiz, AuctionRecordingInfo> {
	
	@Autowired
	private AuctionRecordingInfoBiz auctionRecordingInfoBiz;
	
	/**
	 * 查询竞拍记录列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<AuctionRecordingInfo> query(@RequestBody AuctionRecordingQueryParam param) {
		log.info("查询竞拍记录列表------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return auctionRecordingInfoBiz.queryAuctionRecordingPage(param);
	}

}
