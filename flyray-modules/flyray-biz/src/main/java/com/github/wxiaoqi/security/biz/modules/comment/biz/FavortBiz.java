package com.github.wxiaoqi.security.biz.modules.comment.biz;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.modules.comment.entity.Favort;
import com.github.wxiaoqi.security.biz.modules.comment.mapper.FavortMapper;
import com.github.wxiaoqi.security.biz.modules.community.entity.CommunityViewpoint;
import com.github.wxiaoqi.security.biz.modules.community.mapper.CommunityViewpointMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.CmsSaveFavortParam;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 
 *
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:44
 */
@Slf4j
@Service
public class FavortBiz extends BaseBiz<FavortMapper,Favort> {
	
	@Autowired
	private CommunityViewpointMapper viewpointMapper;
	
	public Map<String, Object> doFavort(CmsSaveFavortParam param) {
		log.info("点赞或取消....start...{}", param);
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String customerId = (String) param.getCustomerId();
			Integer favortStatus = param.getFavortStatus();
			String pointId = (String) param.getPointId();
			String merchantId = (String) param.getMerchantId();
			String platformId = (String) param.getPlatformId();
			//根据观点编号查询出观点，然后点赞数量+1
			CommunityViewpoint point = viewpointMapper.selectByPrimaryKey(pointId);
			Example example = new Example(Favort.class);
			Criteria criteria = example.createCriteria();
			criteria.andEqualTo("customerId", customerId);
			criteria.andEqualTo("pointId", pointId);
			criteria.andEqualTo("merchantId", merchantId);
			criteria.andEqualTo("platformId", platformId);
			List<Favort> favortList = mapper.selectByExample(example);
			Favort favort = new Favort();
			log.info("点赞或取消....favortList.size...{}", favortList.size());
			log.info("点赞或取消....favortStatus...{}", favortStatus);
			if (null != favortList && favortList.size() > 0){
				//说明已经点赞过--更新
				favort = favortList.get(0);
				if(favort.getFavortStatus() == 1){
					favort.setFavortStatus(2);
					Integer count = point.getFavortCount();
					count = count - 1;
					point.setFavortCount(count);
					viewpointMapper.updateByPrimaryKeySelective(point);
				}else if(favort.getFavortStatus() == 2){
					favort.setFavortStatus(1);
					Integer count = point.getFavortCount();
					count = count + 1;
					point.setFavortCount(count);
					viewpointMapper.updateByPrimaryKeySelective(point);
				}
				favort.setUpdateTime(new Timestamp(System.currentTimeMillis()));
				mapper.updateByPrimaryKeySelective(favort);
			} else {
				//没有点赞过--插入
				favort.setId(String.valueOf(SnowFlake.getId()));
				favort.setFavortStatus(1);
				Integer count = point.getFavortCount();
				count = count + 1;
				point.setFavortCount(count);
				viewpointMapper.updateByPrimaryKeySelective(point);
				favort.setPlatformId(platformId);
				favort.setMerchantId(merchantId);
				favort.setCustomerId(customerId);
				favort.setPointId(pointId);
				favort.setNickName(param.getNickName());
				favort.setAvatarUrl(param.getAvatarUrl());
				favort.setCreateTime(new Timestamp(System.currentTimeMillis()));
				favort.setUpdateTime(new Timestamp(System.currentTimeMillis()));
				mapper.insert(favort);
			}
			result.put("favort", favort);
			result.put("code", "00");
			log.info("点赞或取消....result...{}", result);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "01");
		}
		
		return result;
	}
}