package com.github.wxiaoqi.security.biz.modules.restaurant.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Id;

public class RestaurantNoConfirmOrderInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	private String platformId;

	//商户账号
	private String merchantId;

	//用户账号
	private String customerId;

	//支付订单号
	private String payOrderNo;

	//金额
	private BigDecimal price;

	//支付时间
	private Date payTime;

	//状态 00支付成功 01支付失败 02退款成功 03退款失败 04待支付
	private String status;

	//配送姓名
	private String distributionName;

	//配送地址
	private String distributionAddress;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getPayOrderNo() {
		return payOrderNo;
	}

	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getPayTime() {
		return payTime;
	}

	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDistributionName() {
		return distributionName;
	}

	public void setDistributionName(String distributionName) {
		this.distributionName = distributionName;
	}

	public String getDistributionAddress() {
		return distributionAddress;
	}

	public void setDistributionAddress(String distributionAddress) {
		this.distributionAddress = distributionAddress;
	}

}
