package com.github.wxiaoqi.security.biz.modules.restaurant.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantAppraisalInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 菜品评价信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantAppraisalInfoMapper extends Mapper<RestaurantAppraisalInfo> {
	
	List<RestaurantAppraisalInfo> queryRestaurantAppraisalInfo(Map<String, Object> map);
	
}
