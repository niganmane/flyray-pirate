package com.github.wxiaoqi.security.biz.modules.restaurant.entity;

import java.io.Serializable;

import javax.persistence.Id;

/**
 * 购物车菜品详情
 * @author he
 *
 */
public class RestaurantShoppingDishesInfo implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	private String platformId;

	//商户账号
	private String merchantId;

	//用户账号
	private String perId;

	//支付订单号
	private String payOrderNo;

	//餐桌id
	private String tableId;

	//菜品id
	private String dishesId;

	//菜品名称
	private String dishesName;

	//数量
	private Integer dishesNum;

	//总价
	private String dishesPrice;

	//状态 1已选择 2已提交 3已支付
	private String status;

	//类型 1点餐 2外卖 3预约
	private String type;

	//是否评价 0：已评价 1：未评价
	private String isAppraisal;

	//类目id
	private Integer categoryId;

	//说明
	private String instructions;

	//图片地址
	private String imageUrl;

	//预定数量
	private Integer orderNum;

	//价格
	private String price;

	//规格开关
	private String isSpecification;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getPerId() {
		return perId;
	}

	public void setPerId(String perId) {
		this.perId = perId;
	}

	public String getPayOrderNo() {
		return payOrderNo;
	}

	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getDishesId() {
		return dishesId;
	}

	public void setDishesId(String dishesId) {
		this.dishesId = dishesId;
	}

	public String getDishesName() {
		return dishesName;
	}

	public void setDishesName(String dishesName) {
		this.dishesName = dishesName;
	}

	public Integer getDishesNum() {
		return dishesNum;
	}

	public void setDishesNum(Integer dishesNum) {
		this.dishesNum = dishesNum;
	}

	public String getDishesPrice() {
		return dishesPrice;
	}

	public void setDishesPrice(String dishesPrice) {
		this.dishesPrice = dishesPrice;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIsAppraisal() {
		return isAppraisal;
	}

	public void setIsAppraisal(String isAppraisal) {
		this.isAppraisal = isAppraisal;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getIsSpecification() {
		return isSpecification;
	}

	public void setIsSpecification(String isSpecification) {
		this.isSpecification = isSpecification;
	}

}
