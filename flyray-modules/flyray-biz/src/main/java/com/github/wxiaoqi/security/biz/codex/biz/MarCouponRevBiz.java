package com.github.wxiaoqi.security.biz.codex.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.MarCouponRev;
import com.github.wxiaoqi.security.biz.codex.mapper.MarCouponRevMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 红包领取表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Service
public class MarCouponRevBiz extends BaseBiz<MarCouponRevMapper,MarCouponRev> {
}