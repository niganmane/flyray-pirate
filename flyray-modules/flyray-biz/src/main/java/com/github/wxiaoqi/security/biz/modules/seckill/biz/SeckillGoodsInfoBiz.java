package com.github.wxiaoqi.security.biz.modules.seckill.biz;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.wxiaoqi.security.biz.modules.seckill.entity.SeckillGoodsInfo;
import com.github.wxiaoqi.security.biz.modules.seckill.mapper.SeckillGoodsInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

import lombok.extern.slf4j.Slf4j;

/**
 * 秒杀商品信息表
 * @author he
 * @date 2018-09-03 17:42:34
 */
@Slf4j
@Service
public class SeckillGoodsInfoBiz extends BaseBiz<SeckillGoodsInfoMapper,SeckillGoodsInfo> {
	
	@Autowired
	private SeckillGoodsInfoMapper seckillGoodsInfoMapper;
	
	/**
	 * 秒杀商品信息查询
	 * @param request
	 * @return
	 */
	public List<SeckillGoodsInfo> queryGoodsInfo(Map<String, Object> request){
		log.info("秒杀商品信息查询请求。。。。。。{}", request);
		String platformId = (String) request.get("platformId");
		String merchantId = (String) request.get("merchantId");
		SeckillGoodsInfo seckillGoodsInfo = new SeckillGoodsInfo();
		seckillGoodsInfo.setPlatformId(platformId);
		if(!StringUtils.isEmpty(merchantId)){
			seckillGoodsInfo.setMerchantId(merchantId);
		}
		List<SeckillGoodsInfo> selectGoodsInfo = seckillGoodsInfoMapper.select(seckillGoodsInfo);
		log.info("秒杀商品信息查询响应。。。。。。{}", selectGoodsInfo);
		return selectGoodsInfo;
	}
	
	/**
	 * 秒杀商品详情查询
	 * @param request
	 * @return
	 */
	public SeckillGoodsInfo queryGoodsdetailInfo(Map<String, Object> request){
		log.info("秒杀商品详情查询请求。。。。。。{}", request);
		String platformId = (String) request.get("platformId");
		String merchantId = (String) request.get("merchantId");
		String goodsId = (String) request.get("goodsId");
		SeckillGoodsInfo seckillGoodsInfo = new SeckillGoodsInfo();
		seckillGoodsInfo.setPlatformId(platformId);
		if(!StringUtils.isEmpty(merchantId)){
			seckillGoodsInfo.setMerchantId(merchantId);
		}
		seckillGoodsInfo.setGoodsId(goodsId);
		SeckillGoodsInfo selectGoodsInfo = seckillGoodsInfoMapper.selectOne(seckillGoodsInfo);
		log.info("秒杀商品详情查询响应。。。。。。{}", selectGoodsInfo);
		return selectGoodsInfo;
	}
}