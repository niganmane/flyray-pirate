package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 红包使用表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Table(name = "mar_coupon_use")
public class MarCouponUse implements Serializable {
	private static final long serialVersionUID = 1L;

	//
	@Id
	private Long seqNo;

	// 平台编号
	@Column(name = "platform_id")
	private String platformId;

	// 会员号
	@Column(name = "personal_id")
	private String personalId;
	
	// 商户号
	@Column(name = "merchant_id")
	private String merchantId;

	// 红包流水号
	@Column(name = "COU_SERIAL")
	private String couSerial;

	// 红包编号
	@Column(name = "COU_ID")
	private String couId;

	// 使用时间
	@Column(name = "USE_TIME")
	private Date useTime;

	// 场景ID
	@Column(name = "SCENE_ID")
	private String sceneId;

	// 活动id
	@Column(name = "ACT_ID")
	private String actId;

	// 事件编号
	@Column(name = "INC_CODE")
	private String incCode;

	// 支付订单号
	@Column(name = "MERORDERNO")
	private String merorderno;

	// 订单状态 00已支付 01已退款
	@Column(name = "TXSTATUS")
	private String txstatus;

	// 抵扣金额
	@Column(name = "COUPON_AMT")
	private BigDecimal couponAmt;

	
	
	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	/**
	 * 设置：
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}

	/**
	 * 获取：
	 */
	public Long getSeqNo() {
		return seqNo;
	}

	/**
	 * 设置：红包流水号
	 */
	public void setCouSerial(String couSerial) {
		this.couSerial = couSerial;
	}

	/**
	 * 获取：红包流水号
	 */
	public String getCouSerial() {
		return couSerial;
	}

	/**
	 * 设置：红包编号
	 */
	public void setCouId(String couId) {
		this.couId = couId;
	}

	/**
	 * 获取：红包编号
	 */
	public String getCouId() {
		return couId;
	}

	/**
	 * 设置：使用时间
	 */
	public void setUseTime(Date useTime) {
		this.useTime = useTime;
	}

	/**
	 * 获取：使用时间
	 */
	public Date getUseTime() {
		return useTime;
	}

	/**
	 * 设置：场景ID
	 */
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}

	/**
	 * 获取：场景ID
	 */
	public String getSceneId() {
		return sceneId;
	}

	/**
	 * 设置：活动id
	 */
	public void setActId(String actId) {
		this.actId = actId;
	}

	/**
	 * 获取：活动id
	 */
	public String getActId() {
		return actId;
	}

	/**
	 * 设置：事件编号
	 */
	public void setIncCode(String incCode) {
		this.incCode = incCode;
	}

	/**
	 * 获取：事件编号
	 */
	public String getIncCode() {
		return incCode;
	}

	/**
	 * 设置：支付订单号
	 */
	public void setMerorderno(String merorderno) {
		this.merorderno = merorderno;
	}

	/**
	 * 获取：支付订单号
	 */
	public String getMerorderno() {
		return merorderno;
	}

	/**
	 * 设置：订单状态 00已支付 01已退款
	 */
	public void setTxstatus(String txstatus) {
		this.txstatus = txstatus;
	}

	/**
	 * 获取：订单状态 00已支付 01已退款
	 */
	public String getTxstatus() {
		return txstatus;
	}

	/**
	 * 设置：抵扣金额
	 */
	public void setCouponAmt(BigDecimal couponAmt) {
		this.couponAmt = couponAmt;
	}

	/**
	 * 获取：抵扣金额
	 */
	public BigDecimal getCouponAmt() {
		return couponAmt;
	}
}
