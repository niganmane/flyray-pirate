package com.github.wxiaoqi.security.biz.codex.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.codex.entity.MarDiscountBalanceFile;
import com.github.wxiaoqi.security.biz.codex.entity.MarTotalInfo;

/**
 * 营销引擎混合查询
 * @author centerroot
 * @time 创建时间:2018年3月23日下午3:52:39
 * @description
 */
public interface MarCommonMapper {
	
	/**
	 * 根据条件查询折扣优惠记录列表
	 * @author centerroot
	 * @time 创建时间:2018年3月23日下午3:54:26
	 * @param param
	 * @return
	 */
	List<MarDiscountBalanceFile> queryMarDiscountList(Map<String, Object> param);

	/**
	 * 根据统计条件查询折扣优惠记录
	 * @author centerroot
	 * @time 创建时间:2018年3月23日下午3:54:20
	 * @param param
	 * @return
	 */
	List<MarTotalInfo> queryMarDiscountTotalAmt(Map<String, Object> param);
}