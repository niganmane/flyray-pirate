package com.github.wxiaoqi.security.biz.codex.mapper;

import com.github.wxiaoqi.security.biz.codex.entity.PersonalCouponRelation;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalCouponRelationMapper extends Mapper<PersonalCouponRelation> {
	/**
	 * 状态置为可领取
	 */
	void changStatusToOpen();
	/**
	 * 状态置为不可领取
	 */
	void changStatusToClose();
}
