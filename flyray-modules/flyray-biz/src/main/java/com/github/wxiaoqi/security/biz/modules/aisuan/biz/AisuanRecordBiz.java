package com.github.wxiaoqi.security.biz.modules.aisuan.biz;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanRecord;
import com.github.wxiaoqi.security.biz.modules.aisuan.mapper.AisuanRecordMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.AisuanNoCompleteRecordQueryParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordAddParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordQueryAdminParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordQueryOneParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordQueryParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordReplyParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.FlyrayBeanUtils;
import com.github.wxiaoqi.security.common.util.ResponseHelper;
import com.github.wxiaoqi.security.common.util.SnowFlake;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 
 *
 * @author 
 * @email ${email}
 * @date 2018-09-10 10:58:58
 */
@Service
@Slf4j
@Transactional
public class AisuanRecordBiz extends BaseBiz<AisuanRecordMapper,AisuanRecord> {
	public void save(AisuanRecordAddParam param) throws Exception {
		AisuanRecord record = new AisuanRecord();
		BeanUtils.copyProperties(record, param);
		record.setId(SnowFlake.getId());
		record.setParentId(Long.valueOf("0"));
		record.setTopId(Long.valueOf("0"));
		record.setParentId(Long.valueOf("0"));
		record.setTopId(Long.valueOf("0"));
		record.setCreateTime(new Date());
		record.setPayStatus(1);
		record.setIsTarget(1);
		record.setFirstLevel(5);
		record.setOrderStatus(1);
		mapper.insert(record);
	}
	public List<AisuanRecord> query(AisuanRecordQueryParam param) {
		log.info("分页查询爱算记录。。。。{}"+param);
		Example example = new Example(AisuanRecord.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if (param.getRecordId() != null) {
			criteria.andEqualTo("id",param.getRecordId());
		}
		if(param.getCustomerId() != null){
			criteria.andEqualTo("customerId",param.getCustomerId());
		}
		if(param.getTopId() != null){
			criteria.andEqualTo("topId",param.getTopId());
		}
		if(param.getFirstLevel() != null){
			criteria.andEqualTo("firstLevel",param.getFirstLevel());
		}
		example.setOrderByClause("create_time desc");
		List<AisuanRecord> list = mapper.selectByExample(example);
		return list;
	}
	/**
	 * 咨询方回复，状态变为未回复
	 * @param param
	 * @throws Exception
	 */
	public void ask(AisuanRecordReplyParam param) throws Exception{
		//修改咨询状态
		AisuanRecord oriRecord = new AisuanRecord();
		oriRecord.setId(param.getTopId());
		oriRecord.setOrderStatus(1);
		mapper.updateByPrimaryKeySelective(oriRecord);
		//
		AisuanRecord record = new AisuanRecord();
		BeanUtils.copyProperties(record, param);
		record.setOrderStatus(3);
		record.setCreateTime(new Date());
		record.setId(SnowFlake.getId());
		mapper.insert(record);
	}
	/**
	 * 被咨询方回复，状态变为已回复
	 * @param param
	 * @throws Exception
	 */
	public void reply(AisuanRecordReplyParam param) throws Exception{
		//修改咨询状态
		AisuanRecord oriRecord = new AisuanRecord();
		oriRecord.setId(param.getTopId());
		oriRecord.setOrderStatus(2);
		mapper.updateByPrimaryKeySelective(oriRecord);
		//
		AisuanRecord record = new AisuanRecord();
		BeanUtils.copyProperties(record, param);
		record.setOrderStatus(3);
		record.setCreateTime(new Date());
		record.setId(SnowFlake.getId());
		mapper.insert(record);
	}
	public TableResultResponse<AisuanRecord> queryListPage(AisuanRecordQueryAdminParam param) {
		try {
			Page<AisuanRecord> result = PageHelper.startPage(param.getPage(), param.getLimit());
			param.setTopId(Long.valueOf("0"));
			
			if(param.getUserType() == 1){
				//系统管理员 --- 查询全部的去掉platformId
				param.setPlatformId(null);
				param.setMerchantId(null);
			}
			List<AisuanRecord> list = mapper.queryList(FlyrayBeanUtils.objectToMap(param));
			
			TableResultResponse<AisuanRecord> table = new TableResultResponse<>(result.getTotal(), list);
			return table;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
				
	}
	public List<AisuanRecord> queryReplyRecord(AisuanRecordQueryOneParam param){
		try {
			List<AisuanRecord> list = mapper.queryApplyList(FlyrayBeanUtils.objectToMap(param));
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public Map<String, Object> complete(@RequestBody AisuanRecordQueryOneParam param){
		AisuanRecord record = new AisuanRecord();
		record.setId(param.getId());
		record.setOrderStatus(3);
		mapper.updateByPrimaryKeySelective(record);
		return ResponseHelper.success(record, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	/**
	 * 判断当前用户是否有选定咨询人未完成订单
	 */
	public Map<String, Object> queryNoCompleteRecord(AisuanNoCompleteRecordQueryParam param){
		Example example = new Example(AisuanRecord.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("customerId", param.getCustomerId());
		criteria.andEqualTo("targetTea",param.getTargetTea());
		criteria.andNotEqualTo("orderStatus", 3);
		List<AisuanRecord> list = mapper.selectByExample(example);
		Map<String, Object> result = new HashMap<String, Object>();
		if(list.size() == 0){
			result.put("status", true);
			return ResponseHelper.success(result, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			//取出未完成
			//AisuanRecord record = list.get(0);
			List<List<AisuanRecord>> allList = new ArrayList<List<AisuanRecord>>();
			for (AisuanRecord record : list) {
				Map<String, Object> map = new HashMap<>();
				map.put("topId", record.getId());
				List<AisuanRecord> reList = mapper.queryAskAndApplyList(map);
				allList.add(reList);
			}
			
			result.put("status", false);
			result.put("body", allList);
			return ResponseHelper.success(result, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}
	}
	/**
	 * 查询一条咨询记录以及全部回复
	 */
	public Map<String, Object> queryOneRecordAskAndReply(AisuanRecordQueryOneParam param) {
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> result = new HashMap<>();
		map.put("topId", param.getId());
		List<AisuanRecord> reList = mapper.queryAskAndApplyList(map);
		result.put("body", reList);
		result.put("status", false);
		return ResponseHelper.success(result, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
}