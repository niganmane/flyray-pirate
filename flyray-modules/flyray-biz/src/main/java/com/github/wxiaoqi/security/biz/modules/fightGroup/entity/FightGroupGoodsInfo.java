package com.github.wxiaoqi.security.biz.modules.fightGroup.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * 拼团商品信息
 * 
 * @author he
 * @date 2018-07-10 09:35:10
 */
@Table(name = "fight_group_goods_info")
public class FightGroupGoodsInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	
	//平台编号
	@Column(name = "platform_id")
	private String platformId;
	
	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;
	
	//商品id
	@Column(name = "goods_id")
	private String goodsId;

	//商品名称
	@Column(name = "goods_name")
	private String goodsName;

	//说明
	@Column(name = "goods_description")
	private String goodsDescription;

	//商品图片
	@Column(name = "goods_img")
	private String goodsImg;

	//原价
	@Column(name = "original_price")
	private BigDecimal originalPrice;

	//折扣价
	@Column(name = "discount_price")
	private BigDecimal discountPrice;
	
	//已售数量
	@Column(name = "sold_num")
	private Integer soldNum;
	
	//有效时间说明
	@Column(name = "effective_time_description")
	private String effectiveTimeDescription;
	
	//预约提醒
	@Column(name = "appointment_reminder")
	private String appointmentReminder;
	
	//温馨提示
	@Column(name = "tips")
	private String tips;
	
	//拼团说明
	@Column(name = "group_description")
	private String groupDescription;
	
	@Transient
	private List<FightGroupGoodsPicture> pictures;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	/**
	 * 设置：商品名称
	 */
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	/**
	 * 获取：商品名称
	 */
	public String getGoodsName() {
		return goodsName;
	}
	/**
	 * 设置：说明
	 */
	public void setGoodsDescription(String goodsDescription) {
		this.goodsDescription = goodsDescription;
	}
	/**
	 * 获取：说明
	 */
	public String getGoodsDescription() {
		return goodsDescription;
	}
	/**
	 * 设置：商品图片
	 */
	public void setGoodsImg(String goodsImg) {
		this.goodsImg = goodsImg;
	}
	/**
	 * 获取：商品图片
	 */
	public String getGoodsImg() {
		return goodsImg;
	}
	/**
	 * 设置：原价
	 */
	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}
	/**
	 * 获取：原价
	 */
	public BigDecimal getOriginalPrice() {
		return originalPrice;
	}
	/**
	 * 设置：折扣价
	 */
	public void setDiscountPrice(BigDecimal discountPrice) {
		this.discountPrice = discountPrice;
	}
	/**
	 * 获取：折扣价
	 */
	public BigDecimal getDiscountPrice() {
		return discountPrice;
	}
	public Integer getSoldNum() {
		return soldNum;
	}
	public void setSoldNum(Integer soldNum) {
		this.soldNum = soldNum;
	}
	public String getEffectiveTimeDescription() {
		return effectiveTimeDescription;
	}
	public void setEffectiveTimeDescription(String effectiveTimeDescription) {
		this.effectiveTimeDescription = effectiveTimeDescription;
	}
	public String getAppointmentReminder() {
		return appointmentReminder;
	}
	public void setAppointmentReminder(String appointmentReminder) {
		this.appointmentReminder = appointmentReminder;
	}
	public String getTips() {
		return tips;
	}
	public void setTips(String tips) {
		this.tips = tips;
	}
	public String getGroupDescription() {
		return groupDescription;
	}
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}
	public List<FightGroupGoodsPicture> getPictures() {
		return pictures;
	}
	public void setPictures(List<FightGroupGoodsPicture> pictures) {
		this.pictures = pictures;
	}
	
}
