package com.github.wxiaoqi.security.biz.codex.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.wxiaoqi.security.biz.codex.entity.MarActivity;

import tk.mybatis.mapper.common.Mapper;

/**
 * 活动基本信息表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@org.apache.ibatis.annotations.Mapper
public interface MarActivityMapper extends Mapper<MarActivity> {
	List<MarActivity> queryActivityList(@Param(value = "actInfo") String actInfo);
	List<MarActivity> selectActivityList(MarActivity marActivity);
}
