package com.github.wxiaoqi.security.biz.modules.restaurant.biz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantDistributionInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantDistributionInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 配送信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantDistributionInfoBiz extends BaseBiz<RestaurantDistributionInfoMapper,RestaurantDistributionInfo> {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantDistributionInfoBiz.class);

	@Autowired
	private RestaurantDistributionInfoMapper restaurantDistributionInfoMapper;

	/**
	 * 默认配送地址查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryDefaultDistributionInfo(Map<String, Object> request){
		logger.info("默认配送地址查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String perId = (String) request.get("perId");
		String addressId = (String) request.get("addressId");

		RestaurantDistributionInfo restaurantDistributionInfo = new RestaurantDistributionInfo();
		restaurantDistributionInfo.setPlatformId(platFormId);
		restaurantDistributionInfo.setMerchantId(merId);
		restaurantDistributionInfo.setPerId(perId);
		restaurantDistributionInfo.setDefaultType("1");//默认
		RestaurantDistributionInfo selectDistributionInfo = restaurantDistributionInfoMapper.selectOne(restaurantDistributionInfo);
		Map<String, Object> butionMap = new HashMap<String, Object>();

		if(!"0".equals(addressId) && !StringUtils.isEmpty(addressId)){
			RestaurantDistributionInfo butionInfo = new RestaurantDistributionInfo();
			butionInfo.setPlatformId(platFormId);
			butionInfo.setMerchantId(merId);
			butionInfo.setPerId(perId);
			butionInfo.setId(Long.valueOf(addressId));
			RestaurantDistributionInfo selectButionInfo = restaurantDistributionInfoMapper.selectOne(butionInfo);
			if(null != selectButionInfo){
				butionMap.put("id", selectButionInfo.getId());
				butionMap.put("name", selectButionInfo.getDistributionName());
				if("1".equals(selectButionInfo.getDefaultType())){
					butionMap.put("isDefault", "0");
				}
				butionMap.put("mobile", selectButionInfo.getDistributionPhone());
				butionMap.put("address", selectButionInfo.getDistributionAddress());
				butionMap.put("distributionId", selectButionInfo.getDistributionId());
			}
		}else{
			if(null != selectDistributionInfo){
				butionMap.put("id", selectDistributionInfo.getId());
				butionMap.put("name", selectDistributionInfo.getDistributionName());
				butionMap.put("isDefault", "0");
				butionMap.put("mobile", selectDistributionInfo.getDistributionPhone());
				butionMap.put("address", selectDistributionInfo.getDistributionAddress());
				butionMap.put("distributionId", selectDistributionInfo.getDistributionId());
			}
		}
		response.put("selectDistributionInfo", butionMap);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("默认配送地址查询响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 配送地址列表查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryDistributionListInfo(Map<String, Object> request){
		logger.info("配送地址列表查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String perId = (String) request.get("perId");
		RestaurantDistributionInfo restaurantDistributionInfo = new RestaurantDistributionInfo();
		restaurantDistributionInfo.setPlatformId(platFormId);
		restaurantDistributionInfo.setMerchantId(merId);
		restaurantDistributionInfo.setPerId(perId);
		List<RestaurantDistributionInfo> infoList = restaurantDistributionInfoMapper.select(restaurantDistributionInfo);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for(RestaurantDistributionInfo butionInfo:infoList){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("name", butionInfo.getDistributionName());
			map.put("mobile", butionInfo.getDistributionPhone());
			map.put("detailedAddress", butionInfo.getDistributionProvince()+butionInfo.getDistributionCity()+butionInfo.getDistributionArea()+" "+butionInfo.getDistributionAddress());
			map.put("id", butionInfo.getId());
			map.put("distributionId", butionInfo.getDistributionId());
			if("1".equals(butionInfo.getDefaultType())){
				map.put("isDefault", "0");
			}
			list.add(map);
		}

		//查询非地址列表
		//		List<RestaurantDistributionInfo> infoList = restaurantDistributionInfoMapper.selectNotDefault(restaurantDistributionInfo);
		//		response.put("distributionListInfo", infoList);

		//查询默认地址
		//		restaurantDistributionInfo.setDefaultType("1");//默认
		//		RestaurantDistributionInfo selectDistributionInfo = restaurantDistributionInfoMapper.selectOne(restaurantDistributionInfo);
		//		response.put("selectDistributionInfo", selectDistributionInfo);
		response.put("addressList", list);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("配送地址列表查询响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 设置默认配送地址
	 * @param request
	 * @return
	 */
	public Map<String, Object> settingDefaultDistribution(Map<String, Object> request){
		logger.info("设置默认配送地址请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String perId = (String) request.get("perId");
		Integer distributionId = (Integer) request.get("distributionId");
		RestaurantDistributionInfo restaurantDistributionInfo = new RestaurantDistributionInfo();
		restaurantDistributionInfo.setPlatformId(platFormId);
		restaurantDistributionInfo.setMerchantId(merId);
		restaurantDistributionInfo.setPerId(perId);
		restaurantDistributionInfo.setDefaultType("1");//默认
		//将原来默认改为非默认
		RestaurantDistributionInfo selectDefaultInfo = restaurantDistributionInfoMapper.selectOne(restaurantDistributionInfo);
		selectDefaultInfo.setDefaultType(null);
		restaurantDistributionInfoMapper.updateByPrimaryKey(selectDefaultInfo);

		//设置默认地址
		restaurantDistributionInfo.setDefaultType(null);
		restaurantDistributionInfo.setId(Long.valueOf(distributionId));
		RestaurantDistributionInfo selectDistributionInfo = restaurantDistributionInfoMapper.selectOne(restaurantDistributionInfo);
		selectDistributionInfo.setDefaultType("1");//默认
		restaurantDistributionInfoMapper.updateByPrimaryKey(selectDistributionInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("设置默认配送地址响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 删除配送地址
	 * @param request
	 * @return
	 */
	public Map<String, Object> deleteDistributionInfo(Map<String, Object> request){
		logger.info("删除配送地址请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String perId = (String) request.get("perId");
		Integer distributionId = (Integer) request.get("distributionId");
		RestaurantDistributionInfo restaurantDistributionInfo = new RestaurantDistributionInfo();
		restaurantDistributionInfo.setPlatformId(platFormId);
		restaurantDistributionInfo.setMerchantId(merId);
		restaurantDistributionInfo.setPerId(perId);
		restaurantDistributionInfo.setId(Long.valueOf(distributionId));
		restaurantDistributionInfoMapper.delete(restaurantDistributionInfo);		
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("删除配送地址响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 添加/修改配送地址
	 * @param request
	 * @return
	 */
	public Map<String, Object> addDistributionInfo(Map<String, Object> request){
		logger.info("添加/修改配送地址请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String perId = (String) request.get("perId");
		String distributionName = (String) request.get("distributionName");
		String distributionPhone = (String) request.get("distributionPhone");
		String distributionProvince = (String) request.get("distributionProvince");
		String distributionCity = (String) request.get("distributionCity");
		String distributionArea = (String) request.get("distributionArea");
		String distributionAddress = (String) request.get("distributionAddress");
		String isDefault = (String) request.get("isDefault");
		String distributionId = (String) request.get("distributionId");
		long id = SnowFlake.getId();
		RestaurantDistributionInfo restaurantDistributionInfo = new RestaurantDistributionInfo();
		restaurantDistributionInfo.setPlatformId(platFormId);
		restaurantDistributionInfo.setMerchantId(merId);
		restaurantDistributionInfo.setPerId(perId);
		if("1".equals(isDefault) || "true".equals(isDefault)){
			restaurantDistributionInfo.setDefaultType("1");
			RestaurantDistributionInfo selectOne = restaurantDistributionInfoMapper.selectOne(restaurantDistributionInfo);
			selectOne.setDefaultType(null);
			restaurantDistributionInfoMapper.updateByPrimaryKey(selectOne);
		}
		restaurantDistributionInfo.setDistributionPhone(distributionPhone);
		restaurantDistributionInfo.setDistributionProvince(distributionProvince);
		restaurantDistributionInfo.setDistributionCity(distributionCity);
		restaurantDistributionInfo.setDistributionArea(distributionArea);
		restaurantDistributionInfo.setDistributionAddress(distributionAddress);
		restaurantDistributionInfo.setDistributionName(distributionName);
		if(StringUtils.isEmpty(distributionId)){
			//新增
			restaurantDistributionInfo.setDistributionId(String.valueOf(id));
			restaurantDistributionInfoMapper.insert(restaurantDistributionInfo);
		}else{
			//修改
			restaurantDistributionInfo.setId(Long.valueOf(distributionId));
			restaurantDistributionInfoMapper.updateByPrimaryKey(restaurantDistributionInfo);
		}
		
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("添加/修改配送地址响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询配送地址明细
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryDistributionDeatilInfo(Map<String, Object> request){
		logger.info("查询配送地址明细请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String perId = (String) request.get("perId");
		String distributionId = (String) request.get("distributionId");
		RestaurantDistributionInfo restaurantDistributionInfo = new RestaurantDistributionInfo();
		restaurantDistributionInfo.setPlatformId(platFormId);
		restaurantDistributionInfo.setMerchantId(merId);
		restaurantDistributionInfo.setPerId(perId);
		restaurantDistributionInfo.setId(Long.valueOf(distributionId));
		RestaurantDistributionInfo selectButionInfo = restaurantDistributionInfoMapper.selectOne(restaurantDistributionInfo);
		Map<String, Object> butionMap = new HashMap<String, Object>();
		butionMap.put("name", selectButionInfo.getDistributionName());
		butionMap.put("mobile", selectButionInfo.getDistributionPhone());
		butionMap.put("provinceName", selectButionInfo.getDistributionProvince());
		butionMap.put("cityName", selectButionInfo.getDistributionCity());
		butionMap.put("areaName", selectButionInfo.getDistributionArea());
		butionMap.put("address", selectButionInfo.getDistributionAddress());
		if("1".equals(selectButionInfo.getDefaultType())){
			butionMap.put("isDefault", "1");
		}
		response.put("butionInfo", butionMap);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("查询配送地址明细响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询配送信息列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<RestaurantDistributionInfo> queryRestaurantDistributionInfoPage(RestaurantQueryParam param) {
		logger.info("查询配送信息列表。。。。{}"+param);
		Example example = new Example(RestaurantDistributionInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		Page<RestaurantDistributionInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<RestaurantDistributionInfo> list = mapper.selectByExample(example);
		TableResultResponse<RestaurantDistributionInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
}