package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 随机红包等级
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Table(name = "mar_random_coupon_level")
public class MarRandomCouponLevel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //主键
    @Id
    private Long seqNo;
    
  //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
    
    //商户号
    @Column(name = "MERCHANT_ID")
    private String merchantId;
	
	    //红包编号
    @Column(name = "COU_ID")
    private String couId;
	
	    //第一档数量
    @Column(name = "FIRST_COUNT")
    private Integer firstCount;
	
	    //第一档金额
    @Column(name = "FIRST_AMT")
    private BigDecimal firstAmt;
	
	    //第二档数量
    @Column(name = "SECOND_COUNT")
    private Integer secondCount;
	
	    //第二档金额
    @Column(name = "SECOND_AMT")
    private BigDecimal secondAmt;
	
	    //第三档数量
    @Column(name = "THIRD_COUNT")
    private Integer thirdCount;
	
	    //第三档金额
    @Column(name = "THIRD_AMT")
    private BigDecimal thirdAmt;
	

	/**
	 * 设置：主键
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：主键
	 */
	public Long getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：红包编号
	 */
	public void setCouId(String couId) {
		this.couId = couId;
	}
	/**
	 * 获取：红包编号
	 */
	public String getCouId() {
		return couId;
	}
	/**
	 * 设置：第一档数量
	 */
	public void setFirstCount(Integer firstCount) {
		this.firstCount = firstCount;
	}
	/**
	 * 获取：第一档数量
	 */
	public Integer getFirstCount() {
		return firstCount;
	}
	/**
	 * 设置：第一档金额
	 */
	public void setFirstAmt(BigDecimal firstAmt) {
		this.firstAmt = firstAmt;
	}
	/**
	 * 获取：第一档金额
	 */
	public BigDecimal getFirstAmt() {
		return firstAmt;
	}
	/**
	 * 设置：第二档数量
	 */
	public void setSecondCount(Integer secondCount) {
		this.secondCount = secondCount;
	}
	/**
	 * 获取：第二档数量
	 */
	public Integer getSecondCount() {
		return secondCount;
	}
	/**
	 * 设置：第二档金额
	 */
	public void setSecondAmt(BigDecimal secondAmt) {
		this.secondAmt = secondAmt;
	}
	/**
	 * 获取：第二档金额
	 */
	public BigDecimal getSecondAmt() {
		return secondAmt;
	}
	/**
	 * 设置：第三档数量
	 */
	public void setThirdCount(Integer thirdCount) {
		this.thirdCount = thirdCount;
	}
	/**
	 * 获取：第三档数量
	 */
	public Integer getThirdCount() {
		return thirdCount;
	}
	/**
	 * 设置：第三档金额
	 */
	public void setThirdAmt(BigDecimal thirdAmt) {
		this.thirdAmt = thirdAmt;
	}
	/**
	 * 获取：第三档金额
	 */
	public BigDecimal getThirdAmt() {
		return thirdAmt;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	
}
