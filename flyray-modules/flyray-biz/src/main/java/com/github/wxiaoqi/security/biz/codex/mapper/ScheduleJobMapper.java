package com.github.wxiaoqi.security.biz.codex.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.codex.entity.ScheduleJob;

import tk.mybatis.mapper.common.Mapper;

/**
 * 定时任务
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-26 13:56:02
 */
@org.apache.ibatis.annotations.Mapper
public interface ScheduleJobMapper extends Mapper<ScheduleJob> {
	List<ScheduleJob> queryList(Map<String, Object> map);

	void addJob(ScheduleJob entity);

	void updateJob(ScheduleJob entity);

	void deleteJob(ScheduleJob entity);

	void updateStatus(ScheduleJob entity);
}
