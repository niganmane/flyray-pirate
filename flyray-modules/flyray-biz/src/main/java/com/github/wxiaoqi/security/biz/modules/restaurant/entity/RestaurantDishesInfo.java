package com.github.wxiaoqi.security.biz.modules.restaurant.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 菜品信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Table(name = "restaurant_dishes_info")
public class RestaurantDishesInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	
	//菜品id
	@Column(name = "dishes_id")
	private String dishesId;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//类目id
	@Column(name = "category_id")
	private Integer categoryId;

	//菜品名称
	@Column(name = "dishes_name")
	private String dishesName;

	//说明
	@Column(name = "instructions")
	private String instructions;

	//图片地址
	@Column(name = "image_url")
	private String imageUrl;

	//预定数量
	@Column(name = "order_num")
	private Integer orderNum;

	//价格
	@Column(name = "price")
	private String price;

	//规格开关
	@Column(name = "is_specification")
	private String isSpecification;

	//业务类型 1点餐 2外卖 3公共
	@Column(name = "type")
	private String type;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	public String getDishesId() {
		return dishesId;
	}
	public void setDishesId(String dishesId) {
		this.dishesId = dishesId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：类目id
	 */
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	/**
	 * 获取：类目id
	 */
	public Integer getCategoryId() {
		return categoryId;
	}
	/**
	 * 设置：菜品名称
	 */
	public void setDishesName(String dishesName) {
		this.dishesName = dishesName;
	}
	/**
	 * 获取：菜品名称
	 */
	public String getDishesName() {
		return dishesName;
	}
	/**
	 * 设置：说明
	 */
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	/**
	 * 获取：说明
	 */
	public String getInstructions() {
		return instructions;
	}
	/**
	 * 设置：图片地址
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	/**
	 * 获取：图片地址
	 */
	public String getImageUrl() {
		return imageUrl;
	}
	/**
	 * 设置：预定数量
	 */
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	/**
	 * 获取：预定数量
	 */
	public Integer getOrderNum() {
		return orderNum;
	}
	/**
	 * 设置：价格
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * 获取：价格
	 */
	public String getPrice() {
		return price;
	}
	/**
	 * 设置：规格开关
	 */
	public void setIsSpecification(String isSpecification) {
		this.isSpecification = isSpecification;
	}
	/**
	 * 获取：规格开关
	 */
	public String getIsSpecification() {
		return isSpecification;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "RestaurantDishesInfo [id=" + id + ", dishesId=" + dishesId + ", platformId=" + platformId + ", merchantId="
				+ merchantId + ", categoryId=" + categoryId + ", dishesName=" + dishesName + ", instructions=" + instructions
				+ ", imageUrl=" + imageUrl + ", orderNum=" + orderNum + ", price=" + price + ", isSpecification="
				+ isSpecification + ", type=" + type + "]";
	}
}
