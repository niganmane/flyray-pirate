package com.github.wxiaoqi.security.biz.codex.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.wxiaoqi.security.biz.codex.biz.MarIncidentBiz;
import com.github.wxiaoqi.security.biz.codex.entity.MarIncident;
import com.github.wxiaoqi.security.common.rest.BaseController;

/**
 * 事件管理
 * @author Administrator
 *
 */
@Controller
@RequestMapping("marIncident")
public class MarIncidentController extends BaseController<MarIncidentBiz, MarIncident> {

}
