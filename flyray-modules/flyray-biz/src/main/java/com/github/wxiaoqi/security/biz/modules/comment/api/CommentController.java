package com.github.wxiaoqi.security.biz.modules.comment.api;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.api.BaseController;
import com.github.wxiaoqi.security.biz.modules.comment.biz.CommentBiz;
import com.github.wxiaoqi.security.common.cms.request.CmsQueryCommentParam;
import com.github.wxiaoqi.security.common.cms.request.CmsSaveCommentParam;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags="评论")
@Controller
@RequestMapping("comments")
public class CommentController extends BaseController {

	@Autowired
	private CommentBiz commentBiz;
	
	/**
	 * 查询观点评论 
	 */
	@ResponseBody
	@RequestMapping(value="/query", method = RequestMethod.POST)
	public Map<String, Object> queryComment(@RequestBody CmsQueryCommentParam param) {
		Map<String, Object> respMap = commentBiz.queryComment(param);
		return respMap;
	}
	
	/**
	 * 查询观点评论 
	 * 查询多级评论
	 */
	@ResponseBody
	@RequestMapping(value="/manyQuery", method = RequestMethod.POST)
	public Map<String, Object> queryTwo(@RequestBody CmsQueryCommentParam param) {
		log.info("查询观点评论 ---start---{}",param);
		Map<String, Object> respMap = commentBiz.queryTwo(param);
		return respMap;
	}
	
	/**
	 * 观点回复添加
	 */
	@ResponseBody
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public Map<String, Object> addPointComment(@RequestBody CmsSaveCommentParam param) {
		log.info("观点回复添加 ---start---{}",param);
		Map<String, Object> respMap = commentBiz.addPointComment(param);
		return respMap;
	}
}
