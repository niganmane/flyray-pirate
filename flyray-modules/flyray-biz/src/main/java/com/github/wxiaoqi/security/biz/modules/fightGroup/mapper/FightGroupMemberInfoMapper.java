package com.github.wxiaoqi.security.biz.modules.fightGroup.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupMemberInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 团成员信息表
 * 
 * @author he
 * @date 2018-07-12 10:17:33
 */
@org.apache.ibatis.annotations.Mapper
public interface FightGroupMemberInfoMapper extends Mapper<FightGroupMemberInfo> {
	
	List<FightGroupMemberInfo> queryFightGroupMemberInfo(Map<String, Object> map);
	
}
