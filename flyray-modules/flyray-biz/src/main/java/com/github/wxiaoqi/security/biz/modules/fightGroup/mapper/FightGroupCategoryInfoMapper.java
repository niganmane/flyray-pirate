package com.github.wxiaoqi.security.biz.modules.fightGroup.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupCategoryInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 团类目信息表
 * 
 * @author he
 * @date 2018-07-12 10:17:33
 */
@org.apache.ibatis.annotations.Mapper
public interface FightGroupCategoryInfoMapper extends Mapper<FightGroupCategoryInfo> {
	
	List<FightGroupCategoryInfo> queryFightGroupCategory(Map<String, Object> map);
	
}
