package com.github.wxiaoqi.security.biz.modules.restaurant.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 餐桌信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Table(name = "restaurant_table_info")
public class RestaurantTableInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	
	//餐桌id
	@Column(name = "table_id")
	private String tableId;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//餐桌名称
	@Column(name = "table_name")
	private String tableName;

	//人数
	@Column(name = "table_people")
	private String tablePeople;

	//状态
	@Column(name = "status")
	private String status;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerchantId(String merId) {
		this.merchantId = merId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：餐桌名称
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	/**
	 * 获取：餐桌名称
	 */
	public String getTableName() {
		return tableName;
	}
	/**
	 * 设置：人数
	 */
	public void setTablePeople(String tablePeople) {
		this.tablePeople = tablePeople;
	}
	/**
	 * 获取：人数
	 */
	public String getTablePeople() {
		return tablePeople;
	}
	/**
	 * 设置：状态
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：状态
	 */
	public String getStatus() {
		return status;
	}
}
