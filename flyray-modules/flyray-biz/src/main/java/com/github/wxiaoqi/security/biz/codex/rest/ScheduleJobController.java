
package com.github.wxiaoqi.security.biz.codex.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.codex.entity.ScheduleJob;
import com.github.wxiaoqi.security.biz.codex.service.ScheduleJobSchemaTaskService;
import com.github.wxiaoqi.security.biz.util.ValidatorUtils;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

/**
 * 定时任务
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.2.0 2016-11-28
 */
@RestController
@RequestMapping("marJobs")
public class ScheduleJobController {
	private final Logger logger = LoggerFactory.getLogger(ScheduleJobController.class);
	
	@Autowired
	private ScheduleJobSchemaTaskService scheduleJobService;
	
	/**
	 * 定时任务列表
	 */
	@ResponseBody
	@RequestMapping(value = "/page", method = RequestMethod.POST)
	public TableResultResponse<ScheduleJob> queryJonList(@RequestParam("page") String page, @RequestParam("limit") String limit, String beanName){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("page", page);
		params.put("limit", limit);
		if(StringUtils.isNotBlank(beanName)){
			params.put("beanName", beanName);
		}
		logger.info("查询renwu列表。。。{}"+params);
		Query query = new Query(params);
		Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		List<ScheduleJob> list = scheduleJobService.queryJobList(params);
		return new TableResultResponse<ScheduleJob>(result.getTotal(), list);
	}
	
	/**
	 * 保存定时任务
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ObjectRestResponse<ScheduleJob> addJob(@RequestBody Map<String, Object> params){
		logger.info("添加任务。。。{}"+params);
		ScheduleJob jobEntity = new ScheduleJob();
		String beanName = (String) params.get("beanName");
		String methodName = (String) params.get("methodName");
		String param = (String) params.get("params");
		String cronExpression = (String) params.get("cronExpression");
		String remark = (String) params.get("remark");
		jobEntity.setBeanName(beanName);
		jobEntity.setMethodName(methodName);
		jobEntity.setCronExpression(cronExpression);
		jobEntity.setParams(param);
		jobEntity.setRemark(remark);
		ValidatorUtils.validateEntity(jobEntity);
		scheduleJobService.addJob(jobEntity);
		return new ObjectRestResponse<ScheduleJob>();
	}
	
	/**
	 * 修改定时任务
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ObjectRestResponse<ScheduleJob> updateJob(@RequestBody Map<String, Object> params){
		logger.info("修改任务。。。{}"+params);
		List<ScheduleJob> list = scheduleJobService.queryJobList(params);
		if (list != null && list.size() > 0) {
			ScheduleJob jobEntity = list.get(0);
			String beanName = (String) params.get("beanName");
			String methodName = (String) params.get("methodName");
			String param = (String) params.get("params");
			String cronExpression = (String) params.get("cronExpression");
			String remark = (String) params.get("remark");
			jobEntity.setBeanName(beanName);
			jobEntity.setMethodName(methodName);
			jobEntity.setParams(param);
			jobEntity.setCronExpression(cronExpression);
			jobEntity.setRemark(remark);
			ValidatorUtils.validateEntity(jobEntity);
			scheduleJobService.updateJob(jobEntity);
		}
		return new ObjectRestResponse<ScheduleJob>();
	}
	
	/**
	 * 删除定时任务
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ObjectRestResponse<ScheduleJob> deleteJob(@RequestBody Map<String, Object> params){
		logger.info("删除任务。。。{}"+params);
		Integer jobIds = (Integer) params.get("jobId");
		Long jobId = Long.valueOf(String.valueOf(jobIds));
		scheduleJobService.deleteBatch(jobId);
		return new ObjectRestResponse<ScheduleJob>();
	}
	
	/**
	 * 立即执行任务
	 */
	@ResponseBody
	@RequestMapping(value = "/run", method = RequestMethod.POST)
	public ObjectRestResponse<ScheduleJob> runJob(@RequestBody Map<String, Object> params){
		logger.info("立即执行任务。。。{}"+params);
		Integer jobIds = (Integer) params.get("jobId");
		Long jobId = Long.valueOf(String.valueOf(jobIds));
		scheduleJobService.runJob(jobId);
		return new ObjectRestResponse<>();
	}
	
	/**
	 * 暂停定时任务
	 */
	@ResponseBody
	@RequestMapping(value = "/pause", method = RequestMethod.POST)
	public ObjectRestResponse<ScheduleJob> pauseJob(@RequestBody Map<String, Object> params){
		logger.info("暂停定时任务。。。{}"+params);
		Integer jobIds = (Integer) params.get("jobId");
		Long jobId = Long.valueOf(String.valueOf(jobIds));
		scheduleJobService.pauseJob(jobId);
		return new ObjectRestResponse<>();
	}
	
	/**
	 * 恢复定时任务
	 */
	@ResponseBody
	@RequestMapping(value = "/resume", method = RequestMethod.POST)
	public ObjectRestResponse<ScheduleJob> resumeJob(@RequestBody Map<String, Object> params){
		logger.info("恢复定时任务。。。{}"+params);
		Integer jobIds = (Integer) params.get("jobId");
		Long jobId = Long.valueOf(String.valueOf(jobIds));
		scheduleJobService.resumeJob(jobId);
		return new ObjectRestResponse<>();
	}
	
	
	/**
	 * 获取信息
	 */
	@ResponseBody
	@RequestMapping(value = "/info", method = RequestMethod.POST)
	public ObjectRestResponse<ScheduleJob> queryInfo(@RequestBody Map<String, Object> params) {
		ObjectRestResponse<ScheduleJob> response = new ObjectRestResponse<>();
		logger.info("获取任务信息。。。{}"+params);
		List<ScheduleJob> list = scheduleJobService.queryJobList(params);
		if (list != null && list.size() > 0) {
			ScheduleJob entity = list.get(0);
			response.data(entity);
		}
		return response;
	}

}
