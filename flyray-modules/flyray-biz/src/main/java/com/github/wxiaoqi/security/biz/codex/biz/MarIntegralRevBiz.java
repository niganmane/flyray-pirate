package com.github.wxiaoqi.security.biz.codex.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.MarIntegralRev;
import com.github.wxiaoqi.security.biz.codex.mapper.MarIntegralRevMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 积分领取表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Service
public class MarIntegralRevBiz extends BaseBiz<MarIntegralRevMapper,MarIntegralRev> {
}