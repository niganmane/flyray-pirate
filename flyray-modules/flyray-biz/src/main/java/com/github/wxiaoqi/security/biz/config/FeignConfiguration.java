package com.github.wxiaoqi.security.biz.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.wxiaoqi.security.biz.interceptor.UserTokenInterceptor;


/**
 * Created by ace on 2017/9/12.
 */
@Configuration
public class FeignConfiguration {
    @Bean
    UserTokenInterceptor getClientTokenInterceptor(){
        return new UserTokenInterceptor();
    }
}
