package com.github.wxiaoqi.security.biz.modules.activity.biz;

import java.util.List;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.modules.activity.entity.ActivityCustomer;
import com.github.wxiaoqi.security.biz.modules.activity.mapper.ActivityCustomerMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.CmsQueryActivityJoinParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 
 *
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:43
 */
@Slf4j
@Service
public class ActivityCustomerBiz extends BaseBiz<ActivityCustomerMapper,ActivityCustomer> {

	
	
	/**
	 * 查询参加活动信息
	 * @param param
	 * @return
	 */
	public TableResultResponse<ActivityCustomer> queryActivityJoin(CmsQueryActivityJoinParam param) {
		log.info("查询参加活动信息。。。{}"+param);
			Example example = new Example(ActivityCustomer.class);
			Criteria criteria = example.createCriteria();
			if (param.getActivityId() != null && param.getActivityId().length() > 0) {
				criteria.andEqualTo("activityId",param.getActivityId());
			}
			example.setOrderByClause("create_time desc");
			Page<ActivityCustomer> result = PageHelper.startPage(param.getPage(), param.getLimit());
			List<ActivityCustomer> list = mapper.selectByExample(example);
			TableResultResponse<ActivityCustomer> table = new TableResultResponse<>(result.getTotal(), list);
			return table;
	}
}