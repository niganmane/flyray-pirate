package com.github.wxiaoqi.security.biz.modules.restaurant.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 菜品评价信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Table(name = "restaurant_appraisal_info")
public class RestaurantAppraisalInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//菜品id
	@Column(name = "dishes_id")
	private String dishesId;

	//很满意数量
	@Column(name = "very_satisfied_num")
	private Integer verySatisfiedNum;

	//满意数量
	@Column(name = "satisfied_num")
	private Integer satisfiedNum;

	//一般数量
	@Column(name = "general_num")
	private Integer generalNum;

	//不满意数量
	@Column(name = "not_satisfied_num")
	private Integer notSatisfiedNum;
	
	//总评价数量
	@Column(name = "total_num")
	private Integer totalNum;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerchantId(String merId) {
		this.merchantId = merId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：菜品id
	 */
	public void setDishesId(String dishesId) {
		this.dishesId = dishesId;
	}
	/**
	 * 获取：菜品id
	 */
	public String getDishesId() {
		return dishesId;
	}
	/**
	 * 设置：很满意数量
	 */
	public void setVerySatisfiedNum(Integer verySatisfiedNum) {
		this.verySatisfiedNum = verySatisfiedNum;
	}
	/**
	 * 获取：很满意数量
	 */
	public Integer getVerySatisfiedNum() {
		return verySatisfiedNum;
	}
	/**
	 * 设置：满意数量
	 */
	public void setSatisfiedNum(Integer satisfiedNum) {
		this.satisfiedNum = satisfiedNum;
	}
	/**
	 * 获取：满意数量
	 */
	public Integer getSatisfiedNum() {
		return satisfiedNum;
	}
	/**
	 * 设置：一般数量
	 */
	public void setGeneralNum(Integer generalNum) {
		this.generalNum = generalNum;
	}
	/**
	 * 获取：一般数量
	 */
	public Integer getGeneralNum() {
		return generalNum;
	}
	public Integer getNotSatisfiedNum() {
		return notSatisfiedNum;
	}
	public void setNotSatisfiedNum(Integer notSatisfiedNum) {
		this.notSatisfiedNum = notSatisfiedNum;
	}
	public Integer getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}
}
