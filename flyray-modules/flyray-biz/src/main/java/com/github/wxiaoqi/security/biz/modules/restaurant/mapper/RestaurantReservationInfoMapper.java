package com.github.wxiaoqi.security.biz.modules.restaurant.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantReservationInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 预订信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantReservationInfoMapper extends Mapper<RestaurantReservationInfo> {
	
	List<RestaurantReservationInfo> queryRestaurantReservationInfo(Map<String, Object> map);
	
}
