package com.github.wxiaoqi.security.biz.codex.biz;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.MarActivity;
import com.github.wxiaoqi.security.biz.codex.entity.MarCoupon;
import com.github.wxiaoqi.security.biz.codex.entity.MarIncident;
import com.github.wxiaoqi.security.biz.codex.entity.MarScene;
import com.github.wxiaoqi.security.biz.codex.mapper.MarActivityMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarCouponMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarIncidentMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarSceneMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 红包基本信息表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Service
public class MarCouponBiz extends BaseBiz<MarCouponMapper,MarCoupon> {
	
	private static final Logger logger = LoggerFactory.getLogger(MarCouponBiz.class);

	@Autowired
	private MarCouponMapper couponMapper;
	@Autowired
	private MarIncidentMapper incidentMapper;
	@Autowired
	private MarActivityMapper marActivityMapper;
	@Autowired
	private MarSceneMapper sceneMapper;
	
	public List<MarCoupon> getCouponList(Map<String, Object> params) {
		logger.info("查询列表。。。"+params);
		String actId = (String) params.get("actId");
		String revStart = (String) params.get("revStart");
		String revEnd = (String) params.get("revEnd");
		String couType = (String) params.get("couType");
		List<MarCoupon> list = couponMapper.queryCouponList(revStart, revEnd, actId, couType);
		if (list != null && list.size() > 0) {
			logger.info("红包列表。。。{}"+list.toString());
			for (MarCoupon marCoupon : list) {
				String sceneStr = marCoupon.getSceneId();
				String incStr = marCoupon.getIncCode();
				//场景转译
				if (sceneStr != null) {
					String sceneId = "";
					List<MarScene> scenes = sceneMapper.selectAll();
					if (sceneStr.contains(",")) {
						List<String> arr1 = new ArrayList<>(); 
						String[] sarr = sceneStr.split(",");
						for (String string : sarr) {
							for (MarScene scene : scenes) {
								if (string.equals(scene.getSceneCode())) {
									string = scene.getSceneName();
								}
							}
							arr1.add(string);
						}
						String s1 = arr1.toString();
						String s2 = s1.replace("[", "");
						sceneId = s2.replace("]", "");
					}else {
						for (MarScene scene : scenes) {
							if (sceneStr.equals(scene.getSceneCode())) {
								sceneStr = scene.getSceneName();
								break;
							}
						}
						sceneId = sceneStr;
					}
					marCoupon.setSceneId(sceneId);
				}
				//事件转译
				if (incStr != null) {
					String incCode = "";
					List<MarIncident> incidents = incidentMapper.selectAll();
					if (incStr.contains(",")) {
						List<String> arr2 = new ArrayList<>(); 
						String[] arr = incStr.split(",");
						for (String string2 : arr) {
							for (MarIncident incident : incidents) {
								if (string2.equals(incident.getIncCode())) {
									string2 = incident.getIncName();
								}
							}
							arr2.add(string2);
						}
						String s1 = arr2.toString();
						String s2 = s1.replace("[", "");
						incCode = s2.replace("]", "");
					}else {
						for (MarIncident incident : incidents) {
							if (incStr.equals(incident.getIncCode())) {
								incStr = incident.getIncName();
								break;
							}
						}
						incCode = incStr;
					}
					marCoupon.setIncCode(incCode);
				}
				//时间比较
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String revS = dateFormat.format(marCoupon.getRevStart());
				String nowS = dateFormat.format(new Date());
				try {
					if (dateFormat.parse(revS).getTime() > dateFormat.parse(nowS).getTime()) {
						marCoupon.setIsOpen("00");
					}else {
						marCoupon.setIsOpen("01");
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		logger.info("转后红包列表。。。{}"+list);
		return list;
	}

	/**
	 * 新增红包时 根据活动状态来设置可领取状态
	 * 
	 * @param actId
	 * @return
	 */
	public String getCouStatus(String actId ,String couStatus) {
		logger.info("查询活动状态。。。{}"+actId+", "+couStatus);
		//查看活动进行状态
		MarActivity marActivity = new MarActivity();
		marActivity.setActId(actId);
		MarActivity record = marActivityMapper.selectOne(marActivity);
		String actStatus = record.getActStauts();
		if (!"01".equals(actStatus)) {
			couStatus = "01";
		}
		return couStatus;
	}

	/**
	 * 更新红包时  根据活动状态来设置可领取状态
	 * 
	 * @param actId
	 * @return
	 */
	public String getUpCouStatus(String couId, String couStatus) {
		MarCoupon marCoupon = couponMapper.selectCoup(couId);
		String actId = marCoupon.getActId();
		//查看活动进行状态
		MarActivity marActivity = new MarActivity();
		marActivity.setActId(actId);
		MarActivity record = marActivityMapper.selectOne(marActivity);
		String actStatus = record.getActStauts();
		if (!"01".equals(actStatus)) {
			couStatus = "01";
		}
		return couStatus;
	}
	/**
	 * 获取单个红包
	 * */
	public MarCoupon selectBycouId(String couId){
		logger.info("获取单个红包信息。。。{}"+couId);
		return couponMapper.selectCoup(couId);
	}
	

}