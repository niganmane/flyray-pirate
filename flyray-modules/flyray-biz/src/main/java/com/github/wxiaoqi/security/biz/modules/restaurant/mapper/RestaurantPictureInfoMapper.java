package com.github.wxiaoqi.security.biz.modules.restaurant.mapper;

import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantPictureInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 餐厅照片信息表
 * @author he
 * @date 2018-06-29 10:31:27
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantPictureInfoMapper extends Mapper<RestaurantPictureInfo> {
	
}
