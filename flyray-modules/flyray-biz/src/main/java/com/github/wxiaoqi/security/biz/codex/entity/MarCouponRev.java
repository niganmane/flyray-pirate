package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * 红包领取表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Table(name = "mar_coupon_rev")
public class MarCouponRev implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Long seqNo;
    
    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
    
    //商户号
    @Column(name = "MERCHANT_ID")
    private String merchantId;
	
	    //商户会员号
    @Column(name = "personal_id")
    private String personalId;
	
	    //红包流水号
    @Column(name = "COU_SERIAL")
    private String couSerial;
	
	    //红包id
    @Column(name = "COU_ID")
    private String couId;
	
	    //领取时间
    @Column(name = "RECEIVER_TIME")
    private Date receiverTime;
	
	    //场景编号
    @Column(name = "SCENE_ID")
    private String sceneId;
	
	    //活动编号
    @Column(name = "ACT_ID")
    private String actId;
	
	    //事件编号
    @Column(name = "INC_CODE")
    private String incCode;
	
	    //支付订单号
    @Column(name = "MERORDERNO")
    private String merorderno;
	
	    //抵扣金额
    @Column(name = "COUPON_AMT")
    private BigDecimal couponAmt;
	
	    //红包等级 01一档；02二挡；03三挡
    @Column(name = "COUPON_LEVEL")
    private String couponLevel;
	
	    //领取方式 0：手动 1：自动 2：定向
    @Column(name = "REV_TYPE")
    private String revType;
	
    /**
     * 红包类型
     * 
     * */
    @Transient
    private String couType;
    

	public String getCouType() {
		return couType;
	}
	public void setCouType(String couType) {
		this.couType = couType;
	}
	/**
	 * 设置：
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：
	 */
	public Long getSeqNo() {
		return seqNo;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	
	/**
	 * 获取：商户会员号
	 */
	public String getPersonalId() {
		return personalId;
	}
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 设置：红包流水号
	 */
	public void setCouSerial(String couSerial) {
		this.couSerial = couSerial;
	}
	/**
	 * 获取：红包流水号
	 */
	public String getCouSerial() {
		return couSerial;
	}
	/**
	 * 设置：红包id
	 */
	public void setCouId(String couId) {
		this.couId = couId;
	}
	/**
	 * 获取：红包id
	 */
	public String getCouId() {
		return couId;
	}
	/**
	 * 设置：领取时间
	 */
	public void setReceiverTime(Date receiverTime) {
		this.receiverTime = receiverTime;
	}
	/**
	 * 获取：领取时间
	 */
	public Date getReceiverTime() {
		return receiverTime;
	}
	/**
	 * 设置：场景编号
	 */
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	/**
	 * 获取：场景编号
	 */
	public String getSceneId() {
		return sceneId;
	}
	/**
	 * 设置：活动编号
	 */
	public void setActId(String actId) {
		this.actId = actId;
	}
	/**
	 * 获取：活动编号
	 */
	public String getActId() {
		return actId;
	}
	/**
	 * 设置：事件编号
	 */
	public void setIncCode(String incCode) {
		this.incCode = incCode;
	}
	/**
	 * 获取：事件编号
	 */
	public String getIncCode() {
		return incCode;
	}
	/**
	 * 设置：支付订单号
	 */
	public void setMerorderno(String merorderno) {
		this.merorderno = merorderno;
	}
	/**
	 * 获取：支付订单号
	 */
	public String getMerorderno() {
		return merorderno;
	}
	/**
	 * 设置：抵扣金额
	 */
	public void setCouponAmt(BigDecimal couponAmt) {
		this.couponAmt = couponAmt;
	}
	/**
	 * 获取：抵扣金额
	 */
	public BigDecimal getCouponAmt() {
		return couponAmt;
	}
	/**
	 * 设置：红包等级 01一档；02二挡；03三挡
	 */
	public void setCouponLevel(String couponLevel) {
		this.couponLevel = couponLevel;
	}
	/**
	 * 获取：红包等级 01一档；02二挡；03三挡
	 */
	public String getCouponLevel() {
		return couponLevel;
	}
	/**
	 * 设置：领取方式 0：手动 1：自动 2：定向
	 */
	public void setRevType(String revType) {
		this.revType = revType;
	}
	/**
	 * 获取：领取方式 0：手动 1：自动 2：定向
	 */
	public String getRevType() {
		return revType;
	}
}
