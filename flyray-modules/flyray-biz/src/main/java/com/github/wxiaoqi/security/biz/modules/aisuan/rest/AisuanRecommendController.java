package com.github.wxiaoqi.security.biz.modules.aisuan.rest;

import com.github.wxiaoqi.security.biz.feign.AdminFeign;
import com.github.wxiaoqi.security.biz.modules.aisuan.biz.AisuanRecommendBiz;
import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanRecommend;
import com.github.wxiaoqi.security.common.cms.request.AisuanAddRecommendParam;
import com.github.wxiaoqi.security.common.cms.request.BaseParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@RequestMapping("aisuanRecommendAdmin")
public class AisuanRecommendController extends BaseController<AisuanRecommendBiz,AisuanRecommend> {
	@Autowired
	private AdminFeign adminFeign;
	@Autowired
	private AisuanRecommendBiz aisuanRecommendBiz;
	/**
	 * 查询平台下的用户
	 */
	@RequestMapping(value = "/queryUserUnderPlatform",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryUserUnderPlatform(@RequestBody BaseParam param) {
		log.info("查询平台下的用户传入参数{}", EntityUtils.beanToMap(param));
		Map<String, Object> map = new HashMap<>();
		map.put("platformId", param.getPlatformId());
		Map<String, Object> result = adminFeign.query(map);
		log.info("查询平台下的用户返回{}", result);
		return result;
	}
	/**
	 * 添加推荐人，不能超过5个人，不能重复
	 */
	 @RequestMapping(value = "/add",method = RequestMethod.POST)
	 @ResponseBody
	 public Map<String, Object> add(@RequestBody AisuanAddRecommendParam param) {
	  log.info("添加推荐人，不能超过5个人，不能重复传入参数{}", EntityUtils.beanToMap(param));
	  Map<String, Object> result = aisuanRecommendBiz.add(param);
	  log.info("添加推荐人，不能超过5个人，不能重复返回{}", result);
	  return result;
	 }
	 /**
	  * 查询时需要忽略商户号所以重写查询
	  */
	 @RequestMapping(value = "/queyrList",method = RequestMethod.GET)
	    @ResponseBody
	    public TableResultResponse<AisuanRecommend> queyrList(@RequestParam Map<String, Object> params){
		 log.info("查询金额列表传入参数{}", EntityUtils.beanToMap(params));
	        //查询列表数据
	    	params.remove("userType");
	    	params.remove("merchantId");
	    	params.remove("platformId");
	        Query query = new Query(params);
	        return baseBiz.selectByQuery(query);
	    }
	
	
}