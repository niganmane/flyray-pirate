package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 随机立减基本信息表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Table(name = "mar_random_subtract")
public class MarRandomSubtract implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //主键
    @Id
    private Long seqNo;
    
    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
    
    //商户号
    @Column(name = "MERCHANT_ID")
    private String merchantId;
	
	    //随机立减id
    @Column(name = "SUB_ID")
    private String subId;
	
	    //随机立减名称
    @Column(name = "SUB_NAME")
    private String subName;
	
	    //随机立减配置总金额
    @Column(name = "SUB_SUM")
    private BigDecimal subSum;
	
	    //随机立减配置总数量
    @Column(name = "SUB_NUM")
    private Integer subNum;
	
	    //活动id
    @Column(name = "ACT_ID")
    private String actId;
	
	    //创建时间
    @Column(name = "CREATE_TIME")
    private Date createTime;
	
	    //关闭时间
    @Column(name = "SHUT_TIME")
    private Date shutTime;
	
	    //状态，00可领，01不可领
    @Column(name = "SUB_STATUS")
    private String subStatus;
	
	    //
    @Column(name = "SUB_INFO")
    private String subInfo;
	
	    //领取开始时间
    @Column(name = "REV_START")
    private Date revStart;
	
	    //领取结束时间
    @Column(name = "REV_END")
    private Date revEnd;
	
	    //场景编号
    @Column(name = "SCENE_ID")
    private String sceneId;
	
	    //事件编码 多个事件用逗号分隔
    @Column(name = "INC_CODE")
    private String incCode;
	
	    //创建人
    @Column(name = "CREAT_USER")
    private String creatUser;
	

	/**
	 * 设置：主键
	 */
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	/**
	 * 获取：主键
	 */
	public Long getSeqNo() {
		return seqNo;
	}
	/**
	 * 设置：随机立减id
	 */
	public void setSubId(String subId) {
		this.subId = subId;
	}
	/**
	 * 获取：随机立减id
	 */
	public String getSubId() {
		return subId;
	}
	/**
	 * 设置：随机立减名称
	 */
	public void setSubName(String subName) {
		this.subName = subName;
	}
	/**
	 * 获取：随机立减名称
	 */
	public String getSubName() {
		return subName;
	}
	/**
	 * 设置：随机立减配置总金额
	 */
	public void setSubSum(BigDecimal subSum) {
		this.subSum = subSum;
	}
	/**
	 * 获取：随机立减配置总金额
	 */
	public BigDecimal getSubSum() {
		return subSum;
	}
	/**
	 * 设置：随机立减配置总数量
	 */
	public void setSubNum(Integer subNum) {
		this.subNum = subNum;
	}
	/**
	 * 获取：随机立减配置总数量
	 */
	public Integer getSubNum() {
		return subNum;
	}
	/**
	 * 设置：活动id
	 */
	public void setActId(String actId) {
		this.actId = actId;
	}
	/**
	 * 获取：活动id
	 */
	public String getActId() {
		return actId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：关闭时间
	 */
	public void setShutTime(Date shutTime) {
		this.shutTime = shutTime;
	}
	/**
	 * 获取：关闭时间
	 */
	public Date getShutTime() {
		return shutTime;
	}
	/**
	 * 设置：状态，00可领，01不可领
	 */
	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}
	/**
	 * 获取：状态，00可领，01不可领
	 */
	public String getSubStatus() {
		return subStatus;
	}
	/**
	 * 设置：
	 */
	public void setSubInfo(String subInfo) {
		this.subInfo = subInfo;
	}
	/**
	 * 获取：
	 */
	public String getSubInfo() {
		return subInfo;
	}
	/**
	 * 设置：领取开始时间
	 */
	public void setRevStart(Date revStart) {
		this.revStart = revStart;
	}
	/**
	 * 获取：领取开始时间
	 */
	public Date getRevStart() {
		return revStart;
	}
	/**
	 * 设置：领取结束时间
	 */
	public void setRevEnd(Date revEnd) {
		this.revEnd = revEnd;
	}
	/**
	 * 获取：领取结束时间
	 */
	public Date getRevEnd() {
		return revEnd;
	}
	/**
	 * 设置：场景编号
	 */
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	/**
	 * 获取：场景编号
	 */
	public String getSceneId() {
		return sceneId;
	}
	/**
	 * 设置：事件编码 多个事件用逗号分隔
	 */
	public void setIncCode(String incCode) {
		this.incCode = incCode;
	}
	/**
	 * 获取：事件编码 多个事件用逗号分隔
	 */
	public String getIncCode() {
		return incCode;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreatUser(String creatUser) {
		this.creatUser = creatUser;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreatUser() {
		return creatUser;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	
}
