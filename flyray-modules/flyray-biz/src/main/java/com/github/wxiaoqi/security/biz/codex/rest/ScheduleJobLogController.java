/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.github.wxiaoqi.security.biz.codex.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.codex.entity.ScheduleJobLog;
import com.github.wxiaoqi.security.biz.codex.service.ScheduleJobLogSchemaTaskService;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

/**
 * 定时任务日志
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.2.0 2016-11-28
 */
@RestController
@RequestMapping("marJobLogs")
public class ScheduleJobLogController {
	
	private static Logger logger = LoggerFactory.getLogger(ScheduleJobLogController.class);
	
	@Autowired
	private ScheduleJobLogSchemaTaskService scheduleJobLogService;
	
	/**
	 * 定时任务日志列表
	 */
	@ResponseBody
	@RequestMapping(value = "/page", method = RequestMethod.POST)
	public TableResultResponse<ScheduleJobLog> queryLogList(@RequestParam("page") String page, @RequestParam("limit") String limit, Long jobId){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("page", page);
		params.put("limit", limit);
		if (jobId != null) {
			params.put("jobId", jobId);
		}
		logger.info("查询任务日志列表。。。{}"+params);
		Query query = new Query(params);
		Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		List<ScheduleJobLog> list = scheduleJobLogService.queryLogList(params);
		return new TableResultResponse<ScheduleJobLog>(result.getTotal(), list);
	}
}
