package com.github.wxiaoqi.security.biz.modules.fightGroup.biz;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupCategoryInfo;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupGoodsInfo;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupGoodsPicture;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupInfo;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupMemberInfo;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupCategoryInfoMapper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupGoodsInfoMapper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupGoodsPictureMapper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupInfoMapper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupMemberInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.FightGroupCategoryQueryParam;
import com.github.wxiaoqi.security.common.cms.request.FightGroupCategoryRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 团类目信息表
 *
 * @author he
 * @date 2018-07-12 10:17:33
 */
@Slf4j
@Service
public class FightGroupCategoryInfoBiz extends BaseBiz<FightGroupCategoryInfoMapper,FightGroupCategoryInfo> {


	@Autowired
	private FightGroupGoodsInfoMapper fightGroupGoodsInfoMapper;
	@Autowired
	private FightGroupInfoMapper fightGroupInfoMapper;
	@Autowired
	private FightGroupCategoryInfoMapper fightGroupCategoryInfoMapper;
	@Autowired
	private FightGroupMemberInfoMapper fightGroupMemberInfoMapper;
	@Autowired
	private FightGroupGoodsPictureMapper fightGroupGoodsPictureMapper;

	/**
	 * 查询拼团详情
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryFightGroupDetailInfo(Map<String, Object> request){
		log.info("查询拼团详情请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String goodsId = (String) request.get("goodsId");
		String perId = (String) request.get("perId");
		String groupId = (String) request.get("groupId");
		FightGroupGoodsInfo fightGroupGoodsInfo = new FightGroupGoodsInfo();
		fightGroupGoodsInfo.setPlatformId(platFormId);
		fightGroupGoodsInfo.setMerchantId(merId);
		fightGroupGoodsInfo.setGoodsId(goodsId);
		FightGroupGoodsInfo goodsInfoInfo = fightGroupGoodsInfoMapper.selectOne(fightGroupGoodsInfo);
		
		FightGroupGoodsPicture fightGroupGoodsPicture = new FightGroupGoodsPicture();
		fightGroupGoodsPicture.setPlatformId(platFormId);
		fightGroupGoodsPicture.setMerchantId(merId);
		fightGroupGoodsPicture.setGoodsId(goodsId);
		List<FightGroupGoodsPicture> pictureInfo = fightGroupGoodsPictureMapper.select(fightGroupGoodsPicture);

		//查询团
		FightGroupInfo fightGroupInfo = new FightGroupInfo();
		fightGroupInfo.setPlatformId(platFormId);
		fightGroupInfo.setMerchantId(merId);
		fightGroupInfo.setGroupId(Integer.valueOf(groupId));
		FightGroupInfo selectGroupInfo = fightGroupInfoMapper.selectOne(fightGroupInfo);

		//查询团类目
		FightGroupCategoryInfo fightGroupCategoryInfo = new FightGroupCategoryInfo();
		fightGroupCategoryInfo.setPlatformId(platFormId);
		fightGroupCategoryInfo.setMerchantId(merId);
		fightGroupCategoryInfo.setGoodsId(Integer.valueOf(goodsId));
		fightGroupCategoryInfo.setGroupsId(selectGroupInfo.getGroupsId());
		FightGroupCategoryInfo groupCategoryInfo = fightGroupCategoryInfoMapper.selectOne(fightGroupCategoryInfo);

		//查询团成员
		Example example = new Example(FightGroupMemberInfo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("platformId", platFormId);
		criteria.andEqualTo("merchantId", merId);
		criteria.andEqualTo("groupId", groupId);
		example.setOrderByClause("create_time");
		List<FightGroupMemberInfo> selectByExample = fightGroupMemberInfoMapper.selectByExample(example);
		
		
		FightGroupMemberInfo fightGroupMemberInfo = new FightGroupMemberInfo();
		fightGroupMemberInfo.setPlatformId(platFormId);
		fightGroupMemberInfo.setMerchantId(merId);
		fightGroupMemberInfo.setGroupId(Integer.valueOf(groupId));
		fightGroupMemberInfo.setPerId(selectGroupInfo.getHeadPerId());
		FightGroupMemberInfo selectMemberInfo = fightGroupMemberInfoMapper.selectOne(fightGroupMemberInfo);

		//查询用户是否参团
		fightGroupMemberInfo.setPerId(perId);
		FightGroupMemberInfo selectByPerId = fightGroupMemberInfoMapper.selectOne(fightGroupMemberInfo);
		Timestamp endDate = selectGroupInfo.getEndDate();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, Object> fightGroupMap = EntityUtils.beanToMap(selectGroupInfo);
		Map<String, Object> memberMap = EntityUtils.beanToMap(selectMemberInfo);
		fightGroupMap.putAll(memberMap);
		if(null != selectByPerId){
			//标识已参团
			fightGroupMap.put("isJoin", "00");//00表示已参团
		}
		fightGroupMap.put("endDate", sdf.format(endDate));
		fightGroupMap.put("groupName", groupCategoryInfo.getGroupName());
		fightGroupMap.put("peopleNum", groupCategoryInfo.getPeopleNum());
		fightGroupMap.put("txAmt", groupCategoryInfo.getTxAmt());
		if(StringUtils.isEmpty(groupCategoryInfo.getPeopleNum())){
			fightGroupMap.put("lastPeoplenum", "*");
		}else{
			fightGroupMap.put("lastPeoplenum", groupCategoryInfo.getPeopleNum()-selectGroupInfo.getJoinedGroupNum());
		}

		response.put("pictureInfo", pictureInfo);
		response.put("memberInfo", selectByExample);
		response.put("fightGroupMap", fightGroupMap);
		response.put("goodsInfo", goodsInfoInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("查询拼团详情响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询拼团团类目列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<FightGroupCategoryInfo> queryFightGroupCategoryPage(FightGroupCategoryQueryParam param) {
		log.info("查询拼团团类目列表。。。。{}"+param);
		Example example = new Example(FightGroupCategoryInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if (param.getGroupsId() != null && param.getGroupsId().length() > 0) {
			criteria.andEqualTo("groupsId",param.getGroupsId());
		}
		if (param.getPage() == null) {
			param.setPage(1);
		}
		if (param.getLimit() == null) {
			param.setLimit(9999);
		}
		Page<FightGroupCategoryInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<FightGroupCategoryInfo> list = mapper.selectByExample(example);
		TableResultResponse<FightGroupCategoryInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	public Map<String, Object> addFightGroupCategory(FightGroupCategoryRequestParam param) {
		log.info("添加拼团团类目。。。。。。。。。start。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		FightGroupCategoryInfo categoryInfo = new FightGroupCategoryInfo();
		try {
			BeanUtils.copyProperties(categoryInfo, param);
			String id = String.valueOf(SnowFlake.getId());
			categoryInfo.setGroupsId(Integer.valueOf(id));
			categoryInfo.setGoodsId(Integer.valueOf(param.getGoodsId()));;
			BigDecimal amt = new BigDecimal(param.getTxAmt());
			categoryInfo.setTxAmt(amt);
			categoryInfo.setPeopleNum(Integer.valueOf(param.getPeopleNum()));
			if (mapper.insert(categoryInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加拼团团类目】   响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【添加拼团团类目】  报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加拼团团类目】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【添加拼团团类目】   响应参数：{}", respMap);
		return respMap;
	}
	
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteFightGroupCategory(FightGroupCategoryRequestParam param) {
		log.info("删除拼团团类目。。。。。。。。。start。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		FightGroupCategoryInfo categoryInfo = new FightGroupCategoryInfo();
		try {
			BeanUtils.copyProperties(categoryInfo, param);
			categoryInfo.setId(Long.valueOf(param.getId()));
			if (mapper.delete(categoryInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除拼团团类目】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【删除拼团团类目】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除拼团团类目】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【删除拼团团类目】   响应参数：{}", respMap);
		 return respMap;
	}
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateFightGroupCategory(FightGroupCategoryRequestParam param) {
		log.info("修改拼团团类目。。。。。。。start。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		FightGroupCategoryInfo categoryInfo = new FightGroupCategoryInfo();
		try {
			BeanUtils.copyProperties(categoryInfo, param);
			categoryInfo.setId(Long.valueOf(param.getId()));
			categoryInfo.setGoodsId(Integer.valueOf(param.getGoodsId()));;
			BigDecimal amt = new BigDecimal(param.getTxAmt());
			categoryInfo.setTxAmt(amt);
			categoryInfo.setPeopleNum(Integer.valueOf(param.getPeopleNum()));
			if (mapper.updateByPrimaryKeySelective(categoryInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【修改拼团团类目】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【修改拼团团类目】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【修改拼团团类目】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【修改拼团团类目】   响应参数：{}", respMap);
		 return respMap;
	}
}