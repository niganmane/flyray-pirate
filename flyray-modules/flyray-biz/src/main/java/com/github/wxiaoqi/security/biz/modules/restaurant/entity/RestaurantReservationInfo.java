package com.github.wxiaoqi.security.biz.modules.restaurant.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 预订信息表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Table(name = "restaurant_reservation_info")
public class RestaurantReservationInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//预订id
	@Column(name = "reservation_id")
	private String reservationId;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//用户账号
	@Column(name = "per_id")
	private String perId;

	//预定日期
	@Column(name = "reservation_date")
	private Date reservationDate;

	//用餐时间
	@Column(name = "reservation_time")
	private Date reservationTime;

	//用餐人数
	@Column(name = "reservation_people")
	private String reservationPeople;

	//姓名
	@Column(name = "reservation_name")
	private String reservationName;

	//手机号
	@Column(name = "reservation_phone")
	private String reservationPhone;

	//备注
	@Column(name = "reservation_remark")
	private String reservationRemark;

	//状态 1已预订 2已消费 3已取消
	@Column(name = "status")
	private String status;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	public String getReservationId() {
		return reservationId;
	}
	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerchantId(String merId) {
		this.merchantId = merId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	public String getPerId() {
		return perId;
	}
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 设置：预定日期
	 */
	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}
	/**
	 * 获取：预定日期
	 */
	public Date getReservationDate() {
		return reservationDate;
	}
	/**
	 * 设置：用餐时间
	 */
	public void setReservationTime(Date reservationTime) {
		this.reservationTime = reservationTime;
	}
	/**
	 * 获取：用餐时间
	 */
	public Date getReservationTime() {
		return reservationTime;
	}
	/**
	 * 设置：用餐人数
	 */
	public void setReservationPeople(String reservationPeople) {
		this.reservationPeople = reservationPeople;
	}
	/**
	 * 获取：用餐人数
	 */
	public String getReservationPeople() {
		return reservationPeople;
	}
	/**
	 * 设置：姓名
	 */
	public void setReservationName(String reservationName) {
		this.reservationName = reservationName;
	}
	/**
	 * 获取：姓名
	 */
	public String getReservationName() {
		return reservationName;
	}
	/**
	 * 设置：手机号
	 */
	public void setReservationPhone(String reservationPhone) {
		this.reservationPhone = reservationPhone;
	}
	/**
	 * 获取：手机号
	 */
	public String getReservationPhone() {
		return reservationPhone;
	}
	/**
	 * 设置：备注
	 */
	public void setReservationRemark(String reservationRemark) {
		this.reservationRemark = reservationRemark;
	}
	/**
	 * 获取：备注
	 */
	public String getReservationRemark() {
		return reservationRemark;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
