package com.github.wxiaoqi.security.biz.modules.fightGroup.biz;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupCategoryInfo;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupGoodsInfo;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupGoodsPicture;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupInfo;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupMemberInfo;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupCategoryInfoMapper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupGoodsInfoMapper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupGoodsPictureMapper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupInfoMapper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupMemberInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.FightGroupPicRequestParam;
import com.github.wxiaoqi.security.common.cms.request.FightGroupQueryParam;
import com.github.wxiaoqi.security.common.cms.request.FightGroupRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 拼团商品信息
 *
 * @author he
 * @date 2018-07-10 09:35:10
 */
@Slf4j
@Service
public class FightGroupGoodsInfoBiz extends BaseBiz<FightGroupGoodsInfoMapper,FightGroupGoodsInfo> {


	@Autowired
	private FightGroupGoodsInfoMapper fightGroupGoodsInfoMapper;
	@Autowired
	private FightGroupGoodsPictureMapper fightGroupGoodsPictureMapper;
	@Autowired
	private FightGroupCategoryInfoMapper fightGroupCategoryInfoMapper;
	@Autowired
	private FightGroupInfoMapper fightGroupInfoMapper;
	@Autowired
	private FightGroupMemberInfoMapper fightGroupMemberInfoMapper;

	/**
	 * 查询拼团商品信息列表
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryFightGroupGoodsInfo(Map<String, Object> request){
		log.info("查询拼团商品信息列表请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		FightGroupGoodsInfo fightGroupGoodsInfo = new FightGroupGoodsInfo();
		fightGroupGoodsInfo.setPlatformId(platFormId);
		fightGroupGoodsInfo.setMerchantId(merId);
		List<FightGroupGoodsInfo> goodsInfoList = fightGroupGoodsInfoMapper.select(fightGroupGoodsInfo);
		response.put("goodsInfoList", goodsInfoList);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("查询拼团商品信息列表响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 查询拼团商品信息明细
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryFightGroupGoodsDetailInfo(Map<String, Object> request){
		log.info("查询拼团商品信息明细请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String goodsId = (String) request.get("goodsId");
		String perId = (String) request.get("perId");
		FightGroupGoodsInfo fightGroupGoodsInfo = new FightGroupGoodsInfo();
		fightGroupGoodsInfo.setPlatformId(platFormId);
		fightGroupGoodsInfo.setMerchantId(merId);
		fightGroupGoodsInfo.setGoodsId(goodsId);
		FightGroupGoodsInfo goodsInfoInfo = fightGroupGoodsInfoMapper.selectOne(fightGroupGoodsInfo);
		FightGroupGoodsPicture fightGroupGoodsPicture = new FightGroupGoodsPicture();
		fightGroupGoodsPicture.setPlatformId(platFormId);
		fightGroupGoodsPicture.setMerchantId(merId);
		fightGroupGoodsPicture.setGoodsId(goodsId);
		List<FightGroupGoodsPicture> pictureInfo = fightGroupGoodsPictureMapper.select(fightGroupGoodsPicture);

		//查询团类目
		FightGroupCategoryInfo fightGroupCategoryInfo = new FightGroupCategoryInfo();
		fightGroupCategoryInfo.setPlatformId(platFormId);
		fightGroupCategoryInfo.setMerchantId(merId);
		fightGroupCategoryInfo.setGoodsId(Integer.valueOf(goodsId));
		List<FightGroupCategoryInfo> groupCategoryInfo = fightGroupCategoryInfoMapper.select(fightGroupCategoryInfo);

		//查询已有团
		Example example = new Example(FightGroupInfo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("platformId", platFormId);
		criteria.andEqualTo("merchantId", merId);
		criteria.andEqualTo("goodsId", goodsId);
		example.setOrderByClause("create_time desc");
		List<FightGroupInfo> selectByExample = fightGroupInfoMapper.selectByExample(example);
		List<Map<String, Object>> groupList = new ArrayList<Map<String, Object>>();
		for(FightGroupInfo fightGroupInfo: selectByExample){
			Integer groupId = fightGroupInfo.getGroupId();
			String headPerId = fightGroupInfo.getHeadPerId();
			Integer groupsId = fightGroupInfo.getGroupsId();
			fightGroupCategoryInfo.setGroupsId(groupsId);
			FightGroupCategoryInfo selectCategoryInfo = fightGroupCategoryInfoMapper.selectOne(fightGroupCategoryInfo);

			//查询队长信息
			FightGroupMemberInfo fightGroupMemberInfo = new FightGroupMemberInfo();
			fightGroupMemberInfo.setPlatformId(platFormId);
			fightGroupMemberInfo.setMerchantId(merId);
			fightGroupMemberInfo.setGroupId(groupId);
			fightGroupMemberInfo.setPerId(headPerId);
			FightGroupMemberInfo selectMemberInfo = fightGroupMemberInfoMapper.selectOne(fightGroupMemberInfo);

			//查询用户是否参团
			fightGroupMemberInfo.setPerId(perId);
			FightGroupMemberInfo selectByPerId = fightGroupMemberInfoMapper.selectOne(fightGroupMemberInfo);
			Timestamp endDate = fightGroupInfo.getEndDate();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Map<String, Object> fightGroupMap = EntityUtils.beanToMap(fightGroupInfo);
			Map<String, Object> memberMap = EntityUtils.beanToMap(selectMemberInfo);
			fightGroupMap.putAll(memberMap);
			if(null != selectByPerId){
				//标识已参团
				fightGroupMap.put("isJoin", "00");//00表示已参团
			}
			fightGroupMap.put("endDate", sdf.format(endDate));
			fightGroupMap.put("groupName", selectCategoryInfo.getGroupName());
			fightGroupMap.put("peopleNum", selectCategoryInfo.getPeopleNum());
			groupList.add(fightGroupMap);
		}

		response.put("groupList", groupList);
		response.put("groupCategoryInfo", groupCategoryInfo);
		response.put("goodsInfo", goodsInfoInfo);
		response.put("pictureInfo", pictureInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("查询拼团商品信息明细响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询拼团商品信息列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<FightGroupGoodsInfo> queryFightGroupGoodsInfoPage(FightGroupQueryParam param) {
		log.info("查询拼团商品信息列表。。。。{}"+param);
		Example example = new Example(FightGroupGoodsInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if (param.getGoodsName() != null && param.getGoodsName().length() > 0) {
			criteria.andEqualTo("goodsName",param.getGoodsName());
		}
		if (param.getPage() == null) {
			param.setPage(1);
		}
		if (param.getLimit() == null) {
			param.setLimit(9999);
		}
		Page<FightGroupGoodsInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<FightGroupGoodsInfo> list = mapper.selectByExample(example);
		
		//图片
		if (list != null && list.size() > 0) {
			for (FightGroupGoodsInfo fightGroupGoodsInfo : list) {
				FightGroupGoodsPicture fightGroupGoodsPicture = new FightGroupGoodsPicture();
				fightGroupGoodsPicture.setPlatformId(fightGroupGoodsInfo.getPlatformId());
				fightGroupGoodsPicture.setMerchantId(fightGroupGoodsInfo.getMerchantId());
				fightGroupGoodsPicture.setGoodsId(fightGroupGoodsInfo.getGoodsId());
				List<FightGroupGoodsPicture> pictures = fightGroupGoodsPictureMapper.select(fightGroupGoodsPicture);
				fightGroupGoodsInfo.setPictures(pictures);
			}
		}
		
		TableResultResponse<FightGroupGoodsInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	
	
	/**
	 * 添加商品
	 * @param param
	 * @return
	 */
	public Map<String, Object> addGoodsInfo(FightGroupRequestParam param) {
		log.info("添加拼团商品。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		FightGroupGoodsInfo goodsInfo = new FightGroupGoodsInfo();
		try {
			BeanUtils.copyProperties(goodsInfo, param);
			goodsInfo.setSoldNum(0);
			goodsInfo.setOriginalPrice(new BigDecimal(param.getOriginalPrice()));
			goodsInfo.setDiscountPrice(new BigDecimal(param.getDiscountPrice()));
			if (mapper.insert(goodsInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加拼团商品】   响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【添加拼团商品】  报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加拼团商品】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【添加拼团商品】   响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 删除商品
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteGoodsInfo(FightGroupRequestParam param) {
		log.info("删除商品。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		FightGroupGoodsInfo goodsInfo = new FightGroupGoodsInfo();
		try {
			BeanUtils.copyProperties(goodsInfo, param);
			goodsInfo.setId(Long.valueOf(param.getId()));
			if (mapper.delete(goodsInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除商品】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【删除商品】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除商品】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【删除商品】   响应参数：{}", respMap);
		 return respMap;
	}
	
	/**
	 * 修改商品
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateGoodsInfo(FightGroupRequestParam param) {
		log.info("修改商品。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		FightGroupGoodsInfo goodsInfo = new FightGroupGoodsInfo();
		try {
			BeanUtils.copyProperties(goodsInfo, param);
			goodsInfo.setId(Long.valueOf(param.getId()));
			goodsInfo.setOriginalPrice(new BigDecimal(param.getOriginalPrice()));
			goodsInfo.setDiscountPrice(new BigDecimal(param.getDiscountPrice()));
			if (mapper.updateByPrimaryKeySelective(goodsInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【修改拼团商品】   响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【修改拼团商品】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【修改拼团商品】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【修改拼团商品】   响应参数：{}", respMap);
		 return respMap;
	}
	
	
	/**
	 * 添加商品图片
	 * @param param
	 * @return
	 */
	public Map<String, Object> addPic(FightGroupPicRequestParam param) {
		log.info("添加商品图片。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		FightGroupGoodsPicture picInfo = new FightGroupGoodsPicture();
		try {
			BeanUtils.copyProperties(picInfo, param);
			if (fightGroupGoodsPictureMapper.insert(picInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加商品图片】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【添加商品图片】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加商品图片】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【添加商品图片   响应参数：{}", respMap);
		 return respMap;
	}
	
	/**
	 * 删除商品图片
	 * @param param
	 * @return
	 */
	public Map<String, Object> deletePic(FightGroupPicRequestParam param) {
		log.info("删除商品图片。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		FightGroupGoodsPicture picInfo = new FightGroupGoodsPicture();
		try {
			BeanUtils.copyProperties(picInfo, param);
			picInfo.setId(Long.valueOf(param.getId()));
			if (fightGroupGoodsPictureMapper.delete(picInfo) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除商品图片】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【删除商品图片】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除商品图片】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【删除商品图片   响应参数：{}", respMap);
		 return respMap;
	}

}