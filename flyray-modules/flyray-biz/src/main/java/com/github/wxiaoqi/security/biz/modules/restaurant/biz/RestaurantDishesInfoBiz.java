package com.github.wxiaoqi.security.biz.modules.restaurant.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantDishesInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantDishesInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 菜品信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Slf4j
@Service
public class RestaurantDishesInfoBiz extends BaseBiz<RestaurantDishesInfoMapper,RestaurantDishesInfo> {
	
	
	@Autowired
	private RestaurantDishesInfoMapper restaurantDishesInfoMapper;
	
	/**
	 * 菜品详情查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryDishesDetailInfo(Map<String, Object> request){
		log.info("菜品详情查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String dishesId = (String) request.get("dishesId");
		RestaurantDishesInfo restaurantDishesInfo = new RestaurantDishesInfo();
		restaurantDishesInfo.setPlatformId(platFormId);
		restaurantDishesInfo.setMerchantId(merId);
		restaurantDishesInfo.setDishesId(dishesId);
		RestaurantDishesInfo selectDishesInfo = restaurantDishesInfoMapper.selectOne(restaurantDishesInfo);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("map", selectDishesInfo);
 		response.put("dishesInfo", map);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("菜品详情查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询菜品列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<RestaurantDishesInfo> queryRestaurantDishesInfoPage(RestaurantQueryParam param) {
		log.info("查询菜品列表。。。。{}"+param);
		Example example = new Example(RestaurantDishesInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
//		if (param.getGroupsId() != null && param.getGroupsId().length() > 0) {
//			criteria.andEqualTo("groupsId",param.getGroupsId());
//		}
		if (param.getPage() == null) {
			param.setPage(1);
		}
		if (param.getLimit() == null) {
			param.setLimit(9999);
		}
		Page<RestaurantDishesInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<RestaurantDishesInfo> list = mapper.selectByExample(example);
		TableResultResponse<RestaurantDishesInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteRestaurantDishe(Map<String, Object> param) {
		log.info("删除菜品。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Integer id = (Integer) param.get("id");
			RestaurantDishesInfo dishesInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (dishesInfo != null) {
				mapper.delete(dishesInfo);
				respMap.put("code", "00");
				respMap.put("msg", "请求成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "记录不存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		return respMap;
	}
	
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	public Map<String, Object> addRestaurantDishe(Map<String, Object> param) {
		log.info("添加菜品。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			String platformId = (String) param.get("platformId");
			String merId = (String) param.get("merchantId");
			String categoryId = (String) param.get("categoryId");
			String dishesName = (String) param.get("dishesName");
			String instructions = (String) param.get("instructions");
			String imageUrl = (String) param.get("imageUrl");
			String price = (String) param.get("price");
			String isSpecification = (String) param.get("isSpecification");
			String type = (String) param.get("type");
			RestaurantDishesInfo dishesInfo = new RestaurantDishesInfo();
			dishesInfo.setDishesId(String.valueOf(SnowFlake.getId()));
			dishesInfo.setPlatformId(platformId);
			dishesInfo.setMerchantId(merId);
			dishesInfo.setCategoryId(Integer.valueOf(categoryId));
			dishesInfo.setDishesName(dishesName);
			dishesInfo.setInstructions(instructions);
			dishesInfo.setImageUrl(imageUrl);
			dishesInfo.setOrderNum(0);
			dishesInfo.setPrice(price);
			dishesInfo.setIsSpecification(isSpecification);
			dishesInfo.setType(type);
			mapper.insert(dishesInfo);
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		return respMap;
	}
	
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateRestaurantDishe(Map<String, Object> param) {
		log.info("修改菜品。。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			String categoryId = (String) param.get("categoryId");
			String dishesName = (String) param.get("dishesName");
			String instructions = (String) param.get("instructions");
			String imageUrl = (String) param.get("imageUrl");
			String price = (String) param.get("price");
			String isSpecification = (String) param.get("isSpecification");
			String type = (String) param.get("type");
			Integer id = (Integer) param.get("id");
			RestaurantDishesInfo dishesInfo = mapper.selectByPrimaryKey(Long.valueOf(id));
			if (dishesInfo != null) {
				dishesInfo.setCategoryId(Integer.valueOf(categoryId));
				dishesInfo.setDishesName(dishesName);
				dishesInfo.setInstructions(instructions);
				dishesInfo.setImageUrl(imageUrl);
				dishesInfo.setPrice(price);
				dishesInfo.setIsSpecification(isSpecification);
				dishesInfo.setType(type);
				mapper.updateByPrimaryKeySelective(dishesInfo);
				respMap.put("code", "00");
				respMap.put("msg", "请求成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "记录不存在");
			}
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		return respMap;
	}
}