package com.github.wxiaoqi.security.biz.modules.restaurant.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 订单表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Table(name = "restaurant_order_info")
public class RestaurantOrderInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//用户账号
	@Column(name = "customer_id")
	private String customerId;
	
	//支付订单号
	@Column(name = "pay_order_no")
	private String payOrderNo;

	//金额
	@Column(name = "price")
	private BigDecimal price;

	//支付时间
	@Column(name = "pay_time")
	private Date payTime;

	//状态 00支付成功 01支付失败 02退款成功 03退款失败 04待支付
	@Column(name = "status")
	private String status;

	//餐桌id
	@Column(name = "table_id")
	private String tableId;
	
	//预订id
	@Column(name = "reservation_id")
	private String reservationId;
	
	//配送id
	@Column(name = "distribution_id")
	private String distributionId;

	//备注
	@Column(name = "remark")
	private String remark;
	
	//类型 1点餐 2预订 3外卖
	@Column(name = "type")
	private String type;
	
	//确认收货标识 type=3时有值 0是 1否
	@Column(name = "is_confirm")
	private String isConfirm;
	
	//评论标识 type=3时有值 0已评论 1未评论
	@Column(name = "is_appraisal")
	private String isAppraisal;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerchantId(String merId) {
		this.merchantId = merId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getPayOrderNo() {
		return payOrderNo;
	}
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	/**
	 * 设置：金额
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	/**
	 * 获取：金额
	 */
	public BigDecimal getPrice() {
		return price;
	}
	/**
	 * 设置：支付时间
	 */
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	/**
	 * 获取：支付时间
	 */
	public Date getPayTime() {
		return payTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getReservationId() {
		return reservationId;
	}
	public void setReservationId(String reservationId) {
		this.reservationId = reservationId;
	}
	public String getDistributionId() {
		return distributionId;
	}
	public void setDistributionId(String distributionId) {
		this.distributionId = distributionId;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIsConfirm() {
		return isConfirm;
	}
	public void setIsConfirm(String isConfirm) {
		this.isConfirm = isConfirm;
	}
	public String getIsAppraisal() {
		return isAppraisal;
	}
	public void setIsAppraisal(String isAppraisal) {
		this.isAppraisal = isAppraisal;
	}
}
