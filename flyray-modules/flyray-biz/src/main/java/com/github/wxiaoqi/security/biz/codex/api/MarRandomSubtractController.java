package com.github.wxiaoqi.security.biz.codex.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.wxiaoqi.security.biz.codex.biz.MarRandomSubtractBiz;
import com.github.wxiaoqi.security.biz.codex.entity.MarRandomSubtract;
import com.github.wxiaoqi.security.common.rest.BaseController;

@Controller
@RequestMapping("marRandomSubtract")
public class MarRandomSubtractController extends BaseController<MarRandomSubtractBiz,MarRandomSubtract> {

}