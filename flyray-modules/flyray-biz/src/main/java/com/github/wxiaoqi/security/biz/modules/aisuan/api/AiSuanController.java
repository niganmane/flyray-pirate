package com.github.wxiaoqi.security.biz.modules.aisuan.api;

import com.github.wxiaoqi.security.biz.modules.aisuan.biz.AisuanAdviceBiz;
import com.github.wxiaoqi.security.biz.modules.aisuan.biz.AisuanAmtBiz;
import com.github.wxiaoqi.security.biz.modules.aisuan.biz.AisuanRecommendBiz;
import com.github.wxiaoqi.security.biz.modules.aisuan.biz.AisuanRecordBiz;
import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanAdvice;
import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanRecommend;
import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanRecord;
import com.github.wxiaoqi.security.common.cms.request.AisuanAdviceAddparam;
import com.github.wxiaoqi.security.common.cms.request.AisuanAdviceQueryAdminParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanAmtQueryOneParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanNoCompleteRecordQueryParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanQueryRecommendParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordAddParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordQueryOneParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordQueryParam;
import com.github.wxiaoqi.security.common.cms.request.AisuanRecordReplyParam;
import com.github.wxiaoqi.security.common.cms.request.BaseParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.ResponseHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(tags="爱算接口")
@Controller
@RequestMapping("aisuan")
@Slf4j
public class AiSuanController {
	@Autowired
	private AisuanRecordBiz aisuanRecordBiz;
	@Autowired
	private AisuanAdviceBiz aisuanAdviceBiz;
	@Autowired
	private AisuanRecommendBiz aisuanRecommendBiz;
	@Autowired
	private AisuanAmtBiz aisuanAmtBiz;
	
	/**
	 * 添加预测-免费风水咨询
	 */
	@ApiOperation("新加预测")
	@RequestMapping(value = "/addForeCast",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> addForeCast(@RequestBody AisuanRecordAddParam param){
		log.info("新加预测传入参数{}", EntityUtils.beanToMap(param));
		try {
			aisuanRecordBiz.save(param);
			return ResponseHelper.success(param, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseHelper.success(param, null, ResponseCode.ABNORMAL_FIELDS.getCode(), ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		
	}
	/**
	 * 回复,对之前的咨询提出咨询	
	 */
	@ApiOperation("对之前预测提问")
	@RequestMapping(value = "/addForeCastAgain",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> addForeCastAgain(@RequestBody AisuanRecordReplyParam param){
		log.info("对之前预测提问传入参数{}", EntityUtils.beanToMap(param));
		try {
			//aisuanRecordBiz.save(param);
			aisuanRecordBiz.ask(param);
			return ResponseHelper.success(param, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseHelper.success(param, null, ResponseCode.ABNORMAL_FIELDS.getCode(), ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		
	}
	
	/**
	 * 添加建议
	 */
	@ApiOperation("新加建议")
	@RequestMapping(value = "/addAdvice",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> addAdvice(@RequestBody AisuanAdviceAddparam param){
		log.info("新加建议传入参数{}", EntityUtils.beanToMap(param));
		try {
			aisuanAdviceBiz.save(param);
			return ResponseHelper.success(param, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseHelper.success(param, null, ResponseCode.ABNORMAL_FIELDS.getCode(), ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
	}
	/**
	 * 查询预测
	 */
	@ApiOperation("查询预测")
	@RequestMapping(value = "/queryForeCast",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryForeCast(@RequestBody AisuanRecordQueryParam param){
		log.info("查询预测传入参数{}", EntityUtils.beanToMap(param));
		List<AisuanRecord> list = aisuanRecordBiz.query(param);
		return ResponseHelper.success(list, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	/**
	 * 我的建议
	 */
	@ApiOperation("查询我的建议")
	@RequestMapping(value = "/queryAdvice",method = RequestMethod.GET)
    @ResponseBody
	public Map<String, Object> queryAdvice(@RequestParam AisuanAdviceQueryAdminParam param){
		log.info("查询我的建议传入参数{}", EntityUtils.beanToMap(param));
		List<AisuanAdvice> list = aisuanAdviceBiz.query(param);
		return ResponseHelper.success(list, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	/**
	 * 推荐预测人
	 */
	@ApiOperation("查询推荐人")
	@RequestMapping(value = "/queryRecommend",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryRecommend(@RequestBody BaseParam param){
		log.info("查询推荐人传入参数{}", EntityUtils.beanToMap(param));
		List<AisuanRecommend> list = aisuanRecommendBiz.query();
		return ResponseHelper.success(list, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	/**
	 * 查询单个咨询人
	 */
	@ApiOperation("查询单个推荐人")
	@RequestMapping(value = "/queryOneRecommend",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryOneRecommend(@RequestBody AisuanQueryRecommendParam param){
		log.info("查询单个推荐人传入参数{}", EntityUtils.beanToMap(param));
		AisuanRecommend recommend = new AisuanRecommend();
		recommend.setUserId(param.getUserId());
		recommend = aisuanRecommendBiz.selectOne(recommend);
		return ResponseHelper.success(recommend, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
	@RequestMapping(value = "/queryReplyRecord",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryReplyRecord(@RequestBody AisuanRecordQueryOneParam param){
		log.info("查询单条咨询记录回复传入参数{}", EntityUtils.beanToMap(param));
    	List<AisuanRecord> list = aisuanRecordBiz.queryReplyRecord(param);
    	return ResponseHelper.success(list, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
    }
	/**
	 * 根据编号查询
	 */
	@RequestMapping(value = "/queryAmt",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryAmt(@RequestBody AisuanAmtQueryOneParam param){
		log.info("根据编号查询传入参数{}", EntityUtils.beanToMap(param));
		return aisuanAmtBiz.queryByType(param);
    }
	/**
	 * 咨询完成
	 */
	@RequestMapping(value = "/complete",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> complete(@RequestBody AisuanRecordQueryOneParam param){
		log.info("咨询完成传入参数{}", EntityUtils.beanToMap(param));
		return aisuanRecordBiz.complete(param);
    }
	/**
	 * 判断当前用户有没有，未完成的指定预测人订单
	 */
	@RequestMapping(value = "/queryNoCompleteRecord",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryNoCompleteRecord(@RequestBody AisuanNoCompleteRecordQueryParam param){
		log.info("判断当前用户有没有，未完成的指定预测人订单传入参数{}", EntityUtils.beanToMap(param));
		return aisuanRecordBiz.queryNoCompleteRecord(param);
		
	}
	
	@RequestMapping(value = "/queryOneRecordAskAndReply",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryOneRecordAskAndReply(@RequestBody AisuanRecordQueryOneParam param) {
		log.info("查询单条咨询记录及回复传入参数{}", EntityUtils.beanToMap(param));
		return aisuanRecordBiz.queryOneRecordAskAndReply(param);
	}
}
