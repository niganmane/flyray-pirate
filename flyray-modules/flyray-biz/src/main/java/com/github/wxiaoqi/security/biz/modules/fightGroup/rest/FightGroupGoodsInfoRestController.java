package com.github.wxiaoqi.security.biz.modules.fightGroup.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.fightGroup.biz.FightGroupGoodsInfoBiz;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupGoodsInfo;
import com.github.wxiaoqi.security.common.cms.request.FightGroupPicRequestParam;
import com.github.wxiaoqi.security.common.cms.request.FightGroupQueryParam;
import com.github.wxiaoqi.security.common.cms.request.FightGroupRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序拼团商品信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("fightGroup/goodsInfo")
public class FightGroupGoodsInfoRestController extends BaseController<FightGroupGoodsInfoBiz, FightGroupGoodsInfo> {
	
	@Autowired
	private FightGroupGoodsInfoBiz fightGroupGoodsInfoBiz;
	
	/**
	 * 查询拼团商品信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<FightGroupGoodsInfo> query(@RequestBody FightGroupQueryParam param) {
		log.info("查询拼团商品信息列表------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return fightGroupGoodsInfoBiz.queryFightGroupGoodsInfoPage(param);
	}
	
	/**
	 * 添加拼团商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody FightGroupRequestParam param) {
		log.info("添加拼团商品------start------{}", param);
		Map<String, Object> respMap = fightGroupGoodsInfoBiz.addGoodsInfo(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("添加拼团商品-----end-------------{}");
		return respMap;
	}
	
	/**
	 * 删除商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody FightGroupRequestParam param) {
		log.info("删除商品------start------{}", param);
		Map<String, Object> respMap = fightGroupGoodsInfoBiz.deleteGoodsInfo(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除商品------end------{}", respMap);
		return respMap;
	}

	/**
	 * 修改商品
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody FightGroupRequestParam param) {
		log.info("修改商品------start------{}", param);
		Map<String, Object> respMap = fightGroupGoodsInfoBiz.updateGoodsInfo(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("修改商品------end------{}", respMap);
		return respMap;
	}
	
	/**
	 * 添加商品图片
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addPic", method = RequestMethod.POST)
	public Map<String, Object> addPic(@RequestBody FightGroupPicRequestParam param) {
		log.info("添加商品图片------start------{}", param);
		Map<String, Object> respMap = fightGroupGoodsInfoBiz.addPic(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("添加商品图片------end------{}", respMap);
		return respMap;
	}
	
	
	/**
	 * 删除商品图片
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deletePic", method = RequestMethod.POST)
	public Map<String, Object> deletePic(@RequestBody FightGroupPicRequestParam param) {
		log.info("删除商品图片------start------{}", param);
		Map<String, Object> respMap = fightGroupGoodsInfoBiz.deletePic(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除商品图片------end------{}", respMap);
		return respMap;
	}
}
