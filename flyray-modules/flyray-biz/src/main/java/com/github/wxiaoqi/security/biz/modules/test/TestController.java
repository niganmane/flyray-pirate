package com.github.wxiaoqi.security.biz.modules.test;

import com.github.wxiaoqi.security.cache.CacheProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *　@author bolei
 *　@date Oct 5, 2018
　*　@description 
**/

@Slf4j
@Controller
@RequestMapping("/test")
public class TestController {

	@Autowired
	private TestService userService;
    @ResponseBody
    @RequestMapping("index")
    public String index(){
    	
    	final User user = userService.saveOrUpdate(new User(5L, "u5", "p5"));
		log.info("[saveOrUpdate] - [{}]", user);
		final User user1 = userService.get(5L);
		log.info("[get] - [{}]", user1);
		userService.delete(5L);

        String str =  "";

        CacheProvider.set("tyh", "aaaaaaaaaaaaaaaaaa");
        str += "|";
        str += CacheProvider.get("tyh");
        str += "|";
        str += CacheProvider.del("tyh");

        str += "|||";


        CacheProvider.set("users", user);
        str += "|";
        str += CacheProvider.get("users", User.class);
        str += "|";
        str += CacheProvider.del("users");

        return str.toString();
    }
}
