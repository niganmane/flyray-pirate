package com.github.wxiaoqi.security.biz.modules.restaurant.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantDishesCategoryInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 菜品类目表
 * 
 * @author he
 * @date 2018-06-29 10:31:27
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantDishesCategoryInfoMapper extends Mapper<RestaurantDishesCategoryInfo> {
	
	List<RestaurantDishesCategoryInfo> queryRestaurantDishesCategoryInfo(Map<String, Object> map);
	
}
