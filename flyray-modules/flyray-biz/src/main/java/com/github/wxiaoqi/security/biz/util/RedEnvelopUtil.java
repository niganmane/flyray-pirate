package com.github.wxiaoqi.security.biz.util;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.util.Random;


/**
 * 红包工具类
 * 
 * @author Hong
 */
public class RedEnvelopUtil {

	/** 随机种子 **/
	public final static Random random = new Random();
    static DecimalFormat df   = new DecimalFormat("######0.00");
    /**
     * total 总钱数
     * count 红包个数
     * max 红包最大值 
     * min 红包最小值
     * 
     * */
	public static double[] generate(double total, int count, double max, double min) {
        //生成 count 个
        double[] result = new double[count];
        double average = total / count;
        for (int i = 0; i < result.length; i++) {
            //因为小红包的数量通常是要比大红包的数量要多的，因为这里的概率要调换过来。
            //当随机数>平均值，则产生小红包
            //当随机数<平均值，则产生大红包
            double temp = 0;
            if (nextLong(min, max) > average) {
                // 在平均线上减钱
                temp = min + xRandom(min, average);
                temp =Double.valueOf(df.format(temp));
                result[i] = temp;
               // total -= temp;
                total=Double.valueOf(df.format(total-temp));
            } else {
                // 在平均线上加钱
                temp = max - xRandom(average, max);
                temp =Double.valueOf(df.format(temp));
                result[i] = temp;
                //total -= temp;
                total=Double.valueOf(df.format(total-temp));
            }
 
        }
        double alltotal=0;
        for (int i = 0; i < result.length; i++) {
            // System.out.println("result[" + i + "]:" + result[i]);
            // System.out.println(result[i]);
        	alltotal += result[i];
        	// b=b.add(new BigDecimal(result[i]));
        }
        // 如果还有余钱，则尝试加到小红包里，如果加不进去，则尝试下一个。
        while (total> 0) {
        	//System.out.println("剩余的红包钱--###"+total);
            for (int i = 0; i < result.length; i++) {
                if (total > 0 && result[i] < max) {
                    result[i] =Double.valueOf(df.format(result[i]+0.01));
                    //total -= 0.01;
                    total= Double.valueOf(df.format(total- 0.01));
              //      System.out.println("存在剩余的红包--###"+ total);
                }
            }
        }
        // 如果钱是负数了，还得从已生成的小红包中抽取回来
        while (total < 0) {
        	//System.out.println("剩余的红包钱--###"+total);
            for (int i = 0; i < result.length; i++) {
                if (total < 0 && result[i] > min) {
                    result[i] =Double.valueOf(df.format(result[i]-0.01));
                    //total+=0.01;
                    total= Double.valueOf(df.format(total+0.01));
         //           System.out.println("剩余的红包为负数--###"+total);
                }
            }
        }
        return result;
    }
    static double xRandom(double min, double max) {
        return sqrt(nextLong(sqr(max - min)));
    }
	static double sqrt(double n) {
	        return (double) Math.sqrt(n);
	    }

	    static double sqr(double n) {
	        return n * n;
	    }

	    static double nextLong(double n) {
	        return random.nextDouble()*n;
	    }

	    static double nextLong(double min, double max) {
	        return random.nextDouble()*(max - min) + min;
	    }
	//获取指定范围的随机数
	public static int getRandomnext(int seed){
		return Math.abs(random.nextInt()%seed);
	}
//	public static void main(String[] args) {
//		 System.out.println(getRandomnext(10));
//	}

}
