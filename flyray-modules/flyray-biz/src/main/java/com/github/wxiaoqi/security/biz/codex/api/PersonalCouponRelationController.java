package com.github.wxiaoqi.security.biz.codex.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.wxiaoqi.security.biz.codex.biz.PersonalCouponRelationBiz;
import com.github.wxiaoqi.security.biz.codex.entity.PersonalCouponRelation;
import com.github.wxiaoqi.security.common.rest.BaseController;

@Controller
@RequestMapping("personalCouponRelation")
public class PersonalCouponRelationController extends BaseController<PersonalCouponRelationBiz,PersonalCouponRelation> {

}