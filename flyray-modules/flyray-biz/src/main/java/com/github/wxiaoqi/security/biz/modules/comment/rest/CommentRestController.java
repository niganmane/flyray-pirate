package com.github.wxiaoqi.security.biz.modules.comment.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.comment.biz.CommentBiz;
import com.github.wxiaoqi.security.biz.modules.comment.entity.Comment;
import com.github.wxiaoqi.security.common.cms.request.CmsQueryCommentParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序后台评论管理相关接口
 * @author Administrator
 *
 */
@Slf4j
@Controller
@RequestMapping("comments")
public class CommentRestController extends BaseController<CommentBiz, Comment> {
	
	@Autowired
	private CommentBiz biz;
	
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<Comment> query(@RequestBody CmsQueryCommentParam param) {
		log.info("查询所有评论------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return biz.queryComments(param); 
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody CmsQueryCommentParam param) {
		log.info("删除评论------start------{}", param);
		Map<String, Object> respMap = biz.deleteComment(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除评论------end------{}", respMap);
		return respMap;
	}
}
