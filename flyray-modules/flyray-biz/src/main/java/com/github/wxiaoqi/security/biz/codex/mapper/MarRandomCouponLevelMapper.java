package com.github.wxiaoqi.security.biz.codex.mapper;

import com.github.wxiaoqi.security.biz.codex.entity.MarRandomCouponLevel;

import tk.mybatis.mapper.common.Mapper;

/**
 * 随机红包等级
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@org.apache.ibatis.annotations.Mapper
public interface MarRandomCouponLevelMapper extends Mapper<MarRandomCouponLevel> {
	
}
