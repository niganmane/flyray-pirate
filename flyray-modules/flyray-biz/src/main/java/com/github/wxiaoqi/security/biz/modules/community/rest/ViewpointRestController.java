package com.github.wxiaoqi.security.biz.modules.community.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.community.biz.CommunityViewpointBiz;
import com.github.wxiaoqi.security.biz.modules.community.entity.CommunityViewpoint;
import com.github.wxiaoqi.security.common.cms.request.CmsQueryViewPointParam;
import com.github.wxiaoqi.security.common.cms.request.CmsViewPointRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序后台观点管理相关接口
 * @author Administrator
 *
 */
@Slf4j
@Controller
@RequestMapping("viewpoints")
public class ViewpointRestController extends BaseController<CommunityViewpointBiz, CommunityViewpoint> {

	@Autowired
	private CommunityViewpointBiz biz;
	
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<CommunityViewpoint> query(@RequestBody CmsQueryViewPointParam param) {
		log.info("查询所有观点------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return biz.queryViewpoints(param);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody CmsViewPointRequestParam param) {
		log.info("删除观点------start------{}", param);
		Map<String, Object> respMap = biz.deleteViewpoint(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除观点------end------{}", respMap);
		return respMap;
	}
}
