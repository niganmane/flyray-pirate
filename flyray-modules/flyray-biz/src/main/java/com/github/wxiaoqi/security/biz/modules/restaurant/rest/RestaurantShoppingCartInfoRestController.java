package com.github.wxiaoqi.security.biz.modules.restaurant.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantShoppingCartInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantShoppingCartInfo;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序点餐购物车信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/shoppingCart")
public class RestaurantShoppingCartInfoRestController extends BaseController<RestaurantShoppingCartInfoBiz, RestaurantShoppingCartInfo> {
	
	@Autowired
	private RestaurantShoppingCartInfoBiz restaurantShoppingCartInfoBiz;
	
	/**
	 * 查询点餐购物车信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<RestaurantShoppingCartInfo> query(@RequestBody RestaurantQueryParam param) {
		log.info("查询点餐购物车信息列表------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return restaurantShoppingCartInfoBiz.queryRestaurantShoppingCartInfoPage(param);
	}

}
