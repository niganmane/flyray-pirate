package com.github.wxiaoqi.security.biz.codex.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.wxiaoqi.security.biz.codex.entity.MarCouponRev;

import tk.mybatis.mapper.common.Mapper;

/**
 * 红包领取表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@org.apache.ibatis.annotations.Mapper
public interface MarCouponRevMapper extends Mapper<MarCouponRev> {
	/**
	 * 根据红包id统计红包领取数量
	 * @param couId 红包id
	 * */
	int queryCountCoupRev(@Param(value="couId") String couId);
	
	/**
	 * 统计当天指定用户指定红包领取次数
	 * @param dateTime 当前时间 merCusrNo 商户会员号 couId 红包id
	 * */
	int countCounpRevByday(@Param(value="dateTime") String dateTime,@Param(value="merCusrNo") String merCusrNo,@Param(value="couId") String couId);
	/**
	 * 统计指定时间指定用户指定红包领取次数 每周或者每月
	 * @param beginTime 开始日期 endTime 结束日期 merCusrNo 商户会员号 couId 红包id
	 * */
	int countCounpRevBydate(@Param(value="beginTime") String beginTime , @Param(value="endTime") String  endTime, @Param(value="merCusrNo") String merCusrNo,@Param(value="couId") String  couId);
	/**
	 * 根据商户会员号与使用场景查询可用红包
	 * @param merCustNo 商户会员号merCustNo sceneId 场景编号sceneId
	 * */
	List<MarCouponRev> selectUseAble(@Param(value="merCustNo") String merCustNo, @Param(value="sceneId") String sceneId);
	
	List<MarCouponRev> selectUseAbleBetween(@Param(value="merCustNo") String merCustNo, @Param(value="sceneId") String sceneId);
	/**
	 * 查询红包领取个数
	 * @author centerroot
	 * @time 创建时间:2018年2月27日上午9:50:47
	 * @param actId
	 * @return
	 */
	Integer getCouponRevNum();
	
	/**
	 * 查询活动中红包领取个数
	 * @author centerroot
	 * @time 创建时间:2018年2月24日下午2:35:20
	 * @param actId
	 * @return
	 */
	Integer getActivityCouponRevNum(String actId);
	
	/**
	 * 统计随机红包领取金额
	 * @param couId 红包id
	 * 
	 * */
	String getCouponAmtSum(String couId);
	
	/**
	 * 根据支付订单号查询是否领取过红包
	 * @param merCustNo
	 * @param sceneId
	 * @return
	 */
	Integer getCouponRevByOrderNoAndIncCode(@Param(value="merOrderNo") String merOrderNo, @Param(value="incCode") String incCode);
	
	/**
	 * 获取用户领取的红包
	 * @param merCusrNo 会员号; couId 红包id
	 * */
	List<MarCouponRev> getCouponRev(@Param(value="merCusrNo") String merCusrNo, @Param(value="couId") String couId);
}
