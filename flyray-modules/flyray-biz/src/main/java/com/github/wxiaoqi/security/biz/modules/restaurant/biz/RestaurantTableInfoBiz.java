package com.github.wxiaoqi.security.biz.modules.restaurant.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantTableInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantTableInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.cms.request.RestaurantTableRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 餐桌信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Slf4j
@Service
public class RestaurantTableInfoBiz extends BaseBiz<RestaurantTableInfoMapper,RestaurantTableInfo> {

	@Autowired
	private RestaurantInfoMapper restaurantInfoMapper;
	@Autowired
	private RestaurantTableInfoMapper restaurantTableInfoMapper;

	/**
	 * 餐桌信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryTableInfo(Map<String, Object> request){
		log.info("餐桌信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		RestaurantInfo restaurantInfo = new RestaurantInfo();
		restaurantInfo.setPlatformId(platFormId);
		restaurantInfo.setMerchantId(merId);
		RestaurantInfo selectRestaurantInfo = restaurantInfoMapper.selectOne(restaurantInfo);
		Map<String, Object> map = EntityUtils.beanToMap(selectRestaurantInfo);
		RestaurantTableInfo restaurantTableInfo = new RestaurantTableInfo();
		restaurantTableInfo.setPlatformId(platFormId);
		restaurantTableInfo.setMerchantId(merId);
		List<RestaurantTableInfo> selectRestaurantTableInfo = restaurantTableInfoMapper.select(restaurantTableInfo);
		response.put("info", selectRestaurantTableInfo);
		response.put("company", map);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("餐桌信息查询响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 查询餐桌信息列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<RestaurantTableInfo> queryRestaurantTableInfoPage(RestaurantQueryParam param) {
		log.info("查询餐桌信息列表。。。。{}"+param);
		Example example = new Example(RestaurantTableInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if (param.getTableName() != null && param.getTableName().length() > 0) {
			criteria.andEqualTo("tableName",param.getTableName());
		}
		if (param.getPage() == null) {
			param.setPage(1);
		}
		if (param.getLimit() == null) {
			param.setLimit(9999);
		}
		Page<RestaurantTableInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<RestaurantTableInfo> list = mapper.selectByExample(example);
		TableResultResponse<RestaurantTableInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	/**
	 * 删除餐桌
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteRestaurantTableInfo(RestaurantTableRequestParam param) {
		log.info("删除餐桌。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		RestaurantTableInfo info = new RestaurantTableInfo();
		try {
			BeanUtils.copyProperties(info, param);
			info.setId(Long.valueOf(param.getId()));
			if (mapper.delete(info) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除餐桌】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【删除餐桌】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除餐桌】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【删除餐桌】   响应参数：{}", respMap);
		 return respMap;
	}
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	public Map<String, Object> addRestaurantTableInfo(RestaurantTableRequestParam param) {
		log.info("添加餐桌。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		RestaurantTableInfo info = new RestaurantTableInfo();
		try {
			BeanUtils.copyProperties(info, param);
			info.setTableId(String.valueOf(SnowFlake.getId()));
			if (mapper.delete(info) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加餐桌】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【添加餐桌】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加餐桌】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【添加餐桌】   响应参数：{}", respMap);
		 return respMap;
	}
	
	/**
	 * 修改餐桌
	 * @param param
	 * @return
	 */
	public Map<String, Object> updateRestaurantTableInfo(RestaurantTableRequestParam param) {
		log.info("修改餐桌。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		RestaurantTableInfo info = new RestaurantTableInfo();
		try {
			BeanUtils.copyProperties(info, param);
			info.setId(Long.valueOf(param.getId()));
			if (mapper.updateByPrimaryKeySelective(info) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【修改餐桌】   响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【修改餐桌】  报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【修改餐桌】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【修改餐桌】   响应参数：{}", respMap);
		return respMap;
	}
}