package com.github.wxiaoqi.security.biz.codex.biz;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.MarActivity;
import com.github.wxiaoqi.security.biz.codex.entity.MarIncident;
import com.github.wxiaoqi.security.biz.codex.entity.MarIntegral;
import com.github.wxiaoqi.security.biz.codex.mapper.MarActivityMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarIncidentMapper;
import com.github.wxiaoqi.security.biz.codex.mapper.MarIntegralMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 积分基本信息表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Service
public class MarIntegralBiz extends BaseBiz<MarIntegralMapper,MarIntegral> {
	
	private static final Logger logger = LoggerFactory.getLogger(MarIntegralBiz.class);
	@Autowired
	private MarIntegralMapper integralMapper;
	@Autowired
	private MarIncidentMapper incidentMapper;
	@Autowired
	private MarActivityMapper marActivityMapper;

	/**
	 * 查询积分
	 * @param params
	 * @return
	 */
	public List<MarIntegral> getIntegralList(Map<String, Object> params) {
		logger.info("查询积分列表。。。{}"+params);
		String actId = (String) params.get("actId");
		String createTime = (String) params.get("createTime");
		String shutTime = (String) params.get("shutTime");
		List<MarIntegral> list = integralMapper.getIntegralList(createTime, shutTime, actId);
		if (list != null && list.size() > 0) {
			logger.info("积分列表。。。{}"+list.toString());
			for (MarIntegral integral : list) {
				String incStr = integral.getIncCode();
				//事件转译
				if (incStr != null) {
					String incCode = "";
					List<MarIncident> incidents = incidentMapper.selectAll();
					if (incStr.contains(",")) {
						List<String> arr2 = new ArrayList<>(); 
						String[] arr = incStr.split(",");
						for (String string2 : arr) {
							for (MarIncident incident : incidents) {
								if (string2.equals(incident.getIncCode())) {
									string2 = incident.getIncName();
								}
							}
							arr2.add(string2);
						}
						String s1 = arr2.toString();
						String s2 = s1.replace("[", "");
						incCode = s2.replace("]", "");
					}else {
						for (MarIncident incident : incidents) {
							if (incStr.equals(incident.getIncCode())) {
								incStr = incident.getIncName();
								break;
							}
						}
						incCode = incStr;
					}
					integral.setIncCode(incCode);
				}
				//时间比较
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String revS = dateFormat.format(integral.getCreateTime());
				String nowS = dateFormat.format(new Date());
				try {
					if (dateFormat.parse(revS).getTime() > dateFormat.parse(nowS).getTime()) {
						integral.setIsOpen("00");
					}else {
						integral.setIsOpen("01");
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
		logger.info("转后积分列表。。。{}"+list);
		return list;
	}

	/**
	 * 新增积分时，根据活动状态给积分赋值
	 * @param actId
	 * @param integralStatus
	 * @return
	 */
	public String getIntegralStatus(String actId, String integralStatus) {
		//查看活动进行状态
		MarActivity marActivity = new MarActivity();
		marActivity.setActId(actId);
		MarActivity record = marActivityMapper.selectOne(marActivity);
		String actStatus = record.getActStauts();
		if (!"01".equals(actStatus)) {
			integralStatus = "01";
		}
		return integralStatus;
	}
	
	public MarIntegral selectByIntegralId(String integralId) {
		logger.info("查询单条积分信息。。。{}"+integralId);
		return integralMapper.selectByIntegralId(integralId);
	}

}