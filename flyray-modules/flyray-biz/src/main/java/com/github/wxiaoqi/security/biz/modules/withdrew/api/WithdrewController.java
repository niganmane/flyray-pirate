package com.github.wxiaoqi.security.biz.modules.withdrew.api;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.api.BaseController;
import com.github.wxiaoqi.security.biz.modules.withdrew.biz.CmsWithdrewOrderBiz;
import com.github.wxiaoqi.security.common.cms.request.WithdrewApplyParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="提现管理")
@Controller
@RequestMapping("withdrew")
public class WithdrewController extends BaseController {
	
	@Autowired
	private CmsWithdrewOrderBiz cmsWithdrewOrderBiz;
	
	/**
	 * 提现申请
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("提现申请")
	@RequestMapping(value = "/apply",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> withdrewApply(@RequestBody @Valid WithdrewApplyParam withdrewApplyParam) throws Exception {
		Map<String, Object> response = cmsWithdrewOrderBiz.withdrewApply(EntityUtils.beanToMap(withdrewApplyParam));
		if(ResponseCode.OK.getCode().equals(response.get("code"))){
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else{
			return ResponseHelper.success(null, null, ResponseCode.WITHDREW_FAIL.getCode(), ResponseCode.WITHDREW_FAIL.getMessage());
		}
    }

}
