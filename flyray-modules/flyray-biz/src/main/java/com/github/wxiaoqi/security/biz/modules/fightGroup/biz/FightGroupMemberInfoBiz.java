package com.github.wxiaoqi.security.biz.modules.fightGroup.biz;

import java.util.List;

import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupMemberInfo;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupMemberInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.FightGroupQueryParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 团成员信息表
 *
 * @author he
 * @date 2018-07-12 10:17:33
 */
@Slf4j
@Service
public class FightGroupMemberInfoBiz extends BaseBiz<FightGroupMemberInfoMapper,FightGroupMemberInfo> {
	
	
	/**
	 * 查询拼团团成员信息列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<FightGroupMemberInfo> queryFightGroupMemberInfoPage(FightGroupQueryParam param) {
		log.info("查询拼团团成员信息列表。。。。{}"+param);
		Example example = new Example(FightGroupMemberInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if (param.getName() != null && param.getName().length() > 0) {
			criteria.andEqualTo("name", param.getName());
		}
		example.setOrderByClause("create_time desc");
		Page<FightGroupMemberInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<FightGroupMemberInfo> list = mapper.selectByExample(example);
		TableResultResponse<FightGroupMemberInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
}