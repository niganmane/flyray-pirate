package com.github.wxiaoqi.security.biz.codex.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.MarRandomSubtract;
import com.github.wxiaoqi.security.biz.codex.mapper.MarRandomSubtractMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 随机立减基本信息表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Service
public class MarRandomSubtractBiz extends BaseBiz<MarRandomSubtractMapper,MarRandomSubtract> {
}