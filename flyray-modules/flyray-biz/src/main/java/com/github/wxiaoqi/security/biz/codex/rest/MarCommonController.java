package com.github.wxiaoqi.security.biz.codex.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.codex.biz.MarCommonBiz;

/**
 * 营销引擎混合查询
 * @author centerroot
 * @time 创建时间:2018年3月20日下午5:31:30
 * @description
 */
@Controller
@RequestMapping("common")
public class MarCommonController {

	private final static Logger logger = LoggerFactory.getLogger(MarCommonController.class);
	
	@Autowired
	private MarCommonBiz marCommonBiz;
	
	/**
	 * 获取红包积分使用列表
	 * @author centerroot
	 * @time 创建时间:2018年3月20日下午5:31:45
	 * @param params
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/getDiscountList", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getDiscountList(@RequestParam("page") String page, @RequestParam("limit") String limit, 
			String actId, String merNo, String sceneId, String discountType, String beginDate, String endDate) throws ParseException {
		
		Map<String, Object> params = new HashMap<String, Object>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (!StringUtils.isEmpty(beginDate)) {
			beginDate = beginDate.replace("Z", " UTC");
			Date startD = format.parse(beginDate);
			params.put("beginDate", sdf.format(startD));
			System.out.println(sdf.format(startD));
		}
		if (!StringUtils.isEmpty(endDate)) {
			endDate = endDate.replace("Z", " UTC");
			Date endDTemp = format.parse(endDate);
			Calendar c = Calendar.getInstance();
		    c.setTime(endDTemp);
		    c.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天
		    Date endD = c.getTime();
			params.put("endDate", sdf.format(endD));
			System.out.println(sdf.format(endD));
		}
		params.put("page", page);
		params.put("limit", limit);
		params.put("actId", actId);
		params.put("merNo", merNo);
		params.put("sceneId", sceneId);
		params.put("discountType", discountType);
		logger.info("获取红包使用列表。。。{}"+params);
		return marCommonBiz.getDiscountList(params);
	}
}
