package com.github.wxiaoqi.security.biz.modules.fightGroup.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.fightGroup.biz.FightGroupCategoryInfoBiz;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupCategoryInfo;
import com.github.wxiaoqi.security.common.cms.request.FightGroupCategoryQueryParam;
import com.github.wxiaoqi.security.common.cms.request.FightGroupCategoryRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序拼团团类目相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("fightGroup/category")
public class FightGroupCategoryRestController extends BaseController<FightGroupCategoryInfoBiz, FightGroupCategoryInfo> {
	
	@Autowired
	private FightGroupCategoryInfoBiz fightGroupCategoryInfoBiz;
	
	/**
	 * 查询拼团团类目列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<FightGroupCategoryInfo> query(@RequestBody FightGroupCategoryQueryParam param) {
		log.info("查询拼团团类目列表------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return fightGroupCategoryInfoBiz.queryFightGroupCategoryPage(param);
	}
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody FightGroupCategoryRequestParam param) {
		log.info("添加拼团团类目，请求参数 {}"+param);
		Map<String, Object> respMap = fightGroupCategoryInfoBiz.addFightGroupCategory(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("添加拼团团类目，响应参数 {}"+ respMap);
		return respMap;
	}
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody FightGroupCategoryRequestParam param) {
		log.info("删除拼团团类目，请求参数 {}"+param);
		Map<String, Object> respMap = fightGroupCategoryInfoBiz.deleteFightGroupCategory(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除拼团团类目，响应参数 {}"+respMap);
		return respMap;
	}
	
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody FightGroupCategoryRequestParam param) {
		log.info("修改拼团团类目，请求参数 {}"+param);
		Map<String, Object> respMap = fightGroupCategoryInfoBiz.updateFightGroupCategory(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("修改拼团团类目，响应参数 {}"+respMap);
		return respMap;
	}

}
