package com.github.wxiaoqi.security.biz.modules.auction.mapper;

import com.github.wxiaoqi.security.biz.modules.auction.entity.AuctionOrder;

import tk.mybatis.mapper.common.Mapper;

/**
 * 竞拍订单表
 * @author he
 * @date 2018-08-10 13:36:20
 */
@org.apache.ibatis.annotations.Mapper
public interface AuctionOrderMapper extends Mapper<AuctionOrder> {
	
	/**
	 * 查询时间降序后的第一条记录
	 * @param auctionOrder
	 * @return
	 */
	public AuctionOrder queryLastOne(AuctionOrder auctionOrder);
	
	/**
	 * 修改状态
	 * @param auctionOrder
	 * @return
	 */
	public int updateStatus(AuctionOrder auctionOrder);
}
