package com.github.wxiaoqi.security.biz.modules.fightGroup.biz;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupInfo;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupMemberInfo;
import com.github.wxiaoqi.security.biz.modules.fightGroup.entity.FightGroupOrder;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupInfoMapper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupMemberInfoMapper;
import com.github.wxiaoqi.security.biz.modules.fightGroup.mapper.FightGroupOrderMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.FightGroupQueryParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.SnowFlake;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 团信息表
 *
 * @author he
 * @date 2018-07-12 10:17:33
 */
@Slf4j
@Service
public class FightGroupInfoBiz extends BaseBiz<FightGroupInfoMapper,FightGroupInfo> {

	@Autowired
	private FightGroupInfoMapper fightGroupInfoMapper;
	@Autowired
	private FightGroupMemberInfoMapper fightGroupMemberInfoMapper;
	@Autowired
	private FightGroupOrderMapper fightGroupOrderMapper;

	/**
	 * 参团
	 * @param request
	 * @return
	 */
	public Map<String, Object> joinGroup(Map<String, Object> request){
		log.info("参团请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String groupId = (String) request.get("groupId");
		String customerId = (String) request.get("customerId");
		String name = (String) request.get("name");
		String userHeadPortrait = (String) request.get("userHeadPortrait");
		FightGroupInfo fightGroupInfo = new FightGroupInfo();
		fightGroupInfo.setPlatformId(platFormId);
		fightGroupInfo.setMerchantId(merId);
		fightGroupInfo.setGroupId(Integer.valueOf(groupId));
		FightGroupInfo groupInfo = fightGroupInfoMapper.selectOne(fightGroupInfo);
		Integer joinedGroupNum = groupInfo.getJoinedGroupNum();
		groupInfo.setJoinedGroupNum(joinedGroupNum + 1);
		fightGroupInfoMapper.updateByPrimaryKey(groupInfo);

		FightGroupMemberInfo fightGroupMemberInfo = new FightGroupMemberInfo();
		fightGroupMemberInfo.setPlatformId(platFormId);
		fightGroupMemberInfo.setMerchantId(merId);
		fightGroupMemberInfo.setPerId(customerId);
		fightGroupMemberInfo.setGroupId(Integer.valueOf(groupId));
		fightGroupMemberInfo.setName(name);
		fightGroupMemberInfo.setUserHeadPortrait(userHeadPortrait);
		fightGroupMemberInfo.setCreateTime(new Date());
		fightGroupMemberInfoMapper.insert(fightGroupMemberInfo);
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("参团响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 创建团
	 * @param request
	 * @return
	 */
	public Map<String, Object> createGroup(Map<String, Object> request){
		log.info("创建团请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String merchantId = (String) request.get("merchantId");
		String payOrderNo = (String) request.get("payOrderNo");
		String groupsId = (String) request.get("groupsId");
		String customerId = (String) request.get("customerId");
		String goodsId = (String) request.get("goodsId");
		String name = (String) request.get("name");
		String userHeadPortrait = (String) request.get("userHeadPortrait");
		FightGroupInfo fightGroupInfo = new FightGroupInfo();
		fightGroupInfo.setPlatformId(platformId);
		fightGroupInfo.setMerchantId(merchantId);
		fightGroupInfo.setHeadPerId(customerId);
		fightGroupInfo.setJoinedGroupNum(1);
		fightGroupInfo.setGroupsId(Integer.valueOf(groupsId));
		fightGroupInfo.setCreateTime(new Date());
		fightGroupInfo.setGoodsId(Integer.valueOf(goodsId));
		long groupId = SnowFlake.getId();
		fightGroupInfo.setGroupId((int)groupId);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date parse;
		try {
			parse = sdf.parse(getFetureDate(7));
			Timestamp date = new Timestamp(parse.getTime());
			fightGroupInfo.setEndDate(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		fightGroupInfoMapper.insert(fightGroupInfo);
		
		FightGroupMemberInfo fightGroupMemberInfo = new FightGroupMemberInfo();
		fightGroupMemberInfo.setPlatformId(platformId);
		fightGroupMemberInfo.setMerchantId(merchantId);
		fightGroupMemberInfo.setPerId(customerId);
		fightGroupMemberInfo.setGroupId((int)groupId);
		fightGroupMemberInfo.setName(name);
		fightGroupMemberInfo.setUserHeadPortrait(userHeadPortrait);
		fightGroupMemberInfo.setCreateTime(new Date());
		fightGroupMemberInfoMapper.insert(fightGroupMemberInfo);
		
		//更新订单表中团id
		FightGroupOrder fightGroupOrder = new FightGroupOrder();
		fightGroupOrder.setPlatformId(platformId);
		fightGroupOrder.setMerchantId(merchantId);
		fightGroupOrder.setPayOrderNo(payOrderNo);
		FightGroupOrder selectFightGroupOrder = fightGroupOrderMapper.selectOne(fightGroupOrder);
		if(null != selectFightGroupOrder){
			selectFightGroupOrder.setGroupId(String.valueOf(groupId));
			fightGroupOrderMapper.updateByPrimaryKey(selectFightGroupOrder);
		}

		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("创建团响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 获取的n天后的日期
	 * @param past
	 * @return
	 */
	public static String getFetureDate(int past) {  
		Calendar calendar = Calendar.getInstance();  
		calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + past);  
		Date today = calendar.getTime();  
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
		String result = format.format(today);  
		return result;  
	} 
	
	/**
	 * 查询拼团团信息列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<FightGroupInfo> queryFightGroupInfoPage(FightGroupQueryParam param) {
		log.info("查询拼团团信息列表。。。。{}"+param);
		Example example = new Example(FightGroupInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if (param.getGoodsId() != null && param.getGoodsId().length() > 0) {
			criteria.andEqualTo("goodsId",param.getGoodsId());
		}
		example.setOrderByClause("create_time desc");
		Page<FightGroupInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<FightGroupInfo> list = mapper.selectByExample(example);
		TableResultResponse<FightGroupInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
}