package com.github.wxiaoqi.security.biz.codex.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class MarDiscountBalanceFile implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3077439481143727943L;
	/**
	 * 商户号
	 */
	private String merchantId;
	/**
	 * 场景ID
	 */
	private String sceneId;
	/**
	 * 红包金额
	 */
	private BigDecimal discountAmt;
	/**
	 * 支付订单号
	 */
	private String merorderno;
	/**
	 * 活动id
	 */
	private String actId;
	/**
	 * 使用时间
	 */
	private Date useTime;

	/**
	 * 折扣类型
	 * 01：红包，02：积分
	 */
	private String discountType;

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getSceneId() {
		return sceneId;
	}

	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}

	public BigDecimal getDiscountAmt() {
		return discountAmt;
	}

	public void setDiscountAmt(BigDecimal discountAmt) {
		this.discountAmt = discountAmt;
	}

	public String getMerorderno() {
		return merorderno;
	}

	public void setMerorderno(String merorderno) {
		this.merorderno = merorderno;
	}

	public String getActId() {
		return actId;
	}

	public void setActId(String actId) {
		this.actId = actId;
	}

	public Date getUseTime() {
		return useTime;
	}

	public void setUseTime(Date useTime) {
		this.useTime = useTime;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	@Override
	public String toString() {
		return "MarDiscountBalanceFile [merchantId=" + merchantId + ", sceneId=" + sceneId + ", discountAmt=" + discountAmt
				+ ", merorderno=" + merorderno + ", actId=" + actId + ", useTime=" + useTime + ", discountType="
				+ discountType + "]";
	}


}
