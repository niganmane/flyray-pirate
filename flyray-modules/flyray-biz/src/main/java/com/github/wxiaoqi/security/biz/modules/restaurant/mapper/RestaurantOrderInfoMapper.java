package com.github.wxiaoqi.security.biz.modules.restaurant.mapper;

import java.util.List;

import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantNoConfirmOrderInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantOrderInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 订单表
 * @author he
 * @date 2018-06-29 10:31:27
 */
@org.apache.ibatis.annotations.Mapper
public interface RestaurantOrderInfoMapper extends Mapper<RestaurantOrderInfo> {
	
	/**
	 * 查询当天最后一条订单
	 * @param restaurantOrderInfo
	 * @return
	 */
	public RestaurantOrderInfo queryTodayLastOrderInfo(RestaurantOrderInfo restaurantOrderInfo);
	
	/**
	 * 查询未确认的订单信息
	 * @param restaurantOrderInfo
	 * @return
	 */
	public List<RestaurantNoConfirmOrderInfo> queryNoConfirmList(RestaurantOrderInfo restaurantOrderInfo);
	
}
