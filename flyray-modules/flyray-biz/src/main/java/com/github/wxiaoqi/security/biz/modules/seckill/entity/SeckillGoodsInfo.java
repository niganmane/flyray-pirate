package com.github.wxiaoqi.security.biz.modules.seckill.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 秒杀商品信息表
 * @author he
 * @date 2018-09-03 17:42:34
 */
@Table(name = "seckill_goods_info")
public class SeckillGoodsInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//商品编号
	@Column(name = "goods_id")
	private String goodsId;

	//商品名称
	@Column(name = "goods_name")
	private String goodsName;

	//商品说明
	@Column(name = "goods_description")
	private String goodsDescription;

	//商品照片
	@Column(name = "goods_img")
	private String goodsImg;

	//金额
	@Column(name = "tx_amt")
	private BigDecimal txAmt;


	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：商品编号
	 */
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	/**
	 * 获取：商品编号
	 */
	public String getGoodsId() {
		return goodsId;
	}
	/**
	 * 设置：商品名称
	 */
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	/**
	 * 获取：商品名称
	 */
	public String getGoodsName() {
		return goodsName;
	}
	/**
	 * 设置：商品说明
	 */
	public void setGoodsDescription(String goodsDescription) {
		this.goodsDescription = goodsDescription;
	}
	/**
	 * 获取：商品说明
	 */
	public String getGoodsDescription() {
		return goodsDescription;
	}
	/**
	 * 设置：商品照片
	 */
	public void setGoodsImg(String goodsImg) {
		this.goodsImg = goodsImg;
	}
	/**
	 * 获取：商品照片
	 */
	public String getGoodsImg() {
		return goodsImg;
	}
	/**
	 * 设置：金额
	 */
	public void setTxAmt(BigDecimal txAmt) {
		this.txAmt = txAmt;
	}
	/**
	 * 获取：金额
	 */
	public BigDecimal getTxAmt() {
		return txAmt;
	}
}
