package com.github.wxiaoqi.security.biz.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.wxiaoqi.security.common.admin.pay.response.BaseResponse;
import com.github.wxiaoqi.security.common.biz.request.CoupIntegRevCrmReq;
import com.github.wxiaoqi.security.common.crm.request.MerchantBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.PersonalBaseRequest;

/**
 * 支付相关接口调用
 * @author he
 *
 */
@FeignClient(value = "flyray-crm-core")
public interface CrmFeign {
	
	/**
	 * 第三方支付入账
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/intoAccount",method = RequestMethod.POST)
	public Map<String, Object> inAccounting(@RequestBody Map<String, Object> params);
	
	/**
	 * 第三方退款出账
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/payment/outAccounting",method = RequestMethod.POST)
	public Map<String, Object> outAccounting(@RequestBody Map<String, Object> params);
	
	/**
	 * 余额支付入账
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/accounttransfer",method = RequestMethod.POST)
	public Map<String, Object> balancePay(@RequestBody Map<String, Object> params);
	
	/**
	 * 余额退款出账
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/payment/balanceRefund",method = RequestMethod.POST)
	public Map<String, Object> balanceRefund(@RequestBody Map<String, Object> params);

	/**
	 * 账户余额冻结
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/freeze",method = RequestMethod.POST)
	public Map<String, Object> freeze(@RequestBody Map<String, Object> params);
	/**
	 * 账户余额解冻
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/unfreeze",method = RequestMethod.POST)
	public Map<String, Object> unfreeze(@RequestBody Map<String, Object> params);
	/**
	 * 账户余额解冻并出账
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/unFreAndOutAccount",method = RequestMethod.POST)
	public Map<String, Object> unFreAndOutAccount(@RequestBody Map<String, Object> params);
	/***
	 * 
	 * 用户基础信息校验
	 */
	
	 @RequestMapping(value = "/feign/personal/queryInfo", method =RequestMethod.POST) 
	 public Map<String, Object> queryPersonalInfo(@RequestBody Map<String, Object> params);

	/***
	 * 
	 * 商户开户信息校验
	 */
	@RequestMapping(value = "/feign/merchant/queryInfo", method = RequestMethod.POST)
	public Map<String, Object> queryMerchantInfo(@RequestBody Map<String, Object> params);
	
	/***
	 * 
	 * 账户信息查询
	 */
	@RequestMapping(value = "/feign/accountQuery", method = RequestMethod.POST)
	public Map<String, Object> accountQuery(@RequestBody Map<String, Object> params);
	/***
	 * 
	 * 账户流水信息查询
	 */
	@RequestMapping(value = "/feign/accountJournalQuery", method = RequestMethod.POST)
	public Map<String, Object> accountJournalQuery(@RequestBody Map<String, Object> params);
	
	/**
	* 查询商户通道费率
	* @param req
	* @return
	*/
	@RequestMapping(value = "feign/payChannelConfig/queryMerChannelFee",method = RequestMethod.POST)
	public Map<String, Object> queryMerChannelFee(@RequestBody Map<String, Object> params);
}
