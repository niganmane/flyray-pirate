package com.github.wxiaoqi.security.biz.modules.comment.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:44
 */
@Table(name = "cms_community_view_favort")
public class Favort implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //主键，js无法转化18位数字
    @Id
    private String id;
	
	    //外键索引，用户编号
    @Column(name = "customer_id")
    private String customerId;
	
	    //外键索引，观点编号
    @Column(name = "point_id")
    private String pointId;
	
	    //1点赞2取消赞
    @Column(name = "favort_status")
    private Integer favortStatus;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	
	    //商户号
    @Column(name = "merchant_id")
    private String merchantId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
    
    @Column(name = "nickName")
    private String nickName;
    
    @Column(name = "avatarUrl")
    private String avatarUrl;
	

	/**
	 * 设置：主键，js无法转化18位数字
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：主键，js无法转化18位数字
	 */
	public String getId() {
		return id;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 设置：外键索引，观点编号
	 */
	public void setPointId(String pointId) {
		this.pointId = pointId;
	}
	/**
	 * 获取：外键索引，观点编号
	 */
	public String getPointId() {
		return pointId;
	}
	/**
	 * 设置：1点赞2取消赞
	 */
	public void setFavortStatus(Integer favortStatus) {
		this.favortStatus = favortStatus;
	}
	/**
	 * 获取：1点赞2取消赞
	 */
	public Integer getFavortStatus() {
		return favortStatus;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：商户号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	@Override
	public String toString() {
		return "Favort [id=" + id + ", customerId=" + customerId + ", pointId=" + pointId + ", favortStatus="
				+ favortStatus + ", createTime=" + createTime + ", updateTime=" + updateTime + ", merchantId="
				+ merchantId + ", platformId=" + platformId + ", nickName=" + nickName + ", avatarUrl=" + avatarUrl
				+ "]";
	}
}
