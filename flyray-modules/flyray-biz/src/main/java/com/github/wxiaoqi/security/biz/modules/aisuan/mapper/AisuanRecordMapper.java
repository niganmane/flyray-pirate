package com.github.wxiaoqi.security.biz.modules.aisuan.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.biz.modules.aisuan.entity.AisuanRecord;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-10 10:58:58
 */
@org.apache.ibatis.annotations.Mapper
public interface AisuanRecordMapper extends Mapper<AisuanRecord> {
	List<AisuanRecord> queryList(Map<String, Object> map);
	List<AisuanRecord> queryApplyList(Map<String, Object> map);
	List<AisuanRecord> queryAskAndApplyList(Map<String, Object> map);
}
