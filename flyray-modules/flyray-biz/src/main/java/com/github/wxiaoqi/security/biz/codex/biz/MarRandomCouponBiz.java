package com.github.wxiaoqi.security.biz.codex.biz;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.MarRandomCoupon;
import com.github.wxiaoqi.security.biz.codex.mapper.MarRandomCouponMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 随机红包池表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Service
public class MarRandomCouponBiz extends BaseBiz<MarRandomCouponMapper,MarRandomCoupon> {
	
	private Logger logger = LoggerFactory.getLogger(MarRandomCouponBiz.class);
	
	@Autowired
	private MarRandomCouponMapper marRandomCouponMapper;
	
	public void addRandomCouponList(List<MarRandomCoupon> list) {
		logger.info("批量插入随机红包。。。{}"+list);
		marRandomCouponMapper.addRandomCouponList(list);
	}


}