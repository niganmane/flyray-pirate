package com.github.wxiaoqi.security.biz.modules.comment.mapper;

import com.github.wxiaoqi.security.biz.modules.comment.entity.Favort;

import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author chj
 * @email 
 * @date 2018-07-09 14:10:44
 */
@org.apache.ibatis.annotations.Mapper
public interface FavortMapper extends Mapper<Favort> {
	
}
