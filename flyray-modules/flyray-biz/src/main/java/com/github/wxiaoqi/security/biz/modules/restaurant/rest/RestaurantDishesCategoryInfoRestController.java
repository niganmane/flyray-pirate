package com.github.wxiaoqi.security.biz.modules.restaurant.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantDishesCategoryInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantDishesCategoryInfo;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序点餐菜品类目信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/dishesCategory")
public class RestaurantDishesCategoryInfoRestController extends BaseController<RestaurantDishesCategoryInfoBiz, RestaurantDishesCategoryInfo>{
	
	@Autowired
	private RestaurantDishesCategoryInfoBiz restaurantDishesCategoryInfoBiz;
	
	/**
	 * 查询菜品类目信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<RestaurantDishesCategoryInfo> query(@RequestBody RestaurantQueryParam param) {
		log.info("查询菜品类目信息列表------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return restaurantDishesCategoryInfoBiz.queryRestaurantDishesCategoryInfoPage(param);
	}
	
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody Map<String, Object> param) {
		log.info("删除菜品类目信息，请求参数{}"+ param);
		Map<String, Object> respMap = restaurantDishesCategoryInfoBiz.deleteDishesCategory(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除菜品类目信息，请求参数{}"+ respMap);
		return respMap;
	}
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> add(@RequestBody Map<String, Object> param) {
		log.info("添加菜品类目信息，请求参数{}"+ param);
		Map<String, Object> respMap = restaurantDishesCategoryInfoBiz.addDishesCategory(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("添加菜品类目信息，响应参数{}"+ respMap);
		return respMap;
	}
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> update(@RequestBody Map<String, Object> param) {
		log.info("修改菜品类目信息，请求参数{}"+ param);
		Map<String, Object> respMap = restaurantDishesCategoryInfoBiz.updateDishesCategory(param);
		if (respMap.get("code").equals(ResponseCode.OK.getCode())) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("修改菜品类目信息，请求参数{}"+ respMap);
		return respMap;
	}

}
