package com.github.wxiaoqi.security.biz.codex.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.wxiaoqi.security.biz.codex.biz.MarRandomCouponBiz;
import com.github.wxiaoqi.security.biz.codex.entity.MarRandomCoupon;
import com.github.wxiaoqi.security.common.rest.BaseController;

@Controller
@RequestMapping("marRandomCoupon")
public class MarRandomCouponController extends BaseController<MarRandomCouponBiz,MarRandomCoupon> {

}