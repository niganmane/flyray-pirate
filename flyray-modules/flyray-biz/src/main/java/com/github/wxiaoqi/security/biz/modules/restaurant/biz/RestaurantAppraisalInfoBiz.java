package com.github.wxiaoqi.security.biz.modules.restaurant.biz;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantAppraisalDetailInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantAppraisalInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantOrderInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantShoppingCartInfo;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantAppraisalDetailInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantAppraisalInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantOrderInfoMapper;
import com.github.wxiaoqi.security.biz.modules.restaurant.mapper.RestaurantShoppingCartInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 菜品评价信息表
 *
 * @author he
 * @date 2018-06-29 10:31:27
 */
@Service
public class RestaurantAppraisalInfoBiz extends BaseBiz<RestaurantAppraisalInfoMapper,RestaurantAppraisalInfo> {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(RestaurantAppraisalInfoBiz.class);

	@Autowired
	private RestaurantAppraisalInfoMapper restaurantAppraisalInfoMapper;
	@Autowired
	private RestaurantAppraisalDetailInfoMapper restaurantAppraisalDetailInfoMapper;
	@Autowired
	private RestaurantShoppingCartInfoMapper restaurantShoppingCartInfoMapper;
	@Autowired
	private RestaurantOrderInfoMapper restaurantOrderInfoMapper;

	/**
	 * 菜品评价信息查询
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryAppraisalInfo(Map<String, Object> request){
		logger.info("菜品评价信息查询请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platFormId = (String) request.get("platformId");
		String merId = (String) request.get("merchantId");
		String dishesId = (String) request.get("dishesId");
		RestaurantAppraisalInfo restaurantAppraisalInfo = new RestaurantAppraisalInfo();
		restaurantAppraisalInfo.setPlatformId(platFormId);
		restaurantAppraisalInfo.setMerchantId(merId);
		restaurantAppraisalInfo.setDishesId(dishesId);
		RestaurantAppraisalInfo appraisalInfo = restaurantAppraisalInfoMapper.selectOne(restaurantAppraisalInfo);
		if(null != appraisalInfo){
			//查询评价明细列表
			RestaurantAppraisalDetailInfo restaurantAppraisalDetailInfo = new RestaurantAppraisalDetailInfo();
			restaurantAppraisalDetailInfo.setPlatformId(platFormId);
			restaurantAppraisalDetailInfo.setMerchantId(merId);
			restaurantAppraisalDetailInfo.setDishesId(appraisalInfo.getDishesId());
			List<RestaurantAppraisalDetailInfo> detailList = restaurantAppraisalDetailInfoMapper.queryDetailInfoList(restaurantAppraisalDetailInfo);
			response.put("detailList", detailList);
			response.put("appraisalInfo", appraisalInfo);
		}else{
			RestaurantAppraisalInfo newInfo = new RestaurantAppraisalInfo();
			newInfo.setPlatformId(platFormId);
			newInfo.setMerchantId(merId);
			newInfo.setDishesId(dishesId);
			newInfo.setGeneralNum(0);
			newInfo.setNotSatisfiedNum(0);
			newInfo.setSatisfiedNum(0);
			newInfo.setTotalNum(0);
			newInfo.setVerySatisfiedNum(0);
			response.put("detailList", null);
			response.put("appraisalInfo", newInfo);
		}

		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("菜品评价信息查询响应。。。。。。{}", response);
		return response;
	}

	/**
	 * 查询评价信息列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<RestaurantAppraisalInfo> queryRestaurantAppraisalInfoPage(RestaurantQueryParam param) {
		logger.info("查询评价信息列表。。。。{}"+param);
		Example example = new Example(RestaurantAppraisalInfo.class);
		Criteria criteria = example.createCriteria();
		if (param.getPlatformId() != null && param.getPlatformId().length() > 0) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if (param.getMerchantId() != null && param.getMerchantId().length() > 0) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		Page<RestaurantAppraisalInfo> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<RestaurantAppraisalInfo> list = mapper.selectByExample(example);
		TableResultResponse<RestaurantAppraisalInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}

	/**
	 * 发起评论
	 * @param request
	 * @return
	 */
	public Map<String, Object> toAppraisal(Map<String, Object> request){
		logger.info("发起评论请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String merchantId = (String) request.get("merchantId");
		String customerId = (String) request.get("customerId");
		String appraisalInfo = (String) request.get("appraisalInfo");
		String appraisalGrade = (String) request.get("appraisalGrade");//评价等级  1很满意 2满意 3一般 4不满意
		String name = (String) request.get("name");
		String userHeadPortrait = (String) request.get("userHeadPortrait");
		String id = (String) request.get("id");
		RestaurantShoppingCartInfo restaurantShoppingCartInfo = new RestaurantShoppingCartInfo();
		restaurantShoppingCartInfo.setId(Long.valueOf(id));
		RestaurantShoppingCartInfo selectShoppingCartInfo = restaurantShoppingCartInfoMapper.selectOne(restaurantShoppingCartInfo);
		if(null != selectShoppingCartInfo){
			selectShoppingCartInfo.setIsAppraisal("0");//已评论
			restaurantShoppingCartInfoMapper.updateByPrimaryKey(selectShoppingCartInfo);
			String dishesId = selectShoppingCartInfo.getDishesId();
			RestaurantAppraisalInfo restaurantAppraisalInfo = new RestaurantAppraisalInfo();
			restaurantAppraisalInfo.setPlatformId(platformId);
			restaurantAppraisalInfo.setMerchantId(merchantId);
			restaurantAppraisalInfo.setDishesId(dishesId);
			RestaurantAppraisalInfo selectAppraisalInfo = restaurantAppraisalInfoMapper.selectOne(restaurantAppraisalInfo);
			if(null != selectAppraisalInfo){
				if("1".equals(appraisalGrade)){//1很满意
					Integer verySatisfiedNum = selectAppraisalInfo.getVerySatisfiedNum();
					selectAppraisalInfo.setVerySatisfiedNum(verySatisfiedNum + 1);
				}else if("2".equals(appraisalGrade)){//2满意
					Integer satisfiedNum = selectAppraisalInfo.getSatisfiedNum();
					selectAppraisalInfo.setSatisfiedNum(satisfiedNum + 1);
				}else if("3".equals(appraisalGrade)){//3一般
					Integer generalNum = selectAppraisalInfo.getGeneralNum();
					selectAppraisalInfo.setGeneralNum(generalNum + 1);
				}else if("4".equals(appraisalGrade)){//4不满意
					Integer notSatisfiedNum = selectAppraisalInfo.getNotSatisfiedNum();
					selectAppraisalInfo.setNotSatisfiedNum(notSatisfiedNum + 1);
				}
				Integer totalNum = selectAppraisalInfo.getTotalNum();
				selectAppraisalInfo.setTotalNum(totalNum + 1);
				restaurantAppraisalInfoMapper.updateByPrimaryKey(selectAppraisalInfo);
			}else{
				if("1".equals(appraisalGrade)){//1很满意
					restaurantAppraisalInfo.setVerySatisfiedNum(1);
					restaurantAppraisalInfo.setSatisfiedNum(0);
					restaurantAppraisalInfo.setGeneralNum(0);
					restaurantAppraisalInfo.setNotSatisfiedNum(0);
				}else if("2".equals(appraisalGrade)){//2满意
					restaurantAppraisalInfo.setVerySatisfiedNum(0);
					restaurantAppraisalInfo.setSatisfiedNum(1);
					restaurantAppraisalInfo.setGeneralNum(0);
					restaurantAppraisalInfo.setNotSatisfiedNum(0);
				}else if("3".equals(appraisalGrade)){//3一般
					restaurantAppraisalInfo.setVerySatisfiedNum(0);
					restaurantAppraisalInfo.setSatisfiedNum(0);
					restaurantAppraisalInfo.setGeneralNum(1);
					restaurantAppraisalInfo.setNotSatisfiedNum(0);
				}else if("4".equals(appraisalGrade)){//4不满意
					restaurantAppraisalInfo.setVerySatisfiedNum(0);
					restaurantAppraisalInfo.setSatisfiedNum(0);
					restaurantAppraisalInfo.setGeneralNum(0);
					restaurantAppraisalInfo.setNotSatisfiedNum(1);
				}
				restaurantAppraisalInfo.setTotalNum(1);
				restaurantAppraisalInfoMapper.insert(restaurantAppraisalInfo);
			}
			
			//新增一条评论明细
			RestaurantAppraisalDetailInfo restaurantAppraisalDetailInfo = new RestaurantAppraisalDetailInfo();
			restaurantAppraisalDetailInfo.setPlatformId(platformId);
			restaurantAppraisalDetailInfo.setMerchantId(merchantId);
			restaurantAppraisalDetailInfo.setPerId(customerId);
			restaurantAppraisalDetailInfo.setDishesId(dishesId);
			restaurantAppraisalDetailInfo.setUserName(name);
			restaurantAppraisalDetailInfo.setUserHeadPortrait(userHeadPortrait);
			restaurantAppraisalDetailInfo.setAppraisalGrade(appraisalGrade);
			restaurantAppraisalDetailInfo.setAppraisalInfo(appraisalInfo);
			restaurantAppraisalDetailInfo.setAppraisalTime(new Date());
			restaurantAppraisalDetailInfo.setPayOrderNo(selectShoppingCartInfo.getPayOrderNo());
			restaurantAppraisalDetailInfoMapper.insert(restaurantAppraisalDetailInfo);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("发起评论响应。。。。。。{}", response);
		return response;
	}
	
	/**
	 * 外卖评论
	 * @param request
	 * @return
	 */
	public Map<String, Object> takeawayAppraisal(Map<String, Object> request){
		logger.info("外卖评论请求。。。。。。{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String merchantId = (String) request.get("merchantId");
		String customerId = (String) request.get("customerId");
		String appraisalInfo = (String) request.get("appraisalInfo");
		String appraisalGrade = (String) request.get("appraisalGrade");//评价等级  1很满意 2满意 3一般 4不满意
		String name = (String) request.get("name");
		String userHeadPortrait = (String) request.get("userHeadPortrait");
		String payOrderNo = (String) request.get("payOrderNo");
		//新增一条评论明细
		RestaurantAppraisalDetailInfo restaurantAppraisalDetailInfo = new RestaurantAppraisalDetailInfo();
		restaurantAppraisalDetailInfo.setPlatformId(platformId);
		restaurantAppraisalDetailInfo.setMerchantId(merchantId);
		restaurantAppraisalDetailInfo.setPerId(customerId);
		restaurantAppraisalDetailInfo.setUserName(name);
		restaurantAppraisalDetailInfo.setUserHeadPortrait(userHeadPortrait);
		restaurantAppraisalDetailInfo.setAppraisalGrade(appraisalGrade);
		restaurantAppraisalDetailInfo.setAppraisalInfo(appraisalInfo);
		restaurantAppraisalDetailInfo.setAppraisalTime(new Date());
		restaurantAppraisalDetailInfo.setPayOrderNo(payOrderNo);
		restaurantAppraisalDetailInfoMapper.insert(restaurantAppraisalDetailInfo);
		
		//修改订单评论标识
		RestaurantOrderInfo restaurantOrderInfo = new RestaurantOrderInfo();
		restaurantOrderInfo.setPlatformId(platformId);
		restaurantOrderInfo.setMerchantId(merchantId);
		restaurantOrderInfo.setCustomerId(customerId);
		restaurantOrderInfo.setPayOrderNo(payOrderNo);
		restaurantOrderInfo.setType("3");//外卖
		RestaurantOrderInfo selectOrderInfo = restaurantOrderInfoMapper.selectOne(restaurantOrderInfo);
		if(null != selectOrderInfo){
			selectOrderInfo.setIsAppraisal("0");//已评论
			restaurantOrderInfoMapper.updateByPrimaryKey(selectOrderInfo);
		}
		
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		logger.info("外卖评论响应。。。。。。{}", response);
		return response;
	}
	
}