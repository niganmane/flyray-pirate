package com.github.wxiaoqi.security.biz.codex.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.biz.codex.entity.MarRandomUse;
import com.github.wxiaoqi.security.biz.codex.mapper.MarRandomUseMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 随机立减使用记录表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-25 16:56:18
 */
@Service
public class MarRandomUseBiz extends BaseBiz<MarRandomUseMapper,MarRandomUse> {
}