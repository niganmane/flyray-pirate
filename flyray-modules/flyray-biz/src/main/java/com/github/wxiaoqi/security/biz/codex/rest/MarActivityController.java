package com.github.wxiaoqi.security.biz.codex.rest;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.codex.biz.MarActivityBiz;
import com.github.wxiaoqi.security.biz.codex.entity.MarActivity;
import com.github.wxiaoqi.security.common.codex.request.MarActivityQueryRequest;
import com.github.wxiaoqi.security.common.codex.request.MarActivityRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

@Controller
@RequestMapping("marActivities")
public class MarActivityController extends BaseController<MarActivityBiz, MarActivity>{
	
	private static final Logger logger = LoggerFactory.getLogger(MarActivityController.class);
	
	/**
	 * 获取活动列表
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<MarActivity> getCouponList(@RequestBody MarActivityQueryRequest param) {
		logger.info("查询活动列表。。。{}"+param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return baseBiz.queryActivityList(param);
	}
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> addActivityInfo(@RequestBody MarActivityRequestParam param) {
        logger.info("添加活动。。。{}"+param);
        Map<String, Object> respMap = baseBiz.addActivity(param);
		return ResponseHelper.success(respMap, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateActivityInfo(@RequestBody MarActivityRequestParam param) {
		logger.info("修改活动信息。。。{}"+param);
		MarActivity activity = baseBiz.updateActivity(param);
		if (activity != null) {
			return ResponseHelper.success(activity, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
	}
	
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> deleteActivityInfo(@RequestBody MarActivityRequestParam param) {
		logger.info("删除活动信息。。。{}"+param);
		MarActivity activity = baseBiz.deleteActivity(param);
		if (activity != null) {
			return ResponseHelper.success(activity, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
	}
	
	
	
	
	 /**
	  * 获取红包领取率
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:10
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getCouponRevRateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getCouponRevRateData(@RequestBody Map<String, Object> params) {
		return baseBiz.getCouponRevRateData(params);
	}
	/**
	  * 获取活动中红包领取率
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:10
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getActivityCouponRevRateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getActivityCouponRevRateData(@RequestBody Map<String, Object> params) {
		return baseBiz.getActivityCouponRevRateData(params);
	}
	 /**
	  * 获取红包使用率
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:12
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getCouponUseRateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getCouponUseRateData() {
		Map<String, Object> respMap = new HashMap<>();
		return baseBiz.getCouponUseRateData(respMap);
	}
	/**
	  * 获取活动中红包使用率
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:12
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getActivityCouponUseRateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getActivityCouponUseRateData(@RequestBody Map<String, Object> params) {
		return baseBiz.getActivityCouponUseRateData(params);
	}
	 /**
	  * 获取红包场景占比
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:15
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getCouponSceneRateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getCouponSceneRateData() {
		Map<String, Object> params= new HashMap<>();
		return baseBiz.getCouponSceneRateData(params);
	}
	/**
	  * 获取活动中红包场景占比
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:15
	  * @param params
	  * @returngetIntegralUseRateData
	  */
	@RequestMapping(value = "/getActivityCouponSceneRateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getActivityCouponSceneRateData(@RequestBody Map<String, Object> params) {
		return baseBiz.getActivityCouponSceneRateData(params);
	}
	 /**
	  * 获取积分领取率
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:14
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getIntegralRevRateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getIntegralRevRateData(@RequestBody Map<String, Object> params) {
		return baseBiz.getIntegralRevRateData(params);
	}
	/**
	  * 获取活动中积分领取率
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:14
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getActivityIntegralRevRateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getActivityIntegralRevRateData(@RequestBody Map<String, Object> params) {
		return baseBiz.getActivityIntegralRevRateData(params);
	}
	 /**
	  * 获取积分使用率
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:17
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getIntegralUseRateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getIntegralUseRateData() {
		Map<String, Object> params= new HashMap<>();
		return baseBiz.getIntegralUseRateData(params);
	}
	/**
	  * 获取活动中积分使用率
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:17
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getActivityIntegralUseRateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getActivityIntegralUseRateData(@RequestBody Map<String, Object> params) {
		return baseBiz.getActivityIntegralUseRateData(params);
	}
	 /**
	  * 获取积分场景占比
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:18
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getIntegralSceneRateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getIntegralSceneRateData() {
		Map<String, Object> params= new HashMap<>();
		return baseBiz.getIntegralSceneRateData(params);
	}
	/**
	  * 获取活动中积分场景占比
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:18
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getActivityIntegralSceneRateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getActivityIntegralSceneRateData(@RequestBody Map<String, Object> params) {
		return baseBiz.getActivityIntegralSceneRateData(params);
	}
	 /**
	  * 获取让利占比
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:22
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getAmountProportionData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getAmountProportionData() {
		Map<String, Object> params= new HashMap<>();
		return baseBiz.getAmountProportionData(params);
	}
	/**
	  * 获取活动中让利占比
	  * @author centerroot
	  * @time 创建时间:2018年2月24日下午2:16:22
	  * @param params
	  * @return
	  */
	@RequestMapping(value = "/getActivityAmountProportionData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> getActivityAmountProportionData(@RequestBody Map<String, Object> params) {
		return baseBiz.getActivityAmountProportionData(params);
	}
	

}
