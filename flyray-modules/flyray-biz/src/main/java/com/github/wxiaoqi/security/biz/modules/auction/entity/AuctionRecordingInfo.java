package com.github.wxiaoqi.security.biz.modules.auction.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 竞拍记录表
 * 
 * @author he
 * @date 2018-07-17 13:44:31
 */
@Table(name = "auction_recording_info")
public class AuctionRecordingInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//用户账号
	@Column(name = "per_id")
	private String perId;

	//商品编号
	@Column(name = "goods_id")
	private String goodsId;

	//竞拍金额
	@Column(name = "amt")
	private BigDecimal amt;

	//竞拍时间
	@Column(name = "create_time")
	private Date createTime;

	//用户名
	@Column(name = "name")
	private String name;

	//头像
	@Column(name = "user_head_portrait")
	private String userHeadPortrait;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：用户账号
	 */
	public void setPerId(String perId) {
		this.perId = perId;
	}
	/**
	 * 获取：用户账号
	 */
	public String getPerId() {
		return perId;
	}
	/**
	 * 设置：商品编号
	 */
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	/**
	 * 获取：商品编号
	 */
	public String getGoodsId() {
		return goodsId;
	}
	/**
	 * 设置：竞拍金额
	 */
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	/**
	 * 获取：竞拍金额
	 */
	public BigDecimal getAmt() {
		return amt;
	}
	/**
	 * 设置：竞拍时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：竞拍时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：用户名
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：用户名
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：头像
	 */
	public void setUserHeadPortrait(String userHeadPortrait) {
		this.userHeadPortrait = userHeadPortrait;
	}
	/**
	 * 获取：头像
	 */
	public String getUserHeadPortrait() {
		return userHeadPortrait;
	}
}
