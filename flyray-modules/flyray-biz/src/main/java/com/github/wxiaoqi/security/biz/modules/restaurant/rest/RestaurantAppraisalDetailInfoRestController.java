package com.github.wxiaoqi.security.biz.modules.restaurant.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.biz.modules.restaurant.biz.RestaurantAppraisalDetailInfoBiz;
import com.github.wxiaoqi.security.biz.modules.restaurant.entity.RestaurantAppraisalDetailInfo;
import com.github.wxiaoqi.security.common.cms.request.RestaurantQueryParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

import lombok.extern.slf4j.Slf4j;

/**
 * 小程序点餐评论详情信息相关接口
 * @author he
 *
 */
@Slf4j
@Controller
@RequestMapping("restaurant/appraisalDetail")
public class RestaurantAppraisalDetailInfoRestController extends BaseController<RestaurantAppraisalDetailInfoBiz, RestaurantAppraisalDetailInfo> {
	
	@Autowired
	private RestaurantAppraisalDetailInfoBiz restaurantAppraisalDetailInfoBiz;
	
	/**
	 * 查询点餐评论详情信息列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public TableResultResponse<RestaurantAppraisalDetailInfo> query(@RequestBody RestaurantQueryParam param) {
		log.info("查询点餐评论详情信息列表------start------{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return restaurantAppraisalDetailInfoBiz.queryRestaurantAppraisalDetailInfoPage(param);
	}

}
