package com.github.wxiaoqi.security.biz;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.wxiaoqi.security.biz.modules.test.TestService;
import com.github.wxiaoqi.security.biz.modules.test.User;

/**
 *　@author bolei
 *　@date Oct 5, 2018
　*　@description 
**/

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootCacheRedisApplicationTests {

	private static final Logger log = LoggerFactory.getLogger(SpringBootCacheRedisApplicationTests.class);


	@Autowired
	private TestService userService;


	@Test
	public void get() {
		final User user = userService.saveOrUpdate(new User(5L, "u5", "p5"));
		log.info("[saveOrUpdate] - [{}]", user);
		final User user1 = userService.get(5L);
		log.info("[get] - [{}]", user1);
		userService.delete(5L);
	}

}