package me.flyray.biz.thymeleaf.modules.goods.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 商品管理
 * @author he
 * @date 2018-09-12 14:11:56
 */
@Table(name = "qr_goods_info")
public class QrGoodsInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	//序号
	@Id
	private Long id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商品编号
	@Column(name = "goods_id")
	private String goodsId;

	//商户号
	@Column(name = "merchant_id")
	private String merchantId;

	//商品名称
	@Column(name = "goods_name")
	private String goodsName;

	//商品价格
	@Column(name = "goods_price")
	private BigDecimal goodsPrice;

	//商品图片地址
	@Column(name = "goods_pic")
	private String goodsPic;

	//商品属性
	@Column(name = "attribute")
	private String attribute;

	//入库日期
	@Column(name = "storage_date")
	private Date storageDate;

	//上架日期--分配商户日期
	@Column(name = "shelf_date")
	private Date shelfDate;

	//取消关联商户日期
	@Column(name = "off_date")
	private Date offDate;

	//首次扫描日期
	@Column(name = "first_scan_date")
	private Date firstScanDate;

	//扫描次数
	@Column(name = "scan_count")
	private Long scanCount;

	//首次付费日期
	@Column(name = "first_pay_date")
	private Date firstPayDate;

	//付费次数
	@Column(name = "pay_count")
	private Long payCount;

	//上次付费日期
	@Column(name = "last_pay_date")
	private Date lastPayDate;

	//运营人员id --关联系统用户表
	@Column(name = "operator_id")
	private String operatorId;

	//密码起始位
	@Column(name = "pwd_start_bit")
	private Integer pwdStartBit;

	//密码库
	@Column(name = "pwd_library")
	private String pwdLibrary;
	
	//最后一次使用密码
	@Column(name = "last_pwd")
	private String lastPwd;

	/**
	 * 设置：序号
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商品编号
	 */
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	/**
	 * 获取：商品编号
	 */
	public String getGoodsId() {
		return goodsId;
	}
	/**
	 * 设置：商户号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：商品名称
	 */
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	/**
	 * 获取：商品名称
	 */
	public String getGoodsName() {
		return goodsName;
	}
	/**
	 * 设置：商品价格
	 */
	public void setGoodsPrice(BigDecimal goodsPrice) {
		this.goodsPrice = goodsPrice;
	}
	/**
	 * 获取：商品价格
	 */
	public BigDecimal getGoodsPrice() {
		return goodsPrice;
	}
	/**
	 * 设置：商品图片地址
	 */
	public void setGoodsPic(String goodsPic) {
		this.goodsPic = goodsPic;
	}
	/**
	 * 获取：商品图片地址
	 */
	public String getGoodsPic() {
		return goodsPic;
	}
	/**
	 * 设置：商品属性
	 */
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	/**
	 * 获取：商品属性
	 */
	public String getAttribute() {
		return attribute;
	}
	/**
	 * 设置：入库日期
	 */
	public void setStorageDate(Date storageDate) {
		this.storageDate = storageDate;
	}
	/**
	 * 获取：入库日期
	 */
	public Date getStorageDate() {
		return storageDate;
	}
	/**
	 * 设置：上架日期--分配商户日期
	 */
	public void setShelfDate(Date shelfDate) {
		this.shelfDate = shelfDate;
	}
	/**
	 * 获取：上架日期--分配商户日期
	 */
	public Date getShelfDate() {
		return shelfDate;
	}
	/**
	 * 设置：取消关联商户日期
	 */
	public void setOffDate(Date offDate) {
		this.offDate = offDate;
	}
	/**
	 * 获取：取消关联商户日期
	 */
	public Date getOffDate() {
		return offDate;
	}
	/**
	 * 设置：首次扫描日期
	 */
	public void setFirstScanDate(Date firstScanDate) {
		this.firstScanDate = firstScanDate;
	}
	/**
	 * 获取：首次扫描日期
	 */
	public Date getFirstScanDate() {
		return firstScanDate;
	}
	/**
	 * 设置：扫描次数
	 */
	public void setScanCount(Long scanCount) {
		this.scanCount = scanCount;
	}
	/**
	 * 获取：扫描次数
	 */
	public Long getScanCount() {
		return scanCount;
	}
	/**
	 * 设置：首次付费日期
	 */
	public void setFirstPayDate(Date firstPayDate) {
		this.firstPayDate = firstPayDate;
	}
	/**
	 * 获取：首次付费日期
	 */
	public Date getFirstPayDate() {
		return firstPayDate;
	}
	/**
	 * 设置：付费次数
	 */
	public void setPayCount(Long payCount) {
		this.payCount = payCount;
	}
	/**
	 * 获取：付费次数
	 */
	public Long getPayCount() {
		return payCount;
	}
	/**
	 * 设置：上次付费日期
	 */
	public void setLastPayDate(Date lastPayDate) {
		this.lastPayDate = lastPayDate;
	}
	/**
	 * 获取：上次付费日期
	 */
	public Date getLastPayDate() {
		return lastPayDate;
	}
	/**
	 * 设置：运营人员id --关联系统用户表
	 */
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	/**
	 * 获取：运营人员id --关联系统用户表
	 */
	public String getOperatorId() {
		return operatorId;
	}
	/**
	 * 设置：密码起始位
	 */
	public void setPwdStartBit(Integer pwdStartBit) {
		this.pwdStartBit = pwdStartBit;
	}
	/**
	 * 获取：密码起始位
	 */
	public Integer getPwdStartBit() {
		return pwdStartBit;
	}
	/**
	 * 设置：密码库
	 */
	public void setPwdLibrary(String pwdLibrary) {
		this.pwdLibrary = pwdLibrary;
	}
	/**
	 * 获取：密码库
	 */
	public String getPwdLibrary() {
		return pwdLibrary;
	}
	public String getLastPwd() {
		return lastPwd;
	}
	public void setLastPwd(String lastPwd) {
		this.lastPwd = lastPwd;
	}
}
