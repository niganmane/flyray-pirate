package me.flyray.biz.thymeleaf.modules.goods.rest;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.qr.request.QrGoodsAttributeRequestParam;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

import me.flyray.biz.thymeleaf.api.BaseController;
import me.flyray.biz.thymeleaf.modules.goods.biz.QrGoodsAttributeBiz;
import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsAttribute;

@Controller
@RequestMapping("qrGoodsAttributes")
public class QrGoodsAttributeController extends BaseController{

	private static final Logger logger = LoggerFactory.getLogger(QrGoodsAttributeController.class);

	@Autowired
	private QrGoodsAttributeBiz attributeBiz;
	
	/**
	 * 查询商品属性列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public TableResultResponse<QrGoodsAttribute> query(@RequestParam Map<String, Object> param) {
		logger.info("查询商品属性列表。。。{}"+param);
		return attributeBiz.queryList(param);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public Map<String, Object> add(@RequestBody QrGoodsAttributeRequestParam param) {
		logger.info("添加商品属性。。。{}"+param);
		QrGoodsAttribute attribute = attributeBiz.addAttribute(param);
		if (attribute == null) {
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_EXIST.getCode(), ResponseCode.FUNCTION_EXIST.getMessage());
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public Map<String, Object> update(@RequestBody QrGoodsAttributeRequestParam param) {
		logger.info("修改商品属性。。。{}"+param);
		QrGoodsAttribute attribute = attributeBiz.updateAttribute(param);
		if (attribute != null) {
			return ResponseHelper.success(attribute, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
	}
	
	
	@ResponseBody
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody QrGoodsAttributeRequestParam param) {
		logger.info("删除商品属性。。。{}"+param);
		QrGoodsAttribute attribute = attributeBiz.deleteAttribute(param);
		if (attribute != null) {
			return ResponseHelper.success(attribute, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
	}
}