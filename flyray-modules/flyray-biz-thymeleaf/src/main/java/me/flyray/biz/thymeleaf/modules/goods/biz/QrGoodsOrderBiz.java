package me.flyray.biz.thymeleaf.modules.goods.biz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.qr.request.QrQueryOrderParam;

import me.flyray.biz.thymeleaf.feign.CrmFeign;
import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsOrder;
import me.flyray.biz.thymeleaf.modules.goods.mapper.QrGoodsOrderMapper;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 商品订单
 * @author he
 * @date 2018-09-12 14:11:56
 */
@Service
public class QrGoodsOrderBiz extends BaseBiz<QrGoodsOrderMapper,QrGoodsOrder> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(QrGoodsOrderBiz.class);
	@Autowired
	private CrmFeign crmFeign;
	
	@Autowired
	private QrGoodsOrderMapper qrGoodsOrderMapper;
	
	public TableResultResponse<QrGoodsOrder> queryList(QrQueryOrderParam param) {
		logger.info("查询商品订单。。。{}"+param);
		Map<String, Object> reqMap = new HashMap<String, Object>();
		if (!StringUtils.isEmpty(param.getPlatformId())) {
			reqMap.put("platformId", param.getPlatformId());
		}
		if (!StringUtils.isEmpty(param.getMerchantId())) {
			reqMap.put("merchantId", param.getMerchantId());
		}else{
			if(!StringUtils.isEmpty(param.getProvince())){
				//查询省对应的商户号
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("platformId", param.getPlatformId()); 
				params.put("province", param.getProvince());
				Map<String, Object> queryByProvince = crmFeign.queryByProvince(params);
				List<Map<String, Object>> merchantList = (List<Map<String, Object>>) queryByProvince.get("merchantList");
				List<String> paramsList = new ArrayList<>();
				for(int i = 0; i < merchantList.size(); i++){
					paramsList.add((String) merchantList.get(i).get("merchantId"));
				}
				reqMap.put("paramsList", paramsList);
			}
		}
		
		if (!StringUtils.isEmpty(param.getGoodsId())) {
			reqMap.put("goodsId", param.getGoodsId());
		}
		if (!StringUtils.isEmpty(param.getPayOrderNo())) {
			reqMap.put("payOrderNo", param.getPayOrderNo());
		}
		if (!StringUtils.isEmpty(param.getStatus())) {
			reqMap.put("status", param.getStatus());
		}
		Page<QrGoodsOrder> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<QrGoodsOrder> list = mapper.selectGoodsList(reqMap);
		TableResultResponse<QrGoodsOrder> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	/**
	 * 日付费次数统计 昨天与今天
	 * @param params
	 * @return
	 */
	public Map<String, Object> dayPayStatistics(Map<String, Object> params){
		logger.info("查询商品订单请求。。。{}"+params);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String platformId = (String) params.get("platformId");
		String merchantId = (String) params.get("merchantId");
		String goodsId = (String) params.get("goodsId");
		QrGoodsOrder qrGoodsOrder = new QrGoodsOrder();
		if (platformId != null && platformId.length() > 0) {
			qrGoodsOrder.setPlatformId(platformId);
		}
		if (merchantId != null && merchantId.length() > 0) {
			qrGoodsOrder.setMerchantId(merchantId);
		}
		qrGoodsOrder.setGoodsId(goodsId);
		//查询昨日支付成功次数
		Integer lastDayPayCount = qrGoodsOrderMapper.selectLastDayPayCount(qrGoodsOrder);
		//查询今日支付成功次数
		Integer toDayPayCount = qrGoodsOrderMapper.selectToDayPay(qrGoodsOrder);
		resultMap.put("lastDayPayCount", lastDayPayCount);
		resultMap.put("toDayPayCount", toDayPayCount);
		logger.info("查询商品订单结束。。。{}"+resultMap);
		return resultMap;
	}
	
}