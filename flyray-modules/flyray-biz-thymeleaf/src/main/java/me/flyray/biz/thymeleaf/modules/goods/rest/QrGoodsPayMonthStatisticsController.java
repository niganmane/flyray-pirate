package me.flyray.biz.thymeleaf.modules.goods.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.github.wxiaoqi.security.common.rest.BaseController;

import me.flyray.biz.thymeleaf.modules.goods.biz.QrGoodsPayMonthStatisticsBiz;
import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsPayMonthStatistics;

@Controller
@RequestMapping("qrGoodsPayMonthStatistics")
public class QrGoodsPayMonthStatisticsController extends BaseController<QrGoodsPayMonthStatisticsBiz,QrGoodsPayMonthStatistics> {

}