package me.flyray.biz.thymeleaf.modules.goods.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * 商品订单
 * @author he
 * @date 2018-09-12 14:11:56
 */
@Table(name = "qr_goods_order")
public class QrGoodsOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//用户编号
	@Column(name = "customer_id")
	private String customerId;

	//商品编号
	@Column(name = "goods_id")
	private String goodsId;

	//支付订单号
	@Column(name = "pay_order_no")
	private String payOrderNo;

	//金额
	@Column(name = "tx_amt")
	private BigDecimal txAmt;

	//状态 00支付成功 01支付失败 02待支付 03支付中
	@Column(name = "status")
	private String status;

	//支付时间
	@Column(name = "pay_time")
	private Date payTime;
	
	//微信openid
	@Column(name = "open_id")
	private String openId;
	
	//微信昵称
	@Column(name = "nick_name")
	private String nickName;
	
	//状态
	@Transient
	private String txStatus;


	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户账号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户账号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：用户编号
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：用户编号
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * 设置：商品编号
	 */
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	/**
	 * 获取：商品编号
	 */
	public String getGoodsId() {
		return goodsId;
	}
	/**
	 * 设置：支付订单号
	 */
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	/**
	 * 获取：支付订单号
	 */
	public String getPayOrderNo() {
		return payOrderNo;
	}
	/**
	 * 设置：金额
	 */
	public void setTxAmt(BigDecimal txAmt) {
		this.txAmt = txAmt;
	}
	/**
	 * 获取：金额
	 */
	public BigDecimal getTxAmt() {
		return txAmt;
	}
	/**
	 * 设置：状态 00支付成功 01支付失败 02待支付 03支付中
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：状态 00支付成功 01支付失败 02待支付 03支付中
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：支付时间
	 */
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	/**
	 * 获取：支付时间
	 */
	public Date getPayTime() {
		return payTime;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getTxStatus() {
		String status = getStatus();
		if("00".equals(status)){
			txStatus = "支付成功";
		}else if("01".equals(status)){
			txStatus = "支付失败";
		}else if("02".equals(status)){
			txStatus = "待支付";
		}else if("03".equals(status)){
			txStatus = "支付中";
		}
		return txStatus;
	}
	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}
}
