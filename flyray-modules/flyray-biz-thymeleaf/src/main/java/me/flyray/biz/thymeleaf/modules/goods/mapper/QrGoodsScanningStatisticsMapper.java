package me.flyray.biz.thymeleaf.modules.goods.mapper;

import java.util.List;
import java.util.Map;

import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsScanningStatistics;
import tk.mybatis.mapper.common.Mapper;

/**
 * 扫描统计
 * 
 * @author chj
 * @email ${email}
 * @date 2018-09-28 16:08:28
 */
@org.apache.ibatis.annotations.Mapper
public interface QrGoodsScanningStatisticsMapper extends Mapper<QrGoodsScanningStatistics> {
	
	public List<QrGoodsScanningStatistics> selectStatisticsList(Map<String, Object> request);
	
}
