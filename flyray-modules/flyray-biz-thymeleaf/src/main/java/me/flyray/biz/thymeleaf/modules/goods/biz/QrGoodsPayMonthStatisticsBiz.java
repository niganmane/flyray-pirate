package me.flyray.biz.thymeleaf.modules.goods.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.common.biz.BaseBiz;

import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsPayMonthStatistics;
import me.flyray.biz.thymeleaf.modules.goods.mapper.QrGoodsPayMonthStatisticsMapper;

/**
 * 商品二维码月份统计
 *
 * @author chj
 * @email ${email}
 * @date 2018-09-28 16:08:28
 */
@Service
public class QrGoodsPayMonthStatisticsBiz extends BaseBiz<QrGoodsPayMonthStatisticsMapper,QrGoodsPayMonthStatistics> {
}