package me.flyray.biz.thymeleaf.api;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONObject;
import com.github.wxiaoqi.security.common.util.EntityUtils;


/**
 * 
 * */
public class BaseController {

	/**
	 * 读取字节流参数转换成Map
	 * @throws IllegalAccessException 
	 * @throws Exception 
	 *
	 * */
	public Map<String, Object> ParamStr2Bean(HttpServletRequest request) throws Exception{
		String jsonStr =EntityUtils.getBodyData(request);
		Map<String, Object>map=(Map<String, Object>) JSONObject.parse(jsonStr);
		return map;
	}
//	public Map<String, Object> ParamStr2Bean(HttpServletRequest request) throws Exception{
//		String params=String.valueOf(request.getAttribute("param"));
//		@SuppressWarnings("unchecked")
//		Map<String, Object>map=(Map<String, Object>) JSONObject.parse(params);
//		return map;
//	}
	 
}
