package me.flyray.biz.thymeleaf.modules.goods.mapper;

import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsInfo;
import tk.mybatis.mapper.common.Mapper;

/**
 * 商品管理
 * @author he
 * @date 2018-09-12 14:11:56
 */
@org.apache.ibatis.annotations.Mapper
public interface QrGoodsInfoMapper extends Mapper<QrGoodsInfo> {
	
}
