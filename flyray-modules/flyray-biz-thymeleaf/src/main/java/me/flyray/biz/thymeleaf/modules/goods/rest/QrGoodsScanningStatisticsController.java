package me.flyray.biz.thymeleaf.modules.goods.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.qr.request.QrQueryGoodsParam;
import com.github.wxiaoqi.security.common.rest.BaseController;

import me.flyray.biz.thymeleaf.modules.goods.biz.QrGoodsScanningStatisticsBiz;
import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsScanningStatistics;

@Controller
@RequestMapping("qrGoodsScanningStatistics")
public class QrGoodsScanningStatisticsController extends BaseController<QrGoodsScanningStatisticsBiz,QrGoodsScanningStatistics> {

	private static final Logger logger = LoggerFactory.getLogger(QrGoodsScanningStatisticsController.class);
	
	/**
	 * 查询扫描列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/list", method=RequestMethod.POST)
	public TableResultResponse<QrGoodsScanningStatistics> query(@RequestBody QrQueryGoodsParam param) {
		logger.info("查询扫描列表。。。{}"+param);
		return baseBiz.queryList(param);
	}
	
}