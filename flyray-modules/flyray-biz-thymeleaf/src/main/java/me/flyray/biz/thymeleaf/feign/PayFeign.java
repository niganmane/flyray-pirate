package me.flyray.biz.thymeleaf.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 支付相关接口调用
 * @author he
 *
 */
@FeignClient(value = "flyray-pay-core")
public interface PayFeign {
	
	/**
	 * 创建订单
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/payment/creatPayOrder",method = RequestMethod.POST)
	public Map<String, Object> createPayOrder(@RequestBody Map<String, Object> params);
	
	/**
	 * 支付
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/payment/doPay",method = RequestMethod.POST)
	public Map<String, Object> doPay(@RequestBody Map<String, Object> params);

}
