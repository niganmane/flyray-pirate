package me.flyray.biz.thymeleaf.modules.goods.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.qr.request.QrQueryOrderParam;
import com.github.wxiaoqi.security.common.qr.request.QrThenPayStatisticsParam;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

import me.flyray.biz.thymeleaf.modules.goods.biz.QrGoodsInfoBiz;
import me.flyray.biz.thymeleaf.modules.goods.biz.QrGoodsMonthStatisticsBiz;
import me.flyray.biz.thymeleaf.modules.goods.biz.QrGoodsOrderBiz;
import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsOrder;


/**
 * 商品订单
 * @author Administrator
 *
 */
@Controller
@RequestMapping("qrGoodsOrders")
public class QrGoodsOrderController extends BaseController<QrGoodsOrderBiz, QrGoodsOrder> {
	
	private static final Logger logger = LoggerFactory.getLogger(QrGoodsOrderController.class);

	
	@Autowired
	private QrGoodsInfoBiz goodsInfoBiz;
	
	@Autowired
	private QrGoodsMonthStatisticsBiz goodsMonthBiz;
	
	/**
	 * 查询商品列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/list", method=RequestMethod.POST)
	public TableResultResponse<QrGoodsOrder> query(@RequestBody QrQueryOrderParam param) {
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		logger.info("查询商品订单列表。。。{}"+param);
		return baseBiz.queryList(param);
	}
	
	@ResponseBody
	@RequestMapping(value="/dataCount1", method=RequestMethod.POST)
	public Map<String, Object> queryDataCount1(@RequestBody QrThenPayStatisticsParam param) {
		logger.info("付费次数统计。。。{}"+param);
		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("platformId", param.getPlatformId());
		reqMap.put("merchantId", param.getMerchantId());
		reqMap.put("goodsId", param.getGoodsId());
		reqMap.put("year", param.getYear());
		Map<String, Object> respMap =  goodsMonthBiz.queryThenPayStatisticsList(reqMap);
		return ResponseHelper.success(respMap, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
	
	
	@ResponseBody
	@RequestMapping(value="/dataCount", method=RequestMethod.POST)
	public Map<String, Object> queryDataCount(@RequestBody QrThenPayStatisticsParam param) {
		logger.info("付费次数统计。。。{}"+param);
		Map<String, Object> reqMap = new HashMap<>();
		reqMap.put("goodsId", param.getGoodsId());
		Map<String, Object> respMap =  baseBiz.dayPayStatistics(reqMap);
		return ResponseHelper.success(respMap, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
	@ResponseBody
	@RequestMapping(value="/areaCount", method=RequestMethod.POST)
	public Map<String, Object> queryAreaCount(@RequestBody QrThenPayStatisticsParam param) {
		logger.info("地区付费次数统计。。。{}"+param);
		List<Map<String, Object>> list = goodsInfoBiz.queryAreaCount(param);
		return ResponseHelper.success(list, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
}