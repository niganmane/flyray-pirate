package me.flyray.biz.thymeleaf.modules.goods.mapper;

import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsPayMonthStatistics;
import tk.mybatis.mapper.common.Mapper;

/**
 * 商品二维码月份统计
 * 
 * @author chj
 * @email ${email}
 * @date 2018-09-28 16:08:28
 */
@org.apache.ibatis.annotations.Mapper
public interface QrGoodsPayMonthStatisticsMapper extends Mapper<QrGoodsPayMonthStatistics> {
	
}
