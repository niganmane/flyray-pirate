package me.flyray.biz.thymeleaf.modules.goods.mapper;

import java.util.List;
import java.util.Map;

import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsOrder;
import tk.mybatis.mapper.common.Mapper;

/**
 * 商品订单
 * @author he
 * @date 2018-09-12 14:11:56
 */
@org.apache.ibatis.annotations.Mapper
public interface QrGoodsOrderMapper extends Mapper<QrGoodsOrder> {
	
	/**
	 * 查询昨日支付次数
	 * @param qrGoodsOrder
	 * @return
	 */
	public Integer selectLastDayPayCount(QrGoodsOrder qrGoodsOrder);
	
	/**
	 * 查询今日支付次数
	 * @param qrGoodsOrder
	 * @return
	 */
	public Integer selectToDayPay(QrGoodsOrder qrGoodsOrder);
	
	public List<QrGoodsOrder> selectGoodsList(Map<String, Object> request);
	
	/**
	 * 查询支付成功次数
	 * @param qrGoodsOrder
	 * @return
	 */
	public Integer selectSuccessCount(QrGoodsOrder qrGoodsOrder);
	
}
