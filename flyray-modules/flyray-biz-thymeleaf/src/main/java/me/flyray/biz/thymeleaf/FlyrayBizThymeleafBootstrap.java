package me.flyray.biz.thymeleaf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"com.github.wxiaoqi.security.auth.client.feign","com.github.wxiaoqi.security.biz.client","me.flyray.biz.thymeleaf.feign"})
@EnableScheduling
@MapperScan("me.flyray.biz.thymeleaf")
public class FlyrayBizThymeleafBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(FlyrayBizThymeleafBootstrap.class);
    }

}
