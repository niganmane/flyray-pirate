package me.flyray.biz.thymeleaf.modules.goods.biz;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;

import com.github.wxiaoqi.security.common.biz.BaseBiz;

import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsMonthStatistics;
import me.flyray.biz.thymeleaf.modules.goods.mapper.QrGoodsMonthStatisticsMapper;

/**
 * 商品二维码月份统计
 *
 * @author he
 * @date 2018-09-27 19:57:19
 */
@Service
public class QrGoodsMonthStatisticsBiz extends BaseBiz<QrGoodsMonthStatisticsMapper,QrGoodsMonthStatistics> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(QrGoodsMonthStatisticsBiz.class);
	
	/**
	 * 查询当年该商品支付次数统计
	 * @param request
	 * @return
	 */
	public Map<String, Object> queryThenPayStatisticsList(@RequestBody Map<String, Object> params){
		logger.info("查询当年该商品支付次数统计。。。{}"+params);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		String platformId = (String) params.get("platformId");
		String merchantId = (String) params.get("merchantId");
		String goodsId = (String) params.get("goodsId");
		String year = (String) params.get("year");
		QrGoodsMonthStatistics qrGoodsMonthStatistics = new QrGoodsMonthStatistics();
		qrGoodsMonthStatistics.setPlatformId(platformId);
		qrGoodsMonthStatistics.setMerchantId(merchantId);
		qrGoodsMonthStatistics.setGoodsId(goodsId);
		Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        String month = sdf.format(date);
        int monthInt = Integer.valueOf(month);
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for(int i = 1; i <= 12; i++){
        	Map<String, Object> map = new HashMap<String, Object>();
        	if(monthInt >= i){
        		String format = String.format("%02d", i);
        		SimpleDateFormat one = new SimpleDateFormat("yyyy-"+format);
        		if(!StringUtils.isEmpty(year)){
        			one = new SimpleDateFormat(year+ "-" +format);
        		}
        		
        		qrGoodsMonthStatistics.setMonth(one.format(date));
        		QrGoodsMonthStatistics queryThen = mapper.selectOne(qrGoodsMonthStatistics);
        		if(null != queryThen){
        			map.put("count", queryThen.getQuantity());
        			map.put("release", String.valueOf(i) + "月");
        		}else{
        			map.put("count", 0);
        			map.put("release", String.valueOf(i) + "月");
        		}
        	}else{
        		map.put("count", null);
        		map.put("release", String.valueOf(i) + "月");
        	}
        	list.add(map);
        }
        resultMap.put("statisticsList", list);
		return resultMap;
	}
	
}