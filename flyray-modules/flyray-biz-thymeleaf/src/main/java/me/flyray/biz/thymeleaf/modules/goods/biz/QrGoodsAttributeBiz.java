package me.flyray.biz.thymeleaf.modules.goods.biz;

import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.SceneRequestParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.qr.request.QrGoodsAttributeRequestParam;

import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsAttribute;
import me.flyray.biz.thymeleaf.modules.goods.mapper.QrGoodsAttributeMapper;
import tk.mybatis.mapper.entity.Example;

/**
 * 商品属性
 * @author he
 * @date 2018-09-12 14:11:56
 */
@Service
public class QrGoodsAttributeBiz extends BaseBiz<QrGoodsAttributeMapper,QrGoodsAttribute> {
	
	private static final Logger logger = LoggerFactory.getLogger(QrGoodsAttributeBiz.class);
	/**
	 * 列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<QrGoodsAttribute> queryList(Map<String, Object> param) {
		Example example = new Example(QrGoodsAttribute.class);
		Page<QrGoodsAttribute> result = PageHelper.startPage(Integer.valueOf((String) param.get("page")), Integer.valueOf((String) param.get("limit")));
		List<QrGoodsAttribute> list = mapper.selectByExample(example);
		return new TableResultResponse<QrGoodsAttribute>(result.getTotal(), list);
	}
	
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	public QrGoodsAttribute addAttribute(QrGoodsAttributeRequestParam param) {
		logger.info("添加商品属性,请求参数。。。{}"+param);
		QrGoodsAttribute reqConfig = new QrGoodsAttribute();
		QrGoodsAttribute config1 = null;
		try {
			BeanUtils.copyProperties(reqConfig, param);
			config1 = mapper.selectOne(reqConfig);
			if (config1 == null) {
				mapper.insert(reqConfig);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config1;
	}
	
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	public QrGoodsAttribute updateAttribute(QrGoodsAttributeRequestParam param) {
		logger.info("修改商品属性,请求参数。。。{}"+param);
		QrGoodsAttribute reqConfig = new QrGoodsAttribute();
		QrGoodsAttribute config1 = null;
		try {
			BeanUtils.copyProperties(reqConfig, param);
			reqConfig.setId(Long.valueOf(param.getId()));
			config1 = mapper.selectOne(reqConfig);
			if (config1 != null) {
				mapper.updateByPrimaryKeySelective(reqConfig);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config1;
	}
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	public QrGoodsAttribute deleteAttribute(QrGoodsAttributeRequestParam param) {
		logger.info("删除商品属性,请求参数。。。{}"+param);
		QrGoodsAttribute reqConfig = new QrGoodsAttribute();
		QrGoodsAttribute config1 = null;
		try {
			BeanUtils.copyProperties(reqConfig, param);
			reqConfig.setId(Long.valueOf(param.getId()));
			config1 = mapper.selectOne(reqConfig);
			if (config1 != null) {
				mapper.delete(reqConfig);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config1;
	}
	
}