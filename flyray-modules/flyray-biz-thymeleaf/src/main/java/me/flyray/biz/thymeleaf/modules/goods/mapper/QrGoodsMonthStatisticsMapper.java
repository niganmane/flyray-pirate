package me.flyray.biz.thymeleaf.modules.goods.mapper;

import java.util.List;

import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsMonthStatistics;
import tk.mybatis.mapper.common.Mapper;

/**
 * 商品二维码月份统计
 * 
 * @author he
 * @date 2018-09-27 19:57:19
 */
@org.apache.ibatis.annotations.Mapper
public interface QrGoodsMonthStatisticsMapper extends Mapper<QrGoodsMonthStatistics> {
	
	/**
	 * 当年支付次数统计
	 * @param qrGoodsMonthStatistics
	 * @return
	 */
	public List<QrGoodsMonthStatistics> queryThenList(QrGoodsMonthStatistics qrGoodsMonthStatistics);
	
}
