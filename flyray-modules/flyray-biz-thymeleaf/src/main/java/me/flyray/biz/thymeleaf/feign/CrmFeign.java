package me.flyray.biz.thymeleaf.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 支付相关接口调用
 * @author he
 *
 */
@FeignClient(value = "flyray-crm-core")
public interface CrmFeign {
	
	/**
	 * 查询用户下的商户列表
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/merchantInfo/queryListByOwner",method = RequestMethod.POST)
	public Map<String, Object> queryListByOwner(@RequestBody Map<String, Object> params);
	
	/**
	 * 根据省查询商户
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/merchantInfo/queryByProvince",method = RequestMethod.POST)
	public Map<String, Object> queryByProvince(@RequestBody Map<String, Object> params);
	
	/**
	* 第三方支付入账
	* @param req
	* @return
	*/
	@RequestMapping(value = "feign/intoAccount",method = RequestMethod.POST)
	public Map<String, Object> inAccounting(@RequestBody Map<String, Object> params);
	
	/**
	* 查询商户通道费率
	* @param req
	* @return
	*/
	@RequestMapping(value = "feign/payChannelConfig/queryMerChannelFee",method = RequestMethod.POST)
	public Map<String, Object> queryMerChannelFee(@RequestBody Map<String, Object> params);
}
