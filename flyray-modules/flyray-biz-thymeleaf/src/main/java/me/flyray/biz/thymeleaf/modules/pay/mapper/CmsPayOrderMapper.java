package me.flyray.biz.thymeleaf.modules.pay.mapper;

import me.flyray.biz.thymeleaf.modules.pay.entity.CmsPayOrder;
import tk.mybatis.mapper.common.Mapper;

/**
 * 支付订单
 * @author he
 * @date 2018-08-09 16:34:06
 */
@org.apache.ibatis.annotations.Mapper
public interface CmsPayOrderMapper extends Mapper<CmsPayOrder> {
	
}
