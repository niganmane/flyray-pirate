package me.flyray.biz.thymeleaf.modules.goods.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 扫描统计
 * 
 * @author chj
 * @email ${email}
 * @date 2018-09-28 16:08:28
 */
@Table(name = "qr_goods_scanning_statistics")
public class QrGoodsScanningStatistics implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //商户号
    @Column(name = "merchant_id")
    private String merchantId;
	
	    //商品id
    @Column(name = "goods_id")
    private String goodsId;
	
	    //扫描日期
    @Column(name = "scanning_time")
    private Date scanningTime;
	
	    //扫描用户openid
    @Column(name = "user_wechat_open_id")
    private String userWechatOpenId;
    
    //昵称
    @Column(name = "nick_name")
    private String nickname;
	

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：商品id
	 */
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	/**
	 * 获取：商品id
	 */
	public String getGoodsId() {
		return goodsId;
	}
	/**
	 * 设置：扫描日期
	 */
	public void setScanningTime(Date scanningTime) {
		this.scanningTime = scanningTime;
	}
	/**
	 * 获取：扫描日期
	 */
	public Date getScanningTime() {
		return scanningTime;
	}
	/**
	 * 设置：扫描用户openid
	 */
	public void setUserWechatOpenId(String userWechatOpenId) {
		this.userWechatOpenId = userWechatOpenId;
	}
	/**
	 * 获取：扫描用户openid
	 */
	public String getUserWechatOpenId() {
		return userWechatOpenId;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
}
