package me.flyray.biz.thymeleaf.modules.goods.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 商品属性
 * @author he
 * @date 2018-09-12 14:11:56
 */
@Table(name = "qr_goods_attribute")
public class QrGoodsAttribute implements Serializable {
	private static final long serialVersionUID = 1L;

	//序号
	@Id
	private Long id;

	//属性代码
	@Column(name = "code")
	private String code;

	//属性描述
	@Column(name = "description")
	private String desc;


	/**
	 * 设置：序号
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：属性代码
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * 获取：属性代码
	 */
	public String getCode() {
		return code;
	}
	/**
	 * 设置：属性描述
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
	/**
	 * 获取：属性描述
	 */
	public String getDesc() {
		return desc;
	}
}
