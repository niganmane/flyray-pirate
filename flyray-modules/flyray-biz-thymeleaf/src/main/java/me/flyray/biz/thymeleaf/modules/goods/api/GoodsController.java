package me.flyray.biz.thymeleaf.modules.goods.api;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.qr.request.QrMerchantGoodsParam;
import com.github.wxiaoqi.security.common.qr.request.QrRecordingScanningNumParam;
import com.github.wxiaoqi.security.common.qr.request.QrThenPayStatisticsParam;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.flyray.biz.thymeleaf.api.BaseController;
import me.flyray.biz.thymeleaf.modules.goods.biz.QrGoodsInfoBiz;
import me.flyray.biz.thymeleaf.modules.goods.biz.QrGoodsMonthStatisticsBiz;
import me.flyray.biz.thymeleaf.modules.goods.biz.QrGoodsOrderBiz;
import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsInfo;

/**
 * 商品二维码接口
 * @author he
 *
 */
@Api(tags="商品二维码管理")
@Controller
@RequestMapping("qrGoods")
public class GoodsController extends BaseController {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(GoodsController.class);

	@Autowired
	private QrGoodsInfoBiz qrGoodsInfoBiz;
	@Autowired
	private QrGoodsMonthStatisticsBiz qrGoodsMonthStatisticsBiz;
	@Autowired
	private QrGoodsOrderBiz qrGoodsOrderBiz;

	/**
	 * 记录扫描次数
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("记录扫描次数")
	@RequestMapping(value = "/recordingScanningNum",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> recordingScanningNum(@RequestBody @Valid QrRecordingScanningNumParam qrRecordingScanningNumParam) throws Exception {
		QrGoodsInfo qrGoodsInfo = qrGoodsInfoBiz.recordingScanningNum(EntityUtils.beanToMap(qrRecordingScanningNumParam));
		if(null != qrGoodsInfo){
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else{
			return ResponseHelper.success(null, null, ResponseCode.GOODS_IS_NULL.getCode(), ResponseCode.GOODS_IS_NULL.getMessage());
		}
	}


	@ResponseBody
	@RequestMapping(value="/info", method=RequestMethod.POST)
	public Map<String, Object> createQr(@RequestBody Map<String, Object> param) {
		logger.info("商品详情参数。。。{}"+param);
		QrGoodsInfo info = qrGoodsInfoBiz.queryGoodsInfo(param);
		if (info != null) {
			return ResponseHelper.success(info, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.GOODS_IS_NULL.getCode(), ResponseCode.GOODS_IS_NULL.getMessage());
		}
	}

	/**
	 * 获取商品密码
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("获取商品密码")
	@RequestMapping(value = "/obtainGoodsPwd",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> obtainGoodsPwd(@RequestBody @Valid QrRecordingScanningNumParam qrRecordingScanningNumParam) throws Exception {
		QrGoodsInfo qrGoodsInfo = qrGoodsInfoBiz.obtainGoodsPwd(EntityUtils.beanToMap(qrRecordingScanningNumParam));
		if(null != qrGoodsInfo){
			return ResponseHelper.success(qrGoodsInfo, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else{
			return ResponseHelper.success(null, null, ResponseCode.GOODS_IS_NULL.getCode(), ResponseCode.GOODS_IS_NULL.getMessage());
		}
	}

	/**
	 * 查询商户商品列表
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("查询商户商品列表")
	@RequestMapping(value = "/queryMerchantGoodsList",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryMerchantGoodsList(@RequestBody @Valid QrMerchantGoodsParam qrMerchantGoodsParam) throws Exception {
		Map<String, Object> respMap = qrGoodsInfoBiz.queryMerchantGoodsList(EntityUtils.beanToMap(qrMerchantGoodsParam));
		if(null != respMap){
			return ResponseHelper.success(respMap, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else{
			return ResponseHelper.success(null, null, ResponseCode.GOODS_IS_NULL.getCode(), ResponseCode.GOODS_IS_NULL.getMessage());
		}
	}

	/**
	 * 查询当年该商品支付次数统计
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("查询当年该商品支付次数统计")
	@RequestMapping(value = "/queryThenPayStatisticsList",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryThenPayStatisticsList(@RequestBody @Valid QrThenPayStatisticsParam qrThenPayStatisticsParam) throws Exception {
		Map<String, Object> respMap = qrGoodsMonthStatisticsBiz.queryThenPayStatisticsList(EntityUtils.beanToMap(qrThenPayStatisticsParam));
		return ResponseHelper.success(respMap, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
	/**
	 * 日付费次数统计 昨天与今天
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("日付费次数统计 昨天与今天")
	@RequestMapping(value = "/dayPayStatistics",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> dayPayStatistics(@RequestBody @Valid QrThenPayStatisticsParam qrThenPayStatisticsParam) throws Exception {
		Map<String, Object> respMap = qrGoodsOrderBiz.dayPayStatistics(EntityUtils.beanToMap(qrThenPayStatisticsParam));
		return ResponseHelper.success(respMap, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
	@ApiOperation("商品绑定商户")
	@RequestMapping(value = "/doBind",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> doBind(@RequestBody @Valid QrThenPayStatisticsParam qrThenPayStatisticsParam) throws Exception {
		QrGoodsInfo goodsInfo = qrGoodsInfoBiz.doBindMerchant(qrThenPayStatisticsParam);
		if(null != goodsInfo){
			return ResponseHelper.success(goodsInfo, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else{
			return ResponseHelper.success(null, null, ResponseCode.GOODS_IS_NULL.getCode(), ResponseCode.GOODS_IS_NULL.getMessage());
		}
	}
	
	
	@ApiOperation("商品取消绑定商户")
	@RequestMapping(value = "/cancelBind",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> cancelBind(@RequestBody @Valid QrThenPayStatisticsParam qrThenPayStatisticsParam) throws Exception {
		QrGoodsInfo goodsInfo = qrGoodsInfoBiz.cancelBindMerchant(qrThenPayStatisticsParam);
		if(null != goodsInfo){
			return ResponseHelper.success(goodsInfo, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else{
			return ResponseHelper.success(null, null, ResponseCode.GOODS_IS_NULL.getCode(), ResponseCode.GOODS_IS_NULL.getMessage());
		}
	}
	

}
