package me.flyray.biz.thymeleaf.modules.goods.biz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.qr.request.QrQueryGoodsParam;

import me.flyray.biz.thymeleaf.feign.CrmFeign;
import me.flyray.biz.thymeleaf.modules.goods.entity.QrGoodsScanningStatistics;
import me.flyray.biz.thymeleaf.modules.goods.mapper.QrGoodsScanningStatisticsMapper;

/**
 * 扫描统计
 *
 * @author chj
 * @email ${email}
 * @date 2018-09-28 16:08:28
 */
@Service
public class QrGoodsScanningStatisticsBiz extends BaseBiz<QrGoodsScanningStatisticsMapper,QrGoodsScanningStatistics> {
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(QrGoodsInfoBiz.class);
	@Autowired
	private CrmFeign crmFeign;
	
	/**
	 * 
	 * @param param
	 * @return
	 */
	public TableResultResponse<QrGoodsScanningStatistics> queryList(QrQueryGoodsParam param) {
		Map<String, Object> reqMap = new HashMap<String, Object>();
		if (!StringUtils.isEmpty(param.getPlatformId())) {
			reqMap.put("platformId", param.getPlatformId());
		}
		if (!StringUtils.isEmpty(param.getMerchantId())) {
			reqMap.put("merchantId", param.getMerchantId());
		}else{
			if(!StringUtils.isEmpty(param.getProvince())){
				//查询省对应的商户号
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("platformId", param.getPlatformId()); 
				params.put("province", param.getProvince());
				Map<String, Object> queryByProvince = crmFeign.queryByProvince(params);
				List<Map<String, Object>> merchantList = (List<Map<String, Object>>) queryByProvince.get("merchantList");
				List<String> paramsList = new ArrayList<>();
				for(int i = 0; i < merchantList.size(); i++){
					paramsList.add((String) merchantList.get(i).get("merchantId"));
				}
				reqMap.put("paramsList", paramsList);
			}
		}
		if (!StringUtils.isEmpty(param.getGoodsId())) {
			reqMap.put("goodsId", param.getGoodsId());
		}
		Page<QrGoodsScanningStatistics> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<QrGoodsScanningStatistics> list = mapper.selectStatisticsList(reqMap);
		TableResultResponse<QrGoodsScanningStatistics> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
}