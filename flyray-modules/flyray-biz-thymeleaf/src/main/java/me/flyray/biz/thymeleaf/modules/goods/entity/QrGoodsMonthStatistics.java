package me.flyray.biz.thymeleaf.modules.goods.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 商品二维码月份统计
 * 
 * @author he
 * @date 2018-09-27 19:57:19
 */
@Table(name = "qr_goods_pay_month_statistics")
public class QrGoodsMonthStatistics implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户号
	@Column(name = "merchant_id")
	private String merchantId;
	
	//商品id
	@Column(name = "goods_id")
	private String goodsId;

	//月份 yyyy-mm
	@Column(name = "month")
	private String month;

	//付费次数
	@Column(name = "quantity")
	private Integer quantity;


	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	/**
	 * 设置：月份 yyyy-mm
	 */
	public void setMonth(String month) {
		this.month = month;
	}
	/**
	 * 获取：月份 yyyy-mm
	 */
	public String getMonth() {
		return month;
	}
	/**
	 * 设置：付费次数
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * 获取：付费次数
	 */
	public Integer getQuantity() {
		return quantity;
	}
}
