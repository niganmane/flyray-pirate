package com.github.wxiaoqi.security.crm.core.biz;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.common.crm.request.AccountTransferRequest;
import com.github.wxiaoqi.security.common.enums.AccountStateEnums;
import com.github.wxiaoqi.security.common.enums.AccountTransferTypeEnums;
import com.github.wxiaoqi.security.common.enums.InOutFlagEnums;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.MD5;
import com.github.wxiaoqi.security.crm.core.entity.*;
import com.github.wxiaoqi.security.crm.core.mapper.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 转账相关的操作
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class CommonAccountTransferBiz {

	@Value("${bountyHunter.balanceSaltValue}")
	private String balanceSaltValue;
	@Autowired
	private MerchantAccountMapper merAccountMapper;
	@Autowired
	private PersonalAccountMapper personalAccountMapper;
	@Autowired
	private PersonalAccountJournalMapper personalAccountJournalMapper;
	@Autowired
	private MerchantAccountJournalMapper merchantAccountJournalMapper;
	@Autowired
	private PlatformAccoutConfigMapper platformAccoutConfigMapper;
	
	/**
	 * 个人或者商户转账接口 
	 */
	public Map<String, Object> accountTransfer(AccountTransferRequest accountTransferRequest) {
		log.info("个人或者商户入账相关的接口 请求参数：{}" + EntityUtils.beanToMap(accountTransferRequest));
		Map<String, Object> response = new HashMap<String, Object>();
		//个人转个人
		if(AccountTransferTypeEnums.p2p.getCode().equals(accountTransferRequest.getTransferType())){
			log.info("个人或者商户入账相关的接口-个人账户转个人账户");
			Map<String, Object>intoPerAccount=personAccountOperate(accountTransferRequest.getPlatformId(),accountTransferRequest.getIntoAccountId(),accountTransferRequest.getIntoAccountType(),accountTransferRequest.getIntoCustomerType(),InOutFlagEnums.in.getCode(),accountTransferRequest.getTransferAmt(),accountTransferRequest.getTradeType(),accountTransferRequest.getOrderNo());
			if(!ResponseCode.OK.getCode().equals(intoPerAccount.get("code"))){
				return intoPerAccount;
			}
			Map<String, Object>outPerAccount=personAccountOperate(accountTransferRequest.getPlatformId(),accountTransferRequest.getOutAccountId(),accountTransferRequest.getIntoAccountType(),accountTransferRequest.getOutCustomerType(),InOutFlagEnums.out.getCode(),accountTransferRequest.getTransferAmt(),accountTransferRequest.getTradeType(),accountTransferRequest.getOrderNo());
			if(!ResponseCode.OK.getCode().equals(outPerAccount.get("code"))){
				return outPerAccount;
			}
		}
		//个人转商户
		if(AccountTransferTypeEnums.p2m.getCode().equals(accountTransferRequest.getTransferType())){
			log.info("个人或者商户入账相关的接口-个人账户转商户账户");
			Map<String, Object>intoMerAccount=merchantAccountOperate(accountTransferRequest.getPlatformId(),accountTransferRequest.getIntoAccountId(),accountTransferRequest.getIntoAccountType(),accountTransferRequest.getIntoCustomerType(),InOutFlagEnums.in.getCode(),accountTransferRequest.getTransferAmt(),accountTransferRequest.getTradeType(),accountTransferRequest.getOrderNo());
			if(!ResponseCode.OK.getCode().equals(intoMerAccount.get("code"))){
				return intoMerAccount;
			}
			Map<String, Object>outPerAccount=personAccountOperate(accountTransferRequest.getPlatformId(),accountTransferRequest.getOutAccountId(),accountTransferRequest.getOutAccountType(),accountTransferRequest.getOutCustomerType(),InOutFlagEnums.out.getCode(),accountTransferRequest.getTransferAmt(),accountTransferRequest.getTradeType(),accountTransferRequest.getOrderNo());
			if(!ResponseCode.OK.getCode().equals(outPerAccount.get("code"))){
				return outPerAccount;
			}
		}
		//商户转商户
		if(AccountTransferTypeEnums.m2m.getCode().equals(accountTransferRequest.getTransferType())){
			log.info("个人或者商户入账相关的接口-商户账户转商户账户");
			Map<String, Object>intoMerAccount=merchantAccountOperate(accountTransferRequest.getPlatformId(),accountTransferRequest.getIntoAccountId(),accountTransferRequest.getIntoCustomerType(),accountTransferRequest.getIntoAccountType(),InOutFlagEnums.in.getCode(),accountTransferRequest.getTransferAmt(),accountTransferRequest.getTradeType(),accountTransferRequest.getOrderNo());
			if(!ResponseCode.OK.getCode().equals(intoMerAccount.get("code"))){
				return intoMerAccount;
			}
			Map<String, Object>outMerAccount=merchantAccountOperate(accountTransferRequest.getPlatformId(),accountTransferRequest.getOutAccountId(),accountTransferRequest.getOutCustomerType(),accountTransferRequest.getOutAccountType(),InOutFlagEnums.out.getCode(),accountTransferRequest.getTransferAmt(),accountTransferRequest.getTradeType(),accountTransferRequest.getOrderNo());
			if(!ResponseCode.OK.getCode().equals(outMerAccount.get("code"))){
				return outMerAccount;
			}
		}
		//商户转个人
		if(AccountTransferTypeEnums.m2p.getCode().equals(accountTransferRequest.getTransferType())){
			log.info("个人或者商户入账相关的接口-商户账户转个人账户");
			Map<String, Object>outPerAccount=personAccountOperate(accountTransferRequest.getPlatformId(),accountTransferRequest.getIntoAccountId(),accountTransferRequest.getIntoAccountType(),accountTransferRequest.getIntoCustomerType(),InOutFlagEnums.in.getCode(),accountTransferRequest.getTransferAmt(),accountTransferRequest.getTradeType(),accountTransferRequest.getOrderNo());
			if(!ResponseCode.OK.getCode().equals(outPerAccount.get("code"))){
				return outPerAccount;
			}
			Map<String, Object>outMerAccount=merchantAccountOperate(accountTransferRequest.getPlatformId(),accountTransferRequest.getOutAccountId(),accountTransferRequest.getOutCustomerType(),accountTransferRequest.getOutAccountType(),InOutFlagEnums.out.getCode(),accountTransferRequest.getTransferAmt(),accountTransferRequest.getTradeType(),accountTransferRequest.getOrderNo());
			if(!ResponseCode.OK.getCode().equals(outMerAccount.get("code"))){
				return outMerAccount;
			}
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		return response;
	}

	
	/***
	 * 个人账户操作
	 * */
	public Map<String,Object> personAccountOperate(String platformId,String personalId,String accountType,String customerType,String inOutFlag,String transferAmt,String tradeType,String orderNo){
		Map<String, Object> response = new HashMap<String, Object>();
		PersonalAccount queryPerAccountparam=new PersonalAccount();
		queryPerAccountparam.setPlatformId(platformId);
		queryPerAccountparam.setPersonalId(personalId);
		queryPerAccountparam.setAccountType(accountType);
		try {
			PersonalAccount personalAccount=personalAccountMapper.selectOne(queryPerAccountparam);
			String accountId="";
			if(InOutFlagEnums.out.getCode().equals(inOutFlag)){
				if(null==personalAccount){//商户出账时 个人账户不能为空
					response.put("code", ResponseCode.PER_ACC_NOTEXIST.getCode());
					response.put("msg", ResponseCode.PER_ACC_NOTEXIST.getMessage());
					log.info("个人或者商户转账接口 返回参数。。。。。。{}", response);
					return response;
				}else if(null!=personalAccount&&AccountStateEnums.freeze.equals(personalAccount.getStatus())){
					response.put("code", ResponseCode.PER_ACC_FREEZE.getCode());
					response.put("msg", ResponseCode.PER_ACC_FREEZE.getMessage());
					log.info("个人或者商户转账接口。。。。。。{}", response);
					return response;
				}
				if (!MD5.sign(personalAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8").equals(personalAccount.getCheckSum())) {
					response.put("code", ResponseCode.PER_ACC_BALANCE_NOT_MATE.getCode());
					response.put("msg", ResponseCode.PER_ACC_BALANCE_NOT_MATE.getMessage());
					log.info("个人或者商户转账接口-返回参数。。。。。。{}", response);
					return response;
				}
				if(personalAccount.getAccountBalance().compareTo(new BigDecimal(transferAmt))<0){//个人账户余额不足
					response.put("code", ResponseCode.PER_ACC_BALANCE_NOT_ENOUGH.getCode());
					response.put("msg", ResponseCode.PER_ACC_BALANCE_NOT_ENOUGH.getMessage());
					log.info("个人或者商户转账接口-返回参数。。。。。。{}", response);
					return response;
				}
				personalAccount.setAccountBalance(personalAccount.getAccountBalance().subtract(new BigDecimal(transferAmt)));
				personalAccount.setCheckSum(MD5.sign(personalAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8"));
			}
			if(InOutFlagEnums.in.getCode().equals(inOutFlag)){
				if(null==personalAccount){//账户不存在的话则需要开通个人账户
					/**
					 * 获取平台下能开通的账户类型
					 * */
					PlatformAccoutConfig queryaccoutConfig=new PlatformAccoutConfig();
					queryaccoutConfig.setPlatformId(platformId);
					queryaccoutConfig.setCustomerType(customerType);
					queryaccoutConfig.setAccountType(accountType);
					PlatformAccoutConfig accoutConfig =platformAccoutConfigMapper.selectOne(queryaccoutConfig);
					if(null==accoutConfig){//平台支持的账户类型不存在 就没法开户
						response.put("code", ResponseCode.PLAT_ACC_TYPE_NOT_EXIST.getCode());
						response.put("msg", ResponseCode.PLAT_ACC_TYPE_NOT_EXIST.getMessage());
						log.info("个人或者商户转账接口-返回参数。。。。。。{}", response);
						return response;
					}
					/***
					 * 开通个人账户
					 * */
					PersonalAccount newPerAcc= new PersonalAccount();
					newPerAcc.setAccountId(String.valueOf(SnowFlake.getId()));
					newPerAcc.setPlatformId(platformId);
					newPerAcc.setPersonalId(personalId);
					newPerAcc.setAccountType(accountType);
					newPerAcc.setCcy("CNY");
					newPerAcc.setAccountBalance(new BigDecimal(transferAmt));
					newPerAcc.setCheckSum(MD5.sign(transferAmt.toString(), balanceSaltValue, "utf-8"));
					newPerAcc.setFreezeBalance(BigDecimal.ZERO);
					newPerAcc.setStatus(AccountStateEnums.normal.getCode());
					newPerAcc.setCreateTime(new Date());
					personalAccountMapper.insertSelective(newPerAcc);
				}else if(null!=personalAccount&&AccountStateEnums.freeze.equals(personalAccount.getStatus())){
					response.put("code", ResponseCode.PER_ACC_FREEZE.getCode());
					response.put("msg", ResponseCode.PER_ACC_FREEZE.getMessage());
					log.info("个人或者商户转账接口。。。。。。{}", response);
					return response;
				}else{
					if (!MD5.sign(personalAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8").equals(personalAccount.getCheckSum())) {
						response.put("code", ResponseCode.MER_ACC_BALANCE_NOT_MATE.getCode());
						response.put("msg", ResponseCode.MER_ACC_BALANCE_NOT_MATE.getMessage());
						log.info("个人或者商户转账接口-返回参数。。。。。。{}", response);
						return response;
					}
					personalAccount.setAccountBalance(personalAccount.getAccountBalance().add(new BigDecimal(transferAmt)));
					personalAccount.setCheckSum(MD5.sign(personalAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8"));
				}
				
			}
			personalAccountMapper.updateByPrimaryKeySelective(personalAccount);
			if (StringUtils.isEmpty(accountId)) {
				accountId = personalAccount.getAccountId();
			}
			PersonalAccountJournal personalAccountJournal=new PersonalAccountJournal();
			personalAccountJournal.setJournalId(String.valueOf(SnowFlake.getId()));
			personalAccountJournal.setPlatformId(platformId);
			personalAccountJournal.setPersonalId(personalId);
			personalAccountJournal.setAccountId(accountId);
			personalAccountJournal.setAccountType(accountType);
			personalAccountJournal.setOrderNo(orderNo);
			personalAccountJournal.setInOutFlag(inOutFlag);//来往标志  1：来账   2：往账
			personalAccountJournal.setTradeAmt(new BigDecimal(transferAmt));
			personalAccountJournal.setTradeType(tradeType);//交易类型（支付:01，退款:02，提现:03，充值:04）
			personalAccountJournal.setCreateTime(new Date());
			personalAccountJournalMapper.insertSelective(personalAccountJournal);
			response.put("code", ResponseCode.OK.getCode());
			response.put("msg", ResponseCode.OK.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			log.info("个人或者商户转账接口-报错。。。。。。{}"+e.getMessage());
			response.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			response.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		return response;
	}
	
	/**
	 * 
	 * 商户账户操作
	 * */
	public Map<String,Object> merchantAccountOperate(String platformId,String merchantId,String accountType,String merchantType,String inOutFlag,String transferAmt,String tradeType,String orderNo){
		Map<String, Object> response = new HashMap<String, Object>();
		MerchantAccount queryMerAccountparam=new MerchantAccount();
		queryMerAccountparam.setPlatformId(platformId);
		queryMerAccountparam.setMerchantId(merchantId);
		queryMerAccountparam.setAccountType(accountType);
		try {
			MerchantAccount merchantAccount = merAccountMapper.selectOne(queryMerAccountparam);
			String accountId="";
			if(InOutFlagEnums.out.getCode().equals(inOutFlag)){
				if(null==merchantAccount){//商户出账时 个人账户不能为空
					response.put("code", ResponseCode.MER_ACC_NOTEXIST.getCode());
					response.put("msg", ResponseCode.MER_ACC_NOTEXIST.getMessage());
					log.info("个人或者商户转账接口 返回参数。。。。。。{}", response);
					return response;
				}else if(null!=merchantAccount&&AccountStateEnums.freeze.equals(merchantAccount.getStatus())){
					response.put("code", ResponseCode.MER_ACC_FREEZE.getCode());
					response.put("msg", ResponseCode.MER_ACC_FREEZE.getMessage());
					log.info("个人或者商户转账接口。。。。。。{}", response);
					return response;
				}
				if (!MD5.sign(merchantAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8").equals(merchantAccount.getCheckSum())) {
					response.put("code", ResponseCode.MER_ACC_BALANCE_NOT_MATE.getCode());
					response.put("msg", ResponseCode.MER_ACC_BALANCE_NOT_MATE.getMessage());
					log.info("个人或者商户转账接口-返回参数。。。。。。{}", response);
					return response;
				}
				if(merchantAccount.getAccountBalance().compareTo(new BigDecimal(transferAmt))<0){//商户账户余额不足
					response.put("code", ResponseCode.MER_ACC_BALANCE_NOT_ENOUGH.getCode());
					response.put("msg", ResponseCode.MER_ACC_BALANCE_NOT_ENOUGH.getMessage());
					log.info("个人或者商户转账接口-返回参数。。。。。。{}", response);
					return response;
				}
				merchantAccount.setAccountBalance(merchantAccount.getAccountBalance().subtract(new BigDecimal(transferAmt)));
				merchantAccount.setCheckSum(MD5.sign(merchantAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8"));
			}
			if(InOutFlagEnums.in.getCode().equals(inOutFlag)){
				if(null==merchantAccount){//账户不存在的话则需要开通商户账户
					/**
					 * 获取平台下能开通的账户类型
					 * */
					PlatformAccoutConfig queryaccoutConfig=new PlatformAccoutConfig();
					queryaccoutConfig.setPlatformId(platformId);
					queryaccoutConfig.setCustomerType(merchantType);
					queryaccoutConfig.setAccountType(accountType);
					PlatformAccoutConfig accoutConfig =platformAccoutConfigMapper.selectOne(queryaccoutConfig);
					if(null==accoutConfig){//平台支持的账户类型不存在 就没法开户
						response.put("code", ResponseCode.PLAT_ACC_TYPE_NOT_EXIST.getCode());
						response.put("msg", ResponseCode.PLAT_ACC_TYPE_NOT_EXIST.getMessage());
						log.info("个人或者商户转账接口-返回参数。。。。。。{}", response);
						return response;
					}
					MerchantAccount newAccount = new MerchantAccount();
					accountId = String.valueOf(SnowFlake.getId());
					newAccount.setAccountId(accountId);
					newAccount.setPlatformId(platformId);
					newAccount.setMerchantId(merchantId);
					newAccount.setMerchantType(merchantType);
					newAccount.setAccountType(accountType);
					newAccount.setCcy("CNY");
					newAccount.setFreezeBalance(BigDecimal.ZERO);
					newAccount.setAccountBalance(new BigDecimal(transferAmt));
					newAccount.setCheckSum(MD5.sign(transferAmt.toString(), balanceSaltValue, "utf-8"));
					newAccount.setStatus(AccountStateEnums.normal.getCode());
					newAccount.setCreateTime(new Date());
					merAccountMapper.insertSelective(newAccount);
				}else if(null!=merchantAccount&&AccountStateEnums.freeze.equals(merchantAccount.getStatus())){
					response.put("code", ResponseCode.MER_ACC_FREEZE.getCode());
					response.put("msg", ResponseCode.MER_ACC_FREEZE.getMessage());
					log.info("个人或者商户转账接口。。。。。。{}", response);
					return response;
				}else{
					if (!MD5.sign(merchantAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8").equals(merchantAccount.getCheckSum())) {
						response.put("code", ResponseCode.MER_ACC_BALANCE_NOT_MATE.getCode());
						response.put("msg", ResponseCode.MER_ACC_BALANCE_NOT_MATE.getMessage());
						log.info("个人或者商户转账接口-返回参数。。。。。。{}", response);
						return response;
					}
					merchantAccount.setAccountBalance(merchantAccount.getAccountBalance().add(new BigDecimal(transferAmt)));
					merchantAccount.setCheckSum(MD5.sign(merchantAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8"));
					merAccountMapper.updateByPrimaryKeySelective(merchantAccount);
				}
			}
			if (StringUtils.isEmpty(accountId)) {
				accountId = merchantAccount.getAccountId();
			}
			MerchantAccountJournal merAccountJournal = new MerchantAccountJournal();
			merAccountJournal.setJournalId(String.valueOf(SnowFlake.getId()));
			merAccountJournal.setPlatformId(platformId);
			merAccountJournal.setMerchantId(merchantId);
			merAccountJournal.setAccountId(accountId);
			merAccountJournal.setAccountType(accountType);
			merAccountJournal.setOrderNo(orderNo);
			merAccountJournal.setInOutFlag(inOutFlag);//来往标志  1：来账   2：往账
			merAccountJournal.setTradeAmt(new BigDecimal(transferAmt));
			merAccountJournal.setTradeType(tradeType);//交易类型（支付:01，退款:02，提现:03，充值:04）
			merAccountJournal.setCreateTime(new Date());
			merchantAccountJournalMapper.insertSelective(merAccountJournal);
			response.put("code", ResponseCode.OK.getCode());
			response.put("msg", ResponseCode.OK.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			log.info("个人或者商户转账接口-报错。。。。。。{}"+e.getMessage());
			response.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			response.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		return response;
	}

}
