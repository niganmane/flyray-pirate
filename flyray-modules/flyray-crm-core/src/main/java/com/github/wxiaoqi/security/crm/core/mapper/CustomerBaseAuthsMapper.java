package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.CustomerBaseAuths;
import tk.mybatis.mapper.common.Mapper;

/**
 * 客户授权信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface CustomerBaseAuthsMapper extends Mapper<CustomerBaseAuths> {
	
}
