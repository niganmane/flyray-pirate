package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;


/**
 * 支付通道费率配置表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-06 13:58:35
 */
@Table(name = "pay_channel_fee_config")
public class PayChannelFeeConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //费率代码
    @Id
    private String feeCode;
	
	
	    //费率名称
    @Column(name = "fee_name")
    private String feeName;
	
	    //币种
    @Column(name = "ccy")
    private String ccy;
	
	    //计费方式 00固定金额，01固定比例
    @Column(name = "billing_method")
    private String billingMethod;
	
	    //收费金额
    @Column(name = "fee_amt")
    private BigDecimal feeAmt;
	
	    //收费比例
    @Column(name = "fee_ratio")
    private BigDecimal feeRatio;
	
	    //最低收费
    @Column(name = "min_fee")
    private BigDecimal minFee;
	
	    //最高收费
    @Column(name = "max_fee")
    private BigDecimal maxFee;
	

	/**
	 * 设置：费率代码
	 */
	public void setFeeCode(String feeCode) {
		this.feeCode = feeCode;
	}
	/**
	 * 获取：费率代码
	 */
	public String getFeeCode() {
		return feeCode;
	}
	/**
	 * 设置：费率名称
	 */
	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}
	/**
	 * 获取：费率名称
	 */
	public String getFeeName() {
		return feeName;
	}
	/**
	 * 设置：币种
	 */
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	/**
	 * 获取：币种
	 */
	public String getCcy() {
		return ccy;
	}
	/**
	 * 设置：计费方式
	 */
	public void setBillingMethod(String billingMethod) {
		this.billingMethod = billingMethod;
	}
	/**
	 * 获取：计费方式
	 */
	public String getBillingMethod() {
		return billingMethod;
	}
	/**
	 * 设置：收费金额
	 */
	public void setFeeAmt(BigDecimal feeAmt) {
		this.feeAmt = feeAmt;
	}
	/**
	 * 获取：收费金额
	 */
	public BigDecimal getFeeAmt() {
		return feeAmt;
	}
	/**
	 * 设置：收费比例
	 */
	public void setFeeRatio(BigDecimal feeRatio) {
		this.feeRatio = feeRatio;
	}
	/**
	 * 获取：收费比例
	 */
	public BigDecimal getFeeRatio() {
		return feeRatio;
	}
	/**
	 * 设置：最低收费
	 */
	public void setMinFee(BigDecimal minFee) {
		this.minFee = minFee;
	}
	/**
	 * 获取：最低收费
	 */
	public BigDecimal getMinFee() {
		return minFee;
	}
	/**
	 * 设置：最高收费
	 */
	public void setMaxFee(BigDecimal maxFee) {
		this.maxFee = maxFee;
	}
	/**
	 * 获取：最高收费
	 */
	public BigDecimal getMaxFee() {
		return maxFee;
	}
}
