package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 场景支付方式配置
 * 
 * @author chj
 * @email ${email}
 * @date 2018-09-07 10:55:04
 */
@Table(name = "scene_pay_method_config")
public class ScenePayMethodConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Long id;
	
	    //场景编号
    @Column(name = "scene_code")
    private String sceneCode;
	
	    //支付方式编号 1微信 2支付宝 3火源 4火钻 5余额 6原力值 7医保卡
    @Column(name = "method_code")
    private String methodCode;
	
	    //支付方式代码
    @Column(name = "method")
    private String method;
	

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：场景编号
	 */
	public void setSceneCode(String sceneCode) {
		this.sceneCode = sceneCode;
	}
	/**
	 * 获取：场景编号
	 */
	public String getSceneCode() {
		return sceneCode;
	}
	/**
	 * 设置：支付方式编号 1微信 2支付宝 3火源 4火钻 5余额 6原力值 7医保卡
	 */
	public void setMethodCode(String methodCode) {
		this.methodCode = methodCode;
	}
	/**
	 * 获取：支付方式编号 1微信 2支付宝 3火源 4火钻 5余额 6原力值 7医保卡
	 */
	public String getMethodCode() {
		return methodCode;
	}
	/**
	 * 设置：支付方式代码
	 */
	public void setMethod(String method) {
		this.method = method;
	}
	/**
	 * 获取：支付方式代码
	 */
	public String getMethod() {
		return method;
	}
}
