package com.github.wxiaoqi.security.crm.core.feignserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.OutAccountRequest;
import com.github.wxiaoqi.security.common.crm.request.UnFreAndOutAccountRequest;
import com.github.wxiaoqi.security.crm.core.biz.CommonOutAccountBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 出账相关的接口
 * */
@Api(tags="出账相关的接口")
@Controller
@RequestMapping("feign")
public class FeignCommonOutAccount {
	
	@Autowired
	private CommonOutAccountBiz outAccountBiz;
	
	
	/***
	 * 个人或者商户解冻并出账的接口
	 * */
	@ApiOperation("个人或者商户解冻并出账的接口")
	@RequestMapping(value = "/unFreAndOutAccount",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> unFreAndOutAccount(@RequestBody @Valid UnFreAndOutAccountRequest unFreAndOutAccountRequest){
		Map<String, Object> response = outAccountBiz.unFreAndOutAccount(unFreAndOutAccountRequest);
		return response;
	}
	
	/**
	 * 个人或者商户直接出账的接口
	 * */
	@ApiOperation("个人或者商户直接出账的接口")
	@RequestMapping(value = "/outAccount",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> outAccount(@RequestBody @Valid OutAccountRequest outAccountRequest){
		Map<String, Object> response = outAccountBiz.outAccount(outAccountRequest);
		return response;
    }

}
