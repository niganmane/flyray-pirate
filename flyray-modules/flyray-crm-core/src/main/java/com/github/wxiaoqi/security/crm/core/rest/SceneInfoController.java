package com.github.wxiaoqi.security.crm.core.rest;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.SceneRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.ResponseHelper;
import com.github.wxiaoqi.security.crm.core.biz.SceneInfoBiz;
import com.github.wxiaoqi.security.crm.core.entity.SceneInfo;

/**
 * 场景
 * @author Administrator
 *
 */


@Controller
@RequestMapping("sceneInfos")
public class SceneInfoController extends BaseController<SceneInfoBiz,SceneInfo> {
	
	private static final Logger logger = LoggerFactory.getLogger(SceneInfoController.class);
	
	@ResponseBody
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public TableResultResponse<SceneInfo> query(@RequestParam Map<String, Object> param) {
		logger.info("查询场景，请求参数。。。{}"+param);
		return baseBiz.queryScenes(param);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public Map<String, Object> add(@Valid @RequestBody SceneRequestParam param) {
		logger.info("添加场景，请求参数。。。{}"+param);
		SceneInfo config = baseBiz.addScene(param);
		if (config == null) {
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_EXIST.getCode(), ResponseCode.FUNCTION_EXIST.getMessage());
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public Map<String, Object> delete(@Valid @RequestBody SceneRequestParam param) {
		logger.info("删除场景，请求参数。。。{}"+param);
		SceneInfo config = baseBiz.deleteScene(param);
		if (config != null) {
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
		
	}

}