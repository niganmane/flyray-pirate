package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PersonalBaseTracking;
import tk.mybatis.mapper.common.Mapper;

/**
 * 个人客户跟踪表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-05 10:18:49
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalBaseTrackingMapper extends Mapper<PersonalBaseTracking> {
	
}
