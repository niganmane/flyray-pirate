package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.MerchantBaseExt;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantBaseExtMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 商户基础信息扩展表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:49
 */
@Service
public class MerchantBaseExtBiz extends BaseBiz<MerchantBaseExtMapper,MerchantBaseExt> {
}