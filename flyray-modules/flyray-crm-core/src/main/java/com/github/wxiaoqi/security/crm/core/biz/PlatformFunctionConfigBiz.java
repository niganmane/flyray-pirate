package com.github.wxiaoqi.security.crm.core.biz;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.PlatformFunctionConfigRequestParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.crm.core.entity.PlatformFunctionConfig;
import com.github.wxiaoqi.security.crm.core.mapper.PlatformFunctionConfigMapper;
import com.github.wxiaoqi.security.crm.core.rest.PlatformFunctionConfigController;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 平台功能配置
 *
 * @author chj
 * @email ${email}
 * @date 2018-09-04 11:19:24
 */
@Service
public class PlatformFunctionConfigBiz extends BaseBiz<PlatformFunctionConfigMapper,PlatformFunctionConfig> {
	
	
	
	private static final Logger logger = LoggerFactory.getLogger(PlatformFunctionConfigController.class);
	/**
	 * 功能列表
	 * @param params
	 * @return
	 */
	public TableResultResponse<PlatformFunctionConfig> queryList(Map<String, Object> params) {
		Example example = new Example(PlatformFunctionConfig.class);
		example.setOrderByClause("create_time desc");
		Criteria criteria = example.createCriteria();
		Object platformId = params.get("platformId");
		if(!"".equals(platformId) && null != platformId){
			//是平台管理员
			criteria.andEqualTo("platformId", platformId);
		}
		Page<PlatformFunctionConfig> result = PageHelper.startPage(Integer.valueOf((String) params.get("page")), Integer.valueOf((String) params.get("limit")));
		List<PlatformFunctionConfig> list = mapper.selectByExample(example);
		return new TableResultResponse<PlatformFunctionConfig>(result.getTotal(), list);
	}
	
	
	public PlatformFunctionConfig addFunction(PlatformFunctionConfigRequestParam param) {
		logger.info("【添加功能】。。。。请求参数{}"+param);
		PlatformFunctionConfig func = new PlatformFunctionConfig();
		PlatformFunctionConfig func1 = null;
		try {
			BeanUtils.copyProperties(func, param);
			func1 = mapper.selectOne(func);
			if (func1 == null) {
				func.setCreateTime(new Date());
				func.setPCode(param.getPpCode());
				mapper.insert(func);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return func1;
	}
	
	
	public void deleteFunction(PlatformFunctionConfigRequestParam param) {
		logger.info("【刪除功能】。。。。请求参数{}"+param);
		PlatformFunctionConfig func = new PlatformFunctionConfig();
		try {
			BeanUtils.copyProperties(func, param);
			func.setId(Long.valueOf(param.getId()));
			mapper.delete(func);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateFunction(PlatformFunctionConfigRequestParam param) {
		logger.info("【修改功能】。。。。请求参数{}"+param);
		PlatformFunctionConfig func = new PlatformFunctionConfig();
		try {
			BeanUtils.copyProperties(func, param);
			func.setId(Long.valueOf(param.getId()));
			mapper.updateByPrimaryKeySelective(func);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}