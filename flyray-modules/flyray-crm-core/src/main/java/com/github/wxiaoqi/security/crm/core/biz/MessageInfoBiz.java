package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.MessageInfo;
import com.github.wxiaoqi.security.crm.core.mapper.MessageInfoMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 消息表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:49
 */
@Service
public class MessageInfoBiz extends BaseBiz<MessageInfoMapper,MessageInfo> {
}