package com.github.wxiaoqi.security.crm.core.blockchain.transaction;

import com.github.wxiaoqi.security.crm.core.blockchain.util.SerializeUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * 交易
 */
@Data
@Slf4j
public class DposTransaction {

    private static final int SUBSIDY = 10;


    /**
     * 交易的Hash
     */
    private byte[] txId;
    /**
     * 交易输入
     */
    private DposTXInput[] inputs;
    /**
     * 交易输出
     */
    private DposTXOutput[] outputs;
    /**
     * 创建日期
     */
    private long createTime;
    /**
     * 交易签名
     */
    private String signature;
    /**
     * 出账方公钥，使用keyUtil生成
     */
    private String publicKey;
    /**
     * 所属区块hash
     */
    private String blockHash;
    /**
     * 交易类型
     */
    private String txType;


    public DposTransaction(byte[] txId, DposTXInput[] inputs, DposTXOutput[] outputs, long createTime, String signature, String publicKey, String blockHash, String type) {
        this.txId = txId;
        this.inputs = inputs;
        this.outputs = outputs;
        this.createTime = createTime;
        this.signature = signature;
        this.publicKey = publicKey;
        this.blockHash = blockHash;
        this.txType = type;
    }

    public DposTransaction() {
    }

    /**
     * 计算交易信息的Hash值
     *
     * @return
     */
    public byte[] hash() {
        // 使用序列化的方式对Transaction对象进行深度复制
        byte[] serializeBytes = SerializeUtils.serialize(this);
        DposTransaction copyTx = (DposTransaction) SerializeUtils.deserialize(serializeBytes);
        copyTx.setTxId(new byte[]{});
        copyTx.setBlockHash("");
        return DigestUtils.sha256(SerializeUtils.serialize(copyTx));
    }

    public static int getSUBSIDY() {
        return SUBSIDY;
    }

    public byte[] getTxId() {
        return txId;
    }

    public void setTxId(byte[] txId) {
        this.txId = txId;
    }

    public DposTXInput[] getInputs() {
        return inputs;
    }

    public void setInputs(DposTXInput[] inputs) {
        this.inputs = inputs;
    }

    public DposTXOutput[] getOutputs() {
        return outputs;
    }

    public void setOutputs(DposTXOutput[] outputs) {
        this.outputs = outputs;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getBlockHash() {
        return blockHash;
    }

    public void setBlockHash(String blockHash) {
        this.blockHash = blockHash;
    }

    public String getTxType() {
        return txType;
    }

    public void setTxType(String type) {
        this.txType = type;
    }
}
