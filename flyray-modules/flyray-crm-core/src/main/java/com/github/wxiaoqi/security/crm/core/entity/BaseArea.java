package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.github.wxiaoqi.security.common.util.CommonUtils;


/**
 * 行政区划
 * 
 * @author chj
 * @email ${email}
 * @date 2018-09-19 16:28:11
 */
@Table(name = "base_area")
public class BaseArea implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //区域id
    @Id
    private Long areaId;
	
	    //行政区划代码
    @Column(name = "area_code")
    private String areaCode;
	
	    //父级id
    @Column(name = "parent_code")
    private String parentCode;
	
	    //地区名称
    @Column(name = "name")
    private String name;
	
	    //层级
    @Column(name = "layer")
    private Integer layer;
	
	    //排序号,1:省级,2:地市,3:区县
    @Column(name = "order_num")
    private Integer orderNum;
	
	    //显示,1:显示,0:隐藏
    @Column(name = "status")
    private Integer status;
	
	    //备注
    @Column(name = "remark")
    private String remark;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //修改时间
    @Column(name = "update_time")
    private Date updateTime;
    
    //父级区域名称
    @Transient
    private String parentName;
    
    //ztree
    @Transient
    private Boolean open;
	
    @Transient
	private Boolean isParent;
	
    @Transient
	private Integer size;
	
    @Transient
	private List<?> list;
	
	public BaseArea() {
		super();
	}
	/**
	 * 设置：区域id
	 */
	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}
	/**
	 * 获取：区域id
	 */
	public Long getAreaId() {
		return areaId;
	}
	/**
	 * 设置：行政区划代码
	 */
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	/**
	 * 获取：行政区划代码
	 */
	public String getAreaCode() {
		return areaCode;
	}
	/**
	 * 设置：父级id
	 */
	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}
	/**
	 * 获取：父级id
	 */
	public String getParentCode() {
		return parentCode;
	}
	/**
	 * 设置：地区名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 获取：地区名称
	 */
	public String getName() {
		return name;
	}
	/**
	 * 设置：层级
	 */
	public void setLayer(Integer layer) {
		this.layer = layer;
	}
	/**
	 * 获取：层级
	 */
	public Integer getLayer() {
		return layer;
	}
	/**
	 * 设置：排序号,1:省级,2:地市,3:区县
	 */
	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	/**
	 * 获取：排序号,1:省级,2:地市,3:区县
	 */
	public Integer getOrderNum() {
		return orderNum;
	}
	/**
	 * 设置：显示,1:显示,0:隐藏
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：显示,1:显示,0:隐藏
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：修改时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：修改时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	public Boolean getOpen() {
		return open;
	}
	public void setOpen(Boolean open) {
		this.open = open;
	}
	public Boolean getIsParent() {
		return isParent;
	}
	public void setIsParent(Boolean isParent) {
		this.isParent = isParent;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public List<?> getList() {
		return list;
	}
	public void setList(List<?> list) {
		this.list = list;
	}
	
	public void checkParent() {
		if(CommonUtils.isIntThanZero(this.size)) {
			this.isParent = true;
		} else {
			this.isParent = false;
		}
	}
	
	public void checkParentName() {
		if(this.parentCode.equals("0")) {
			this.parentName = "省级区域";
		}
	}
	@Override
	public String toString() {
		return "BaseArea [areaId=" + areaId + ", areaCode=" + areaCode + ", parentCode=" + parentCode + ", name=" + name
				+ ", layer=" + layer + ", orderNum=" + orderNum + ", status=" + status + ", remark=" + remark
				+ ", createTime=" + createTime + ", updateTime=" + updateTime + ", parentName=" + parentName + ", open="
				+ open + ", isParent=" + isParent + ", size=" + size + ", list=" + list + "]";
	}
	
}
