package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 场景
 * 
 * @author chj
 * @email ${email}
 * @date 2018-09-07 10:55:04
 */
@Table(name = "scene_info")
public class SceneInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Long id;
	
	    //场景编号
    @Column(name = "scene_code")
    private String sceneCode;
	
	    //场景名称
    @Column(name = "scene_name")
    private String sceneName;
	

	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：场景编号
	 */
	public void setSceneCode(String sceneCode) {
		this.sceneCode = sceneCode;
	}
	/**
	 * 获取：场景编号
	 */
	public String getSceneCode() {
		return sceneCode;
	}
	/**
	 * 设置：场景名称
	 */
	public void setSceneName(String sceneName) {
		this.sceneName = sceneName;
	}
	/**
	 * 获取：场景名称
	 */
	public String getSceneName() {
		return sceneName;
	}
}
