package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PersonalAccountJournal;
import tk.mybatis.mapper.common.Mapper;

/**
 * 个人账户流水（充、转、提、退、冻结流水）
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalAccountJournalMapper extends Mapper<PersonalAccountJournal> {
	
}
