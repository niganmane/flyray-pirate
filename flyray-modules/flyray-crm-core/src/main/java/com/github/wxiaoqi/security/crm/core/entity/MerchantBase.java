package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 商户客户基础信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "merchant_base")
public class MerchantBase extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//序号
    @Id
    private Integer id;
    
	    //商户编号
    @Column(name = "merchant_id")
    private String merchantId;
	

    //商户名称
	@Column(name = "merchant_name")
	private String merchantName;

	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //用户编号
    @Column(name = "customer_id")
    private String customerId;
	
	    //第三方商户编号
    @Column(name = "third_no")
    private String thirdNo;
	
	    //父级商户编号
    @Column(name = "parent_mer_id")
    private String parentMerId;
	
	    //商户类型  00：普通商户  01：子商户 
    @Column(name = "merchant_level")
    private String merchantLevel;
	
	    //企业名称
    @Column(name = "company_name")
    private String companyName;
	
	    //经营范围
    @Column(name = "business_scope")
    private String businessScope;
	
	    //工商注册号
    @Column(name = "business_no")
    private String businessNo;
	
	    //法人姓名
    @Column(name = "legal_person_name")
    private String legalPersonName;
	
	    //法人证件类型  0-身份证；1-户口本；2-护照；3-军官证；4-士兵证；5-港澳居民往来内地通行证；6-台湾通报往来内地通行证；7-临时身份证；8-外国人居留证；9-警官证；x-其他证件 
    @Column(name = "legal_person_cred_type")
    private String legalPersonCredType;
	
	    //法人证件号码
    @Column(name = "legal_person_cred_no")
    private String legalPersonCredNo;
	
	    //营业执照
    @Column(name = "business_licence")
    private String businessLicence;
	
	    //联系电话
    @Column(name = "phone")
    private String phone;
	
	    //企业座机
    @Column(name = "mobile")
    private String mobile;
	
	    //企业传真
    @Column(name = "fax")
    private String fax;
	
	    //企业网址
    @Column(name = "http_address")
    private String httpAddress;
	
	    //注册资金
    @Column(name = "registered_capital")
    private BigDecimal registeredCapital;
	
	    //企业地址
    @Column(name = "company_address")
    private String companyAddress;
	
	    //认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
    @Column(name = "authentication_status")
    private String authenticationStatus;
	
	    //账户状态 00：正常，01：冻结
    @Column(name = "status")
    private String status;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	
    // 归属人（存后台管理系统登录人员id）指谁发展的客户
    @Column(name = "owner")
    private Long owner;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getMerchantName() {
		return merchantName;
	}
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户编号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：用户编号
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：用户编号
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * 设置：第三方商户编号
	 */
	public void setThirdNo(String thirdNo) {
		this.thirdNo = thirdNo;
	}
	/**
	 * 获取：第三方商户编号
	 */
	public String getThirdNo() {
		return thirdNo;
	}
	/**
	 * 设置：父级商户编号
	 */
	public void setParentMerId(String parentMerId) {
		this.parentMerId = parentMerId;
	}
	/**
	 * 获取：父级商户编号
	 */
	public String getParentMerId() {
		return parentMerId;
	}
	/**
	 * 设置：商户类型  00：普通商户  01：子商户 
	 */
	public String getMerchantLevel() {
		return merchantLevel;
	}
	public void setMerchantLevel(String merchantLevel) {
		this.merchantLevel = merchantLevel;
	}
	/**
	 * 设置：企业名称
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	/**
	 * 获取：企业名称
	 */
	public String getCompanyName() {
		return companyName;
	}
	/**
	 * 设置：经营范围
	 */
	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}
	/**
	 * 获取：经营范围
	 */
	public String getBusinessScope() {
		return businessScope;
	}
	/**
	 * 设置：工商注册号
	 */
	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}
	/**
	 * 获取：工商注册号
	 */
	public String getBusinessNo() {
		return businessNo;
	}
	/**
	 * 设置：法人姓名
	 */
	public void setLegalPersonName(String legalPersonName) {
		this.legalPersonName = legalPersonName;
	}
	/**
	 * 获取：法人姓名
	 */
	public String getLegalPersonName() {
		return legalPersonName;
	}
	/**
	 * 设置：法人证件类型  0-身份证；1-户口本；2-护照；3-军官证；4-士兵证；5-港澳居民往来内地通行证；6-台湾通报往来内地通行证；7-临时身份证；8-外国人居留证；9-警官证；x-其他证件 
	 */
	public void setLegalPersonCredType(String legalPersonCredType) {
		this.legalPersonCredType = legalPersonCredType;
	}
	/**
	 * 获取：法人证件类型  0-身份证；1-户口本；2-护照；3-军官证；4-士兵证；5-港澳居民往来内地通行证；6-台湾通报往来内地通行证；7-临时身份证；8-外国人居留证；9-警官证；x-其他证件 
	 */
	public String getLegalPersonCredType() {
		return legalPersonCredType;
	}
	/**
	 * 设置：法人证件号码
	 */
	public void setLegalPersonCredNo(String legalPersonCredNo) {
		this.legalPersonCredNo = legalPersonCredNo;
	}
	/**
	 * 获取：法人证件号码
	 */
	public String getLegalPersonCredNo() {
		return legalPersonCredNo;
	}
	/**
	 * 设置：营业执照
	 */
	public void setBusinessLicence(String businessLicence) {
		this.businessLicence = businessLicence;
	}
	/**
	 * 获取：营业执照
	 */
	public String getBusinessLicence() {
		return businessLicence;
	}
	/**
	 * 设置：联系电话
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * 获取：联系电话
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * 设置：企业座机
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	/**
	 * 获取：企业座机
	 */
	public String getMobile() {
		return mobile;
	}
	/**
	 * 设置：企业传真
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	/**
	 * 获取：企业传真
	 */
	public String getFax() {
		return fax;
	}
	/**
	 * 设置：企业网址
	 */
	public void setHttpAddress(String httpAddress) {
		this.httpAddress = httpAddress;
	}
	/**
	 * 获取：企业网址
	 */
	public String getHttpAddress() {
		return httpAddress;
	}
	/**
	 * 设置：注册资金
	 */
	public void setRegisteredCapital(BigDecimal registeredCapital) {
		this.registeredCapital = registeredCapital;
	}
	/**
	 * 获取：注册资金
	 */
	public BigDecimal getRegisteredCapital() {
		return registeredCapital;
	}
	/**
	 * 设置：企业地址
	 */
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	/**
	 * 获取：企业地址
	 */
	public String getCompanyAddress() {
		return companyAddress;
	}
	/**
	 * 设置：认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
	 */
	public void setAuthenticationStatus(String authenticationStatus) {
		this.authenticationStatus = authenticationStatus;
	}
	/**
	 * 获取：认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
	 */
	public String getAuthenticationStatus() {
		return authenticationStatus;
	}
	/**
	 * 设置：账户状态 00：正常，01：冻结
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：账户状态 00：正常，01：冻结
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	public Long getOwner() {
		return owner;
	}
	public void setOwner(Long owner) {
		this.owner = owner;
	}
}
