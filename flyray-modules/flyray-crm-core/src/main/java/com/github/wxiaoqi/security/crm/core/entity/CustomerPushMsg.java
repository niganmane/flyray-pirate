package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 客户消息推送参数表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:49
 */
@Table(name = "customer_push_msg")
public class CustomerPushMsg implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //个人或商户编号（客户类型为平台时字段为空）
    @Column(name = "customer_no")
    private String customerNo;
	
	    //客户类型    CUST00：平台   CUST01：商户  CUST02：用户
    @Column(name = "trade_type")
    private String tradeType;
	
	    //消息推送tag
    @Column(name = "push_tag")
    private String pushTag;
	
	    //消息推送alias
    @Column(name = "push_alias")
    private String pushAlias;
	
	    //消息推送RegistrationID
    @Column(name = "push_registration_id")
    private String pushRegistrationId;
	

	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：个人或商户编号（客户类型为平台时字段为空）
	 */
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	/**
	 * 获取：个人或商户编号（客户类型为平台时字段为空）
	 */
	public String getCustomerNo() {
		return customerNo;
	}
	/**
	 * 设置：客户类型    CUST00：平台   CUST01：商户  CUST02：用户
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	/**
	 * 获取：客户类型    CUST00：平台   CUST01：商户  CUST02：用户
	 */
	public String getTradeType() {
		return tradeType;
	}
	/**
	 * 设置：消息推送tag
	 */
	public void setPushTag(String pushTag) {
		this.pushTag = pushTag;
	}
	/**
	 * 获取：消息推送tag
	 */
	public String getPushTag() {
		return pushTag;
	}
	/**
	 * 设置：消息推送alias
	 */
	public void setPushAlias(String pushAlias) {
		this.pushAlias = pushAlias;
	}
	/**
	 * 获取：消息推送alias
	 */
	public String getPushAlias() {
		return pushAlias;
	}
	/**
	 * 设置：消息推送RegistrationID
	 */
	public void setPushRegistrationId(String pushRegistrationId) {
		this.pushRegistrationId = pushRegistrationId;
	}
	/**
	 * 获取：消息推送RegistrationID
	 */
	public String getPushRegistrationId() {
		return pushRegistrationId;
	}
}
