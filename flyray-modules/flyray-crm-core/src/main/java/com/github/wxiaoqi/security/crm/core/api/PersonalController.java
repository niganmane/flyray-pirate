package com.github.wxiaoqi.security.crm.core.api;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.cms.request.MiniProgramParam;
import com.github.wxiaoqi.security.common.crm.request.PersonalInfoRequest;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="个人客户基础信息管理")
@Controller
@RequestMapping("personalBase")
public class PersonalController {
	
	@Autowired
	private PersonalBaseBiz personalBaseBiz;
	
	/**
	 * 小程序用户新增
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("小程序用户新增")
	@RequestMapping(value = "/wechatMiniProgramAdd",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> wechatMiniProgramAdd(@RequestBody @Valid MiniProgramParam miniProgramParam) throws Exception {
		Map<String, Object> response = personalBaseBiz.wechatMiniProgramAdd(EntityUtils.beanToMap(miniProgramParam));
		return response;
    }
	
	/**
	 * 小程序用户修改
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("小程序用户修改")
	@RequestMapping(value = "/wechatMiniProgramUpdate",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> wechatMiniProgramUpdate(@RequestBody @Valid MiniProgramParam miniProgramParam) throws Exception {
		Map<String, Object> response = personalBaseBiz.wechatMiniProgramUpdate(EntityUtils.beanToMap(miniProgramParam));
		return response;
	}
	
	/**
	 * 市民卡用户信息修改
	 * @author centerroot
	 * @time 创建时间:2018年9月21日下午5:26:41
	 * @param personalInfoRequest
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> memberModify(@RequestBody @Valid PersonalInfoRequest personalInfoRequest) throws Exception {
		return personalBaseBiz.modify(personalInfoRequest);
	}
	
	/***
	 * 市民卡用户信息查询
	 * @author centerroot
	 * @time 创建时间:2018年9月21日下午5:52:25
	 * @param personalInfoRequest
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/queryInfo", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryInfo(@RequestBody @Valid PersonalInfoRequest personalInfoRequest) throws Exception {
		return personalBaseBiz.queryInfo(personalInfoRequest);
	}
	
	/**
	 * 市民卡用户信息列表查询
	 * @author centerroot
	 * @time 创建时间:2018年9月21日下午5:58:21
	 * @param personalInfoRequest
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/queryInfoList", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryInfoList(@RequestBody @Valid PersonalInfoRequest personalInfoRequest) throws Exception {
		return personalBaseBiz.queryInfoList(personalInfoRequest);
	}

}
