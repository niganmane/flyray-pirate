package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 平台功能配置
 * 
 * @author chj
 * @email ${email}
 * @date 2018-09-06 10:47:17
 */
@Table(name = "platform_function_config")
public class PlatformFunctionConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Long id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //功能代码
    @Column(name = "code")
    private String code;
	
	    //01 app 02小程序 03PC
    @Column(name = "channel")
    private String channel;
	
	    //级别
    @Column(name = "level")
    private String level;
	
	    //是否展示 0开 1关
    @Column(name = "isShow")
    private String isshow;
	
	    //父级菜单
    @Column(name = "p_code")
    private String pCode;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	

	/**
	 * 设置：序号
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：功能代码
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * 获取：功能代码
	 */
	public String getCode() {
		return code;
	}
	/**
	 * 设置：01 app 02小程序 03PC
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}
	/**
	 * 获取：01 app 02小程序 03PC
	 */
	public String getChannel() {
		return channel;
	}
	/**
	 * 设置：级别
	 */
	public void setLevel(String level) {
		this.level = level;
	}
	/**
	 * 获取：级别
	 */
	public String getLevel() {
		return level;
	}
	/**
	 * 设置：是否展示 0开 1关
	 */
	public void setIsshow(String isshow) {
		this.isshow = isshow;
	}
	/**
	 * 获取：是否展示 0开 1关
	 */
	public String getIsshow() {
		return isshow;
	}
	/**
	 * 设置：父级菜单
	 */
	public void setPCode(String pCode) {
		this.pCode = pCode;
	}
	/**
	 * 获取：父级菜单
	 */
	public String getPCode() {
		return pCode;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
