package com.github.wxiaoqi.security.crm.core.rest;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.crm.request.MerchantBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryMerchantBaseListRequest;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.MerchantBaseBiz;
import com.github.wxiaoqi.security.crm.core.entity.MerchantBase;

/**
 * 商户客户基础信息
 * @author centerroot
 * @time 创建时间:2018年7月16日下午6:09:38
 * @description
 */
@RestController
@RequestMapping("merchantBase")
public class MerchantBaseController extends BaseController<MerchantBaseBiz,MerchantBase> {
	@Autowired
	private MerchantBaseBiz merchantBaseBiz;
	
	/**
	 * 查询个人基础信息列表
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:04
	 * @param queryMerchantBaseListRequest
	 * @return
	 */
	@RequestMapping(value = "/queryList", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryList(@RequestBody @Valid QueryMerchantBaseListRequest queryMerchantBaseListRequest){
		queryMerchantBaseListRequest.setPlatformId(setPlatformId(queryMerchantBaseListRequest.getPlatformId()));
		return merchantBaseBiz.queryList(queryMerchantBaseListRequest);
	}
	
	/**
	 * 添加商户基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:15
	 * @param MerchantBaseRequest
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> add(@RequestBody @Valid MerchantBaseRequest MerchantBaseRequest){
		return merchantBaseBiz.add(MerchantBaseRequest);
	}
	/**
	 * 查询单个客户基本信息
	 */
	@RequestMapping(value = "/info/{merchantId}", method = RequestMethod.GET)
    @ResponseBody
	public MerchantBase queryInfo(@PathVariable String merchantId){
		return merchantBaseBiz.queryInfo(merchantId);
	}
	
	/**
	 * 根据序号删除客户基本信息
	 * @author centerroot
	 * @time 创建时间:2018年8月24日下午2:45:07
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/deleteOne/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> deleteOne(@PathVariable Integer id){
		Map<String, Object> respMap = baseBiz.deleteOne(id);
        return respMap;
    }
	
}