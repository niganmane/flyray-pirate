package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 平台所支持账户配置
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "platform_accout_config")
public class PlatformAccoutConfig extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //客户类型    CUST00：平台   CUST01：商户  CUST02：用户
    @Column(name = "customer_type")
    private String customerType;
	
	    //账户类型  ACC001：余额账户，
    @Column(name = "account_type")
    private String accountType;
	
    //账户别名
    @Column(name = "account_alias")
    private String accountAlias;

	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：客户类型    CUST00：平台   CUST01：商户  CUST02：用户
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	/**
	 * 获取：客户类型    CUST00：平台   CUST01：商户  CUST02：用户
	 */
	public String getCustomerType() {
		return customerType;
	}
	/**
	 * 设置：账户类型  ACC001：余额账户，
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	/**
	 * 获取：账户类型  ACC001：余额账户，
	 */
	public String getAccountType() {
		return accountType;
	}
	public String getAccountAlias() {
		return accountAlias;
	}
	public void setAccountAlias(String accountAlias) {
		this.accountAlias = accountAlias;
	}
	
}
