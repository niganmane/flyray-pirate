package com.github.wxiaoqi.security.crm.core.biz;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.PlatformFunctionRequestParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.crm.core.entity.PlatformFunction;
import com.github.wxiaoqi.security.crm.core.mapper.PlatformFunctionMapper;

import tk.mybatis.mapper.entity.Example;

/**
 * 平台功能
 *
 * @author chj
 * @email ${email}
 * @date 2018-09-04 11:18:41
 */
@Service
public class PlatformFunctionBiz extends BaseBiz<PlatformFunctionMapper,PlatformFunction> {
	
	private static final Logger logger = LoggerFactory.getLogger(PlatformFunctionBiz.class);
	
	/**
	 * 功能列表
	 * @param params
	 * @return
	 */
	public TableResultResponse<PlatformFunction> queryList(Map<String, Object> params) {
		Example example = new Example(PlatformFunction.class);
		example.setOrderByClause("create_time desc");
		Page<PlatformFunction> result = PageHelper.startPage(Integer.valueOf((String) params.get("page")), Integer.valueOf((String) params.get("limit")));
		List<PlatformFunction> list = mapper.selectByExample(example);
		return new TableResultResponse<PlatformFunction>(result.getTotal(), list);
	}
	
	public Map<String, Object> queryWithLevel(Map<String, Object> params) {
		Map<String, Object> respMap = new HashMap<>();
		List<PlatformFunction> list1 = mapper.selectAll();
		List<PlatformFunction> list2 = mapper.queryWithLevel(params);
		respMap.put("allList", list1);
		respMap.put("list", list2);
		return respMap;
	}
	
	
	public List<Map<String, Object>> queryFunctionMenu(Map<String, Object> map) {
		logger.info("查询菜单列表，请求参数。。。{}"+map);
		map.put("level", "1");
		List<Map<String, Object>> list = mapper.queryLevelOne(map);
		logger.info("1级菜单。。。{}"+list);
		if (list != null && list.size() > 0) {
			for (Map<String, Object> map2 : list) {
				map.put("level", "2");
				map.put("isshow", "0");
				map.put("ppCode", map2.get("code"));
				List<Map<String, Object>> list1 = mapper.queryLevelTwo(map);
				map2.put((String) map2.get("code"), list1);
			}
		}
		logger.info("所有菜单。。。{}"+list);
		return list;
	}
	
	
	public PlatformFunction addFunction(PlatformFunctionRequestParam param) {
		logger.info("【添加功能】。。。。请求参数{}"+param);
		PlatformFunction func = new PlatformFunction();
		PlatformFunction func1 = null;
		try {
			BeanUtils.copyProperties(func, param);
			func1 = mapper.selectOne(func);
			if (func1 == null) {
				func.setCreateTime(new Date());
				mapper.insert(func);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return func1;
	}
	
	
	public void updateFunction(PlatformFunctionRequestParam param) {
		logger.info("【修改功能】。。。。请求参数{}"+param);
		PlatformFunction func = new PlatformFunction();
		try {
			BeanUtils.copyProperties(func, param);
			func.setId(Long.valueOf(param.getId()));
			mapper.updateByPrimaryKeySelective(func);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void deleteFunction(PlatformFunctionRequestParam param) {
		logger.info("【刪除功能】。。。。请求参数{}"+param);
		PlatformFunction func = new PlatformFunction();
		try {
			BeanUtils.copyProperties(func, param);
			func.setId(Long.valueOf(param.getId()));
			mapper.delete(func);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}