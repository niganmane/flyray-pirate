package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.MessageInfoBiz;
import com.github.wxiaoqi.security.crm.core.entity.MessageInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("messageInfo")
public class MessageInfoController extends BaseController<MessageInfoBiz,MessageInfo> {

}