package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 平台基础信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:48
 */
@Table(name = "platform_base")
public class PlatformBase extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //平台名称
    @Column(name = "platform_name")
    private String platformName;
	
	    //平台简介
    @Column(name = "platform_introduction")
    private String platformIntroduction;
	
	    //认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
    @Column(name = "authentication_status")
    private String authenticationStatus;
	
	    //最后操作人编号
    @Column(name = "operator_id")
    private Integer operatorId;
	
	    //最后操作人名称
    @Column(name = "operator_name")
    private String operatorName;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
    
    //平台级别  01:一级平台（顶级） 02：二级平台（默认）
    @Column(name = "platform_level")
    private String platformLevel;
    
    //平台登录名称
    @Column(name = "platform_login_name")
    private String platformLoginName;
	
    //平台logo
    @Column(name = "platform_logo")
    private byte[] platformLogo;
    
    @Transient
    private String platformLogoStr;
    
    @Transient
    private String token;
    
    public byte[] getPlatformLogo() {
		return platformLogo;
	}
	public void setPlatformLogo(byte[] platformLogo) {
		this.platformLogoStr = platformLogo.toString();
		this.platformLogo = platformLogo;
	}
	public String getPlatformLogoStr() {
		return platformLogoStr;
	}
	public void setPlatformLogoStr(String platformLogoStr) {
		this.platformLogo = platformLogoStr.getBytes();
		this.platformLogoStr = platformLogoStr;
	}
    
	public String getPlatformLoginName() {
		return platformLoginName;
	}
	public void setPlatformLoginName(String platformLoginName) {
		this.platformLoginName = platformLoginName;
	}
	public String getPlatformLevel() {
		return platformLevel;
	}
	public void setPlatformLevel(String platformLevel) {
		this.platformLevel = platformLevel;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：平台名称
	 */
	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}
	/**
	 * 获取：平台名称
	 */
	public String getPlatformName() {
		return platformName;
	}
	/**
	 * 设置：平台简介
	 */
	public void setPlatformIntroduction(String platformIntroduction) {
		this.platformIntroduction = platformIntroduction;
	}
	/**
	 * 获取：平台简介
	 */
	public String getPlatformIntroduction() {
		return platformIntroduction;
	}
	/**
	 * 设置：认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
	 */
	public void setAuthenticationStatus(String authenticationStatus) {
		this.authenticationStatus = authenticationStatus;
	}
	/**
	 * 获取：认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
	 */
	public String getAuthenticationStatus() {
		return authenticationStatus;
	}
	/**
	 * 设置：最后操作人编号
	 */
	public void setOperatorId(Integer operatorId) {
		this.operatorId = operatorId;
	}
	/**
	 * 获取：最后操作人编号
	 */
	public Integer getOperatorId() {
		return operatorId;
	}
	/**
	 * 设置：最后操作人名称
	 */
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	/**
	 * 获取：最后操作人名称
	 */
	public String getOperatorName() {
		return operatorName;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
