package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.CustomerPushMsgBiz;
import com.github.wxiaoqi.security.crm.core.entity.CustomerPushMsg;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("customerPushMsg")
public class CustomerPushMsgController extends BaseController<CustomerPushMsgBiz,CustomerPushMsg> {

}