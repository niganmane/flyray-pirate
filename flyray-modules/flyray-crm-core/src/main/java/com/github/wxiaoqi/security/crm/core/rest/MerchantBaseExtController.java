package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.MerchantBaseExtBiz;
import com.github.wxiaoqi.security.crm.core.entity.MerchantBaseExt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("merchantBaseExt")
public class MerchantBaseExtController extends BaseController<MerchantBaseExtBiz,MerchantBaseExt> {

}