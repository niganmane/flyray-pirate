package com.github.wxiaoqi.security.crm.core.client;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.wxiaoqi.security.common.admin.pay.response.QueryBankCodeResponse;

@FeignClient(value = "flyray-pay-core")
public interface BankCodeBizService {

	@RequestMapping(value = "rest/payForAnother/queryBankCode",method = RequestMethod.POST)
	public QueryBankCodeResponse queryBankCode();
	
	/**
	 * 代付申请
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/payForAnother/payForAnotherApply",method = RequestMethod.POST)
	public Map<String, Object> payForAnotherApply(@RequestBody Map<String, Object> params);
	
	/**
	 * 代付
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/payForAnother/payForAnother",method = RequestMethod.POST)
	public Map<String, Object> payForAnother(@RequestBody Map<String, Object> params);
}
