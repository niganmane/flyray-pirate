package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 商户账户流水（充、转、提、退、冻结流水）
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "merchant_account_journal")
public class MerchantAccountJournal extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    @Id
    private String journalId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //商户编号
    @Column(name = "merchant_id")
    private String merchantId;
	
	    //账户编号
    @Column(name = "account_id")
    private String accountId;
	
	    //账户类型    ACC001：余额账户
    @Column(name = "account_type")
    private String accountType;
	
	    //订单号
    @Column(name = "order_no")
    private String orderNo;
	
	    //来往标志  1：来账   2：往账 
    @Column(name = "in_out_flag")
    private String inOutFlag;
	
	    //交易金额
    @Column(name = "trade_amt")
    private BigDecimal tradeAmt;
	
	    //交易类型  支付:01，退款:02，提现:03，充值:04
    @Column(name = "trade_type")
    private String tradeType;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	

	/**
	 * 设置：流水号
	 */
	public void setJournalId(String journalId) {
		this.journalId = journalId;
	}
	/**
	 * 获取：流水号
	 */
	public String getJournalId() {
		return journalId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户编号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：账户编号
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * 获取：账户编号
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * 设置：账户类型    ACC001：余额账户
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	/**
	 * 获取：账户类型    ACC001：余额账户
	 */
	public String getAccountType() {
		return accountType;
	}
	/**
	 * 设置：订单号
	 */
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	/**
	 * 获取：订单号
	 */
	public String getOrderNo() {
		return orderNo;
	}
	/**
	 * 设置：来往标志  1：来账   2：往账 
	 */
	public void setInOutFlag(String inOutFlag) {
		this.inOutFlag = inOutFlag;
	}
	/**
	 * 获取：来往标志  1：来账   2：往账 
	 */
	public String getInOutFlag() {
		return inOutFlag;
	}
	/**
	 * 设置：交易金额
	 */
	public void setTradeAmt(BigDecimal tradeAmt) {
		this.tradeAmt = tradeAmt;
	}
	/**
	 * 获取：交易金额
	 */
	public BigDecimal getTradeAmt() {
		return tradeAmt;
	}
	/**
	 * 设置：交易类型  支付:01，退款:02，提现:03，充值:04
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	/**
	 * 获取：交易类型  支付:01，退款:02，提现:03，充值:04
	 */
	public String getTradeType() {
		return tradeType;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
