package com.github.wxiaoqi.security.crm.core.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.PlatformBaseAddRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryMerchantBaseListRequest;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PlatformBaseBiz;
import com.github.wxiaoqi.security.crm.core.entity.PlatformBase;

@Controller
@RequestMapping("platformBase")
public class PlatformBaseController extends BaseController<PlatformBaseBiz,PlatformBase> {
	
	@Autowired
	private PlatformBaseBiz platformBaseBiz;
	/**
	 * 添加平台
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addPlatform(@RequestBody PlatformBaseAddRequest entity) throws Exception {
		Map<String, Object> result = platformBaseBiz.addPlatform(entity);
        return result;
    }
	/**
	 * 查询类表
	 * @param bean
	 * @return
	 */
	@RequestMapping(value = "/pageList",method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<PlatformBase> pageList(@RequestBody QueryMerchantBaseListRequest bean){
		bean.setPlatformId(setPlatformId(bean.getPlatformId()));
        return baseBiz.pageList(bean);
    }
	
	/**
	 * 根据平台编号查询平台信息
	 * @author centerroot
	 * @time 创建时间:2018年8月16日上午10:13:26
	 * @param platformId
	 * @return
	 */
	@RequestMapping(value = "/queryOne/{platformId}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOneObj(@PathVariable String platformId){
		Map<String, Object> respMap = baseBiz.getOneObj(platformId);
        return respMap;
    }
	
	/**
	 * 根据序号删除平台信息
	 * @author centerroot
	 * @time 创建时间:2018年8月24日下午2:45:07
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/deleteOne/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> deleteOne(@PathVariable Integer id){
		Map<String, Object> respMap = baseBiz.deleteOne(id);
        return respMap;
    }
	
	
	@RequestMapping(value = "/update",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateObj(@RequestBody PlatformBaseAddRequest entity){
		Map<String, Object> respMap = baseBiz.updateObj(entity);
        return respMap;
    }

}