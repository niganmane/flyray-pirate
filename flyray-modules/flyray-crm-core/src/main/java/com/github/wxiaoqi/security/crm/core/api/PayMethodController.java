package com.github.wxiaoqi.security.crm.core.api;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.QueryPayMethodRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.ResponseHelper;
import com.github.wxiaoqi.security.crm.core.biz.ScenePayMethodConfigBiz;

import io.swagger.annotations.Api;


@Api(tags="支付方式查询")
@Controller
@RequestMapping("payMethods")
public class PayMethodController {
	
	@Autowired
	private ScenePayMethodConfigBiz configBiz;
	
	/**
	 * 查询支付方式
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/method", method=RequestMethod.POST)
	public Map<String, Object> queryPayMethod(@RequestBody @Valid QueryPayMethodRequest param) {
		List<Map<String, Object>> list = configBiz.queryPayMethod(param);
		if (list != null) {
			return ResponseHelper.success(list, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.NO_AVAILABLE_PAYCHANNEL.getCode(), ResponseCode.NO_AVAILABLE_PAYCHANNEL.getMessage());
		}
	}

}
