package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 个人客户基础信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "personal_base")
public class PersonalBase extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//序号
    @Id
    private Integer id;
    
	    //个人客户编号
    @Column(name = "personal_id")
    private String personalId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //用户编号
    @Column(name = "customer_id")
    private String customerId;
	
	    //第三方会员编号
    @Column(name = "third_no")
    private String thirdNo;
	
	    //手机号
	@Column(name = "phone")
	private String phone;
	
	    //用户名称
    @Column(name = "real_name")
    private String realName;
	
	    //身份证号
    @Column(name = "id_card")
    private String idCard;
	
	    //用户昵称
    @Column(name = "nick_name")
    private String nickName;
	
	    //性别 1：男 2：女
    @Column(name = "sex")
    private String sex;
	
	    //生日
    @Column(name = "birthday")
    private String birthday;
	

	    //居住地
    @Column(name = "address")
    private String address;
	
	    //身份证正面
    @Column(name = "id_positive")
    private String idPositive;
	
	    //身份证反面
    @Column(name = "id_negative")
    private String idNegative;
	
	    //认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
    @Column(name = "authentication_status")
    private String authenticationStatus;
	
	    //账户状态 00：正常，01：冻结
    @Column(name = "status")
    private String status;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	
    // 归属人（存后台管理系统登录人员id）指谁发展的客户
    @Column(name = "owner")
    private Long owner;

    // 用户头像
    @Column(name = "avatar")
    private String avatar;
    
	    //备注
	@Column(name = "remark")
	private String remark;
	
	    //个人客户类型  00：注册用户  01：运营添加用户
	@Column(name = "personal_type")
	private String personalType;
    
	@Transient
    private String parentPerId;
	
	@Transient
    private String parentLevel;
	
	
	
	public String getParentPerId() {
		return parentPerId;
	}
	public void setParentPerId(String parentPerId) {
		this.parentPerId = parentPerId;
	}
	public String getParentLevel() {
		return parentLevel;
	}
	public void setParentLevel(String parentLevel) {
		this.parentLevel = parentLevel;
	}
	public String getPersonalType() {
		return personalType;
	}
	public void setPersonalType(String personalType) {
		this.personalType = personalType;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 设置：个人客户编号
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 获取：个人客户编号
	 */
	public String getPersonalId() {
		return personalId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：用户编号
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：用户编号
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * 设置：第三方会员编号
	 */
	public void setThirdNo(String thirdNo) {
		this.thirdNo = thirdNo;
	}
	/**
	 * 获取：第三方会员编号
	 */
	public String getThirdNo() {
		return thirdNo;
	}
	/**
	 * 设置：用户名称
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}
	/**
	 * 获取：用户名称
	 */
	public String getRealName() {
		return realName;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	/**
	 * 设置：用户昵称
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	/**
	 * 获取：用户昵称
	 */
	public String getNickName() {
		return nickName;
	}
	/**
	 * 设置：性别 1：男 2：女
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	/**
	 * 获取：性别 1：男 2：女
	 */
	public String getSex() {
		return sex;
	}
	/**
	 * 设置：生日
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	/**
	 * 获取：生日
	 */
	public String getBirthday() {
		return birthday;
	}
	/**
	 * 设置：居住地
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：居住地
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：身份证正面
	 */
	public void setIdPositive(String idPositive) {
		this.idPositive = idPositive;
	}
	/**
	 * 获取：身份证正面
	 */
	public String getIdPositive() {
		return idPositive;
	}
	/**
	 * 设置：身份证反面
	 */
	public void setIdNegative(String idNegative) {
		this.idNegative = idNegative;
	}
	/**
	 * 获取：身份证反面
	 */
	public String getIdNegative() {
		return idNegative;
	}
	/**
	 * 设置：认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
	 */
	public void setAuthenticationStatus(String authenticationStatus) {
		this.authenticationStatus = authenticationStatus;
	}
	/**
	 * 获取：认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
	 */
	public String getAuthenticationStatus() {
		return authenticationStatus;
	}
	/**
	 * 设置：账户状态 00：正常，01：冻结
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：账户状态 00：正常，01：冻结
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	public Long getOwner() {
		return owner;
	}
	public void setOwner(Long owner) {
		this.owner = owner;
	}
}
