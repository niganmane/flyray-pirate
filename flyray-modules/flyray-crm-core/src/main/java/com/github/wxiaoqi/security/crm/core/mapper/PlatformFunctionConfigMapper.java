package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PlatformFunctionConfig;

import tk.mybatis.mapper.common.Mapper;

/**
 * 平台功能配置
 * 
 * @author chj
 * @email ${email}
 * @date 2018-09-04 11:19:24
 */
@org.apache.ibatis.annotations.Mapper
public interface PlatformFunctionConfigMapper extends Mapper<PlatformFunctionConfig> {
	
}
