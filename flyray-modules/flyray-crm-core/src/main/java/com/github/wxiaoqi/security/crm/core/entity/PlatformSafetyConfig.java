package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 平台安全配置信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-11 14:42:03
 */
@Table(name = "platform_safety_config")
public class PlatformSafetyConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //平台编号密文
    @Column(name = "app_id")
    private String appId;
	
	    //密钥（盐值）：在首次登录或者首次获取tiken的时候用到
    @Column(name = "app_key")
    private String appKey;
	
	    //公钥
    @Column(name = "public_key")
    private String publicKey;
	
	    //私钥
    @Column(name = "private_key")
    private String privateKey;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	

	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：平台编号密文
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	/**
	 * 获取：平台编号密文
	 */
	public String getAppId() {
		return appId;
	}
	/**
	 * 设置：密钥（盐值）：在首次登录或者首次获取token的时候用到
	 */
	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}
	/**
	 * 获取：密钥（盐值）：在首次登录或者首次获取token的时候用到
	 */
	public String getAppKey() {
		return appKey;
	}
	/**
	 * 设置：公钥
	 */
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	/**
	 * 获取：公钥
	 */
	public String getPublicKey() {
		return publicKey;
	}
	/**
	 * 设置：私钥
	 */
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	/**
	 * 获取：私钥
	 */
	public String getPrivateKey() {
		return privateKey;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
