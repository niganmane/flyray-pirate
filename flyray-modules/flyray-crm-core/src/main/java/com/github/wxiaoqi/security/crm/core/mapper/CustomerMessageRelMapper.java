package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.CustomerMessageRel;
import tk.mybatis.mapper.common.Mapper;

/**
 * 用户消息关联表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:50
 */
@org.apache.ibatis.annotations.Mapper
public interface CustomerMessageRelMapper extends Mapper<CustomerMessageRel> {
	
}
