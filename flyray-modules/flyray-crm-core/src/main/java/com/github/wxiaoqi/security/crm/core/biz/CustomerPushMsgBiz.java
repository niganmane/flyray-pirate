package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.CustomerPushMsg;
import com.github.wxiaoqi.security.crm.core.mapper.CustomerPushMsgMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 客户消息推送参数表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:49
 */
@Service
public class CustomerPushMsgBiz extends BaseBiz<CustomerPushMsgMapper,CustomerPushMsg> {
}