package com.github.wxiaoqi.security.crm.core.feignserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.AccountJournalQueryRequest;
import com.github.wxiaoqi.security.common.crm.request.AccountQueryRequest;
import com.github.wxiaoqi.security.crm.core.biz.CommonAccountQueryBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 账户查询
 * */
@Api(tags="个人或者商户账户查询")
@Controller
@RequestMapping("feign")
public class FeignCommonAccountQuery {
	
	@Autowired
	private CommonAccountQueryBiz commonAccountQueryBiz;
	
	/**
	 * 充值时调用的接口
	 * @param 
	 * @return withdrawApply
	 */
	@ApiOperation("账户查询")
	@RequestMapping(value = "/accountQuery",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> accountQuery(@RequestBody @Valid AccountQueryRequest accountQueryRequest){
		Map<String, Object> response = commonAccountQueryBiz.accountQuery(accountQueryRequest);
		return response;
    }
	
	/**
	 * 查询账户交易流水
	 * @author centerroot
	 * @time 创建时间:2018年9月27日下午4:10:22
	 * @param accountJournalQueryRequest
	 * @return
	 */
	@ApiOperation("查询账户交易流水")
	@RequestMapping(value = "/accountJournalQuery",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> accountJournalQuery(@RequestBody @Valid AccountJournalQueryRequest accountJournalQueryRequest){
		Map<String, Object> response = commonAccountQueryBiz.accountJournalQuery(accountJournalQueryRequest);
		return response;
    }
	
}
