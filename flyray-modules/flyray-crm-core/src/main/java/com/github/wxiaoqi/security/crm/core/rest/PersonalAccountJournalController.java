package com.github.wxiaoqi.security.crm.core.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PersonalAccountJournalBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalAccountJournal;

@RestController
@RequestMapping("personalAccountJournal")
public class PersonalAccountJournalController extends BaseController<PersonalAccountJournalBiz,PersonalAccountJournal> {

}