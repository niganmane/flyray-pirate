package com.github.wxiaoqi.security.crm.core.blockchain.transaction;

public enum TransactionType {
    Reward("01","奖励"),
    Transfer("02","转账"),
    TxFee("03","手续费");

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private String code,msg;
    TransactionType(String code,String msg) {
        this.code = code;
        this.msg = msg;
    }
}
