package com.github.wxiaoqi.security.crm.core.biz;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.github.wxiaoqi.security.crm.core.entity.PlatformBaseExtend;
import com.github.wxiaoqi.security.crm.core.mapper.PlatformBaseExtendMapper;
import com.github.wxiaoqi.security.auth.common.config.UserAuthConfig;
import com.github.wxiaoqi.security.auth.common.util.jwt.IJWTInfo;
import com.github.wxiaoqi.security.auth.common.util.jwt.JWTHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.PlatformBaseExtendRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryPayChannelConfigRequest;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;

/**
 * 平台扩展信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class PlatformBaseExtendBiz extends BaseBiz<PlatformBaseExtendMapper,PlatformBaseExtend> {
    @Autowired
    private UserAuthConfig userAuthConfig;
    @Autowired
    private PlatformBaseExtendMapper platformBaseExtendMapper;
    
    @Autowired 
	private PayChannelConfigBiz payChannelConfigBiz;
    
	public void addPlatformBaseExtend(PlatformBaseExtend entity) {
		String token = entity.getToken();
		String userNo = "";
		String userName = "";
		try {
			IJWTInfo info = JWTHelper.getInfoFromToken(token, userAuthConfig.getPubKeyByte());
			userNo = info.getXId();
			userName = info.getName();
		} catch (Exception e) {
			e.printStackTrace(); 
		}
		entity.setOperatorId(Integer.valueOf(userNo));
		entity.setOperatorName(userName);
		entity.setCreateTime(new Date());
		platformBaseExtendMapper.insert(entity);
	}
	public Map<String, Object> upload(HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		MultipartHttpServletRequest multipartRequest =     (MultipartHttpServletRequest) request;
	    MultipartFile file = multipartRequest.getFile("file"); // 通过参数名获取指定文件
	    String fileName = file.getOriginalFilename();
	    String rootPath = "d:/home/upload";
        String filePath = rootPath + "/";
        File dir = new File(filePath);
        if (!dir.isDirectory())
            dir.mkdir();
        String newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf("."));
        File writeFile = new File(filePath + newFileName);
        //文件写入磁盘
        try {
			file.transferTo(writeFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
        result.put("name", newFileName);
        return result;
	}
	public Map<String, Object> queryInfo(String platformId){
		Map<String, Object> result = new HashMap<String, Object>();
		ObjectRestResponse<PlatformBaseExtend> entityObjectRestResponse = new ObjectRestResponse<>();
		PlatformBaseExtend paramExtend = new PlatformBaseExtend();
		paramExtend.setPlatformId(platformId);
		PlatformBaseExtend baseExtend =platformBaseExtendMapper.selectOne(paramExtend);
		result.put("extend", baseExtend);
        //安全配置
        
        //平台回调地址配置
        
        //平台通道配置
        QueryPayChannelConfigRequest queryPayChannelConfigRequest = new QueryPayChannelConfigRequest();
        queryPayChannelConfigRequest.setPlatformId(platformId);
        Map<String, Object> channelConfigMap = payChannelConfigBiz.queryList(queryPayChannelConfigRequest);
        result.put("channelConfig", channelConfigMap.get("body"));
        return result;
	}
	public void updatePlatformBase(PlatformBaseExtendRequest entity) {
		PlatformBaseExtend extend = new PlatformBaseExtend();
		try {
			BeanUtils.copyProperties(extend, entity);
			extend.setUpdateTime(new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		platformBaseExtendMapper.updateByPrimaryKeySelective(extend);
	}
}