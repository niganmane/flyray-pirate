package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 商户提现银行卡信息表
 * @author he
 */
@Table(name = "merchant_pay_for_another_bank")
public class MerchantPayForAnotherBank implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	//账户编号
	@Column(name = "platform_id")
	private String platformId;

	//商户编号
	@Column(name = "merchant_id")
	private String merchantId;

	//银行名称
	@Column(name = "bank_name")
	private String bankName;
	
	//银行账户名
	@Column(name = "bank_account_name")
	private String bankAccountName;

	//银行卡号
	@Column(name = "bank_no")
	private String bankNo;


	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：账户编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：账户编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户编号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：银行名称
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/**
	 * 获取：银行名称
	 */
	public String getBankName() {
		return bankName;
	}
	public String getBankAccountName() {
		return bankAccountName;
	}
	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}
	/**
	 * 设置：银行卡号
	 */
	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}
	/**
	 * 获取：银行卡号
	 */
	public String getBankNo() {
		return bankNo;
	}
}
