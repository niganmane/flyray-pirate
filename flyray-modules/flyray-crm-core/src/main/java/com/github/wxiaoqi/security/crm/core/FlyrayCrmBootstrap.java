package com.github.wxiaoqi.security.crm.core;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.github.wxiaoqi.security.auth.client.EnableAceAuthClient;
import com.spring4all.swagger.EnableSwagger2Doc;

/**
 * ${DESCRIPTION}
 *
 * @author bolei
 * @create 2018-4-2 20:30
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"com.github.wxiaoqi.security.auth.client.feign","com.github.wxiaoqi.security.crm.core.client"})
@EnableScheduling
@EnableAceAuthClient
@MapperScan("com.github.wxiaoqi.security.crm.core.mapper")
@EnableTransactionManagement
@EnableSwagger2Doc
public class FlyrayCrmBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(FlyrayCrmBootstrap.class, args);
    }
}
