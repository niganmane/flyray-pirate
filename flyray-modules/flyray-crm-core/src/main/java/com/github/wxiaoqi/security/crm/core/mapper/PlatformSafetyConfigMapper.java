package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PlatformSafetyConfig;
import tk.mybatis.mapper.common.Mapper;

/**
 * 平台安全配置信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-11 14:42:03
 */
public interface PlatformSafetyConfigMapper extends Mapper<PlatformSafetyConfig> {
	
}
