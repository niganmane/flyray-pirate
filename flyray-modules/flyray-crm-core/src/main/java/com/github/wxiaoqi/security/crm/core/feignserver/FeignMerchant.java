package com.github.wxiaoqi.security.crm.core.feignserver;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.MerchantBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.PersonalBaseRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.crm.core.biz.MerchantBaseBiz;
import com.github.wxiaoqi.security.crm.core.entity.MerchantBase;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 商户客户
 * */
@Api(tags="商户客户")
@Controller
@RequestMapping("feign/merchant")
public class FeignMerchant {
	
	@Autowired
	private MerchantBaseBiz merchantBaseBiz;
	
	/**
	 * 商户客户查询
	 * @author centerroot
	 * @time 创建时间:2018年9月26日下午4:31:39
	 * @param merchantBaseRequest
	 * @return
	 */
	@ApiOperation("商户客户查询")
	@RequestMapping(value = "/queryInfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> accountQuery(@RequestBody @Valid MerchantBaseRequest merchantBaseRequest){
		Map<String, Object> respMap = new HashMap<>();
		MerchantBase merchantBaseReq = new MerchantBase();
		BeanUtils.copyProperties(merchantBaseRequest, merchantBaseReq);
		MerchantBase merchantBase = merchantBaseBiz.selectOne(merchantBaseReq);
		if (null == merchantBase) {
			respMap.put("code", ResponseCode.MER_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.MER_NOTEXIST.getMessage());
		}else{
			MerchantBaseRequest merchantBaseEntity = new MerchantBaseRequest();
			BeanUtils.copyProperties(merchantBase, merchantBaseEntity);
			respMap.put("merchantBase", merchantBaseEntity);
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		}
		return respMap;
    }
	
	
}
