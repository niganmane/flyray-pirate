package com.github.wxiaoqi.security.crm.core.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PersonalDistributionRelationBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalDistributionRelation;

@RestController
@RequestMapping("personalDistributionRelation")
public class PersonalDistributionRelationController extends BaseController<PersonalDistributionRelationBiz,PersonalDistributionRelation> {

}