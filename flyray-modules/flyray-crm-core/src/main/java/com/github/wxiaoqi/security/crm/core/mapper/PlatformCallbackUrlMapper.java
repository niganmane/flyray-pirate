package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PlatformCallbackUrl;
import tk.mybatis.mapper.common.Mapper;

/**
 * 平台回调地址配置
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:48
 */
@org.apache.ibatis.annotations.Mapper
public interface PlatformCallbackUrlMapper extends Mapper<PlatformCallbackUrl> {
	
}
