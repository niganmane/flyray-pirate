package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import io.swagger.annotations.ApiModelProperty;


/**
 * 个人基础信息扩展表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-14 14:15:09
 */
@Table(name = "personal_base_ext")
public class PersonalBaseExt implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //个人客户编号
    @Id
    private String personalId;
	
	    //客户等级
    @Column(name = "personal_level")
    private Integer personalLevel;
	
	    //行业
    @Column(name = "industry")
    private String industry;
	
	    //行业描述
    @Column(name = "industry_description")
    private String industryDescription;
	
	    //职务
    @Column(name = "job")
    private String job;
	
	    //所在省市
    @Column(name = "provinces")
    private String provinces;
	
	    //电子邮件
    @Column(name = "email")
    private String email;
	
	    //邮编
    @Column(name = "zip_code")
    private String zipCode;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
    
	    //关注行业
	@Column(name = "attention_industry")
	private String attentionIndustry;
	
	    //关注职位
	@Column(name = "attention_jobs")
	private String attentionJobs;
	
		//标签
	@Column(name = "self_tag")
	private String selfTag;
	
		//设备ID
	@Column(name = "deviceId")
	private String deviceId;
	
		//设备类型
	@Column(name = "deviceType")
	private String deviceType;

	
	
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	/**
	 * 设置：个人客户编号
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 获取：个人客户编号
	 */
	public String getPersonalId() {
		return personalId;
	}
	/**
	 * 设置：客户等级
	 */
	public void setPersonalLevel(Integer personalLevel) {
		this.personalLevel = personalLevel;
	}
	/**
	 * 获取：客户等级
	 */
	public Integer getPersonalLevel() {
		return personalLevel;
	}
	/**
	 * 设置：行业
	 */
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	/**
	 * 获取：行业
	 */
	public String getIndustry() {
		return industry;
	}
	/**
	 * 设置：行业描述
	 */
	public void setIndustryDescription(String industryDescription) {
		this.industryDescription = industryDescription;
	}
	/**
	 * 获取：行业描述
	 */
	public String getIndustryDescription() {
		return industryDescription;
	}
	/**
	 * 设置：职务
	 */
	public void setJob(String job) {
		this.job = job;
	}
	/**
	 * 获取：职务
	 */
	public String getJob() {
		return job;
	}
	/**
	 * 设置：所在省市
	 */
	public void setProvinces(String provinces) {
		this.provinces = provinces;
	}
	/**
	 * 获取：所在省市
	 */
	public String getProvinces() {
		return provinces;
	}
	/**
	 * 设置：电子邮件
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * 获取：电子邮件
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * 设置：邮编
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * 获取：邮编
	 */
	public String getZipCode() {
		return zipCode;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：关注行业
	 */
	public void setAttentionIndustry(String attentionIndustry) {
		this.attentionIndustry = attentionIndustry;
	}
	/**
	 * 获取：关注行业
	 */
	public String getAttentionIndustry() {
		return attentionIndustry;
	}
	/**
	 * 设置：关注职位
	 */
	public void setAttentionJobs(String attentionJobs) {
		this.attentionJobs = attentionJobs;
	}
	/**
	 * 获取：关注职位
	 */
	public String getAttentionJobs() {
		return attentionJobs;
	}
	/**
	 * 设置：标签
	 */
	public void setSelfTag(String selfTag) {
		this.selfTag = selfTag;
	}
	/**
	 * 获取：标签
	 */
	public String getSelfTag() {
		return selfTag;
	}
}
