package com.github.wxiaoqi.security.crm.core.rest;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.PersonalBindCardRequest;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBindCardBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBindCard;

@Controller
@RequestMapping("personalBindCard")
public class PersonalBindCardController extends BaseController<PersonalBindCardBiz,PersonalBindCard> {
	/**
	 * 根据条件查询绑卡信息
	 * @author centerroot
	 * @time 创建时间:2018年8月16日下午5:03:35
	 * @param personalBindCardRequest
	 * @return
	 */
	@RequestMapping(value = "/pageList",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> pageList(@RequestBody @Valid PersonalBindCardRequest personalBindCardRequest){
		personalBindCardRequest.setPlatformId(setPlatformId(personalBindCardRequest.getPlatformId()));
		Map<String, Object> respMap = baseBiz.pageList(personalBindCardRequest);
        return respMap;
    }
	
	/**
	 * 添加绑卡信息
	 * @author centerroot
	 * @time 创建时间:2018年8月16日下午5:04:26
	 * @param personalBindCardRequest
	 * @return
	 */
	@RequestMapping(value = "/addObj",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addCardInfo(@RequestBody @Valid PersonalBindCardRequest personalBindCardRequest){
		Map<String, Object> respMap = baseBiz.addCardInfo(personalBindCardRequest);
        return respMap;
    }
}