package com.github.wxiaoqi.security.crm.core.feignserver;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.AccountTransferRequest;
import com.github.wxiaoqi.security.common.crm.request.IntoAccountRequest;
import com.github.wxiaoqi.security.crm.core.biz.CommonAccountTransferBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 转账相关的接口
 * */
@Api(tags="转账相关的接口")
@Controller
@RequestMapping("feign")
public class FeignCommonAccountTransfer {
	
	@Autowired
	private CommonAccountTransferBiz commonAccountTransferBiz;
	
	/**
	 * @param 
	 * @return
	 */
	@ApiOperation("个人或者商户转账相关的接口")
	@RequestMapping(value = "/accounttransfer",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> accountTransfer(@RequestBody @Valid AccountTransferRequest accountTransferRequest){
		Map<String, Object> response = commonAccountTransferBiz.accountTransfer(accountTransferRequest);
		return response;
    }
	
}
