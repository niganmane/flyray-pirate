package com.github.wxiaoqi.security.crm.core.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.QueryPayMethodRequest;
import com.github.wxiaoqi.security.common.crm.request.ScenePayMethodRequestParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.crm.core.entity.ScenePayMethodConfig;
import com.github.wxiaoqi.security.crm.core.mapper.PayChannelConfigMapper;
import com.github.wxiaoqi.security.crm.core.mapper.ScenePayMethodConfigMapper;

import tk.mybatis.mapper.entity.Example;

/**
 * 场景支付方式配置
 *
 * @author chj
 * @email ${email}
 * @date 2018-09-07 10:55:04
 */
@Service
public class ScenePayMethodConfigBiz extends BaseBiz<ScenePayMethodConfigMapper,ScenePayMethodConfig> {
	
	private static final Logger logger = LoggerFactory.getLogger(ScenePayMethodConfigBiz.class);
	
	@Autowired
	private PayChannelConfigMapper payChannelConfigMapper;
	
	
	
	public List<Map<String, Object>> queryPayMethod(QueryPayMethodRequest param) {
		logger.info("查询支付方式,请求参数。。。{}"+param);
		Map<String, Object> map = new HashMap<>();
		map.put("platformId", param.getPlatformId());
		map.put("merchantId", param.getMerchantId());
		map.put("channel", param.getChannel());
		map.put("sceneCode", param.getSceneCode());
		List<Map<String, Object>>  list = payChannelConfigMapper.queryPayMethod(map);
		return list;
	}
	
	
	public TableResultResponse<ScenePayMethodConfig> queryPayMethods(Map<String, Object> param) {
		logger.info("查询支付方式,请求参数。。。{}"+param);
		Example example = new Example(ScenePayMethodConfig.class);
		example.createCriteria().andEqualTo("sceneCode", (String) param.get("sceneCode"));
		Page<ScenePayMethodConfig> result = PageHelper.startPage(Integer.valueOf((String) param.get("page")), Integer.valueOf((String) param.get("limit")));
		List<ScenePayMethodConfig> list = mapper.selectByExample(example);
		TableResultResponse<ScenePayMethodConfig> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	
	public ScenePayMethodConfig addPayMethod(ScenePayMethodRequestParam param) {
		logger.info("添加支付方式,请求参数。。。{}"+param);
		ScenePayMethodConfig reqConfig = new ScenePayMethodConfig();
		ScenePayMethodConfig config1 = null;
		try {
			BeanUtils.copyProperties(reqConfig, param);
			config1 = mapper.selectOne(reqConfig);
			if (config1 == null) {
				mapper.insert(reqConfig);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config1;
	}
	
	public ScenePayMethodConfig deletePayMethod(ScenePayMethodRequestParam param) {
		logger.info("删除支付方式,请求参数。。。{}"+param);
		ScenePayMethodConfig reqConfig = new ScenePayMethodConfig();
		try {
			BeanUtils.copyProperties(reqConfig, param);
			reqConfig.setId(Long.valueOf(param.getId()));
			mapper.delete(reqConfig);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reqConfig;
	}

	
	
	
}