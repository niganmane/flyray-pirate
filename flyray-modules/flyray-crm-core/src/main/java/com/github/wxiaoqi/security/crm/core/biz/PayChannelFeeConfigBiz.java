package com.github.wxiaoqi.security.crm.core.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.PayChannelFeeConfigRequest;
import com.github.wxiaoqi.security.common.crm.request.SafetyConfigRequest;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.crm.core.entity.PayChannelConfig;
import com.github.wxiaoqi.security.crm.core.entity.PayChannelFeeConfig;
import com.github.wxiaoqi.security.crm.core.entity.PlatformSafetyConfig;
import com.github.wxiaoqi.security.crm.core.mapper.PayChannelConfigMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PayChannelFeeConfigMapper;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 支付通道费率配置表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-06 13:58:35
 */
@Service
public class PayChannelFeeConfigBiz extends BaseBiz<PayChannelFeeConfigMapper,PayChannelFeeConfig> {
	
	@Autowired
	private PayChannelConfigMapper  payChannelConfigMapper;
	/**
	 * 列表
	 * @param bean
	 * @return
	 */
	public TableResultResponse<PayChannelFeeConfig> payChannelFeeList(SafetyConfigRequest bean){
		 Example example = new Example(PlatformSafetyConfig.class);
		 Criteria criteria = example.createCriteria();
		 criteria.andEqualTo("platformId",bean.getPlatformId());
		 Page<PayChannelFeeConfig> result = PageHelper.startPage(bean.getPage(), bean.getLimit());
		 List<PayChannelFeeConfig> list = mapper.selectByExample(example);
		 return new TableResultResponse<PayChannelFeeConfig>(result.getTotal(), list);
	}
	/**
	 * 添加
	 */
	public Map<String, Object> addPayChannelFee(PayChannelFeeConfigRequest request){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			PayChannelFeeConfig config = new PayChannelFeeConfig();
			BeanUtils.copyProperties(request, config);
			//名不能重复start
			PayChannelFeeConfig nameConfig = new PayChannelFeeConfig();
			nameConfig.setFeeName(config.getFeeName());
			List<PayChannelFeeConfig> list = mapper.select(nameConfig);
			if(list.size() > 0){
				result.put("code", "01");
				result.put("msg", "费率名重复");
				return result;
			}
			//名不能重复end
			config.setFeeCode(String.valueOf(SnowFlake.getId()));
			mapper.insertSelective(config);
			result.put("code", "0000");
			result.put("msg", "成功");
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "01");
			result.put("msg", "成功");
		}

		return result;
	}
	/**
	 * 删除
	 */
	public Map<String, Object> deletePayChannelFee(String feeCode){
		Map<String, Object> result = new HashMap<String, Object>();
		PayChannelFeeConfig config = new PayChannelFeeConfig();
		config.setFeeCode(feeCode);
		try {
			//判断是否使用了，如果使用了不能删除
			PayChannelConfig channelConfig = new PayChannelConfig();
			channelConfig.setFeeCode(feeCode);
			List<PayChannelConfig> list = payChannelConfigMapper.select(channelConfig);
			if(list.size() > 0){
				result.put("code", "01");
				result.put("msg", "该费率已使用无法删除");
				return result;
			}
			mapper.delete(config);
			result.put("code", "0000");
			result.put("msg", "成功");
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "01");
			result.put("msg", "异常");
		}
		return result;
	}
	public PayChannelFeeConfig queryInfo(String feeCode){
		return mapper.selectByPrimaryKey(feeCode);
	}
	/**
	 * 查询列表
	 */
	public TableResultResponse<PayChannelFeeConfig> queryList(PayChannelFeeConfigRequest request){
        Example example = new Example(PayChannelFeeConfig.class);
        Map<String, Object> params = new HashMap<String, Object>();
        Query query = new Query(params);
        Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
        List<PayChannelFeeConfig> list = mapper.selectByExample(example);
        return new TableResultResponse<PayChannelFeeConfig>(result.getTotal(), list);
    
	}
	/**
	 * 修改
	 */
	public Map<String, Object> updatePayChannelFee(PayChannelFeeConfigRequest request){
		Map<String, Object> result = new HashMap<String, Object>();
		PayChannelFeeConfig config = new PayChannelFeeConfig();
		try {
			BeanUtils.copyProperties(request, config);
			
			//名字不能重复start
			Example example = new Example(PayChannelFeeConfig.class);
			Criteria criteria = example.createCriteria();
			criteria.andEqualTo("feeName", config.getFeeName());
			criteria.andNotEqualTo("feeCode", config.getFeeCode());
			List<PayChannelFeeConfig> list = mapper.selectByExample(example);
			if(list.size() > 0){
				result.put("code", "01");
				result.put("msg", "费率名重复");
				return result;
			}
			//名字不能重复end
			mapper.updateByPrimaryKey(config);
			result.put("code", "0000");
			result.put("msg", "成功");
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "01");
			result.put("msg", "异常");
		}
		return result;
	}
	
}