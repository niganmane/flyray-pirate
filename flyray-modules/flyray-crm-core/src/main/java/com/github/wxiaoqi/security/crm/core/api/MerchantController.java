package com.github.wxiaoqi.security.crm.core.api;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.QueryMerchantInfoParam;
import com.github.wxiaoqi.security.common.crm.request.QueryMerchantListParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.ResponseHelper;
import com.github.wxiaoqi.security.crm.core.biz.MerchantBaseBiz;
import com.github.wxiaoqi.security.crm.core.entity.MerchantBase;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="商户管理")
@Controller
@RequestMapping("merchants")
public class MerchantController {
	
	@Autowired
	private MerchantBaseBiz merchantBaseBiz;
	
	/**
	 * 微信小程序商户信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("微信小程序商户信息查询")
	@RequestMapping(value = "/list",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryFireSourceInfo(@RequestBody @Valid QueryMerchantListParam param) throws Exception {
		List<MerchantBase> list = merchantBaseBiz.queryList(param);
		return ResponseHelper.success(list, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
    }
	
	/**
	 * 查询单个客户基本信息
	 */
	@ApiOperation("微信小程序单个商户信息查询")
	@RequestMapping(value = "/info", method = RequestMethod.POST)
    @ResponseBody
	public MerchantBase queryInfo(@RequestBody @Valid QueryMerchantInfoParam param){
		return merchantBaseBiz.queryInfo(param.getMerchantId());
	}
	
}
