package com.github.wxiaoqi.security.crm.core.biz;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.SafetyConfigRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.MD5;
import com.github.wxiaoqi.security.common.util.SHA256Utils;
import com.github.wxiaoqi.security.crm.core.entity.PlatformSafetyConfig;
import com.github.wxiaoqi.security.crm.core.mapper.PlatformSafetyConfigMapper;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 平台安全配置信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Slf4j
@Service
public class PlatformSafetyConfigBiz extends BaseBiz<PlatformSafetyConfigMapper,PlatformSafetyConfig> {

	/**
	 * 列表
	 * @param bean
	 * @return
	 */
	public TableResultResponse<PlatformSafetyConfig> safetyConfigList(SafetyConfigRequest bean){
		 Example example = new Example(PlatformSafetyConfig.class);
		 Criteria criteria = example.createCriteria();
		 criteria.andEqualTo("platformId",bean.getPlatformId());
		 example.setOrderByClause("create_time desc");
		 Page<PlatformSafetyConfig> result = PageHelper.startPage(bean.getPage(), bean.getLimit());
		 List<PlatformSafetyConfig> list = mapper.selectByExample(example);
		 return new TableResultResponse<PlatformSafetyConfig>(result.getTotal(), list);
	}
	
	/**
	 * 添加
	 * @param bean
	 * @return
	 */
	public Map<String, Object> addConfig(SafetyConfigRequest bean){
		 PlatformSafetyConfig config = new PlatformSafetyConfig();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 String appIdCipherText = MD5.md5(SHA256Utils.SHA256(config.getPlatformId()));			 
			 config.setAppId(appIdCipherText);
			 config.setCreateTime(new Date());
			 config.setUpdateTime(new Date());
			 if (mapper.insert(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加安全配置】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【添加安全配置】  报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加安全配置】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【添加安全配置】   响应参数：{}", respMap);
		 return respMap;
	}
	
	
	/**
	 * 修改
	 * @param bean
	 * @return
	 */
	public Map<String, Object> updateConfig(SafetyConfigRequest bean){
		 PlatformSafetyConfig config = new PlatformSafetyConfig();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 config.setUpdateTime(new Date());
			 if (mapper.updateByPrimaryKeySelective(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【修改安全配置】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【修改安全配置】  报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【修改安全配置】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【修改安全配置】   响应参数：{}", respMap);
		 return respMap;
	}
	
	
	/**
	 * 删除
	 * @param bean
	 * @return
	 */
	public Map<String, Object> deleteConfig(SafetyConfigRequest bean){
		 PlatformSafetyConfig config = new PlatformSafetyConfig();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 if (mapper.delete(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除安全配置】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【删除安全配置】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除安全配置】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【删除安全配置】   响应参数：{}", respMap);
		 return respMap;
	}
}