package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PlatformBase;
import tk.mybatis.mapper.common.Mapper;

/**
 * 平台基础信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:48
 */
@org.apache.ibatis.annotations.Mapper
public interface PlatformBaseMapper extends Mapper<PlatformBase> {
	
}
