package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 用户绑卡表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-16 16:56:09
 */
@Table(name = "personal_bind_card")
public class PersonalBindCard extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //流水号
    @Id
    private String id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //个人客户编号
    @Column(name = "personal_id")
    private String personalId;
	
	    //银行卡号  隐藏中间数位的银行卡号
    @Column(name = "bind_card_no")
    private String bindCardNo;
	
	    //加密银行卡号
    @Column(name = "bind_encrypt_card_no")
    private String bindEncryptCardNo;
	
	    //持卡人姓名
    @Column(name = "cardholder_name")
    private String cardholderName;
	
	    //银行编号
    @Column(name = "bank_no")
    private String bankNo;
	
	    //银行名称
    @Column(name = "bank_name")
    private String bankName;
	
	    //支行编号
    @Column(name = "subbranch_no")
    private String subbranchNo;
	
	    //支行信息
    @Column(name = "subbranch_name")
    private String subbranchName;
	
	    //状态  00：待验证  01：绑定  02：解绑 
    @Column(name = "status")
    private String status;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	

	/**
	 * 设置：流水号
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：流水号
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：个人客户编号
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 获取：个人客户编号
	 */
	public String getPersonalId() {
		return personalId;
	}
	/**
	 * 设置：银行卡号  隐藏中间数位的银行卡号
	 */
	public void setBindCardNo(String bindCardNo) {
		this.bindCardNo = bindCardNo;
	}
	/**
	 * 获取：银行卡号  隐藏中间数位的银行卡号
	 */
	public String getBindCardNo() {
		return bindCardNo;
	}
	/**
	 * 设置：加密银行卡号
	 */
	public void setBindEncryptCardNo(String bindEncryptCardNo) {
		this.bindEncryptCardNo = bindEncryptCardNo;
	}
	/**
	 * 获取：加密银行卡号
	 */
	public String getBindEncryptCardNo() {
		return bindEncryptCardNo;
	}
	/**
	 * 设置：持卡人姓名
	 */
	public void setCardholderName(String cardholderName) {
		this.cardholderName = cardholderName;
	}
	/**
	 * 获取：持卡人姓名
	 */
	public String getCardholderName() {
		return cardholderName;
	}
	/**
	 * 设置：银行编号
	 */
	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}
	/**
	 * 获取：银行编号
	 */
	public String getBankNo() {
		return bankNo;
	}
	/**
	 * 设置：银行名称
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/**
	 * 获取：银行名称
	 */
	public String getBankName() {
		return bankName;
	}
	/**
	 * 设置：支行编号
	 */
	public void setSubbranchNo(String subbranchNo) {
		this.subbranchNo = subbranchNo;
	}
	/**
	 * 获取：支行编号
	 */
	public String getSubbranchNo() {
		return subbranchNo;
	}
	/**
	 * 设置：支行信息
	 */
	public void setSubbranchName(String subbranchName) {
		this.subbranchName = subbranchName;
	}
	/**
	 * 获取：支行信息
	 */
	public String getSubbranchName() {
		return subbranchName;
	}
	/**
	 * 设置：状态  00：待验证  01：绑定  02：解绑 
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：状态  00：待验证  01：绑定  02：解绑 
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
