package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 个人客户跟踪表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-05 10:34:23
 */
@Table(name = "personal_base_tracking")
public class PersonalBaseTracking implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //跟踪人ID
    @Column(name = "ticker_personal_id")
    private String tickerPersonalId;
	
	    //被跟踪人ID
    @Column(name = "personal_id")
    private String personalId;
	
	    //备注
    @Column(name = "remark")
    private String remark;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	

	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：跟踪人ID
	 */
	public void setTickerPersonalId(String tickerPersonalId) {
		this.tickerPersonalId = tickerPersonalId;
	}
	/**
	 * 获取：跟踪人ID
	 */
	public String getTickerPersonalId() {
		return tickerPersonalId;
	}
	/**
	 * 设置：被跟踪人ID
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 获取：被跟踪人ID
	 */
	public String getPersonalId() {
		return personalId;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
