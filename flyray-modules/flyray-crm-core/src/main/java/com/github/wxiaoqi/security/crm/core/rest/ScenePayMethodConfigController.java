package com.github.wxiaoqi.security.crm.core.rest;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.ScenePayMethodRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.ResponseHelper;
import com.github.wxiaoqi.security.crm.core.biz.ScenePayMethodConfigBiz;
import com.github.wxiaoqi.security.crm.core.entity.ScenePayMethodConfig;


/**
 * 场景支付方式
 * @author Administrator
 *
 */
@Controller
@RequestMapping("scenePayMethodConfig")
public class ScenePayMethodConfigController extends BaseController<ScenePayMethodConfigBiz,ScenePayMethodConfig> {
	
	private static final Logger logger = LoggerFactory.getLogger(ScenePayMethodConfigController.class);
	
	
	@ResponseBody
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public TableResultResponse<ScenePayMethodConfig> query(@RequestParam Map<String, Object> param) {
		logger.info("查询场景支付方式，请求参数。。。{}"+param);
		return baseBiz.queryPayMethods(param);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public Map<String, Object> add(@Valid @RequestBody ScenePayMethodRequestParam param) {
		logger.info("添加场景支付方式，请求参数。。。{}"+param);
		ScenePayMethodConfig config = baseBiz.addPayMethod(param);
		if (config == null) {
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_EXIST.getCode(), ResponseCode.FUNCTION_EXIST.getMessage());
		}
		
	}
	
	@ResponseBody
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public Map<String, Object> delete(@RequestBody ScenePayMethodRequestParam param) {
		logger.info("删除场景支付方式，请求参数。。。{}"+param);
		ScenePayMethodConfig config = baseBiz.deletePayMethod(param);
		if (config != null) {
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
		
	}

}