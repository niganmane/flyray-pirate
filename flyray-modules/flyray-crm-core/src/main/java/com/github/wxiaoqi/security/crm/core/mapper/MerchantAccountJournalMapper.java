package com.github.wxiaoqi.security.crm.core.mapper;

import java.math.BigDecimal;

import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournal;
import tk.mybatis.mapper.common.Mapper;

/**
 * 商户账户流水（充、转、提、退、冻结流水）
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface MerchantAccountJournalMapper extends Mapper<MerchantAccountJournal> {
	/**
	 * 获取指定订单下的商户退款出账合计金额
	 * */
	public BigDecimal merAccountRefoundCount(MerchantAccountJournal merchantAccountJournal);
}
