package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 消息表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:49
 */
@Table(name = "message_info")
public class MessageInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //消息编号
    @Id
    private String messageId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //消息类型  00：系统消息  01：会员消息  02：商户消息
    @Column(name = "message_type")
    private String messageType;
	
	    //消息标题
    @Column(name = "message_title")
    private String messageTitle;
	
	    //消息内容
    @Column(name = "message_content")
    private String messageContent;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	

	/**
	 * 设置：消息编号
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	/**
	 * 获取：消息编号
	 */
	public String getMessageId() {
		return messageId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：消息类型  00：系统消息  01：会员消息  02：商户消息
	 */
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	/**
	 * 获取：消息类型  00：系统消息  01：会员消息  02：商户消息
	 */
	public String getMessageType() {
		return messageType;
	}
	/**
	 * 设置：消息标题
	 */
	public void setMessageTitle(String messageTitle) {
		this.messageTitle = messageTitle;
	}
	/**
	 * 获取：消息标题
	 */
	public String getMessageTitle() {
		return messageTitle;
	}
	/**
	 * 设置：消息内容
	 */
	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}
	/**
	 * 获取：消息内容
	 */
	public String getMessageContent() {
		return messageContent;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
