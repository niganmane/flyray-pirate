package com.github.wxiaoqi.security.crm.core.rest;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.PlatformAccoutConfigRequest;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PlatformAccoutConfigBiz;
import com.github.wxiaoqi.security.crm.core.entity.PlatformAccoutConfig;

@Controller
@RequestMapping("platformAccoutConfig")
public class PlatformAccoutConfigController extends BaseController<PlatformAccoutConfigBiz,PlatformAccoutConfig> {

	/**
	 * 根据平台编号查询平台支持账户类型
	 * @author centerroot
	 * @time 创建时间:2018年8月16日上午11:41:30
	 * @param platformId
	 * @return
	 */
	@RequestMapping(value = "/pageList",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> pageList(@RequestBody @Valid PlatformAccoutConfigRequest platformAccoutConfigRequest){
		Map<String, Object> respMap = baseBiz.pageList(platformAccoutConfigRequest);
        return respMap;
    }
	
	/**
	 * 添加支持的账户类型
	 * @author centerroot
	 * @time 创建时间:2018年8月16日下午1:44:08
	 * @param platformAccoutConfigRequest
	 * @return
	 */
	@RequestMapping(value = "/addObj",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addPlatformAccout(@RequestBody @Valid PlatformAccoutConfigRequest platformAccoutConfigRequest){
		Map<String, Object> respMap = baseBiz.addPlatformAccout(platformAccoutConfigRequest);
        return respMap;
    }
	
}