package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.PersonalBaseExt;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBaseExtMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 个人基础信息扩展表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-14 14:15:09
 */
@Service
public class PersonalBaseExtBiz extends BaseBiz<PersonalBaseExtMapper,PersonalBaseExt> {
}