package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.common.crm.request.PersonalInfoRequest;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.entity.PersonalInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 个人客户基础信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalBaseMapper extends Mapper<PersonalBase> {
	/**
	 * 根据条件查询个人信息列表（包含扩展信息）
	 * @author centerroot
	 * @time 创建时间:2018年8月14日下午3:10:26
	 * @param map
	 * @return
	 */
	List<PersonalInfo> queryPersonalList(Map<String, Object> map);
	
	/**
	 * 根据用户ID查询下属三级人员
	 * @author centerroot
	 * @time 创建时间:2018年8月28日上午11:30:52
	 * @param map
	 * @return
	 */
	List<PersonalBase> queryPersonalNetwork(Map<String, Object> map);
	
}
