package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 用户实名认证信息表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-10 15:02:35
 */
@Table(name = "personal_certification_info")
public class PersonalCertificationInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private String id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //个人客户编号
    @Column(name = "personal_id")
    private String personalId;
	
	    //银行卡号
    @Column(name = "bank_card_no")
    private String bankCardNo;
	
	    //加密的银行卡号
    @Column(name = "bind_encrypt_card_no")
    private String bindEncryptCardNo;
	
	    //持卡人
    @Column(name = "cardholder_name")
    private String cardholderName;
	
	    //银行编号
    @Column(name = "bank_no")
    private String bankNo;
	
	    //银行名称
    @Column(name = "bank_name")
    private String bankName;
	
	    //支行编号
    @Column(name = "subbranch_no")
    private String subbranchNo;
	
	    //支行信息
    @Column(name = "subbranch_name")
    private String subbranchName;
	
	    //第三方认证id
    @Column(name = "auth_id")
    private String authId;
	
	    //第三方认证类型 00代表银行卡四要素实名人 01代表泰华电子实名认证
    @Column(name = "auth_type")
    private String authType;
	
	    //
    @Column(name = "create_time")
    private Date createTime;
	
	    //
    @Column(name = "update_time")
    private Date updateTime;
	

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：个人客户编号
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 获取：个人客户编号
	 */
	public String getPersonalId() {
		return personalId;
	}
	/**
	 * 设置：银行卡号
	 */
	public void setBankCardNo(String bankCardNo) {
		this.bankCardNo = bankCardNo;
	}
	/**
	 * 获取：银行卡号
	 */
	public String getBankCardNo() {
		return bankCardNo;
	}
	/**
	 * 设置：加密的银行卡号
	 */
	public void setBindEncryptCardNo(String bindEncryptCardNo) {
		this.bindEncryptCardNo = bindEncryptCardNo;
	}
	/**
	 * 获取：加密的银行卡号
	 */
	public String getBindEncryptCardNo() {
		return bindEncryptCardNo;
	}
	/**
	 * 设置：持卡人
	 */
	public void setCardholderName(String cardholderName) {
		this.cardholderName = cardholderName;
	}
	/**
	 * 获取：持卡人
	 */
	public String getCardholderName() {
		return cardholderName;
	}
	/**
	 * 设置：银行编号
	 */
	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}
	/**
	 * 获取：银行编号
	 */
	public String getBankNo() {
		return bankNo;
	}
	/**
	 * 设置：银行名称
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/**
	 * 获取：银行名称
	 */
	public String getBankName() {
		return bankName;
	}
	/**
	 * 设置：支行编号
	 */
	public void setSubbranchNo(String subbranchNo) {
		this.subbranchNo = subbranchNo;
	}
	/**
	 * 获取：支行编号
	 */
	public String getSubbranchNo() {
		return subbranchNo;
	}
	/**
	 * 设置：支行信息
	 */
	public void setSubbranchName(String subbranchName) {
		this.subbranchName = subbranchName;
	}
	/**
	 * 获取：支行信息
	 */
	public String getSubbranchName() {
		return subbranchName;
	}
	/**
	 * 设置：第三方认证id
	 */
	public void setAuthId(String authId) {
		this.authId = authId;
	}
	/**
	 * 获取：第三方认证id
	 */
	public String getAuthId() {
		return authId;
	}
	/**
	 * 设置：第三方认证类型 01代表泰华电子实名认证
	 */
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	/**
	 * 获取：第三方认证类型 01代表泰华电子实名认证
	 */
	public String getAuthType() {
		return authType;
	}
	/**
	 * 设置：
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
