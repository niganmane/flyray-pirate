package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.BaseArea;

import tk.mybatis.mapper.common.Mapper;

/**
 * 行政区划
 * 
 * @author chj
 * @email ${email}
 * @date 2018-09-19 16:28:11
 */
@org.apache.ibatis.annotations.Mapper
public interface BaseAreaMapper extends Mapper<BaseArea> {
	
	List<BaseArea> listAreaByParentCode(Map<String, Object> param);
}
