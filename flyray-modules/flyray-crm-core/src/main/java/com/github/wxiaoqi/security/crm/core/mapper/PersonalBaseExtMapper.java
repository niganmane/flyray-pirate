package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PersonalBaseExt;
import tk.mybatis.mapper.common.Mapper;

/**
 * 个人基础信息扩展表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-14 14:15:09
 */
@org.apache.ibatis.annotations.Mapper
public interface PersonalBaseExtMapper extends Mapper<PersonalBaseExt> {
	
}
