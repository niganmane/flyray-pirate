package com.github.wxiaoqi.security.crm.core.biz;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.MerchantAccountRequest;
import com.github.wxiaoqi.security.common.crm.request.MerchantOneAccountRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccount;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountMapper;

/**
 * 商户账户信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class MerchantAccountBiz extends BaseBiz<MerchantAccountMapper,MerchantAccount> {
	private static Logger logger = LoggerFactory.getLogger(MerchantAccountBiz.class);
	
	/**
	 * 查询商户账户列表
	 * @author centerroot
	 * @time 创建时间:2018年8月16日下午4:25:56
	 * @param merchantAccountRequest
	 * @return
	 */
	public Map<String, Object> pageList(MerchantAccountRequest merchantAccountRequest){
		logger.info("查询商户账户列表请求参数：{}",EntityUtils.beanToMap(merchantAccountRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		int page = merchantAccountRequest.getPage();
		int limit = merchantAccountRequest.getLimit();
		
		MerchantAccount merchantAccountReq = new MerchantAccount();
		BeanUtils.copyProperties(merchantAccountRequest, merchantAccountReq);
		List<MerchantAccount> list = mapper.select(merchantAccountReq);
		
		if (page == 0) {
			respMap.put("merchantAccounts", list);
		}else{
			Map<String, Object> params = new HashMap<String, Object>();
		    params.put("page", page);
		    params.put("limit", limit);
		    Query query = new Query(params);
		    Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		    
			TableResultResponse<MerchantAccount> table = new TableResultResponse<MerchantAccount>(result.getTotal(), list);
	        respMap.put("body", table);
		}
		
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
        logger.info("查询商户账户列表 响应结果：{}",respMap);
		return respMap;
	}
	
	/**
	 * 查询某一商户某一账户余额
	 * @param merchantOneAccountRequest
	 * @return
	 */
	public Map<String, Object> queryOneAccount(MerchantOneAccountRequest merchantOneAccountRequest){
		logger.info("查询某一商户某一账户余额请求参数：{}",EntityUtils.beanToMap(merchantOneAccountRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		MerchantAccount merchantAccount = new MerchantAccount();
		BeanUtils.copyProperties(merchantOneAccountRequest, merchantAccount);
		MerchantAccount selectOne = mapper.selectOne(merchantAccount);
		if(null != selectOne){
			BigDecimal accountBalance = selectOne.getAccountBalance();
			if(null == accountBalance){
				respMap.put("balance", "0.00");
			}else{
				respMap.put("balance", String.valueOf(accountBalance));
			}
		}else{
			respMap.put("balance", "0.00");
		}
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
        logger.info("查询某一商户某一账户余额 响应结果：{}",respMap);
		return respMap;
	}
	
}