package com.github.wxiaoqi.security.crm.core.biz;

import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.SceneRequestParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.crm.core.entity.SceneInfo;
import com.github.wxiaoqi.security.crm.core.mapper.SceneInfoMapper;

import tk.mybatis.mapper.entity.Example;

/**
 * 场景
 *
 * @author chj
 * @email ${email}
 * @date 2018-09-07 10:55:04
 */
@Service
public class SceneInfoBiz extends BaseBiz<SceneInfoMapper,SceneInfo> {
	
	private static final Logger logger = LoggerFactory.getLogger(SceneInfoBiz.class);
	
	public TableResultResponse<SceneInfo> queryScenes(Map<String, Object> param) {
		logger.info("查询场景,请求参数。。。{}"+param);
		Example example = new Example(SceneInfo.class);
		Page<SceneInfo> result = PageHelper.startPage(Integer.valueOf((String) param.get("page")), Integer.valueOf((String) param.get("limit")));
		List<SceneInfo> list = mapper.selectByExample(example);
		TableResultResponse<SceneInfo> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	
	public SceneInfo addScene(SceneRequestParam param) {
		logger.info("添加场景,请求参数。。。{}"+param);
		SceneInfo reqConfig = new SceneInfo();
		SceneInfo config1 = null;
		try {
			BeanUtils.copyProperties(reqConfig, param);
			config1 = mapper.selectOne(reqConfig);
			if (config1 == null) {
				mapper.insert(reqConfig);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config1;
	}
	
	public SceneInfo deleteScene(SceneRequestParam param) {
		logger.info("删除场景,请求参数。。。{}"+param);
		SceneInfo reqConfig = new SceneInfo();
		try {
			BeanUtils.copyProperties(reqConfig, param);
			reqConfig.setId(Long.valueOf(param.getId()));
			mapper.delete(reqConfig);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reqConfig;
	}
}