package com.github.wxiaoqi.security.crm.core.biz;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.PersonalAccountRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.crm.core.entity.PersonalAccount;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.entity.PlatformCoinCustomer;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalAccountMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBaseMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PlatformCoinCustomerMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * 个人账户信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Slf4j
@Service
public class PersonalAccountBiz extends BaseBiz<PersonalAccountMapper,PersonalAccount> {
	
	@Autowired
	private PersonalAccountMapper personalAccountMapper;
	@Autowired
	private PersonalBaseMapper personalBaseMapper;
	@Autowired
	private PlatformCoinCustomerMapper platformCoinCustomerMapper;
	
	/**
	 * 查询个人账户信息列表
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:38
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	public Map<String, Object> queryList(PersonalAccountRequest personalAccountRequest){
		log.info("【查询个人账户信息列表】   请求参数：{}",EntityUtils.beanToMap(personalAccountRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String platformId = personalAccountRequest.getPlatformId();
		String personalId = personalAccountRequest.getPersonalId();
		int page = personalAccountRequest.getPage();
		int limit = personalAccountRequest.getLimit();
		
	    PersonalAccount personalAccountReq = new PersonalAccount();
	    personalAccountReq.setPersonalId(personalId);
	    personalAccountReq.setPlatformId(platformId);
        List<PersonalAccount> list = mapper.select(personalAccountReq);
        respMap.put("personalAccountList", list);
        respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【查询个人账户信息列表】   响应参数：{}",respMap);
		return respMap;
	}
	

	/**
	 * 微信小程序原力值账户前三排名用户信息查询
	 * @param request
	 * @return
	 */
	public List<Map<String, Object>> forceTopAccountInfo(Map<String, Object> request){
		log.info("微信小程序原力值账户前三排名用户信息查询.......{}", request);
		PersonalAccount personalAccount = new PersonalAccount();
		personalAccount.setPlatformId((String) request.get("platformId"));
		List<PersonalAccount> selectTopThreeList = personalAccountMapper.selectTopList(personalAccount);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		int num = 1;
		for(PersonalAccount forAccount:selectTopThreeList){
			Map<String, Object> map = new HashMap<String, Object>();
			String personalId = forAccount.getPersonalId();
			BigDecimal accountBalance = forAccount.getAccountBalance();
			String forceBalance = String.valueOf(accountBalance);
			PersonalBase personalBase = new PersonalBase();
			personalBase.setPlatformId((String) request.get("platformId"));
			personalBase.setPersonalId(personalId);
			PersonalBase selectPersonalBase = personalBaseMapper.selectOne(personalBase);
			String nickName = selectPersonalBase.getNickName();
			String avatar = selectPersonalBase.getAvatar();
			PlatformCoinCustomer platformCoinCustomer = new PlatformCoinCustomer();
			platformCoinCustomer.setPlatformId("13608825924956160");//顶级平台编号
			platformCoinCustomer.setIdCard(selectPersonalBase.getIdCard());
			PlatformCoinCustomer selectCoinCustomer = platformCoinCustomerMapper.selectOne(platformCoinCustomer);
			String fireSource = "0";
			if(null != selectCoinCustomer){
				BigDecimal balance = selectCoinCustomer.getBalance();
				fireSource = String.valueOf(balance);
			}
			map.put("forceBalance", forceBalance);
			map.put("nickName", nickName);
			map.put("avatar", avatar);
			map.put("fireSource", fireSource);
			map.put("num", num);
			list.add(map);
			num++;
		}
		log.info("微信小程序原力值账户前三排名用户信息查询.......{}",list);
		return list;
	}
}