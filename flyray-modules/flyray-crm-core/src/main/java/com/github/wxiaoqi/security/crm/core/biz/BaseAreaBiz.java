package com.github.wxiaoqi.security.crm.core.biz;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.BaseAreaRequestParam;
import com.github.wxiaoqi.security.common.util.TreeUtil;
import com.github.wxiaoqi.security.crm.core.entity.BaseArea;
import com.github.wxiaoqi.security.crm.core.mapper.BaseAreaMapper;
import com.github.wxiaoqi.security.crm.core.vo.AreaTree;

/**
 * 行政区划
 *
 * @author chj
 * @email ${email}
 * @date 2018-09-19 16:28:11
 */
@Service
public class BaseAreaBiz extends BaseBiz<BaseAreaMapper,BaseArea> {
	
	private static final Logger logger = LoggerFactory.getLogger(BaseAreaBiz.class);
	
	/**
	 * 根据父级code查询子节点，树形目录
	 * 
	 * @param map
	 * @return
	 */
	public List<BaseArea> listAreaByParentCode(String areaCode) {
		Map<String, Object> map = new HashMap<>();
		map.put("parentCode", areaCode);
		List<BaseArea> areas = mapper.listAreaByParentCode(map);
		for(BaseArea area : areas) {
			area.checkParent();
		}
		return areas;
	}
	
	public List<AreaTree> listAreaTree(String areaCode) {
		List<AreaTree> trees = new ArrayList<>();
		Map<String, Object> map = new HashMap<>();
		map.put("parentCode", areaCode);
		List<BaseArea> areas = mapper.listAreaByParentCode(map);
		for(BaseArea area : areas) {
			area.checkParent();
			AreaTree node = new AreaTree();
			try {
				BeanUtils.copyProperties(node, area);
				node.setLabel(area.getName());
				node.setValue(area.getAreaCode());
				trees.add(node);
			} catch (IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return TreeUtil.bulid1(trees,0) ;
	}
	
	/**
	 * 
	 * 根据父级code查询子节点，子区域列表
	 * @param map
	 * @return
	 */
	public List<BaseArea> listAreaByParentCode(Map<String, Object> map) {
		List<BaseArea> areas = mapper.listAreaByParentCode(map);
		for(BaseArea area : areas) {
			area.checkParent();
		}
		return areas;
	}
	
	/**
	 * 添加区域
	 * @param param
	 * @return
	 */
	public BaseArea addBaseArea(BaseAreaRequestParam param) {
		logger.info("添加区域,请求参数。。。{}"+param);
		BaseArea area = new BaseArea();
		BaseArea area1 = null;
		try {
			BeanUtils.copyProperties(area, param);
			area1 = mapper.selectOne(area);
			if (area1 == null) {
				area.setCreateTime(new Date());
				mapper.insert(area);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return area1;
	}
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	public BaseArea deleteBaseArea(BaseAreaRequestParam param) {
		logger.info("删除区域,请求参数。。。{}"+param);
		BaseArea area = new BaseArea();
		BaseArea area1 = null;
		try {
			BeanUtils.copyProperties(area, param);
			area.setAreaId(Long.valueOf(param.getAreaId()));
			area1 = mapper.selectOne(area);
			if (area1 != null) {
				mapper.delete(area);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return area1;	
	}
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	public BaseArea updateBaseArea(BaseAreaRequestParam param) {
		logger.info("修改区域,请求参数。。。{}"+param);
		BaseArea area = new BaseArea();
		BaseArea area1 = null;
		try {
			BeanUtils.copyProperties(area, param);
			area.setAreaId(Long.valueOf(param.getAreaId()));
			area1 = mapper.selectOne(area);
			if (area1 != null) {
				area.setUpdateTime(new Date());
				mapper.updateByPrimaryKeySelective(area);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return area1;
	}
	
	/**
	 * 信息
	 * @param param
	 * @return
	 */
	public BaseArea areaInfo(BaseAreaRequestParam param) {
		logger.info("查询区域详情,请求参数。。。{}"+param);
		BaseArea area = new BaseArea();
		BaseArea area1 = null;
		try {
			BeanUtils.copyProperties(area, param);
			area.setAreaId(Long.valueOf(param.getAreaId()));
			area1 = mapper.selectOne(area);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return area1;
	}
	
	
	public BaseArea QueryAreaInfo(BaseAreaRequestParam param) {
		logger.info("查询区域详情,请求参数。。。{}"+param);
		BaseArea area = new BaseArea();
		BaseArea area1 = null;
		try {
			area.setAreaCode(param.getAreaCode());
			area1 = mapper.selectOne(area);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return area1;
	}
	
	public List<BaseArea> queryProvinces(BaseAreaRequestParam param) {
		BaseArea area = new BaseArea();
		area.setLayer(Integer.valueOf(param.getLayer()));
		List<BaseArea> list = mapper.select(area);
		return list;
	}
	
}