package com.github.wxiaoqi.security.crm.core.blockchain.block;

import java.time.Instant;

import com.github.wxiaoqi.security.crm.core.blockchain.dpos.DelegatedProofOfStake;
import com.github.wxiaoqi.security.crm.core.blockchain.dpos.PowResult;
import com.github.wxiaoqi.security.crm.core.blockchain.dpos.ProofOfWork;
import com.github.wxiaoqi.security.crm.core.blockchain.transaction.MerkleTree;
import com.github.wxiaoqi.security.crm.core.blockchain.transaction.Transaction;
import com.github.wxiaoqi.security.crm.core.blockchain.util.ByteUtils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 区块
 *
 * @author wangwei
 * @date 2018/02/02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Block {

    /**
     * 区块hash值
     */
    private String hash;
    /**
     * 前一个区块的hash值
     */
    private String prevBlockHash;
    /**
     * 交易信息
     */
    private Transaction[] transactions;
    ;
    /**
     * 区块创建时间(单位:秒)
     */
    private long timeStamp;
    /**
     * 工作量证明计数器
     */
    private long nonce;
    /**
     * 区块奖励交易
     */
    private Transaction rewardTransaction;

    /**
     * <p> 创建创世区块 </p>
     *
     * @param coinbase
     * @return
     */
    public static Block newGenesisBlock(Transaction coinbase) {
        return Block.newBlock(ByteUtils.ZERO_HASH, new Transaction[]{coinbase});
    }

    /**
     * <p> 创建新区块 </p>
     *
     * @param previousHash
     * @param transactions
     * @return
     */
    public static Block newBlock(String previousHash, Transaction[] transactions) {
        Block block = new Block("", previousHash, transactions, Instant.now().getEpochSecond(), 0, null);
        ProofOfWork pow = ProofOfWork.newProofOfWork(block);
        PowResult powResult = pow.run();
        block.setHash(powResult.getHash());
        block.setNonce(powResult.getNonce());
        return block;
    }

    public static Block newGenesisDposBlock(Transaction coinbase,Long platformId) {
        return Block.newDposBlock(ByteUtils.ZERO_HASH, new Transaction[]{coinbase},platformId);
    }
    /**
     * 创建dpos区块
     * @param previousHash
     * @param transactions
     * @param platformId
     * @return
     */
    public static Block newDposBlock(String previousHash, Transaction[] transactions,Long platformId){
    	//TODO 生成区块之前验证交易签名
        Block block = new Block("", previousHash, transactions, Instant.now().getEpochSecond(), 0, null);
//        DelegatedProofOfStake dpos = DelegatedProofOfStake.newDelegatedProofOfStake(Dblock,platformId);
//        String blockHash = dpos.run();
//        block.setHash(blockHash);
        return block;
    }

    /**
     * 对区块中的交易信息进行Hash计算
     *
     * @return
     */
    public byte[] hashTransaction() {
        if (this.getRewardTransaction() != null) {
            byte[][] txIdArrays = new byte[this.getTransactions().length + 1][];
            for (int i = 0; i < this.getTransactions().length; i++) {
                txIdArrays[i] = this.getTransactions()[i].hash();
            }
            txIdArrays[this.getTransactions().length] = getRewardTransaction().hash();
            return new MerkleTree(txIdArrays).getRoot().getHash();
        } else {
            byte[][] txIdArrays = new byte[this.getTransactions().length][];
            for (int i = 0; i < this.getTransactions().length; i++) {
                txIdArrays[i] = this.getTransactions()[i].hash();
            }
            return new MerkleTree(txIdArrays).getRoot().getHash();
        }


    }
}
