package com.github.wxiaoqi.security.crm.core.feignserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.FreezeRequest;
import com.github.wxiaoqi.security.common.crm.request.UnFreezeRequest;
import com.github.wxiaoqi.security.crm.core.biz.CommonFreezeUnFreezeBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 冻结、解冻相关的接口
 * */
@Api(tags="冻结、解冻相关的接口")
@Controller
@RequestMapping("feign")
public class FeignCommonFreezeUnFreeze {
	
	@Autowired
	private CommonFreezeUnFreezeBiz commonFreezeUnFreezeBiz;
	
	
	/***
	 * 个人或者商户冻结
	 * */
	@ApiOperation("冻结接口")
	@RequestMapping(value = "/freeze",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> freeze(@RequestBody @Valid FreezeRequest freezeRequest){
		Map<String, Object> response = commonFreezeUnFreezeBiz.freeze(freezeRequest);
		return response;
	}
	
	/**
	 * 个人或者商户解冻
	 * */
	@ApiOperation("解冻接口")
	@RequestMapping(value = "/unfreeze",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> unfreeze(@RequestBody @Valid UnFreezeRequest unFreezeRequest){
		Map<String, Object> response = commonFreezeUnFreezeBiz.unfreeze(unFreezeRequest);
		return response;
    }

}
