package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.CustomerMessageRelBiz;
import com.github.wxiaoqi.security.crm.core.entity.CustomerMessageRel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("customerMessageRel")
public class CustomerMessageRelController extends BaseController<CustomerMessageRelBiz,CustomerMessageRel> {

}