package com.github.wxiaoqi.security.crm.core.rest;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.crm.request.PersonalBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.PersonalInfoRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryPersonalBaseListRequest;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;

/**
 * 个人基础信息
 * @author centerroot
 * @time 创建时间:2018年7月16日下午6:01:37
 * @description
 */
@RestController
@RequestMapping("personalBase")
public class PersonalBaseController extends BaseController<PersonalBaseBiz,PersonalBase> {
	
	@Autowired
	private PersonalBaseBiz personalBaseBiz;
	
	/**
	 * 查询个人基础信息列表
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:04
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	@RequestMapping(value = "/queryList", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryList(@RequestBody @Valid QueryPersonalBaseListRequest queryPersonalBaseListRequest){
		queryPersonalBaseListRequest.setPlatformId(setPlatformId(queryPersonalBaseListRequest.getPlatformId()));
		return personalBaseBiz.queryList(queryPersonalBaseListRequest);
	}
	
	/**
	 * 添加个人基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:15
	 * @param personalBaseRequest
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> add(@RequestBody @Valid PersonalBaseRequest personalBaseRequest){
		return personalBaseBiz.add(personalBaseRequest);
	}
	
	/**
	 * 查询单个个人客户信息
	 * @author centerroot
	 * @time 创建时间:2018年8月13日下午5:35:45
	 * @param personalId
	 * @return
	 */
	@RequestMapping(value = "/queryOne/{personalId}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOneObj(@PathVariable String personalId){
		Map<String, Object> respMap = baseBiz.getOneObj(personalId);
        return respMap;
    }
	
	/**
	 * 删除个人客户信息
	 * @author centerroot
	 * @time 创建时间:2018年8月13日下午5:35:59
	 * @param personalId
	 * @return
	 */
	@RequestMapping(value = "/deleteObj/{personalId}",method = RequestMethod.DELETE)
    @ResponseBody
    public Map<String, Object> removeObj(@PathVariable String personalId){
		Map<String, Object> respMap = baseBiz.removeObj(personalId);
		return respMap;
    }
	
	/**
	 * 更新个人客户信息
	 * @author centerroot
	 * @time 创建时间:2018年8月13日下午5:36:15
	 * @param personalBase
	 * @return
	 */
	@RequestMapping(value = "/updateObj",method = RequestMethod.PUT)
    @ResponseBody
    public Map<String, Object> updateObj(@RequestBody @Valid PersonalBaseRequest personalBaseRequest){
		Map<String, Object> respMap = baseBiz.updateObj(personalBaseRequest);
        return respMap;
    }
	
	
	
	
	@RequestMapping(value = "/queryPeopleNetwork",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryPeopleNetwork(@RequestBody @Valid PersonalBaseRequest personalBaseRequest){
		Map<String, Object> respMap = baseBiz.queryPeopleNetwork(personalBaseRequest);
        return respMap;
    }
	
}