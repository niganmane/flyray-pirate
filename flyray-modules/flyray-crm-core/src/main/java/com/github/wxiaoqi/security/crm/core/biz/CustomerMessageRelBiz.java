package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.CustomerMessageRel;
import com.github.wxiaoqi.security.crm.core.mapper.CustomerMessageRelMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 用户消息关联表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:50
 */
@Service
public class CustomerMessageRelBiz extends BaseBiz<CustomerMessageRelMapper,CustomerMessageRel> {
}