package com.github.wxiaoqi.security.crm.core.rest;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherApplyRequest;
import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherRequest;
import com.github.wxiaoqi.security.common.crm.request.MerchantPayForAnotherBankParam;
import com.github.wxiaoqi.security.common.crm.request.MerchantPayForAnotherBankRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.ResponseHelper;
import com.github.wxiaoqi.security.crm.core.biz.MerchantBaseBiz;
import com.github.wxiaoqi.security.crm.core.biz.MerchantPayForAnotherBankBiz;
import com.github.wxiaoqi.security.crm.core.entity.MerchantBase;
import com.github.wxiaoqi.security.crm.core.entity.MerchantPayForAnotherBank;

/**
 * 商户提现银行卡信息
 * @author Administrator
 *
 */
@RestController
@RequestMapping("payForAnotherBank")
public class MerchantPayForAnotherBankController extends BaseController<MerchantPayForAnotherBankBiz,MerchantPayForAnotherBank> {
	
	private static final Logger logger = LoggerFactory.getLogger(MerchantPayForAnotherBankController.class);
	
	@Autowired
	private MerchantPayForAnotherBankBiz merchantPayForAnotherBankBiz;
	@Autowired
	private MerchantBaseBiz merchantBaseBiz;
	
	/**
	 * 代付申请
	 * @return
	 */
	@RequestMapping(value = "/payForAnotherApply", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> payForAnotherApply(@RequestBody @Valid PayForAnotherApplyRequest payForAnotherApplyRequest){
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("platformId",payForAnotherApplyRequest.getPlatformId());
		param.put("merchantId",payForAnotherApplyRequest.getMerchantId());
		MerchantBase queryMerchantBaseInfo = merchantBaseBiz.queryMerchantBaseInfo(param);
		if(null == queryMerchantBaseInfo){
			return ResponseHelper.success(null, null, ResponseCode.MER_NOTEXIST.getCode(), ResponseCode.MER_NOTEXIST.getMessage());
		}
		Map<String, Object> payForAnotherApply = merchantPayForAnotherBankBiz.payForAnotherApply(payForAnotherApplyRequest);
		if(ResponseCode.OK.getCode().equals(payForAnotherApply.get("code"))){
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else{
			return ResponseHelper.success(null, null, ResponseCode.PAY_FOR_ANOTHER_FAIL.getCode(), ResponseCode.PAY_FOR_ANOTHER_FAIL.getMessage());
		}
	}
	
	/**
	 * 代付
	 * @return
	 */
	@RequestMapping(value = "/payForAnother", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> payForAnother(@RequestBody @Valid PayForAnotherRequest payForAnotherRequest){
		Map<String, Object> payForAnother = merchantPayForAnotherBankBiz.payForAnother(payForAnotherRequest);
		if(ResponseCode.OK.getCode().equals(payForAnother.get("code"))){
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else{
			return ResponseHelper.success(null, null, ResponseCode.PAY_FOR_ANOTHER_FAIL.getCode(), ResponseCode.PAY_FOR_ANOTHER_FAIL.getMessage());
		}
	}
	
	/**
	 * 列表 
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/list", method=RequestMethod.POST)
	@ResponseBody
	public TableResultResponse<MerchantPayForAnotherBank> listAll(@RequestBody MerchantPayForAnotherBankRequest param) {
		logger.info("查询商户提现银行卡信息列表。。。{}"+param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return baseBiz.payForAnotherBankList(param);
	}
	
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/add", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> addInfo(@RequestBody MerchantPayForAnotherBankParam param) {
		logger.info("添加商户提现银行卡信息。。。{}"+param);
		Map<String, Object> respMap = baseBiz.addPayForAnotherBank(param);
		return ResponseHelper.success(respMap, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> deleteArea(@RequestBody MerchantPayForAnotherBankParam param) {
		MerchantPayForAnotherBank bank = baseBiz.deletePayForAnotherBank(param);
		if (bank != null) {
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
	}
	
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateArea(@RequestBody MerchantPayForAnotherBankParam param) {
		MerchantPayForAnotherBank bank = baseBiz.updatePayForAnotherBank(param);
		if (bank != null) {
			return ResponseHelper.success(bank, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
	}
	
	
}