package com.github.wxiaoqi.security.crm.core.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.MerchantAccountJournalBiz;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournal;

@RestController
@RequestMapping("merchantAccountJournal")
public class MerchantAccountJournalController extends BaseController<MerchantAccountJournalBiz,MerchantAccountJournal> {

}