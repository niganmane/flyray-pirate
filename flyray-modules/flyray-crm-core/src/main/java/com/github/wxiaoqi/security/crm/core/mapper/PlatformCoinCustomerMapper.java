package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.PlatformCoinCustomer;

import tk.mybatis.mapper.common.Mapper;

/**
 * 平台唯一用户表
 * @author he
 * @date 2018-09-08 14:08:33
 */
@org.apache.ibatis.annotations.Mapper
public interface PlatformCoinCustomerMapper extends Mapper<PlatformCoinCustomer> {
	
}
