package com.github.wxiaoqi.security.crm.core.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBillingBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBilling;

@RestController
@RequestMapping("personalBilling")
public class PersonalBillingController extends BaseController<PersonalBillingBiz,PersonalBilling> {

}