package com.github.wxiaoqi.security.crm.core.biz;

import java.math.BigDecimal;
import java.text.Bidi;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.common.crm.request.FreezeRequest;
import com.github.wxiaoqi.security.common.crm.request.UnFreezeRequest;
import com.github.wxiaoqi.security.common.enums.AccountStateEnums;
import com.github.wxiaoqi.security.common.enums.CustomerTypeEnums;
import com.github.wxiaoqi.security.common.enums.FreezeStateEnums;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.MD5;
import com.github.wxiaoqi.security.crm.core.entity.FreezeJournal;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccount;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournal;
import com.github.wxiaoqi.security.crm.core.entity.PersonalAccount;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBilling;
import com.github.wxiaoqi.security.crm.core.entity.UnfreezeJournal;
import com.github.wxiaoqi.security.crm.core.mapper.FreezeJournalMapper;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountJournalMapper;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalAccountMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBillingMapper;
import com.github.wxiaoqi.security.crm.core.mapper.UnfreezeJournalMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * 冻结、解冻相关的接口
 */
@Transactional(rollbackFor = Exception.class)
@Service
public class CommonFreezeUnFreezeBiz {
	
	private static final Logger log = LoggerFactory.getLogger(MerchantPayForAnotherBankBiz.class);
	@Autowired
	private PersonalBillingMapper personalBillingMapper;
	@Autowired
	private MerchantAccountJournalMapper merchantAccountJournalMapper;
	@Autowired
	private MerchantAccountMapper merchantAccountMapper;
	@Autowired
	private FreezeJournalMapper freezeJournalMapper;
	@Autowired
	private UnfreezeJournalMapper unfreezeJournalMapper;
	@Autowired
	private PersonalAccountMapper personalAccountMapper;
	@Value("${bountyHunter.balanceSaltValue}")
	private String balanceSaltValue;
	/**
	 * 冻结接口接口
	 * 若是商户 
	 * 1.商户账户余额扣除，冻结金额增加
	 * 2.新增冻结流水记录
	 * 若是个人
	 * 1.个人账户余额扣除，冻结金额增加
	 * 2.新增冻结流水记录
	 */
	public Map<String, Object> freeze(FreezeRequest freezeRequest) {
		log.info("冻结接口-请求参数：{}" + EntityUtils.beanToMap(freezeRequest));
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			if(freezeRequest.getCustomerType().equals(CustomerTypeEnums.mer_user.getCode())){
				MerchantAccount queryMerchantAccount=new MerchantAccount();
				queryMerchantAccount.setPlatformId(freezeRequest.getPlatformId());
				queryMerchantAccount.setMerchantId(freezeRequest.getMerchantId());
				queryMerchantAccount.setMerchantType(freezeRequest.getMerchantType());
				queryMerchantAccount.setAccountType(freezeRequest.getAccountType());
				MerchantAccount merchantAccount= merchantAccountMapper.selectOne(queryMerchantAccount);
				if(null!=merchantAccount&&merchantAccount.getStatus().equals(AccountStateEnums.normal.getCode())){
					if(!MD5.sign(merchantAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8").equals(merchantAccount.getCheckSum())){
						response.put("code", ResponseCode.MER_ACC_BALANCE_NOT_MATE.getCode());
						response.put("msg", ResponseCode.MER_ACC_BALANCE_NOT_MATE.getMessage());
						log.info("冻结接口 返回参数。。。。。。{}", response);
						return response;
					}
					//校验商户账户余额是否充足 和余额加密值的校验
					if(merchantAccount.getAccountBalance().compareTo(new BigDecimal(freezeRequest.getFreezeAmt()))<0){//商户账户余额不足
						response.put("code", ResponseCode.MER_ACC_BALANCE_NOT_ENOUGH.getCode());
						response.put("msg", ResponseCode.MER_ACC_BALANCE_NOT_ENOUGH.getMessage());
						log.info("冻结接口 返回参数。。。。。。{}", response);
						return response;
					}
					merchantAccount.setAccountBalance(merchantAccount.getAccountBalance().subtract(new BigDecimal(freezeRequest.getFreezeAmt())));
					merchantAccount.setCheckSum(MD5.sign(merchantAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8"));
					merchantAccount.setFreezeBalance(merchantAccount.getFreezeBalance().add(new BigDecimal(freezeRequest.getFreezeAmt())));
					merchantAccountMapper.updateByPrimaryKeySelective(merchantAccount);
				}else{
					log.info("冻结接口--商户账户不存在或者被冻结");
					response.put("code", ResponseCode.MER_ACC_NOTEXIST.getCode());
					response.put("msg", ResponseCode.MER_ACC_NOTEXIST.getMessage());
					log.info("冻结接口 返回参数。。。。。。{}", response);
					return response;
				}
			}
			if(freezeRequest.getCustomerType().equals(CustomerTypeEnums.per_user.getCode())){
				PersonalAccount queryPersonalAccount = new PersonalAccount();
				queryPersonalAccount.setPlatformId(freezeRequest.getPlatformId());
				queryPersonalAccount.setPersonalId(freezeRequest.getPersonalId());
				queryPersonalAccount.setAccountType(freezeRequest.getAccountType());
				PersonalAccount personalAccount = personalAccountMapper.selectOne(queryPersonalAccount);
				if(null!=personalAccount&&personalAccount.getStatus().equals(AccountStateEnums.normal.getCode())){
					if(!MD5.sign(personalAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8").equals(personalAccount.getCheckSum())){
						log.info("冻结接口--个人账户余额不匹配");
						response.put("code", ResponseCode.PER_ACC_BALANCE_NOT_MATE.getCode());
						response.put("msg", ResponseCode.PER_ACC_BALANCE_NOT_MATE.getMessage());
						log.info("冻结接口。。。。。。{}", response);
						return response;
					}
					if(personalAccount.getAccountBalance().compareTo(new BigDecimal(freezeRequest.getFreezeAmt()))<0){
						response.put("code", ResponseCode.PER_ACC_BALANCE_NOT_ENOUGH.getCode());
						response.put("msg", ResponseCode.PER_ACC_BALANCE_NOT_ENOUGH.getMessage());
						log.info("冻结接口 返回参数。。。。。。{}", response);
						return response;
					}
					personalAccount.setAccountBalance(personalAccount.getAccountBalance().subtract(new BigDecimal(freezeRequest.getFreezeAmt())));
					personalAccount.setCheckSum(MD5.sign(personalAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8"));
					personalAccount.setFreezeBalance(personalAccount.getFreezeBalance().add(new BigDecimal(freezeRequest.getFreezeAmt())));
					personalAccountMapper.updateByPrimaryKeySelective(personalAccount);
				}else{
					log.info("发起退款时 冻结资金--个人账户不存在或者被冻结");
					response.put("code", ResponseCode.PER_ACC_NOTEXIST.getCode());
					response.put("msg", ResponseCode.PER_ACC_NOTEXIST.getMessage());
					log.info("发起退款时 冻结资金 返回参数。。。。。。{}", response);
					return response;
				}
				
			}
			FreezeJournal freezeJournal=new FreezeJournal();
			String freezeId =String.valueOf(SnowFlake.getId());
			freezeJournal.setJournalId(freezeId);
			freezeJournal.setPlatformId(freezeRequest.getPlatformId());
			freezeJournal.setCustomerType(freezeRequest.getMerchantType());
			freezeJournal.setCustomerNo(freezeRequest.getMerchantId());
			freezeJournal.setTradeType(freezeRequest.getTradeType());//交易类型（支付:01，退款:02，提现:03，充值:04）
			freezeJournal.setOrderNo(freezeRequest.getOrderNo());
			freezeJournal.setFreezeBalance(new BigDecimal(freezeRequest.getFreezeAmt()));
			freezeJournal.setStatus(FreezeStateEnums.freeze.getCode());//冻结状态 1：已冻结  2：部分解冻  3：已解冻
			freezeJournal.setCreateTime(new Date());
			freezeJournalMapper.insertSelective(freezeJournal);
			response.put("code", ResponseCode.OK.getCode());
			response.put("msg", ResponseCode.OK.getMessage());
		} catch (Exception e1) {
			e1.printStackTrace();
			log.info("冻结接口 报错--"+e1.getMessage());
			response.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			response.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			return response;
		}
		return response;
	}
	
	/**
	 * 解冻接口
	 * 若是商户
	 * 1.商户账户冻结金额扣除，商户账户余额增加。 
	 * 2.新增解冻流水记录
	 * 若是个人
	 * 1.个人账户冻结金额扣除，个人账户余额增加。 
	 * 2.新增解冻流水记录
	 */
	public Map<String, Object> unfreeze(UnFreezeRequest unFreezeRequest) {
		log.info("退款失败时 解冻资金的接口 -请求参数：{}" + EntityUtils.beanToMap(unFreezeRequest));
		Map<String, Object> response = new HashMap<String, Object>();
		/**
		 * 根据冻结流水号查询是否被解冻
		 * */
		UnfreezeJournal queryUnfreezeJournal = new UnfreezeJournal();
		queryUnfreezeJournal.setFreezeId(unFreezeRequest.getFreezeId());
		queryUnfreezeJournal.setOrderNo(unFreezeRequest.getOrderNo());
		UnfreezeJournal unfreezeJournal=unfreezeJournalMapper.selectOne(queryUnfreezeJournal);
		if(null!=unfreezeJournal){
			log.info("解冻接口--冻结资金已解冻");
			response.put("code", ResponseCode.UNFREEZE_IS_EXIST.getCode());
			response.put("msg", ResponseCode.UNFREEZE_IS_EXIST.getMessage());
			return response;
		}
		try {
			if(unFreezeRequest.getCustomerType().equals(CustomerTypeEnums.mer_user.getCode())){
				MerchantAccount queryMerchantAccount=new MerchantAccount();
				queryMerchantAccount.setPlatformId(unFreezeRequest.getPlatformId());
				queryMerchantAccount.setMerchantId(unFreezeRequest.getMerchantId());
				queryMerchantAccount.setMerchantType(unFreezeRequest.getMerchantType());
				queryMerchantAccount.setAccountType(unFreezeRequest.getAccountType());
				MerchantAccount merchantAccount= merchantAccountMapper.selectOne(queryMerchantAccount);
				if(null!=merchantAccount&&merchantAccount.getStatus().equals(AccountStateEnums.normal.getCode())){
					if(!MD5.sign(merchantAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8").equals(merchantAccount.getCheckSum())){
						response.put("code", ResponseCode.MER_ACC_BALANCE_NOT_MATE.getCode());
						response.put("msg", ResponseCode.MER_ACC_BALANCE_NOT_MATE.getMessage());
						log.info("冻结接口 返回参数。。。。。。{}", response);
						return response;
					}
					//校验商户账户余额是否充足 和余额加密值的校验
					if(merchantAccount.getAccountBalance().compareTo(new BigDecimal(unFreezeRequest.getUnfreezeAmt()))<0){//商户账户余额不足
						response.put("code", ResponseCode.MER_ACC_BALANCE_NOT_ENOUGH.getCode());
						response.put("msg", ResponseCode.MER_ACC_BALANCE_NOT_ENOUGH.getMessage());
						log.info("冻结接口 返回参数。。。。。。{}", response);
						return response;
					}
					merchantAccount.setAccountBalance(queryMerchantAccount.getAccountBalance().add(new BigDecimal(unFreezeRequest.getUnfreezeAmt())));
					merchantAccount.setCheckSum(MD5.sign(merchantAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8"));
					merchantAccount.setFreezeBalance(queryMerchantAccount.getFreezeBalance().subtract(new BigDecimal(unFreezeRequest.getUnfreezeAmt())));
					merchantAccountMapper.updateByPrimaryKeySelective(merchantAccount);
				}else{
					log.info("冻结接口--商户账户不存在或者被冻结");
					response.put("code", ResponseCode.MER_ACC_NOTEXIST.getCode());
					response.put("msg", ResponseCode.MER_ACC_NOTEXIST.getMessage());
					log.info("冻结接口 返回参数。。。。。。{}", response);
					return response;
				}
			}
			if(unFreezeRequest.getCustomerType().equals(CustomerTypeEnums.per_user.getCode())){
				PersonalAccount queryPersonalAccount = new PersonalAccount();
				queryPersonalAccount.setPlatformId(unFreezeRequest.getPlatformId());
				queryPersonalAccount.setPersonalId(unFreezeRequest.getPersonalId());
				queryPersonalAccount.setAccountType(unFreezeRequest.getAccountType());
				PersonalAccount personalAccount = personalAccountMapper.selectOne(queryPersonalAccount);
				if(null!=personalAccount&&personalAccount.getStatus().equals(AccountStateEnums.normal.getCode())){
					if(!MD5.sign(personalAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8").equals(personalAccount.getCheckSum())){
						log.info("冻结接口--个人账户余额不匹配");
						response.put("code", ResponseCode.PER_ACC_BALANCE_NOT_MATE.getCode());
						response.put("msg", ResponseCode.PER_ACC_BALANCE_NOT_MATE.getMessage());
						log.info("冻结接口。。。。。。{}", response);
						return response;
					}
					
					personalAccount.setAccountBalance(personalAccount.getAccountBalance().add(new BigDecimal(unFreezeRequest.getUnfreezeAmt())));
					personalAccount.setCheckSum(MD5.sign(personalAccount.getAccountBalance().toString(), balanceSaltValue, "utf-8"));
					personalAccount.setFreezeBalance(personalAccount.getFreezeBalance().subtract(new BigDecimal(unFreezeRequest.getUnfreezeAmt())));
					personalAccountMapper.updateByPrimaryKeySelective(personalAccount);
				}else{
					log.info("发起退款时 冻结资金--个人账户不存在或者被冻结");
					response.put("code", ResponseCode.PER_ACC_NOTEXIST.getCode());
					response.put("msg", ResponseCode.PER_ACC_NOTEXIST.getMessage());
					log.info("发起退款时 冻结资金 返回参数。。。。。。{}", response);
					return response;
				}
				
			}
			queryUnfreezeJournal.setJournalId(String.valueOf(SnowFlake.getId()));
			queryUnfreezeJournal.setTradeType(unFreezeRequest.getTradeType());
			queryUnfreezeJournal.setUnfreezeBalance(new BigDecimal(unFreezeRequest.getUnfreezeAmt()));
			queryUnfreezeJournal.setCreateTime(new Date());
			unfreezeJournalMapper.insertSelective(queryUnfreezeJournal);
			response.put("code", ResponseCode.OK.getCode());
			response.put("msg", ResponseCode.OK.getMessage());
		} catch (Exception e1) {
			e1.printStackTrace();
			log.info("冻结接口 报错--"+e1.getMessage());
			response.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			response.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			return response;
		}
		return response;
		
	}
}
