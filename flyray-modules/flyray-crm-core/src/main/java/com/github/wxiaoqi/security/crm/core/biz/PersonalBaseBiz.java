package com.github.wxiaoqi.security.crm.core.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.PersonalBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.PersonalInfoRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryPersonalBaseListRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.common.util.SnowFlake;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBaseExt;
import com.github.wxiaoqi.security.crm.core.entity.PersonalDistributionRelation;
import com.github.wxiaoqi.security.crm.core.entity.PersonalInfo;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBaseExtMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBaseMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalDistributionRelationMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * 个人客户基础信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Slf4j
@Service
public class PersonalBaseBiz extends BaseBiz<PersonalBaseMapper,PersonalBase> {
	
	@Autowired
	private PersonalBaseExtMapper personalBaseExtMapper;
	@Autowired
	private PersonalDistributionRelationMapper personalDistributionRelationMapper;
	
	/**
	 * 修改用户信息
	 * @author centerroot
	 * @time 创建时间:2018年9月21日下午5:28:59
	 * @param personalInfoRequest
	 * @return
	 */
	public Map<String, Object> modify(PersonalInfoRequest personalInfoRequest){
		log.info("【修改用户信息】   请求参数：{}",EntityUtils.beanToMap(personalInfoRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PersonalBase personalBaseReq = new PersonalBase();
		PersonalBase personalBase = new PersonalBase();
		PersonalBaseExt personalBaseExt = new PersonalBaseExt();
		personalBaseReq.setPlatformId(personalInfoRequest.getPlatformId());
		personalBaseReq.setPersonalId(personalInfoRequest.getPersonalId());
		personalBaseReq = mapper.selectOne(personalBaseReq);
		log.info("【修改用户信息】   查询用户记录：{}",EntityUtils.beanToMap(personalBaseReq));
		if (personalBase != null) {
			BeanUtils.copyProperties(personalInfoRequest, personalBase);
			BeanUtils.copyProperties(personalInfoRequest, personalBaseExt);
			personalBase.setId(personalBaseReq.getId());
			log.info("【修改用户信息】   修改用户信息记录：{}",EntityUtils.beanToMap(personalBase));
			log.info("【修改用户信息】   修改用户扩展信息记录：{}",EntityUtils.beanToMap(personalBaseExt));
			mapper.updateByPrimaryKeySelective(personalBase);
			personalBaseExtMapper.updateByPrimaryKeySelective(personalBaseExt);
	        respMap.put("code", ResponseCode.OK.getCode());
	        respMap.put("msg", ResponseCode.OK.getMessage());
		} else {
			respMap.put("code", ResponseCode.PER_NOTEXIST.getCode());
	        respMap.put("msg", ResponseCode.PER_NOTEXIST.getMessage());
		}
		log.info("【修改用户信息】   响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 查询用户信息
	 * @author centerroot
	 * @time 创建时间:2018年9月21日下午5:28:59
	 * @param personalInfoRequest
	 * @return
	 */
	public Map<String, Object> queryInfo(PersonalInfoRequest personalInfoRequest){
		log.info("【查询用户信息】   请求参数：{}",EntityUtils.beanToMap(personalInfoRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PersonalBase personalBaseReq = new PersonalBase();
		BeanUtils.copyProperties(personalInfoRequest, personalBaseReq);
		
		List<PersonalBase> personalBases = mapper.select(personalBaseReq);
		if (null != personalBases && personalBases.size() > 0) {
			PersonalBase personalBase = personalBases.get(0);
			PersonalBaseExt personalBaseExt = personalBaseExtMapper.selectByPrimaryKey(personalBase.getPersonalId());
			respMap.put("personalBase", personalBase);
			respMap.put("personalBaseExt", personalBaseExt);
			respMap.put("code", ResponseCode.OK.getCode());
	        respMap.put("msg", ResponseCode.OK.getMessage());
		} else {
			respMap.put("code", ResponseCode.PER_NOTEXIST.getCode());
	        respMap.put("msg", ResponseCode.PER_NOTEXIST.getMessage());
		}
        
		log.info("【查询用户信息】   响应参数：{}",respMap);
		return respMap;
	}
	/**
	 * 用户信息列表查询
	 * @author centerroot
	 * @time 创建时间:2018年9月21日下午5:28:59
	 * @param personalInfoRequest
	 * @return
	 */
	public Map<String, Object> queryInfoList(PersonalInfoRequest personalInfoRequest){
		log.info("【用户信息列表查询】   请求参数：{}",EntityUtils.beanToMap(personalInfoRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("platformId", personalInfoRequest.getPlatformId());
		reqMap.put("personalId", personalInfoRequest.getPersonalId());
		reqMap.put("customerId", personalInfoRequest.getCustomerId());
		reqMap.put("realName", personalInfoRequest.getRealName());
		reqMap.put("authenticationStatus", personalInfoRequest.getAuthenticationStatus());
		reqMap.put("status", personalInfoRequest.getStatus());
		
		List<PersonalInfo> personalInfos = mapper.queryPersonalList(reqMap);
		
		respMap.put("personalInfos", personalInfos);
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【用户信息列表查询】   响应参数：{}",respMap);
		return respMap;
	}
	
	

	/**
	 * 查询个人基础信息列表
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:38
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	public Map<String, Object> queryList(QueryPersonalBaseListRequest queryPersonalBaseListRequest){
		log.info("【查询个人基础信息列表】   请求参数：{}",EntityUtils.beanToMap(queryPersonalBaseListRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String platformId = queryPersonalBaseListRequest.getPlatformId();
		String personalId = queryPersonalBaseListRequest.getPersonalId();
		String customerId = queryPersonalBaseListRequest.getCustomerId();
		String realName = queryPersonalBaseListRequest.getRealName();
		String authenticationStatus = queryPersonalBaseListRequest.getAuthenticationStatus();
		String status = queryPersonalBaseListRequest.getStatus();
		int page = queryPersonalBaseListRequest.getPage();
		int limit = queryPersonalBaseListRequest.getLimit();
		Map<String, Object> params = new HashMap<String, Object>();
	    params.put("page", page);
	    params.put("limit", limit);
	    Query query = new Query(params);
	    Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		

		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("platformId", platformId);
		reqMap.put("personalId", personalId);
		reqMap.put("customerId", customerId);
		reqMap.put("realName", realName);
		reqMap.put("authenticationStatus", authenticationStatus);
		reqMap.put("status", status);
		
        List<PersonalInfo> list = mapper.queryPersonalList(reqMap);
        TableResultResponse<PersonalInfo> table = new TableResultResponse<PersonalInfo>(result.getTotal(), list);
        
        respMap.put("body", table);
        respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【查询个人基础信息列表】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 添加个人基础信息
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:52
	 * @param personalBaseRequest
	 * @return
	 */
	public Map<String, Object> add(PersonalBaseRequest personalBaseRequest){
		log.info("【添加个人基础信息】   请求参数：{}",EntityUtils.beanToMap(personalBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PersonalBase personalBase = new PersonalBase();
		PersonalBaseExt personalBaseExt = new PersonalBaseExt();
		BeanUtils.copyProperties(personalBaseRequest,personalBase);
		BeanUtils.copyProperties(personalBaseRequest,personalBaseExt);
		String personalId = String.valueOf(SnowFlake.getId());
		personalBase.setPersonalId(personalId);
		personalBase.setAuthenticationStatus("00");
		personalBase.setStatus("00");
		mapper.insert(personalBase);
		
		personalBaseExt.setPersonalId(personalId);
		personalBaseExtMapper.insert(personalBaseExt);
		
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【添加个人基础信息】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 查询单个个人客户信息
	 * @author centerroot
	 * @time 创建时间:2018年8月13日下午5:16:08
	 * @param personalBaseRequest
	 * @return
	 */
	public Map<String, Object> getOneObj(String personalId){
		log.info("【查询单个个人客户信息】   请求参数：personalId:{}",personalId);
		Map<String, Object> respMap = new HashMap<String, Object>();
		Map<String, Object> reqMap = new HashMap<String, Object>();
		reqMap.put("personalId", personalId);
        List<PersonalInfo> list = mapper.queryPersonalList(reqMap);
		if (list != null && list.size() > 0) {
			PersonalInfo personalInfo = list.get(0);
			respMap.put("personalInfo", personalInfo);
			respMap.put("code", ResponseCode.OK.getCode());
	        respMap.put("msg", ResponseCode.OK.getMessage());
		} else {
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
	        respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		
		log.info("【查询单个个人客户信息】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 删除个人客户信息
	 * @author centerroot
	 * @time 创建时间:2018年8月13日下午5:27:03
	 * @param personalBaseReq
	 * @return
	 */
	public Map<String, Object> removeObj(String personalId){
		log.info("【删除个人客户信息】   请求参数：personalId:{}",personalId);
		Map<String, Object> respMap = new HashMap<String, Object>();
		PersonalBase personalBase = new PersonalBase();
		PersonalBaseExt personalBaseExt = new PersonalBaseExt();
		personalBase.setPersonalId(personalId);
		personalBaseExt.setPersonalId(personalId);
		mapper.delete(personalBase);
		personalBaseExtMapper.delete(personalBaseExt);
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【删除个人客户信息】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 更新个人客户信息
	 * @author centerroot
	 * @time 创建时间:2018年8月13日下午5:34:13
	 * @param personalBaseReq
	 * @return
	 */
	public Map<String, Object> updateObj(PersonalBaseRequest personalBaseRequest){
		log.info("【更新个人客户信息】   请求参数：{}",EntityUtils.beanToMap(personalBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		
		PersonalBase personalBase = new PersonalBase();
		PersonalBaseExt personalBaseExt = new PersonalBaseExt();
		BeanUtils.copyProperties(personalBaseRequest,personalBase);
		BeanUtils.copyProperties(personalBaseRequest,personalBaseExt);
		
		mapper.updateByPrimaryKeySelective(personalBase);
		personalBaseExtMapper.updateByPrimaryKeySelective(personalBaseExt);
		
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【更新个人客户信息】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 查询个人信息关系网
	 * 1、入参  personalid
	 * 2、查询父级个人信息
	 * 3、查询个人一级个人信息列表
	 * @author centerroot
	 * @time 创建时间:2018年8月28日上午9:54:09
	 * @param personalId
	 * @return
	 */
	public Map<String, Object> queryPeopleNetwork(PersonalBaseRequest personalBaseRequest){
		log.info("【查询个人信息关系网】   请求参数：{}",EntityUtils.beanToMap(personalBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		
		PersonalDistributionRelation personalDistributionRelationReq = new PersonalDistributionRelation();
		personalDistributionRelationReq.setPlatformId(personalBaseRequest.getPlatformId());
		personalDistributionRelationReq.setPersonalId(personalBaseRequest.getPersonalId());
		personalDistributionRelationReq.setParentLevel(1);
		PersonalDistributionRelation personalDistributionRelation = personalDistributionRelationMapper.selectOne(personalDistributionRelationReq);
		if (null != personalDistributionRelation) {
			log.info("【查询个人信息关系网】   查询父级关系信息：{}",EntityUtils.beanToMap(personalDistributionRelation));
			PersonalBase personalBaseReq = new PersonalBase();
			personalBaseReq.setPlatformId(personalDistributionRelation.getPlatformId());
			personalBaseReq.setPersonalId(personalDistributionRelation.getParentPerId());
			PersonalBase parentPersonalBase = mapper.selectOne(personalBaseReq);
			log.info("【查询个人信息关系网】   查询父级个人信息：{}",EntityUtils.beanToMap(parentPersonalBase));
			respMap.put("parentPersonalBase", parentPersonalBase);
		}
		
		Map<String, Object> reqChild = new HashMap<>();
		reqChild.put("platformId", personalBaseRequest.getPlatformId());
		reqChild.put("personalId", personalBaseRequest.getPersonalId());
		List<PersonalBase> childPersonals = mapper.queryPersonalNetwork(reqChild);
		log.info("【查询个人信息关系网】   查询子级个人信息：{}",EntityUtils.beanToMap(childPersonals));
		respMap.put("childPersonals", childPersonals);
		
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【查询个人信息关系网】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 小程序用户新增
	 * @param request
	 * @return
	 */
	public Map<String, Object> wechatMiniProgramAdd(Map<String, Object> request){
		log.info("【小程序用户新增】   请求参数：{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String customerId = (String) request.get("customerId");
		String avatar = (String) request.get("avatar");
		String nickName = (String) request.get("nickName");
		PersonalBase personalBase = new PersonalBase();
		personalBase.setPlatformId(platformId);
		personalBase.setCustomerId(customerId);
		PersonalBase selectPersonalBase = mapper.selectOne(personalBase);
		if(null == selectPersonalBase){
			personalBase.setPersonalId(String.valueOf(SnowFlake.getId()));
			personalBase.setAvatar(avatar);
			personalBase.setNickName(nickName);
			mapper.insert(personalBase);
		}else{
			selectPersonalBase.setAvatar(avatar);
			selectPersonalBase.setNickName(nickName);
			mapper.updateByPrimaryKey(selectPersonalBase);
		}
		response.put("code", ResponseCode.OK.getCode());
		response.put("msg", ResponseCode.OK.getMessage());
		log.info("【小程序用户新增】   响应参数：{}", response);
		return response;
	}
	
	/**
	 * 小程序用户修改
	 * @param request
	 * @return
	 */
	public Map<String, Object> wechatMiniProgramUpdate(Map<String, Object> request){
		log.info("【小程序用户修改】   请求参数：{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		String platformId = (String) request.get("platformId");
		String customerId = (String) request.get("customerId");
		String avatar = (String) request.get("avatar");
		String nickName = (String) request.get("nickName");
		PersonalBase personalBase = new PersonalBase();
		personalBase.setPlatformId(platformId);
		personalBase.setCustomerId(customerId);
		PersonalBase selectPersonalBase = mapper.selectOne(personalBase);
		if(null == selectPersonalBase){
			response.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			response.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
		}else{
			selectPersonalBase.setAvatar(avatar);
			selectPersonalBase.setNickName(nickName);
			mapper.updateByPrimaryKey(selectPersonalBase);
			response.put("code", ResponseCode.OK.getCode());
			response.put("msg", ResponseCode.OK.getMessage());
		}
		log.info("【小程序用户修改响应】   响应参数：{}", response);
		return response;
	}
	
	
	public Map<String, Object> savePersonalDistributionRelation(Map<String, Object> request){ 
		log.info("【小程序用户修改】   请求参数：{}", request);
		Map<String, Object> response = new HashMap<String, Object>();
		
		
		return response;
	}
	
	
}