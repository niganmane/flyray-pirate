package com.github.wxiaoqi.security.crm.core.rest;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseExtBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBaseExt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("personalBaseExt")
public class PersonalBaseExtController extends BaseController<PersonalBaseExtBiz,PersonalBaseExt> {

}