package com.github.wxiaoqi.security.crm.core.blockchain.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 交易输入
 * 既然不需要UTXO,直接使用余额，所以可以只保存出账方地址和数额，其他参数参照platform_coin_transaction_from
 * 表中参数 交易hash因为是计算后生成，所以不放在里面
 */
@Data
public class DposTXInput {
    /**
     * 出账方地址
     */
    private String address;

    /**
     * 出账数额
     */
    private double amount;

    public DposTXInput() {
    }

    public DposTXInput(String address, double amount) {
        this.address = address;
        this.amount = amount;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

}
