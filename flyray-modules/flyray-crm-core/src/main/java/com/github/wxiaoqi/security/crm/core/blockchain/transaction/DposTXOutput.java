package com.github.wxiaoqi.security.crm.core.blockchain.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 交易输出  不需要UTXO，所以也不需要解锁难题，直接入账。所以只保存 入账方地址和数额
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DposTXOutput {
    /**
     * 入账数额
     */
    private double amount;
    /**
     * 入账方地址
     */
    private String address;


    public static DposTXOutput newDposTXOutput(double value, String address) {
       return new DposTXOutput(value,address);

    }

}
