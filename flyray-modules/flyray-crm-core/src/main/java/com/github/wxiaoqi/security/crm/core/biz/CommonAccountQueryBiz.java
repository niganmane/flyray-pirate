package com.github.wxiaoqi.security.crm.core.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.common.crm.request.AccountJournalQueryRequest;
import com.github.wxiaoqi.security.common.crm.request.AccountQueryRequest;
import com.github.wxiaoqi.security.common.entity.MerchantAccountEntity;
import com.github.wxiaoqi.security.common.entity.PersonalAccountEntity;
import com.github.wxiaoqi.security.common.enums.CustomerTypeEnums;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccount;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccountJournal;
import com.github.wxiaoqi.security.crm.core.entity.PersonalAccount;
import com.github.wxiaoqi.security.crm.core.entity.PersonalAccountJournal;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountJournalMapper;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalAccountJournalMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalAccountMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * 个人或者商户账户信息查询
 */
@Slf4j
@Transactional(rollbackFor = Exception.class)
@Service
public class CommonAccountQueryBiz {
	@Autowired
	private PersonalAccountMapper personalAccountMapper;
	@Autowired
	private PersonalAccountJournalMapper personalAccountJournalMapper;

	@Autowired
	private MerchantAccountMapper merchantAccountMapper;
	@Autowired
	private MerchantAccountJournalMapper merchantAccountJournalMapper;
	
	@Value("${bountyHunter.balanceSaltValue}")
	private String balanceSaltValue;
	/**
	 * 账户查询接口
	 */
	public Map<String, Object> accountQuery(AccountQueryRequest accountQueryRequest) {
		log.info("个人或者商户账户信息查询-请求参数：{}" + EntityUtils.beanToMap(accountQueryRequest));
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			if(CustomerTypeEnums.mer_user.getCode().equals(accountQueryRequest.getCustomerType())){//商户账户
				MerchantAccount queryMerAccountparam=new MerchantAccount();
				queryMerAccountparam.setMerchantId(accountQueryRequest.getMerchantId());
				if(!StringUtils.isEmpty(accountQueryRequest.getMerchantType())){
					queryMerAccountparam.setMerchantType(accountQueryRequest.getMerchantType());
				}
				if(!StringUtils.isEmpty(accountQueryRequest.getAccountType())){
					/**
					 * 获取指定账户类型的商户账户信息
					 * */
					queryMerAccountparam.setAccountType(accountQueryRequest.getAccountType());
					MerchantAccount merchantAccount=merchantAccountMapper.selectOne(queryMerAccountparam);
					MerchantAccountEntity merchantAccountEntity = new MerchantAccountEntity();
					BeanUtils.copyProperties(merchantAccount, merchantAccountEntity);
					response.put("accountInfo",merchantAccountEntity);
				}else{
					/***
					 * 获取全部的商户账户信息
					 * */
					List<MerchantAccount>merchantAccounts=merchantAccountMapper.select(queryMerAccountparam);
					response.put("accountInfo",merchantAccounts);
				}
				
			}
			if(CustomerTypeEnums.per_user.getCode().equals(accountQueryRequest.getCustomerType())){//个人账户
				PersonalAccount queryPerAccountparam=new PersonalAccount();
				queryPerAccountparam.setPlatformId(accountQueryRequest.getPlatformId());
				queryPerAccountparam.setPersonalId(accountQueryRequest.getPersonalId());
				if(!StringUtils.isEmpty(accountQueryRequest.getAccountType())){
					queryPerAccountparam.setAccountType(accountQueryRequest.getAccountType());
					PersonalAccount personalAccount=personalAccountMapper.selectOne(queryPerAccountparam);
					PersonalAccountEntity personalAccountEntity = new PersonalAccountEntity();
					BeanUtils.copyProperties(personalAccount, personalAccountEntity);
					response.put("accountInfo",personalAccountEntity);
				}else{
					List<PersonalAccount>personalAccounts=personalAccountMapper.select(queryPerAccountparam);
					response.put("accountInfo",personalAccounts);
				}
			}
			response.put("code", ResponseCode.OK.getCode());
			response.put("msg", ResponseCode.OK.getMessage());
			log.info("个人或者商户账户信息查询 返回参数。。。。。。{}", response);
		} catch (Exception e) {
			e.printStackTrace();
			log.info("个人或者商户账户信息查询  报错--"+e.getMessage());
			response.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			response.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			return response;
		}
		return response;
	}
	
	/**
	 * 查询账户交易流水
	 * @author centerroot
	 * @time 创建时间:2018年9月27日下午3:57:14
	 * @param accountJournalQueryRequest
	 * @return
	 */
	public Map<String, Object> accountJournalQuery(AccountJournalQueryRequest accountJournalQueryRequest) {
		log.info("个人或者商户查询账户交易流水-请求参数：{}" + EntityUtils.beanToMap(accountJournalQueryRequest));
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			if(CustomerTypeEnums.mer_user.getCode().equals(accountJournalQueryRequest.getCustomerType())){//商户账户
				MerchantAccountJournal merchantAccountJournalReq = new MerchantAccountJournal();
				merchantAccountJournalReq.setPlatformId(accountJournalQueryRequest.getPlatformId());
				merchantAccountJournalReq.setMerchantId(accountJournalQueryRequest.getMerchantId());
				merchantAccountJournalReq.setAccountType(accountJournalQueryRequest.getAccountType());
				List<MerchantAccountJournal> merchantAccountJournals = merchantAccountJournalMapper.select(merchantAccountJournalReq);
				response.put("accountJournalInfo",merchantAccountJournals);
			}
			if(CustomerTypeEnums.per_user.getCode().equals(accountJournalQueryRequest.getCustomerType())){//个人账户
				PersonalAccountJournal personalAccountJournalReq = new PersonalAccountJournal();
				personalAccountJournalReq.setPlatformId(accountJournalQueryRequest.getPlatformId());
				personalAccountJournalReq.setPersonalId(accountJournalQueryRequest.getPersonalId());
				personalAccountJournalReq.setAccountType(accountJournalQueryRequest.getAccountType());
				List<PersonalAccountJournal> personalAccountJournals = personalAccountJournalMapper.select(personalAccountJournalReq);
				response.put("accountJournalInfo",personalAccountJournals);
			}
			response.put("code", ResponseCode.OK.getCode());
			response.put("msg", ResponseCode.OK.getMessage());
			log.info("个人或者商户查询账户交易流水 返回参数。。。。。。{}", response);
		} catch (Exception e) {
			e.printStackTrace();
			log.info("个人或者商户查询账户交易流水  报错--"+e.getMessage());
			response.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			response.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			return response;
		}
		return response;
	}
	
}
