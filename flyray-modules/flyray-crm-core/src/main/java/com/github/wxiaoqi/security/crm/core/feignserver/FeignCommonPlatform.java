package com.github.wxiaoqi.security.crm.core.feignserver;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.entity.PlatformSafetyConfigCommon;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.crm.core.biz.PlatformSafetyConfigBiz;
import com.github.wxiaoqi.security.crm.core.entity.PlatformSafetyConfig;

@Controller
@RequestMapping("feign/platform")
public class FeignCommonPlatform {
	
	@Autowired
	private PlatformSafetyConfigBiz platformSafetyConfigBiz;

	@RequestMapping(value = "queryPlatformInfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryPlatformInfo(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
    	try {
    		PlatformSafetyConfig platformSafetyConfigReq = new PlatformSafetyConfig();
    		platformSafetyConfigReq.setAppId((String) param.get("appid"));
    		PlatformSafetyConfig platformSafetyConfig = platformSafetyConfigBiz.selectOne(platformSafetyConfigReq);
    		PlatformSafetyConfigCommon platformSafetyConfigCommon = new PlatformSafetyConfigCommon();
    		BeanUtils.copyProperties(platformSafetyConfig, platformSafetyConfigCommon);
    		
    		if(null != platformSafetyConfig){
    			result.put("platformSafetyConfig", platformSafetyConfigCommon);
    			result.put("code", ResponseCode.OK.getCode());
    			result.put("msg", ResponseCode.OK.getMessage());
    			result.put("success", true);
    		}else{
    			result.put("code", ResponseCode.PLATFORM_SAFETY_CONFIG_NO_EXIST.getCode());
    			result.put("msg", ResponseCode.PLATFORM_SAFETY_CONFIG_NO_EXIST.getMessage());
    			result.put("success", false);
    		}
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			result.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			result.put("success", false);
		}
    	return result;
    }
	
	
}
