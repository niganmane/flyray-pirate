package com.github.wxiaoqi.security.crm.core.feignserver;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.BaseAreaRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.ResponseHelper;
import com.github.wxiaoqi.security.crm.core.biz.BaseAreaBiz;
import com.github.wxiaoqi.security.crm.core.entity.BaseArea;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * 地区
 * */
@Api(tags="地区详情")
@Controller
@RequestMapping("feign/baseArea")
public class FeignBaseArea {
	
	@Autowired
	private BaseAreaBiz areaBiz;
	
	/**
	 * 地区详情查询
	 * @author chj
	 * @return
	 */
	@ApiOperation("地区详情查询")
	@RequestMapping(value = "/queryInfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> accountQuery(@RequestBody BaseAreaRequestParam param){
		BaseArea area = areaBiz.QueryAreaInfo(param);
		Map<String, Object> respMap = new HashMap<>();
		if (area != null) {
			respMap.put("areaCode", area.getAreaCode());
			respMap.put("layer", area.getLayer());
			respMap.put("name", area.getName());
		}
		return respMap;
    }
	
	
}
