package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.FreezeJournal;
import tk.mybatis.mapper.common.Mapper;

/**
 * 冻结流水表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface FreezeJournalMapper extends Mapper<FreezeJournal> {
	
}
