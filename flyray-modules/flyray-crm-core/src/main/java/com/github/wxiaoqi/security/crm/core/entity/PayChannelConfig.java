package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 平台（商户）支付通道配置
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:50
 */
@Table(name = "pay_channel_config")
public class PayChannelConfig extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //商户号
    @Column(name = "merchant_id")
    private String merchantId;
	
	    //支付通道名称
    @Column(name = "pay_channel_name")
    private String payChannelName;
    
    //支付通道名称编号
    @Column(name = "pay_channel_no")
    private String payChannelNo;
    
    //支付公司编号
    @Column(name = "pay_company_no")
    private String payCompanyNo;
    
    //渠道 01APP 02小程序 03PC
    @Column(name = "channel")
    private String channel;
	
	    //客户类型    CUST00：平台   CUST01：商户  CUST02：用户
    @Column(name = "merchant_type")
    private String merchantType;
	
	    //费率代码
    @Column(name = "fee_code")
    private String feeCode;
	
	    //第三方商户号 APPID
    @Column(name = "out_mer_no")
    private String outMerNo;
	
	    //第三方子商户号
    @Column(name = "out_sub_mer_no")
    private String outSubMerNo;
	
	    //第三方账号
    @Column(name = "out_mer_account")
    private String outMerAccount;
	
	    //第三方公钥
    @Column(name = "out_mer_public_key")
    private String outMerPublicKey;
	
	    //第三方私钥
    @Column(name = "out_mer_private_key")
    private String outMerPrivateKey;
	
	    //加密方式 MD5 RSA
    @Column(name = "encryption_method")
    private String encryptionMethod;
	
	    //证书地址
    @Column(name = "key_path")
    private String keyPath;
	
	    //证书密码
    @Column(name = "key_pwd")
    private String keyPwd;
	
	    //状态  0：开启   1：关闭
    @Column(name = "status")
    private String status;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	

	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 获取：商户号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：支付通道编号
	 */
	public void setPayChannelNo(String payChannelNo) {
		this.payChannelNo = payChannelNo;
	}
	/**
	 * 获取：支付通道编号
	 */
	public String getPayChannelNo() {
		return payChannelNo;
	}
	/**
	 * 设置：客户类型    CUST00：平台   CUST01：商户  CUST02：用户
	 */
	public String getMerchantType() {
		return merchantType;
	}
	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}
	/**
	 * 设置：费率代码
	 */
	public void setFeeCode(String feeCode) {
		this.feeCode = feeCode;
	}
	
	/**
	 * 获取：费率代码
	 */
	public String getFeeCode() {
		return feeCode;
	}
	/**
	 * 设置：第三方商户号 APPID
	 */
	public void setOutMerNo(String outMerNo) {
		this.outMerNo = outMerNo;
	}
	/**
	 * 获取：第三方商户号 APPID
	 */
	public String getOutMerNo() {
		return outMerNo;
	}
	/**
	 * 设置：第三方子商户号
	 */
	public void setOutSubMerNo(String outSubMerNo) {
		this.outSubMerNo = outSubMerNo;
	}
	/**
	 * 获取：第三方子商户号
	 */
	public String getOutSubMerNo() {
		return outSubMerNo;
	}
	/**
	 * 设置：第三方账号
	 */
	public void setOutMerAccount(String outMerAccount) {
		this.outMerAccount = outMerAccount;
	}
	/**
	 * 获取：第三方账号
	 */
	public String getOutMerAccount() {
		return outMerAccount;
	}
	/**
	 * 设置：第三方公钥
	 */
	public void setOutMerPublicKey(String outMerPublicKey) {
		this.outMerPublicKey = outMerPublicKey;
	}
	/**
	 * 获取：第三方公钥
	 */
	public String getOutMerPublicKey() {
		return outMerPublicKey;
	}
	/**
	 * 设置：第三方私钥
	 */
	public void setOutMerPrivateKey(String outMerPrivateKey) {
		this.outMerPrivateKey = outMerPrivateKey;
	}
	/**
	 * 获取：第三方私钥
	 */
	public String getOutMerPrivateKey() {
		return outMerPrivateKey;
	}
	/**
	 * 设置：加密方式 MD5 RSA
	 */
	public void setEncryptionMethod(String encryptionMethod) {
		this.encryptionMethod = encryptionMethod;
	}
	/**
	 * 获取：加密方式 MD5 RSA
	 */
	public String getEncryptionMethod() {
		return encryptionMethod;
	}
	/**
	 * 设置：证书地址
	 */
	public void setKeyPath(String keyPath) {
		this.keyPath = keyPath;
	}
	/**
	 * 获取：证书地址
	 */
	public String getKeyPath() {
		return keyPath;
	}
	/**
	 * 设置：证书密码
	 */
	public void setKeyPwd(String keyPwd) {
		this.keyPwd = keyPwd;
	}
	/**
	 * 获取：证书密码
	 */
	public String getKeyPwd() {
		return keyPwd;
	}
	/**
	 * 设置：状态  0：开启   1：关闭
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：状态  0：开启   1：关闭
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	public String getPayCompanyNo() {
		return payCompanyNo;
	}
	public void setPayCompanyNo(String payCompanyNo) {
		this.payCompanyNo = payCompanyNo;
	}
	public String getPayChannelName() {
		return payChannelName;
	}
	public void setPayChannelName(String payChannelName) {
		this.payChannelName = payChannelName;
	}
	
}
