package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.PayChannelConfig;
import tk.mybatis.mapper.common.Mapper;

/**
 * 平台（商户）支付通道配置
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:50
 */
@org.apache.ibatis.annotations.Mapper
public interface PayChannelConfigMapper extends Mapper<PayChannelConfig> {
	
	
	List<Map<String, Object>> queryPayMethod(Map<String, Object> map);
}
