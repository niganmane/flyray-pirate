package com.github.wxiaoqi.security.crm.core.api;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.CertificationRequest;
import com.github.wxiaoqi.security.common.crm.request.CustomerRealNameRequest;
import com.github.wxiaoqi.security.common.crm.request.CustomerSetupPasswordRequest;
import com.github.wxiaoqi.security.common.crm.request.CustomerUpdatePasswordRequest;
import com.github.wxiaoqi.security.common.crm.request.CustomerVerifyPasswordRequest;
import com.github.wxiaoqi.security.common.crm.request.LoginRequest;
import com.github.wxiaoqi.security.common.crm.request.RegisterRequestParam;
import com.github.wxiaoqi.security.crm.core.biz.CustomerBaseBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalCertificationInfoBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="客户基础信息管理")
@Controller
@RequestMapping("customer")
public class CustomerController {

	@Autowired
	private CustomerBaseBiz customerBaseBiz;
	@Autowired
	private PersonalCertificationInfoBiz personalCertificationInfoBiz;
	/**
	 * 用户注册
	 * @author centerroot
	 * @time 创建时间:2018年9月7日下午5:00:32
	 * @param personalBaseRequest
	 * @return
	 */
	@ApiOperation("用户注册")
	@RequestMapping(value = "/register",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> register(@RequestBody @Valid RegisterRequestParam registerRequest) {
        Map<String, Object> response = customerBaseBiz.register(registerRequest);
		return response;
    }
	
	/**
	 * 用户登录
	 * @author centerroot
	 * @time 创建时间:2018年9月7日下午5:00:32
	 * @param loginRequest
	 * @return
	 */
	@ApiOperation("用户登录")
	@RequestMapping(value = "/login",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> login(@RequestBody @Valid LoginRequest loginRequest) {
        Map<String, Object> response = customerBaseBiz.login(loginRequest);
		return response;
    }
	
	/**
	 * 用户实名认证
	 * */
	@ApiOperation("用户实名认证")
	@RequestMapping(value = "/certification",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> certification(@RequestBody @Valid CertificationRequest certificationRequest) {
        Map<String, Object> response = personalCertificationInfoBiz.certification(certificationRequest);
		return response;
    }
	
	/**
	 * realNameQuery 实名认证查询接口
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午9:16:55
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/realNameQuery", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> realNameQuery(@RequestBody @Valid CertificationRequest certificationRequest) {
		return personalCertificationInfoBiz.realNameQuery(certificationRequest);
	}
	
	/**
	 * setupPayPassword 设置/重置交易密码
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午10:54:44
	 * @param customerSetupPasswordRequest
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setupPayPassword", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> setupPayPassword(@RequestBody @Valid CustomerSetupPasswordRequest customerSetupPasswordRequest) {
		return customerBaseBiz.setupPayPassword(customerSetupPasswordRequest);
	}

	/**
	 * verifyPayPassword 校验交易密码
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午10:54:37
	 * @param customerVerifyPasswordRequest
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/verifyPayPassword", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> verifyPayPassword(@RequestBody @Valid CustomerVerifyPasswordRequest customerVerifyPasswordRequest) {
		return customerBaseBiz.verifyPayPassword(customerVerifyPasswordRequest);
	}

	/**
	 * updatePayPassword 修改交易密码
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午10:54:30
	 * @param customerUpdatePasswordRequest
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePayPassword", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updatePayPassword(@RequestBody @Valid CustomerUpdatePasswordRequest customerUpdatePasswordRequest) {
		return customerBaseBiz.updatePayPassword(customerUpdatePasswordRequest);
	}
	/**
	 * 会员实名信息列表查询
	 * @author centerroot
	 * @time 创建时间:2018年9月25日下午4:08:01
	 * @param customerRealNameRequest
	 * @return
	 */
	@RequestMapping(value = "/queryCustRealNameList", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryCustRealNameList(@RequestBody @Valid CustomerRealNameRequest customerRealNameRequest) {
		return customerBaseBiz.queryCustRealNameList(customerRealNameRequest);
	}

}
