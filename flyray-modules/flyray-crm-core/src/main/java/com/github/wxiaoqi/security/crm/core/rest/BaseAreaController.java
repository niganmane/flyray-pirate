package com.github.wxiaoqi.security.crm.core.rest;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.crm.request.BaseAreaRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.ResponseHelper;
import com.github.wxiaoqi.security.crm.core.biz.BaseAreaBiz;
import com.github.wxiaoqi.security.crm.core.entity.BaseArea;
import com.github.wxiaoqi.security.crm.core.vo.AreaTree;

/**
 * 行政区域
 * @author Administrator
 *
 */
@RestController
@RequestMapping("baseArea")
public class BaseAreaController extends BaseController<BaseAreaBiz,BaseArea> {
	
	private static final Logger logger = LoggerFactory.getLogger(BaseAreaController.class);
	
	/**
	 * 查询树形目录
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/tree", method=RequestMethod.GET)
	@ResponseBody
	public List<AreaTree> select(@RequestParam Map<String, Object> param) {
		logger.info("查询行政区域树。。。{}"+param);
		String areaCode = (String) param.get("areaCode");
		return baseBiz.listAreaTree(areaCode);
	}
	
	
	/**
	 * 区域列表
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/list", method=RequestMethod.GET)
	@ResponseBody
	public List<BaseArea> listAll(@RequestParam Map<String, Object> param) {
		logger.info("查询行政区域列表。。。{}"+param);
		return baseBiz.listAreaByParentCode(param);
	}
	
	
	/**
	 * 添加区域
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/add", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> addArea(@RequestBody BaseAreaRequestParam param) {
		BaseArea area = baseBiz.addBaseArea(param);
		if (area == null) {
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_EXIST.getCode(), ResponseCode.FUNCTION_EXIST.getMessage());
		}
	}
	
	/**
	 * 删除区域
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> deleteArea(@RequestBody BaseAreaRequestParam param) {
		BaseArea area = baseBiz.deleteBaseArea(param);
		if (area != null) {
			return ResponseHelper.success(area, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
	}
	
	
	/**
	 * 修改区域
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateArea(@RequestBody BaseAreaRequestParam param) {
		BaseArea area = baseBiz.updateBaseArea(param);
		if (area != null) {
			return ResponseHelper.success(area, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
	}
	
	/**
	 * 详情
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/info", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryInfo(@RequestBody BaseAreaRequestParam param) {
		BaseArea area = baseBiz.areaInfo(param);
		if (area != null) {
			return ResponseHelper.success(area, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
	}
	
	/**
	 * 查询省份
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/province", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryProvinces(@RequestBody BaseAreaRequestParam param) {
		List<BaseArea> list = baseBiz.queryProvinces(param);
		return ResponseHelper.success(list, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
	
	
	
	
	
}