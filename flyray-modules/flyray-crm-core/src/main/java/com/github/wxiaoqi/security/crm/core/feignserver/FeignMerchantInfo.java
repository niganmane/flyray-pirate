package com.github.wxiaoqi.security.crm.core.feignserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.crm.core.entity.MerchantBase;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantBaseMapper;

/**
 * 商户信息
 * @author he
 */
@Controller
@RequestMapping("feign/merchantInfo")
public class FeignMerchantInfo {
	
	@Autowired
	private MerchantBaseMapper merchantBaseMapper;
	
	/**
	 * 查询用户下的商户列表
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "queryListByOwner",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryListByOwner(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
		String operatorId = (String) param.get("operatorId");
		MerchantBase merchantBase = new MerchantBase();
		merchantBase.setOwner(Long.valueOf(operatorId));
		List<MerchantBase> selectMerchantBase = merchantBaseMapper.select(merchantBase);
		result.put("merchantList", selectMerchantBase);
		result.put("code", ResponseCode.OK.getCode());
		result.put("msg", ResponseCode.OK.getMessage());
		result.put("success", true);
		return result;
	}

    /**
	 * 根据省查询商户
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "queryByProvince",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryByProvince(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
		String platformId = (String) param.get("platformId");
		String province = (String) param.get("province");
		String city = (String) param.get("city");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("platformId", platformId);
		if(!StringUtils.isEmpty(province)){
			map.put("province", province);
		}
		if(!StringUtils.isEmpty(city)){
			map.put("city", city);
		}
		List<MerchantBase> queryByProvince = merchantBaseMapper.queryByProvince(map);
		result.put("merchantList", queryByProvince);
		result.put("code", ResponseCode.OK.getCode());
		result.put("msg", ResponseCode.OK.getMessage());
		result.put("success", true);
		return result;
	}
}
