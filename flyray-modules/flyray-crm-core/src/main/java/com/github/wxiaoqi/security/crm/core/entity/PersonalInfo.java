package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 个人客户信息
 * @author centerroot
 * @time 创建时间:2018年8月14日下午2:47:21
 * @description
 */
public class PersonalInfo extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//序号
    private Integer id;
    
	    //个人客户编号
    private String personalId;
	
	    //平台编号
    private String platformId;
	
	    //用户编号
    private String customerId;
	
	    //第三方会员编号
    private String thirdNo;
	
	    //手机号
	private String phone;
	
	    //用户名称
    private String realName;
	
	    //身份证号
    private String idCard;
	
	    //用户昵称
    private String nickName;
	
	    //性别 1：男 2：女
    private String sex;
	
	    //生日
    private String birthday;
	
	    //居住地
    private String address;
	
	    //身份证正面
    private String idPositive;
	
	    //身份证反面
    private String idNegative;
	
	    //认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
    private String authenticationStatus;
	
	    //账户状态 00：正常，01：冻结
    private String status;
	
	    //创建时间
    private Date createTime;
	
	    //更新时间
    private Date updateTime;
	
    // 归属人（存后台管理系统登录人员id）指谁发展的客户
    private Long owner;

    // 用户头像
    private String avatar;
    
	    //备注
	private String remark;
    //个人客户类型  00：注册用户  01：运营添加用户
	private String personalType;
    
	    //客户等级
    private Integer personalLevel;
	
	    //行业
    private String industry;
	
	    //行业描述
    private String industryDescription;
	
	    //职务
    private String job;
	
	    //所在省市
    private String provinces;
	
	    //电子邮件
    private String email;
	
	    //邮编
    private String zipCode;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getThirdNo() {
		return thirdNo;
	}

	public void setThirdNo(String thirdNo) {
		this.thirdNo = thirdNo;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}


	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIdPositive() {
		return idPositive;
	}

	public void setIdPositive(String idPositive) {
		this.idPositive = idPositive;
	}

	public String getIdNegative() {
		return idNegative;
	}

	public void setIdNegative(String idNegative) {
		this.idNegative = idNegative;
	}

	public String getAuthenticationStatus() {
		return authenticationStatus;
	}

	public void setAuthenticationStatus(String authenticationStatus) {
		this.authenticationStatus = authenticationStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Long getOwner() {
		return owner;
	}

	public void setOwner(Long owner) {
		this.owner = owner;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getPersonalLevel() {
		return personalLevel;
	}

	public void setPersonalLevel(Integer personalLevel) {
		this.personalLevel = personalLevel;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getIndustryDescription() {
		return industryDescription;
	}

	public void setIndustryDescription(String industryDescription) {
		this.industryDescription = industryDescription;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getProvinces() {
		return provinces;
	}

	public void setProvinces(String provinces) {
		this.provinces = provinces;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPersonalType() {
		return personalType;
	}

	public void setPersonalType(String personalType) {
		this.personalType = personalType;
	}
	
}
