package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.MerchantAccount;
import tk.mybatis.mapper.common.Mapper;

/**
 * 商户账户信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@org.apache.ibatis.annotations.Mapper
public interface MerchantAccountMapper extends Mapper<MerchantAccount> {
	
}
