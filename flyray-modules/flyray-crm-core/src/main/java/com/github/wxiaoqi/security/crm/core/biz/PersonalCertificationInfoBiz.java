package com.github.wxiaoqi.security.crm.core.biz;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.entity.PersonalCertificationInfo;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBaseMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalCertificationInfoMapper;

import lombok.extern.slf4j.Slf4j;

import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.CertificationRequest;
import com.github.wxiaoqi.security.common.crm.request.CustomerBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.LoginRequest;
import com.github.wxiaoqi.security.common.enums.CertificationStatus;
import com.github.wxiaoqi.security.common.enums.CertificationType;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

/**
 * 用户实名认证
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-10 15:02:35
 */
@Service
@Slf4j
public class PersonalCertificationInfoBiz extends BaseBiz<PersonalCertificationInfoMapper,PersonalCertificationInfo> {
	@Autowired
	private PersonalBaseMapper personalBaseMapper;
	
	@Autowired
	private PersonalCertificationInfoMapper personalCertificationInfoMapper;
	/**
	 * 用户实名认证
	 * @author centerroot
	 * @time 创建时间:2018年9月8日下午4:47:04
	 * @param 
	 * @return
	 */
	public Map<String, Object> certification(CertificationRequest certificationRequest){
		log.info("【 用户实名认证】   请求参数：{}",EntityUtils.beanToMap(certificationRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		try {
			PersonalBase queryPerParam=new PersonalBase();
			queryPerParam.setPlatformId(certificationRequest.getPlatformId());
			queryPerParam.setCustomerId(certificationRequest.getCustomerId());
			PersonalBase personalBase=personalBaseMapper.selectOne(queryPerParam);
			if(null==personalBase){
				respMap.put("code", ResponseCode.PER_NOTEXIST.getCode());
			    respMap.put("msg", ResponseCode.PER_NOTEXIST.getMessage());
			    return respMap;
			}
			
			PersonalCertificationInfo querycertificationInfo =new PersonalCertificationInfo();
			querycertificationInfo.setPlatformId(certificationRequest.getPlatformId());
			querycertificationInfo.setAuthType(certificationRequest.getAuthType());
			querycertificationInfo.setPersonalId(personalBase.getPersonalId());
			PersonalCertificationInfo personalCert=personalCertificationInfoMapper.selectOne(querycertificationInfo);
			if(null!=personalCert){
				respMap.put("code", ResponseCode.PER_CERTIFICATION_EXIST.getCode());
		        respMap.put("msg", ResponseCode.PER_CERTIFICATION_EXIST.getMessage());
		        return respMap;
			}
			String id = String.valueOf(SnowFlake.getId()); 
			PersonalCertificationInfo  certificationInfo=new PersonalCertificationInfo();
			if(CertificationType.FOURFACTORS_CERTIFY.getCode().equals(certificationRequest.getAuthType())){//四要素的实名认证
				if(CertificationStatus.SUCCESS_CERTIFY.getCode().equals(personalBase.getAuthenticationStatus())){//认证成功
					respMap.put("code", ResponseCode.PER_CERTIFICATION_EXIST.getCode());
			        respMap.put("msg", ResponseCode.PER_CERTIFICATION_EXIST.getMessage());
			        return respMap;
				}
				certificationInfo.setId(id);
				certificationInfo.setPlatformId(certificationRequest.getPlatformId());
				certificationInfo.setPersonalId(personalBase.getPersonalId());
				certificationInfo.setBankCardNo(certificationRequest.getBankCardNo());
				//certificationInfo.setBindEncryptCardNo();
				certificationInfo.setCardholderName(certificationRequest.getCardHolderName());
				certificationInfo.setBankCardNo(certificationRequest.getBankNo());
				certificationInfo.setBankName(certificationRequest.getBankName());
				certificationInfo.setSubbranchNo(certificationRequest.getSubbranchNo());
				certificationInfo.setSubbranchName(certificationRequest.getSubbranchName());
			}else if(CertificationType.TAHUA_THIRD_CERTIFY.getCode().equals(certificationRequest.getAuthType())){//泰华实名认证
				certificationInfo.setId(id);
				certificationInfo.setPlatformId(certificationRequest.getPlatformId());
				certificationInfo.setPersonalId(personalBase.getPersonalId());
				certificationInfo.setAuthId(certificationRequest.getAuthId());
			}

			certificationInfo.setAuthType(certificationRequest.getAuthType());
			certificationInfo.setCreateTime(new Date());
			personalCertificationInfoMapper.insertSelective(certificationInfo);
			personalBase.setAuthenticationStatus("02"); // 认证成功
			personalBaseMapper.updateByPrimaryKey(personalBase);
			
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			log.info("用户实名认证-报错。。。。。。{}"+e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		return respMap;
		
	}
	
	/**
	 * 实名认证状态查询
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午9:50:56
	 * @param certificationRequest
	 * @return
	 */
	public Map<String, Object> realNameQuery(CertificationRequest certificationRequest){
		log.info("【 查询实名认证状态】   请求参数：{}",EntityUtils.beanToMap(certificationRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PersonalCertificationInfo certificationInfoReq = new PersonalCertificationInfo();
		BeanUtils.copyProperties(certificationRequest, certificationInfoReq);
		PersonalBase queryPerParam=new PersonalBase();
		queryPerParam.setPlatformId(certificationRequest.getPlatformId());
		queryPerParam.setCustomerId(certificationRequest.getCustomerId());
		PersonalBase personalBase=personalBaseMapper.selectOne(queryPerParam);
		if(null==personalBase){
			respMap.put("code", ResponseCode.PER_NOTEXIST.getCode());
		    respMap.put("msg", ResponseCode.PER_NOTEXIST.getMessage());
			log.info("【 查询实名认证状态】   响应结果：{}",respMap);
		    return respMap;
		}
		certificationInfoReq.setPersonalId(personalBase.getPersonalId());
		List<PersonalCertificationInfo> certificationInfos = personalCertificationInfoMapper.select(certificationInfoReq);

		respMap.put("personalBase", personalBase);
		respMap.put("certificationInfos", certificationInfos);
		respMap.put("code", ResponseCode.OK.getCode());
	    respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【 查询实名认证状态】   响应结果：{}",respMap);
		return respMap;
	}
	
}