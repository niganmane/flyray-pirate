package com.github.wxiaoqi.security.crm.core.api;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.ResponseHelper;
import com.github.wxiaoqi.security.crm.core.biz.PlatformFunctionBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="功能菜单管理")
@Controller
@RequestMapping("platformFunction")
public class FunctionController {
	private static final Logger logger = LoggerFactory.getLogger(FunctionController.class);

	@Autowired
	private PlatformFunctionBiz functionbiz;
	
	
	@ApiOperation("功能菜单")
	@RequestMapping(value = "/list",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> wechatMiniProgramAdd(@RequestBody Map<String, Object> param) throws Exception {
		logger.info("查询功能菜单。。。{}"+param);
		List<Map<String, Object>> list = functionbiz.queryFunctionMenu(param);
		return ResponseHelper.success(list, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
    }
}
