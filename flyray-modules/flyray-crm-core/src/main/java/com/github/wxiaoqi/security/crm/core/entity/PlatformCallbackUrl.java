package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 平台回调地址配置
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:48
 */
@Table(name = "platform_callback_url")
public class PlatformCallbackUrl extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //回调类型   00：支付回调  01：退款回调  02：消息通知
    @Column(name = "callback_type")
    private String callbackType;
	
	    //回调地址
    @Column(name = "callback_url")
    private String callbackUrl;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	

	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：回调类型   00：支付回调  01：退款回调  02：消息通知
	 */
	public void setCallbackType(String callbackType) {
		this.callbackType = callbackType;
	}
	/**
	 * 获取：回调类型   00：支付回调  01：退款回调  02：消息通知
	 */
	public String getCallbackType() {
		return callbackType;
	}
	/**
	 * 设置：回调地址
	 */
	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}
	/**
	 * 获取：回调地址
	 */
	public String getCallbackUrl() {
		return callbackUrl;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	@Override
	public String toString() {
		return "PlatformCallbackUrl [id=" + id + ", platformId=" + platformId + ", callbackType=" + callbackType
				+ ", callbackUrl=" + callbackUrl + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}
	
}
