package com.github.wxiaoqi.security.crm.core.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.CustomerBaseAuthsBiz;
import com.github.wxiaoqi.security.crm.core.entity.CustomerBaseAuths;

@RestController
@RequestMapping("customerBaseAuths")
public class CustomerBaseAuthsController extends BaseController<CustomerBaseAuthsBiz,CustomerBaseAuths> {

}