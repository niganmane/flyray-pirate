package com.github.wxiaoqi.security.crm.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.wxiaoqi.security.crm.core.interceptor.UserTokenInterceptor;



/**
 * Created by ace on 2017/9/12.
 */
@Configuration
public class FeignConfiguration {
    @Bean
    UserTokenInterceptor getClientTokenInterceptor(){
        return new UserTokenInterceptor();
    }
}
