package com.github.wxiaoqi.security.crm.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.wxiaoqi.security.crm.core.entity.PlatformFunction;

import tk.mybatis.mapper.common.Mapper;

/**
 * 平台功能
 * 
 * @author chj
 * @email ${email}
 * @date 2018-09-04 11:18:41
 */

public interface PlatformFunctionMapper extends Mapper<PlatformFunction> {
	
	List<PlatformFunction> queryWithLevel(Map<String, Object> map);
	
	List<Map<String, Object>> queryLevelOne(Map<String, Object> map);
	
	List<Map<String, Object>> queryLevelTwo(Map<String, Object> map);
	
}
