package com.github.wxiaoqi.security.crm.core.api;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.cms.request.CreateFireSourceWalletRequest;
import com.github.wxiaoqi.security.common.crm.request.AccountJournalQueryRequest;
import com.github.wxiaoqi.security.common.crm.request.AccountQueryRequest;
import com.github.wxiaoqi.security.common.crm.request.ForceTopAccountRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryFireSourceParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.ResponseHelper;
import com.github.wxiaoqi.security.crm.core.biz.CommonAccountQueryBiz;
import com.github.wxiaoqi.security.crm.core.biz.PersonalAccountBiz;
import com.github.wxiaoqi.security.crm.core.biz.PlatformCoinCustomerBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.entity.PlatformCoinCustomer;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="账户管理")
@Controller
@RequestMapping("account")
public class AccountController {
	
	@Autowired
	private PlatformCoinCustomerBiz platformCoinCustomerBiz;
	@Autowired
	private CommonAccountQueryBiz commonAccountQueryBiz;
	@Autowired
	private PersonalAccountBiz personalAccountBiz;
	
	/**
	 * 微信小程序火源账户信息查询
	 * @return
	 * @throws Exception 
	 */ 
	@ApiOperation("微信小程序火源账户信息查询")
	@RequestMapping(value = "/queryFireSourceInfo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryFireSourceInfo(@RequestBody @Valid QueryFireSourceParam queryFireSourceParam) throws Exception {
		PlatformCoinCustomer platformCoinCustomer = platformCoinCustomerBiz.wechatMiniFireSourceInfo(EntityUtils.beanToMap(queryFireSourceParam));
		return ResponseHelper.success(platformCoinCustomer, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
    }
	
	/**
	 * 账户查询
	 * @param 
	 */
	@ApiOperation("账户查询")
	@RequestMapping(value = "/accountQuery",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> accountQuery(@RequestBody @Valid AccountQueryRequest accountQueryRequest){
		Map<String, Object> response = commonAccountQueryBiz.accountQuery(accountQueryRequest);
		return response;
    }
	
	/**
	 * 查询账户交易流水
	 * @param accountJournalQueryRequest
	 * @return
	 */
	@ApiOperation("查询账户交易流水")
	@RequestMapping(value = "/accountJournalQuery",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> accountJournalQuery(@RequestBody @Valid AccountJournalQueryRequest accountJournalQueryRequest){
		Map<String, Object> response = commonAccountQueryBiz.accountJournalQuery(accountJournalQueryRequest);
		return response;
    }
	
	/**
	 * 火源账户钱包创建
	 * @param 
	 */
	@ApiOperation("火源账户钱包创建")
	@RequestMapping(value = "/createFireSourceWallet",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> createFireSourceWallet(@RequestBody @Valid CreateFireSourceWalletRequest createFireSourceWalletRequest){
		PersonalBase personalBase = platformCoinCustomerBiz.createFireSourceWallet(EntityUtils.beanToMap(createFireSourceWalletRequest));
		if(null != personalBase){
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else{
			return ResponseHelper.success(null, null, ResponseCode.CUST_NOTEXIST.getCode(), ResponseCode.CUST_NOTEXIST.getMessage());
		}
    }
	
	/**
	 * 微信小程序原力值账户前三排名用户信息查询
	 * @param 
	 */
	@ApiOperation("微信小程序原力值账户前三排名用户信息查询")
	@RequestMapping(value = "/forceTopAccountInfo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> forceTopAccountInfo(@RequestBody @Valid ForceTopAccountRequest forceTopAccountRequest){
		List<Map<String, Object>> respList = personalAccountBiz.forceTopAccountInfo(EntityUtils.beanToMap(forceTopAccountRequest));
		return ResponseHelper.success(respList, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}

}
