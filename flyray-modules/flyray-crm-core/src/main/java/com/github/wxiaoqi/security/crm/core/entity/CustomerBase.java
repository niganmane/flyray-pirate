package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 客户基础信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Table(name = "customer_base")
public class CustomerBase extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
    //序号
    @Id
    private Integer id;
	
	    //用户编号
    @Column(name = "customer_id")
    private String customerId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //客户类型    CUST00：平台  （ CUST01：商户  CUST02：用户）：普通客户默认为空
    @Column(name = "customer_type")
    private String customerType;
	
	    //支付密码
    @Column(name = "pay_password")
    private String payPassword;
	
	    //登录密码错误次数
    @Column(name = "password_error_count")
    private Integer passwordErrorCount;
	
	    //支付密码错误次数
    @Column(name = "pay_password_error_count")
    private Integer payPasswordErrorCount;
	
	    //登录密码状态  00：正常   01：未设置   02：锁定
    @Column(name = "password_status")
    private String passwordStatus;
	
	    //支付密码状态    00：正常   01：未设置   02：锁定
    @Column(name = "pay_password_status")
    private String payPasswordStatus;
	
	    //个人客户编号
    @Column(name = "personal_id")
    private String personalId;
	
	    //商户客户编号
    @Column(name = "merchant_id")
    private String merchantId;
	
	    //账户状态 00：正常，01：客户冻结
    @Column(name = "status")
    private String status;
	
	    //认证状态（同步个人客户基础信息表中认证状态）  00：未认证  01：无需认证  02：认证成功  03：认证失败
    @Column(name = "authentication_status")
    private String authenticationStatus;
	
	    //注册时间
    @Column(name = "register_time")
    private Date registerTime;
	
	    //注册IP
    @Column(name = "register_ip")
    private String registerIp;
	
	    //上次登录时间
    @Column(name = "login_time")
    private Date loginTime;
	
	    //上次登录IP
    @Column(name = "login_ip")
    private String loginIp;
	
	    //最后登录角色（默认为空）  01：个人客户     02 ：商户客户
    @Column(name = "last_login_role")
    private String lastLoginRole;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	
	    //登录密码盐值
	@Column(name = "password_salt")
	private String passwordSalt;
	
		//支付密码盐值
	@Column(name = "pay_password_salt")
	private String payPasswordSalt;
	
	
	
	public String getPasswordSalt() {
		return passwordSalt;
	}
	public void setPasswordSalt(String passwordSalt) {
		this.passwordSalt = passwordSalt;
	}
	public String getPayPasswordSalt() {
		return payPasswordSalt;
	}
	public void setPayPasswordSalt(String payPasswordSalt) {
		this.payPasswordSalt = payPasswordSalt;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 设置：用户编号
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：用户编号
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：客户类型    CUST00：平台  （ CUST01：商户  CUST02：用户）：普通客户默认为空
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	/**
	 * 获取：客户类型    CUST00：平台  （ CUST01：商户  CUST02：用户）：普通客户默认为空
	 */
	public String getCustomerType() {
		return customerType;
	}
	/**
	 * 设置：支付密码
	 */
	public void setPayPassword(String payPassword) {
		this.payPassword = payPassword;
	}
	/**
	 * 获取：支付密码
	 */
	public String getPayPassword() {
		return payPassword;
	}
	/**
	 * 设置：登录密码错误次数
	 */
	public void setPasswordErrorCount(Integer passwordErrorCount) {
		this.passwordErrorCount = passwordErrorCount;
	}
	/**
	 * 获取：登录密码错误次数
	 */
	public Integer getPasswordErrorCount() {
		return passwordErrorCount;
	}
	/**
	 * 设置：支付密码错误次数
	 */
	public void setPayPasswordErrorCount(Integer payPasswordErrorCount) {
		this.payPasswordErrorCount = payPasswordErrorCount;
	}
	/**
	 * 获取：支付密码错误次数
	 */
	public Integer getPayPasswordErrorCount() {
		return payPasswordErrorCount;
	}
	/**
	 * 设置：登录密码状态  00：正常   01：未设置   02：锁定
	 */
	public void setPasswordStatus(String passwordStatus) {
		this.passwordStatus = passwordStatus;
	}
	/**
	 * 获取：登录密码状态  00：正常   01：未设置   02：锁定
	 */
	public String getPasswordStatus() {
		return passwordStatus;
	}
	/**
	 * 设置：支付密码状态    00：正常   01：未设置   02：锁定
	 */
	public void setPayPasswordStatus(String payPasswordStatus) {
		this.payPasswordStatus = payPasswordStatus;
	}
	/**
	 * 获取：支付密码状态    00：正常   01：未设置   02：锁定
	 */
	public String getPayPasswordStatus() {
		return payPasswordStatus;
	}
	/**
	 * 设置：个人客户编号
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 获取：个人客户编号
	 */
	public String getPersonalId() {
		return personalId;
	}
	/**
	 * 设置：商户客户编号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户客户编号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：账户状态 00：正常，01：客户冻结
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：账户状态 00：正常，01：客户冻结
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：认证状态（同步个人客户基础信息表中认证状态）  00：未认证  01：无需认证  02：认证成功  03：认证失败
	 */
	public void setAuthenticationStatus(String authenticationStatus) {
		this.authenticationStatus = authenticationStatus;
	}
	/**
	 * 获取：认证状态（同步个人客户基础信息表中认证状态）  00：未认证  01：无需认证  02：认证成功  03：认证失败
	 */
	public String getAuthenticationStatus() {
		return authenticationStatus;
	}
	/**
	 * 设置：注册时间
	 */
	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}
	/**
	 * 获取：注册时间
	 */
	public Date getRegisterTime() {
		return registerTime;
	}
	/**
	 * 设置：注册IP
	 */
	public void setRegisterIp(String registerIp) {
		this.registerIp = registerIp;
	}
	/**
	 * 获取：注册IP
	 */
	public String getRegisterIp() {
		return registerIp;
	}
	/**
	 * 设置：上次登录时间
	 */
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
	/**
	 * 获取：上次登录时间
	 */
	public Date getLoginTime() {
		return loginTime;
	}
	/**
	 * 设置：上次登录IP
	 */
	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}
	/**
	 * 获取：上次登录IP
	 */
	public String getLoginIp() {
		return loginIp;
	}
	/**
	 * 设置：最后登录角色（默认为空）  01：个人客户     02 ：商户客户
	 */
	public void setLastLoginRole(String lastLoginRole) {
		this.lastLoginRole = lastLoginRole;
	}
	/**
	 * 获取：最后登录角色（默认为空）  01：个人客户     02 ：商户客户
	 */
	public String getLastLoginRole() {
		return lastLoginRole;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
