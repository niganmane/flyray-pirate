package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.PersonalDistributionRelation;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalDistributionRelationMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 个人分销关系
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class PersonalDistributionRelationBiz extends BaseBiz<PersonalDistributionRelationMapper,PersonalDistributionRelation> {
	
	
	
	/**
	 * 列表
	 */
}