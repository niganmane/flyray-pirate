package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 客户授权信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 * * 详情请参考  http://www.cnblogs.com/jiqing9006/p/5937733.html
 */
@Table(name = "customer_base_auths")
public class CustomerBaseAuths extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //用户编号
    @Column(name = "customer_id")
    private String customerId;
	
	    //客户类型    CUST00：平台   CUST01：商户  CUST02：用户
    @Column(name = "customer_type")
    private String customerType;
	
	    //授权类型  00：登录授权  01：支付授权（预留）
    @Column(name = "auth_type")
    private String authType;
	
	    //授权方式  phone：手机号  email：邮箱   username：用户名  weixin：微信  qq：QQ  weibo：微博
    @Column(name = "auth_method")
    private String authMethod;
	
	    //是否第三方  00：站内    01：第三方
    @Column(name = "isThird")
    private String isthird;
	
	    //识别码
    @Column(name = "identifier")
    private String identifier;
	
	    //凭据
    @Column(name = "credential")
    private String credential;
	

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：用户编号
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * 获取：用户编号
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * 设置：客户类型    CUST00：平台   CUST01：商户  CUST02：用户
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	/**
	 * 获取：客户类型    CUST00：平台   CUST01：商户  CUST02：用户
	 */
	public String getCustomerType() {
		return customerType;
	}
	/**
	 * 设置：授权类型  00：登录授权  01：支付授权（预留）
	 */
	public void setAuthType(String authType) {
		this.authType = authType;
	}
	/**
	 * 获取：授权类型  00：登录授权  01：支付授权（预留）
	 */
	public String getAuthType() {
		return authType;
	}
	/**
	 * 设置：授权方式  phone：手机号  email：邮箱   username：用户名  weixin：微信  qq：QQ  weibo：微博
	 */
	public void setAuthMethod(String authMethod) {
		this.authMethod = authMethod;
	}
	/**
	 * 获取：授权方式  phone：手机号  email：邮箱   username：用户名  weixin：微信  qq：QQ  weibo：微博
	 */
	public String getAuthMethod() {
		return authMethod;
	}
	/**
	 * 设置：是否第三方  00：站内    01：第三方
	 */
	public void setIsthird(String isthird) {
		this.isthird = isthird;
	}
	/**
	 * 获取：是否第三方  00：站内    01：第三方
	 */
	public String getIsthird() {
		return isthird;
	}
	/**
	 * 设置：识别码
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	/**
	 * 获取：识别码
	 */
	public String getIdentifier() {
		return identifier;
	}
	/**
	 * 设置：凭据
	 */
	public void setCredential(String credential) {
		this.credential = credential;
	}
	/**
	 * 获取：凭据
	 */
	public String getCredential() {
		return credential;
	}
}
