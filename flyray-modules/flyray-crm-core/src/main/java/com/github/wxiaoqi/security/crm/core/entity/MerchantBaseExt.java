package com.github.wxiaoqi.security.crm.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 商户基础信息扩展表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:49
 */
@Table(name = "merchant_base_ext")
public class MerchantBaseExt implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //商户编号
    @Id
    private String merchantId;
	
	    //邀请面试次数
    @Column(name = "interview")
    private Integer interview;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
    
    //邮编
    @Column(name = "zip_code")
    private String zipCode;
    
    //商户备用地址
    @Column(name = "spare_address")
    private String spareAddress;
    
    //签约日期
    @Column(name = "signing_date")
    private Date signingDate;
	
	    //节约日期
    @Column(name = "release_date")
    private Date releaseDate;
    
    //微信号
    @Column(name = "wechat_no")
    private String wechatNo;
    
    //省份代码
    @Column(name = "province")
    private String province;
    
    //市
    @Column(name = "city")
    private String city;
    
    //县
    @Column(name = "county")
    private String county;
	

	/**
	 * 设置：商户编号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户编号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：邀请面试次数
	 */
	public void setInterview(Integer interview) {
		this.interview = interview;
	}
	/**
	 * 获取：邀请面试次数
	 */
	public Integer getInterview() {
		return interview;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getSpareAddress() {
		return spareAddress;
	}
	public void setSpareAddress(String spareAddress) {
		this.spareAddress = spareAddress;
	}
	public Date getSigningDate() {
		return signingDate;
	}
	public void setSigningDate(Date signingDate) {
		this.signingDate = signingDate;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getWechatNo() {
		return wechatNo;
	}
	public void setWechatNo(String wechatNo) {
		this.wechatNo = wechatNo;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	
}
