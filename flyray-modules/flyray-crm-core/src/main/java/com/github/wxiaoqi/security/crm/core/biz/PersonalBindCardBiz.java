package com.github.wxiaoqi.security.crm.core.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.PersonalBindCardRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.common.util.SnowFlake;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBindCard;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBindCardMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * 用户绑卡表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-16 16:56:09
 */
@Slf4j
@Service
public class PersonalBindCardBiz extends BaseBiz<PersonalBindCardMapper,PersonalBindCard> {
	/**
	 * 根据条件查询绑卡信息
	 * @author centerroot
	 * @time 创建时间:2018年8月16日下午5:05:43
	 * @param personalBindCardRequest
	 * @return
	 */
	public Map<String, Object> pageList(PersonalBindCardRequest personalBindCardRequest){
		log.info("根据条件查询绑卡信息请求参数：{}",EntityUtils.beanToMap(personalBindCardRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PersonalBindCard personalBindCardReq = new PersonalBindCard();
		int page = personalBindCardRequest.getPage();
		int limit = personalBindCardRequest.getLimit();
		
		BeanUtils.copyProperties(personalBindCardRequest, personalBindCardReq);
		List<PersonalBindCard> list = mapper.select(personalBindCardReq);
		
		if (page == 0) {
			respMap.put("personalCards", list);
		}else{
			Map<String, Object> params = new HashMap<String, Object>();
		    params.put("page", page);
		    params.put("limit", limit);
		    Query query = new Query(params);
		    Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
			
			TableResultResponse<PersonalBindCard> table = new TableResultResponse<PersonalBindCard>(result.getTotal(), list);
	        respMap.put("body", table);
		}
        
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("根据条件查询绑卡信息 响应结果：{}",respMap);
		return respMap;
	}
	
	/**
	 * 添加绑卡信息
	 * @author centerroot
	 * @time 创建时间:2018年8月16日下午5:05:52
	 * @param personalBindCardRequest
	 * @return
	 */
	public Map<String, Object> addCardInfo(PersonalBindCardRequest personalBindCardRequest){
		log.info("添加绑卡信息请求参数：{}",EntityUtils.beanToMap(personalBindCardRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PersonalBindCard personalBindCard = new PersonalBindCard();
		BeanUtils.copyProperties(personalBindCardRequest, personalBindCard);
		String id = String.valueOf(SnowFlake.getId());
		personalBindCard.setId(id);
		personalBindCard.setStatus("00");
		mapper.insert(personalBindCard);
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("添加绑卡信息 响应结果：{}",respMap);
		return respMap;
	}
}