package com.github.wxiaoqi.security.crm.core.biz;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherApplyRequest;
import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherRequest;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.FreezeRequest;
import com.github.wxiaoqi.security.common.crm.request.MerchantPayForAnotherBankParam;
import com.github.wxiaoqi.security.common.crm.request.MerchantPayForAnotherBankRequest;
import com.github.wxiaoqi.security.common.crm.request.UnFreAndOutAccountRequest;
import com.github.wxiaoqi.security.common.enums.CustomerTypeEnums;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.crm.core.client.BankCodeBizService;
import com.github.wxiaoqi.security.crm.core.entity.MerchantPayForAnotherBank;
import com.github.wxiaoqi.security.crm.core.entity.PayChannelConfig;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantPayForAnotherBankMapper;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 商户提现银行卡信息表
 * @author he
 */
@Service
public class MerchantPayForAnotherBankBiz extends BaseBiz<MerchantPayForAnotherBankMapper,MerchantPayForAnotherBank> {
	
	private static final Logger logger = LoggerFactory.getLogger(MerchantPayForAnotherBankBiz.class);
	
	@Autowired
	private BankCodeBizService bankCodeBizService;
	@Autowired
	private CommonFreezeUnFreezeBiz commonFreezeUnFreezeBiz;
	@Autowired
	private CommonOutAccountBiz outAccountBiz;
	@Autowired
	private PayChannelConfigBiz payChannelConfigBiz;

	/**
	 * 代付申请
	 * @return
	 */
	public Map<String, Object> payForAnotherApply(PayForAnotherApplyRequest payForAnotherApplyRequest){
		logger.info("代付申请开始..........{}",EntityUtils.beanToMap(payForAnotherApplyRequest));
		
		BigDecimal fee = BigDecimal.ZERO;
		//计算手续费
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("platformId", payForAnotherApplyRequest.getPlatformId());
		params.put("merchantId", payForAnotherApplyRequest.getMerchantId());
		params.put("payChannelNo", payForAnotherApplyRequest.getPayChannelNo());
		params.put("payAmt", payForAnotherApplyRequest.getAmount());
		params.put("outCustomerId", payForAnotherApplyRequest.getCustomerId());
		PayChannelConfig payChannelConfig = new PayChannelConfig();
		payChannelConfig.setPlatformId(payForAnotherApplyRequest.getPlatformId());
		payChannelConfig.setMerchantId(payForAnotherApplyRequest.getMerchantId());
		payChannelConfig.setPayChannelNo(payForAnotherApplyRequest.getPayChannelNo());
		Map<String, Object> queryMerChannelFee = payChannelConfigBiz.getPayChannelFee(payChannelConfig, payForAnotherApplyRequest.getAmount());
		logger.info("代付申请手续费.......{}",queryMerChannelFee);
		if(ResponseCode.OK.getCode().equals(queryMerChannelFee.get("code"))){
			String feeStr = (String)queryMerChannelFee.get("fee");
			fee = new BigDecimal(feeStr);
		}
		
		Map<String, Object> param = new HashMap<String, Object>();
		String orderId = String.valueOf(SnowFlake.getId());
		param.put("platformId",payForAnotherApplyRequest.getPlatformId());
		param.put("merchantId",payForAnotherApplyRequest.getMerchantId());
		param.put("bankName", payForAnotherApplyRequest.getBankName());
		param.put("bankAccountName", payForAnotherApplyRequest.getBankAccountName());
		param.put("bankAccountNo", payForAnotherApplyRequest.getBankNo());
		param.put("orderId", orderId);
		param.put("amount", payForAnotherApplyRequest.getAmount());
		param.put("payFee", String.valueOf(fee));
		Map<String, Object> payForAnotherApply = bankCodeBizService.payForAnotherApply(param);
		logger.info("paycore代付申请结束..........{}",payForAnotherApply);
		if(ResponseCode.OK.getCode().equals(payForAnotherApply.get("code"))){
			//冻结商户可提现余额账户
			FreezeRequest freezeRequest = new FreezeRequest();
			freezeRequest.setPlatformId(payForAnotherApplyRequest.getPlatformId());
			freezeRequest.setMerchantId(payForAnotherApplyRequest.getMerchantId());
			freezeRequest.setCustomerType(CustomerTypeEnums.mer_user.getCode());
			freezeRequest.setMerchantType("CUST01");//商户
			freezeRequest.setAccountType("ACC013");//可提现余额账户
			freezeRequest.setFreezeAmt(payForAnotherApplyRequest.getAmount());
			freezeRequest.setTradeType("02");//提现
			freezeRequest.setOrderNo(orderId);
			commonFreezeUnFreezeBiz.freeze(freezeRequest);
		}
		logger.info("代付申请结束..........");
		return payForAnotherApply;
	}

	/**
	 * 代付
	 * @return
	 */
	public Map<String, Object> payForAnother(PayForAnotherRequest payForAnotherRequest){
		logger.info("代付开始..........{}",EntityUtils.beanToMap(payForAnotherRequest));
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("platformId",payForAnotherRequest.getPlatformId());
		param.put("orderId", payForAnotherRequest.getOrderId());
		Map<String, Object> payForAnother = bankCodeBizService.payForAnother(param);
		logger.info("paycore代付结束..........{}",payForAnother);
		if(ResponseCode.OK.getCode().equals(payForAnother.get("code"))){
			//解冻并出账商户可提现余额账户
			UnFreAndOutAccountRequest unFreAndOutAccountRequest = new UnFreAndOutAccountRequest();
			unFreAndOutAccountRequest.setPlatformId(payForAnotherRequest.getPlatformId());
			unFreAndOutAccountRequest.setMerchantId(payForAnotherRequest.getMerchantId());
			unFreAndOutAccountRequest.setFreezeId(String.valueOf(SnowFlake.getId()));
			unFreAndOutAccountRequest.setCustomerType(CustomerTypeEnums.mer_user.getCode());
			unFreAndOutAccountRequest.setMerchantType("CUST01");//商户
			unFreAndOutAccountRequest.setAccountType("ACC013");//可提现余额账户
			unFreAndOutAccountRequest.setUnFreAmt((String) payForAnother.get("amount"));
			unFreAndOutAccountRequest.setOrderNo(payForAnotherRequest.getOrderId());
			unFreAndOutAccountRequest.setTradeType("02");//提现
			outAccountBiz.unFreAndOutAccount(unFreAndOutAccountRequest);
		}
		return payForAnother;
	}
	
	
	/**
	 * 列表
	 * @param bean
	 * @return
	 */
	public TableResultResponse<MerchantPayForAnotherBank> payForAnotherBankList(MerchantPayForAnotherBankRequest param){
		 Example example = new Example(MerchantPayForAnotherBank.class);
		 Criteria criteria = example.createCriteria();
		 if(!StringUtils.isEmpty(param.getPlatformId())) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		 }
		 if(!StringUtils.isEmpty(param.getMerchantId())) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		 }
		 Page<MerchantPayForAnotherBank> result = PageHelper.startPage(param.getPage(), param.getLimit());
		 List<MerchantPayForAnotherBank> list = mapper.selectByExample(example);
		 return new TableResultResponse<MerchantPayForAnotherBank>(result.getTotal(), list);
	}
	
	/** 
	 * 添加
	 */
	public Map<String, Object> addPayForAnotherBank(MerchantPayForAnotherBankParam param){
		Map<String, Object> respMap = new HashMap<String, Object>();
		try {
			MerchantPayForAnotherBank config = new MerchantPayForAnotherBank();
			BeanUtils.copyProperties(param, config);
			if (mapper.insertSelective(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		return respMap;
	}
	
	/**
	 * 删除
	 */
	public MerchantPayForAnotherBank deletePayForAnotherBank(MerchantPayForAnotherBankParam param){
		MerchantPayForAnotherBank config1 = null;
		try {
			MerchantPayForAnotherBank config = new MerchantPayForAnotherBank();
			BeanUtils.copyProperties(param, config);
			config.setId(Integer.valueOf(param.getId()));
			config1 = mapper.selectOne(config);
			mapper.delete(config);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config1;
	}

	
	/**
	 * 修改
	 */
	public MerchantPayForAnotherBank updatePayForAnotherBank(MerchantPayForAnotherBankParam param){
		MerchantPayForAnotherBank config1 = null;
		try {
			MerchantPayForAnotherBank config = new MerchantPayForAnotherBank();
			BeanUtils.copyProperties(param, config);
			config.setId(Integer.valueOf(param.getId()));
			config1 = mapper.selectOne(config);
			mapper.updateByPrimaryKey(config);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config1;
	}
}