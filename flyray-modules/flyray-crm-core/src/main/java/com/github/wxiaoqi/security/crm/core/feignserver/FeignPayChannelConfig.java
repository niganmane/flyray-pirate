package com.github.wxiaoqi.security.crm.core.feignserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.crm.core.biz.PayChannelConfigBiz;
import com.github.wxiaoqi.security.crm.core.entity.PayChannelConfig;
import com.github.wxiaoqi.security.crm.core.mapper.PayChannelFeeConfigMapper;

@Controller
@RequestMapping("feign/payChannelConfig")
public class FeignPayChannelConfig {
	
	@Autowired
	private PayChannelConfigBiz payChannelConfigBiz;
	@Autowired
	private PayChannelFeeConfigMapper payChannelFeeConfigMapper;
	
	/**
	 * 查询商户/平台支付通道配置
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "query",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> query(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
    	try {
    		PayChannelConfig payChannelConfig = new PayChannelConfig();
    		payChannelConfig.setPlatformId((String)param.get("platformId"));
    		payChannelConfig.setMerchantId((String)param.get("merchantId"));
    		payChannelConfig.setPayChannelNo((String)param.get("payChannelNo"));
    		PayChannelConfig channelConfig = payChannelConfigBiz.getPayChannelConfig(payChannelConfig);
    		if(null != channelConfig){
    			result.put("payChannelConfigInfo", channelConfig);
    			result.put("code", ResponseCode.OK.getCode());
    			result.put("msg", ResponseCode.OK.getMessage());
    			result.put("success", true);
    		}else{
    			result.put("code", ResponseCode.PAYCHANNEL_CONFIGURATION_NOTEXIST.getCode());
    			result.put("msg", ResponseCode.PAYCHANNEL_CONFIGURATION_NOTEXIST.getMessage());
    			result.put("success", false);
    		}
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			result.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			result.put("success", false);
		}
    	return result;
    }
	
	/**
	 * 根据第三方商户号查询支付通道配置配置
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "queryByOutMerNo",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryByOutMerNo(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			PayChannelConfig payChannelConfig = new PayChannelConfig();
			payChannelConfig.setOutMerNo((String)param.get("outMerNo"));
			payChannelConfig.setOutMerAccount((String)param.get("outMerAccount"));
			List<PayChannelConfig> payChannelList = payChannelConfigBiz.selectList(payChannelConfig);
			if(payChannelList == null || payChannelList.size() == 0){
				result.put("code", ResponseCode.PAYCHANNEL_CONFIGURATION_NOTEXIST.getCode());
    			result.put("msg", ResponseCode.PAYCHANNEL_CONFIGURATION_NOTEXIST.getMessage());
    			result.put("success", false);
			}else{
				result.put("payChannelConfigInfo", payChannelList.get(0));
    			result.put("code", ResponseCode.OK.getCode());
    			result.put("msg", ResponseCode.OK.getMessage());
    			result.put("success", true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			result.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			result.put("success", false);
		}
		return result;
	}
	
	/**
	 * 查询商户通道费率
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "queryMerChannelFee",method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryMerChannelFee(@RequestBody Map<String, Object> param){
		String platformId = (String)param.get("platformId");
		String merchantId = (String)param.get("merchantId");
		String payChannelNo = (String)param.get("payChannelNo");
		String payAmt = (String)param.get("payAmt");
		PayChannelConfig payChannelConfig = new PayChannelConfig();
		payChannelConfig.setPlatformId(platformId);
		payChannelConfig.setMerchantId(merchantId);
		payChannelConfig.setPayChannelNo(payChannelNo);
		Map<String, Object> channelConfig = payChannelConfigBiz.getPayChannelFee(payChannelConfig, payAmt);
		return channelConfig;
	}
}
