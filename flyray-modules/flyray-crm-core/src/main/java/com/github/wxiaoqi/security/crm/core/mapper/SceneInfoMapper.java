package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.SceneInfo;

import tk.mybatis.mapper.common.Mapper;

/**
 * 场景
 * 
 * @author chj
 * @email ${email}
 * @date 2018-09-07 10:55:04
 */
@org.apache.ibatis.annotations.Mapper
public interface SceneInfoMapper extends Mapper<SceneInfo> {
	
}
