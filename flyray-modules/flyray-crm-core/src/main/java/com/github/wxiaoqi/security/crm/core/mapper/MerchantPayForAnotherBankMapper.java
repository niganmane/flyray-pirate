package com.github.wxiaoqi.security.crm.core.mapper;

import com.github.wxiaoqi.security.crm.core.entity.MerchantPayForAnotherBank;

import tk.mybatis.mapper.common.Mapper;

/**
 * 商户提现银行卡信息表
 * @author he
 */
@org.apache.ibatis.annotations.Mapper
public interface MerchantPayForAnotherBankMapper extends Mapper<MerchantPayForAnotherBank> {
	
}
