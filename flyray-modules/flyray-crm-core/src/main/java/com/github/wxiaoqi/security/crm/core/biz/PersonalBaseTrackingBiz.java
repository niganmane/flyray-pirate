package com.github.wxiaoqi.security.crm.core.biz;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.PersonalBaseTrackingRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBaseTracking;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBaseTrackingMapper;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 个人客户跟踪表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-09-05 10:18:49
 */
@Slf4j
@Service
public class PersonalBaseTrackingBiz extends BaseBiz<PersonalBaseTrackingMapper,PersonalBaseTracking> {
	
	/**
	 * 添加客户跟踪记录
	 * @author centerroot
	 * @time 创建时间:2018年9月5日上午11:22:39
	 * @param personalBaseTrackingRequest
	 * @return
	 */
	public Map<String, Object> add(PersonalBaseTrackingRequest personalBaseTrackingRequest){
		log.info("【添加客户跟踪记录】   请求参数：{}",EntityUtils.beanToMap(personalBaseTrackingRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		
		PersonalBaseTracking personalBaseTracking = new PersonalBaseTracking();
		BeanUtils.copyProperties(personalBaseTrackingRequest,personalBaseTracking);
		personalBaseTracking.setCreateTime(new Timestamp(System.currentTimeMillis()));
		mapper.insert(personalBaseTracking);
		
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【添加客户跟踪记录】   响应参数：{}",respMap);
		return respMap;
	}
	
	/**
	 * 查询单个客户跟踪记录
	 * @author centerroot
	 * @time 创建时间:2018年9月5日上午11:22:24
	 * @param personalBaseTrackingRequest
	 * @return
	 */
	public Map<String, Object> queryTrackingList(PersonalBaseTrackingRequest personalBaseTrackingRequest){
		log.info("【查询单个客户跟踪记录】   请求参数：:{}",EntityUtils.beanToMap(personalBaseTrackingRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String platformId = personalBaseTrackingRequest.getPlatformId();
		String personalId = personalBaseTrackingRequest.getPersonalId();
		
		
		Example example = new Example(PersonalBaseTracking.class);
		example.setOrderByClause("create_time asc");
        Criteria criteria = example.createCriteria();
        if (!StringUtils.isEmpty(platformId)) {
        	criteria.andEqualTo("platformId", platformId);
		}
        if (!StringUtils.isEmpty(personalId)) {
        	criteria.andEqualTo("personalId", personalId);
		}
        List<PersonalBaseTracking> list = mapper.selectByExample(example);
        respMap.put("personalBaseTrackings", list);
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());
		
		log.info("【查询单个客户跟踪记录】   响应参数：{}",respMap);
		return respMap;
	}
}