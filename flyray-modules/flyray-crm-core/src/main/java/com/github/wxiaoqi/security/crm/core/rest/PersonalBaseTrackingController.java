package com.github.wxiaoqi.security.crm.core.rest;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.common.crm.request.PersonalBaseTrackingRequest;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.crm.core.biz.PersonalBaseTrackingBiz;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBaseTracking;

/**
 * 客户跟踪记录
 * @author centerroot
 * @time 创建时间:2018年9月5日上午10:29:40
 * @description
 */
@Controller
@RequestMapping("personalBaseTracking")
public class PersonalBaseTrackingController extends BaseController<PersonalBaseTrackingBiz,PersonalBaseTracking> {
	/**
	 * 添加客户跟踪记录
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:15
	 * @param personalBaseRequest
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> add(@RequestBody @Valid PersonalBaseTrackingRequest personalBaseTrackingRequest){
		return baseBiz.add(personalBaseTrackingRequest);
	}
	
	/**
	 * 查询单个客户跟踪记录
	 * @author centerroot
	 * @time 创建时间:2018年9月5日上午11:21:55
	 * @param personalBaseTrackingRequest
	 * @return
	 */
	@RequestMapping(value = "/queryTrackingList",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryTrackingList(@RequestBody @Valid PersonalBaseTrackingRequest personalBaseTrackingRequest){
		Map<String, Object> respMap = baseBiz.queryTrackingList(personalBaseTrackingRequest);
        return respMap;
    }

}