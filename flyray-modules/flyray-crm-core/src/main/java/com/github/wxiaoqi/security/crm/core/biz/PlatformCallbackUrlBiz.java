package com.github.wxiaoqi.security.crm.core.biz;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.PlatformCallbackUrl;
import com.github.wxiaoqi.security.crm.core.entity.PlatformSafetyConfig;
import com.github.wxiaoqi.security.crm.core.mapper.PlatformCallbackUrlMapper;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.CallBackUrlRequest;
import com.github.wxiaoqi.security.common.crm.request.SafetyConfigRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;

/**
 * 平台回调地址配置
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:48
 */
@Slf4j
@Service
public class PlatformCallbackUrlBiz extends BaseBiz<PlatformCallbackUrlMapper,PlatformCallbackUrl> {
	
	public TableResultResponse<PlatformCallbackUrl> callbackUrlList(CallBackUrlRequest bean){
		 Example example = new Example(PlatformCallbackUrl.class);
		 Criteria criteria = example.createCriteria();
		 criteria.andEqualTo("platformId",bean.getPlatformId());
		 example.setOrderByClause("create_time desc");
		 Page<PlatformCallbackUrl> result = PageHelper.startPage(bean.getPage(), bean.getLimit());
		 List<PlatformCallbackUrl> list = mapper.selectByExample(example);
		 return new TableResultResponse<PlatformCallbackUrl>(result.getTotal(), list);
	}
	
	
	public Map<String, Object> addCallbackUrl(CallBackUrlRequest bean){
		PlatformCallbackUrl config = new PlatformCallbackUrl();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 log.info("【添加回调地址】 请求参数。。。"+config);
			 config.setCreateTime(new Date());
			 config.setUpdateTime(new Date());
			 if (mapper.insert(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加回调地址】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			 e.printStackTrace();
			log.info("【添加回调地址】  报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加回调地址】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【添加回调地址】   响应参数：{}", respMap);
		return respMap;
	}
	
	
	public Map<String, Object> updateCallbackUrl(CallBackUrlRequest bean){
		PlatformCallbackUrl config = new PlatformCallbackUrl();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 config.setUpdateTime(new Date());
			 if (mapper.updateByPrimaryKeySelective(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【修改回调地址】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【修改回调地址】 报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【修改回调地址】  响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【修改回调地址】  响应参数：{}", respMap);
		 return respMap;
	}
	
	
	public Map<String, Object> deleteCallbackUrl(CallBackUrlRequest bean){
		 PlatformCallbackUrl config = new PlatformCallbackUrl();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 if (mapper.delete(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除回调地址】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【删除安全配置】   报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除回调地址】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		 respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		 log.info("【删除回调地址】   响应参数：{}", respMap);
		 return respMap;
	}
}