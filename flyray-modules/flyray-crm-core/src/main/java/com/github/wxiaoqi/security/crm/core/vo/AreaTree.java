package com.github.wxiaoqi.security.crm.core.vo;

import com.github.wxiaoqi.security.common.vo.AreaNode;

public class AreaTree extends AreaNode {

	private Long areaId;
	
	private String value;
	
	private String label;
	
	private Integer layer;
	
	private Integer orderNum;
	
	private Integer status;
	
	private String remark;

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getLayer() {
		return layer;
	}

	public void setLayer(Integer layer) {
		this.layer = layer;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public AreaTree() {
	}

	public AreaTree(String areaCode, String parentCode, String label) {
		super();
		this.areaCode = areaCode;
		this.parentCode = parentCode;
		this.label = label;
	}
	
	
	public AreaTree(String areaCode, AreaTree areaTree, String label) {
		super();
		this.areaCode = areaCode;
		this.parentCode = areaTree.getAreaCode();
		this.label = label;
	}

	@Override
	public String toString() {
		return "AreaTree [areaId=" + areaId + ", value=" + value + ", label=" + label + ", layer=" + layer
				+ ", orderNum=" + orderNum + ", status=" + status + ", remark=" + remark + "]";
	}

}
