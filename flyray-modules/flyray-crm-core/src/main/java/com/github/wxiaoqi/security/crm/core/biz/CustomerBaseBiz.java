package com.github.wxiaoqi.security.crm.core.biz;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.validation.Valid;

import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.CustomerBaseRequest;
import com.github.wxiaoqi.security.common.crm.request.CustomerRealNameRequest;
import com.github.wxiaoqi.security.common.crm.request.CustomerSetupPasswordRequest;
import com.github.wxiaoqi.security.common.crm.request.CustomerUpdatePasswordRequest;
import com.github.wxiaoqi.security.common.crm.request.CustomerVerifyPasswordRequest;
import com.github.wxiaoqi.security.common.crm.request.LoginRequest;
import com.github.wxiaoqi.security.common.crm.request.QueryCustomerBaseListRequest;
import com.github.wxiaoqi.security.common.crm.request.RegisterRequestParam;
import com.github.wxiaoqi.security.common.crm.request.ResetPasswordRequest;
import com.github.wxiaoqi.security.common.crm.request.UpdateCustomerStatusRequest;
import com.github.wxiaoqi.security.common.crm.request.ValidateSMSRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.MD5;
import com.github.wxiaoqi.security.common.util.Query;
import com.github.wxiaoqi.security.common.util.SnowFlake;
import com.github.wxiaoqi.security.crm.core.entity.CustomerBase;
import com.github.wxiaoqi.security.crm.core.entity.CustomerBaseAuths;
import com.github.wxiaoqi.security.crm.core.entity.MerchantAccount;
import com.github.wxiaoqi.security.crm.core.entity.MerchantBase;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBase;
import com.github.wxiaoqi.security.crm.core.entity.PersonalBaseExt;
import com.github.wxiaoqi.security.crm.core.entity.PersonalCertificationInfo;
import com.github.wxiaoqi.security.crm.core.mapper.CustomerBaseAuthsMapper;
import com.github.wxiaoqi.security.crm.core.mapper.CustomerBaseMapper;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantAccountMapper;
import com.github.wxiaoqi.security.crm.core.mapper.MerchantBaseMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBaseExtMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalBaseMapper;
import com.github.wxiaoqi.security.crm.core.mapper.PersonalCertificationInfoMapper;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 客户基础信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Slf4j
@Service
public class CustomerBaseBiz extends BaseBiz<CustomerBaseMapper, CustomerBase> {

	@Autowired
	public CustomerBaseAuthsMapper customerBaseAuthsMapper;
	@Autowired
	public PersonalBaseMapper personalBaseMapper;
	@Autowired
	public PersonalBaseExtMapper personalBaseExtMapper;
	@Autowired
	public MerchantBaseMapper merchantBaseMapper;
	@Autowired
	private MerchantAccountMapper merchantAccountMapper;
	@Autowired
	public SMSBiz smsBiz;
	@Autowired
	private PersonalCertificationInfoMapper personalCertificationInfoMapper;

	/**
	 * 查询客户基础信息列表
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:38
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	public Map<String, Object> queryList(QueryCustomerBaseListRequest queryCustomerBaseListRequest) {
		log.info("【查询客户基础信息列表】   请求参数：{}", EntityUtils.beanToMap(queryCustomerBaseListRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String platformId = queryCustomerBaseListRequest.getPlatformId();
		String customerId = queryCustomerBaseListRequest.getCustomerId();
		String authenticationStatus = queryCustomerBaseListRequest.getAuthenticationStatus();
		String status = queryCustomerBaseListRequest.getStatus();
		int page = queryCustomerBaseListRequest.getPage();
		int limit = queryCustomerBaseListRequest.getLimit();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("page", page);
		params.put("limit", limit);
		Query query = new Query(params);
		Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());

		Example example = new Example(PersonalBase.class);
		Criteria criteria = example.createCriteria();
		if (!StringUtils.isEmpty(platformId)) {
			criteria.andEqualTo("platformId", platformId);
		}
		if (!StringUtils.isEmpty(customerId)) {
			criteria.andEqualTo("customerId", customerId);
		}
		if (!StringUtils.isEmpty(authenticationStatus)) {
			criteria.andEqualTo("authenticationStatus", authenticationStatus);
		}
		if (!StringUtils.isEmpty(status)) {
			criteria.andEqualTo("status", status);
		}
		example.setOrderByClause("create_time desc");
		List<CustomerBase> list = mapper.selectByExample(example);

		TableResultResponse<CustomerBase> table = new TableResultResponse<CustomerBase>(result.getTotal(), list);
		respMap.put("body", table);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【查询客户基础信息列表】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 重置密码
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月18日上午11:35:00
	 * @param resetPasswordRequest
	 * @return
	 */
	public Map<String, Object> resetPassword(ResetPasswordRequest resetPasswordRequest) {
		log.info("【重置密码】   请求参数：{}", EntityUtils.beanToMap(resetPasswordRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String customerId = resetPasswordRequest.getCustomerId();
		String phone = resetPasswordRequest.getPhone();
		String password = "123456";
		int count = 0;
		if ("0".equals(resetPasswordRequest.getType())) {
			// 登录密码
			CustomerBaseAuths customerBaseAuths = new CustomerBaseAuths();
			customerBaseAuths.setCredential(password);

			Example example = new Example(CustomerBaseAuths.class);
			Criteria criteria = example.createCriteria();
			criteria.andEqualTo("customerId", customerId);
			criteria.andEqualTo("authType", "00");// 00:登录授权
			criteria.andEqualTo("isthird", "00");// 00:站内登录
			count = customerBaseAuthsMapper.updateByExampleSelective(customerBaseAuths, example);

		} else if ("1".equals(resetPasswordRequest.getType())) {
			// 支付密码
			CustomerBase customerBase = mapper.selectByPrimaryKey(customerId);
			customerBase.setPayPassword(password);
			customerBase.setUpdateTime(new Timestamp(System.currentTimeMillis()));
			count = mapper.updateByPrimaryKey(customerBase);
		}

		if (count > 0) {
			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		} else {
			respMap.put("code", ResponseCode.PWD_RESET_ERROR.getCode());
			respMap.put("msg", ResponseCode.PWD_RESET_ERROR.getMessage());
		}

		log.info("【重置密码】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 修改客户状态
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月18日上午11:35:12
	 * @param updateCustomerStatusRequest
	 * @return
	 */
	public Map<String, Object> updateStatus(UpdateCustomerStatusRequest updateCustomerStatusRequest) {
		log.info("【修改客户状态】   请求参数：{}", EntityUtils.beanToMap(updateCustomerStatusRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String status = updateCustomerStatusRequest.getStatus();
		CustomerBase customerBase = new CustomerBase();
		customerBase.setCustomerId(updateCustomerStatusRequest.getCustomerId());
		customerBase.setUpdateTime(new Timestamp(System.currentTimeMillis()));
		if ("2".equals(updateCustomerStatusRequest.getType())) {
			// 登录密码状态
			customerBase.setPasswordStatus(status);
			mapper.updateByPrimaryKeySelective(customerBase);
		} else if ("3".equals(updateCustomerStatusRequest.getType())) {
			// 支付密码状态
			customerBase.setPayPasswordStatus(status);
			mapper.updateByPrimaryKeySelective(customerBase);
		} else if ("4".equals(updateCustomerStatusRequest.getType())) {
			// 客户状态
			customerBase.setStatus(status);
			mapper.updateByPrimaryKeySelective(customerBase);
		}
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【修改客户状态】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 查询单个客户基础信息详情
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月17日上午11:41:36
	 * @param merchantId
	 * @return
	 */
	public Map<String, Object> queryOneByKeys(String customerId) {
		log.info("【查询单个客户基础信息详情】   请求参数：{}", customerId);
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase customerBaseReq = new CustomerBase();
		customerBaseReq.setCustomerId(customerId);
		CustomerBase customerBase = mapper.selectOne(customerBaseReq);
		respMap.put("CustomerBase", customerBase);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【查询单个客户基础信息详情】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 添加客户基础信息
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:15
	 * @param CustomerBaseRequest
	 * @return
	 */
	public Map<String, Object> add(CustomerBaseRequest customerBaseRequest) {
		log.info("【添加客户基础信息】   请求参数：{}", EntityUtils.beanToMap(customerBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase customerBase = new CustomerBase();
		BeanUtils.copyProperties(customerBaseRequest, customerBase);
		mapper.insert(customerBase);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【添加客户基础信息】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 修改客户基础信息
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:22
	 * @param CustomerBaseRequest
	 * @return
	 */
	public Map<String, Object> update(CustomerBaseRequest customerBaseRequest) {
		log.info("【修改客户基础信息】   请求参数：{}", EntityUtils.beanToMap(customerBaseRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase CustomerBase = new CustomerBase();
		BeanUtils.copyProperties(customerBaseRequest, CustomerBase);
		mapper.updateByPrimaryKey(CustomerBase);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【修改客户基础信息】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 删除客户基础信息
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:30
	 * @param personalId
	 * @return
	 */
	public Map<String, Object> delete(
			@ApiParam("客户编号") @RequestParam(value = "customerId", required = true) String customerId) {
		log.info("【删除客户基础信息】   请求参数：{}", customerId);
		Map<String, Object> respMap = new HashMap<String, Object>();
		int count = mapper.deleteByPrimaryKey(customerId);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【删除客户基础信息】   响应参数：{}", respMap);
		return respMap;
	}

	public Map<String, Object> queryCustInfoAndAccountInfo(String merchantId) {
		Map<String, Object> result = new HashMap<>();
		MerchantBase merchantBase = new MerchantBase();
		merchantBase.setMerchantId(merchantId);
		merchantBase = merchantBaseMapper.selectOne(merchantBase);
		result.put("customerBase", merchantBase);

		MerchantAccount merchantAccount = new MerchantAccount();
		merchantAccount.setMerchantId(merchantId);
		List<MerchantAccount> accountList = merchantAccountMapper.select(merchantAccount);
		result.put("accountList", accountList);
		result.put("code", "0000");
		result.put("msg", "查询成功");
		return result;
	}

	/**
	 * 用户个人信息注册
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年9月7日下午1:59:51
	 * @param registerRequest
	 * @return
	 */
	public Map<String, Object> register(RegisterRequestParam registerRequest) {
		log.info("【用户个人信息注册】   请求参数：{}", EntityUtils.beanToMap(registerRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		String customerId = String.valueOf(SnowFlake.getId());
		String userId = String.valueOf(SnowFlake.getId());
		String roleType = registerRequest.getRoleType();

		ValidateSMSRequest validateSMSRequest = new ValidateSMSRequest();
		validateSMSRequest.setPhone(registerRequest.getPhone());
		validateSMSRequest.setCode(registerRequest.getSmsCode());
		respMap = smsBiz.validateSmsCode(validateSMSRequest);
		if (!ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			log.info("【用户个人信息注册】   响应参数：{}", respMap);
			return respMap;
		}
		CustomerBaseAuths customerBaseAuthsReq = new CustomerBaseAuths();
		customerBaseAuthsReq.setPlatformId(registerRequest.getPlatformId());
		customerBaseAuthsReq.setAuthType("00");
		customerBaseAuthsReq.setAuthMethod("phone");
		customerBaseAuthsReq.setIsthird("00");
		customerBaseAuthsReq.setIdentifier(registerRequest.getPhone());
		List<CustomerBaseAuths> CustomerBaseAuthsList = customerBaseAuthsMapper.select(customerBaseAuthsReq);
		if (null != CustomerBaseAuthsList && CustomerBaseAuthsList.size() > 0) {
			respMap.put("code", ResponseCode.USERINFO_ISEXIST.getCode());
			respMap.put("msg", ResponseCode.USERINFO_ISEXIST.getMessage());
			log.info("【用户个人信息注册】   响应参数：{}", respMap);
			return respMap;
		}

		String passwordSalt = ObjectUtils.toString((new Random().nextInt() * (99999 - 10000 + 1)) + 10000);
		String passwordCiphertext = MD5.md5(registerRequest.getPassword() + passwordSalt);

		CustomerBase customerBase = new CustomerBase();
		CustomerBaseAuths customerBaseAuths = new CustomerBaseAuths();

		customerBase.setPlatformId(registerRequest.getPlatformId());
		customerBase.setCustomerId(customerId);
		customerBase.setPasswordErrorCount(0);
		customerBase.setPayPasswordErrorCount(0);
		customerBase.setPasswordStatus("00");
		customerBase.setPayPasswordStatus("00");
		customerBase.setStatus("00");
		customerBase.setAuthenticationStatus("00");
		customerBase.setPasswordSalt(passwordSalt);
		customerBase.setCreateTime(new Timestamp(System.currentTimeMillis()));

		customerBaseAuths.setPlatformId(registerRequest.getPlatformId());
		customerBaseAuths.setCustomerId(customerId);
		customerBaseAuths.setAuthType("00");
		customerBaseAuths.setAuthMethod("phone");
		customerBaseAuths.setIsthird("00");
		customerBaseAuths.setIdentifier(registerRequest.getPhone());
		customerBaseAuths.setCredential(passwordCiphertext);

		if ("01".equals(roleType)) { // 个人信息注册
			PersonalBase personalBase = new PersonalBase();
			PersonalBaseExt personalBaseExt = new PersonalBaseExt();

			customerBase.setPersonalId(userId);

			customerBaseAuths.setCustomerType("CUST02");
			customerBaseAuths.setCustomerType("CUST02");

			personalBase.setPlatformId(registerRequest.getPlatformId());
			personalBase.setPersonalId(userId);
			personalBase.setCustomerId(customerId);
			personalBase.setPhone(registerRequest.getPhone());
			personalBase.setAuthenticationStatus("00");
			personalBase.setStatus("00");
			personalBase.setPersonalType("00");
			personalBase.setCreateTime(new Date());

			personalBaseExt.setPersonalId(userId);
			personalBaseExt.setDeviceId(registerRequest.getDeviceId());
			personalBaseExt.setDeviceType(registerRequest.getDeviceType());

			mapper.insertSelective(customerBase);
			customerBaseAuthsMapper.insertSelective(customerBaseAuths);
			personalBaseMapper.insertSelective(personalBase);
			personalBaseExtMapper.insertSelective(personalBaseExt);

		} else if ("02".equals(roleType)) { // 商户信息注册
			MerchantBase merchantBase = new MerchantBase();

			customerBase.setMerchantId(userId);

			customerBaseAuths.setCustomerType("CUST01");
			customerBaseAuths.setCustomerType("CUST01");

			merchantBase.setMerchantId(userId);
			merchantBase.setPlatformId(registerRequest.getPlatformId());
			merchantBase.setCustomerId(customerId);
			merchantBase.setMerchantLevel("00");
			merchantBase.setAuthenticationStatus("00");
			merchantBase.setStatus("00");
			merchantBase.setCreateTime(new Date());

			mapper.insertSelective(customerBase);
			customerBaseAuthsMapper.insertSelective(customerBaseAuths);
			merchantBaseMapper.insertSelective(merchantBase);
		}

		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【用户个人信息注册】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 用户登录
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年9月8日下午4:47:04
	 * @param loginRequest
	 * @return
	 */
	public Map<String, Object> login(LoginRequest loginRequest) {
		log.info("【用户登录】   请求参数：{}", EntityUtils.beanToMap(loginRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBaseAuths customerBaseAuthsReq = new CustomerBaseAuths();
		customerBaseAuthsReq.setPlatformId(loginRequest.getPlatformId());
		customerBaseAuthsReq.setAuthType("00");
		customerBaseAuthsReq.setAuthMethod("phone");
		customerBaseAuthsReq.setIsthird("00");
		customerBaseAuthsReq.setIdentifier(loginRequest.getPhone());
		List<CustomerBaseAuths> CustomerBaseAuthsList = customerBaseAuthsMapper.select(customerBaseAuthsReq);
		if (null != CustomerBaseAuthsList && CustomerBaseAuthsList.size() > 0) {
			CustomerBaseAuths customerBaseAuths = CustomerBaseAuthsList.get(0);
			CustomerBase customerBaseReq = new CustomerBase();
			customerBaseReq.setPlatformId(customerBaseAuths.getPlatformId());
			customerBaseReq.setCustomerId(customerBaseAuths.getCustomerId());
			CustomerBase customerBase = mapper.selectOne(customerBaseReq);
			if (null != customerBase) {
				String passwordCiphertext = MD5.md5(loginRequest.getPassword() + customerBase.getPasswordSalt());

				if (passwordCiphertext.equals(customerBaseAuths.getCredential())) {
					respMap.put("code", ResponseCode.OK.getCode());
					respMap.put("msg", ResponseCode.OK.getMessage());
				} else {
					respMap.put("code", ResponseCode.USERINFO_PWD_ERROR.getCode());
					respMap.put("msg", ResponseCode.USERINFO_PWD_ERROR.getMessage());
				}
			} else {
				respMap.put("code", ResponseCode.USERINFO_NOTEXIST.getCode());
				respMap.put("msg", ResponseCode.USERINFO_NOTEXIST.getMessage());
			}
		} else {
			respMap.put("code", ResponseCode.USERINFO_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.USERINFO_NOTEXIST.getMessage());
		}
		log.info("【用户登录】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * setupPayPassword 设置/重置交易密码
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午10:54:44
	 * @param customerSetupPasswordRequest
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> setupPayPassword(
			@RequestBody @Valid CustomerSetupPasswordRequest customerSetupPasswordRequest) {
		log.info("【设置/重置交易密码】   请求参数：{}", EntityUtils.beanToMap(customerSetupPasswordRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase customerBaseReq = new CustomerBase();
		customerBaseReq.setPlatformId(customerSetupPasswordRequest.getPlatformId());
		customerBaseReq.setCustomerId(customerSetupPasswordRequest.getCustomerId());
		CustomerBase customerBase = mapper.selectOne(customerBaseReq);
		if (null == customerBase) {
			respMap.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
			log.info("【设置/重置交易密码】   响应参数：{}", respMap);
			return respMap;
		}
		if (customerSetupPasswordRequest.getNewPassword().equals(customerSetupPasswordRequest.getConfirmPassword())) {
			customerBase.setPayPassword(customerSetupPasswordRequest.getNewPassword());
			customerBase.setPayPasswordErrorCount(0);
			customerBase.setPayPasswordStatus("00");
			mapper.updateByPrimaryKeySelective(customerBase);

			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		} else {
			respMap.put("code", ResponseCode.PAY_PWD_CONFIRM_ERROR.getCode());
			respMap.put("msg", ResponseCode.PAY_PWD_CONFIRM_ERROR.getMessage());
		}

		log.info("【设置/重置交易密码】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * verifyPayPassword 校验交易密码
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午10:54:37
	 * @param customerVerifyPasswordRequest
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> verifyPayPassword(
			@RequestBody @Valid CustomerVerifyPasswordRequest customerVerifyPasswordRequest) {
		log.info("【校验交易密码】   请求参数：{}", EntityUtils.beanToMap(customerVerifyPasswordRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase customerBaseReq = new CustomerBase();
		customerBaseReq.setPlatformId(customerVerifyPasswordRequest.getPlatformId());
		if (null == customerVerifyPasswordRequest.getCustomerId()) {
			PersonalBase personalBaseReq = new PersonalBase();
			personalBaseReq.setPlatformId(customerVerifyPasswordRequest.getPlatformId());
			personalBaseReq.setRealName(customerVerifyPasswordRequest.getRealName());
			personalBaseReq.setIdCard(customerVerifyPasswordRequest.getCredNo());
			PersonalBase personalBase = personalBaseMapper.selectOne(personalBaseReq);
			if (null == personalBase) {
				respMap.put("code", ResponseCode.PER_NOTEXIST.getCode());
				respMap.put("msg", ResponseCode.PER_NOTEXIST.getMessage());
				log.info("【校验交易密码】   响应参数：{}", respMap);
				return respMap;
			}
			customerBaseReq.setCustomerId(personalBase.getCustomerId());
		} else {
			customerBaseReq.setCustomerId(customerVerifyPasswordRequest.getCustomerId());
		}
		CustomerBase customerBase = mapper.selectOne(customerBaseReq);
		if (null == customerBase) {
			respMap.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
			log.info("【校验交易密码】   响应参数：{}", respMap);
			return respMap;
		}
		if (customerVerifyPasswordRequest.getPassword().equals(customerBase.getPayPassword())) {
			respMap.put("verifyFlag", true);
		} else {
			respMap.put("verifyFlag", false);
		}

		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【校验交易密码】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * updatePayPassword 修改交易密码
	 * 
	 * @author centerroot
	 * @time 创建时间:2018年9月25日上午10:54:30
	 * @param customerUpdatePasswordRequest
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> updatePayPassword(
			@RequestBody @Valid CustomerUpdatePasswordRequest customerUpdatePasswordRequest) {
		log.info("【修改交易密码】   请求参数：{}", EntityUtils.beanToMap(customerUpdatePasswordRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		CustomerBase customerBaseReq = new CustomerBase();
		customerBaseReq.setPlatformId(customerUpdatePasswordRequest.getPlatformId());
		customerBaseReq.setCustomerId(customerUpdatePasswordRequest.getCustomerId());
		CustomerBase customerBase = mapper.selectOne(customerBaseReq);
		if (null == customerBase) {
			respMap.put("code", ResponseCode.CUST_NOTEXIST.getCode());
			respMap.put("msg", ResponseCode.CUST_NOTEXIST.getMessage());
			log.info("【设置/重置交易密码】   响应参数：{}", respMap);
			return respMap;
		}
		if (!customerUpdatePasswordRequest.getPassword().equals(customerBase.getPayPassword())) {
			respMap.put("code", ResponseCode.PAY_PWD_ERROR.getCode());
			respMap.put("msg", ResponseCode.PAY_PWD_ERROR.getMessage());
			log.info("【设置/重置交易密码】   响应参数：{}", respMap);
			return respMap;
		}

		if (customerUpdatePasswordRequest.getNewPassword().equals(customerUpdatePasswordRequest.getConfirmPassword())) {
			customerBase.setPayPassword(customerUpdatePasswordRequest.getNewPassword());
			customerBase.setPayPasswordErrorCount(0);
			customerBase.setPayPasswordStatus("00");
			mapper.updateByPrimaryKeySelective(customerBase);

			respMap.put("code", ResponseCode.OK.getCode());
			respMap.put("msg", ResponseCode.OK.getMessage());
		} else {
			respMap.put("code", ResponseCode.PAY_PWD_CONFIRM_ERROR.getCode());
			respMap.put("msg", ResponseCode.PAY_PWD_CONFIRM_ERROR.getMessage());
		}

		log.info("【修改交易密码】   响应参数：{}", respMap);
		return respMap;
	}

	/**
	 * 会员实名信息列表查询
	 * @author centerroot
	 * @time 创建时间:2018年9月25日下午4:09:14
	 * @param customerUpdatePasswordRequest
	 * @return
	 */
	public Map<String, Object> queryCustRealNameList(CustomerRealNameRequest customerRealNameRequest) {
		log.info("【会员实名信息列表查询】   请求参数：{}", EntityUtils.beanToMap(customerRealNameRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		int page = customerRealNameRequest.getPage();
		int limit = customerRealNameRequest.getLimit();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("page", page);
		params.put("limit", limit);
		Query query = new Query(params);
		Page<Object> result = PageHelper.startPage(query.getPage(), query.getLimit());
		PersonalCertificationInfo certificationInfoReq = new PersonalCertificationInfo();
		certificationInfoReq.setPlatformId(customerRealNameRequest.getPlatformId());
		certificationInfoReq.setPersonalId(customerRealNameRequest.getPersonalId());
		List<PersonalCertificationInfo> certificationInfoList = personalCertificationInfoMapper.select(certificationInfoReq);
		TableResultResponse<PersonalCertificationInfo> table = new TableResultResponse<PersonalCertificationInfo>(result.getTotal(), certificationInfoList);
		respMap.put("body", table);
		respMap.put("code", ResponseCode.OK.getCode());
		respMap.put("msg", ResponseCode.OK.getMessage());
		log.info("【会员实名信息列表查询】   响应参数：{}", respMap);
		return respMap;
	}
}