package com.github.wxiaoqi.security.crm.core.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.crm.core.entity.UnfreezeJournal;
import com.github.wxiaoqi.security.crm.core.mapper.UnfreezeJournalMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 解冻流水表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class UnfreezeJournalBiz extends BaseBiz<UnfreezeJournalMapper,UnfreezeJournal> {
}