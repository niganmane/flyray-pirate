package com.github.wxiaoqi.security.crm.core.blockchain.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class KeyTest {
    public static void main(String[] args) {
        try {
            testKey();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void testKey() throws InvalidAlgorithmParameterException, NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, InvalidKeySpecException {
        String content = "Mock the world";
        Security.addProvider(new BouncyCastleProvider());
        // 创建椭圆曲线算法的密钥对生成器，算法为 ECDSA
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("ECDSA", BouncyCastleProvider.PROVIDER_NAME);
        // 椭圆曲线（EC）域参数设定
        // bitcoin 为什么会选择 secp256k1，详见：https://bitcointalk.org/index.php?topic=151120.0
        ECParameterSpec ecSpec = ECNamedCurveTable.getParameterSpec("secp256k1");
        keyPairGenerator.initialize(ecSpec, new SecureRandom());
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        BCECPrivateKey privateKey = (BCECPrivateKey) keyPair.getPrivate();
        BCECPublicKey publicKey = (BCECPublicKey) keyPair.getPublic();

        BASE64Encoder base64Encoder = new BASE64Encoder();
        BASE64Decoder base64Decoder = new BASE64Decoder();

//私钥签名 公钥验证
        byte[] signedbytes = sign(privateKey, content);
        System.out.println(base64Encoder.encode(signedbytes));
        System.out.println(verify(publicKey, signedbytes, content));
        assert (!verify(publicKey, signedbytes, content));
        System.out.println("============");

//转换出的私钥签名 使用同一个公钥解密
        byte[] loadedSignedbytes = sign(loadKey(privateKey), content);
        System.out.println(base64Encoder.encode(loadedSignedbytes));
        System.out.println(verify(publicKey, loadedSignedbytes, content));
        assert (verify(publicKey, loadedSignedbytes, content));
        System.out.println("============");

//使用新生成的私钥签名，使用同一个公钥解密
        KeyPair keyPair1 = keyPairGenerator.generateKeyPair();
        BCECPrivateKey privateKey1 = (BCECPrivateKey) keyPair1.getPrivate();
        System.out.println(verify(publicKey, sign(privateKey1, content), content));
        assert (!verify(publicKey, sign(privateKey1, content), content));
    }

    public static byte[] sign(BCECPrivateKey key, String content) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, SignatureException, UnsupportedEncodingException {
        Security.addProvider(new BouncyCastleProvider());
        Signature ecdsaSign = Signature.getInstance("SHA256withECDSA", BouncyCastleProvider.PROVIDER_NAME);
        ecdsaSign.initSign(key);

        ecdsaSign.update(content.getBytes("UTF-8"));
        return ecdsaSign.sign();
    }

    public static boolean verify(BCECPublicKey key, byte[] sig, String content) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, SignatureException, UnsupportedEncodingException {

        Security.addProvider(new BouncyCastleProvider());
        ECParameterSpec ecParameters = ECNamedCurveTable.getParameterSpec("secp256k1");
        KeyFactory keyFactory = KeyFactory.getInstance("ECDSA", BouncyCastleProvider.PROVIDER_NAME);
        Signature ecdsaVerify = Signature.getInstance("SHA256withECDSA", BouncyCastleProvider.PROVIDER_NAME);
        ecdsaVerify.initVerify(key);
        ecdsaVerify.update(content.getBytes("UTF-8"));
        return ecdsaVerify.verify(sig);
    }

    public static BCECPrivateKey loadKey(BCECPrivateKey privateKey) throws NoSuchProviderException, NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        BASE64Encoder base64Encoder = new BASE64Encoder();
        BASE64Decoder base64Decoder = new BASE64Decoder();
        byte[] privateKeyByte = privateKey.getEncoded();
        String privateKeyByteEncode = base64Encoder.encode(privateKeyByte);
        KeyFactory keyFactory = KeyFactory.getInstance("ECDSA", "BC");
        PKCS8EncodedKeySpec skSpec = new PKCS8EncodedKeySpec(base64Decoder.decodeBuffer(privateKeyByteEncode));
        BCECPrivateKey privateKey1 = (BCECPrivateKey) keyFactory.generatePrivate(skSpec);
        return privateKey1;
    }
}