package com.github.wxiaoqi.security.admin.biz;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.admin.entity.Dept;
import com.github.wxiaoqi.security.admin.mapper.DeptMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

/** 
* @author: bolei
* @date：2018年4月8日 下午4:05:54 
* @description：类说明
*/

@Service
@Transactional(rollbackFor = Exception.class)
public class DeptBiz extends BaseBiz<DeptMapper,Dept> {

	public void updateByPlatformId(Dept dept) {
		mapper.updateByPlatformId(dept);
	}
	public Map<String, Object> add(Map<String, Object> param)  throws Exception{
		Dept dept = EntityUtils.map2Bean(param, Dept.class);
		mapper.insert(dept);
		Map<String, Object> result = new HashMap<String, Object>();
		Dept deptOther = mapper.selectOne(dept);
		result.put("deptId", deptOther.getId());
    	result.put("code", ResponseCode.OK.getCode());
    	result.put("msg", ResponseCode.OK.getMessage());
    	result.put("success", true);
    	return result;
	}
	public Map<String, Object> selectByPlatformId(Map<String, Object> param) throws Exception {
		Dept dept = EntityUtils.map2Bean(param, Dept.class);
		Dept deptOther = mapper.selectOne(dept);
		Map<String, Object> deptMap = EntityUtils.beanToMap(deptOther);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("dept", deptMap);
    	result.put("code", ResponseCode.OK.getCode());
    	result.put("msg", ResponseCode.OK.getMessage());
    	result.put("success", true);
    	return result;
	}
	
	public Map<String, Object> update(Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
		String platformId = (String) param.get("platformId");
		String platformName = (String) param.get("platformName");
		
		Map<String, Object> deptPMap = new HashMap<String, Object>();
		deptPMap.put("parentId", 1);
		deptPMap.put("platformId",Long.valueOf(platformId));
		Dept dept = new Dept();
		dept.setPlatformId(Long.valueOf(platformId));
		dept.setParentId(1);
		Dept deptOther = mapper.selectOne(dept);
		deptOther.setName(platformName);
		mapper.updateByPrimaryKeySelective(deptOther);
    	result.put("code", ResponseCode.OK.getCode());
    	result.put("msg", ResponseCode.OK.getMessage());
    	result.put("success", true);
    	return result;
	}
}
