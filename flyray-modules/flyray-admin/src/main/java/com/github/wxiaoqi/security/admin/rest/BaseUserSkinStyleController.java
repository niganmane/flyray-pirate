package com.github.wxiaoqi.security.admin.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.admin.biz.BaseUserSkinStyleBiz;
import com.github.wxiaoqi.security.admin.entity.BaseUserSkinStyle;
import com.github.wxiaoqi.security.common.crm.request.BaseUserSkinStyleRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.EntityUtils;

@Controller
@RequestMapping("baseUserSkinStyle")
public class BaseUserSkinStyleController extends BaseController<BaseUserSkinStyleBiz,BaseUserSkinStyle> {
	
	/**
	 * 查询用户皮肤
	 * @author centerroot
	 * @time 创建时间:2018年8月31日上午10:42:03
	 * @param baseUserSkinStyleRequest
	 * @return
	 */
	@RequestMapping(value = "/queryObj", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryObj(@RequestBody @Valid BaseUserSkinStyleRequest baseUserSkinStyleRequest){
        return baseBiz.queryObj(baseUserSkinStyleRequest);
	}
	/**
	 * 添加或更新用户皮肤
	 * @author centerroot
	 * @time 创建时间:2018年8月30日下午3:44:50
	 * @param baseUserSkinStyleRequest
	 * @return
	 */
	@RequestMapping(value = "/addOrUpdate", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> addOrUpdate(@RequestBody @Valid BaseUserSkinStyleRequest baseUserSkinStyleRequest){
		return baseBiz.addOrUpdate(baseUserSkinStyleRequest);
	}
}