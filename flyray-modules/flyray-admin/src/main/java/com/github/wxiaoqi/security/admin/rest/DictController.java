package com.github.wxiaoqi.security.admin.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.biz.DictBiz;
import com.github.wxiaoqi.security.admin.entity.Dict;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.rest.BaseController;

/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 
**/

@RestController
@RequestMapping("dict")
public class DictController extends BaseController<DictBiz, Dict> {
	
	/**
	 * 根据类型查询字典表数据
	 * @author centerroot
	 * @time 创建时间:2018年8月16日下午2:59:31
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/selectByType",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> selectByType(@RequestBody Map<String, Object> param) {
		Map<String, Object> result = new HashMap<String, Object>();
		String type = (String) param.get("type");
		Dict dict = new Dict();
		dict.setType(type);
		List<Dict> dictList = baseBiz.selectList(dict);
		result.put("dictList", dictList);
		result.put("code", ResponseCode.OK.getCode());
		result.put("msg", ResponseCode.OK.getMessage());
		result.put("success", true);
		return result;
	}
}
