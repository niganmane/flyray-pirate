package com.github.wxiaoqi.security.admin.client;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.wxiaoqi.security.common.crm.request.BaseAreaRequestParam;

@FeignClient(value = "flyray-crm-core")
public interface BaseAreaFeginClient {

	/**
	 * 获取地区详情
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/baseArea/queryInfo",method = RequestMethod.POST)
	public Map<String, Object> queryBaseAreaInfo(BaseAreaRequestParam param);
}
