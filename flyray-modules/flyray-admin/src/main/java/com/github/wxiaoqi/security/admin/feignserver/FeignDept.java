package com.github.wxiaoqi.security.admin.feignserver;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.admin.biz.DeptBiz;
import com.github.wxiaoqi.security.admin.biz.RoleBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

@Controller
@RequestMapping("feign/dept")
public class FeignDept {
	@Autowired
	private DeptBiz deptBiz;
	
	@RequestMapping(value = "add",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addDept(@RequestBody Map<String, Object> param) {
		Map<String, Object> result = new HashMap<String, Object>();
    	try {
    		result = deptBiz.add(param);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			result.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			result.put("success", false);
		}
    	return result;
    }
	/**
	 * 根据平台编号查询平台对应的机构
	 */
	@RequestMapping(value = "selectByPlatformId",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> selectByPlatformId(@RequestBody Map<String, Object> param) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			result = deptBiz.selectByPlatformId(param);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			result.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			result.put("success", false);
		}
		return result;
	}
	
	
	@RequestMapping(value = "update",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateDept(@RequestBody Map<String, Object> param) {
		Map<String, Object> result = deptBiz.update(param);
    	return result;
    }
}
