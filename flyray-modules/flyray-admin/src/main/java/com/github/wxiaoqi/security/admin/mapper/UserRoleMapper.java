package com.github.wxiaoqi.security.admin.mapper;

import java.util.List;

import com.github.wxiaoqi.security.admin.entity.UserRole;
import tk.mybatis.mapper.common.Mapper;

public interface UserRoleMapper extends Mapper<UserRole> {
	
	List<UserRole> getUserRolesByUserId(Long id);
	
}