package com.github.wxiaoqi.security.admin.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 用户动态显示字段
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-30 15:31:30
 */
@Table(name = "base_user_table_field")
public class BaseUserTableField implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //用户ID
    @Column(name = "user_id")
    private Long userId;
	
	    //页面标签
    @Column(name = "page_tag")
    private String pageTag;
	
	    //显示字段
    @Column(name = "columns")
    private String columns;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	

	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：用户ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户ID
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * 设置：页面标签
	 */
	public void setPageTag(String pageTag) {
		this.pageTag = pageTag;
	}
	/**
	 * 获取：页面标签
	 */
	public String getPageTag() {
		return pageTag;
	}
	/**
	 * 设置：显示字段
	 */
	public void setColumns(String columns) {
		this.columns = columns;
	}
	/**
	 * 获取：显示字段
	 */
	public String getColumns() {
		return columns;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
