package com.github.wxiaoqi.security.admin.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询用户列表请求参数")
public class QueryUserBaseRequest {
	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;
	
	@ApiModelProperty(value = "平台编号")
	private String platformId;

	@ApiModelProperty(value = "个人客户编号")
	private String personalId;
	/**
	 * 用户权限1，系统管理员2，平台管理员3，商户管理员4、平台操作员
	 */
	@ApiModelProperty(value = "用户权限")
	private int userType;
	
}
