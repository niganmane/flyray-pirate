package com.github.wxiaoqi.security.admin.vo;

/**
 * 修改用户密码
 * @author centerroot
 * @time 创建时间:2018年8月27日下午2:59:35
 * @description
 */

public class UserPassword {

	private Integer id;
	private String password;
	private String newPassword;

	private String userId;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

}
