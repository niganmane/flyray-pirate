package com.github.wxiaoqi.security.admin.rest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *　@author bolei
 *　@date Apr 17, 2018
　*　@description 
**/

@RestController
@RequestMapping("test")
public class TestController {

	@RequestMapping(value = "/{id}",method = RequestMethod.GET)
    @ResponseBody
    public String get(@PathVariable int id){
        return String.valueOf(id);
    }
}
