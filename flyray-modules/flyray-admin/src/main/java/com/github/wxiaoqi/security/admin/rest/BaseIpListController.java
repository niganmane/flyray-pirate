package com.github.wxiaoqi.security.admin.rest;

import com.github.wxiaoqi.security.admin.biz.BaseIpListBiz;
import com.github.wxiaoqi.security.common.crm.request.QueryPersonalBaseListRequest;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.admin.entity.BaseIpList;
import com.github.wxiaoqi.security.admin.entity.User;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("ipList")
public class BaseIpListController extends BaseController<BaseIpListBiz,BaseIpList> {
	
	
}