package com.github.wxiaoqi.security.admin.config;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.ace.cache.api.CacheAPI;
import com.alibaba.fastjson.JSON;
import com.github.wxiaoqi.security.admin.mapper.BaseIpListMapper;


/**
 * 动态记载白名单信息
 */
@Component
@Aspect
@Slf4j
@Service
public class IpListLoadAspect {
	@Autowired 
	private BaseIpListMapper maper;
	/*@Autowired 
	private CacheAPI cacheApi;*/
	@Autowired
	private RedisTemplate redisTemplate;
	@AfterReturning(value="execution(* com.github.wxiaoqi.security.admin.mapper.BaseIpListMapper.update*(..)) or "
			+ "execution(* com.github.wxiaoqi.security.admin.mapper.BaseIpListMapper.insert*(..)) or "
			+ "execution(* com.github.wxiaoqi.security.admin.mapper.BaseIpListMapper.delete*(..))"
			, argNames="rtv", returning="rtv")
	public void loadIpList(JoinPoint jp, Object rtv){
		 log.info("重新加载ip白名单开始");
		 List<Map<String, Object>> ipList = maper.getAllBaseIpList();
		 if(null!=ipList&&ipList.size()>0){
			// cacheApi.set("ipList", ipList, 9999999);
			 redisTemplate.opsForValue().set("i_flyray-admin:ipList", ipList);
	    	}
		 log.info("重新加载ip白名单结束");
	}

	
}
