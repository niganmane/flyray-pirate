package com.github.wxiaoqi.security.admin.biz;

import com.github.wxiaoqi.security.admin.constant.AdminCommonConstant;
import com.github.wxiaoqi.security.admin.entity.ResourceAuthority;
import com.github.wxiaoqi.security.admin.mapper.ResourceAuthorityMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Ace on 2017/6/19.
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ResourceAuthorityBiz extends BaseBiz<ResourceAuthorityMapper,ResourceAuthority> {
	/**
	 * 方法中固定使用id为13（name“云支付平台管理员（本角色绝对不能删除）”）的角色作为基础角色
	 * @param param
	 */
	public Map<String, Object> copyResourceAuthority(Map<String, Object> param) throws Exception{
		Map<String, Object> result = new HashMap<String, Object>();
		Integer roleId = (Integer) param.get("roleId");
		Example example = new Example(ResourceAuthority.class);
        Criteria criteria = example.createCriteria();
        //方法中固定使用id为13（name“云支付平台管理员（本角色绝对不能删除）”）的角色作为基础角色,如果基础角色修改了，则此处修改
        criteria.andEqualTo("authorityId", "13");
        criteria.andEqualTo("resourceType", "menu");
        List<ResourceAuthority> list = mapper.selectByExample(example);
        for (ResourceAuthority resourceAuthority : list) {
        	ResourceAuthority authority = new ResourceAuthority(AdminCommonConstant.AUTHORITY_TYPE_GROUP, AdminCommonConstant.RESOURCE_TYPE_MENU);
            authority.setAuthorityId(roleId + "");
            authority.setResourceId(resourceAuthority.getResourceId());
            //默认-1
            authority.setParentId("-1");
            mapper.insertSelective(authority);
            /**
             * ResourceAuthority authority = new ResourceAuthority(AdminCommonConstant.AUTHORITY_TYPE_GROUP, AdminCommonConstant.RESOURCE_TYPE_BTN);
        authority.setAuthorityId(roleId + "");
        authority.setResourceId(elementId + "");
        authority.setParentId("-1");
             */
		}
        //插入资源
        Example example2 = new Example(ResourceAuthority.class);
        Criteria criteria2 = example2.createCriteria();
        //方法中固定使用id为13（name“云支付平台管理员（本角色绝对不能删除）”）的角色作为基础角色,如果基础角色修改了，则此处修改
        criteria2.andEqualTo("authorityId", "13");
        criteria2.andEqualTo("resourceType", "button");
        List<ResourceAuthority> listBtn = mapper.selectByExample(example2);
        for (ResourceAuthority resourceAuthority : listBtn) {
        	ResourceAuthority authority = new ResourceAuthority(AdminCommonConstant.AUTHORITY_TYPE_GROUP, AdminCommonConstant.RESOURCE_TYPE_BTN);
            authority.setAuthorityId(roleId + "");
            authority.setResourceId(resourceAuthority.getResourceId());
            //默认-1
            authority.setParentId("-1");
            mapper.insertSelective(authority);
		}
    	result.put("code", ResponseCode.OK.getCode());
    	result.put("msg", ResponseCode.OK.getMessage());
    	result.put("success", true);
    	return result;
	}
}
