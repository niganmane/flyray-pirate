package com.github.wxiaoqi.security.admin.feignserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.admin.biz.BaseIpListBiz;
import com.github.wxiaoqi.security.admin.entity.BaseIpList;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

@Controller
@RequestMapping("feign/ip")
public class FeignIp {
	@Autowired
	private BaseIpListBiz ipBiz;
	
	/**
	 * 获取白名单信息
	 */
	@RequestMapping(value = "list",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> getIplist() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<BaseIpList> ipList = ipBiz.selectListAll();
		
		result.put("ipList", ipList);
		result.put("code", ResponseCode.OK.getCode());
		result.put("msg", ResponseCode.OK.getMessage());
		result.put("success", false);
		return result;
	}
}
