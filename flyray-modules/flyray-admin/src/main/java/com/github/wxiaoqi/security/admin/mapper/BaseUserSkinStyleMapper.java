package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.BaseUserSkinStyle;

import tk.mybatis.mapper.common.Mapper;

/**
 * 用户皮肤样式
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-31 09:19:23
 */
@org.apache.ibatis.annotations.Mapper
public interface BaseUserSkinStyleMapper extends Mapper<BaseUserSkinStyle> {
	
}
