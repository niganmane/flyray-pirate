package com.github.wxiaoqi.security.admin.biz;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.admin.mapper.BaseIpListMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.admin.entity.BaseIpList;


/**
 * 白名单
 *
 * @author mu
 * @email ${email}
 * @date 2018-08-28 14:30:04
 */
@Service
public class BaseIpListBiz extends BaseBiz<BaseIpListMapper,BaseIpList> {
}