package com.github.wxiaoqi.security.admin.rest;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.biz.UserRoleBiz;
import com.github.wxiaoqi.security.admin.entity.UserRole;
import com.github.wxiaoqi.security.common.msg.ObjectRestResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 
**/

@RestController
@RequestMapping("userRole")
public class UserRoleController extends BaseController<UserRoleBiz, UserRole> {
	
	/**
     * 通过用户ID获取用户角色
     * @param id
     * @return
     */
    @RequestMapping(value = "/{userId}/roles", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse<List<UserRole>> getUserRoles(@PathVariable String userId){
    	Long usId = Long.valueOf(userId);
        return new ObjectRestResponse<List<UserRole>>().data(baseBiz.getUserRolesByUserId(usId));
        
    }
}
