package com.github.wxiaoqi.security.admin.entity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 白名单
 * 
 * @author mu
 * @email ${email}
 * @date 2018-08-28 14:30:04
 */
@Table(name = "base_ip_list")
public class BaseIpList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Integer id;
	
	    //平台编号
    @Column(name = "PLATFORM_ID")
    private String platformId;
	
	    //ip地址
    @Column(name = "IP")
    private String ip;
	
	    //业务类型
    @Column(name = "BUSINESS_TYPE")
    private String businessType;
	
	    //状态
    @Column(name = "STATUS")
    private String status;
	
	

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：ip地址
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}
	/**
	 * 获取：ip地址
	 */
	public String getIp() {
		return ip;
	}
	/**
	 * 设置：业务类型
	 */
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	/**
	 * 获取：业务类型
	 */
	public String getBusinessType() {
		return businessType;
	}
	/**
	 * 设置：状态
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：状态
	 */
	public String getStatus() {
		return status;
	}
	
}
