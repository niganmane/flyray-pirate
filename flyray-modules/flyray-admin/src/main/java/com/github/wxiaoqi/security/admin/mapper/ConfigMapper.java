package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.Config;
import tk.mybatis.mapper.common.Mapper;

public interface ConfigMapper extends Mapper<Config> {
}