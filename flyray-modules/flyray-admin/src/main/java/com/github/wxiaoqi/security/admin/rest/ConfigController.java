package com.github.wxiaoqi.security.admin.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.biz.ConfigBiz;
import com.github.wxiaoqi.security.admin.entity.Config;
import com.github.wxiaoqi.security.common.rest.BaseController;

/**
 *　@author bolei
 *　@date Apr 8, 2018
　*　@description 
**/
@RestController
@RequestMapping("config")
public class ConfigController extends BaseController<ConfigBiz, Config> {

}
