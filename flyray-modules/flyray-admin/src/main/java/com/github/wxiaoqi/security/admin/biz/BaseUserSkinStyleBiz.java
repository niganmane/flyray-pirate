package com.github.wxiaoqi.security.admin.biz;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.github.wxiaoqi.security.admin.entity.BaseUserSkinStyle;
import com.github.wxiaoqi.security.admin.mapper.BaseUserSkinStyleMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.BaseUserSkinStyleRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 用户皮肤样式
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-31 09:19:23
 */
@Slf4j
@Service
public class BaseUserSkinStyleBiz extends BaseBiz<BaseUserSkinStyleMapper,BaseUserSkinStyle> {
	
	/**
	 * 查询用户皮肤
	 * @author centerroot
	 * @time 创建时间:2018年8月31日上午10:42:03
	 * @param baseUserSkinStyleRequest
	 * @return
	 */
	public Map<String, Object> queryObj(BaseUserSkinStyleRequest baseUserSkinStyleRequest){
		log.info("【查询用户皮肤】   请求参数：{}",EntityUtils.beanToMap(baseUserSkinStyleRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		BaseUserSkinStyle baseUserSkinStyleReq = new BaseUserSkinStyle();
		baseUserSkinStyleReq.setPlatformId(baseUserSkinStyleRequest.getPlatformId());
		baseUserSkinStyleReq.setUserId(baseUserSkinStyleRequest.getUserId());
		
		List<BaseUserSkinStyle> baseUserSkinStyleList = mapper.select(baseUserSkinStyleReq);
		if (null != baseUserSkinStyleList && baseUserSkinStyleList.size() > 0) {
			BaseUserSkinStyle baseUserSkinStyle = baseUserSkinStyleList.get(0);
			respMap.put("baseUserSkinStyle", baseUserSkinStyle);
		}
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());

		log.info("【查询用户皮肤】   响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 添加或更新用户皮肤
	 * @author centerroot
	 * @time 创建时间:2018年8月31日上午10:41:47
	 * @param baseUserSkinStyleRequest
	 * @return
	 */
	public Map<String, Object> addOrUpdate(BaseUserSkinStyleRequest baseUserSkinStyleRequest){
		log.info("【添加或更新用户皮肤】   请求参数：{}",EntityUtils.beanToMap(baseUserSkinStyleRequest));
		Map<String, Object> respMap = new HashMap<String, Object>();
		BaseUserSkinStyle baseUserSkinStyleReq = new BaseUserSkinStyle();
		baseUserSkinStyleReq.setPlatformId(baseUserSkinStyleRequest.getPlatformId());
		baseUserSkinStyleReq.setUserId(baseUserSkinStyleRequest.getUserId());
		
		List<BaseUserSkinStyle> baseUserSkinStyleList = mapper.select(baseUserSkinStyleReq);
		if (null != baseUserSkinStyleList && baseUserSkinStyleList.size() > 0) {
			BaseUserSkinStyle baseUserSkinStyle = baseUserSkinStyleList.get(0);
			baseUserSkinStyle.setSkinStyle(baseUserSkinStyleRequest.getSkinStyle());
			baseUserSkinStyle.setUpdateTime(new Timestamp(System.currentTimeMillis()));
			mapper.updateByPrimaryKeySelective(baseUserSkinStyle);
			
		} else {
			baseUserSkinStyleReq.setSkinStyle(baseUserSkinStyleRequest.getSkinStyle());
			baseUserSkinStyleReq.setCreateTime(new Timestamp(System.currentTimeMillis()));
			mapper.insertSelective(baseUserSkinStyleReq);
		}
		respMap.put("code", ResponseCode.OK.getCode());
        respMap.put("msg", ResponseCode.OK.getMessage());

		log.info("【添加或更新用户皮肤】   响应参数：{}", respMap);
		return respMap;
	}
	
}