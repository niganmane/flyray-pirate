package com.github.wxiaoqi.security.admin.feignserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.admin.biz.DictBiz;
import com.github.wxiaoqi.security.admin.entity.Dict;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

@Controller
@RequestMapping("feign/dict")
public class FeignDict {
	@Autowired
	private DictBiz dictBiz;
	
	/**
	 * 根据类型查询字典表数据
	 * @author centerroot
	 * @time 创建时间:2018年8月16日下午2:59:31
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "selectByType",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> selectByType(@RequestBody Map<String, Object> param) {
		Map<String, Object> result = new HashMap<String, Object>();
		String type = (String) param.get("type");
		Dict dict = new Dict();
		dict.setType(type);
		List<Dict> dictList = dictBiz.selectList(dict);
		result.put("dictList", dictList);
		result.put("code", ResponseCode.OK.getCode());
		result.put("msg", ResponseCode.OK.getMessage());
		result.put("success", false);
		return result;
	}
}
