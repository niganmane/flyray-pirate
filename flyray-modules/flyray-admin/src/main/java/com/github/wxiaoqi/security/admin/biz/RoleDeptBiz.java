package com.github.wxiaoqi.security.admin.biz;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.admin.entity.RoleDept;
import com.github.wxiaoqi.security.admin.mapper.RoleDeptMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/** 
* @author: bolei
* @date：2018年4月8日 下午4:14:33 
* @description：类说明
*/

@Service
@Transactional(rollbackFor = Exception.class)
public class RoleDeptBiz extends BaseBiz<RoleDeptMapper, RoleDept> {

}
