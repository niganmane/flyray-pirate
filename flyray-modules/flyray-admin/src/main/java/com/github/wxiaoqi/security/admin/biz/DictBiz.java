package com.github.wxiaoqi.security.admin.biz;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.admin.entity.Dict;
import com.github.wxiaoqi.security.admin.mapper.DictMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/** 
* @author: bolei
* @date：2018年4月8日 下午4:09:57 
* @description：类说明
*/

@Service
@Transactional(rollbackFor = Exception.class)
public class DictBiz extends BaseBiz<DictMapper,Dict> {

}
