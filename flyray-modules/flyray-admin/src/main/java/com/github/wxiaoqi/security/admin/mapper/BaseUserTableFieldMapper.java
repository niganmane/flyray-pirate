package com.github.wxiaoqi.security.admin.mapper;

import com.github.wxiaoqi.security.admin.entity.BaseUserTableField;

import tk.mybatis.mapper.common.Mapper;

/**
 * 用户动态显示字段
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-30 15:31:30
 */
@org.apache.ibatis.annotations.Mapper
public interface BaseUserTableFieldMapper extends Mapper<BaseUserTableField> {
	
}
