package com.github.wxiaoqi.security.admin.feignserver;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.admin.biz.CommonBiz;

@Controller
@RequestMapping("feign/common")
public class FeignCommon {
	@Autowired
	private CommonBiz commonBiz;
	
	@RequestMapping(value = "addPlatformOrMerchant",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> addPlatformOrMerchant(@RequestBody Map<String, Object> param){
		Map<String, Object> result = commonBiz.addPlatformOrMerchant(param);
		return result;
	}
	
	@RequestMapping(value = "deletePlatformOrMerchant",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> deletePlatformOrMerchant(@RequestBody Map<String, Object> param){
		Map<String, Object> result = commonBiz.deletePlatformOrMerchant(param);
		return result;
	}
}
