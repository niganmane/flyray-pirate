package com.github.wxiaoqi.security.admin.feignserver;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.admin.biz.UserRoleBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

@Controller
@RequestMapping("feign/userRole")
public class FeignUserRole {
	@Autowired
	private UserRoleBiz userRoleBiz;
	
	@RequestMapping(value = "add",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addUserRole(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
    	try {
    		result = userRoleBiz.add(param);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			result.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			result.put("success", false);
		}
    	return result;
    }
}
