package com.github.wxiaoqi.security.admin.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.wxiaoqi.security.admin.entity.Role;
import com.github.wxiaoqi.security.admin.entity.UserRole;
import com.github.wxiaoqi.security.admin.mapper.UserRoleMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

/** 
* @author: bolei
* @date：2018年4月8日 下午4:17:59 
* @description：类说明
*/

@Service
@Transactional(rollbackFor = Exception.class)
public class UserRoleBiz extends BaseBiz<UserRoleMapper,UserRole> {

	public List<UserRole> getUserRolesByUserId(Long id) {
		return mapper.getUserRolesByUserId(id);
	}
	public Map<String, Object> add(Map<String, Object> param) throws Exception{
		UserRole userRole = EntityUtils.map2Bean(param, UserRole.class);
		mapper.insert(userRole);
		Map<String, Object> result = new HashMap<String, Object>();
    	result.put("code", ResponseCode.OK.getCode());
    	result.put("msg", ResponseCode.OK.getMessage());
    	result.put("success", true);
    	result.put("userRoleId", userRole.getId());
    	return result;
	}
}
