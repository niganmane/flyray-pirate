package com.github.wxiaoqi.security.admin.rest;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.wxiaoqi.security.admin.biz.DeptBiz;
import com.github.wxiaoqi.security.admin.biz.MenuBiz;
import com.github.wxiaoqi.security.admin.biz.RoleBiz;
import com.github.wxiaoqi.security.admin.biz.UserBiz;
import com.github.wxiaoqi.security.admin.client.BaseAreaFeginClient;
import com.github.wxiaoqi.security.admin.entity.Dept;
import com.github.wxiaoqi.security.admin.entity.Menu;
import com.github.wxiaoqi.security.admin.entity.Role;
import com.github.wxiaoqi.security.admin.entity.User;
import com.github.wxiaoqi.security.admin.entity.UserRole;
import com.github.wxiaoqi.security.admin.rpc.service.PermissionService;
import com.github.wxiaoqi.security.admin.rpc.service.UserRoleService;
import com.github.wxiaoqi.security.admin.rpc.service.UserService;
import com.github.wxiaoqi.security.admin.vo.FrontUser;
import com.github.wxiaoqi.security.admin.vo.MenuTree;
import com.github.wxiaoqi.security.admin.vo.UserPassword;
import com.github.wxiaoqi.security.admin.vo.UserVo;
import com.github.wxiaoqi.security.auth.common.config.UserAuthConfig;
import com.github.wxiaoqi.security.auth.common.util.SnowFlake;
import com.github.wxiaoqi.security.common.crm.request.BaseAreaRequestParam;
import com.github.wxiaoqi.security.common.crm.request.QueryPersonalBaseListRequest;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-08 11:51
 */
@RestController
@RequestMapping("user")
public class UserController extends BaseController<UserBiz,User> {
	
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private MenuBiz menuBiz;
    @Autowired
    private UserService userService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private RoleBiz roleBiz;
    @Autowired
    private DeptBiz deptBiz;
    @Autowired
	private UserAuthConfig userAuthConfig;
    @Autowired
    private BaseAreaFeginClient areaClient;
    
    
    @RequestMapping(value = "/front/info", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getUserInfo() throws Exception {
        FrontUser userInfo = permissionService.getUserInfo(request.getHeader(userAuthConfig.getTokenHeader()));
        if(userInfo==null) {
            return ResponseEntity.status(401).body(false);
        } else {
            return ResponseEntity.ok(userInfo);
        }
    }

    @RequestMapping(value = "/front/menus", method = RequestMethod.GET)
    public @ResponseBody
    List<MenuTree> getMenusByUsername() throws Exception {
        return permissionService.getMenusByUsername(request.getHeader(userAuthConfig.getTokenHeader()));
    }
    
    @RequestMapping(value = "/front/menu/all", method = RequestMethod.GET)
    public @ResponseBody
    List<Menu> getAllMenus() throws Exception {
        return menuBiz.selectListAll();
    }
    
    /**
	 * 新增用户并分配角色
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> addUser(@RequestBody UserVo req) throws Exception {
		User entity = new User();
		long userId = SnowFlake.getId();//商户号crc自校验数据 目的防止伪造造成脏数据
		BeanUtils.copyProperties(req, entity);
		entity.setUserId(userId);
		//根据角色获取平台编号
		Role role = roleBiz.selectById(Integer.valueOf(req.getRoleId()));
		Dept dept = deptBiz.selectById(role.getDeptId());
		Long platformId = dept.getPlatformId();
		entity.setPlatformId(String.valueOf(platformId));
		
		//地区
		BaseAreaRequestParam param = new BaseAreaRequestParam();
		param.setAreaCode(req.getAreaCode());
		Map<String, Object> respMap = areaClient.queryBaseAreaInfo(param);
		entity.setAreaCode((String) respMap.get("areaCode"));
		entity.setAreaLayer(String.valueOf((Integer) respMap.get("layer")));
		entity.setAreaName((String) respMap.get("name"));
		userService.addUser(entity);
		UserRole userRole = new UserRole();
		//目前只支持一个用户一个角色
		userRole.setRoleId(Integer.valueOf(req.getRoleId()));
		userRole.setUserId(userId);
		userRoleService.addUserRole(userRole);
        return ResponseEntity.ok(entity);
    }
    @RequestMapping(value = "/pageList",method = RequestMethod.POST)
    @ResponseBody
    public TableResultResponse<User> pageList(@RequestBody QueryPersonalBaseListRequest bean){
    	bean.setPlatformId(setPlatformId(bean.getPlatformId()));
        return baseBiz.pageList(bean);
    }
    
    /**
     * 修改用户密码
     * @author centerroot
     * @time 创建时间:2018年8月27日下午3:08:12
     * @param req
     * @return
     */
    @RequestMapping(value = "/updateUserPwd", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> updateUserPwd(@RequestBody UserPassword req) {
    	Map<String, Object> resp = baseBiz.updateUserPwd(req);
    	return resp;
    }
    
    /**
     * 重置用户密码
     * @author centerroot
     * @time 创建时间:2018年8月27日下午3:32:09
     * @param req
     * @return
     */
    @RequestMapping(value = "/resetUserPwd", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> resetUserPwd(@RequestBody UserPassword req) {
    	Map<String, Object> resp = baseBiz.resetUserPwd(req);
    	return resp;
    }
    
}
