package com.github.wxiaoqi.security.admin.feignserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.admin.biz.UserBiz;
import com.github.wxiaoqi.security.admin.entity.User;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

@Controller
@RequestMapping("feign/user")
public class FeignUser {
	@Autowired
	private UserBiz userBiz;
	
	@RequestMapping(value = "add",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> addUser(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
    	try {
    		result = userBiz.add(param);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			result.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			result.put("success", false);
		}
    	return result;
    }
	/**
	 * 根据条件查询用户
	 */
	@RequestMapping(value = "query",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> query(@RequestBody Map<String, Object> param){
		Map<String, Object> result = new HashMap<>();
		User entity = EntityUtils.map2Bean(param, User.class);
		List<User> list = userBiz.selectList(entity);
		result.put("list", list);
		result.put("code", ResponseCode.OK.getCode());
		result.put("msg", ResponseCode.OK.getMessage());
		return result;
	}
}
