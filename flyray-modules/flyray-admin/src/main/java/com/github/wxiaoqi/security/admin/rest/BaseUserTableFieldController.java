package com.github.wxiaoqi.security.admin.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.wxiaoqi.security.admin.biz.BaseUserTableFieldBiz;
import com.github.wxiaoqi.security.admin.entity.BaseUserTableField;
import com.github.wxiaoqi.security.common.crm.request.BaseUserTableFieldRequest;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.EntityUtils;

@Controller
@RequestMapping("baseUserTableField")
public class BaseUserTableFieldController extends BaseController<BaseUserTableFieldBiz,BaseUserTableField> {
	
	/**
	 * 查询用户显示字段
	 * @author centerroot
	 * @time 创建时间:2018年8月30日下午3:45:28
	 * @param baseUserTableFieldRequest
	 * @return
	 */
	@RequestMapping(value = "/queryObj", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryObj(@RequestBody @Valid BaseUserTableFieldRequest baseUserTableFieldRequest){
		return baseBiz.queryObj(baseUserTableFieldRequest);
	}
	/**
	 * 添加或更新用户显示字段数据
	 * @author centerroot
	 * @time 创建时间:2018年8月30日下午3:44:50
	 * @param baseUserTableFieldRequest
	 * @return
	 */
	@RequestMapping(value = "/addOrUpdate", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> addOrUpdate(@RequestBody @Valid BaseUserTableFieldRequest baseUserTableFieldRequest){
		return baseBiz.addOrUpdate(baseUserTableFieldRequest);
	}

}