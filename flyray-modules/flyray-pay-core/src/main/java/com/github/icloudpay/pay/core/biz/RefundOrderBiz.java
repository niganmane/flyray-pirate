package com.github.icloudpay.pay.core.biz;

import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.entity.RefundOrder;
import com.github.icloudpay.pay.core.mapper.RefundOrderMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;

/**
 * 退款订单表
 *
 * @author hexufeng
 * @email lfw6699@163.com
 * @date 2018-06-05 14:17:16
 */
@Service
public class RefundOrderBiz extends BaseBiz<RefundOrderMapper,RefundOrder> {
}