package com.github.icloudpay.pay.core.mapper;

import com.github.icloudpay.pay.core.entity.RefundOrder;
import tk.mybatis.mapper.common.Mapper;

/**
 * 退款订单表
 * 
 * @author hexufeng
 * @date 2018-06-05 14:17:16
 */
@org.apache.ibatis.annotations.Mapper
public interface RefundOrderMapper extends Mapper<RefundOrder> {
	
}
