package com.github.icloudpay.pay.core.rest;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.icloudpay.pay.core.biz.PayOrderBiz;
import com.github.icloudpay.pay.core.entity.PayOrder;
import com.github.wxiaoqi.security.common.cms.request.PayOrderQueryParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

@Controller
@RequestMapping("payOrders")
public class PayOrderController extends BaseController<PayOrderBiz, PayOrder> {

	
	private static Logger logger = LoggerFactory.getLogger(PayOrderController.class);
	/**
	 * 查询支付订单列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/list", method=RequestMethod.POST)
	public TableResultResponse<PayOrder> orderList(@RequestBody PayOrderQueryParam param) {
		logger.info("查询支付订单列表。。。{}"+param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return baseBiz.queryAllPayOrder(param);
	}
	
	/**
	 * 统计数据
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/count", method=RequestMethod.POST)
	public Map<String, Object> updateTxStatus(@RequestBody PayOrderQueryParam param) {
		logger.info("提现订单统计数据。。。{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		Map<String, Object> respMap = baseBiz.queryDataCount(param);
		return ResponseHelper.success(respMap, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
}
