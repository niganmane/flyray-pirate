package com.github.icloudpay.pay.core.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.icloudpay.pay.core.entity.PayOrder;
import com.github.icloudpay.pay.core.mapper.PayOrderMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.PayOrderQueryParam;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 支付订单表
 *
 * @author hexufeng
 * @email lfw6699@163.com
 * @date 2018-06-05 14:17:16
 */
@Service
public class PayOrderBiz extends BaseBiz<PayOrderMapper,PayOrder> {
	
	
	private static Logger logger = LoggerFactory.getLogger(PayOrderBiz.class);
	/**
	 * 支付订单列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<PayOrder> queryAllPayOrder(PayOrderQueryParam param) {
		logger.info("查询支付订单，请求参数.{}"+param);
		Example example = new Example(PayOrder.class);
		Criteria criteria = example.createCriteria();
		if(!StringUtils.isEmpty(param.getPlatformId())) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if(!StringUtils.isEmpty(param.getMerchantId())) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if(!StringUtils.isEmpty(param.getPayOrderNo())) {
			criteria.andEqualTo("payOrderNo",param.getPayOrderNo());
		}
		if(!StringUtils.isEmpty(param.getOrderStatus())) {
			criteria.andEqualTo("orderStatus",param.getOrderStatus());
		}
		example.setOrderByClause("pay_time desc");
		Page<PayOrder> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<PayOrder> list = mapper.selectByExample(example);
		logger.info("查询支付订单列表，响应参数.。。。{}"+list);
		TableResultResponse<PayOrder> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	/**
	 * 查询统计
	 * @return
	 */
	public Map<String, Object> queryDataCount(PayOrderQueryParam param) {
		String platformId = param.getPlatformId();
		String merchantId = param.getMerchantId();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("platformId", platformId);
		map.put("merchantId", merchantId);
		Map<String, Object> respMap = mapper.queryDataCount(map);
		return respMap;
	}
}