package com.github.icloudpay.pay.core.inter.schedule;

/**
 * 代付状态查询调度
 * @author hexufeng
 *
 */
public interface PayForAnotherStatusService {
	
	public void payForAnotherStatus();

}
