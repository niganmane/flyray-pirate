package com.github.icloudpay.pay.core.biz;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.entity.PayChannel;
import com.github.icloudpay.pay.core.mapper.PayChannelMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.PayChannelQueryParam;
import com.github.wxiaoqi.security.common.cms.request.PayChannelRequestParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.util.Query;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Slf4j
@Service
public class PayChannelBiz extends BaseBiz<PayChannelMapper, PayChannel> {

	
	/**
	 * 支付通道
	 * @param map
	 * @return
	 */
	public TableResultResponse<PayChannel> queryPayChannel(PayChannelQueryParam param) {
		log.info("查询支付通道，请求参数.{}"+param);
		Example example = new Example(PayChannel.class);
		Criteria criteria = example.createCriteria();
		if (param.getPayChannelNo() != null && param.getPayChannelNo().length() > 0) {
			criteria.andEqualTo("payChannelNo",param.getPayChannelNo());
		}
		if (param.getPayChannelName() != null && param.getPayChannelName().length() > 0) {
			criteria.andEqualTo("payChannelName",param.getPayChannelName());
		}
		example.setOrderByClause("create_time desc");
		if (param.getPage() == null) {
			param.setPage(1);
		}
		if (param.getLimit() == null) {
			param.setLimit(9999);
		}
		Page<PayChannel> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<PayChannel> list = mapper.selectByExample(example);
		log.info("查询支付通道列表，响应参数.。。。{}"+list);
		TableResultResponse<PayChannel> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	/**
	 * 详情
	 * @param param
	 * @return
	 */
	public Map<String, Object> payChannelInfo(PayChannelRequestParam param) {
		log.info("查询支付通道详情，请求参数.{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			PayChannel config = mapper.selectByPrimaryKey(Integer.valueOf(param.getId()));
			if (config != null) {
				respMap.put("info", config);
				respMap.put("code", "00");
				respMap.put("msg", "请求成功");
			}else {
				respMap.put("code", "01");
				respMap.put("msg", "通道不存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "请求异常");
		}
		return respMap;
	}
	
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	public Map<String, Object> deleteChannelInfo(PayChannelRequestParam param) {
		log.info("删除支付通道详情，请求参数.{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		PayChannel config = new PayChannel();
		try {
			BeanUtils.copyProperties(config, param);
			config.setId(Integer.valueOf(param.getId()));
			if (mapper.delete(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除支付通道】  响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【删除支付通道】 报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除支付通道】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【删除支付通道】   响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	public Map<String, Object> addPayChannel(PayChannelRequestParam params) {
		log.info("【添加支付通道】。。。。。start。。。{}"+params);
		Map<String, Object> respMap = new HashMap<>();
		PayChannel config = new PayChannel();
		try {
			BeanUtils.copyProperties(config, params);
			config.setCreateTime(new Date());
			config.setUpdateTime(new Date());
			if (mapper.insert(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加支付通道】  响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【添加支付通道】 报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加支付通道】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【添加支付通道】   响应参数：{}", respMap);
		return respMap;
	}
	
	
	/**
	 * 更新
	 * @param params
	 * @return
	 */
	public Map<String, Object> updatePayChannel(PayChannelRequestParam param) {
		log.info("更新支付通道。。。。。start。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		PayChannel config = new PayChannel();
		try {
			BeanUtils.copyProperties(config, param);
			config.setId(Integer.valueOf(param.getId()));
			if (mapper.updateByPrimaryKeySelective(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【修改支付通道】  响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【修改支付通道】 报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【修改支付通道】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【修改支付通道】   响应参数：{}", respMap);
		return respMap;
	}
}
