package com.github.icloudpay.pay.core.service.pay.alipay.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.service.pay.alipay.service.AliSignatureService;
import com.google.common.collect.Maps;


/* *
 *类名：AlipaySubmit
 *功能：支付宝各接口请求提交类
 *详细：构造支付宝各接口表单HTML文本，获取远程HTTP数据
 *版本：3.3
 *日期：2012-08-13
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

@Service("alipaySubmit")
public class AlipaySubmit {
    
	@Autowired
	private static AliSignatureService aliSignatureBiz;
    /**
     * 生成签名结果
     * @param sPara 要签名的数组
     * @return 签名结果字符串
     */
	public static String buildRequestMysign(Map<String, String> sPara,Map<String, Object> configMap) {
    	String prestr = AlipayCore.createLinkString(sPara); //把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
        return aliSignatureBiz.sign(prestr, configMap);
    }
	
    /**
     * 生成要请求给支付宝的参数数组
     * @param sParaTemp 请求前的参数数组
     * @return 要请求的参数数组
     */
    public static Map<String, String> buildRequestPara(Map<String, String> sParaTemp,Map<String, Object> configMap) {
        //除去数组中的空值和签名参数
        Map<String, String> sPara = AlipayCore.paraFilter(sParaTemp);
        //生成签名结果
        String mysign = buildRequestMysign(sPara,configMap);

        //签名结果与签名方式加入请求提交参数组中
        sPara.put("sign", mysign);
        sPara.put("sign_type", (String)configMap.get("encryptionMethod"));

        return sPara;
    }
    
    /**
     * 生成要请求给支付宝的参数数组
     * @param sParaTemp 请求前的参数数组
     * @return 要请求的参数数组
     */
    public static Map<String, Object> buildRequestParaObject(Map<String, String> sParaTemp,Map<String, Object> configMap) {
        //除去数组中的空值和签名参数
        Map<String, String> sPara = AlipayCore.paraFilter(sParaTemp);
        //生成签名结果
        String mysign = buildRequestMysign(sPara,configMap);

        //签名结果与签名方式加入请求提交参数组中
        sPara.put("sign", mysign);
        sPara.put("sign_type", (String)configMap.get("encryptionMethod"));

        return getMap(sPara);
    }
    
    public static Map<String, Object> getMap(Map<String, String> requestMap){
        Map<String,Object> returnMap = Maps.newHashMap();
        List<String> keys = new ArrayList<String>(requestMap.keySet());
        for (int i = 0; i < keys.size(); i++) {
            String name = keys.get(i);
            String value = requestMap.get(name);
            returnMap.put(name, value);
        }
        return returnMap;
    }

    
}
