package com.github.icloudpay.pay.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 支付助手app地址配置表
 * @author he
 */
@Table(name = "pay_helper_url_configuration")
public class PayHelperUrlConfiguration implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//商户编号
	@Column(name = "merchant_id")
	private String merchantId;

	//账号类型 ：支付宝 alipay 微信 wechat
	@Column(name = "number_type")
	private String numberType;

	//账号
	@Column(name = "number")
	private String number;

	//请求地址
	@Column(name = "request_url")
	private String requestUrl;
	
	//状态 00正常 01异常
	@Column(name = "tx_status")
	private String txStatus;


	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：商户编号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户编号
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * 设置：账号类型 ：支付宝 alipay 微信 wechat
	 */
	public void setNumberType(String numberType) {
		this.numberType = numberType;
	}
	/**
	 * 获取：账号类型 ：支付宝 alipay 微信 wechat
	 */
	public String getNumberType() {
		return numberType;
	}
	/**
	 * 设置：账号
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * 获取：账号
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * 设置：请求地址
	 */
	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}
	/**
	 * 获取：请求地址
	 */
	public String getRequestUrl() {
		return requestUrl;
	}
	public String getTxStatus() {
		return txStatus;
	}
	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}
}