package com.github.icloudpay.pay.core.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 获取支付通道配置信息
 * @author hexufeng
 *
 */
@FeignClient(value = "flyray-crm-core")
public interface PayChannelConfigFeign {
	
	/**
	 * 获取平台/商户支付通道配置
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/payChannelConfig/query",method = RequestMethod.POST)
	public Map<String, Object> query(@RequestBody Map<String, Object> req);
	
	/**
	 * 根据第三方商户号获取支付通道配置
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/payChannelConfig/queryByOutMerNo",method = RequestMethod.POST)
	public Map<String, Object> queryByOutMerNo(@RequestBody Map<String, Object> req);

}
