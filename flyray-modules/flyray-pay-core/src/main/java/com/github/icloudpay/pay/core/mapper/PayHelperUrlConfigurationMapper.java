package com.github.icloudpay.pay.core.mapper;

import java.util.Map;

import com.github.icloudpay.pay.core.entity.PayHelperUrlConfiguration;

import tk.mybatis.mapper.common.Mapper;

/**
 * 支付助手app地址配置表
 * @author he
 */
@org.apache.ibatis.annotations.Mapper
public interface PayHelperUrlConfigurationMapper extends Mapper<PayHelperUrlConfiguration> {
	
	/**
	 * 查询指定第几条，从0开始
	 * @param num
	 * @return
	 */
	public PayHelperUrlConfiguration queryConfigurationInfo(Map<String, Object> requestMap);
	
}