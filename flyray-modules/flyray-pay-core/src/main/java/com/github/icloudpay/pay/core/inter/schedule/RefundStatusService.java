package com.github.icloudpay.pay.core.inter.schedule;

/**
 * 退款状态查询调度
 * @author hexufeng
 *
 */
public interface RefundStatusService {

	public void RefundStatus();
	
}
