package com.github.icloudpay.pay.core.mapper;

import java.util.Map;

import com.github.icloudpay.pay.core.entity.PayOrder;
import tk.mybatis.mapper.common.Mapper;

/**
 * 支付订单表
 * 
 * @author hexufeng
 * @date 2018-06-05 14:17:16
 */
@org.apache.ibatis.annotations.Mapper
public interface PayOrderMapper extends Mapper<PayOrder> {
	
	
	Map<String, Object> queryDataCount(Map<String, Object> map);
}
