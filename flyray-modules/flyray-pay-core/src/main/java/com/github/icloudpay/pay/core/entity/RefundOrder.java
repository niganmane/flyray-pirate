package com.github.icloudpay.pay.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 退款订单表
 * 
 * @author hexufeng
 * @date 2018-06-05 14:17:16
 */
@Table(name = "refund_order")
public class RefundOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	//支付订单号
	@Column(name = "pay_order_no")
	private String payOrderNo;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	// 商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//退款订单号
	@Column(name = "refund_order_no")
	private String refundOrderNo;

	//退款金额
	@Column(name = "refund_amt")
	private BigDecimal refundAmt;

	//退款手续费
	@Column(name = "refund_fee")
	private BigDecimal refundFee;

	//退款时间
	@Column(name = "refund_time")
	private Date refundTime;

	//退款原因
	@Column(name = "refund_reason")
	private String refundReason;

	//退款方式（跟支付方式对应）
	@Column(name = "refund_method")
	private String refundMethod;

	//退款状态（00退款成功 01退款失败 02退款处理中 03退款申请成功）
	@Column(name = "refund_status")
	private String refundStatus;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置：支付订单号
	 */
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	/**
	 * 获取：支付订单号
	 */
	public String getPayOrderNo() {
		return payOrderNo;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：退款订单号
	 */
	public void setRefundOrderNo(String refundOrderNo) {
		this.refundOrderNo = refundOrderNo;
	}
	/**
	 * 获取：退款订单号
	 */
	public String getRefundOrderNo() {
		return refundOrderNo;
	}
	/**
	 * 设置：退款金额
	 */
	public void setRefundAmt(BigDecimal refundAmt) {
		this.refundAmt = refundAmt;
	}
	/**
	 * 获取：退款金额
	 */
	public BigDecimal getRefundAmt() {
		return refundAmt;
	}
	/**
	 * 设置：退款手续费
	 */
	public void setRefundFee(BigDecimal refundFee) {
		this.refundFee = refundFee;
	}
	/**
	 * 获取：退款手续费
	 */
	public BigDecimal getRefundFee() {
		return refundFee;
	}
	/**
	 * 设置：退款时间
	 */
	public void setRefundTime(Date refundTime) {
		this.refundTime = refundTime;
	}
	/**
	 * 获取：退款时间
	 */
	public Date getRefundTime() {
		return refundTime;
	}
	/**
	 * 设置：退款原因
	 */
	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}
	/**
	 * 获取：退款原因
	 */
	public String getRefundReason() {
		return refundReason;
	}
	/**
	 * 设置：退款方式（跟支付方式对应）
	 */
	public void setRefundMethod(String refundMethod) {
		this.refundMethod = refundMethod;
	}
	/**
	 * 获取：退款方式（跟支付方式对应）
	 */
	public String getRefundMethod() {
		return refundMethod;
	}
	/**
	 * 设置：退款状态（00退款成功 01退款失败 02退款处理中 03退款已发起）
	 */
	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}
	/**
	 * 获取：退款状态（00退款成功 01退款失败 02退款处理中 03退款已发起）
	 */
	public String getRefundStatus() {
		return refundStatus;
	}
}
