package com.github.icloudpay.pay.core.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.icloudpay.pay.core.entity.PayForAnotherOrder;
import com.github.icloudpay.pay.core.mapper.PayForAnotherOrderMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.PayForAnotherOrderRequestParam;
import com.github.wxiaoqi.security.common.cms.request.PayOrderQueryParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 代付
 * @author hexufeng
 * @email lfw6699@163.com
 * @date 2018-06-12 11:32:12
 */
@Service
public class PayForAnotherOrderBiz extends BaseBiz<PayForAnotherOrderMapper,PayForAnotherOrder> {
	
	private static Logger logger = LoggerFactory.getLogger(PayForAnotherOrderBiz.class);
	
	@Autowired
	private PayForAnotherOrderMapper payForAnotherOrderMapper;
	
	/**
	 * 查询处理中的代付订单数
	 * @param endDateTime
	 * @return
	 */
	public int getCountByDateTime(String endDateTime){
		return payForAnotherOrderMapper.getCountByDateTime(endDateTime);
	}
	
	/**
	 * 查询处理中的代付订单
	 * @param endDateTime
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<PayForAnotherOrder> queryPayForAnotherList(String endDateTime, int page, int limit){
		return payForAnotherOrderMapper.queryPayForAnotherList(endDateTime, page, limit);
	}
	
	/**
	 * 代付订单列表
	 * @param param
	 * @return
	 */
	public TableResultResponse<PayForAnotherOrder> queryAllPayForAnotherOrder(PayOrderQueryParam param) {
		logger.info("查询代付订单列表，请求参数.{}"+param);
		Example example = new Example(PayForAnotherOrder.class);
		Criteria criteria = example.createCriteria();
		if(!StringUtils.isEmpty(param.getPlatformId())) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		}
		if(!StringUtils.isEmpty(param.getMerchantId())) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		}
		if(!StringUtils.isEmpty(param.getOrderId())) {
			criteria.andEqualTo("orderId",param.getOrderId());
		}
		if(!StringUtils.isEmpty(param.getTxStatus())) {
			criteria.andEqualTo("txStatus",param.getTxStatus());
		}
		example.setOrderByClause("create_time desc");
		Page<PayForAnotherOrder> result = PageHelper.startPage(param.getPage(), param.getLimit());
		List<PayForAnotherOrder> list = mapper.selectByExample(example);
		logger.info("查询代付订单列表，响应参数.。。。{}"+list);
		TableResultResponse<PayForAnotherOrder> table = new TableResultResponse<>(result.getTotal(), list);
		return table;
	}
	
	
	/**
	 * 更新
	 * @param params
	 * @return
	 */
	public Map<String, Object> updatePayChannel(PayForAnotherOrderRequestParam param) {
		logger.info("更新提现状态。。。。。start。。。{}"+param);
		Map<String, Object> respMap = new HashMap<>();
		PayForAnotherOrder config = new PayForAnotherOrder();
		try {
			BeanUtils.copyProperties(config, param);
			config.setId(Integer.valueOf(param.getId()));
			config.setTxStatus("00");
			if (mapper.updateByPrimaryKeySelective(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				logger.info("【更新提现状态】  响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("【更新提现状态】 报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			logger.info("【更新提现状态】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		logger.info("【更新提现状态】   响应参数：{}", respMap);
		return respMap;
	}
	
	public Map<String, Object> queryData(PayOrderQueryParam param) {
		String platformId = param.getPlatformId();
		String merchantId = param.getMerchantId();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("platformId", platformId);
		map.put("merchantId", merchantId);
		Map<String, Object> respMap = mapper.queryDataCount(map);
		logger.info("查询统计结果。。。{}"+respMap);
		return respMap;
	}
	
}