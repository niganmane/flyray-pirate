package com.github.icloudpay.pay.core.util;

import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtil {
    
    public static String DEFAULT_DATE_PATTERN = "yyyyMMdd";
    
    /**
     * 默认时间格式HHmmss
     */
    public static String DEFAULT_TIME_PATTERN = "HHmmss";
    
    /**
     * 日期+时间的格式
     */
    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    
    /**
     * 日期+时间的格式
     */
    public static final String SIMPLE_DATE_TIME_PATTERN = "yyyyMMddHHmmss";
    
    public static java.util.Date toDate(String date,String pattern) throws ParseException{
        DateFormat myformat = new SimpleDateFormat(pattern);
        return myformat.parse(date);
    }
    
    public static Date toSqlDate(String date) {
        DateFormat myformat = new SimpleDateFormat(DEFAULT_DATE_PATTERN);
        try {
            return new java.sql.Date(myformat.parse(date).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    public static Time toTime(String time) {
        DateFormat myformat = new SimpleDateFormat(DEFAULT_TIME_PATTERN);
        try {
            return new Time(myformat.parse(time).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static java.util.Date toUtilDate(String date ,String time){
        DateFormat myformat = new SimpleDateFormat(DEFAULT_DATE_PATTERN + DEFAULT_TIME_PATTERN);
        try {
            return myformat.parse(date + time);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static java.util.Date toUtilDate(java.util.Date date,java.util.Date time){
        DateFormat dateformat = new SimpleDateFormat(DEFAULT_DATE_PATTERN);
        DateFormat timeformat = new SimpleDateFormat(DEFAULT_TIME_PATTERN);
        return toUtilDate( dateformat.format(date) , timeformat.format(time));
    }
    
    public static Date getCurrentDate(Long currentTime){
        return new Date(currentTime);
    }
    
    public static Time getCurrentTime(Long currentTime){
        return new Time(currentTime);
    }
    
    public static String toStringDate(java.util.Date date){
        DateFormat myformat = new SimpleDateFormat(DEFAULT_DATE_PATTERN);
        return myformat.format(date);
    }
    
    public static String toStringTime(java.util.Date date){
        DateFormat myformat = new SimpleDateFormat(DEFAULT_TIME_PATTERN);
        return myformat.format(date);
    }
    
    public static String getStandardDateTime() {
    	DateFormat sdf = new SimpleDateFormat(DATE_TIME_PATTERN);
    	return sdf.format(new java.util.Date());
    }
    
    public static java.util.Date getStandardDateTime(String dateTime) throws ParseException {
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_PATTERN);
	    return sdf.parse(dateTime);
    }

	public static String getSimpleDateTime() {
		SimpleDateFormat sdf = new SimpleDateFormat(SIMPLE_DATE_TIME_PATTERN);
		return sdf.format(new java.util.Date());
	}
	
	public static String getSimpleDateTime(java.util.Date date){
		SimpleDateFormat sdf = new SimpleDateFormat(SIMPLE_DATE_TIME_PATTERN);
		return sdf.format(date);
	}
	
	/**
	 * 获取标准的时间日期格式
	 * @param date
	 * @return
	 */
	public static String getStandardDateTime(java.util.Date date){
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_TIME_PATTERN);
	    return sdf.format(date);
	}
	
}
