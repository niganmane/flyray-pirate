package com.github.icloudpay.pay.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 支付流水表
 * 
 * @author hexufeng
 * @date 2018-06-05 14:17:16
 */	
@Table(name = "pay_serial")
public class PaySerial implements Serializable {
	private static final long serialVersionUID = 1L;

	//支付流水号
	@Id
	private String serialNo;

	//订单号
	@Column(name = "pay_order_no")
	private String payOrderNo;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	// 商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//支付渠道号
	@Column(name = "pay_channel_no")
	private String payChannelNo;

	//支付公司编号
	@Column(name = "pay_company_no")
	private String payCompanyNo;

	//交易金额
	@Column(name = "pay_amt")
	private BigDecimal payAmt;

	//交易手续费
	@Column(name = "pay_fee")
	private BigDecimal payFee;

	//支付状态（00支付成功 01支付失败 02待支付 03支付中 04作废）
	@Column(name = "pay_status")
	private String payStatus;


	/**
	 * 设置：支付流水号
	 */
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	/**
	 * 获取：支付流水号
	 */
	public String getSerialNo() {
		return serialNo;
	}
	/**
	 * 设置：订单号
	 */
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	/**
	 * 获取：订单号
	 */
	public String getPayOrderNo() {
		return payOrderNo;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：支付渠道号
	 */
	public void setPayChannelNo(String payChannelNo) {
		this.payChannelNo = payChannelNo;
	}
	/**
	 * 获取：支付渠道号
	 */
	public String getPayChannelNo() {
		return payChannelNo;
	}
	/**
	 * 设置：支付公司编号
	 */
	public void setPayCompanyNo(String payCompanyNo) {
		this.payCompanyNo = payCompanyNo;
	}
	/**
	 * 获取：支付公司编号
	 */
	public String getPayCompanyNo() {
		return payCompanyNo;
	}
	/**
	 * 设置：交易金额
	 */
	public void setPayAmt(BigDecimal payAmt) {
		this.payAmt = payAmt;
	}
	/**
	 * 获取：交易金额
	 */
	public BigDecimal getPayAmt() {
		return payAmt;
	}
	/**
	 * 设置：交易手续费
	 */
	public void setPayFee(BigDecimal payFee) {
		this.payFee = payFee;
	}
	/**
	 * 获取：交易手续费
	 */
	public BigDecimal getPayFee() {
		return payFee;
	}
	/**
	 * 设置：支付状态（00支付成功 01支付失败 02待支付 03支付中 04作废）
	 */
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	/**
	 * 获取：支付状态（00支付成功 01支付失败 02待支付 03支付中 04作废）
	 */
	public String getPayStatus() {
		return payStatus;
	}
}
