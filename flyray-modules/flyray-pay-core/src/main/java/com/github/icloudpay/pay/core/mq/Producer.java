package com.github.icloudpay.pay.core.mq;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 生产者
 * Created by zhongcy on 2017-02-22.
 */
@Component
public class Producer {
	
	private static final Logger logger = LoggerFactory.getLogger(Producer.class);


	@Autowired
	private static AmqpTemplate rabbitTemplate;

//	public static void send() {
//		String context = "hi, i am message all";
//		System.out.println("Sender : " + context);
//		this.rabbitTemplate.convertAndSend("topic.payment", context);
//	}
	
	public static void main(String[] args) {
		Map<String, Object> map = new HashMap<>();
		map.put("amount", "23");
		map.put("orderNo", "14715951683284992");
		map.put("platformId", "14376521810391040");
		map.put("tradeType", "1");
		map.put("txStatus", "00");
		String context = map.toString();
		rabbitTemplate.convertAndSend("topic.payment", context);
	}
}
