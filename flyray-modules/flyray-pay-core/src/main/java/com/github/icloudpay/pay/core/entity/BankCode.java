package com.github.icloudpay.pay.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 银行编码
 * @author hexufeng
 * @email lfw6699@163.com
 * @date 2018-06-12 11:32:12
 */
@Table(name = "bank_code")
public class BankCode implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //银行编码
    @Column(name = "bank_code")
    private String bankCode;
	
	    //银行名称
    @Column(name = "bank_name")
    private String bankName;
	

	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：银行编码
	 */
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	/**
	 * 获取：银行编码
	 */
	public String getBankCode() {
		return bankCode;
	}
	/**
	 * 设置：银行名称
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	/**
	 * 获取：银行名称
	 */
	public String getBankName() {
		return bankName;
	}
}
