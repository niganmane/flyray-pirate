package com.github.icloudpay.pay.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 支付订单表
 * 
 * @author hexufeng
 * @date 2018-06-05 14:17:16
 */
@Table(name = "pay_order")
public class PayOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	// 订单号
	@Column(name = "pay_order_no")
	private String payOrderNo;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//客户账号
	@Column(name = "customer_id")
	private String customerId;

	// 商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	//交易时间
	@Column(name = "pay_time")
	private Date payTime;

	//订单金额
	@Column(name = "order_amt")
	private BigDecimal orderAmt;

	//实际交易金额
	@Column(name = "pay_amt")
	private BigDecimal payAmt;

	//交易手续费
	@Column(name = "pay_fee")
	private BigDecimal payFee;

	//订单描述
	@Column(name = "body")
	private String body;

	//交易类型 1支付 2充值
	@Column(name = "pay_code")
	private String payCode;

	//支付方式（01支付宝 02微信）
	@Column(name = "pay_method")
	private String payMethod;

	//交易状态（00支付成功 01支付失败 02待支付 03支付中 04作废）
	@Column(name = "order_status")
	private String orderStatus;

	// 已退款金额
	@Column(name = "refunded_amt")
	private BigDecimal refundedAmt;
	
	// 支付宝微信账号
	@Column(name = "out_number")
	private String outNumber;

	//扩展map
	@Column(name = "ext_value")
	private String extValue;


	/**
	 * 设置：
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Long getId() {
		return id;
	}
	/**
	 * 设置： 订单号
	 */
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	/**
	 * 获取： 订单号
	 */
	public String getPayOrderNo() {
		return payOrderNo;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}

	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 设置：交易时间
	 */
	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}
	/**
	 * 获取：交易时间
	 */
	public Date getPayTime() {
		return payTime;
	}
	/**
	 * 设置：订单金额
	 */
	public void setOrderAmt(BigDecimal orderAmt) {
		this.orderAmt = orderAmt;
	}
	/**
	 * 获取：订单金额
	 */
	public BigDecimal getOrderAmt() {
		return orderAmt;
	}
	/**
	 * 设置：实际交易金额
	 */
	public void setPayAmt(BigDecimal payAmt) {
		this.payAmt = payAmt;
	}
	/**
	 * 获取：实际交易金额
	 */
	public BigDecimal getPayAmt() {
		return payAmt;
	}
	/**
	 * 设置：交易手续费
	 */
	public void setPayFee(BigDecimal payFee) {
		this.payFee = payFee;
	}
	/**
	 * 获取：交易手续费
	 */
	public BigDecimal getPayFee() {
		return payFee;
	}
	/**
	 * 设置：订单描述
	 */
	public void setBody(String body) {
		this.body = body;
	}
	/**
	 * 获取：订单描述
	 */
	public String getBody() {
		return body;
	}
	/**
	 * 设置：交易类型 1支付 2充值
	 */
	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}
	/**
	 * 获取：交易类型 1支付 2充值
	 */
	public String getPayCode() {
		return payCode;
	}
	/**
	 * 设置：支付方式（01支付宝 02微信）
	 */
	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}
	/**
	 * 获取：支付方式（01支付宝 02微信）
	 */
	public String getPayMethod() {
		return payMethod;
	}
	/**
	 * 设置：交易状态（00支付成功 01支付失败 02待支付 03支付中 04作废）
	 */
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	/**
	 * 获取：交易状态（00支付成功 01支付失败 02待支付 03支付中 04作废）
	 */
	public String getOrderStatus() {
		return orderStatus;
	}
	/**
	 * 设置： 已退款金额
	 */
	public void setRefundedAmt(BigDecimal refundedAmt) {
		this.refundedAmt = refundedAmt;
	}
	/**
	 * 获取： 已退款金额
	 */
	public BigDecimal getRefundedAmt() {
		return refundedAmt;
	}
	public String getOutNumber() {
		return outNumber;
	}
	public void setOutNumber(String outNumber) {
		this.outNumber = outNumber;
	}
	/**
	 * 设置：扩展map
	 */
	public void setExtValue(String extValue) {
		this.extValue = extValue;
	}
	/**
	 * 获取：扩展map
	 */
	public String getExtValue() {
		return extValue;
	}

}
