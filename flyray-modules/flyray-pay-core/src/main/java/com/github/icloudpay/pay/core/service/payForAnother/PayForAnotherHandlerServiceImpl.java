package com.github.icloudpay.pay.core.service.payForAnother;

import java.math.BigDecimal;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.entity.PayChannel;
import com.github.icloudpay.pay.core.entity.PayChannelInterface;
import com.github.icloudpay.pay.core.entity.PayForAnotherOrder;
import com.github.icloudpay.pay.core.inter.PayForAnotherHandlerService;
import com.github.icloudpay.pay.core.inter.PayForAnotherService;
import com.github.icloudpay.pay.core.mapper.PayChannelInterfaceMapper;
import com.github.icloudpay.pay.core.mapper.PayChannelMapper;
import com.github.icloudpay.pay.core.mapper.PayForAnotherOrderMapper;
import com.github.icloudpay.pay.core.util.SpringContextHolder;
import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherApplyRequest;
import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.PayForAnotherApplyResponse;
import com.github.wxiaoqi.security.common.admin.pay.response.PayForAnotherResponse;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;
import com.github.wxiaoqi.security.common.util.SequenceUtil;

/**
 * 代付逻辑处理
 * @author hexufeng
 *
 */
@Service("payForAnotherHandlerService")
public class PayForAnotherHandlerServiceImpl implements PayForAnotherHandlerService{
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(PayForAnotherHandlerServiceImpl.class);
	
	@Autowired
	private PayForAnotherOrderMapper payForAnotherOrderMapper;
	@Autowired
	private PayChannelMapper payChannelMapper;
	@Autowired
	private PayChannelInterfaceMapper payChannelInterfaceMapper;

	/**
	 * 代付申请
	 */
	@Override
	public PayForAnotherApplyResponse payForAnotherApply(PayForAnotherApplyRequest request) {
		logger.info("代付申请开始。。。。。。。{}",EntityUtils.beanToMap(request));
		PayForAnotherApplyResponse payForAnotherApplyResponse = new PayForAnotherApplyResponse();
		PayForAnotherOrder payForAnotherOrder = new PayForAnotherOrder();
		payForAnotherOrder.setOrderId(request.getOrderId());
		payForAnotherOrder.setPlatformId(request.getPlatformId());
		PayForAnotherOrder selectOrder = payForAnotherOrderMapper.selectOne(payForAnotherOrder);
		if(null != selectOrder){
			payForAnotherApplyResponse.setSuccess(false);
			payForAnotherApplyResponse.setCode(ResponseCode.ORDER_EXIST.getCode());
			payForAnotherApplyResponse.setMsg(ResponseCode.ORDER_EXIST.getMessage());
			return payForAnotherApplyResponse;
		}
		payForAnotherOrder.setOutOrderNo(SequenceUtil.Create27DigitalOrAlphabet());
		payForAnotherOrder.setAmount(new BigDecimal(request.getAmount()));
		payForAnotherOrder.setCustomerId(request.getCustomerId());
		payForAnotherOrder.setMerchantId(request.getMerchantId());
		payForAnotherOrder.setBankAccountName(request.getBankAccountName());
		payForAnotherOrder.setBankAccountNo(request.getBankAccountNo());
		payForAnotherOrder.setBankCode(request.getBankCode());
		payForAnotherOrder.setBankName(request.getBankName());
		payForAnotherOrder.setBankUnionCode(request.getBankUnionCode());
		payForAnotherOrder.setCreateTime(new Date());
		payForAnotherOrder.setSummary(request.getSummary());
		payForAnotherOrder.setTxStatus("03");//代付申请成功
		payForAnotherOrder.setFee(new BigDecimal(request.getPayFee()));
		payForAnotherOrderMapper.insert(payForAnotherOrder);
		payForAnotherApplyResponse.setOrderId(request.getOrderId());
		payForAnotherApplyResponse.setSuccess(true);
		payForAnotherApplyResponse.setCode(ResponseCode.OK.getCode());
		payForAnotherApplyResponse.setMsg(ResponseCode.OK.getMessage());
		logger.info("代付申请结束。。。。。。。");
		return payForAnotherApplyResponse;
	}
	
	/**
	 * 代付
	 */
	@Override
	public PayForAnotherResponse payForAnother(PayForAnotherRequest request) {
		logger.info("代付开始。。。。。。。{}",EntityUtils.beanToMap(request));
		PayForAnotherResponse payForAnotherResponse = new PayForAnotherResponse();
		PayForAnotherOrder payForAnotherOrder = new PayForAnotherOrder();
		payForAnotherOrder.setOrderId(request.getOrderId());
		payForAnotherOrder.setPlatformId(request.getPlatformId());
		PayForAnotherOrder selectOrder = payForAnotherOrderMapper.selectOne(payForAnotherOrder);
		if(null == selectOrder){
			payForAnotherResponse.setSuccess(false);
			payForAnotherResponse.setCode(ResponseCode.ORDER_NO_EXIST.getCode());
			payForAnotherResponse.setMsg(ResponseCode.ORDER_NO_EXIST.getMessage());
			return payForAnotherResponse;
		}
		
		PayChannel payChannel = new PayChannel();
		payChannel.setPayCompanyNo(request.getPayCompanyNo());
		payChannel.setPayChannelNo(request.getPayChannelNo());
		payChannel.setTradeType("01");;//代付
		PayChannel selectPayChannel = payChannelMapper.selectOne(payChannel);
		if(null == selectPayChannel){
			payForAnotherResponse.setSuccess(false);
			payForAnotherResponse.setCode(ResponseCode.NO_AVAILABLE_PAYCHANNEL.getCode());
			payForAnotherResponse.setMsg(ResponseCode.NO_AVAILABLE_PAYCHANNEL.getMessage());
			return payForAnotherResponse;
		}
		
		PayChannelInterface payChannelInterface = new PayChannelInterface();
		payChannelInterface.setPayChannelNo(selectPayChannel.getPayChannelNo());
		payChannelInterface.setTradeType("04");//代付
		PayChannelInterface selectInterface = payChannelInterfaceMapper.selectOne(payChannelInterface);
		if(null == selectInterface || null == selectInterface.getServiceName()){
			logger.error("查询代付通道接口为null,支付通道编号:{}", payChannel.getPayChannelNo());
			payForAnotherResponse.setSuccess(false);
			payForAnotherResponse.setCode(ResponseCode.INTERFACE_NOTEXIST.getCode());
			payForAnotherResponse.setMsg(ResponseCode.INTERFACE_NOTEXIST.getMessage());
			return payForAnotherResponse;
		}
		
		logger.info("查询代付通道接口,代付接口:{}", selectInterface.getServiceName());
		
		PayForAnotherService payForAnotherService = SpringContextHolder.getBean(selectInterface.getServiceName());
		
		request.setOutOrderNo(selectOrder.getOutOrderNo());
		request.setAmount(selectOrder.getAmount());
		request.setBankCode(selectOrder.getBankCode());
		request.setBankAccountNo(selectOrder.getBankAccountNo());
		request.setBankAccountName(selectOrder.getBankAccountName());
		request.setBankUnionCode(selectOrder.getBankUnionCode());
		request.setMerchantId(selectOrder.getMerchantId());
		request.setPayChannelNo(selectPayChannel.getPayChannelNo());
		request.setPayCompanyNo(selectPayChannel.getPayCompanyNo());
		// TODO 测试修改
		payForAnotherResponse = payForAnotherService.toPayForAnother(request);
		// TODO 测试添加
//		payForAnotherResponse.setStatus("0000");
//		payForAnotherResponse.setSuccess(true);
//		payForAnotherResponse.setCode(ResponseCode.OK.getCode());
//		payForAnotherResponse.setMsg(ResponseCode.OK.getMessage());
		
		if(ResponseCode.OK.getCode().equals(payForAnotherResponse.getCode())){
			if("0000".equals(payForAnotherResponse.getStatus())){
				selectOrder.setTxStatus("02");//代付处理中
			}else{
				selectOrder.setTxStatus("01");//代付失败
			}
			payForAnotherOrderMapper.updateByPrimaryKey(selectOrder);
		}else if("200".equals(payForAnotherResponse.getCode())){
			//线下打款
			selectOrder.setTxStatus("00");//代付成功
			payForAnotherResponse.setCode(ResponseCode.OK.getCode());
		}
		payForAnotherOrderMapper.updateByPrimaryKey(selectOrder);
		payForAnotherResponse.setAmount(String.valueOf(selectOrder.getAmount()));
		return payForAnotherResponse;
	}

}
