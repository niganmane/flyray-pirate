package com.github.icloudpay.pay.core.service.payForAnother.other.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.inter.PayForAnotherService;
import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.PayForAnotherResponse;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

/**
 * 线下代付
 * @author hexufeng
 */
@Service("offlinePayForAnotherService")
public class OfflinePayForAnotherService implements PayForAnotherService {
	
	private static final Logger logger = LoggerFactory.getLogger(OfflinePayForAnotherService.class);

	@Override
	public PayForAnotherResponse toPayForAnother(PayForAnotherRequest request) {
		
		logger.info("****************调用线下代付接口开始*******************");
		
		PayForAnotherResponse payForAnotherResponse = new PayForAnotherResponse();
		
		payForAnotherResponse.setCode("200");//手工提现当做成功处理
		payForAnotherResponse.setSuccess(true);
		payForAnotherResponse.setMsg(ResponseCode.OK.getMessage());
		
		return payForAnotherResponse;
	}

}