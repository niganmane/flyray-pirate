package com.github.icloudpay.pay.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 代付订单表
 * @author hexufeng
 * @email lfw6699@163.com
 * @date 2018-06-12 11:32:12
 */
@Table(name = "pay_for_another_order")
public class PayForAnotherOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	//序号
	@Id
	private Integer id;

	//代付订单号
	@Column(name = "order_id")
	private String orderId;

	//外部订单号
	@Column(name = "out_order_no")
	private String outOrderNo;

	//平台编号
	@Column(name = "platform_id")
	private String platformId;

	//代付金额
	@Column(name = "amount")
	private BigDecimal amount;

	//代付手续费
	@Column(name = "fee")
	private BigDecimal fee;

	//银行编码
	@Column(name = "bank_code")
	private String bankCode;
	
	//银行名称
	@Column(name = "bank_name")
	private String bankName;

	//银行卡号密文
	@Column(name = "bank_account_no")
	private String bankAccountNo;

	//银行账户名
	@Column(name = "bank_account_name")
	private String bankAccountName;
	
	//支付宝账号
	@Column(name = "aplipay_no")
	private String aplipayNo;

	//备注
	@Column(name = "summary")
	private String summary;

	//银行联行号
	@Column(name = "bank_union_code")
	private String bankUnionCode;

	//代付时间
	@Column(name = "create_time")
	private Date createTime;

	//代付状态 00成功 01失败 02代付处理中 03代付申请成功
	@Column(name = "tx_status")
	private String txStatus;

	//客户账号
	@Column(name = "customer_id")
	private String customerId;

	// 商户账号
	@Column(name = "merchant_id")
	private String merchantId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOutOrderNo() {
		return outOrderNo;
	}

	public void setOutOrderNo(String outOrderNo) {
		this.outOrderNo = outOrderNo;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccountNo() {
		return bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getBankUnionCode() {
		return bankUnionCode;
	}

	public void setBankUnionCode(String bankUnionCode) {
		this.bankUnionCode = bankUnionCode;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getTxStatus() {
		return txStatus;
	}

	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getAplipayNo() {
		return aplipayNo;
	}

	public void setAplipayNo(String aplipayNo) {
		this.aplipayNo = aplipayNo;
	}

}
