package com.github.icloudpay.pay.core.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * tcp通信支付请求
 * @author hexufeng
 */
@FeignClient(value = "flyray-netty")
public interface NettyFeign {
	
	/**
	 * 获取支付二维码
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "feign/getReceivablesQr",method = RequestMethod.POST)
	public Map<String, Object> getReceivablesQr(@RequestBody Map<String, Object> req);
}
