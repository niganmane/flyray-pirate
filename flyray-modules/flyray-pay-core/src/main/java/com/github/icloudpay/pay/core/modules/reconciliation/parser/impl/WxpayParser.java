package com.github.icloudpay.pay.core.modules.reconciliation.parser.impl;

import com.github.icloudpay.pay.core.modules.reconciliation.Vo.ReconciliationEntityVo;
import com.github.icloudpay.pay.core.modules.reconciliation.entity.ReconciliationBatch;
import com.github.icloudpay.pay.core.modules.reconciliation.parser.ParserInterface;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @Author: bolei
 * @date: 17:20 2018/10/17
 * @Description: 微信官方对账文件
 */

public class WxpayParser implements ParserInterface {
    @Override
    public List<ReconciliationEntityVo> parser(File file, Date billDate, ReconciliationBatch batch) throws IOException {
        return null;
    }
}
