package com.github.icloudpay.pay.core.rest;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.icloudpay.pay.core.biz.PayChannelInterfaceBiz;
import com.github.wxiaoqi.security.common.cms.request.PayChannelInterfaceRequestParam;
import com.github.wxiaoqi.security.common.cms.request.PayChannelQueryParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("payChannelInterfaces")
public class PayChannelInterfaceController {
	
	@Autowired
	private PayChannelInterfaceBiz biz;

	
	/**
	 * 修改支付通道接口
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> updatePayChannelInterface(@RequestBody PayChannelInterfaceRequestParam params) {
		log.info("更新支付通道接口，请求参数。。。{}"+params);
		Map<String, Object> respMap = biz.updateInterface(params);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("更新支付通道配置，响应参数.。。。{}"+respMap);
		return respMap;
	}
	
	
	/**
	 * 支付通道接口详情
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/info", method = RequestMethod.POST)
	public Map<String, Object> queryPayChannelInterfaceInfo(@RequestBody PayChannelQueryParam params) {
		log.info("查询支付通道接口，请求参数.。。。{}"+params);
		Map<String, Object> respMap = biz.queryList(params);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询支付通道配置，响应参数.。。。{}"+respMap);
		return respMap;
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Map<String, Object> addPayChannelInterfaceInfo(@RequestBody PayChannelInterfaceRequestParam params) {
		log.info("查询支付通道接口，请求参数.。。。{}"+params);
		Map<String, Object> respMap = biz.addInterface(params);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("查询支付通道配置，响应参数.。。。{}"+respMap);
		return respMap;
	}
	
	/**
	 * 删除
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> deletePayChannelInterfaceInfo(@RequestBody PayChannelInterfaceRequestParam params) {
		log.info("删除支付通道接口，请求参数.。。。{}"+params);
		Map<String, Object> respMap = biz.deleteInterface(params);
		if (ResponseCode.OK.getCode().equals(respMap.get("code"))) {
			respMap.put("success", true);
		}else {
			respMap.put("success", false);
		}
		log.info("删除支付通道配置，响应参数.。。。{}"+respMap);
		return respMap;
	}
}
