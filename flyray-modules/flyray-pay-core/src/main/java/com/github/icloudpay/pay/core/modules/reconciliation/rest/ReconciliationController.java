package com.github.icloudpay.pay.core.modules.reconciliation.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: bolei
 * @date: 17:39 2018/10/17
 * @Description: 对账请求入口
 */
@Slf4j
@Controller
@RequestMapping("/reconciliation")
public class ReconciliationController {

    /**
     * 手动对账上传对账文件
     *
     */

    /**
     * 展示历史对账批次信息
     *
     */
}
