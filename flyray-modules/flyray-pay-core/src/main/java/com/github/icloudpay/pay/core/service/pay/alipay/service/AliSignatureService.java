package com.github.icloudpay.pay.core.service.pay.alipay.service;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.service.pay.alipay.util.AliPayConfig;
import com.github.wxiaoqi.security.common.util.MD5;
import com.github.wxiaoqi.security.common.util.RSA;

/** 
* @author: bolei
* @date：2017年4月30日 下午3:11:59 
* @description：类说明 
*/
@Service("aliSignatureService")
public class AliSignatureService {

	private static final Logger logger = LoggerFactory.getLogger(AliSignatureService.class);
	
	@Autowired
	private AliPayConfig aliPayConfig;

	public String sign(String toBeSign, Map<String, Object> configMap) {
		logger.info("调用本地签名服务开始,待签名字符串:{},支付通道:{}", toBeSign, configMap.get("PayCompanyNo"));

		String sign = getSign(toBeSign, configMap);

		logger.info("调用本地签名服务完成,签名串:{}", sign);

		return sign;
	}

	private String getSign(String toBeSign, Map<String, Object> configMap) {
		if ("RSA".equalsIgnoreCase((String)configMap.get("encryptionMethod"))) {
			logger.info("支付宝RSA签名");
			return RSA.sign(toBeSign, (String)configMap.get("outMerPrivateKey"), "UTF-8");
		} else {
			logger.info("支付宝MD5签名");
			return MD5.sign(toBeSign, (String)configMap.get("outMerPrivateKey"), "UTF-8");
		}
	}

	public boolean verify(String toBeSign, String sign, Map<String, Object> configMap) {
		if ("RSA".equalsIgnoreCase((String)configMap.get("encryptionMethod"))) {
			return RSA.verify(toBeSign, sign, aliPayConfig.getAli_public_key(), "UTF-8");
		} else {
			return MD5.verify(toBeSign, sign, (String)configMap.get("outMerPrivateKey"), "UTF-8");
		}
	}
}
