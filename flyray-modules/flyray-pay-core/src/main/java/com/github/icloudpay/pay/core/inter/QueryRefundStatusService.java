package com.github.icloudpay.pay.core.inter;

import javax.xml.bind.JAXBException;

import com.github.wxiaoqi.security.common.admin.pay.request.QueryRefundStatusRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.QueryRefundStatusResponse;

public interface QueryRefundStatusService {
	  public QueryRefundStatusResponse queryRefundStatus(QueryRefundStatusRequest queryRefundStatusRequest) throws JAXBException;
}
