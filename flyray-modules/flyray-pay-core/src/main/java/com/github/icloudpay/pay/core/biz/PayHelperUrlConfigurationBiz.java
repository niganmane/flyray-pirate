package com.github.icloudpay.pay.core.biz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.icloudpay.pay.core.entity.PayHelperUrlConfiguration;
import com.github.icloudpay.pay.core.mapper.PayHelperUrlConfigurationMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.crm.request.MerchantPayForAnotherBankRequest;
import com.github.wxiaoqi.security.common.crm.request.PayHelperUrlConfigrationParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 支付助手app地址配置表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-10-28 17:13:17
 */
@Service
public class PayHelperUrlConfigurationBiz extends BaseBiz<PayHelperUrlConfigurationMapper,PayHelperUrlConfiguration> {
	
	/**
	 * 列表
	 * @param bean
	 * @return
	 */
	public TableResultResponse<PayHelperUrlConfiguration> payHelperUrlConfigurationList(MerchantPayForAnotherBankRequest param){
		 Example example = new Example(PayHelperUrlConfiguration.class);
		 Criteria criteria = example.createCriteria();
		 if(!StringUtils.isEmpty(param.getPlatformId())) {
			criteria.andEqualTo("platformId",param.getPlatformId());
		 }
		 if(!StringUtils.isEmpty(param.getMerchantId())) {
			criteria.andEqualTo("merchantId",param.getMerchantId());
		 }
		 Page<PayHelperUrlConfiguration> result = PageHelper.startPage(param.getPage(), param.getLimit());
		 List<PayHelperUrlConfiguration> list = mapper.selectByExample(example);
		 return new TableResultResponse<PayHelperUrlConfiguration>(result.getTotal(), list);
	}
	
	/** 
	 * 添加
	 */
	public Map<String, Object> addPayHelperUrlConfiguration(PayHelperUrlConfigrationParam param){
		Map<String, Object> respMap = new HashMap<String, Object>();
		try {
			PayHelperUrlConfiguration config = new PayHelperUrlConfiguration();
			BeanUtils.copyProperties(param, config);
			PayHelperUrlConfiguration payHelperUrlConfiguration = new PayHelperUrlConfiguration();
			payHelperUrlConfiguration.setPlatformId(config.getPlatformId());
			payHelperUrlConfiguration.setNumberType(config.getNumberType());
			payHelperUrlConfiguration.setNumber(config.getNumber());
			PayHelperUrlConfiguration selectOne = mapper.selectOne(payHelperUrlConfiguration);
			if(null != selectOne){
				respMap.put("code", ResponseCode.DEVICE_NUMBER_EXISTED.getCode());
				respMap.put("msg", ResponseCode.DEVICE_NUMBER_EXISTED.getMessage());
				return respMap;
			}
			if (mapper.insertSelective(config) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		}
		return respMap;
	}
	
	/**
	 * 删除
	 */
	public PayHelperUrlConfiguration deletePayHelperUrlConfiguration(PayHelperUrlConfigrationParam param){
		PayHelperUrlConfiguration config1 = null;
		try {
			PayHelperUrlConfiguration config = new PayHelperUrlConfiguration();
			BeanUtils.copyProperties(param, config);
			config.setId(Integer.valueOf(param.getId()));
			config1 = mapper.selectOne(config);
			mapper.delete(config);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config1;
	}

	
	/**
	 * 修改
	 */
	public PayHelperUrlConfiguration updatePayHelperUrlConfiguration(PayHelperUrlConfigrationParam param){
		PayHelperUrlConfiguration config1 = null;
		try {
			PayHelperUrlConfiguration config = new PayHelperUrlConfiguration();
			BeanUtils.copyProperties(param, config);
			config.setId(Integer.valueOf(param.getId()));
			config1 = mapper.selectOne(config);
			mapper.updateByPrimaryKey(config);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return config1;
	}
}