package com.github.icloudpay.pay.core.feignserver;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.icloudpay.pay.core.api.BaseController;
import com.github.icloudpay.pay.core.biz.RefundOrderBiz;
import com.github.icloudpay.pay.core.entity.RefundOrder;
import com.github.icloudpay.pay.core.inter.RefundHandlerService;
import com.github.wxiaoqi.security.common.admin.pay.request.QueryRefundOrderStatusRequest;
import com.github.wxiaoqi.security.common.admin.pay.request.RefundApplyRequest;
import com.github.wxiaoqi.security.common.admin.pay.request.RefundRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.QueryRefundOrderStatusResponse;
import com.github.wxiaoqi.security.common.admin.pay.response.RefundApplyResponse;
import com.github.wxiaoqi.security.common.admin.pay.response.RefundResponse;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

/**
 * 退款模块
 * @author hexufeng
 *
 */
@Controller
@RequestMapping("feign/refund")
public class RefundController extends BaseController{
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(RefundController.class);
	
	@Autowired
	private RefundHandlerService refundHandlerService;
	@Autowired
	private RefundOrderBiz refundOrderBiz;
	
	/**
	 * 退款申请
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/refundApply", method = RequestMethod.POST)
	@ResponseBody
    public RefundApplyResponse refundApply(@RequestBody Map<String, Object> params) throws Exception {
		RefundApplyRequest refundApplyRequest = EntityUtils.map2Bean(params, RefundApplyRequest.class);
		RefundApplyResponse refundApplyResponse = refundHandlerService.refundApply(refundApplyRequest);
		return refundApplyResponse;
    }
	
	/**
	 * 退款
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/refund", method = RequestMethod.POST)
	@ResponseBody
    public RefundResponse refund(@RequestBody Map<String, Object> params) throws Exception {
		RefundRequest refundRequest = EntityUtils.map2Bean(params, RefundRequest.class);
		RefundResponse refundResponse = refundHandlerService.refund(refundRequest);
		return refundResponse;
    }
	
	/**
	 * 退款订单状态查询
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/queryRefund", method = RequestMethod.POST)
	@ResponseBody
	public QueryRefundOrderStatusResponse queryRefund(@RequestBody Map<String, Object> params) throws Exception {
		QueryRefundOrderStatusRequest queryRefundOrderStatusRequest = EntityUtils.map2Bean(params, QueryRefundOrderStatusRequest.class);
		logger.info("退款订单状态查询请求开始。。。。。{}", EntityUtils.beanToMap(queryRefundOrderStatusRequest));
		QueryRefundOrderStatusResponse queryRefundOrderStatusResponse = new QueryRefundOrderStatusResponse();
		RefundOrder refundOrder = new RefundOrder();
		refundOrder.setPlatformId(queryRefundOrderStatusRequest.getPlatformId());
		refundOrder.setRefundOrderNo(queryRefundOrderStatusRequest.getRefundOrderNo());
		RefundOrder selectRefundOrder = refundOrderBiz.selectOne(refundOrder);
		if(null == selectRefundOrder){
			queryRefundOrderStatusResponse.setSuccess(false);
			queryRefundOrderStatusResponse.setCode(ResponseCode.ORDER_NO_EXIST.getCode());
			queryRefundOrderStatusResponse.setMsg(ResponseCode.ORDER_NO_EXIST.getMessage());
		}else{
			queryRefundOrderStatusResponse.setRefundStatus(selectRefundOrder.getRefundStatus());
			queryRefundOrderStatusResponse.setSuccess(true);
			queryRefundOrderStatusResponse.setCode(ResponseCode.OK.getCode());
			queryRefundOrderStatusResponse.setMsg(ResponseCode.OK.getMessage());
		}
		logger.info("退款订单状态查询请求结束。。。。。{}", EntityUtils.beanToMap(queryRefundOrderStatusResponse));
		return queryRefundOrderStatusResponse;
	}

}
