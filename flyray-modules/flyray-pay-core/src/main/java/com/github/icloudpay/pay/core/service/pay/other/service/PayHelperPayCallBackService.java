package com.github.icloudpay.pay.core.service.pay.other.service;

import java.math.BigDecimal;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.biz.PaySerialBiz;
import com.github.icloudpay.pay.core.entity.PaySerial;
import com.github.icloudpay.pay.core.service.pay.PayCallbackService;
import com.github.wxiaoqi.security.common.admin.pay.request.CallbackRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.PayHelperReponse;

/**
 * 支付助手app支付回调
 * @author he
 */
@Service("payHelperPayCallBackService")
public class PayHelperPayCallBackService {

	private static final Logger logger = (Logger) LoggerFactory.getLogger(PayHelperPayCallBackService.class);

	@Autowired
	private PaySerialBiz paySerialBiz;
	@Autowired
	private PayCallbackService payCallbackService;

	public PayHelperReponse payCallBack(Map<String, Object> request){

		PayHelperReponse callbackReponse = new PayHelperReponse();

		String out_trade_no = (String) request.get("out_trade_no");
		PaySerial paySerial = new PaySerial();
		paySerial.setSerialNo(out_trade_no);
		PaySerial selectPaySerial = paySerialBiz.selectOne(paySerial);
		if(selectPaySerial == null){
			logger.error("查询支付流水为空, out_trade_no{}",out_trade_no);
			callbackReponse.setReturn_code("FAIL");
			return callbackReponse;
		}

		String total_fee = (String) request.get("total_fee");
		String trade_no = (String) request.get("transaction_id");

		CallbackRequest callbackRequest = new CallbackRequest();
		callbackRequest.setRequestNo(out_trade_no);
		callbackRequest.setOrderAmt(new BigDecimal(total_fee));
		callbackRequest.setRemoteTxJournalNo(trade_no);

		//验证成功
		callbackRequest.setStatus("00");
		payCallbackService.callBack(callbackRequest);
		callbackReponse.setReturn_code("SUCCESS");
		return callbackReponse;
	}

}
