package com.github.icloudpay.pay.core.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.wxiaoqi.security.common.admin.pay.request.CompleteTradeRequest;

/**
 * 回调通知商户
 * @author hexufeng
 *
 */
@Async
@FeignClient(value = "flyray-crm-core")
public interface CallBackFeign {
	
	@RequestMapping(value = "feign/biz/callback",method = RequestMethod.POST)
	public Map<String, Object> callBack(CompleteTradeRequest completeTradeRequest);
}
