package com.github.icloudpay.pay.core.service.schedule;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.icloudpay.pay.core.biz.PayForAnotherOrderBiz;
import com.github.icloudpay.pay.core.entity.PayChannel;
import com.github.icloudpay.pay.core.entity.PayChannelInterface;
import com.github.icloudpay.pay.core.entity.PayForAnotherOrder;
import com.github.icloudpay.pay.core.inter.PayForAnotherQueryService;
import com.github.icloudpay.pay.core.inter.schedule.PayForAnotherStatusService;
import com.github.icloudpay.pay.core.mapper.PayChannelInterfaceMapper;
import com.github.icloudpay.pay.core.mapper.PayChannelMapper;
import com.github.icloudpay.pay.core.service.payForAnother.PayForAnotherCallBackService;
import com.github.icloudpay.pay.core.util.DateUtil;
import com.github.icloudpay.pay.core.util.SpringContextHolder;
import com.github.wxiaoqi.security.common.admin.pay.request.CallbackRequest;
import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherQueryRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.PayForAnotherQueryResponse;

/**
 * 代付状态查询调度
 * @author hexufeng
 *
 */
@Service("payForAnotherStatusService")
public class PayForAnotherStatusServiceImpl implements PayForAnotherStatusService{
	
	private static final Logger logger = LoggerFactory.getLogger(PayForAnotherStatusServiceImpl.class);
	
	@Autowired
	private PayForAnotherOrderBiz payForAnotherOrderBiz;
	@Autowired
	private PayForAnotherCallBackService payForAnotherCallBackService;
	@Autowired
	private PayChannelMapper payChannelMapper;
	@Autowired
	private PayChannelInterfaceMapper payChannelInterfaceMapper;

	@Override
	public void payForAnotherStatus() {
		
		String endDateTime = DateUtil.getStandardDateTime();
		
		logger.info("定时查询代付订单状态,时间为：{}",endDateTime);
		
		int payForAnotherOrderCount = payForAnotherOrderBiz.getCountByDateTime(endDateTime);
		if(payForAnotherOrderCount == 0){
			logger.info("时间为：{}之前,定时查询代付状态,代付订单表中不存在代付处理中的订单", endDateTime);
			return;
		}
		logger.info("需要去第三方代付公司查询的代付订单数量为：{}",payForAnotherOrderCount);
		
		int count = payForAnotherOrderCount%500==0?(payForAnotherOrderCount/500):((payForAnotherOrderCount/500)+1);
		for(int i = 0; i < count; i++){
			List<PayForAnotherOrder> payForAnotherList = payForAnotherOrderBiz.queryPayForAnotherList(endDateTime, 1, 500);
			if(payForAnotherList == null || payForAnotherList.size() == 0){
				logger.info("时间为：{}之前,定时查询代付状态,代付订单表中不存在代付处理中的订单", endDateTime);
				return;
			}
			for(int j = 0; j < payForAnotherList.size(); j++){
				ordStatusHandle(payForAnotherList.get(j));
			}
		}
	}
	
	/**
	 * 向第三方查询代付结果并更新状态
	 * @param payForAnotherOrder
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private void ordStatusHandle(PayForAnotherOrder payForAnotherOrder){
		
		PayChannel payChannel = new PayChannel();
		payChannel.setTradeType("2");;//代付
		PayChannel selectPayChannel = payChannelMapper.selectOne(payChannel);
		if(null == selectPayChannel){
			logger.error("查询代付通道接口为null");
			return;
		}
		
		PayChannelInterface payChannelInterface = new PayChannelInterface();
		payChannelInterface.setPayChannelNo(selectPayChannel.getPayChannelNo());
		payChannelInterface.setTradeType("05");//代付查询
		PayChannelInterface selectInterface = payChannelInterfaceMapper.selectOne(payChannelInterface);
		if(null == selectInterface || null == selectInterface.getServiceName()){
			logger.error("查询代付通道接口为null,支付通道编号:{}", selectPayChannel.getPayChannelNo());
			return;
		}
		
		logger.info("查询代付通道接口,代付接口:{}", selectInterface.getServiceName());
		
		PayForAnotherQueryService payForAnotherQueryService = SpringContextHolder.getBean(selectInterface.getServiceName());
		
		PayForAnotherQueryRequest payForAnotherQueryRequest = new PayForAnotherQueryRequest();
		payForAnotherQueryRequest.setMerId(payForAnotherOrder.getMerchantId());
		payForAnotherQueryRequest.setOutOrderNo(payForAnotherOrder.getOutOrderNo());
		payForAnotherQueryRequest.setPlatformId(payForAnotherOrder.getPlatformId());
		payForAnotherQueryRequest.setPayChannelNo(selectPayChannel.getPayChannelNo());
		payForAnotherQueryRequest.setPayCompanyNo(selectPayChannel.getPayCompanyNo());
		PayForAnotherQueryResponse payForAnotherQueryResponse = payForAnotherQueryService.payForAnotherQuery(payForAnotherQueryRequest);
		if(!payForAnotherQueryResponse.isSuccess()){
			logger.info("向第三方查询代付结果失败,订单号.....{}", payForAnotherOrder.getOrderId());
			return;
		}
		
		//回调通知
		CallbackRequest callbackRequest = new CallbackRequest();
		callbackRequest.setRequestNo(payForAnotherOrder.getOrderId());
		if("SUCCESS".equals(payForAnotherQueryResponse.getOrderStatus())){
			callbackRequest.setStatus("00");//成功
		}else if("FAIL".equals(payForAnotherQueryResponse.getOrderStatus()) || "REFUND".equals(payForAnotherQueryResponse.getOrderStatus())){
			callbackRequest.setStatus("01");//失败
		}
		payForAnotherCallBackService.callBack(callbackRequest);
	}

}
