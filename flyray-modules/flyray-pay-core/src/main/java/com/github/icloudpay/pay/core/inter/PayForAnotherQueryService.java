package com.github.icloudpay.pay.core.inter;

import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherQueryRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.PayForAnotherQueryResponse;

/**
 * 代付查询请求
 * @author hexufeng
 *
 */
public interface PayForAnotherQueryService {
	
	public PayForAnotherQueryResponse payForAnotherQuery(PayForAnotherQueryRequest payForAnotherQueryRequest);

}
