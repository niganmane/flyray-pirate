package com.github.icloudpay.pay.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.icloudpay.pay.core.interceptor.UserTokenInterceptor;


/**
 * Created by ace on 2017/9/12.
 */
@Configuration
public class FeignConfiguration {
    @Bean
    UserTokenInterceptor getClientTokenInterceptor(){
        return new UserTokenInterceptor();
    }
}
