package com.github.icloudpay.pay.core.service.pay;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.entity.PayChannel;
import com.github.icloudpay.pay.core.mapper.PayChannelMapper;
import com.github.wxiaoqi.security.common.util.EntityUtils;

/** 
* @author: bolei
* @date：2017年4月30日 上午11:45:45 
* @description： 支付智能路由
*/
@Service("payRouteService")
public class PayRouteService {
	
	private static final Logger logger = LoggerFactory.getLogger(PayRouteService.class);
	
	@Autowired
	private PayChannelMapper payChannelMapper;

	/**
	 * @param payChannelNo
	 * @param payCompanyNo
	 * 1、查询可用通道,查询条件  支付通道编号 payChannelNo
	 * 2、过滤支付方式 支付通道编号 payChannelNo 支付公司编号payCompanyNo
	 */
	public PayChannel getRoute(String payChannelNo,String payCompanyNo) {
		PayChannel payChannel = new PayChannel();
		payChannel.setPayChannelNo(payChannelNo);
		payChannel.setPayCompanyNo(payCompanyNo);
		payChannel.setTradeType("00");//支付
		PayChannel selectOne = payChannelMapper.selectOne(payChannel);
		logger.info("查询可用通道结果......{}", EntityUtils.beanToMap(selectOne));
		return selectOne;
	}
}
