package com.github.icloudpay.pay.core.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.icloudpay.pay.core.service.AliPayCallBackService;
import com.github.icloudpay.pay.core.service.pay.other.service.PayHelperPayCallBackService;
import com.github.icloudpay.pay.core.service.pay.wechat.service.WechatPayCallBackService;
import com.github.icloudpay.pay.core.service.refund.wechat.service.WechatRefundCallBackService;
import com.github.icloudpay.pay.core.util.XmlAnalysis;
import com.github.wxiaoqi.security.common.admin.pay.response.PayHelperReponse;
import com.github.wxiaoqi.security.common.admin.pay.response.WechatCallbackReponse;

/**
 * 回调模块
 * @author hexufeng
 *
 */
@Controller
@RequestMapping("callBack")
public class CallBackController extends BaseController{
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(CallBackController.class);
	
	@Autowired
	private WechatPayCallBackService wechatPayCallBackService;
	@Autowired
	private AliPayCallBackService aliPayCallBackService;
	@Autowired
	private WechatRefundCallBackService wechatRefundCallBackService;
	@Autowired
	private PayHelperPayCallBackService payHelperPayCallBackService;
	
	/**
	 * 微信支付回调
	 * @param request
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "/wechatPay",method = RequestMethod.POST)
    @ResponseBody
	public WechatCallbackReponse wechatPayCallBack(HttpServletRequest request) throws Exception{
    	Map<String, Object> param = XmlAnalysis.wechatCallBackXml(request);
		logger.info("微信支付回调请求参数。。。。。。。{}", param);
		WechatCallbackReponse respMap = wechatPayCallBackService.payCallBack(param);
		logger.info("微信支付回调响应参数。。。。。。。{}", respMap);
		return respMap;
	}
    
    /**
	 * 微信退款回调
	 * @param request
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "/wechatRefund",method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> wechatRefundCallBack(HttpServletRequest request) throws Exception{
    	Map<String, Object> param = XmlAnalysis.wechatCallBackXml(request);
    	logger.info("微信退款回调请求参数。。。。。。。{}", param);
    	WechatCallbackReponse respMap = wechatRefundCallBackService.refundBack(param);
    	logger.info("微信退款回调响应参数。。。。。。。{}", respMap);
    	return param;
    }
    
    /**
	 * 支付宝回调
	 * 支付退款共用一个方法
	 * @param request
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "/aliPay",method = RequestMethod.POST)
    @ResponseBody
	public String aliPayCallBack(Map<String, Object> param) throws Exception{
    	logger.info("支付宝回调请求参数。。。。。。。{}", param);
    	String response = aliPayCallBackService.callBack(param);
    	logger.info("支付宝回调响应参数。。。。。。。{}", response);
    	return response;
    }
    
    /**
	 * 支付助手app回调
	 * @param request
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "/payHelperPay",method = RequestMethod.POST)
    @ResponseBody
	public PayHelperReponse payHelperPay(@RequestBody Map<String, Object> param) throws Exception{
		logger.info("微信支付回调请求参数。。。。。。。{}", param);
		PayHelperReponse respMap = payHelperPayCallBackService.payCallBack(param);
		logger.info("微信支付回调响应参数。。。。。。。{}", respMap);
		return respMap;
	}
}
