package com.github.icloudpay.pay.core;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.github.wxiaoqi.security.auth.client.EnableAceAuthClient;
import com.spring4all.swagger.EnableSwagger2Doc;

/**
 * ${DESCRIPTION}
 *
 * @author bolei
 * @create 2018-4-2 20:30
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"com.github.wxiaoqi.security.auth.client.feign","com.github.icloudpay.pay.core.feign"})
@EnableScheduling
@ComponentScan({"com.github.icloudpay.pay.core","com.github.wxiaoqi.security.cache"})
@EnableAceAuthClient
@MapperScan("com.github.icloudpay.pay.core.mapper")
@EnableSwagger2Doc
public class FlyrayPayBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(FlyrayPayBootstrap.class, args);
    }
}
