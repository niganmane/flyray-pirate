package com.github.icloudpay.pay.core.inter;

import com.github.wxiaoqi.security.common.admin.pay.request.QueryPayStatusRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.QueryPayStatusResponse;

public interface QueryPayStatusService {

    public QueryPayStatusResponse queryPayStatus(QueryPayStatusRequest request);
    
}
