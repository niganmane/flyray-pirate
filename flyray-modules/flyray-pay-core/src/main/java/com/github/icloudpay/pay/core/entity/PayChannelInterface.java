package com.github.icloudpay.pay.core.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 支付通道接口配置
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-06 13:58:35
 */
@Table(name = "pay_channel_interface")
public class PayChannelInterface implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //序号
    @Id
    private Integer id;
	
	    //支付通道编号
    @Column(name = "pay_channel_no")
    private String payChannelNo;
	
	    //交易类型（仅做展示）  00：支付  01：支付查询  02：退款  03：退款查询 04:代付 05:代付查询
    @Column(name = "trade_type")
    private String tradeType;
	
	    //接口名称
    @Column(name = "service_name")
    private String serviceName;
	
	    //创建时间
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @Column(name = "update_time")
    private Date updateTime;
	

	/**
	 * 设置：序号
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：序号
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：支付通道编号
	 */
	public void setPayChannelNo(String payChannelNo) {
		this.payChannelNo = payChannelNo;
	}
	/**
	 * 获取：支付通道编号
	 */
	public String getPayChannelNo() {
		return payChannelNo;
	}
	/**
	 * 设置：交易类型（仅做展示）  00：支付  01：支付查询  02：退款  03：退款查询 04:代付 05:代付查询
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	/**
	 * 获取：交易类型（仅做展示）  00：支付  01：支付查询  02：退款  03：退款查询 04:代付 05:代付查询
	 */
	public String getTradeType() {
		return tradeType;
	}
	/**
	 * 设置：接口名称
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	/**
	 * 获取：接口名称
	 */
	public String getServiceName() {
		return serviceName;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	@Override
	public String toString() {
		return "PayChannelInterface [id=" + id + ", payChannelNo=" + payChannelNo + ", tradeType=" + tradeType
				+ ", serviceName=" + serviceName + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}
	
}
