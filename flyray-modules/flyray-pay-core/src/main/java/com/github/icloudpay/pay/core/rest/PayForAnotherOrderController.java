package com.github.icloudpay.pay.core.rest;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.icloudpay.pay.core.biz.PayForAnotherOrderBiz;
import com.github.icloudpay.pay.core.entity.PayForAnotherOrder;
import com.github.wxiaoqi.security.common.cms.request.PayForAnotherOrderRequestParam;
import com.github.wxiaoqi.security.common.cms.request.PayOrderQueryParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

@Controller
@RequestMapping("payForAnotherOrders")
public class PayForAnotherOrderController extends BaseController<PayForAnotherOrderBiz, PayForAnotherOrder> {

	private static Logger logger = LoggerFactory.getLogger(PayForAnotherOrderController.class);
	
	/**
	 * 查询代付订单列表
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/list", method=RequestMethod.POST)
	public TableResultResponse<PayForAnotherOrder> orderList(@RequestBody PayOrderQueryParam param) {
		logger.info("查询代付订单列表。。。{}"+param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return baseBiz.queryAllPayForAnotherOrder(param);
	}
	
	/**
	 * 修改状态
	 * @param param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public Map<String, Object> updateTxStatus(@RequestBody PayForAnotherOrderRequestParam param) {
		logger.info("修改状态。。。{}"+param);
		Map<String, Object> respMap = baseBiz.updatePayChannel(param);
		return ResponseHelper.success(respMap, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
	/**
	 * 统计数据
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/count", method=RequestMethod.POST)
	public Map<String, Object> updateTxStatus(@RequestBody PayOrderQueryParam param) {
		logger.info("提现统计数据。。。{}", param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		Map<String, Object> respMap = baseBiz.queryData(param);
		return ResponseHelper.success(respMap, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
}
