package com.github.icloudpay.pay.core.mapper;

import java.util.List;
import java.util.Map;

import com.github.icloudpay.pay.core.entity.PayForAnotherOrder;

import tk.mybatis.mapper.common.Mapper;

/**
 * 代付订单表
 * @author hexufeng
 * @email lfw6699@163.com
 * @date 2018-06-12 11:32:12
 */
@org.apache.ibatis.annotations.Mapper
public interface PayForAnotherOrderMapper extends Mapper<PayForAnotherOrder> {
	
	/**
	 * 查询处理中的代付订单数
	 * @param endDateTime
	 * @return
	 */
	public int getCountByDateTime(String endDateTime);
	
	/**
	 * 查询处理中的代付订单
	 * @param endDateTime
	 * @param page
	 * @param limit
	 * @return
	 */
	public List<PayForAnotherOrder> queryPayForAnotherList(String endDateTime, int page, int limit);
	
	/**
	 * 统计
	 * @return
	 */
	Map<String, Object> queryDataCount(Map<String, Object> map);
	
}
