package com.github.icloudpay.pay.core.feignserver;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.icloudpay.pay.core.api.BaseController;
import com.github.icloudpay.pay.core.biz.PayOrderBiz;
import com.github.icloudpay.pay.core.entity.PayOrder;
import com.github.icloudpay.pay.core.inter.PaymentHandlerService;
import com.github.wxiaoqi.security.common.admin.pay.request.CreateOrderRequest;
import com.github.wxiaoqi.security.common.admin.pay.request.PayOrderRequest;
import com.github.wxiaoqi.security.common.admin.pay.request.QueryPayOrderStatusRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.CreateOrderResponse;
import com.github.wxiaoqi.security.common.admin.pay.response.PayOrderResponse;
import com.github.wxiaoqi.security.common.admin.pay.response.QueryPayOrderStatusResponse;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

/**
 * 支付模块
 * @author hexufeng
 *
 */
@Controller
@RequestMapping("feign/payment")
public class PaymentController extends BaseController{
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(PaymentController.class);
	
	@Autowired
	private PaymentHandlerService paymentHandlerService;
	@Autowired
	private PayOrderBiz payOrderBiz;
	
	/**
	 * 创建支付订单
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/creatPayOrder", method = RequestMethod.POST)
	@ResponseBody
    public CreateOrderResponse creatPayOrder(@RequestBody Map<String, Object> params) throws Exception {
		CreateOrderRequest createOrderRequest = EntityUtils.map2Bean(params, CreateOrderRequest.class);
		CreateOrderResponse createOrderResponse = paymentHandlerService.createOrder(createOrderRequest);
		return createOrderResponse;
    }
	
	/**
	 * 支付
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/doPay", method = RequestMethod.POST)
	@ResponseBody
    public PayOrderResponse doPay(@RequestBody Map<String, Object> params) throws Exception {
		PayOrderRequest payOrderRequest = EntityUtils.map2Bean(params, PayOrderRequest.class);
		PayOrderResponse payOrderResponse = paymentHandlerService.pay(payOrderRequest);
		return payOrderResponse;
    }
	
	/**
	 * 支付订单状态查询
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/queryPay", method = RequestMethod.POST)
	@ResponseBody
	public QueryPayOrderStatusResponse queryPay(@RequestBody Map<String, Object> params) throws Exception {
		QueryPayOrderStatusRequest queryPayOrderStatusRequest = EntityUtils.map2Bean(params, QueryPayOrderStatusRequest.class);
		logger.info("支付订单状态查询请求开始。。。。。{}", EntityUtils.beanToMap(queryPayOrderStatusRequest));
		QueryPayOrderStatusResponse queryPayOrderStatusResponse = new QueryPayOrderStatusResponse();
		PayOrder payOrder = new PayOrder();
		payOrder.setPlatformId(queryPayOrderStatusRequest.getPlatformId());
		payOrder.setPayOrderNo(queryPayOrderStatusRequest.getPayOrderNo());
		PayOrder selectPayOrder = payOrderBiz.selectOne(payOrder);
		if(null == selectPayOrder){
			queryPayOrderStatusResponse.setSuccess(false);
			queryPayOrderStatusResponse.setCode(ResponseCode.ORDER_NO_EXIST.getCode());
			queryPayOrderStatusResponse.setMsg(ResponseCode.ORDER_NO_EXIST.getMessage());
		}else{
			queryPayOrderStatusResponse.setPayStatus(selectPayOrder.getOrderStatus());
			queryPayOrderStatusResponse.setSuccess(true);
			queryPayOrderStatusResponse.setCode(ResponseCode.OK.getCode());
			queryPayOrderStatusResponse.setMsg(ResponseCode.OK.getMessage());
		}
		logger.info("支付订单状态查询请求结束。。。。。{}", EntityUtils.beanToMap(queryPayOrderStatusResponse));
		return queryPayOrderStatusResponse;
	}
	
}
