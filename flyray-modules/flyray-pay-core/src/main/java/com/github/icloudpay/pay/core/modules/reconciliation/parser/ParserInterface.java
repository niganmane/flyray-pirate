package com.github.icloudpay.pay.core.modules.reconciliation.parser;


import com.github.icloudpay.pay.core.modules.reconciliation.Vo.ReconciliationEntityVo;
import com.github.icloudpay.pay.core.modules.reconciliation.entity.ReconciliationBatch;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @Author: bolei
 * @date: 17:06 2018/10/17
 * @Description: 解析导入对账文件接口
 */

public interface ParserInterface {

    public List<ReconciliationEntityVo> parser(File file, Date billDate, ReconciliationBatch batch) throws IOException;
}
