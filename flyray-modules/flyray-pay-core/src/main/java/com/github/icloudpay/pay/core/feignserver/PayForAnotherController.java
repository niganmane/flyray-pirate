package com.github.icloudpay.pay.core.feignserver;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.icloudpay.pay.core.api.BaseController;
import com.github.icloudpay.pay.core.biz.BankCodeBiz;
import com.github.icloudpay.pay.core.biz.PayForAnotherOrderBiz;
import com.github.icloudpay.pay.core.entity.PayForAnotherOrder;
import com.github.icloudpay.pay.core.inter.PayForAnotherHandlerService;
import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherApplyRequest;
import com.github.wxiaoqi.security.common.admin.pay.request.PayForAnotherRequest;
import com.github.wxiaoqi.security.common.admin.pay.request.QueryPayForAnotherOrderStatusRequest;
import com.github.wxiaoqi.security.common.admin.pay.response.BankCode;
import com.github.wxiaoqi.security.common.admin.pay.response.PayForAnotherApplyResponse;
import com.github.wxiaoqi.security.common.admin.pay.response.PayForAnotherResponse;
import com.github.wxiaoqi.security.common.admin.pay.response.QueryBankCodeResponse;
import com.github.wxiaoqi.security.common.admin.pay.response.QueryPayForAnotherOrderStatusResponse;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.util.EntityUtils;

/**
 * 代付模块
 * @author hexufeng
 *
 */
@Controller
@RequestMapping("feign/payForAnother")
public class PayForAnotherController extends BaseController{
	
	private static final Logger logger = (Logger) LoggerFactory.getLogger(PayForAnotherController.class);
	
	@Autowired
	private BankCodeBiz bankCodeBiz;
	@Autowired
	private PayForAnotherOrderBiz payForAnotherOrderBiz;
	@Autowired
	private PayForAnotherHandlerService payForAnotherHandlerService;
	
	/**
	 * 银行列表查询
	 * @return
	 * @throws Exception 
	 */ 
	@RequestMapping(value = "/queryBankCode", method = RequestMethod.POST)
	@ResponseBody
    public QueryBankCodeResponse queryBankCode() throws Exception {
		QueryBankCodeResponse queryBankCodeResponse = new QueryBankCodeResponse();
		List<BankCode> bankCodeList = bankCodeBiz.queryBankCode();
		queryBankCodeResponse.setBankList(bankCodeList);
		queryBankCodeResponse.setSuccess(true);
		queryBankCodeResponse.setCode(ResponseCode.OK.getCode());
		queryBankCodeResponse.setMsg(ResponseCode.OK.getMessage());
		return queryBankCodeResponse;
    }
	
	/**
	 * 代付申请
	 * 创建代付订单
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/payForAnotherApply", method = RequestMethod.POST)
	@ResponseBody
    public PayForAnotherApplyResponse payForAnotherApply(@RequestBody Map<String, Object> params) throws Exception {
		PayForAnotherApplyRequest payForAnotherApplyRequest = EntityUtils.map2Bean(params, PayForAnotherApplyRequest.class);
		PayForAnotherApplyResponse payForAnotherApplyResponse = payForAnotherHandlerService.payForAnotherApply(payForAnotherApplyRequest);
		return payForAnotherApplyResponse;
    }
	
	/**
	 * 代付
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/payForAnother", method = RequestMethod.POST)
	@ResponseBody
    public PayForAnotherResponse payForAnother(@RequestBody Map<String, Object> params) throws Exception {
		PayForAnotherRequest payForAnotherRequest = EntityUtils.map2Bean(params, PayForAnotherRequest.class);
		PayForAnotherResponse payForAnotherResponse = payForAnotherHandlerService.payForAnother(payForAnotherRequest);
		return payForAnotherResponse;
    }
	
	/**
	 * 代付订单状态查询
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/queryPayForAnother", method = RequestMethod.POST)
	@ResponseBody
	public QueryPayForAnotherOrderStatusResponse queryPayForAnother(@RequestBody Map<String, Object> params) throws Exception {
		QueryPayForAnotherOrderStatusRequest queryPayForAnotherOrderStatusRequest = EntityUtils.map2Bean(params, QueryPayForAnotherOrderStatusRequest.class);
		logger.info("代付订单状态查询请求开始。。。。。{}", EntityUtils.beanToMap(params));
		QueryPayForAnotherOrderStatusResponse queryPayForAnotherOrderStatusResponse = new QueryPayForAnotherOrderStatusResponse();
		PayForAnotherOrder payForAnotherOrder = new PayForAnotherOrder();
		payForAnotherOrder.setOrderId(queryPayForAnotherOrderStatusRequest.getOrderId());
		payForAnotherOrder.setPlatformId(queryPayForAnotherOrderStatusRequest.getPlatformId());
		PayForAnotherOrder selectOrder = payForAnotherOrderBiz.selectOne(payForAnotherOrder);
		if(null == selectOrder){
			queryPayForAnotherOrderStatusResponse.setSuccess(false);
			queryPayForAnotherOrderStatusResponse.setCode(ResponseCode.ORDER_NO_EXIST.getCode());
			queryPayForAnotherOrderStatusResponse.setMsg(ResponseCode.ORDER_NO_EXIST.getMessage());
		}else{
			queryPayForAnotherOrderStatusResponse.setPayForAnotherStatus(selectOrder.getTxStatus());
			queryPayForAnotherOrderStatusResponse.setSuccess(true);
			queryPayForAnotherOrderStatusResponse.setCode(ResponseCode.OK.getCode());
			queryPayForAnotherOrderStatusResponse.setMsg(ResponseCode.OK.getMessage());
		}
		logger.info("代付订单状态查询请求结束。。。。。{}", EntityUtils.beanToMap(queryPayForAnotherOrderStatusResponse));
		return queryPayForAnotherOrderStatusResponse;
	}

}
