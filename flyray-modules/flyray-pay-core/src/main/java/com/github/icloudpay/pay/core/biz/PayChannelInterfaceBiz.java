package com.github.icloudpay.pay.core.biz;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Service;

import com.github.icloudpay.pay.core.entity.PayChannel;
import com.github.icloudpay.pay.core.entity.PayChannelInterface;
import com.github.icloudpay.pay.core.mapper.PayChannelInterfaceMapper;
import com.github.wxiaoqi.security.common.biz.BaseBiz;
import com.github.wxiaoqi.security.common.cms.request.PayChannelInterfaceRequestParam;
import com.github.wxiaoqi.security.common.cms.request.PayChannelQueryParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Slf4j
@Service
public class PayChannelInterfaceBiz extends BaseBiz<PayChannelInterfaceMapper, PayChannelInterface> {
	
	/**
	 * 查询列表
	 * @param map
	 * @return
	 */
	public Map<String, Object> queryList(PayChannelQueryParam param) {
		log.info("查询支付通道接口配置......start......."+param);
		Map<String, Object> respMap = new HashMap<>();
		try {
			Example example = new Example(PayChannel.class);
			Criteria criteria = example.createCriteria();
			if (param.getPayChannelNo() != null && param.getPayChannelNo().length() > 0) {
				criteria.andEqualTo("payChannelNo",param.getPayChannelNo());
			}
			example.setOrderByClause("create_time desc");
			List<PayChannelInterface> list = mapper.selectByExample(example);
			log.info("查询支付通道接口配置。。。。list...."+list);
			respMap.put("code", "00");
			respMap.put("msg", "请求成功");
			respMap.put("list", list);
		} catch (Exception e) {
			e.printStackTrace();
			respMap.put("code", "01");
			respMap.put("msg", "异常");
		}
		log.info("查询支付通道接口配置............end......."+respMap);
		return respMap;
	}
	
	/**
	 * 添加
	 * @param map
	 * @return
	 */
	public Map<String, Object> addInterface(PayChannelInterfaceRequestParam map) {
		log.info("添加支付通道接口配置......start......."+map);
		Map<String, Object> respMap = new HashMap<>();
		PayChannelInterface interface1 = new PayChannelInterface();
		try {
			BeanUtils.copyProperties(interface1, map);
			interface1.setCreateTime(new Date());
			interface1.setUpdateTime(new Date());
			if (mapper.insert(interface1) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【添加支付通道接口配置】  响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【添加支付通道接口配置】 报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【添加支付通道接口配置】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【添加支付通道接口配置】   响应参数：{}", respMap);
		return respMap;
	}
	
	/**
	 * 更新
	 * @param map
	 * @return
	 */
	public Map<String, Object> updateInterface(PayChannelInterfaceRequestParam map) {
		log.info("更新支付通道接口配置......start......."+map);
		Map<String, Object> respMap = new HashMap<>();
		PayChannelInterface interface1 = new PayChannelInterface();
		try {
			BeanUtils.copyProperties(interface1, map);
			interface1.setId(Integer.valueOf(map.getId()));
			if (mapper.updateByPrimaryKeySelective(interface1) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【更新支付通道接口配置】  响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【更新支付通道接口配置】 报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【更新支付通道接口配置】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【更新支付通道接口配置】   响应参数：{}", respMap);
		return respMap;
	}
	
	
	/**
	 * 删除
	 * @param map
	 * @return
	 */
	public Map<String, Object> deleteInterface(PayChannelInterfaceRequestParam map) {
		log.info("删除支付通道接口配置......start......."+map);
		Map<String, Object> respMap = new HashMap<>();
		PayChannelInterface interface1 = new PayChannelInterface();
		try {
			BeanUtils.copyProperties(interface1, map);
			interface1.setId(Integer.valueOf(map.getId()));
			if (mapper.delete(interface1) > 0) {
				respMap.put("code", ResponseCode.OK.getCode());
				respMap.put("msg", ResponseCode.OK.getMessage());
				log.info("【删除支付通道接口配置】  响应参数：{}", respMap);
				return respMap;
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.info("【删除支付通道接口配置】 报错：" + e.getMessage());
			respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
			respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
			log.info("【删除支付通道接口配置】   响应参数：{}", respMap);
			return respMap;
		}
		respMap.put("code", ResponseCode.ABNORMAL_FIELDS.getCode());
		respMap.put("msg", ResponseCode.ABNORMAL_FIELDS.getMessage());
		log.info("【删除支付通道接口配置】   响应参数：{}", respMap);
		return respMap;
	}
}
