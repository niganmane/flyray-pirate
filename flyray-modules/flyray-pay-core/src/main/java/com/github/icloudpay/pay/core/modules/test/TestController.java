package com.github.icloudpay.pay.core.modules.test;

import com.github.wxiaoqi.security.cache.CacheProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author: bolei
 * @date: 14:19 2018/10/19
 * @Description: 类描述
 */

@Slf4j
@Controller
@RequestMapping("/test")
public class TestController {

    @Autowired
    private TestService userService;
    @ResponseBody
    @RequestMapping("index")
    public String index(){

        final User user = userService.saveOrUpdate(new User(5L, "u5", "p5"));
        log.info("[saveOrUpdate] - [{}]", user);
        final User user1 = userService.get(5L);
        log.info("[get] - [{}]", user1);
        userService.delete(5L);

        StringBuffer str =  new StringBuffer("22");

        CacheProvider.set("tyh", "aaaaaaaaaaaaaaaaaa");
        str.append("|");
        str.append(CacheProvider.get("tyh"));
        str.append("|");
        str.append(CacheProvider.del("tyh"));


        //模拟对账操作
        //从数据中查出今日需要对账的数据写入redis中
        CacheProvider.set("{account}:localSet", "GM002215120800001,0.01,00");
        CacheProvider.set("{account}:localSet", "GM002215120800002,0.21,03");
        CacheProvider.set("{account}:localSet", "GM002215120800003,0.21,01");

        //解析对账文件中数据写入redis中
        CacheProvider.set("{account}:outerSet", "GM002215120800001,0.01,00");
        CacheProvider.set("{account}:outerSet", "GM002215120800002,0.21,01");
        CacheProvider.set("{account}:outerSet", "GM002215120800003,0.21,03");

        //已localSet为准 对比 outerSet中的数据
        //取出两个数据的交集
        CacheProvider.setIntersectAndStore("{account}:localSet", "{account}:outerSet", "{account}:intersect");

        //求本地集合与交集的差集存到差集中
        CacheProvider.setDifferenceAndStore( "{account}:localSet", "{account}:intersect", "{account}:localDiff");

        //求第三方对账数据跟交集的差集存到差集中
        CacheProvider.setDifferenceAndStore( "{account}:outerDiff", "{account}:intersect", "{account}:outerDiff");

        //获取差集写入数据库
        CacheProvider.getMembers("{account}:localDiff");
        CacheProvider.getMembers("{account}:outerDiff");

        //根据状态进行差错处理

        return str.toString();
    }
}
