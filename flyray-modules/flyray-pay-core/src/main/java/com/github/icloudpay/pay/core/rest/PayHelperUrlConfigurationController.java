package com.github.icloudpay.pay.core.rest;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.icloudpay.pay.core.biz.PayHelperUrlConfigurationBiz;
import com.github.icloudpay.pay.core.entity.PayHelperUrlConfiguration;
import com.github.wxiaoqi.security.common.crm.request.MerchantPayForAnotherBankRequest;
import com.github.wxiaoqi.security.common.crm.request.PayHelperUrlConfigrationParam;
import com.github.wxiaoqi.security.common.msg.ResponseCode;
import com.github.wxiaoqi.security.common.msg.TableResultResponse;
import com.github.wxiaoqi.security.common.rest.BaseController;
import com.github.wxiaoqi.security.common.util.ResponseHelper;

import lombok.extern.slf4j.Slf4j;


/**
 * 支付助手APP配置
 * @author Administrator
 *
 */

@Controller
@RequestMapping("payHelperUrlConfiguration")
public class PayHelperUrlConfigurationController extends BaseController<PayHelperUrlConfigurationBiz, PayHelperUrlConfiguration> {
	
	private static Logger logger = LoggerFactory.getLogger(PayHelperUrlConfigurationController.class);
	
	/**
	 * 列表 
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/list", method=RequestMethod.POST)
	@ResponseBody
	public TableResultResponse<PayHelperUrlConfiguration> listAll(@RequestBody MerchantPayForAnotherBankRequest param) {
		logger.info("查询支付助手app地址配置列表。。。{}"+param);
		param.setPlatformId(setPlatformId(param.getPlatformId()));
		return baseBiz.payHelperUrlConfigurationList(param);
	}
	
	
	/**
	 * 添加
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/add", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> addInfo(@RequestBody PayHelperUrlConfigrationParam param) {
		logger.info("添加支付助手app地址配置。。。{}"+param);
		Map<String, Object> respMap = baseBiz.addPayHelperUrlConfiguration(param);
		return ResponseHelper.success(respMap, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
	}
	
	/**
	 * 删除
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> deleteInfo(@RequestBody PayHelperUrlConfigrationParam param) {
		PayHelperUrlConfiguration bank = baseBiz.deletePayHelperUrlConfiguration(param);
		if (bank != null) {
			return ResponseHelper.success(null, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
	}
	
	
	/**
	 * 修改
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/update", method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateInfo(@RequestBody PayHelperUrlConfigrationParam param) {
		PayHelperUrlConfiguration bank = baseBiz.updatePayHelperUrlConfiguration(param);
		if (bank != null) {
			return ResponseHelper.success(bank, null, ResponseCode.OK.getCode(), ResponseCode.OK.getMessage());
		}else {
			return ResponseHelper.success(null, null, ResponseCode.FUNCTION_NO_EXIST.getCode(), ResponseCode.FUNCTION_NO_EXIST.getMessage());
		}
	}
	
}
