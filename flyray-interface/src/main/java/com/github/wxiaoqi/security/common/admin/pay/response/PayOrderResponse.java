package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;
import java.util.Map;

import com.github.wxiaoqi.security.common.admin.pay.request.CallbackRequest;

/** 
* @author: bolei
* @date：2017年4月30日 下午1:22:25 
* @description：支付订单相应
*/

public class PayOrderResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 支付信息
	 */
	private Map<String,Object> payObject;

	public Map<String, Object> getPayObject() {
		return payObject;
	}

	public void setPayObject(Map<String, Object> payObject) {
		this.payObject = payObject;
	}
	
	/***
	 * 暂时写死出发回调，上线去掉
	 */
	private CallbackRequest callbackRequest;

	public CallbackRequest getCallbackRequest() {
		return callbackRequest;
	}

	public void setCallbackRequest(CallbackRequest callbackRequest) {
		this.callbackRequest = callbackRequest;
	}
	
	

}
