package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 微信小程序商户列表查询
 * @author he
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "微信小程序商户列表查询请求参数")
public class QueryMerchantListParam {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	@NotNull(message="运营人员编号不能为空")
	@ApiModelProperty(value = "运营人员编号")
    private String operatorId;
	
}
