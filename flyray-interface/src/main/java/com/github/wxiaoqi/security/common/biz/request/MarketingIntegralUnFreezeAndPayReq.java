package com.github.wxiaoqi.security.common.biz.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.admin.pay.request.BaseRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 积分解冻并出账请求
 * 
 * @author hexufeng 2018年1月29日 下午3:51:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("积分解冻并出账请求")
public class MarketingIntegralUnFreezeAndPayReq extends BaseRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	// /**
	// * 合作平台编号
	// */
	// @NotNull(message="合作平台编号不能为空")
	// private String acceptBizNo;
	//
	// /**
	// * 商户号
	// */
	// @NotNull(message="商户号不能为空")
	// private String merNo;
	//
	// /**
	// * 商户会员号
	// */
	// @NotNull(message="商户会员号不能为空")
	// private String merCustNo;
	// /**
	// * 平台编号
	// */
	// @NotNull(message="平台编号不能为空")
	// private String unionId;

	// 用户编号
	@ApiModelProperty(value = "用户编号")
	private String customerId;

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	// 个人客户编号
	@NotNull(message = "个人客户编号不能为空")
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;

	// 商户客户编号
	@NotNull(message = "商户客户编号不能为空")
	@ApiModelProperty(value = "商户客户编号")
	private String merchantId;
	/**
	 * 商户订单号
	 */
	@NotNull(message = "商户订单号不能为空")
	@ApiModelProperty(value = "商户订单号")
	private String merOrderNo;

	/**
	 * 积分数量
	 */
	@NotNull(message = "积分数量不能为空")
	@ApiModelProperty(value = "积分数量")
	private String integralAmount;

	/**
	 * 解冻业务流水号
	 */
	@NotNull(message = "解冻业务流水号不能为空")
	@ApiModelProperty(value = "解冻业务流水号")
	private String freezeNo;

	/**
	 * 冻结关联流水号
	 */
	@NotNull(message = "冻结关联流水号不能为空")
	@ApiModelProperty(value = "冻结关联流水号")
	private String freezeNoRel;

	/**
	 * 使用场景ID
	 */
	@NotNull(message = "使用场景ID不能为空")
	@ApiModelProperty(value = "使用场景ID")
	private String sceneId;

}
