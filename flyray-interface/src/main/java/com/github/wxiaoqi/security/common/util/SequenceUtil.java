package com.github.wxiaoqi.security.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class SequenceUtil {

	/**
	 * 生成19位序列号
	 * @return
	 */
	public static String Create27DigitalOrAlphabet() {
		String SeqNo = null;

		Random ne=new Random();//实例化一个random的对象ne
		int fiveNo1 = ne.nextInt(99999-10000+1)+10000;//为变量赋随机值10000-99999
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SeqNo = sdf.format(new Date()) + fiveNo1;

		return SeqNo;
	}

}
