package com.github.wxiaoqi.security.common.biz.request;

import java.io.Serializable;
import java.util.List;

import com.github.wxiaoqi.security.common.admin.pay.response.BaseResponse;

/**
 * 积分明细列表查询响应
 * @author hexufeng
 * 2018年3月5日 上午11:34:12
 */
public class MarketingIntegralDetailListRsp extends BaseResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 积分明细列表
	 */
//	private List<CustAccountingDetails> integralDetailList;
	private Object integralDetailList;
	/**
	 * 积分余额
	 */
	private String balance;


	public Object getIntegralDetailList() {
		return integralDetailList;
	}

	public void setIntegralDetailList(Object integralDetailList) {
		this.integralDetailList = integralDetailList;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}
	

}
