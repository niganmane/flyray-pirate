package com.github.wxiaoqi.security.common.biz.request;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.crm.request.AccountQueryRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 调用crm-红包、积分领取参数
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("红包、积分领取参数")
public class CoupIntegRevCrmReq {

	// 用户编号
	@ApiModelProperty(value = "用户编号")
	private String customerId;

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	// 个人客户编号
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;

	// 商户客户编号
	@ApiModelProperty(value = "商户客户编号")
	private String merchantId;

	// private String acceptBizNo;//合作平台编号
	// private String merNo;//商户号
	// private String merCustNo;//商户会员号
	// private String unionId;// 平台编号
	
	@ApiModelProperty(value = "业务类型")
	private String businessType;// 业务类型
	@ApiModelProperty(value = "积分数量")
	private String integralAmount;// 积分数量-领取的积分数量可以是包含多个
	@ApiModelProperty(value = "红包信息")
	List<CouponInfo> couponList;// 红包信息

}
