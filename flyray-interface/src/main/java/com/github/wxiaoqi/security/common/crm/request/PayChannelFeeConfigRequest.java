package com.github.wxiaoqi.security.common.crm.request;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 支付通道费率配置表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "费率请求参数")
public class PayChannelFeeConfigRequest {
	
	@ApiModelProperty(value = "费率代码")
    private String feeCode;
	
	
	@ApiModelProperty(value = "费率名称")
    private String feeName;
	
	@ApiModelProperty(value = "币种")
    private String ccy;
	//00固定金额01按比例
	@ApiModelProperty(value = "计费方式")
    private String billingMethod;
	
	@ApiModelProperty(value = "收费金额")
    private BigDecimal feeAmt;
	
    @ApiModelProperty(value = "收费比例")
    private BigDecimal feeRatio;
	
    @ApiModelProperty(value = "最低收费")
    private BigDecimal minFee;
	
    @ApiModelProperty(value = "最高收费")
    private BigDecimal maxFee;
  	
    @ApiModelProperty(value = "当前页码")
    private Integer page;
    
    @ApiModelProperty(value = "每页条数")
    private Integer limit;
    
	
}
