package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序查询评论参数")
public class CmsQueryCommentParam extends BaseParam {
	@NotNull(message="用户编号不能为空")
	@ApiModelProperty("用户编号")
	private String customerId;
	@ApiModelProperty("当前页")
	private Integer page;
	@ApiModelProperty("数据条数")
	private Integer limit;
	@ApiModelProperty("评论类型(1评论2回复)")
	private String commentType;
	@ApiModelProperty("评论的目标id")
	private String commentTargetId;
	@ApiModelProperty("评论的目标用户id")
	private String commentTargetUserId;
	@ApiModelProperty("评论模块编号01社群 02卖朋友")
	private String commentModuleNo;
	@ApiModelProperty("回复评论的目标id")
	private String parentId;
	@ApiModelProperty("话题id")
	private String id;
	@ApiModelProperty("总数")
	private Integer totalCount;
}
