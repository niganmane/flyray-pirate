package com.github.wxiaoqi.security.common.biz.response;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;

import com.github.wxiaoqi.security.common.admin.pay.response.BaseResponse;

/***
 * 基础红包/积分查询返回值-红包基础信息
 * 最优的红包信息继承此类
 */
public class BaseCoupInfo extends BaseResponse{

	// 红包ID
	private String couponId;
	// 红包流水号
	private String couponSerialNo;
	// 红包有效时间
	private String valdateTime;
	// 领取方式
	private String payment;
	// 使用红包最低金额
	private String totalPay;
	// 优惠券抵扣金额
	private String subtraction;
	//红包使用场景
	private String scenes;
	//红包使用场景编号
	private String sceneId;
	//红包类型
	private String couponType;//'0' 表示普通红包，'1' 表示随机红包
	//红包抵扣金额
	private BigDecimal couponAmt;//

	//红包是否有效  有效状态 00：有效 01：无效
	private String effectiveStatus;
	//活动编号
	private String  actId;
	/**
     * 红包等级
     */
    private String couLevel;
	
	
	
	public String getEffectiveStatus() {
		return effectiveStatus;
	}

	public void setEffectiveStatus(String effectiveStatus) {
		this.effectiveStatus = effectiveStatus;
	}

	public String getActId() {
		return actId;
	}

	public void setActId(String actId) {
		this.actId = actId;
	}

	public BigDecimal getCouponAmt() {
		return couponAmt;
	}

	public void setCouponAmt(BigDecimal couponAmt) {
		this.couponAmt = couponAmt;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public String getSceneId() {
		return sceneId;
	}

	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}

	public String getScenes() {
		return scenes;
	}

	public void setScenes(String scenes) {
		this.scenes = scenes;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}


	public String getCouponSerialNo() {
		return couponSerialNo;
	}

	public void setCouponSerialNo(String couponSerialNo) {
		this.couponSerialNo = couponSerialNo;
	}

	public String getValdateTime() {
		return valdateTime;
	}

	public void setValdateTime(String valdateTime) {
		this.valdateTime = valdateTime;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getTotalPay() {
		return totalPay;
	}

	public void setTotalPay(String totalPay) {
		this.totalPay = totalPay;
	}

	public String getSubtraction() {
		return subtraction;
	}

	public void setSubtraction(String subtraction) {
		this.subtraction = subtraction;
	}

	public String getCouLevel() {
		return couLevel;
	}

	public void setCouLevel(String couLevel) {
		this.couLevel = couLevel;
	}

}
