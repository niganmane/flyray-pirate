package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 实名认证请求参数
 * @author centerroot
 * @time 创建时间:2018年9月8日下午4:39:09
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("实名认证请求参数")
public class CertificationRequest{

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message = "用户编号不能为空")
	@ApiModelProperty(value = "用户编号")
	private String customerId;
	
	/*
	 * 实名认证类型 00代表银行卡四要素实名人 01代表泰华电子实名认证
	 * */
	@ApiModelProperty("实名认证类型")
	private String authType;

	@ApiModelProperty("泰华实名认证ID")
	private String authId;
	
	@ApiModelProperty("银行卡号")
	private String bankCardNo;
	
	@ApiModelProperty("银行卡持卡人")
	private String cardHolderName;
	
	@ApiModelProperty("银行卡编号")
	private String bankNo;
	
	@ApiModelProperty("银行名称")
	private String bankName;
	
	@ApiModelProperty("支行编号")
	private String subbranchNo;
	
	@ApiModelProperty("支行名称")
	private String subbranchName;
	
}
