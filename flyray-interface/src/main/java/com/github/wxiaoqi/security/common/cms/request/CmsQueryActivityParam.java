package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序查询活动参数")
public class CmsQueryActivityParam extends BaseParam {

	@ApiModelProperty("当前页")
	private Integer page;
	@ApiModelProperty("数据条数")
	private Integer limit;
	@ApiModelProperty("活动编号")
	private String id;
	@ApiModelProperty("用户编号")
	private String customerId;
	@ApiModelProperty("活动名称")
	private String activityName;
}
