package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;

/**
 * 退款申请响应
 * @author hexufeng
 *
 */
public class RefundApplyResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 退款订单号
	 */
	private String refundOrderNo;

	public String getRefundOrderNo() {
		return refundOrderNo;
	}

	public void setRefundOrderNo(String refundOrderNo) {
		this.refundOrderNo = refundOrderNo;
	}
	
}
