package com.github.wxiaoqi.security.common.biz.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.admin.pay.request.BaseRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * 小程序-获取可领取红包列表
 * param：sceneId
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序-获取可领取红包列表")
public class WXCouponRevReq extends BaseRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 场景ID
	@NotNull(message="事件ID不能为空")
	private String incidentId;
//	// 商户会员号
//	@NotNull(message="商户会员号不能为空")
//	private String merCustNo;
	
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;

	// 个人客户编号
	@NotNull(message = "个人客户编号不能为空")
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;
	
}
