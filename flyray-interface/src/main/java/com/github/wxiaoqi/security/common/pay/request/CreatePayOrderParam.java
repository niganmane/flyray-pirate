package com.github.wxiaoqi.security.common.pay.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 创建订单
 * @author he
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("创建订单")
public class CreatePayOrderParam {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message="商户账号不能为空")
	@ApiModelProperty("商户账号")
	private String merchantId;
	
	@NotNull(message="用户账号不能为空")
	@ApiModelProperty("用户账号")
	private String customerId;
	
	@NotNull(message="个人客户编号不能为空")
	@ApiModelProperty("个人客户编号")
	private String personalId;
	
	@NotNull(message="订单金额不能为空")
	@ApiModelProperty("订单金额")
	private String orderAmt;
	
	@NotNull(message="支付金额不能为空")
	@ApiModelProperty("支付金额")
	private String payAmt;
	
	@NotNull(message="商品信息不能为空")
	@ApiModelProperty("商品信息")
	private String body;
	
	@NotNull(message="交易方式不能为空")
	@ApiModelProperty("交易方式")
	private String payCode;//1支付，2充值
	
	@NotNull(message="场景编号不能为空")
	@ApiModelProperty("场景编号")
	private String scenesCode;
	
	@NotNull(message="支付方式不能为空")
	@ApiModelProperty("支付方式")
	private String payType;
	
	@NotNull(message="扩展map不能为空")
	@ApiModelProperty("扩展map")
	private String extMap;

}
