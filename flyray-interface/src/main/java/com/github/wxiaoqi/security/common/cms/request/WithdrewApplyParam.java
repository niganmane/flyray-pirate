package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("提现申请")
public class WithdrewApplyParam extends BaseParam{

	@NotNull(message="用户编号不能为空")
	@ApiModelProperty("用户编号")
	private String customerId;
	
	@NotNull(message="个人客户编号不能为空")
	@ApiModelProperty("个人客户编号")
	private String personalId;
	
	@NotNull(message="提现金额不能为空")
	@ApiModelProperty("提现金额")
	private String txAmt;
	
	@NotNull(message="支付宝账号不能为空")
	@ApiModelProperty("支付宝账号")
	private String alipayNo;
	
	@NotNull(message="账户类型不能为空")
	@ApiModelProperty("账户类型")
	private String accountType;
	
}
