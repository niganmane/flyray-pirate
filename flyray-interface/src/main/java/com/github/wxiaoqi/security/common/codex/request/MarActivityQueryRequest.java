package com.github.wxiaoqi.security.common.codex.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("营销活动查询参数")
public class MarActivityQueryRequest {
	
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@ApiModelProperty("商户编号")
	private String merchantId;
	
	@ApiModelProperty("当前页")
	private Integer page;
	
	@ApiModelProperty("条数")
	private Integer limit;
	
	@ApiModelProperty("活动编号")
	private String act;
	
}
