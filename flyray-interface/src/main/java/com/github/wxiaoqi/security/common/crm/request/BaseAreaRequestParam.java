package com.github.wxiaoqi.security.common.crm.request;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 行政区域
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "行政区域请求参数")
public class BaseAreaRequestParam {
	
	@ApiModelProperty(value = "区域id")
	private String areaId;
	
	@ApiModelProperty(value = "行政区划代码")
	private String areaCode;

	@ApiModelProperty(value = "父级id")
    private String parentCode;
	
	@ApiModelProperty(value = "地区名称")
    private String name;
	
	@ApiModelProperty(value = "层级")
    private String layer;
	
	@ApiModelProperty(value = "显示,1:显示,0:隐藏")
    private String status;
	
	@ApiModelProperty(value = "备注")
    private String remark;
	
}
