package com.github.wxiaoqi.security.common.qr.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("查询商户商品列表")
public class QrMerchantGoodsParam {
	
	@NotNull(message="用户编号不能为空")
	@ApiModelProperty("用户编号")
	private String operatorId;
	
}
