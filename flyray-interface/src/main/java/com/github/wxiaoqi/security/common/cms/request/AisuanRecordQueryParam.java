package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("爱算查询参数")
public class AisuanRecordQueryParam extends BaseParam{
	
	@ApiModelProperty("记录编号")
	private Long recordId;
	
	@ApiModelProperty("页数")
	private Integer page;
	
	@ApiModelProperty("数量")
	private Integer limit;
	
	@ApiModelProperty("会员编号")
	private Long customerId;
	
	@ApiModelProperty("咨询记录编号")
	private Long topId;
	
	@ApiModelProperty("一级分类")
	private Integer firstLevel;
	

}
