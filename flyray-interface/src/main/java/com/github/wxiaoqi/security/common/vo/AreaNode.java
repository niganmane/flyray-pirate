package com.github.wxiaoqi.security.common.vo;

import java.util.List;

/**
 * Created by Ace on 2017/6/12.
 */
public class AreaNode {
    protected String areaCode;
    protected String parentCode;

    public List<AreaNode> getChildren() {
        return children;
    }

    public void setChildren(List<AreaNode> children) {
        this.children = children;
    }

    List<AreaNode> children = null;

    public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public void add(AreaNode node){
        children.add(node);
    }
}
