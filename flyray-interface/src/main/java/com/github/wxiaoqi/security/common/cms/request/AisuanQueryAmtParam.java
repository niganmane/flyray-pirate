package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("金额查询参数")
public class AisuanQueryAmtParam {

	@ApiModelProperty("当前页")
	private Integer page;
	@ApiModelProperty("数据条数")
	private Integer limit;
}
