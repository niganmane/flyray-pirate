package com.github.wxiaoqi.security.common.admin.pay.request;

import java.io.Serializable;

/** 
* @author: bolei
* @date：2017年2月23日 下午12:32:58 
* @description：退款请求
*/

public class RefundRequest extends BaseRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;
	//============请求参数================
    /**
	 * 平台编号
	 */
	private String platformId;
	/**
	 * 退款订单号
	 */
	private String refundOrderNo;
	
	//=============组装参数==============
    /**
     * 原订单总金额
     */
    private String ordAmt;
    /**
	 * 商户客户号
	 */
	private String merchantId;

	/**
	 * 支付公司编号
	 */
	private String payCompanyNo;

	/**
	 * 支付通道编号
	 */
	private String payChannelNo;
	/**
	 * 支付订单号
	 */
	private String SerialNo;
	/**
	 * 退款金额
	 */
	private String refundAmt;
	/**
	 * 退款流水号
	 */
	private String refundSerialNo;
    
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getRefundOrderNo() {
		return refundOrderNo;
	}
	public void setRefundOrderNo(String refundOrderNo) {
		this.refundOrderNo = refundOrderNo;
	}
	public String getOrdAmt() {
		return ordAmt;
	}
	public void setOrdAmt(String ordAmt) {
		this.ordAmt = ordAmt;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getPayCompanyNo() {
		return payCompanyNo;
	}
	public void setPayCompanyNo(String payCompanyNo) {
		this.payCompanyNo = payCompanyNo;
	}
	public String getPayChannelNo() {
		return payChannelNo;
	}
	public void setPayChannelNo(String payChannelNo) {
		this.payChannelNo = payChannelNo;
	}
	public String getSerialNo() {
		return SerialNo;
	}
	public void setSerialNo(String serialNo) {
		SerialNo = serialNo;
	}
	public String getRefundAmt() {
		return refundAmt;
	}
	public void setRefundAmt(String refundAmt) {
		this.refundAmt = refundAmt;
	}
	public String getRefundSerialNo() {
		return refundSerialNo;
	}
	public void setRefundSerialNo(String refundSerialNo) {
		this.refundSerialNo = refundSerialNo;
	}
	
}
