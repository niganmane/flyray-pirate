package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("竞拍商品信息详情查询")
public class AuctionGoodsDetailParam extends BaseParam{
	
	@NotNull(message="商品编号不能为空")
	@ApiModelProperty("商品编号")
	private String goodsId;

}
