package com.github.wxiaoqi.security.common.crm.request;

import java.util.Date;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 个人客户跟踪表
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-05 10:18:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "个人客户跟踪表请求参数")
public class PersonalBaseTrackingRequest {
	
	    //序号
	@ApiModelProperty(value = "序号")
    private Integer id;
	
	    //平台编号
    @NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	    //跟踪人ID
	@ApiModelProperty(value = "跟踪人ID")
    private String tickerPersonalId;
	
	    //被跟踪人ID
    @NotNull(message="被跟踪人ID不能为空")
	@ApiModelProperty(value = "被跟踪人ID")
    private String personalId;
	
	    //备注
	@ApiModelProperty(value = "备注")
    private String remark;
	
	    //创建时间
	@ApiModelProperty(value = "创建时间")
    private Date createTime;

}
