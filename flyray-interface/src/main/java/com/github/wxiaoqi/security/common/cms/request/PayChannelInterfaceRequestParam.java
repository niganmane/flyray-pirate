package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("支付通道接口查询参数")
public class PayChannelInterfaceRequestParam {
	
	@ApiModelProperty("序号")
	private String id;
	@ApiModelProperty("支付通道编号")
	private String payChannelNo;
	@ApiModelProperty("交易类型")
	private String tradeType;
	@ApiModelProperty("接口名称")
	private String serviceName;
	
}
