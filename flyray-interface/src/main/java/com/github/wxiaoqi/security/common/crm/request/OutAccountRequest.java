package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("个人或者商户直接出账接口参数")
public class OutAccountRequest {
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message = "出账金额不能为空")
	@ApiModelProperty("出账金额")
	private String outAccAmt;

	@NotNull(message = "商户编号不能为空")
	@ApiModelProperty("商户编号")
	private String merchantId;
	
	@NotNull(message = "商户类型不能为空")
	@ApiModelProperty("商户类型编号")
	private String merchantType;
	
	@NotNull(message = "客户类型不能为空")
	@ApiModelProperty("客户类型")//01:商户 02:个人
	private String customerType;
	
	@NotNull(message = "账户类型不能为空")
	@ApiModelProperty("账户类型")
	private String accountType;
	
	
	@NotNull(message = "订单号不能为空")
	@ApiModelProperty("订单号")
	private String orderNo;

	@NotNull(message = "个人客户编号不能为空")
	@ApiModelProperty("个人客户编号")
	private String personalId;
	
	@NotNull(message = "交易类型不能为空")
	@ApiModelProperty("交易类型")
	private String tradeType;

	
}
