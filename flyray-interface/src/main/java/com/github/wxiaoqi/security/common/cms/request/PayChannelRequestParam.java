package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("支付渠道查询参数")
public class PayChannelRequestParam {
	
	@ApiModelProperty("序号")
	private String id;
	@ApiModelProperty("支付公司编号")
	private String payCompanyNo;
	@ApiModelProperty("支付公司名称")
	private String payCompanyName;
	@ApiModelProperty("支付通道编号")
	private String payChannelNo;
	@ApiModelProperty("支付通道名称")
	private String payChannelName;
	@ApiModelProperty("交易类型")
	private String tradeType;
}
