package com.github.wxiaoqi.security.common.biz.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("查询参数")
public class CouponStatusReq {
//	// 商户会员号
//	@NotNull(message="商户会员号不能为空")
//	private String merCustNo;
//	@NotNull(message="渠道号不能为空")
//	private String acceptBizNo;
	
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;

	// 个人客户编号
	@NotNull(message = "个人客户编号不能为空")
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;
}
