package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序观点参数")
public class CreateFireSourceWalletRequest {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message="用户编号不能为空")
	@ApiModelProperty("用户编号")
	private String customerId;
	
	@NotNull(message="个人客户不能为空")
	@ApiModelProperty("个人客户")
	private String personalId;
	
	@NotNull(message="真实姓名不能为空")
	@ApiModelProperty("真实姓名")
	private String realName;
	
	@NotNull(message="身份证号不能为空")
	@ApiModelProperty("身份证号")
	private String idCard;

}
