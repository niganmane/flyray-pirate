package com.github.wxiaoqi.security.common.biz.request;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.admin.pay.request.BaseRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * 红包/积分消费服务参数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("红包/积分消费服务参数")
public class CouponIntegralUseReq extends BaseRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9055741238583250885L;

	// // 合作平台编号
	// @NotNull(message="合作平台编号不能为空")
	// private String acceptBizNo;
	// // 商户会员号
	// @NotNull(message="商户会员号不能为空")
	// private String merCustNo;
	// // 商户号
	// @NotNull(message="商户号不能为空")
	// private String merNo;

	// 用户编号
	@ApiModelProperty(value = "用户编号")
	private String customerId;

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;

	// 个人客户编号
	@NotNull(message = "个人客户编号不能为空")
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;

	// 商户客户编号
	@NotNull(message = "商户客户编号不能为空")
	@ApiModelProperty(value = "商户客户编号")
	private String merchantId;
	
	// 场景ID
	@NotNull(message = "场景编号不能为空")
	@ApiModelProperty(value = "场景ID")
	private String sceneId;
	
	// 订单号
	@NotNull(message = "订单号不能为空")
	@ApiModelProperty(value = "订单号")
	private String merOrderNo;
	/**
	 * 红包ID
	 */
	@ApiModelProperty(value = "红包ID")
	private String couponId;
	/**
	 * 红包流水号
	 */
	@ApiModelProperty(value = "红包流水号")
	private String couponSerialNo;
	/**
	 * 积分数量
	 */
	@ApiModelProperty(value = "积分数量")
	private String integralAmount;

	/**
	 * 红包抵扣金额
	 */
	@ApiModelProperty(value = "红包抵扣金额")
	private String couponAmt;

	/**
	 * 积分抵扣金额
	 */
	@ApiModelProperty(value = "积分抵扣金额")
	private String integralAmt;
}
