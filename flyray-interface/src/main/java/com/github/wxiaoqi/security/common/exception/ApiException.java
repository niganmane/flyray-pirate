package com.github.wxiaoqi.security.common.exception;

/** 
* @author: bolei
* @date：2018年5月18日 下午5:06:19 
* @description：https://juejin.im/post/5a93bb835188257a7b5ab9a4
*/

public class ApiException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	protected Long code ;
	protected Long msg ;
	protected Object data ;

	public ApiException(Long code,String message,Object data,Throwable e){
	    super(message,e);
	    this.code = code ;
	    this.data = data ;
	}

	public ApiException(Long errorCode,String message,Object data){
	    this(errorCode,message,data,null);
	}

	public ApiException(Long errorCode,String message){
	    this(errorCode,message,null,null);
	}

	public ApiException(String message,Throwable e){
	    this(null,message,null,e);
	}

	public ApiException(){

	}

	public ApiException(Throwable e){
	    super(e);
	}

	public Long getCode() {
	    return code;
	}

	public void setCode(Long code) {
	    this.code = code;
	}

	public Object getData() {
	    return data;
	}

	public void setData(Object data) {
	    this.data = data;
	}

}
