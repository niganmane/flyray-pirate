package com.github.wxiaoqi.security.common.cms.request;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-10 11:45:46
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("添加推荐建议")
public class AisuanAddRecommendParam extends BaseParam {
	private static final long serialVersionUID = 1L;
	
	    //主键自增
	@ApiModelProperty("主键")
    private Integer id;
	
	    //外键，用户编号
	@ApiModelProperty("外键，用户编号")
    private Long userId;
	
	    //头像
	@ApiModelProperty("头像")
    private String avert;
	
	    //姓名
	@ApiModelProperty("姓名")
    private String userName;
	
	    //创建时间
	@ApiModelProperty("创建时间")
    private Date createTime;
	
	    //会员编号
	@ApiModelProperty("会员编号")
    private Long customerId;
	
	@ApiModelProperty("排序号")
    private Integer sort;
	
	@ApiModelProperty("微信号")
    private String wx;
	
	@ApiModelProperty("手机号")
    private String phone;
	
	@ApiModelProperty("简介")
    private String intro;
    
}
