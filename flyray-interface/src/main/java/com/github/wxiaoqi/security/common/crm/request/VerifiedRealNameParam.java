package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("实名认证参数")
public class VerifiedRealNameParam {
	@NotNull(message="用户信息编号不能为空")
	@ApiModelProperty("用户信息编号")
	private String customerId;
	
	@NotNull(message="信息类型不能为空")
	@ApiModelProperty("信息类型 00：会员信息  01： 商户信息")
	private String infoType;
	@ApiModelProperty("真实姓名")
	private String realName;
	@ApiModelProperty("身份证号")
	private String idNumber;
	@ApiModelProperty("手机号")
	private String mobile;
	@ApiModelProperty("身份证正面照片")
	private String idPositiveStr;
	@ApiModelProperty("身份证反面照片")
	private String idNegativeStr;
	
	@ApiModelProperty("法人名字")
	private String legalPersonName;
	@ApiModelProperty("法人身份证号")
	private String legalPersonCredNo;
	@ApiModelProperty("公司名称")
	private String custName;
	@ApiModelProperty("营业执照照片")
	private String businessLicence;
	//会员编号
	private String perId;
	
}
