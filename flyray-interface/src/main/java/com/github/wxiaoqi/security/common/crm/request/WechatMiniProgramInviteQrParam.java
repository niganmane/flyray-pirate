package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("微信小程序获取邀请二维码")
public class WechatMiniProgramInviteQrParam {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message="商户不能为空")
	@ApiModelProperty("商户编号") 
	private String merchantId;
	
	@NotNull(message="会员号")
	@ApiModelProperty("会员号")
	private String customerId;
	
}
