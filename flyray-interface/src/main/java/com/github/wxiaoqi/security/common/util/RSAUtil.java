package com.github.wxiaoqi.security.common.util;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* @author : bolei
* @version : 2016年12月16日 下午3:03:52
* @description : 类说明
*/

public class RSAUtil {

    private final static Logger logger = LoggerFactory.getLogger(RSAUtil.class);
    /**
     * 解密线程执行器
     */
    private static ThreadPoolExecutor decryptExecutor = new ThreadPoolExecutor(10, 50, 20, TimeUnit.MINUTES,new ArrayBlockingQueue<Runnable>(5));

    /**
     * 解密
     * @param encryptedData
     * @param keyStorePath
     * @param alias
     * @param password
     * @return
     * @throws Exception
     */
    public static byte[] decryptByPrivateKey(String key,byte[] encryptedData) throws Exception{
        logger.debug("PrivateKeyCacheUtils:::decryptByPrivateKey start ");
        DecryptTaskByCardNo decryptTask = new DecryptTaskByCardNo(key, encryptedData);
        Future<byte[]> result = decryptExecutor.submit(decryptTask);
        byte[] decryptData  = result.get(1, TimeUnit.MINUTES);
        logger.debug("PrivateKeyCacheUtils:::decryptByPrivateKey end ");
        return decryptData;
    }
    
    /**
     * PC 解密
     * @author huo.xq
     * 2017年3月14日下午5:42:06
     * @param key
     * @param encryptedData
     * @return
     * @throws Exception
     */
    public static byte[] pcDecryptByPrivateKey(String key,byte[] encryptedData) throws Exception{
        logger.debug("PrivateKeyCacheUtils:::decryptByPrivateKey start ");
        PcDecryptTask decryptTask = new PcDecryptTask(key, encryptedData);
        Future<byte[]> result = decryptExecutor.submit(decryptTask);
        byte[] decryptData  = result.get(1, TimeUnit.MINUTES);
        logger.debug("PrivateKeyCacheUtils:::decryptByPrivateKey end ");
        return decryptData;
    }
    
    
    public static void main(String[] args) {
        String cardNoStr = null;
        try {
            byte[] andEnData = Base64Utils.decode("rSmL6Cnk7gAAVgu5TVmX8VXApVbr5yhRNaUCdffQKF1eBpWdhrtyZ/+i6O6RO8p5UH7qPKu3nibaZt2EKmE8/9V8jvYiGrODGJWQE4d7nnxxL+ia+5/H9nOHjKC1FJCQbf4CRB2V8Bm5G2gZ7Pvd4AE5UDW31hOegAe566y6QZg=");
            byte[] dedata = RSAUtil.decryptByPrivateKey("MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOOREtuaCUyZla5uJlOLl4WVuiSS+JDy/YCabYVgdrKKeFeKHeyyaiNKVbb+tUQUfYnav4JZVINHlAonPfh1Pjz7njWwqeRm4Ci7KBy3QzG5DQpfns6n3Ht6UCf+JOVuf33dJifkFmUCWXr8kdNN4lrE3a1ogMuGQALIogCe3HIjAgMBAAECgYEApmJS5Gc6d1wv+Yol0Z3VmmE/6S/gN7LCML44Z3WBM8dUd8TPXlmQit71zsdqZy2IIDvlG/wOc7HpBodsovWuG20kV/YDUmbBZA7OGYC0LLaGV3MeEKI7p+nKAOA+vswd9RMYrNL3ZCM6O0cpQjoNUxVhVXeWbvPhBsYzptIRT4ECQQD72MOeKqucN1N8FRrEexrRwxmPxrbSUfrdk3B3oECEBkY3WYz72o2afHNyQrGe4MbRWJqs2INxJa0uV8jyKqnjAkEA51HOGfr69NBBv2QrmaBhmX1xdWRBvVvnWPeT6ngP7aizfPj/OKCr1XAEVScqxQk2NOvddRPBVJa41D0PAMqKwQJAJNVaHtrcyHcrBZ3MkWnzzTP8nBOTD4Wf7CIxM73DUcvdHyFAxmD7jtmexNpOQg9b5KhRzu/HoZfCEWITaJ+mwwJADaRUbFHR5Qg3KGtm6tt7hgrQ2yR7gAhgf22yanyEK/bsBu3EV3maSK8fgkoaCp69dKeIWvQ8TZnJ8rJbJSfMwQJANJ7xfIElqEPTtWtGymd/zB8kuhSTI1h7Zggh6Jd9NfXT0Rvw3KvI+1/1jsundanw3LGnMnIOyfX6ib+IaX+9MQ==",andEnData);
            cardNoStr = new String(dedata);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(cardNoStr);
    }
}


