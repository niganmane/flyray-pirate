package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("餐饮基础信息参数")
public class RestaurantParam extends BaseParam{
	
	@NotNull(message="用户账号不能为空")
	@ApiModelProperty("用户账号")
	private String perId;
	
	@ApiModelProperty("地址id")
	private String addressId;

}
