package com.github.wxiaoqi.security.common.biz.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.admin.pay.request.BaseRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 积分手动领取请求参数
 * 
 * @author chj
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("积分手动领取请求参数")
public class IntegralReceiveReq extends BaseRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// @NotNull(message="平台编号不能为空")
	// private String acceptBizNo;
	//
	// @NotNull(message="商户会员号不能为空")
	// private String merCustNo;
	//
	// @NotNull(message="商户号不能为空")
	// private String merNo;

	// 用户编号
	@ApiModelProperty(value = "用户编号")
	private String customerId;

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;

	// 个人客户编号
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;

	// 商户客户编号
	@ApiModelProperty(value = "商户客户编号")
	private String merchantId;

	@NotNull(message = "事件id不能为空")
	@ApiModelProperty(value = "事件id")
	private String incidentId;

	@NotNull(message = "积分id不能为空")
	@ApiModelProperty(value = "积分id")
	private String integralId;

	// 支付订单号
	@ApiModelProperty(value = "支付订单号")
	private String merOrderNo;

	// 订单金额
	@ApiModelProperty(value = "订单金额")
	private String orderAmt;

	// 领取方式 0：手动 1：自动 2：定向
	@ApiModelProperty(value = "领取方式")
	private String revType;

}
