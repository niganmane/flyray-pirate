package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 商户某一账户信息
 * @author he
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询商户某一账户信息请求参数")
public class MerchantOneAccountRequest {

	//平台编号
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;

	//商户编号
	@NotNull(message="商户编号不能为空")
	@ApiModelProperty(value = "商户编号")
	private String merchantId;

	//账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：冻结金额账户，ACC008：平台管理费账户，ACC009：平台服务费账户等......
	@NotNull(message="账户类型不能为空")
	@ApiModelProperty(value = "账户类型")
	private String accountType;

}
