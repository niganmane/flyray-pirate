package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("提交预订信息参数")
public class RestaurantSubmitReservationParam {

	@NotNull(message="预订信息不能为空")
	@ApiModelProperty("预订信息")
	private String reservation;
	
	@ApiModelProperty("支付订单号")
	private String payOrderNo;

}
