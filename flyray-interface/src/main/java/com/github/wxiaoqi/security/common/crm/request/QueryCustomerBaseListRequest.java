package com.github.wxiaoqi.security.common.crm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 查询客户基础信息请求实体
 * @author centerroot
 * @time 创建时间:2018年7月17日上午11:39:17
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询客户基础信息请求参数")
public class QueryCustomerBaseListRequest {
	
	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;
	
	@ApiModelProperty(value = "平台编号")
	private String platformId;

	@ApiModelProperty(value = "用户编号")
	private String customerId;
	
	/**
	 * 用户权限1，系统管理员2，平台管理员3，商户管理员4、平台操作员
	 */
	@ApiModelProperty(value = "用户权限")
	private int userType;
	
	@ApiModelProperty(value = "认证状态")
	private String authenticationStatus;
	
	@ApiModelProperty(value = "账户状态")
	private String status;
	
}
