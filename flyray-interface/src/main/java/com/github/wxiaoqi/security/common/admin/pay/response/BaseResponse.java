package com.github.wxiaoqi.security.common.admin.pay.response;

/**
 * 返回参数
 * 
 */
public class BaseResponse {
	private String code;// 返回错误码
	private String msg;// 返回错误信息
	private String resJrnNo;// 返回响应流水号
	private String reqJrnNo;// 返回请求流水号
	private boolean success;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getResJrnNo() {
		return resJrnNo;
	}

	public void setResJrnNo(String resJrnNo) {
		this.resJrnNo = resJrnNo;
	}

	public String getReqJrnNo() {
		return reqJrnNo;
	}

	public void setReqJrnNo(String reqJrnNo) {
		this.reqJrnNo = reqJrnNo;
	}

	public BaseResponse(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public BaseResponse() {
		super();
	}

	
}
