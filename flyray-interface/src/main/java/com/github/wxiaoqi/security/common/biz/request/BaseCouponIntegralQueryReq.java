package com.github.wxiaoqi.security.common.biz.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.admin.pay.request.BaseRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * 基础红包/积分查询参数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("基础红包/积分查询参数")
public class BaseCouponIntegralQueryReq extends BaseRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9055741238583250885L;

	// // 平台编号
	// @NotNull(message="平台编号不能为空")
	// private String acceptBizNo;
	// //商户号
	// @NotNull(message="商户号不能为空")
	// private String merNo;
	// // 商户会员号
	// @NotNull(message="商户会员号不能为空")
	// private String merCustNo;

	// 用户编号
	@ApiModelProperty(value = "用户编号")
	private String customerId;

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;

	// 个人客户编号
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;

	// 商户客户编号
	@ApiModelProperty(value = "商户客户编号")
	private String merchantId;

	// 事件ids
	@NotNull(message = "事件ID不能为空")
	@ApiModelProperty(value = "事件ids")
	private String incidentId;

	// 支付订单号
	@ApiModelProperty(value = "支付订单号")
	private String merOrderNo;

}
