package com.github.wxiaoqi.security.common.msg;

/**
 * 个人、商户类型
 */
public enum MerchantType {
	MER_PLATFORM("CUST00","平台"),
	MER_MERCHANT("CUST01","商户"),
	MER_PERSONAL("CUST02","个人");

    private String code;
    private String desc;

    private MerchantType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static MerchantType getSmsType(String code) {
        for (MerchantType o : MerchantType.values()) {
            if (o.getCode().equals(code)) {
                return o;
            }
        }
        return null;
    }
}
