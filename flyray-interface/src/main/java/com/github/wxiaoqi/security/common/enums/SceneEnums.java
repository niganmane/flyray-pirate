package com.github.wxiaoqi.security.common.enums;

public enum SceneEnums {
	
	RESTAURANT("SC0001", "小程序点餐"),
	FIGHT_GROUP("SC0002", "小程序拼团"),
	AUCTION("SC0003", "小程序竞拍"),
	SECKILL("SC0004", "每日秒杀"),
	MALL("SC0005", "小程序商城"),
	FIREDRILL_RECHARGE("SC0006", "火钻充值");
	
	private String code;
	private String name;
	
	private SceneEnums(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static SceneEnums getSceneModuleNo(String code){
		for (SceneEnums o : SceneEnums.values()) {
			if (o.getCode().equals(code)) {
				return o;
			}
		}
		return null;
	}
	
}
