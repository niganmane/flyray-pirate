package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/** 
* @author: bolei
* @date：2017年2月23日 下午12:46:48 
* @description：类说明 
*/

public class QueryPayStatusResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
     * 支付状态
     */
    private boolean payStatus;
    /**
     * 第三方交易流水号
     */
    private String remoteTxJournalNo;
    /**
     * 第三方交易日期
     */
    private Date remoteTxtDate;
    /**
     * 订单金额
     */
    private BigDecimal orderAmt;
    
    /**
     * 支付状态
     */
    private String payStatusStr;

    public boolean isPayStatus() {
        return payStatus;
    }

    public void setPayStatus(boolean payStatus) {
        this.payStatus = payStatus;
    }

    public String getRemoteTxJournalNo() {
        return remoteTxJournalNo;
    }

    public void setRemoteTxJournalNo(String remoteTxJournalNo) {
        this.remoteTxJournalNo = remoteTxJournalNo;
    }

    public Date getRemoteTxtDate() {
        return remoteTxtDate;
    }

    public void setRemoteTxtDate(Date remoteTxtDate) {
        this.remoteTxtDate = remoteTxtDate;
    }

    public BigDecimal getOrderAmt() {
		return orderAmt;
	}

	public void setOrderAmt(BigDecimal orderAmt) {
		this.orderAmt = orderAmt;
	}

	public String getPayStatusStr() {
        return payStatusStr;
    }

    public void setPayStatusStr(String payStatusStr) {
        this.payStatusStr = payStatusStr;
    }

}
