package com.github.wxiaoqi.security.common.enums;

/** 
* 出入账标识
*/

public enum InOutFlagEnums {

	in("1","入账"),
	out("2","出账");
	
    private String code;
    private String desc;
    
    private InOutFlagEnums (String code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static InOutFlagEnums getCommentModuleNo(String code){
        for(InOutFlagEnums o : InOutFlagEnums.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
