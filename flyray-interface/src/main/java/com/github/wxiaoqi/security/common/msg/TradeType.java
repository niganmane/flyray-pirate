package com.github.wxiaoqi.security.common.msg;

/**
 * 交易类型枚举
 * @author hexufeng
 * 2017年12月26日 下午6:56:02
 */
public enum TradeType {
	
	PAY("01","支付"),
	REFUND("02","退款"),
	CHARGE("03","充值"),
	WITHDRAW("04","提现"),
	PRERECEIPT("05","预收款"),
	PAYRECEIPT("06","消费"),
	SETTLEMENT("07","结算"),
	RECEIVE("08","领取");
	
	private String code;
	private String desc;
	
	private TradeType (String code,String desc){
		this.code = code;
		this.desc = desc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public static TradeType getTradeType(String code){
        for(TradeType o : TradeType.values()){
            if(o.getCode().equals(code)){
            	return o;
            }
        }
        return null;
    }

}
