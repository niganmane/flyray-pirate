package com.github.wxiaoqi.security.common.admin.pay.request;

import java.io.Serializable;

/**
 * 退款订单查询请求
 * @author hexufeng
 *
 */
public class QueryRefundOrderStatusRequest extends BaseRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 平台编号
	 */
	private String platformId;
	
	/**
	 * 退款订单号
	 */
	private String refundOrderNo;

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getRefundOrderNo() {
		return refundOrderNo;
	}

	public void setRefundOrderNo(String refundOrderNo) {
		this.refundOrderNo = refundOrderNo;
	}

}
