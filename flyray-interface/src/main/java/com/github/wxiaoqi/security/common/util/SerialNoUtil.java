package com.github.wxiaoqi.security.common.util;

/**
 * 随机流水号
 * @author hexufeng
 * 2018年1月12日 上午9:36:56
 */
public class SerialNoUtil {
	
	private static byte[] lock = new byte[0];  
	  
    // 位数，默认是8位  
    private final static long w = 100000000;  
  
    public static String createID() {  
        long r = 0;  
        synchronized (lock) {  
            r = (long) ((Math.random() + 1) * w);  
        }  
  
        return "CRM" + System.currentTimeMillis() + String.valueOf(r).substring(1);  
    }
    
    public static void main(String[] args) {
    	String createID = createID();
    	System.out.println(createID);
	}

}
