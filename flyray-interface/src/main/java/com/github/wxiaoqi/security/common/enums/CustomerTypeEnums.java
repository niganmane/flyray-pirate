package com.github.wxiaoqi.security.common.enums;

/** 
* 客户类型
*/

public enum CustomerTypeEnums {

	mer_user("01","商户"),
	per_user("02","个人");
	
    private String code;
    private String desc;
    
    private CustomerTypeEnums (String code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static CustomerTypeEnums getCommentModuleNo(String code){
        for(CustomerTypeEnums o : CustomerTypeEnums.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
