package com.github.wxiaoqi.security.common.crm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 动态路由表
 * @author centerroot
 * @email ${email}
 * @date 2018-08-28 15:36:21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询动态路由表")
public class GatewayApiDefineRequest {
	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;
	
	@ApiModelProperty(value = "序号")
    private String id;

	@ApiModelProperty(value = "请求地址")
    private String path;

	@ApiModelProperty(value = "服务ID")
    private String serviceId;

	@ApiModelProperty(value = "请求地址")
    private String url;

	@ApiModelProperty(value = "retryable")
    private Integer retryable;

	@ApiModelProperty(value = "enabled")
    private Integer enabled;

	@ApiModelProperty(value = "stripPrefix")
    private Integer stripPrefix;

	@ApiModelProperty(value = "apiName")
    private String apiName;
}
