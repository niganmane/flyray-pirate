package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 代付申请
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "代付申请请求参数")
public class PayForAnotherApplyRequest {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
    private String platformId;

	@NotNull(message="商户编号不能为空")
	@ApiModelProperty(value = "商户编号")
	private String merchantId;
	
	@NotNull(message="银行账户名不能为空")
	@ApiModelProperty(value = "银行账户名")
	private String bankAccountName;
	
	@NotNull(message="银行名称不能为空")
	@ApiModelProperty(value = "银行名称")
	private String bankName;
	
	@NotNull(message="银行卡号不能为空")
	@ApiModelProperty(value = "银行卡号")
	private String bankNo;
	
	@NotNull(message="代付金额不能为空")
	@ApiModelProperty(value = "代付金额")
	private String amount;
	
}