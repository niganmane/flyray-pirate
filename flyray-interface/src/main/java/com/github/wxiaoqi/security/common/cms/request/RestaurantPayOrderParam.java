package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("预约点餐支付完成修改状态参数")
public class RestaurantPayOrderParam extends BaseParam{
	
	@NotNull(message="用户编号不能为空")
	@ApiModelProperty("用户编号")
	private String customerId;

}
