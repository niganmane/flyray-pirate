package com.github.wxiaoqi.security.common.codex.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("营销活动请求参数")
public class MarActivityRequestParam {
	
	@ApiModelProperty("seqNo")
	private String seqNo;
	
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@ApiModelProperty("商户编号")
	private String merchantId;
	
	@ApiModelProperty("活动编号")
	private String actId;
	
	@ApiModelProperty("活动详情")
	private String actInfo;
	
	@ApiModelProperty("创建人")
	private String createUser;
	
	@ApiModelProperty("活动状态")
	private String actStauts;
	
	@ApiModelProperty("备注")
	private String actRemark;
	
	@ApiModelProperty("token")
	private String actToken;
	
	
}
