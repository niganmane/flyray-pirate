package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("注册请求参数")
public class RegisterRequestParam {

	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;

	@NotNull(message="手机号不能为空")
	@ApiModelProperty("手机号")
	private String phone;

	@NotNull(message="密码不能为空")
	@ApiModelProperty("密码")
	private String password;
	

	@NotNull(message="验证码不能为空")
	@ApiModelProperty("验证码")
	private String smsCode;

	/**
	 * 01:个人  02：商户
	 */
	@ApiModelProperty("角色类型")
	private String roleType;

	@ApiModelProperty("设备ID")
	private String deviceId;
	
	/**
	 * 设备类型：0-android、1-ios、2-PC
	 */
	@ApiModelProperty("设备类型")
	private String deviceType;
	
}
