package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.admin.pay.request.BaseRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设置重置交易密码请求
 * @author centerroot
 * @time 创建时间:2018年9月25日上午10:42:20
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "设置重置交易密码请求")
public class CustomerSetupPasswordRequest extends BaseRequest{

	private static final long serialVersionUID = 1L;

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message = "用户编号不能为空")
	@ApiModelProperty(value = "用户编号")
	private String customerId;
	
	@NotNull(message="设置密码不能为空")
	@ApiModelProperty(value = "设置密码")
	private String newPassword;
	
	@NotNull(message="确认设置密码不能为空")
	@ApiModelProperty(value = "确认设置密码")
	private String confirmPassword;

}
