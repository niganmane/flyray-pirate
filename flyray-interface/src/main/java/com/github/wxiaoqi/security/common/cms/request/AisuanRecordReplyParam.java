package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("爱算回复记录参数")
public class AisuanRecordReplyParam extends BaseParam{
	
	
	@ApiModelProperty("记录编号")
	private Long id;
	
	
	@NotNull
	@ApiModelProperty("父级记录编号")
	private Long parentId;
	
	
	@ApiModelProperty("回复人编号")
	private Long applyUserId;
	
	
	@ApiModelProperty("咨询人编号")
	private Long customerId;
	
	@NotNull
	@ApiModelProperty("回复内容")
	private String content;
	
	@NotNull
	@ApiModelProperty("类型")
	private Integer type;
	
	@NotNull
	@ApiModelProperty("咨询记录编号")
	private Long topId;
	
	
	
}
