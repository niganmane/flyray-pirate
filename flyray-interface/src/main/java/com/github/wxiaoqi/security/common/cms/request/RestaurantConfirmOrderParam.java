package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("订单确认收货参数")
public class RestaurantConfirmOrderParam extends BaseParam{
	
	@NotNull(message="支付订单号不能为空")
	@ApiModelProperty("支付订单号")
	private String payOrderNo;

}
