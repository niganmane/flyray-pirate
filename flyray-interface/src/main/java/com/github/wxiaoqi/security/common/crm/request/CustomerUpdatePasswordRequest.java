package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.admin.pay.request.BaseRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 修改交易密码
 * @author centerroot
 * @time 创建时间:2018年9月25日上午10:42:38
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "修改交易密码")
public class CustomerUpdatePasswordRequest extends BaseRequest{

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message = "用户编号不能为空")
	@ApiModelProperty(value = "用户编号")
	private String customerId;
	
	@NotNull(message="原密码不能为空")
	@ApiModelProperty(value = "原密码")
	private String password;
	
	@NotNull(message="新密码不能为空")
	@ApiModelProperty(value = "新密码")
	private String newPassword;
	
	@NotNull(message="再次输入新密码不能为空")
	@ApiModelProperty(value = "再次输入新密码")
	private String confirmPassword;

}
