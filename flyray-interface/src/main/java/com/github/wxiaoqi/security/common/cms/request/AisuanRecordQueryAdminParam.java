package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("后台记录查询参数")
public class AisuanRecordQueryAdminParam extends BaseParam{
	
	
	@ApiModelProperty("页数")
	private Integer page;
	
	@ApiModelProperty("数量")
	private Integer limit;
	
	@ApiModelProperty("会员编号")
	private Long customerId;
	
	@ApiModelProperty("开始时间")
	private String startTime;
	
	@ApiModelProperty("结束时间")
	private String endTime;
	
	@ApiModelProperty("支付状态")
	private Integer payStatus;
	
	@ApiModelProperty("订单状态")
	private Integer orderStatus;
	
	@ApiModelProperty("类型")
	private Integer type;
	
	@ApiModelProperty("所属咨询")
	private Long topId;
	
	@ApiModelProperty("用户角色")
	private Integer userType;
	
	@ApiModelProperty("指定咨询人编号")
	private Long targetTea;
	
}
