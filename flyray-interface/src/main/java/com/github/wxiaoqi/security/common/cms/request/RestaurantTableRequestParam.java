package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("餐饮基础信息参数")
public class RestaurantTableRequestParam extends BaseParam{
	
	
	@ApiModelProperty("序号")
	private String id;
	
	@ApiModelProperty("餐桌id")
	private String tableId;
	
	@ApiModelProperty("餐桌名称")
	private String tableName;
	
	@ApiModelProperty("人数")
	private String tablePeople;
	
	@ApiModelProperty("状态")
	private String status;
	
}
