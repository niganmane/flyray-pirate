package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序竞拍参数")
public class AuctionRequestParam extends BaseParam{
	
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@ApiModelProperty("商户号")
	private String merId;
	
	@ApiModelProperty("序号")
	private String id;
	
	@ApiModelProperty("商品编号")
	private String goodsId;
	
	@ApiModelProperty("商品名称")
	private String goodsName;
	
	@ApiModelProperty("商品说明")
	private String goodsDescription;
	
	@ApiModelProperty("商品照片")
	private String goodsImg;
	
	@ApiModelProperty("起拍价")
	private String startingPrice;
	
	@ApiModelProperty("截止时间")
	private String deadlineTimeStr;
	
}
