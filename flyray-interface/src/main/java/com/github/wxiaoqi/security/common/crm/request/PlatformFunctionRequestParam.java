package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 平台功能
 * @author chj
 * @time 创建时间:2018年7月17日上午9:12:35
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "平台功能请求参数")
public class PlatformFunctionRequestParam {
	
	@ApiModelProperty(value = "序号")
    private String id;
	
	@NotNull(message="代码不能为空")
	@ApiModelProperty(value = "代码")
    private String code;
	
	@NotNull(message="名称不能为空")
	@ApiModelProperty(value = "名称")
	private String name;
	
	@ApiModelProperty(value = "图标")
    private String icon;
	
}
