package com.github.wxiaoqi.security.common.admin.pay.response;

/**
 * 银行编码列表
 * @author hexufeng
 *
 */
public class BankCode {
	
	//银行编码
    private String bankCode;
	
	//银行名称
    private String bankName;

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

}
