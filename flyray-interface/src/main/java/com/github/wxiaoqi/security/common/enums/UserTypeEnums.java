package com.github.wxiaoqi.security.common.enums;

/** 
* @author: bolei
* @date：2017年11月2日 下午12:26:50
* @description：类说明 
*/

public enum UserTypeEnums {

	SYSTEM_ADMIN(1,"系统管理员"),
	PLATFORM_ADMIN(2,"平台管理员"),
	MERCHANT_ADMIN(3,"商户管理员"),
	PLATFORM_OPERATOR(4,"平台操作员"),
    AGENT_OPERATOR(5,"代理商管理员");
	
    private Integer code;
    private String desc;
    
    private UserTypeEnums (Integer code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static UserTypeEnums getCommentModuleNo(String code){
        for(UserTypeEnums o : UserTypeEnums.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
