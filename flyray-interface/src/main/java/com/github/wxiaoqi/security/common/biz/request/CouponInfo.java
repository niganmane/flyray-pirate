package com.github.wxiaoqi.security.common.biz.request;

import java.io.Serializable;

/**
 * 红包信息
 * @author hexufeng
 * 2018年1月30日 下午2:19:28
 */
public class CouponInfo implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/**
     * 红包流水号
     */
    private String couponSerialNo;

    /**
     * 红包ID
     */
    private String couponId;
    
    /**
     * 红包可用场景ID
     */
    private String sceneId;
    
    /**
     * 有效状态 00：有效 01：无效
     */
    private String effectiveStatus;

	public String getCouponSerialNo() {
		return couponSerialNo;
	}

	public void setCouponSerialNo(String couponSerialNo) {
		this.couponSerialNo = couponSerialNo;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public String getSceneId() {
		return sceneId;
	}

	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}

	public String getEffectiveStatus() {
		return effectiveStatus;
	}

	public void setEffectiveStatus(String effectiveStatus) {
		this.effectiveStatus = effectiveStatus;
	}

}
