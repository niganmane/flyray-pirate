package com.github.wxiaoqi.security.common.crm.request;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 用户绑卡信息
 * @author centerroot
 * @time 创建时间:2018年8月16日下午5:00:24
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询用户绑卡信息请求参数")
public class PersonalBindCardRequest {
	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;
	
	    //流水号
	@ApiModelProperty(value = "流水号")
    private String id;
	
	    //平台编号
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	    //个人客户编号
	@ApiModelProperty(value = "个人客户编号")
    private String personalId;
	
	    //银行卡号  隐藏中间数位的银行卡号
	@ApiModelProperty(value = "银行卡号")
    private String bindCardNo;
	
	    //加密银行卡号
	@ApiModelProperty(value = "加密银行卡号")
    private String bindEncryptCardNo;
	
	    //持卡人姓名
	@ApiModelProperty(value = "持卡人姓名")
    private String cardholderName;
	
	    //银行编号
	@ApiModelProperty(value = "银行编号")
    private String bankNo;
	
	    //银行名称
	@ApiModelProperty(value = "银行名称")
    private String bankName;
	
	    //支行编号
	@ApiModelProperty(value = "支行编号")
    private String subbranchNo;
	
	    //支行信息
	@ApiModelProperty(value = "支行信息")
    private String subbranchName;
	
	    //状态  00：待验证  01：绑定  02：解绑 
	@ApiModelProperty(value = "状态")
    private String status;
	
	    //创建时间
	@ApiModelProperty(value = "创建时间")
    private Date createTime;
	
	    //更新时间
	@ApiModelProperty(value = "更新时间")
    private Date updateTime;
	
}
