package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("爱算查询单条记录参数")
public class AisuanNoCompleteRecordQueryParam extends BaseParam{
	
	@NotNull
	@ApiModelProperty("用户会员号")
	private Long customerId;
	
	@NotNull
	@ApiModelProperty("指定回复人")
	private Long targetTea;
	
	
	@NotNull
	@ApiModelProperty("是否指定回复人")
	private Long isTarget;
	
	

}
