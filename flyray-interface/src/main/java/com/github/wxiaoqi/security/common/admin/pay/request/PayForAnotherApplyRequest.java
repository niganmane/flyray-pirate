package com.github.wxiaoqi.security.common.admin.pay.request;

import java.io.Serializable;

/**
 * 代付申请请求
 * @author hexufeng
 *
 */
public class PayForAnotherApplyRequest extends BaseRequest implements Serializable{

	private static final long serialVersionUID = 1L;

	//代付订单号
	private String orderId;

	//平台编号
	private String platformId;

	//代付金额
	private String amount;
	
	//代付手续费
	private String payFee;

	//银行编码
	private String bankCode;
	
	//银行名称
	private String bankName;
	
	//银行卡号
	private String bankNo;

	//银行卡号密文
	private String bankAccountNo;

	//银行账户名
	private String bankAccountName;

	//客户账号
	private String customerId;

	//商户账号
	private String merchantId;

	//备注
	private String summary;

	//银行联行号
	private String bankUnionCode;

	//支付宝账号
	private String alipayNo;
	
	//代付通道编号
	private String payChannelNo;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPayFee() {
		return payFee;
	}

	public void setPayFee(String payFee) {
		this.payFee = payFee;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankAccountNo() {
		return bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankNo() {
		return bankNo;
	}

	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getBankUnionCode() {
		return bankUnionCode;
	}

	public void setBankUnionCode(String bankUnionCode) {
		this.bankUnionCode = bankUnionCode;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getAlipayNo() {
		return alipayNo;
	}

	public void setAlipayNo(String alipayNo) {
		this.alipayNo = alipayNo;
	}

	public String getPayChannelNo() {
		return payChannelNo;
	}

	public void setPayChannelNo(String payChannelNo) {
		this.payChannelNo = payChannelNo;
	}


}
