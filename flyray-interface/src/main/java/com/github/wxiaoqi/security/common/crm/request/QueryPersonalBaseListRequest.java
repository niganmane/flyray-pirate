package com.github.wxiaoqi.security.common.crm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 查询用户基础信息请求实体
 * @author centerroot
 * @time 创建时间:2018年7月16日下午4:23:01
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询用户基础信息请求参数")
public class QueryPersonalBaseListRequest {
	
	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;
	
	@ApiModelProperty(value = "平台编号")
	private String platformId;

	@ApiModelProperty(value = "个人客户编号")
	private String personalId;
	
	@ApiModelProperty(value = "用户编号")
	private String customerId;

	@ApiModelProperty(value = "真实姓名")
	private String realName;

	@ApiModelProperty(value = "归属人")
	private Long owner;
	
	/**
	 * 用户权限1，系统管理员2，平台管理员3，商户管理员4、平台操作员
	 */
	@ApiModelProperty(value = "用户权限")
	private int userType;
	
	@ApiModelProperty(value = "认证状态")
	private String authenticationStatus;
	
	@ApiModelProperty(value = "账户状态")
	private String status;
	
	@ApiModelProperty(value = "用户名")
	private String name;
	
	@ApiModelProperty(value = "用户Id")
	private Long userId;
	
	@ApiModelProperty(value = "页面标签")
	private String pageTag;
}
