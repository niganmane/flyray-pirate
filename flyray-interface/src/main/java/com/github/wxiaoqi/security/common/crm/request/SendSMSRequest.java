package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "发送短信请求参数")
public class SendSMSRequest {
	
	@NotNull(message="手机号不能为空")
	@ApiModelProperty(value = "手机号")
	private String phone;

	@NotNull(message="消息类型不能为空")
	@ApiModelProperty(value = "消息类型")
    private String busType;

}
