package com.github.wxiaoqi.security.common.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DateUtils {
	/** 时间格式(yyyy-MM-dd) */
	public final static String DATE_PATTERN = "yyyy-MM-dd";
	/** 时间格式(yyyy-MM-dd HH:mm:ss) */
	public final static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

	public static String format(Date date) {
		return format(date, DATE_PATTERN);
	}

	public static String format(Date date, String pattern) {
		if (date != null) {
			SimpleDateFormat df = new SimpleDateFormat(pattern);
			return df.format(date);
		}
		return null;
	}

	//字符串转date
	public static Date strToDate(String strDate) {
		     SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN);
		     Date strtodate=null;
			try {
				strtodate = formatter.parse(strDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		     return strtodate;
		  }
	// 获取年龄或者工龄
	public static int getAge(Date birthDay) {
		Calendar cal = Calendar.getInstance();
		if (cal.before(birthDay)) {
			throw new IllegalArgumentException("The birthDay is before Now.It's unbelievable!");
		}
		int yearNow = cal.get(Calendar.YEAR);
		int monthNow = cal.get(Calendar.MONTH);
		int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
		cal.setTime(birthDay);
		int yearBirth = cal.get(Calendar.YEAR);
		int monthBirth = cal.get(Calendar.MONTH);
		int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
		int age = yearNow - yearBirth;
		if (monthNow <= monthBirth) {
			if (monthNow == monthBirth) {
				if (dayOfMonthNow < dayOfMonthBirth)
					age--;
			} else {
				age--;
			}
		}
		return age;
	}
	public static DayCompare dayComparePrecise(Date fromDate,Date toDate){
	    Calendar  from  =  Calendar.getInstance();
	    from.setTime(fromDate);
	    Calendar  to  =  Calendar.getInstance();
	    to.setTime(toDate);

	    int fromYear = from.get(Calendar.YEAR);
	    int fromMonth = from.get(Calendar.MONTH);
	    int fromDay = from.get(Calendar.DAY_OF_MONTH);

	    int toYear = to.get(Calendar.YEAR);
	    int toMonth = to.get(Calendar.MONTH);
	    int toDay = to.get(Calendar.DAY_OF_MONTH);
	    int year = toYear  -  fromYear;
	    int month = toMonth  - fromMonth;
	    int day = toDay  - fromDay;
	    return DayCompare.builder().day(day).month(month).year(year).build();
	}
	public static DayCompare dayCompare(Date fromDate,Date toDate){
	    Calendar  from  =  Calendar.getInstance();
	    from.setTime(fromDate);
	    Calendar  to  =  Calendar.getInstance();
	    to.setTime(toDate);
	    //只要年月
	    int fromYear = from.get(Calendar.YEAR);
	    int fromMonth = from.get(Calendar.MONTH);

	    int toYear = to.get(Calendar.YEAR);
	    int toMonth = to.get(Calendar.MONTH);

	    int year = toYear  -  fromYear;
	    int month = toYear *  12  + toMonth  -  (fromYear  *  12  +  fromMonth);
	    int day = (int) ((to.getTimeInMillis()  -  from.getTimeInMillis())  /  (24  *  3600  *  1000));
	    return DayCompare.builder().day(day).month(month).year(year).build();
	}
	public static String yearCompare(Date fromDate,Date toDate){
	    DayCompare result = dayComparePrecise(fromDate, toDate);
	    double month = result.getMonth();
	    double year = result.getYear();
	    //返回2位小数，并且四舍五入
	    DecimalFormat df = new DecimalFormat("######0.0");
	    return df.format(year + month / 12);
	}
	/**
	 * 获取几天之前或者之后的日期
	 * */
	public static String datetimeAgo(int days){
		  Calendar calendar = Calendar.getInstance();
		  SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		  calendar.add(Calendar.DATE, days);
		  return sdf1.format(calendar.getTime());
	}
	
	public static void main(String[] args) throws ParseException, Exception {
		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
		//System.out.println(getAge(sdf.parse("2018-01-02")));
		   System.out.println(yearCompare(sdf.parse("2015-01-14"),new Date()));
	}
}
