package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("添加配送信息参数")
public class RestaurantAddDistributionParam extends BaseParam{
	
	@NotNull(message="用户账号不能为空")
	@ApiModelProperty("用户账号")
	private String perId;
	
	@ApiModelProperty("配送id")
	private String distributionId;
	
	@NotNull(message="配送姓名不能为空")
	@ApiModelProperty("配送姓名")
	private String distributionName;
	
	@NotNull(message="配送手机号不能为空")
	@ApiModelProperty("配送手机号")
	private String distributionPhone;
	
	@NotNull(message="省不能为空")
	@ApiModelProperty("省")
	private String distributionProvince;
	
	@NotNull(message="市不能为空")
	@ApiModelProperty("市")
	private String distributionCity;
	
	@NotNull(message="区不能为空")
	@ApiModelProperty("区")
	private String distributionArea;
	
	@NotNull(message="地址不能为空")
	@ApiModelProperty("地址")
	private String distributionAddress;
	
	@ApiModelProperty("默认标识")
	private String isDefault;
	
}
