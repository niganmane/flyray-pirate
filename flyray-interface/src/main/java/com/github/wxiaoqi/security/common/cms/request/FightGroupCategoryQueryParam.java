package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("拼团商品参数")
public class FightGroupCategoryQueryParam extends BaseParam{
	
	@ApiModelProperty("团类目编号")
	private String groupsId;
	
	@ApiModelProperty("页数")
	private Integer page;
	
	@ApiModelProperty("条数")
	private Integer limit;
	

}
