package com.github.wxiaoqi.security.common.msg;

/**
 * 优惠券使用状态
 * @author hexufeng
 * 2018年1月30日 上午10:04:04
 */
public enum CouponUseStatus {

	UNUSED("00","未使用"),
	USING("01","使用中"),
	USED("02","已使用");
	
	private String code;
	private String desc;
	
	private CouponUseStatus (String code,String desc){
		this.code = code;
		this.desc = desc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public static CouponUseStatus getCouponUseStatus(String code){
        for(CouponUseStatus o : CouponUseStatus.values()){
            if(o.getCode().equals(code)){
            	return o;
            }
        }
        return null;
    }

}
