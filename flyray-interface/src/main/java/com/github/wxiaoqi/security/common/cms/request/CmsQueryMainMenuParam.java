package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序查询活动参数")
public class CmsQueryMainMenuParam {

	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
}
