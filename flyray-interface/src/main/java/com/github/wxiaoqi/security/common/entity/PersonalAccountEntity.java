package com.github.wxiaoqi.security.common.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.github.wxiaoqi.security.common.entity.BaseEntity;


/**
 * 个人账户信息
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
public class PersonalAccountEntity extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//序号
    private Integer id;
	
	    //账户编号
    private String accountId;
	
	    //平台编号
    private String platformId;
	
	    //个人信息编号
    private String personalId;
	
	    //账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：冻结金额账户，ACC008：平台管理费账户，ACC009：平台服务费账户，ACC010：火源账户，ACC011：火钻账户，ACC012：原力值账户等......
    private String accountType;
	
	    //币种  CNY：人民币
    private String ccy;
	
	    //账户余额
    private BigDecimal accountBalance;
	
	    //冻结金额
    private BigDecimal freezeBalance;
	
	    //校验码（余额加密值）
    private String checkSum;
	
	    //账户状态 00：正常，01：冻结
    private String status;
	
	    //创建时间
    private Date createTime;
	
	    //更新时间
    private Date updateTime;
	

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 设置：账户编号
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * 获取：账户编号
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public String getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：个人信息编号
	 */
	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}
	/**
	 * 获取：个人信息编号
	 */
	public String getPersonalId() {
		return personalId;
	}
	/**
	 * 设置：账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：冻结金额账户，ACC008：平台管理费账户，ACC009：平台服务费账户， ACC010：火源账户，ACC011：火钻账户，ACC012：原力值账户等......
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	/**
	 * 获取：账户类型  ACC001：余额账户，ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：冻结金额账户，ACC008：平台管理费账户，ACC009：平台服务费账户， ACC010：火源账户，ACC011：火钻账户，ACC012：原力值账户等......
	 */
	public String getAccountType() {
		return accountType;
	}
	/**
	 * 设置：币种  CNY：人民币
	 */
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	/**
	 * 获取：币种  CNY：人民币
	 */
	public String getCcy() {
		return ccy;
	}
	/**
	 * 设置：账户余额
	 */
	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}
	/**
	 * 获取：账户余额
	 */
	public BigDecimal getAccountBalance() {
		return accountBalance;
	}
	/**
	 * 设置：冻结金额
	 */
	public void setFreezeBalance(BigDecimal freezeBalance) {
		this.freezeBalance = freezeBalance;
	}
	/**
	 * 获取：冻结金额
	 */
	public BigDecimal getFreezeBalance() {
		return freezeBalance;
	}
	/**
	 * 设置：校验码（余额加密值）
	 */
	public void setCheckSum(String checkSum) {
		this.checkSum = checkSum;
	}
	/**
	 * 获取：校验码（余额加密值）
	 */
	public String getCheckSum() {
		return checkSum;
	}
	/**
	 * 设置：账户状态 00：正常，01：冻结
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 获取：账户状态 00：正常，01：冻结
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
}
