package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询平台安全配置请求参数")
public class SafetyConfigRequest {
	
	@ApiModelProperty(value = "序号")
    private String id;
	
	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;
	
	@NotNull
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	/*@ApiModelProperty(value = "加密类型")
	private String encryptionType;
	
	@ApiModelProperty(value = "加密方式")
	private String encryptionMethod;*/
	
	@ApiModelProperty(value = "盐值")
	private String appKey;
	
	@ApiModelProperty(value = "公钥")
	private String publicKey;
	
	@ApiModelProperty(value = "私钥")
	private String privateKey;

}
