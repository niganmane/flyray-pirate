package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序竞拍查询参数")
public class AuctionQueryParam extends BaseParam{
	
	@ApiModelProperty("商品编号")
	private String goodsId;
	
	@ApiModelProperty("页数")
	private Integer page;
	
	@ApiModelProperty("数量")
	private Integer limit;

}
