package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("点餐餐品信息参数")
public class RestaurantOrderDishesParam extends BaseParam{
	
	@ApiModelProperty("支付订单号")
	private String payOrderNo;
	
	@NotNull(message="餐桌编号不能为空")
	@ApiModelProperty("餐桌编号")
	private String tableId;

}
