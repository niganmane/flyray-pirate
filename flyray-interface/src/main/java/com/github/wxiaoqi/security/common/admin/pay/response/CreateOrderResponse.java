package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;

/** 
* @author: bolei
* @date：Jun 14, 2017 8:21:27 AM 
* @description：类描述
*/

public class CreateOrderResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 订单号
	 */
	private String payOrderNo;

	public String getPayOrderNo() {
		return payOrderNo;
	}

	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}

}
