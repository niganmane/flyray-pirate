package com.github.wxiaoqi.security.common.biz.request;
import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.admin.pay.request.BaseRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 查询最优优惠券，积分
 * @author wzg
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("查询最优优惠券，积分")
public class BestCouponIntegralQueryReq extends BaseRequest{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;

	// 个人客户编号
	@NotNull(message = "个人客户编号不能为空")
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;
	
	@NotNull(message="订单金额不能为空")
	@ApiModelProperty(value = "用户编号")
	private String orderAmt;
	
	// 场景ID
	@NotNull(message="场景编号不能为空")
	@ApiModelProperty(value = "用户编号")
	private String sceneId;
	
//	// 商户会员号
//	@NotNull(message="商户会员号不能为空")
//	private String merCustNo;
//	// 平台编号
//	@NotNull(message="平台编号不能为空")
//	private String acceptBizNo;
	
}
