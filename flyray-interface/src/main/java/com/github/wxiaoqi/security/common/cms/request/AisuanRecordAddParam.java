package com.github.wxiaoqi.security.common.cms.request;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-10 10:58:58
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("添加预测")
public class AisuanRecordAddParam extends BaseParam {
	private static final long serialVersionUID = 1L;
	
	    //主键
	@ApiModelProperty("主键")
    private Long id;
	
	
	    //客户编号
	@NotNull(message="客户编号不能为空")
	@ApiModelProperty("客户编号")
    private Long customerId;
	
	    //1、一事一测2、一年运势预测3、八字简批4、个人起名改名5、单位企业起名6、商标命名7、开业择日8、大型户外活动择日，策划9、结婚择日10、八字合婚11、免费风水咨询
	@NotNull(message="预测类型不能为空")
	@ApiModelProperty("预测类型")
	private Integer type;
	
	    //咨询内容
	@NotNull(message="咨询内容不能为空")
	@ApiModelProperty("咨询内容")
    private String content;
	
	

}
