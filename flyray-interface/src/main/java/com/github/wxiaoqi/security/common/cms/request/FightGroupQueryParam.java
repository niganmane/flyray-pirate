package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("拼团商品参数")
public class FightGroupQueryParam extends BaseParam{
	
	@ApiModelProperty("商品名称")
	private String goodsName;
	
	@ApiModelProperty("商品编号")
	private String goodsId;
	
	@ApiModelProperty("页数")
	private Integer page;
	
	@ApiModelProperty("条数")
	private Integer limit;
	
	@ApiModelProperty("用户名")
	private String name;
	
	

}
