package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("爱算查询单条记录参数")
public class AisuanRecordQueryOneParam extends BaseParam{
	
	@NotNull
	@ApiModelProperty("记录编号")
	private Long id;

}
