package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序评论参数")
public class CmsSaveCommentParam extends BaseParam {
	@NotNull(message="用户信息编号不能为空")
	@ApiModelProperty("用户信息编号")
	private String customerId;
	@ApiModelProperty("评论类型")
	private String commentType;
	@NotNull(message="观点编号不能为空")
	@ApiModelProperty("评论的目标id")
	private String commentTargetId;
	@ApiModelProperty("发表评论的用户昵称")
	private String commentByName;
	@ApiModelProperty("评论的目标用户昵称")
	private String commentTargetUserName;
	@ApiModelProperty("评论的目标用户id")
	private String commentTargetUserId;
	@ApiModelProperty("评论模块编号01社群 02卖朋友")
	private String commentModuleNo;
	@ApiModelProperty("回复评论的目标id")
	private String parentId;
	@ApiModelProperty("评论内容")
	private String commentContent;
	@ApiModelProperty("发表评论的用户头像")
	private String avatarUrl;
}
