package com.github.wxiaoqi.security.common.crm.request;


import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 新增平台/商户支付通道信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "新增平台 商户支付通道信息")
public class AddPayChannelConfigRequest {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	@ApiModelProperty(value = "商户号")
	private String merchantId;
	
	
	@ApiModelProperty(value = "支付通道编号")
    private String payChannelNo;
	
	@ApiModelProperty(value = "支付通道名称")
	private String payChannelName;
	    
	    
	    //支付公司编号
	@ApiModelProperty(value = "支付公司编号")
	private String payCompanyNo;
	
	@ApiModelProperty(value = "渠道")
	private String channel;
	
	@ApiModelProperty(value = "客户类型")
    private String customerType;
	
	@ApiModelProperty(value = "费率代码")
    private String feeCode;
	
	@ApiModelProperty(value = "第三方商户号")
    private String outMerNo;
	
	@ApiModelProperty(value = "第三方子商户号")
    private String outSubMerNo;
	
	@ApiModelProperty(value = "第三方账号")
    private String outMerAccount;
	
	@ApiModelProperty(value = "第三方公钥")
    private String outMerPublicKey;
	
	@ApiModelProperty(value = "第三方私钥")
    private String outMerPrivateKey;
	
	@ApiModelProperty(value = "加密方式")
    private String encryptionMethod;
	
	@ApiModelProperty(value = "证书地址")
    private String keyPath;
	
	@ApiModelProperty(value = "证书密码")
    private String keyPwd;
	
	@ApiModelProperty(value = "状态")
	private String status;
	
}
