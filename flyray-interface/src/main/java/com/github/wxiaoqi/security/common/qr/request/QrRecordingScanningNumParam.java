package com.github.wxiaoqi.security.common.qr.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("记录扫描次数请求参数")
public class QrRecordingScanningNumParam {
	
	@ApiModelProperty("平台编号")
	private String platformId;

	@ApiModelProperty("商户号")
	private String merchantId;
	
	@ApiModelProperty("商品编号")
	private String goodsId;
	
	@ApiModelProperty("openid")
	private String openid;
	
	@ApiModelProperty("昵称")
	private String nickname;

}
