package com.github.wxiaoqi.security.common.msg;

/**
 * 客户类型
 */
public enum CustomerType {

	CUST_PERSONAL("CUST001","个人客户"),
	CUST_MERCHANT("CUST002","企业客户"),
	CUST_THIRD("CUST003","第三方");

    private String code;
    private String desc;

    private CustomerType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static CustomerType getSmsType(String code) {
        for (CustomerType o : CustomerType.values()) {
            if (o.getCode().equals(code)) {
                return o;
            }
        }
        return null;
    }
}
