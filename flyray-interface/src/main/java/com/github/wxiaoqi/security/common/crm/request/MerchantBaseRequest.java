package com.github.wxiaoqi.security.common.crm.request;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 商户客户基础信息
 * @author centerroot
 * @time 创建时间:2018年7月17日上午9:12:35
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "商户客户基础信息请求参数")
public class MerchantBaseRequest {
	
	@ApiModelProperty(value = "序号")
    private Integer id;
	
	@ApiModelProperty(value = "商户编号")
    private String merchantId;
	
	@ApiModelProperty(value = "商户名称")
	private String merchantName;
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	@ApiModelProperty(value = "用户编号")
    private String customerId;
	
	@ApiModelProperty(value = "第三方会员编号")
    private String thirdNo;
	
	@ApiModelProperty(value = "父级商户编号")
    private String parentMerId;

	@ApiModelProperty(value = "商户类型")
    private String merType;
	
	@ApiModelProperty(value = "企业名称")
    private String companyName;
	
	@ApiModelProperty(value = "经营范围")
    private String businessScope;
	
	@ApiModelProperty(value = "工商注册号")
    private String businessNo;
	
	@ApiModelProperty(value = "法人姓名")
    private String legalPersonName;
	
	@ApiModelProperty(value = "法人证件类型")
    private String legalPersonCredType;
	
	@ApiModelProperty(value = "法人证件号码")
    private String legalPersonCredNo;
	
	@ApiModelProperty(value = "营业执照")
    private String businessLicence;
	
	@ApiModelProperty(value = "联系电话")
    private String phone;
	
	@ApiModelProperty(value = "企业座机")
    private String mobile;
	
	@ApiModelProperty(value = "企业传真")
    private String fax;
	
	@ApiModelProperty(value = "企业网址")
    private String httpAddress;
	
	@ApiModelProperty(value = "注册资金")
    private BigDecimal registeredCapital;
	
	@ApiModelProperty(value = "企业地址")
    private String companyAddress;
	
	@ApiModelProperty(value = "认证状态")
    private String authenticationStatus;
	
	@ApiModelProperty(value = "账户状态")
    private String status;
	
	@ApiModelProperty(value = "创建时间")
    private Date createTime;

	@ApiModelProperty(value = "更新时间")
    private Date updateTime;
	
	@ApiModelProperty(value = "归属人")
    private long owner;
	
	@ApiModelProperty(value = "token")
	private String token;
	
	@ApiModelProperty(value = "province")
	private String province;
	
	@ApiModelProperty(value = "city")
	private String city;
	
	@ApiModelProperty(value = "county")
	private String county;
}
