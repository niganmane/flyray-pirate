package com.github.wxiaoqi.security.common.exception.auth;

import com.github.wxiaoqi.security.common.constant.CommonConstants;
import com.github.wxiaoqi.security.common.exception.BaseException;

/** 
* @author: bolei
* @date：2018年5月7日 上午8:28:11 
* @description：用户账号异常
*/

public class ImageCodeInvalidException extends BaseException {
    public ImageCodeInvalidException(String message) {
        super(message, CommonConstants.EX_IMAGECODE_CODE);
    }
}
