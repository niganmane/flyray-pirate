package com.github.wxiaoqi.security.common.qr.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("商品请求参数")
public class QrGoodsRequestParam {
	
	@ApiModelProperty("编号")
	private String id;

	@ApiModelProperty("平台编号")
	private String platformId;
	
	@ApiModelProperty("商品编号")
	private String goodsId;
	
	@ApiModelProperty("商户号")
	private String merchantId;
	
	@ApiModelProperty("商品名称")
	private String goodsName;
	
	@ApiModelProperty("商品价格")
	private String goodsPrice;
	
	@ApiModelProperty("商品图片地址")
	private String goodsPic;
	
	@ApiModelProperty("商品属性")
	private String attribute;
	
	@ApiModelProperty("扫描次数")
	private String scanCount;
	
	@ApiModelProperty("付费次数")
	private String payCount;
	
	@ApiModelProperty("运营人员id")
	private String operatorId;
	
	@ApiModelProperty("密码起始位")
	private String pwdStartBit;
	
	@ApiModelProperty("密码库")
	private String pwdLibrary;

}
