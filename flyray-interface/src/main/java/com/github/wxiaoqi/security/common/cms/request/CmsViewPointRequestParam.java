package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序观点参数")
public class CmsViewPointRequestParam extends BaseParam {
	@ApiModelProperty("序号")
	private String id;
	@ApiModelProperty("文字内容")
	private String pointText;
	@ApiModelProperty("图片路径")
	private String pointImg;
	@ApiModelProperty("发表地点")
	private String pointAddress;
	@ApiModelProperty("朋友姓名")
	private String saleName;
	@ApiModelProperty("朋友性别")
	private String saleSex;
	@ApiModelProperty("观点类型")
	private String viewType;
	@ApiModelProperty("是否匿名")	
	private String isPrivate;
	@ApiModelProperty("图片路径")	
	private String imgFile;
	@ApiModelProperty("图片名称")	
	private String imgFileName;
	@ApiModelProperty("昵称")	
	private String nickName;
	@ApiModelProperty("头像")	
	private String avatarUrl;
	
}
