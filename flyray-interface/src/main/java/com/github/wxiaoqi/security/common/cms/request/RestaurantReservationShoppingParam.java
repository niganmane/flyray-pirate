package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("预订外卖购物车信息参数")
public class RestaurantReservationShoppingParam extends BaseParam{
	
	@NotNull(message="用户账号不能为空")
	@ApiModelProperty("用户账号")
	private String perId;
	
	@NotNull(message="业务类型不能为空")
	@ApiModelProperty("业务类型")
	private String type;

}
