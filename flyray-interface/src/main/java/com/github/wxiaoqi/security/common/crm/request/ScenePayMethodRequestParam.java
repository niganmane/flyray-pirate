package com.github.wxiaoqi.security.common.crm.request;


import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 平台/商户支付方式
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询平台 商户支付方式参数")
public class ScenePayMethodRequestParam {
	
	@ApiModelProperty(value = "序号")
	private String id;
	
	@NotNull(message="场景编号不能为空")
	@ApiModelProperty(value = "场景编号")
	private String sceneCode;

	@NotNull(message="支付方式编号")
	@ApiModelProperty(value = "支付方式编号")
    private String methodCode;
	
	@NotNull(message="支付方式不能为空")
	@ApiModelProperty(value = "支付方式")
    private String method;
	
	
	
}
