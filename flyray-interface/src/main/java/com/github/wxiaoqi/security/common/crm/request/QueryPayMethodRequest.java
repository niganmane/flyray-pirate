package com.github.wxiaoqi.security.common.crm.request;


import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 平台/商户支付方式
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询平台 商户支付方式参数")
public class QueryPayMethodRequest {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	@ApiModelProperty(value = "商户号")
    private String merchantId;
	
	@NotNull(message="场景编号不能为空")
	@ApiModelProperty(value = "场景编号")
    private String sceneCode;
	
	@NotNull(message="渠道不能为空")
	@ApiModelProperty(value = "渠道01APP 02小程序 03PC")
    private String channel;
	
}
