package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("参团")
public class FightGroupJoinParam extends BaseParam{
	
	@NotNull(message="用户账号不能为空")
	@ApiModelProperty("用户账号")
	private String customerId;
	
	@NotNull(message="团id不能为空")
	@ApiModelProperty("团id")
	private String groupId;
	
	@NotNull(message="姓名不能为空")
	@ApiModelProperty("姓名")
	private String name;
	
	@NotNull(message="头像不能为空")
	@ApiModelProperty("头像")
	private String userHeadPortrait;

}
