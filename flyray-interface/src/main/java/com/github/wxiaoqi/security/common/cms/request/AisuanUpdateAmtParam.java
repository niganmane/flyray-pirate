package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("更新预算金额")
public class AisuanUpdateAmtParam extends BaseParam{
	
	@NotNull
	@ApiModelProperty("id")
	private Integer id;
	
	@NotNull
	@ApiModelProperty("amt")
	private String amt;
	
}
