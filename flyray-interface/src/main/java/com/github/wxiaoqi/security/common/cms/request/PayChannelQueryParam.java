package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序社区查询参数")
public class PayChannelQueryParam {
	
	@ApiModelProperty("当前页")
	private Integer page;
	@ApiModelProperty("数据条数")
	private Integer limit;
	
	@ApiModelProperty("支付通道编号")
	private String payChannelNo;
	
	@ApiModelProperty("支付通道名称")
	private String payChannelName;
}
