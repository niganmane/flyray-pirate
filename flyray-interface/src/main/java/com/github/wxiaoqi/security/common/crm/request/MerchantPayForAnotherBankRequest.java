package com.github.wxiaoqi.security.common.crm.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "商户提现银行卡信息查询参数")
public class MerchantPayForAnotherBankRequest {
	
	@ApiModelProperty(value = "序号")
    private String id;
	
	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;
	
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	@ApiModelProperty(value = "商户号")
	private String merchantId;
	
}
