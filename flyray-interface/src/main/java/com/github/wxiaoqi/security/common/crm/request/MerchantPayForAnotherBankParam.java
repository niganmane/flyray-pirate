package com.github.wxiaoqi.security.common.crm.request;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "商户提现银行卡信息请求参数")
public class MerchantPayForAnotherBankParam {
	
	@ApiModelProperty(value = "序号")
    private String id;
	
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	@ApiModelProperty(value = "商户号")
	private String merchantId;
	
	@ApiModelProperty(value = "银行名称")
    private String bankName;
	
	@ApiModelProperty(value = "银行账户名")
    private String bankAccountName;
	
    @ApiModelProperty(value = "银行卡号")
    private String bankNo;
	
	
}
