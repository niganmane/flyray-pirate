package com.github.wxiaoqi.security.common.qr.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("商品属性请求参数")
public class QrGoodsAttributeRequestParam {
	
	@ApiModelProperty("序号")
	private String id;

	@ApiModelProperty("编号")
	private String code;
	
	@ApiModelProperty("描述")
	private String desc;
	
}
