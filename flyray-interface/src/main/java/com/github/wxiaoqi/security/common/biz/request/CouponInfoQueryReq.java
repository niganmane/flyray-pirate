package com.github.wxiaoqi.security.common.biz.request;
import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.admin.pay.request.BaseRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * 订单提交时红包校验请求参数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("订单提交时红包校验请求参数")
public class CouponInfoQueryReq extends BaseRequest implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1545469848195883291L;
	
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	// 红包ID
	@NotNull(message="红包ID不能为空")
	private String couponId;
	
	// 红包流水号
	@NotNull(message="红包流水号不能为空")
	private String couponSerialNo;
	
//	// 合作平台编号 
//	@NotNull(message="合作平台编号不能为空")
//	private String acceptBizNo;

}
