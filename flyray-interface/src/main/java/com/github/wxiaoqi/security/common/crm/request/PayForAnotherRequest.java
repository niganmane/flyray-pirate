package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 代付
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "代付请求参数")
public class PayForAnotherRequest {

	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	@NotNull(message="商户编号不能为空")
	@ApiModelProperty(value = "商户编号")
	private String merchantId;
	
	@NotNull(message="代付订单号不能为空")
	@ApiModelProperty(value = "代付订单号")
	private String orderId;

}