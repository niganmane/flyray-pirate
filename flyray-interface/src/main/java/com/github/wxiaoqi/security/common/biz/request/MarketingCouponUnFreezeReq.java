package com.github.wxiaoqi.security.common.biz.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.admin.pay.request.BaseRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 红包解冻请求
 * 
 * @author hexufeng 2018年1月29日 下午3:54:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("红包解冻请求")
public class MarketingCouponUnFreezeReq extends BaseRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	// /**
	// * 合作平台编号
	// */
	// @NotNull(message="合作平台编号不能为空")
	// private String acceptBizNo;
	//
	// /**
	// * 商户号
	// */
	// @NotNull(message="商户号不能为空")
	// private String merNo;
	//
	// /**
	// * 商户会员号
	// */
	// @NotNull(message="商户会员号不能为空")
	// private String merCustNo;
	//
	// /**
	// * 平台编号
	// */
	// @NotNull(message="平台编号不能为空")
	// private String unionId;

	// 用户编号
	@ApiModelProperty(value = "用户编号")
	private String customerId;

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	// 个人客户编号
	@NotNull(message = "个人客户编号不能为空")
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;

	// 商户客户编号
	@NotNull(message = "商户客户编号不能为空")
	@ApiModelProperty(value = "商户客户编号")
	private String merchantId;

	/**
	 * 红包流水号
	 */
	@NotNull(message = "红包流水号")
	@ApiModelProperty(value = "用户编号")
	private String couponSerialNo;

	/**
	 * 红包ID
	 */
	@NotNull(message = "红包ID")
	@ApiModelProperty(value = "用户编号")
	private String couponId;

	/**
	 * 使用场景ID
	 */
	@NotNull(message = "使用场景ID")
	@ApiModelProperty(value = "用户编号")
	private String sceneId;

}
