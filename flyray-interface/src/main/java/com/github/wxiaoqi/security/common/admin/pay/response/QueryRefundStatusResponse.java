package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;

/** 
* @author: bolei
* @date：2017年2月23日 下午12:45:05 
* @description：类说明 
*/

public class QueryRefundStatusResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String refundStatus;//退款状态
	 
	private String refundFee;//退款金额
 
	private String outTradeNo;//商户订单号

	public String getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

	public String getRefundFee() {
		return refundFee;
	}

	public void setRefundFee(String refundFee) {
		this.refundFee = refundFee;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

}
