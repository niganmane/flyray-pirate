package com.github.wxiaoqi.security.common.admin.pay.request;

import java.io.Serializable;

/**
 * 退款申请请求
 * @author hexufeng
 *
 */
public class RefundApplyRequest extends BaseRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
     * 原支付订单号
     */
    private String payOrderNo;
    /**
	 * 平台编号
	 */
	private String platformId;
	/**
	 * 退款订单号
	 */
	private String refundOrderNo;
	/**
	 * 退款金额
	 */
	private String refundAmt;
	/**
	 * 退款原因
	 */
	private String refundReason;
	public String getPayOrderNo() {
		return payOrderNo;
	}
	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	public String getRefundOrderNo() {
		return refundOrderNo;
	}
	public void setRefundOrderNo(String refundOrderNo) {
		this.refundOrderNo = refundOrderNo;
	}
	public String getRefundAmt() {
		return refundAmt;
	}
	public void setRefundAmt(String refundAmt) {
		this.refundAmt = refundAmt;
	}
	public String getRefundReason() {
		return refundReason;
	}
	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

}
