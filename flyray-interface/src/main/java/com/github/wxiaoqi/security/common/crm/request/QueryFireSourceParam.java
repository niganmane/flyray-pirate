package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 微信小程序火源余额查询
 * @author he
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "微信小程序火源余额查询请求参数")
public class QueryFireSourceParam {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	@NotNull(message="个人客户编号不能为空")
	@ApiModelProperty(value = "个人客户编号")
    private String personalId;
	
	@NotNull(message="用户编号不能为空")
	@ApiModelProperty(value = "用户编号")
    private String customerId;

}
