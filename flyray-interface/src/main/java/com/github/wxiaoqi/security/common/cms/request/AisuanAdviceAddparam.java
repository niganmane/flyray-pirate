package com.github.wxiaoqi.security.common.cms.request;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-09-10 10:58:58
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("添加建议")
public class AisuanAdviceAddparam  extends BaseParam {
	private static final long serialVersionUID = 1L;
	
	    //主键
	@ApiModelProperty("主键")
    private Long id;
	
	    //提交的建议内容
	@NotNull(message="提交的建议内容不能为空")
	@ApiModelProperty("提交的建议内容")
    private String advice;
	
	//会员编号
	@NotNull(message="会员编号不能为空")
	@ApiModelProperty("会员编号")
	private Long customerId;
	
}
