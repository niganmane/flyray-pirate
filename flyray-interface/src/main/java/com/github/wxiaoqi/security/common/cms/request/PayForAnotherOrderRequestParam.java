package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("代付订单请求参数")
public class PayForAnotherOrderRequestParam {
	
	@ApiModelProperty("序号")
	private String id;

	@ApiModelProperty("代付订单号")
	private String orderId;

	@ApiModelProperty("平台编号")
	private String platformId;
}
