package com.github.wxiaoqi.security.common.msg;

/**
 * 账户类型
 */
public enum AccountType {

//	ACC002：红包账户，ACC003：积分账户，ACC004：手续费账户，ACC005：已结算账户，ACC006：交易金额账户，ACC007：余额冻结账户,ACC008：平台管理费账户，ACC009：平台服务费账户
	ACC_BALANCE("ACC001","余额账户"),
	ACC_RED_ENVELOPE("ACC002","红包账户"),
	ACC_INTEGRAL("ACC003","积分账户"),
    ACC_FEE("ACC004","手续费账户"),
    ACC_SETTLED("ACC005","已结算账户"),
    ACC_TRANS("ACC006","交易金额账户"),
    ACC_FREEZE("ACC007","余额冻结账户"),
    ACC_PLATFORM_SERVICE("ACC008","平台管理费账户"),
    ACC_PLATFORM_MANAGEMENT("ACC009","平台服务费账户");

    private String code;
    private String desc;

    private AccountType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static AccountType getSmsType(String code) {
        for (AccountType o : AccountType.values()) {
            if (o.getCode().equals(code)) {
                return o;
            }
        }
        return null;
    }
}
