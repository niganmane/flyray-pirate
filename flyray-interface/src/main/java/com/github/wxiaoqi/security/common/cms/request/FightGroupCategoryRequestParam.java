package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("拼团商品参数")
public class FightGroupCategoryRequestParam extends BaseParam{
	
	@ApiModelProperty("序号")
	private String id;
	
	@ApiModelProperty("团类目id")
	private String groupsId;
	
	@ApiModelProperty("商品id")
	private String goodsId;
	
	@ApiModelProperty("团名称")
	private String groupName;
	
	@ApiModelProperty("金额")
	private String txAmt;
	
	@ApiModelProperty("限制人数")
	private String peopleNum;
	

}
