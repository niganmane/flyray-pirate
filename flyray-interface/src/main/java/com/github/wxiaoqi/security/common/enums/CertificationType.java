package com.github.wxiaoqi.security.common.enums;

/** 
* @author: bolei
* @date：2017年11月2日 下午12:26:50
* @description：用户实名认证类型
*/

public enum CertificationType {

	FOURFACTORS_CERTIFY("00","四要素实名认证"),
	TAHUA_THIRD_CERTIFY("01","泰华电子实名认证");
    private String code;
    private String desc;
    
    private CertificationType (String code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static CertificationType getCommentModuleNo(String code){
        for(CertificationType o : CertificationType.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
