package com.github.wxiaoqi.security.common.pay.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 支付
 * @author he
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("支付")
public class DoPayParam {

	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;

	@ApiModelProperty("openId")
	private String openId;

	@ApiModelProperty("支付通道编号")
	private String payChannelNo;

	@ApiModelProperty("支付公司编号")
	private String payCompanyNo;
	
	@NotNull(message="支付方式不能为空")
	@ApiModelProperty("支付方式")
	private String payType;
	
	@NotNull(message="支付订单号不能为空")
	@ApiModelProperty("支付订单号")
	private String payOrderNo;
	
	@NotNull(message="账户类型不能为空")
	@ApiModelProperty("账户类型")
	private String accountType;
	
	@ApiModelProperty("交易密码")
	private String password;

}
