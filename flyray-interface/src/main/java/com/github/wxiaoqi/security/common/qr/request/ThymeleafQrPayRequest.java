package com.github.wxiaoqi.security.common.qr.request;

/**
 * 商品二维码跳支付
 * @author he
 */
public class ThymeleafQrPayRequest {
	
	/**
	 * 支付方式
	 */
	private String payType;
	
	/**
	 * 支付订单号
	 */
	private String payOrderNo;
	
	/**
	 * 二维码
	 */
	private String sendGet;

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getPayOrderNo() {
		return payOrderNo;
	}

	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}

	public String getSendGet() {
		return sendGet;
	}

	public void setSendGet(String sendGet) {
		this.sendGet = sendGet;
	}

}
