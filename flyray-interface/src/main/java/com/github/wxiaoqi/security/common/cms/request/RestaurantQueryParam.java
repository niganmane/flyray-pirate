package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("餐饮基础信息参数")
public class RestaurantQueryParam extends BaseParam{
	
	
	@ApiModelProperty("店铺名称")
	private String name;
	
	@ApiModelProperty("餐桌名称")
	private String tableName;
	
	@ApiModelProperty("页数")
	private Integer page;
	
	@ApiModelProperty("条数")
	private Integer limit;

}
