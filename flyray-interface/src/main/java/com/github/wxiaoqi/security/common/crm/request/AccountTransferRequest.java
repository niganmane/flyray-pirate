package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("个人或者商户转账接口参数")
public class AccountTransferRequest {
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message = "转账金额不能为空")
	@ApiModelProperty("转账金额")
	private String transferAmt;
	
	@NotNull(message = "转账类型不能为空")
	@ApiModelProperty("转账类型")
	private String transferType;

	@NotNull(message = "入账账号不能为空")
	@ApiModelProperty("入账账号")
	private String intoAccountId;
	
	@NotNull(message = "入账账户类型不能为空")
	@ApiModelProperty("入账账户类型")
	private String intoAccountType;
	
	@NotNull(message = "入账客户类型不能为空")
	@ApiModelProperty("入账客户类型")
	private String intoCustomerType;
	
	@NotNull(message = "出账账号不能为空")
	@ApiModelProperty("出账账号")
	private String outAccountId;
	
	@NotNull(message = "出账账户类型不能为空")
	@ApiModelProperty("出账账户类型")
	private String outAccountType;
	
	@NotNull(message = "出账客户类型不能为空")
	@ApiModelProperty("出账客户类型")
	private String outCustomerType;
	
	@NotNull(message = "交易类型不能为空")
	@ApiModelProperty("交易类型")
	private String tradeType;
	
	@NotNull(message = "订单号不能为空")
	@ApiModelProperty("订单号")
	private String orderNo;
	
	
}
