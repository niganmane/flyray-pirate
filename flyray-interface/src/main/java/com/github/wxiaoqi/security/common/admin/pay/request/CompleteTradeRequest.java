package com.github.wxiaoqi.security.common.admin.pay.request;

import java.io.Serializable;

/**
 * 回调商户请求
 * @author hexufeng
 *
 */
public class CompleteTradeRequest extends BaseRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/**
	 * 平台编号
	 */
	private String platformId;
	
	/**
	 * 订单号
	 */
	private String orderNo;
	
	/**
	 * 交易类型1支付 2退款 3代付
	 */
	private String tradeType;
	
	/**
	 * 原支付订单号
	 */
	private String payOrderNo;
	
	/**
	 * 交易状态
	 */
	private String txStatus;
	
	/**
	 * 金额
	 */
	private String amount;

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getPayOrderNo() {
		return payOrderNo;
	}

	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}

	public String getTxStatus() {
		return txStatus;
	}

	public void setTxStatus(String txStatus) {
		this.txStatus = txStatus;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
}
