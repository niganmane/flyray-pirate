package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("订单查询参数")
public class PayOrderQueryParam {

	@ApiModelProperty("当前页")
	private Integer page;
	@ApiModelProperty("数据条数")
	private Integer limit;
	@ApiModelProperty("商户号")
	private String merchantId;
	@ApiModelProperty("平台编号")
	private String platformId;
	@ApiModelProperty("订单号")
	private String payOrderNo;
	@ApiModelProperty("代付订单号")
	private String orderId;
	@ApiModelProperty("支付状态")
	private String orderStatus;
	@ApiModelProperty("代付状态")
	private String txStatus;
}
