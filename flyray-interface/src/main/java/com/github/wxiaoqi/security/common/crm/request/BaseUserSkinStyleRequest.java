package com.github.wxiaoqi.security.common.crm.request;

import java.util.Date;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 用户皮肤样式
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-08-31 09:19:23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "用户皮肤样式")
public class BaseUserSkinStyleRequest {
	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;
	
	    //序号
	@ApiModelProperty(value = "序号")
    private Integer id;
	
	    //平台编号
    @NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	    //用户ID
    @NotNull(message="用户ID不能为空")
	@ApiModelProperty(value = "用户ID")
    private Long userId;
	
	    //显示字段
	@ApiModelProperty(value = "显示字段")
    private String skinStyle;
	
	    //创建时间
	@ApiModelProperty(value = "创建时间")
    private Date createTime;
	
	    //更新时间
	@ApiModelProperty(value = "更新时间")
    private Date updateTime;
	
}
