package com.github.wxiaoqi.security.common.enums;

/** 
* 交易类型（支付:01，退款:02，提现:03，充值:04）
*/

public enum TradeTypeEnums {

	pay("01","支付"),
	refound("02","退款"),
	withdraw("03","提现"),
	recharge("04","充值");
    private String code;
    private String desc;
    
    private TradeTypeEnums (String code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static TradeTypeEnums getCommentModuleNo(String code){
        for(TradeTypeEnums o : TradeTypeEnums.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
