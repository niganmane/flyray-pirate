package com.github.wxiaoqi.security.common.biz.request;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.admin.pay.request.BaseRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 红包积分退款请求参数
 * 
 * @author Administrator
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("红包积分退款请求参数")
public class CouponIntegralRefundReq extends BaseRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	// 订单号
	@NotNull(message = "订单号不能为空")
	@ApiModelProperty(value = "订单号")
	private String merOrderNo;

	// @NotNull(message="合作平台编号不能为空")
	// private String acceptBizNo;

}
