package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序发表观点参数")
public class CmsSaveViewPointParam extends BaseParam {
	@NotNull(message="用户信息编号不能为空")
	@ApiModelProperty("用户信息编号")
	private String customerId;
	@ApiModelProperty("文字内容")
	private String pointText;
	@ApiModelProperty("图片路径")
	private String pointImg;
	@ApiModelProperty("发表地点")
	private String pointAddress;
	@ApiModelProperty("朋友姓名")
	private String saleName;
	@ApiModelProperty("朋友性别")
	private String saleSex;
	@ApiModelProperty("观点类型")
	private String viewType;
	@ApiModelProperty("是否匿名")	
	private String isPrivate;
	@ApiModelProperty("图片路径")	
	private String imgFile;
	@ApiModelProperty("图片名称")	
	private String imgFileName;
	@ApiModelProperty("昵称")	
	private String nickName;
	@ApiModelProperty("头像")	
	private String avatarUrl;
	
}
