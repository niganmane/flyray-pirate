package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("拼团商品参数")
public class FightGroupPicRequestParam extends BaseParam{
	
	@ApiModelProperty("序号")
	private String id;
	@ApiModelProperty("商品编号")
	private String goodsId;
	@ApiModelProperty("商品图片")
	private String pictureUrl;

}
