package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("拼团商品参数")
public class FightGroupRequestParam extends BaseParam{
	
	@ApiModelProperty("商品名称")
	private String goodsName;
	@ApiModelProperty("序号")
	private String id;
	@ApiModelProperty("商品编号")
	private String goodsId;
	@ApiModelProperty("说明")
	private String goodsDescription;
	@ApiModelProperty("商品图片")
	private String goodsImg;
	@ApiModelProperty("原价")
	private String originalPrice;
	@ApiModelProperty("折扣价")
	private String discountPrice;
	@ApiModelProperty("有效时间说明")
	private String effectiveTimeDescription;
	@ApiModelProperty("预约提醒")
	private String appointmentReminder;
	@ApiModelProperty("温馨提示")
	private String tips;
	@ApiModelProperty("拼团说明")
	private String groupDescription;

}
