package com.github.wxiaoqi.security.common.enums;

/** 
* @author: bolei
* @date：2017年11月2日 下午12:26:50
* @description：用户实名认证状态
*/

public enum CertificationStatus {

	NO_CERTIFY("00","未认证"),
	NOMORE_CERTIFY("01","无须认证"),
	SUCCESS_CERTIFY("02","认证成功	"),
	FAIL_CERTIFY("03","认证失败");
    private String code;
    private String desc;
    
    private CertificationStatus (String code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static CertificationStatus getCommentModuleNo(String code){
        for(CertificationStatus o : CertificationStatus.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
