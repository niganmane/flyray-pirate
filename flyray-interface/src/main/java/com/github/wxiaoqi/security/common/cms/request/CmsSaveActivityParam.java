package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("添加活动参数")
public class CmsSaveActivityParam extends BaseParam {
	
	@ApiModelProperty("活动编号")
	private String id;
	@ApiModelProperty("活动名称")
	private String activityName;
	@ApiModelProperty("活动logo")
	private String activityLogo;
	@ApiModelProperty("活动摘要")
	private String activityDes;
	@ApiModelProperty("活动内容")
	private String activityContent;
	@ApiModelProperty("开始时间")
	private String activityStartTimeStr;
	@ApiModelProperty("结束时间")
	private String activityEndTimeStr;
	@ApiModelProperty("活动地点")
	private String activityAddr;
	@ApiModelProperty("封面")
	private String coverImg;
	@ApiModelProperty("二维码")
	private String qrImg;
	@ApiModelProperty("发布者姓名")
	private String publisherName;
	@ApiModelProperty("联系方式")
	private Integer publisherContactway;
	@ApiModelProperty("联系号码")
	private String publisherContactvalue;
	@ApiModelProperty("活动类型名称")
	private String acttypeName;
	@ApiModelProperty("活动类型")
	private Integer acttype;
	@ApiModelProperty("限制人数")
	private Integer peopleNum;
	@ApiModelProperty("状态")
	private Integer initiationStatus;
	
}
