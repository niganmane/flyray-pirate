package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("根据预测类型查询收费")
public class AisuanAmtQueryOneParam extends BaseParam{
	
	@NotNull
	@ApiModelProperty("预测类型")
	private Integer type;
	
}
