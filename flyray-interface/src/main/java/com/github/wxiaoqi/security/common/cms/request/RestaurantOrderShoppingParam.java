package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("点餐购物车信息参数")
public class RestaurantOrderShoppingParam extends BaseParam{

	@NotNull(message="用户编号不能为空")
	@ApiModelProperty("用户编号")
	private String customerId;
	
	@ApiModelProperty("购物车商品id")
	private String id;
	
	@ApiModelProperty("餐桌id")
	private String tableId;
	
	@ApiModelProperty("菜品id")
	private String dishesId;

}
