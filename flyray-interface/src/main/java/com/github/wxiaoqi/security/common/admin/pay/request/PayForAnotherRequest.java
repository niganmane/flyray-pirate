package com.github.wxiaoqi.security.common.admin.pay.request;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 代付请求
 * @author hexufeng
 *
 */
public class PayForAnotherRequest extends BaseRequest implements Serializable{

	private static final long serialVersionUID = 1L;

	//传参
	//代付订单号
	private String orderId;

	//平台编号
	private String platformId;
	
	// 支付通道编号
	private String payChannelNo;
	
	//支付公司编号
	private String payCompanyNo;


	//内部参数
	//外部订单号
	private String outOrderNo;

	//代付金额
	private BigDecimal amount;

	//银行编码
	private String bankCode;

	//银行卡号密文
	private String bankAccountNo;

	//银行账户名
	private String bankAccountName;

	//银行联行号
	private String bankUnionCode;

	//商户号
	private String merchantId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getOutOrderNo() {
		return outOrderNo;
	}

	public void setOutOrderNo(String outOrderNo) {
		this.outOrderNo = outOrderNo;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankAccountNo() {
		return bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public String getBankAccountName() {
		return bankAccountName;
	}

	public void setBankAccountName(String bankAccountName) {
		this.bankAccountName = bankAccountName;
	}

	public String getBankUnionCode() {
		return bankUnionCode;
	}

	public void setBankUnionCode(String bankUnionCode) {
		this.bankUnionCode = bankUnionCode;
	}

	public String getPayChannelNo() {
		return payChannelNo;
	}

	public void setPayChannelNo(String payChannelNo) {
		this.payChannelNo = payChannelNo;
	}

	public String getPayCompanyNo() {
		return payCompanyNo;
	}

	public void setPayCompanyNo(String payCompanyNo) {
		this.payCompanyNo = payCompanyNo;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

}
