package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 会员登录请求报文
 * @author centerroot
 * @time 创建时间:2018年9月8日下午4:39:09
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("会员登录请求报文")
public class LoginRequest{

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	/**
	 * 手机号
	 */
	@NotNull(message = "手机号不能为空")
	@ApiModelProperty("手机号")
	private String phone;
	/**
	 * 登录密码
	 */
	@NotNull(message = "登录密码不能为空")
	@ApiModelProperty("登录密码")
	private String password;
	/**
	 * 角色类型
	 * 01:个人  02：商户
	 */
	@NotNull(message = "角色类型不能为空")
	@ApiModelProperty("角色类型")
	private String roleType;
	/**
	 * 图形验证码
	 */
	@ApiModelProperty("图形验证码")
	private String imgCode;
	/**
	 * APP版本
	 */
	@ApiModelProperty("APP版本")
	private String appVersion;
	/**
	 * 操作系统版本
	 */
	@ApiModelProperty("操作系统版本")
	private String osVersion;
	
}
