package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("创建团")
public class FightGroupCreatParam extends BaseParam{
	
	@NotNull(message="用户账号不能为空")
	@ApiModelProperty("用户账号")
	private String customerId;
	
	@NotNull(message="团类目id不能为空")
	@ApiModelProperty("团类目id")
	private String groupsId;
	
	@NotNull(message="商品编号不能为空")
	@ApiModelProperty("商品编号")
	private String goodsId;
	
	@NotNull(message="姓名不能为空")
	@ApiModelProperty("姓名")
	private String name;
	
	@NotNull(message="头像不能为空")
	@ApiModelProperty("头像")
	private String userHeadPortrait;
	
	@NotNull(message="支付订单号不能为空")
	@ApiModelProperty("支付订单号")
	private String payOrderNo;

}
