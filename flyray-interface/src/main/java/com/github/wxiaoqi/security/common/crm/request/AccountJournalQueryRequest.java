package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("账户流水查询接口参数")
public class AccountJournalQueryRequest {
	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@ApiModelProperty("商户编号")
	private String merchantId;
	
	@ApiModelProperty("商户类型编号")
	private String merchantType;
	
	@ApiModelProperty("个人客户编号")
	private String personalId;	
	
	@ApiModelProperty("账户类型")
	private String accountType;
	
	@NotNull(message = "客户类型不能为空")
	@ApiModelProperty("客户类型")
	private String customerType;
	
}
