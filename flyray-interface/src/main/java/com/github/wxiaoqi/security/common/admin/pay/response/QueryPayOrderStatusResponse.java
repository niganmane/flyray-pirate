package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;

/**
 * 支付订单查询响应
 * @author hexufeng
 *
 */
public class QueryPayOrderStatusResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 支付状态
	 */
	private String payStatus;

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

}
