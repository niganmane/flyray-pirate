package com.github.wxiaoqi.security.common.crm.request;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.github.wxiaoqi.security.common.entity.BaseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 平台扩展信息
 * 
 * @author 
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "平台扩展信息使用接口")
public class PlatformBaseExtendRequest {
	
	@ApiModelProperty(value = "序号")
    private Integer id;
	
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	@ApiModelProperty(value = "企业名称")
    private String companyName;
	
	@ApiModelProperty(value = "经营范围")
    private String businessScope;
	
	    //工商注册号
	@ApiModelProperty(value = "工商注册号")
    private String businessNo;
	
	@ApiModelProperty(value = "法人姓名")
    private String legalPersonName;
	
	    //法人证件类型  0-身份证；1-户口本；2-护照；3-军官证；4-士兵证；5-港澳居民往来内地通行证；6-台湾通报往来内地通行证；7-临时身份证；8-外国人居留证；9-警官证；x-其他证件 
	@ApiModelProperty(value = "法人证件类型")
	private String legalPersonCredType;
	
	    //法人证件号码
	@ApiModelProperty(value = "法人证件号码")
    private String legalPersonCredNo;
	
	    //营业执照
	@ApiModelProperty(value = "营业执照")
    private String businessLicence;
	
	@ApiModelProperty(value = "联系电话")
    private String phone;
	
	@ApiModelProperty(value = "企业座机")
    private String mobile;
	
	@ApiModelProperty(value = "企业传真")
    private String fax;
	
	@ApiModelProperty(value = "企业网址")
    private String httpAddress;
	
	@ApiModelProperty(value = "注册资金")
    private BigDecimal registeredCapital;
	
	    //
	@ApiModelProperty(value = "企业地址")
    private String companyAddress;
	
	
    //
	@ApiModelProperty(value = "最后操作人编号")
	private Integer operatorId;
	
	@ApiModelProperty(value = "最后操作人名称")
	private String operatorName;
	
	
	@ApiModelProperty(value = "用戶token")
    private String token;
    
}
