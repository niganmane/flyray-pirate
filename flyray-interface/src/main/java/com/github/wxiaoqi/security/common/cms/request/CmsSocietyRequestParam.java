package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序社群参数")
public class CmsSocietyRequestParam extends BaseParam {
	@ApiModelProperty("编号")
	private String id;
	@ApiModelProperty("名称")
	private String societyName;
	@ApiModelProperty("二维码")
	private String societyQr;
	@ApiModelProperty("介绍内容")
	private String societyContent;
}
