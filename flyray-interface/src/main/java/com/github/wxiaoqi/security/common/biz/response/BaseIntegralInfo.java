package com.github.wxiaoqi.security.common.biz.response;

/**
 * 基础红包/积分查询返回值-积分基础信息
 * 
 */
public class BaseIntegralInfo {
	// 积分ID
	private String integralId;
	// 积分数量
	private String integralAmt;

	public String getIntegralId() {
		return integralId;
	}

	public void setIntegralId(String integralId) {
		this.integralId = integralId;
	}

	public String getIntegralAmt() {
		return integralAmt;
	}

	public void setIntegralAmt(String integralAmt) {
		this.integralAmt = integralAmt;
	}

}
