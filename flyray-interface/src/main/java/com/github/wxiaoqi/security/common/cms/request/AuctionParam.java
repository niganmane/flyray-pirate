package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("发起竞拍")
public class AuctionParam extends BaseParam{
	
	@NotNull(message="用户账号不能为空")
	@ApiModelProperty("用户账号")
	private String customerId;
	
	@NotNull(message="用户名不能为空")
	@ApiModelProperty("用户名编号")
	private String name;
	
	@NotNull(message="头像不能为空")
	@ApiModelProperty("头像")
	private String userHeadPortrait;
	
	@NotNull(message="竞拍价不能为空")
	@ApiModelProperty("竞拍价")
	private String price;
	
	@NotNull(message="商品编号不能为空")
	@ApiModelProperty("商品编号")
	private String goodsId;

}
