package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("发起评论")
public class RestaurantToAppraisalParam extends BaseParam{
	
	@NotNull(message="用户编号不能为空")
	@ApiModelProperty("用户编号")
	private String customerId;
	
	@NotNull(message="评论信息不能为空")
	@ApiModelProperty("评论信息")
	private String appraisalInfo;
	
	@NotNull(message="评论等级不能为空")
	@ApiModelProperty("评论等级")
	private String appraisalGrade;
	
	@NotNull(message="昵称不能为空")
	@ApiModelProperty("昵称")
	private String name;
	
	@NotNull(message="头像不能为空")
	@ApiModelProperty("头像")
	private String userHeadPortrait;
	
	@NotNull(message="购物车id不能为空")
	@ApiModelProperty("购物车id")
	private String id;

}
