package com.github.wxiaoqi.security.common.qr.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("获取微信用户信息")
public class GetWechatUserParam {
	
	@ApiModelProperty("微信code")
	private String code;
	
	@ApiModelProperty("商品ID")
	private String goodsId;
	
	@ApiModelProperty("商品图片")
	private String goodsPic;
	
	@ApiModelProperty("商品金额")
	private String goodsPrice;
	
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@ApiModelProperty("商户编号")
	private String merchantId;

}
