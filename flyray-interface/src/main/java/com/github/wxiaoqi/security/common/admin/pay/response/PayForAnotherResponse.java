package com.github.wxiaoqi.security.common.admin.pay.response;

import java.io.Serializable;

/**
 * 代付响应
 * @author hexufeng
 *
 */
public class PayForAnotherResponse extends BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 代付状态 0000接收成功 0001订单已存在 0002接收失败 0003账户余额不足 0004账户状态异常 0005订单不存在 0006打款超过限额
	 */
	private String status;

	/**
	* 金额
	*/
	private String amount;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

}
