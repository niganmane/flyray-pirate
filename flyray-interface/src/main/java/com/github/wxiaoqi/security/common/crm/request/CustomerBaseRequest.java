package com.github.wxiaoqi.security.common.crm.request;

import java.util.Date;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 客户基础信息
 * @author centerroot
 * @time 创建时间:2018年7月17日上午11:51:06
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "客户基础信息")
public class CustomerBaseRequest {
	
	    //
	@ApiModelProperty(value = "序号")
    private Integer id;
	
	    //用户编号
	@ApiModelProperty(value = "用户编号")
    private String customerId;
	
    @NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	    //客户类型    CUST00：平台  （ CUST01：商户  CUST02：用户）：普通客户默认为空
	@ApiModelProperty(value = "客户类型")
    private String customerType;
	
	    //支付密码
	@ApiModelProperty(value = "支付密码")
    private String payPassword;
	
	    //登录密码错误次数
	@ApiModelProperty(value = "登录密码错误次数")
    private Integer passwordErrorCount;
	
	    //支付密码错误次数
	@ApiModelProperty(value = "支付密码错误次数")
    private Integer payPasswordErrorCount;
	
	    //登录密码状态  00：正常   01：未设置   02：锁定
	@ApiModelProperty(value = "登录密码状态")
    private String passwordStatus;
	
	    //支付密码状态    00：正常   01：未设置   02：锁定
	@ApiModelProperty(value = "支付密码状态")
    private String payPasswordStatus;
	
	    //个人客户编号
	@ApiModelProperty(value = "个人客户编号")
    private String personalId;
	
	    //商户客户编号
	@ApiModelProperty(value = "商户客户编号")
    private String merchantId;
	
	    //账户状态 00：正常，01：客户冻结
	@ApiModelProperty(value = "账户状态")
    private String status;
	
	    //认证状态（同步个人客户基础信息表中认证状态）  00：未认证  01：无需认证  02：认证成功  03：认证失败
	@ApiModelProperty(value = "认证状态")
    private String authenticationStatus;
	
	    //注册时间
	@ApiModelProperty(value = "注册时间")
    private Date registerTime;
	
	    //注册IP
	@ApiModelProperty(value = "注册IP")
    private String registerIp;
	
	    //上次登录时间
	@ApiModelProperty(value = "上次登录时间")
    private Date loginTime;
	
	    //上次登录IP
	@ApiModelProperty(value = "上次登录IP")
    private String loginIp;
	
	    //最后登录角色（默认为空）  01：个人客户     02 ：商户客户
	@ApiModelProperty(value = "最后登录角色")
    private String lastLoginRole;
	
	    //创建时间
	@ApiModelProperty(value = "创建时间")
    private Date createTime;
	
	    //更新时间
	@ApiModelProperty(value = "更新时间")
    private Date updateTime;
	
	
	@ApiModelProperty(value = "归属人")
    private int owner;
	
    	//登录密码盐值
	@ApiModelProperty(value = "登录密码盐值")
	private String passwordSalt;
	
		//支付密码盐值
	@ApiModelProperty(value = "支付密码盐值")
	private String payPasswordSalt;
}
