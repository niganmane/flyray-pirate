package com.github.wxiaoqi.security.common.crm.request;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.entity.BaseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 查询平台支持账户
 * @author centerroot
 * @time 创建时间:2018年8月16日上午11:43:37
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询平台支持账户请求参数")
public class PlatformAccoutConfigRequest {

	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;
	
	    //序号
	@ApiModelProperty(value = "序号")
    private Integer id;
	
	    //平台编号
    @NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	    //客户类型    CUST00：平台   CUST01：商户  CUST02：用户
	@ApiModelProperty(value = "客户类型")
    private String customerType;
	
	    //账户类型  ACC001：余额账户，
	@ApiModelProperty(value = "账户类型")
    private String accountType;
	
    //账户别名
	@ApiModelProperty(value = "账户别名")
    private String accountAlias;

}
