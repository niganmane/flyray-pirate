package com.github.wxiaoqi.security.common.enums;

/** 
* 出入账标识
*/

public enum FreezeStateEnums {

	freeze("1","已冻结"),
	feeze_part("2","部分冻结"),
	unfeeze("3","部分冻结");
    private String code;
    private String desc;
    
    private FreezeStateEnums (String code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static FreezeStateEnums getCommentModuleNo(String code){
        for(FreezeStateEnums o : FreezeStateEnums.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
