package com.github.wxiaoqi.security.common.cms.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序参加活动参数")
public class CmsJoinActivityParam {

	@NotNull(message="商户号不能为空")
	@ApiModelProperty("商户号")
	private String merchantId;
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	@ApiModelProperty("真实姓名")
	private String realName;
	@ApiModelProperty("联系方式类型")
	private Integer contactWay;
	@ApiModelProperty("联系号码")
	private String contactValue;
	@ApiModelProperty("活动编号")
	private String activityId;
	@ApiModelProperty("用户编号")
	private String customerId;
}
