package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("小程序参加活动参数")
public class CmsQueryActivityJoinParam extends BaseParam {

	@ApiModelProperty("活动编号")
	private String activityId;
	@ApiModelProperty("真实姓名")
	private String realName;
	@ApiModelProperty("当前页")
	private Integer page;
	@ApiModelProperty("数量")
	private Integer limit;
	
}
