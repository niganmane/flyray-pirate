package com.github.wxiaoqi.security.common.crm.request;

import java.util.Date;

import javax.persistence.Column;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 平台添加
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "平台添加请求参数")
public class PlatformBaseAddRequest {
	
	    //序号
	@ApiModelProperty(value = "序号")
    private Integer id;
	
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
    @ApiModelProperty(value = "平台名称")
    private String platformName;
	
	    //平台简介
    @ApiModelProperty(value = "平台简介")
    private String platformIntroduction;
	
	    //认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
    @ApiModelProperty(value = "认证状态")
    private String authenticationStatus;
	
	    //最后操作人编号
    @ApiModelProperty(value = "最后操作人编号")
    private Integer operatorId;
	
	    //最后操作人名称
    @ApiModelProperty(value = "最后操作人名称")
    private String operatorName;
	
	    //创建时间
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	
	    //更新时间
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
    
    //平台级别  01:一级平台（顶级） 02：二级平台（默认）
    @ApiModelProperty(value = "平台级别 ")
    private String platformLevel;
	
    //平台登录名称
    @ApiModelProperty(value = "平台登录名称 ")
    private String platformLoginName;
    
    @ApiModelProperty(value = "token ")
    private String token;
    
    //平台图片
    @ApiModelProperty(value = "平台logo ")
    private String platformLogoStr;
    
}
