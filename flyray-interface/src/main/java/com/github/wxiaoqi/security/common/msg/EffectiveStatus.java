package com.github.wxiaoqi.security.common.msg;

/**
 * 有效状态
 * @author hexufeng
 * 2018年1月30日 上午10:01:51
 */
public enum EffectiveStatus {
	
	EFFECTIVE("00","有效"),
	INVALID("01","无效");
	
	private String code;
	private String desc;
	
	private EffectiveStatus (String code,String desc){
		this.code = code;
		this.desc = desc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public static EffectiveStatus getEffectiveStatus(String code){
        for(EffectiveStatus o : EffectiveStatus.values()){
            if(o.getCode().equals(code)){
            	return o;
            }
        }
        return null;
    }

}
