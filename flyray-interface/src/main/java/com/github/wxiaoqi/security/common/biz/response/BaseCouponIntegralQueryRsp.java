package com.github.wxiaoqi.security.common.biz.response;

import java.util.List;

import com.github.wxiaoqi.security.common.admin.pay.response.BaseResponse;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/***
 * 基础红包/积分查询返回参数
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("基础红包/积分查询返回参数")
public class BaseCouponIntegralQueryRsp extends BaseResponse{

	/**
	 * 红包列表
	 * 
	 */
	@ApiModelProperty(value = "红包列表")
	private List<BaseCoupInfo> couponList;

	/**
	 * 积分数量
	 * */
	@ApiModelProperty(value = "积分数量")
	private List<BaseIntegralInfo> integralList;

}
