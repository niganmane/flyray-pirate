package com.github.wxiaoqi.security.common.admin.pay.request;

import java.io.Serializable;

/** 
* @author: bolei
* @date：Jun 14, 2017 8:21:16 AM 
* @description：类描述
*/

public class CreateOrderRequest extends BaseRequest implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * 订单号
	 */
	private String payOrderNo;
	
	/**
	 * 平台编号
	 */
	private String platformId;
	
	/**
	 * 客户账号
	 */
	private String customerId;

	/**
	 *  商户账号
	 */
	private String merchantId;
	
	/**
	 *  订单金额
	 */
	private String orderAmt;
	
	/**
	 *  实际交易金额
	 */
	private String payAmt;
	
	/**
	 *  订单描述
	 */
	private String body;
	
	/**
	 *  交易类型 1支付 2充值
	 */
	private String payCode;

	public String getPayOrderNo() {
		return payOrderNo;
	}

	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getOrderAmt() {
		return orderAmt;
	}

	public void setOrderAmt(String orderAmt) {
		this.orderAmt = orderAmt;
	}

	public String getPayAmt() {
		return payAmt;
	}

	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getPayCode() {
		return payCode;
	}

	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

}
