package com.github.wxiaoqi.security.common.pay.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 申请退款
 * @author he
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("申请退款")
public class RefundParam {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;
	
	@NotNull(message="商户账号不能为空")
	@ApiModelProperty("商户账号")
	private String merchantId;
	
	@NotNull(message="用户账号不能为空")
	@ApiModelProperty("用户账号")
	private String customerId;
	
	@NotNull(message="订单号不能为空")
	@ApiModelProperty("订单号")
	private String payOrderNo;
	
	@NotNull(message="退款金额不能为空")
	@ApiModelProperty("退款金额")
	private String refundAmt;
	
	@NotNull(message="场景编号不能为空")
	@ApiModelProperty("场景编号")
	private String scenesCode;
	
}
