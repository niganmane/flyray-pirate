package com.github.wxiaoqi.security.common.admin.pay.response;

/**
 * 支付助手app支付回调响应实体
 * @author Administrator
 */
public class PayHelperReponse {

	private String return_code;

	private String return_msg;

	public String getReturn_code() {
		return return_code;
	}

	public void setReturn_code(String return_code) {
		this.return_code = return_code;
	}

	public String getReturn_msg() {
		return return_msg;
	}

	public void setReturn_msg(String return_msg) {
		this.return_msg = return_msg;
	}

}
