package com.github.wxiaoqi.security.common.qr.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("查询商品订单请求参数")
public class QrQueryOrderParam {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty("平台编号")
	private String platformId;

	@ApiModelProperty("商户号")
	private String merchantId;
	
	@ApiModelProperty("商品编号")
	private String goodsId;
	
	@ApiModelProperty("支付订单号")
	private String payOrderNo;
	
	@ApiModelProperty("支付状态")
	private String status;
	
	@ApiModelProperty("省")
	private String province;
	
	@ApiModelProperty("市")
	private String city;
	
	@ApiModelProperty("当前页")
	private Integer page;
	
	@ApiModelProperty("条数")
	private Integer limit;

}
