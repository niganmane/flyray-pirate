package com.github.wxiaoqi.security.common.crm.request;


import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 平台/商户支付通道信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "查询平台 商户支付通道列表信息")
public class QueryPayChannelConfigRequest {
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
    private String platformId;
	
	@ApiModelProperty(value = "商户号")
    private String merchantId;
	
	@ApiModelProperty(value = "当前页码")
	private int page;

	@ApiModelProperty(value = "每页条数")
	private int limit;
}
