package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 更新客户状态
 * @author centerroot
 * @time 创建时间:2018年7月18日上午11:27:21
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "更新客户状态请求参数")
public class UpdateCustomerStatusRequest {
	
	@NotNull(message="用户编号不能为空")
	@ApiModelProperty(value = "用户编号")
	private String customerId;
	
	@NotNull(message="修改状态类型不能为空")
	@ApiModelProperty(value = "状态类型")
	private String type;
	
	@NotNull(message="状态值不能为空")
	@ApiModelProperty(value = "状态值")
	private String status;
	
	
}
