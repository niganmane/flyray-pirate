package com.github.wxiaoqi.security.common.enums;

/** 
* 转账类型标识
* 
*/
public enum AccountTransferTypeEnums {

	p2p("00","个人对个人"),
	p2m("01","个人对商户"),
	m2m("02","商户对商户"),
	m2p("03","商户对个人");
	
    private String code;
    private String desc;
    
    private AccountTransferTypeEnums (String code,String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static AccountTransferTypeEnums getCommentModuleNo(String code){
        for(AccountTransferTypeEnums o : AccountTransferTypeEnums.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
