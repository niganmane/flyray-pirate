package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("后台建议查询参数")
public class AisuanAdviceQueryAdminParam extends BaseParam{
	
	@ApiModelProperty("建议编号")
	private Long adviceId;
	
	@ApiModelProperty("页数")
	private Integer page;
	
	@ApiModelProperty("数量")
	private Integer limit;
	
	@ApiModelProperty("会员编号")
	private Long customerId;
	
	@ApiModelProperty("开始时间")
	private String startTime;
	
	@ApiModelProperty("结束时间")
	private String endTime;
	
	@ApiModelProperty("用户角色")
	private Integer userType;
}
