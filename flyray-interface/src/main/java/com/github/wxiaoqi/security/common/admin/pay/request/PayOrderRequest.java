package com.github.wxiaoqi.security.common.admin.pay.request;

import java.io.Serializable;

/** 
* @author: bolei
* @date：2017年4月30日 下午1:22:10 
* @description：支付订单请求
*/

public class PayOrderRequest extends BaseRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/**
	 * 平台编号
	 */
	private String platformId;
	
	/**
	 * 手续费
	 */
	private String payFee;
	
	/**
	 * 支付通道编号
	 */
	private String payChannelNo;
	
	/**
	 * 支付公司编号
	 */
	private String payCompanyNo;
	
	/**
	 * 支付订单号
	 */
	private String payOrderNo;
	
	/**
	 * 扩展map
	 */
	private String extValue;

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getPayFee() {
		return payFee;
	}

	public void setPayFee(String payFee) {
		this.payFee = payFee;
	}

	public String getPayChannelNo() {
		return payChannelNo;
	}

	public void setPayChannelNo(String payChannelNo) {
		this.payChannelNo = payChannelNo;
	}

	public String getPayCompanyNo() {
		return payCompanyNo;
	}

	public void setPayCompanyNo(String payCompanyNo) {
		this.payCompanyNo = payCompanyNo;
	}

	public String getPayOrderNo() {
		return payOrderNo;
	}

	public void setPayOrderNo(String payOrderNo) {
		this.payOrderNo = payOrderNo;
	}

	public String getExtValue() {
		return extValue;
	}

	public void setExtValue(String extValue) {
		this.extValue = extValue;
	}
	
}
