package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 重置密码
 * @author centerroot
 * @time 创建时间:2018年7月18日上午11:22:26
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "重置密码请求参数")
public class ResetPasswordRequest {
	
	@NotNull(message="用户编号不能为空")
	@ApiModelProperty(value = "用户编号")
	private String customerId;
	
	@NotNull(message="密码类型不能为空")
	@ApiModelProperty(value = "密码类型")
	private String type;
	
	@NotNull(message="手机号不能为空")
	@ApiModelProperty(value = "手机号")
	private String phone;
	
}
