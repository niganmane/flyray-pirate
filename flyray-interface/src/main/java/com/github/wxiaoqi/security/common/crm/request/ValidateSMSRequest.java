package com.github.wxiaoqi.security.common.crm.request;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "验证短信请求参数")
public class ValidateSMSRequest {
	
	@NotNull(message="手机号不能为空")
	@ApiModelProperty(value = "手机号")
	private String phone;

	@NotNull(message="验证码不能为空")
	@ApiModelProperty(value = "验证码")
    private String code;

}
