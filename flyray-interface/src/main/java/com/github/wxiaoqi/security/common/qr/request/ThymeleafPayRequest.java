package com.github.wxiaoqi.security.common.qr.request;

/**
 * 跳转支付页
 * @author he
 */
public class ThymeleafPayRequest {
	
	/**
	 * 平台编号
	 */
	private String platformId;
	
	/**
	 * 商户编号
	 */
	private String merchantId;
	
	/**
	 * 商品ID
	 */
	private String goodsId;
	
	/**
	 * 商品价格
	 */
	private String goodsPrice;
	
	/**
	 * 商品图片
	 */
	private String goodsPic;
	
	/**
	 * openid
	 */
	private String openid;
	
	/**
	 * 昵称
	 */
	private String nickname;
	
	/**
	 * 微信code
	 */
	private String code;

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public String getGoodsPrice() {
		return goodsPrice;
	}

	public void setGoodsPrice(String goodsPrice) {
		this.goodsPrice = goodsPrice;
	}

	public String getGoodsPic() {
		return goodsPic;
	}

	public void setGoodsPic(String goodsPic) {
		this.goodsPic = goodsPic;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
