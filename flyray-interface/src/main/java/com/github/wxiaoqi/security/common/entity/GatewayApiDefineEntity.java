package com.github.wxiaoqi.security.common.entity;

import java.io.Serializable;


/**
 * 动态路由表
 * @author centerroot
 * @email ${email}
 * @date 2018-08-28 15:36:21
 */
public class GatewayApiDefineEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    private String id;
	
	    //
    private String path;
	
	    //
    private String serviceId;
	
	    //
    private String url;
	
	    //
    private Integer retryable;
	
	    //
    private Integer enabled;
	
	    //
    private Integer stripPrefix;
	
	    //
    private String apiName;
	

	/**
	 * 设置：
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public String getId() {
		return id;
	}
	/**
	 * 设置：
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * 获取：
	 */
	public String getPath() {
		return path;
	}
	/**
	 * 设置：
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	/**
	 * 获取：
	 */
	public String getServiceId() {
		return serviceId;
	}
	/**
	 * 设置：
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * 获取：
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * 设置：
	 */
	public void setRetryable(Integer retryable) {
		this.retryable = retryable;
	}
	/**
	 * 获取：
	 */
	public Integer getRetryable() {
		return retryable;
	}
	/**
	 * 设置：
	 */
	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}
	/**
	 * 获取：
	 */
	public Integer getEnabled() {
		return enabled;
	}
	/**
	 * 设置：
	 */
	public void setStripPrefix(Integer stripPrefix) {
		this.stripPrefix = stripPrefix;
	}
	/**
	 * 获取：
	 */
	public Integer getStripPrefix() {
		return stripPrefix;
	}
	/**
	 * 设置：
	 */
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}
	/**
	 * 获取：
	 */
	public String getApiName() {
		return apiName;
	}
}
