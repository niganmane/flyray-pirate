package com.github.wxiaoqi.security.common.qr.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("查询当年该商品支付次数统计")
public class QrThenPayStatisticsParam {
	
	@ApiModelProperty("平台编号")
	private String platformId;

	@ApiModelProperty("商户号")
	private String merchantId;
	
	@ApiModelProperty("商品编号")
	private String goodsId;
	
	@ApiModelProperty("地区编号")
	private String areaCode;
	
	@ApiModelProperty("地区等级")
	private String layer;
	
	@ApiModelProperty("日期")
	private String date;
	
	@ApiModelProperty("年份")
	private String year;
	
}
