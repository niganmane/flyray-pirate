package com.github.wxiaoqi.security.common.biz.request;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.github.wxiaoqi.security.common.admin.pay.request.BaseRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 红包积分领取请求
 * 
 * @author hexufeng 2018年1月29日 上午11:44:11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("红包积分领取请求")
public class MarketingReceiveReq extends BaseRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	// /**
	// * 合作平台编号
	// */
	// @NotNull(message="合作平台编号不能为空")
	// private String acceptBizNo;
	//
	// /**
	// * 商户号
	// */
	// @NotNull(message="商户号不能为空")
	// private String merNo;
	//
	// /**
	// * 商户会员号
	// */
	// @NotNull(message="商户会员号不能为空")
	// private String merCustNo;
	// /**
	// * 平台编号
	// */
	// @NotNull(message="平台编号不能为空")
	// private String unionId;

	// 用户编号
	@ApiModelProperty(value = "用户编号")
	private String customerId;

	@NotNull(message = "平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	// 个人客户编号
	@NotNull(message = "个人客户编号不能为空")
	@ApiModelProperty(value = "个人客户编号")
	private String personalId;

	// 商户客户编号
	@NotNull(message = "商户客户编号不能为空")
	@ApiModelProperty(value = "商户客户编号")
	private String merchantId;
	/**
	 * 业务类型 10:红包 20:积分 30:红包和积分
	 */
	@NotNull(message = "业务类型不能为空")
	@ApiModelProperty(value = "业务类型 ")
	private String businessType;

	/**
	 * 红包信息
	 */
	@ApiModelProperty(value = " 红包信息")
	private List<CouponInfo> couponList;

	/**
	 * 积分数量
	 */
	@ApiModelProperty(value = "积分数量")
	private String integralAmount;

}
