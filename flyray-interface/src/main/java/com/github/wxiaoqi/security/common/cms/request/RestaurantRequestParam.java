package com.github.wxiaoqi.security.common.cms.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("餐饮基础信息参数")
public class RestaurantRequestParam extends BaseParam{
	
	
	@ApiModelProperty("序号")
	private String id;
	
	@ApiModelProperty("店铺名称")
	private String name;
	
	@ApiModelProperty("店铺封面照片")
	private String logoImg;
	
	@ApiModelProperty("区域")
	private String area;
	
	@ApiModelProperty("纬度")
	private String latitude;
	
	@ApiModelProperty("经度")
	private String 经度;
	
	@ApiModelProperty("附加信息")
	private String conService;
	
	@ApiModelProperty("人均消费")
	private String perCapitaSpending;
	
	@ApiModelProperty("公告")
	private String bulletin;
	
	@ApiModelProperty("电话")
	private String conMobile;
	
	@ApiModelProperty("营业开始时间")
	private String openBtime;
	
	@ApiModelProperty("营业结束时间")
	private String openEtime;
	
	@ApiModelProperty("门店介绍")
	private String introduce;
	
	@ApiModelProperty("商家资质")
	private String qualification;
	
	@ApiModelProperty("点餐开关")
	private String isOrder;
	
	@ApiModelProperty("预订开关")
	private String isReservation;
	
	@ApiModelProperty("外卖开关")
	private String isTakeaway;

}
