package com.github.wxiaoqi.security.cache;

import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 *　@author bolei
 *　@date Oct 5, 2018
　*　@description https://www.jianshu.com/p/7bf5dc61ca06/
**/

public class CacheProvider {

	//由于当前class不在spring boot框架内（不在web项目中）所以无法使用autowired，使用此种方法进行注入

	private static RedisTemplate<String, String> template = (RedisTemplate<String, String>) SpringBeanUtil.getBean("redisTemplate");
    
    //@Resource
    //private RedisTemplate<String, String> redisTemplate;
 
    public static <T> void set(String key, T value) {
    	String json = JSON.toJSON(value).toString();
        //将json对象转换为字符串
        template.opsForValue().set(key, json);
    }
    
    public static void set(String key, String value) {
        template.opsForValue().set(key, value);
    }

    /**
     * 获取无序集合
     * @param key1
     */
    public static void getMembers(String key1) {
        template.opsForSet().members(key1);
    }

    /**
     * 求两个无序集合的交集
     * @param key1
     * @param key2
     */
    public static void setIntersect(String key1, String key2) {
        template.opsForSet().intersect(key1,key2);
    }

    /**
     * 求两个无序集合的交集存在 key3集合中
     * @param key1
     * @param key2
     * @param key3
     */
    public static void setIntersectAndStore(String key1, String key2, String key3) {
        template.opsForSet().intersectAndStore(key1,key2,key3);
    }

    /**
     * 无序集合的并集
     * @param key1
     * @param key2
     */
    public static void setUnion(String key1, String key2) {
        template.opsForSet().union(key1,key2);
    }

    /**
     * 求两个结合的并集存在 key3中
     * @param key1
     * @param key2
     * @param key3
     */
    public static void setUnionAndStore(String key1, String key2, String key3) {
        template.opsForSet().unionAndStore(key1,key2,key3);
    }

    /**
     * 求两个无序集合的差集存入key3集合中
     * @param key1
     * @param key2
     * @param key3
     */
    public static void setDifferenceAndStore(String key1, String key2, String key3) {
        template.opsForSet().differenceAndStore(key1,key2,key3);
    }

    /**
     * 求两个无序集合的差集
     * @param key1
     * @param key2
     */
    public static void setDifference(String key1, String key2) {
        template.opsForSet().difference(key1,key2);
    }

    public static boolean set(String key, String value, long validTime) {
        boolean result = template.execute(new RedisCallback<Boolean>() {
            @Override
            public Boolean doInRedis(RedisConnection connection) throws DataAccessException {
                RedisSerializer<String> serializer = template.getStringSerializer();
                connection.set(serializer.serialize(key), serializer.serialize(value));
                connection.expire(serializer.serialize(key), validTime);
                return true;
            }
        });
        return result;
    }

	public static <T> T get(String key, Class<T> clazz) {
		JSONObject jso= JSON.parseObject(get(key));
        return JSONObject.toJavaObject(jso, clazz);
    }

    public static String get(String key) {
    	return template.opsForValue().get(key);
        /*String result = template.execute(new RedisCallback<String>() {
            @Override
            public String doInRedis(RedisConnection connection) throws DataAccessException {
                RedisSerializer<String> serializer = template.getStringSerializer();
                byte[] value = connection.get(serializer.serialize(key));
                return serializer.deserialize(value);
            }
        });
        return result;*/
    }

    public static boolean del(String key) {
        return template.delete(key);
    }
}
