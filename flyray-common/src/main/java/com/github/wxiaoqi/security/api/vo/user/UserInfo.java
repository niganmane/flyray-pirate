package com.github.wxiaoqi.security.api.vo.user;

import java.io.Serializable;
import java.util.Date;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-06-21 8:12
 */
public class UserInfo implements Serializable{
    public String id;
    public Long userId;
    public String username;
    public String password;
    public String name;
    private String description;
    private Integer userType;
    private String platformId;
    private String merchantId;
    private String skinStyle;
    
    
    
    

	public String getSkinStyle() {
		return skinStyle;
	}

	public void setSkinStyle(String skinStyle) {
		this.skinStyle = skinStyle;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	

	public String getPlatformId() {
		return platformId;
	}

	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public Date getUpdTime() {
        return updTime;
    }

    public void setUpdTime(Date updTime) {
        this.updTime = updTime;
    }

    private Date updTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "UserInfo [id=" + id + ", userId=" + userId + ", username=" + username + ", password=" + password
				+ ", name=" + name + ", description=" + description + ", userType=" + userType + ", platformId="
				+ platformId + ", merchantId=" + merchantId + ", skinStyle=" + skinStyle + ", updTime=" + updTime + "]";
	}
	
    
}
